<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\meta_gestor_concentrado;
use PHPUnit\Framework\TestCase;

class meta_gestor_concentradoTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_campo_date_contrato()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);

        $campo_date_contrato = 'a';
        $result = $obj->campo_date_contrato($campo_date_contrato);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("contrato.a",$result);

        error::$en_error = false;

        $campo_date_contrato = 'contrato.xss';
        $result = $obj->campo_date_contrato($campo_date_contrato);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("contrato.xss",$result);
        error::$en_error = false;

    }

    final public function test_campo_fecha_pago()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);

        $campo_fecha_pago = 'x';
        $result = $obj->campo_fecha_pago($campo_fecha_pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("pago.x",$result);

        error::$en_error = false;

        $campo_fecha_pago = 'pago.xss';
        $result = $obj->campo_fecha_pago($campo_fecha_pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("pago.xss",$result);
        error::$en_error = false;

    }

    final public function test_campo_movto()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);

        $campo_movto = 'x';
        $result = $obj->campo_movto($campo_movto);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("pago.x",$result);

        error::$en_error = false;

        $campo_movto = 'pago.xss';
        $result = $obj->campo_movto($campo_movto);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("pago.xss",$result);
        error::$en_error = false;

    }

    final public function test_campo_pago_total()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);

        $campo_pago_total = 'x';
        $result = $obj->campo_pago_total($campo_pago_total);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("pago.x",$result);

        error::$en_error = false;

        $campo_date_contrato = 'pago.xss';
        $result = $obj->campo_pago_total($campo_date_contrato);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("pago.xss",$result);
        error::$en_error = false;

    }

    final public function test_campos_set()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);
        $result = $obj->campos_set();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("meta_gestor_concentrado.meta_proceso = tabla2.monto_proceso, meta_gestor_concentrado.monto_de_mas = tabla2.monto_de_mas, meta_gestor_concentrado.monto_nuevo = tabla2.total_tipo_nuevo, meta_gestor_concentrado.monto_contratos_extra = tabla2.total_tipo_viejo",$result);

        error::$en_error = false;


    }

    final public function test_init_campo()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);

        $campo_name = 'a';
        $separator = 'x';
        $result = $obj->init_campo($campo_name, $separator);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("x.a",$result);

        error::$en_error = false;


    }

    final public function test_init_campos()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);

        $campo_date_contrato = 'b';
        $campo_fecha_pago = 'd';
        $campo_movto = 'c';
        $campo_pago_total = 'a';
        $result = $obj->init_campos($campo_date_contrato,$campo_fecha_pago,$campo_movto,$campo_pago_total);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("pago.a",$result->campo_pago_total);
        $this->assertEquals("contrato.b",$result->campo_date_contrato);
        $this->assertEquals("pago.d",$result->campo_fecha_pago);
        $this->assertEquals("pago.c",$result->campo_movto);

        error::$en_error = false;


    }

    final public function test_join_upd()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);

        $campo_date_contrato = 'contrato.fecha';
        $campo_fecha_pago = 'pago.fecha_cliente';
        $campo_movto = 'pago.movimiento';
        $campo_pago_total = 'pago.monto';
        $entidad_empleado = 'empleado';

        $result = $obj->join_upd($campo_date_contrato, $campo_fecha_pago, $campo_movto,$campo_pago_total,
            $entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("(SELECT meta_gestor_concentrado.id AS 'meta_gestor_concentrado_id', meta_gestor_concentrado.empleado_id, meta_gestor_concentrado.meta_proceso, detalle.monto_proceso, detalle.monto_de_mas, pagos.total_tipo_nuevo, pagos.total_tipo_viejo FROM meta_gestor_concentrado LEFT JOIN (SELECT empleado_id, SUM( meta_gestor_detalle.monto_proceso ) AS monto_proceso, SUM( meta_gestor_detalle.monto_de_mas ) AS monto_de_mas FROM meta_gestor_detalle INNER JOIN meta_gestor_concentrado ON meta_gestor_detalle.meta_gestor_concentrado_id = meta_gestor_concentrado.id  WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id GROUP BY empleado_id ) AS detalle ON detalle.empleado_id = meta_gestor_concentrado.empleado_id LEFT JOIN (SELECT tabla.empleado_id, SUM(tabla.tipo_nuevo) AS total_tipo_nuevo, SUM(tabla.tipo_viejo) AS total_tipo_viejo FROM (SELECT pago.empleado_id, SUM(pago.monto) AS total_pago, SUM(IF (contrato.fecha BETWEEN :fecha_inicio AND :fecha_fin, pago.monto, 0)) AS tipo_nuevo, SUM(IF(contrato.fecha < :fecha_inicio, pago.monto, 0)) AS tipo_viejo FROM pago LEFT JOIN (SELECT mgd.contrato_id, mgc.empleado_id FROM meta_gestor_detalle AS mgd LEFT JOIN meta_gestor_concentrado AS mgc ON mgd.meta_gestor_concentrado_id = mgc.id WHERE mgc.meta_gestor_id = :meta_gestor_id GROUP BY mgc.empleado_id, mgd.contrato_id) AS meta_gestor_detalle ON meta_gestor_detalle.contrato_id = pago.contrato_id AND pago.empleado_id = meta_gestor_detalle.empleado_id LEFT JOIN contrato ON contrato.id = pago.contrato_id WHERE pago.`status` = 'activo' AND pago.fecha_cliente BETWEEN :fecha_inicio AND :fecha_fin AND pago.monto > 0 AND pago.movimiento = 'Abono' AND meta_gestor_detalle.contrato_id IS NULL GROUP BY pago.empleado_id) AS tabla GROUP BY tabla.empleado_id ORDER BY tabla.empleado_id) AS pagos ON pagos.empleado_id = meta_gestor_concentrado.empleado_id WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id) AS tabla2 ON meta_gestor_concentrado.id = tabla2.meta_gestor_concentrado_id",$result);

        error::$en_error = false;

        $campo_date_contrato = 'contrato.DocDate';
        $campo_fecha_pago = 'pago.DocDate';
        $campo_movto = 'pago.U_Movto';
        $campo_pago_total = 'pago.DocTotal';
        $entidad_empleado = 'ohem';

        $result = $obj->join_upd($campo_date_contrato, $campo_fecha_pago, $campo_movto,$campo_pago_total,
            $entidad_empleado);

        $this->assertEquals("(SELECT meta_gestor_concentrado.id AS 'meta_gestor_concentrado_id', meta_gestor_concentrado.ohem_id, meta_gestor_concentrado.meta_proceso, detalle.monto_proceso, detalle.monto_de_mas, pagos.total_tipo_nuevo, pagos.total_tipo_viejo FROM meta_gestor_concentrado LEFT JOIN (SELECT ohem_id, SUM( meta_gestor_detalle.monto_proceso ) AS monto_proceso, SUM( meta_gestor_detalle.monto_de_mas ) AS monto_de_mas FROM meta_gestor_detalle INNER JOIN meta_gestor_concentrado ON meta_gestor_detalle.meta_gestor_concentrado_id = meta_gestor_concentrado.id  WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id GROUP BY ohem_id ) AS detalle ON detalle.ohem_id = meta_gestor_concentrado.ohem_id LEFT JOIN (SELECT tabla.ohem_id, SUM(tabla.tipo_nuevo) AS total_tipo_nuevo, SUM(tabla.tipo_viejo) AS total_tipo_viejo FROM (SELECT pago.ohem_id, SUM(pago.DocTotal) AS total_pago, SUM(IF (contrato.DocDate BETWEEN :fecha_inicio AND :fecha_fin, pago.DocTotal, 0)) AS tipo_nuevo, SUM(IF(contrato.DocDate < :fecha_inicio, pago.DocTotal, 0)) AS tipo_viejo FROM pago LEFT JOIN (SELECT mgd.contrato_id, mgc.ohem_id FROM meta_gestor_detalle AS mgd LEFT JOIN meta_gestor_concentrado AS mgc ON mgd.meta_gestor_concentrado_id = mgc.id WHERE mgc.meta_gestor_id = :meta_gestor_id GROUP BY mgc.ohem_id, mgd.contrato_id) AS meta_gestor_detalle ON meta_gestor_detalle.contrato_id = pago.contrato_id AND pago.ohem_id = meta_gestor_detalle.ohem_id LEFT JOIN contrato ON contrato.id = pago.contrato_id WHERE pago.`status` = 'activo' AND pago.DocDate BETWEEN :fecha_inicio AND :fecha_fin AND pago.DocTotal > 0 AND pago.U_Movto = 'Abono' AND meta_gestor_detalle.contrato_id IS NULL GROUP BY pago.ohem_id) AS tabla GROUP BY tabla.ohem_id ORDER BY tabla.ohem_id) AS pagos ON pagos.ohem_id = meta_gestor_concentrado.ohem_id WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id) AS tabla2 ON meta_gestor_concentrado.id = tabla2.meta_gestor_concentrado_id",$result);
        error::$en_error = false;

    }

    final public function test_on_base()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);


        $result = $obj->on_base();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("meta_gestor_concentrado.id = tabla2.meta_gestor_concentrado_id",$result);

        error::$en_error = false;


    }

    final public function test_recalcula_meta()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        //$obj = new liberator($obj);

        $campo_date_contrato = 'DocDate';
        $campo_fecha_pago = 'pago.DocDate';
        $campo_movto = 'U_Movto';
        $campo_pago_total = 'DocTotal';
        $entidad_empleado = 'ohem';
        $result = $obj->recalcula_meta($campo_date_contrato,$campo_fecha_pago,$campo_movto,$campo_pago_total,$entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("UPDATE meta_gestor_concentrado LEFT JOIN (SELECT meta_gestor_concentrado.id AS 'meta_gestor_concentrado_id', meta_gestor_concentrado.ohem_id, meta_gestor_concentrado.meta_proceso, detalle.monto_proceso, detalle.monto_de_mas, pagos.total_tipo_nuevo, pagos.total_tipo_viejo FROM meta_gestor_concentrado LEFT JOIN (SELECT ohem_id, SUM( meta_gestor_detalle.monto_proceso ) AS monto_proceso, SUM( meta_gestor_detalle.monto_de_mas ) AS monto_de_mas FROM meta_gestor_detalle INNER JOIN meta_gestor_concentrado ON meta_gestor_detalle.meta_gestor_concentrado_id = meta_gestor_concentrado.id  WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id GROUP BY ohem_id ) AS detalle ON detalle.ohem_id = meta_gestor_concentrado.ohem_id LEFT JOIN (SELECT tabla.ohem_id, SUM(tabla.tipo_nuevo) AS total_tipo_nuevo, SUM(tabla.tipo_viejo) AS total_tipo_viejo FROM (SELECT pago.ohem_id, SUM(pago.DocTotal) AS total_pago, SUM(IF (contrato.DocDate BETWEEN :fecha_inicio AND :fecha_fin, pago.DocTotal, 0)) AS tipo_nuevo, SUM(IF(contrato.DocDate < :fecha_inicio, pago.DocTotal, 0)) AS tipo_viejo FROM pago LEFT JOIN (SELECT mgd.contrato_id, mgc.ohem_id FROM meta_gestor_detalle AS mgd LEFT JOIN meta_gestor_concentrado AS mgc ON mgd.meta_gestor_concentrado_id = mgc.id WHERE mgc.meta_gestor_id = :meta_gestor_id GROUP BY mgc.ohem_id, mgd.contrato_id) AS meta_gestor_detalle ON meta_gestor_detalle.contrato_id = pago.contrato_id AND pago.ohem_id = meta_gestor_detalle.ohem_id LEFT JOIN contrato ON contrato.id = pago.contrato_id WHERE pago.`status` = 'activo' AND pago.DocDate BETWEEN :fecha_inicio AND :fecha_fin AND pago.DocTotal > 0 AND pago.U_Movto = 'Abono' AND meta_gestor_detalle.contrato_id IS NULL GROUP BY pago.ohem_id) AS tabla GROUP BY tabla.ohem_id ORDER BY tabla.ohem_id) AS pagos ON pagos.ohem_id = meta_gestor_concentrado.ohem_id WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id) AS tabla2 ON meta_gestor_concentrado.id = tabla2.meta_gestor_concentrado_id SET meta_gestor_concentrado.meta_proceso = tabla2.monto_proceso, meta_gestor_concentrado.monto_de_mas = tabla2.monto_de_mas, meta_gestor_concentrado.monto_nuevo = tabla2.total_tipo_nuevo, meta_gestor_concentrado.monto_contratos_extra = tabla2.total_tipo_viejo WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id",$result);

        error::$en_error = false;


    }

    final public function test_statement_mgc()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);


        $result = $obj->statement_mgc();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id",$result);

        error::$en_error = false;


    }

    final public function test_table_detalle()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);
        $entidad_empleado = 'ohem';
        $result = $obj->table_detalle($entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT ohem_id, SUM( meta_gestor_detalle.monto_proceso ) AS monto_proceso, SUM( meta_gestor_detalle.monto_de_mas ) AS monto_de_mas FROM meta_gestor_detalle INNER JOIN meta_gestor_concentrado ON meta_gestor_detalle.meta_gestor_concentrado_id = meta_gestor_concentrado.id  WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id GROUP BY ohem_id ",$result);

        error::$en_error = false;

        $entidad_empleado = 'empleado';
        $result = $obj->table_detalle($entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT empleado_id, SUM( meta_gestor_detalle.monto_proceso ) AS monto_proceso, SUM( meta_gestor_detalle.monto_de_mas ) AS monto_de_mas FROM meta_gestor_detalle INNER JOIN meta_gestor_concentrado ON meta_gestor_detalle.meta_gestor_concentrado_id = meta_gestor_concentrado.id  WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id GROUP BY empleado_id ",$result);


        error::$en_error = false;


    }

    final public function test_table_meta_gestor_detalle()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);
        $entidad_empleado = 'ohem';
        $result = $obj->table_meta_gestor_detalle($entidad_empleado);
       // print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT mgd.contrato_id, mgc.ohem_id FROM meta_gestor_detalle AS mgd LEFT JOIN meta_gestor_concentrado AS mgc ON mgd.meta_gestor_concentrado_id = mgc.id WHERE mgc.meta_gestor_id = :meta_gestor_id GROUP BY mgc.ohem_id, mgd.contrato_id",$result);

        error::$en_error = false;

        $entidad_empleado = 'empleado';
        $result = $obj->table_meta_gestor_detalle($entidad_empleado);
        //print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT mgd.contrato_id, mgc.empleado_id FROM meta_gestor_detalle AS mgd LEFT JOIN meta_gestor_concentrado AS mgc ON mgd.meta_gestor_concentrado_id = mgc.id WHERE mgc.meta_gestor_id = :meta_gestor_id GROUP BY mgc.empleado_id, mgd.contrato_id",$result);


        error::$en_error = false;


    }

    final public function test_tabla_base()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);
        $campo_date_contrato = 'DocDate';
        $campo_fecha_pago = 'pago.DocDate';
        $campo_movto = 'pago.U_Movto';
        $campo_pago_total = 'pago.DocTotal';
        $entidad_empleado = 'ohem';
        $result = $obj->tabla_base($campo_date_contrato,$campo_fecha_pago,$campo_movto,$campo_pago_total,$entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT pago.ohem_id, SUM(pago.DocTotal) AS total_pago, SUM(IF (contrato.DocDate BETWEEN :fecha_inicio AND :fecha_fin, pago.DocTotal, 0)) AS tipo_nuevo, SUM(IF(contrato.DocDate < :fecha_inicio, pago.DocTotal, 0)) AS tipo_viejo FROM pago LEFT JOIN (SELECT mgd.contrato_id, mgc.ohem_id FROM meta_gestor_detalle AS mgd LEFT JOIN meta_gestor_concentrado AS mgc ON mgd.meta_gestor_concentrado_id = mgc.id WHERE mgc.meta_gestor_id = :meta_gestor_id GROUP BY mgc.ohem_id, mgd.contrato_id) AS meta_gestor_detalle ON meta_gestor_detalle.contrato_id = pago.contrato_id AND pago.ohem_id = meta_gestor_detalle.ohem_id LEFT JOIN contrato ON contrato.id = pago.contrato_id WHERE pago.`status` = 'activo' AND pago.DocDate BETWEEN :fecha_inicio AND :fecha_fin AND pago.DocTotal > 0 AND pago.U_Movto = 'Abono' AND meta_gestor_detalle.contrato_id IS NULL GROUP BY pago.ohem_id",$result);



        error::$en_error = false;


    }

    final public function test_table_base_2()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);
        $campo_date_contrato = 'contrato.DocDate';
        $campo_fecha_pago = 'pago.DocDueDate';
        $campo_movto = 'pago.U_Movto';
        $campo_pago_total = 'pago.DocTotal';
        $entidad_empleado = 'ohem';
        $result = $obj->table_base_2($campo_date_contrato,$campo_fecha_pago,$campo_movto,$campo_pago_total,$entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT meta_gestor_concentrado.id AS 'meta_gestor_concentrado_id', meta_gestor_concentrado.ohem_id, meta_gestor_concentrado.meta_proceso, detalle.monto_proceso, detalle.monto_de_mas, pagos.total_tipo_nuevo, pagos.total_tipo_viejo FROM meta_gestor_concentrado LEFT JOIN (SELECT ohem_id, SUM( meta_gestor_detalle.monto_proceso ) AS monto_proceso, SUM( meta_gestor_detalle.monto_de_mas ) AS monto_de_mas FROM meta_gestor_detalle INNER JOIN meta_gestor_concentrado ON meta_gestor_detalle.meta_gestor_concentrado_id = meta_gestor_concentrado.id  WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id GROUP BY ohem_id ) AS detalle ON detalle.ohem_id = meta_gestor_concentrado.ohem_id LEFT JOIN (SELECT tabla.ohem_id, SUM(tabla.tipo_nuevo) AS total_tipo_nuevo, SUM(tabla.tipo_viejo) AS total_tipo_viejo FROM (SELECT pago.ohem_id, SUM(pago.DocTotal) AS total_pago, SUM(IF (contrato.DocDate BETWEEN :fecha_inicio AND :fecha_fin, pago.DocTotal, 0)) AS tipo_nuevo, SUM(IF(contrato.DocDate < :fecha_inicio, pago.DocTotal, 0)) AS tipo_viejo FROM pago LEFT JOIN (SELECT mgd.contrato_id, mgc.ohem_id FROM meta_gestor_detalle AS mgd LEFT JOIN meta_gestor_concentrado AS mgc ON mgd.meta_gestor_concentrado_id = mgc.id WHERE mgc.meta_gestor_id = :meta_gestor_id GROUP BY mgc.ohem_id, mgd.contrato_id) AS meta_gestor_detalle ON meta_gestor_detalle.contrato_id = pago.contrato_id AND pago.ohem_id = meta_gestor_detalle.ohem_id LEFT JOIN contrato ON contrato.id = pago.contrato_id WHERE pago.`status` = 'activo' AND pago.DocDueDate BETWEEN :fecha_inicio AND :fecha_fin AND pago.DocTotal > 0 AND pago.U_Movto = 'Abono' AND meta_gestor_detalle.contrato_id IS NULL GROUP BY pago.ohem_id) AS tabla GROUP BY tabla.ohem_id ORDER BY tabla.ohem_id) AS pagos ON pagos.ohem_id = meta_gestor_concentrado.ohem_id WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id",$result);


        error::$en_error = false;
        $campo_date_contrato = 'contrato.fecha';
        $campo_fecha_pago = 'pago.fecha_cliente';
        $campo_movto = 'pago.movimiento';
        $campo_pago_total = 'pago.monto';
        $entidad_empleado = 'empleado';
        $result = $obj->table_base_2($campo_date_contrato,$campo_fecha_pago,$campo_movto,$campo_pago_total,$entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("SELECT meta_gestor_concentrado.id AS 'meta_gestor_concentrado_id', meta_gestor_concentrado.empleado_id, meta_gestor_concentrado.meta_proceso, detalle.monto_proceso, detalle.monto_de_mas, pagos.total_tipo_nuevo, pagos.total_tipo_viejo FROM meta_gestor_concentrado LEFT JOIN (SELECT empleado_id, SUM( meta_gestor_detalle.monto_proceso ) AS monto_proceso, SUM( meta_gestor_detalle.monto_de_mas ) AS monto_de_mas FROM meta_gestor_detalle INNER JOIN meta_gestor_concentrado ON meta_gestor_detalle.meta_gestor_concentrado_id = meta_gestor_concentrado.id  WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id GROUP BY empleado_id ) AS detalle ON detalle.empleado_id = meta_gestor_concentrado.empleado_id LEFT JOIN (SELECT tabla.empleado_id, SUM(tabla.tipo_nuevo) AS total_tipo_nuevo, SUM(tabla.tipo_viejo) AS total_tipo_viejo FROM (SELECT pago.empleado_id, SUM(pago.monto) AS total_pago, SUM(IF (contrato.fecha BETWEEN :fecha_inicio AND :fecha_fin, pago.monto, 0)) AS tipo_nuevo, SUM(IF(contrato.fecha < :fecha_inicio, pago.monto, 0)) AS tipo_viejo FROM pago LEFT JOIN (SELECT mgd.contrato_id, mgc.empleado_id FROM meta_gestor_detalle AS mgd LEFT JOIN meta_gestor_concentrado AS mgc ON mgd.meta_gestor_concentrado_id = mgc.id WHERE mgc.meta_gestor_id = :meta_gestor_id GROUP BY mgc.empleado_id, mgd.contrato_id) AS meta_gestor_detalle ON meta_gestor_detalle.contrato_id = pago.contrato_id AND pago.empleado_id = meta_gestor_detalle.empleado_id LEFT JOIN contrato ON contrato.id = pago.contrato_id WHERE pago.`status` = 'activo' AND pago.fecha_cliente BETWEEN :fecha_inicio AND :fecha_fin AND pago.monto > 0 AND pago.movimiento = 'Abono' AND meta_gestor_detalle.contrato_id IS NULL GROUP BY pago.empleado_id) AS tabla GROUP BY tabla.empleado_id ORDER BY tabla.empleado_id) AS pagos ON pagos.empleado_id = meta_gestor_concentrado.empleado_id WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id",$result);
        error::$en_error = false;



    }

    final public function test_table_pagos()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);
        $campo_date_contrato = 'contrato.fecha';
        $campo_fecha_pago = 'pago.fecha_cliente';
        $campo_movto = 'pago.movimiento';
        $campo_pago_total = 'pago.monto';
        $entidad_empleado = 'empleado';
        $result = $obj->table_pagos($campo_date_contrato, $campo_fecha_pago, $campo_movto, $campo_pago_total, $entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("SELECT tabla.empleado_id, SUM(tabla.tipo_nuevo) AS total_tipo_nuevo, SUM(tabla.tipo_viejo) AS total_tipo_viejo FROM (SELECT pago.empleado_id, SUM(pago.monto) AS total_pago, SUM(IF (contrato.fecha BETWEEN :fecha_inicio AND :fecha_fin, pago.monto, 0)) AS tipo_nuevo, SUM(IF(contrato.fecha < :fecha_inicio, pago.monto, 0)) AS tipo_viejo FROM pago LEFT JOIN (SELECT mgd.contrato_id, mgc.empleado_id FROM meta_gestor_detalle AS mgd LEFT JOIN meta_gestor_concentrado AS mgc ON mgd.meta_gestor_concentrado_id = mgc.id WHERE mgc.meta_gestor_id = :meta_gestor_id GROUP BY mgc.empleado_id, mgd.contrato_id) AS meta_gestor_detalle ON meta_gestor_detalle.contrato_id = pago.contrato_id AND pago.empleado_id = meta_gestor_detalle.empleado_id LEFT JOIN contrato ON contrato.id = pago.contrato_id WHERE pago.`status` = 'activo' AND pago.fecha_cliente BETWEEN :fecha_inicio AND :fecha_fin AND pago.monto > 0 AND pago.movimiento = 'Abono' AND meta_gestor_detalle.contrato_id IS NULL GROUP BY pago.empleado_id) AS tabla GROUP BY tabla.empleado_id ORDER BY tabla.empleado_id", $result);


        error::$en_error = false;


    }

    final public function test_valida_datos()
    {
        error::$en_error = false;
        $obj = new meta_gestor_concentrado();
        $obj = new liberator($obj);
        $campo_date_contrato = 'contrato.fecha';
        $campo_fecha_pago = 'pago.fecha_cliente';
        $campo_movto = 'pago.movimiento';
        $campo_pago_total = 'pago.monto';
        $entidad_empleado = 'empleado';
        $result = $obj->valida_datos($campo_date_contrato,$campo_fecha_pago,$campo_movto,$campo_pago_total,$entidad_empleado, true);


        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);


        error::$en_error = false;


    }


}
