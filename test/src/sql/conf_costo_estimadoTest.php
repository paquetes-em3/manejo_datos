<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\sql\conf_costo_estimado;
use desarrollo_em3\manejo_datos\sql\contrato;
use PDO;
use PHPUnit\Framework\TestCase;

class conf_costo_estimadoTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }



    final public function test_costo_estimado_actual()
    {
        error::$en_error = false;
        $obj = new conf_costo_estimado();
        //$obj = new liberator($obj);

        $contrato = new \stdClass();
        $empresa_324 = new \stdClass();
        $empresa_324->id = 1;
        $contrato->producto_id = 1;
        $contrato->plaza_id = 1;
        $result = $obj->costo_estimado_actual($contrato,$empresa_324);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT empresa.id AS empresa_id, conf_costo_estimado.costo FROM conf_costo_estimado LEFT JOIN conf_fc_funeraria ON conf_fc_funeraria.conf_costo_estimado_id = conf_costo_estimado.id LEFT JOIN empresa ON conf_fc_funeraria.empresa_id = empresa.id WHERE conf_costo_estimado.empresa_id = 1 AND conf_costo_estimado.producto_id = 1 AND conf_costo_estimado.plaza_id = 1 AND CURDATE() BETWEEN conf_costo_estimado.fecha_inicial AND conf_costo_estimado.fecha_fin",$result);

        error::$en_error = false;


    }

    final public function test_get_conf()
    {
        error::$en_error = false;
        $obj = new conf_costo_estimado();
        //$obj = new liberator($obj);

        $empresa_id = 1;
        $fecha_pago = '2024-01-01';
        $producto_id = 1;
        $plaza_id = 26;
        $result = $obj->get_conf($empresa_id,$fecha_pago, $plaza_id,$producto_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT * FROM conf_costo_estimado AS conf_costo_estimado WHERE conf_costo_estimado.empresa_id = 1 AND conf_costo_estimado.producto_id = 1 AND conf_costo_estimado.plaza_id = 26 AND '2024-01-01' BETWEEN conf_costo_estimado.fecha_inicial AND conf_costo_estimado.fecha_fin",$result);

        error::$en_error = false;


    }

    final public function test_valida_base()
    {
        error::$en_error = false;
        $obj = new conf_costo_estimado();
        $obj = new liberator($obj);

        $empresa_id = 1;
        $producto_id = 1;
        $plaza_id = 26;
        $result = $obj->valida_base($empresa_id,$producto_id,$plaza_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);
        error::$en_error = false;


    }


}
