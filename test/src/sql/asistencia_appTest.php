<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\asistencia_app;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use PHPUnit\Framework\TestCase;

class asistencia_appTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_actualiza_distancia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$seguridad = new liberator($seguridad);

        $distancia_jefe = 0;
        $asistencia_app_id = 1;
        $result = $obj->actualiza_distancia($asistencia_app_id,$distancia_jefe);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("UPDATE asistencia_app SET distancia_jefe = 0 WHERE id = 1",$result);
        error::$en_error = false;


    }

    final public function test_asistencia_app_id()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$seguridad = new liberator($seguridad);

        $entidad_empleado = 'a';
        $fecha = '2020-01-01';
        $ohem_id = '1';
        $result = $obj->asistencia_app_id($entidad_empleado,$fecha,$ohem_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT asistencia_app.id AS asistencia_app_id FROM asistencia_app WHERE asistencia_app.a_id = 1 AND asistencia_app.fecha = '2020-01-01'",$result);
        error::$en_error = false;


    }

    final public function test_asistencias_periodo()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$obj = new liberator($obj);


        $fecha_final = 'ff';
        $fecha_inicial = 'fi';
        $campo_fecha_ingreso = 'cfi';
        $entidad_empleado = 'ee';
        $result = $obj->asistencias_periodo($campo_fecha_ingreso,$entidad_empleado,$fecha_final,$fecha_inicial);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("SELECT plaza.descripcion AS plaza,ee.status AS status,ee.cfi AS fecha_ingreso,asistencia_app.fecha  AS fecha,asistencia_app.id AS asistencia_app_id,asistencia_app.hora_entrada AS hora_entrada,asistencia_app.asistencia AS asistencia,asistencia_app.dia AS dia,ee.nombre_completo AS nombre_completo,asistencia_app.jefe_checo AS jefe_checo,asistencia_app.checada_en_sitio AS checador,asistencia_app.distancia_jefe AS distancia_jefe,ee.id AS ee_id,oficina_trabajo.id AS oficina_trabajo_id,oficina_trabajo.descripcion AS oficina_trabajo_descripcion FROM asistencia_app AS asistencia_app LEFT JOIN ee AS ee ON ee.id = asistencia_app.ee_id LEFT JOIN plaza AS plaza ON plaza.id = ee.plaza_id LEFT JOIN oficina_trabajo AS oficina_trabajo ON oficina_trabajo.id = ee.oficina_trabajo_id WHERE asistencia_app.fecha BETWEEN 'fi' AND 'ff' ORDER BY asistencia_app.fecha ASC", $result);

        error::$en_error = false;


    }

    final public function test_asistencia_conteo_dias()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$obj = new liberator($obj);

        $ohem_id = 20388;
        $fecha_final = '2024-09-09';
        $fecha_inicial = '2024-09-03';
        $entidad_empleado = 'ohem';
        $status = 'inactivo';

        $result = $obj->asistencia_conteo_dias($entidad_empleado, $fecha_inicial, $fecha_final, $ohem_id, $status);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT COUNT(*) AS dias FROM asistencia_app AS asistencia_app WHERE asistencia_app.ohem_id = 20388 AND asistencia_app.asistencia = 'inactivo' AND asistencia_app.fecha BETWEEN '2024-09-03' AND '2024-09-09'", $result);

        error::$en_error = false;

        $ohem_id = 20388;
        $fecha_final = '2024-09-09';
        $fecha_inicial = '2024-09-03';
        $entidad_empleado = 'ohem';
        $status = 'activo';

        $result = $obj->asistencia_conteo_dias($entidad_empleado, $fecha_inicial, $fecha_final, $ohem_id, $status);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT COUNT(*) AS dias FROM asistencia_app AS asistencia_app WHERE asistencia_app.ohem_id = 20388 AND asistencia_app.asistencia = 'activo' AND asistencia_app.fecha BETWEEN '2024-09-03' AND '2024-09-09'", $result);

        error::$en_error = false;

    }

    final public function test_asistencias_por_dias()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$seguridad = new liberator($seguridad);

        $fecha = 'x';
        $result = $obj->asistencias_por_dias($fecha);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT *FROM asistencia_app WHERE fecha = 'x'",$result);
        error::$en_error = false;


    }

    final public function test_asistencias_recientes()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$seguridad = new liberator($seguridad);

        $result = $obj->asistencias_recientes(0);
        //PRINT_R($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT *FROM asistencia_app WHERE asistencia = 'inactivo' AND fecha>='2025-03-04' ORDER BY asistencia_app.fecha DESC LIMIT 10 OFFSET  0",$result);
        error::$en_error = false;


    }

    final public function test_campo_empleado_id()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $entidad_empleado = 'a';
        $result = $obj->campo_empleado_id($entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a.id AS a_id",$result);
        error::$en_error = false;


    }

    final public function test_campos_reporte()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $campo_fecha_ingreso = 'startDate';
        $entidad_empleado = 'ohem';
        $result = $obj->campos_reporte($campo_fecha_ingreso,$entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("plaza.descripcion AS plaza,ohem.status AS status,ohem.startDate AS fecha_ingreso,asistencia_app.fecha  AS fecha,asistencia_app.id AS asistencia_app_id,asistencia_app.hora_entrada AS hora_entrada,asistencia_app.asistencia AS asistencia,asistencia_app.dia AS dia,ohem.nombre_completo AS nombre_completo,asistencia_app.jefe_checo AS jefe_checo,asistencia_app.checada_en_sitio AS checador,asistencia_app.distancia_jefe AS distancia_jefe,ohem.id AS ohem_id,oficina_trabajo.id AS oficina_trabajo_id,oficina_trabajo.descripcion AS oficina_trabajo_descripcion", $result);

        error::$en_error = false;


    }

    final public function test_campos_rpt()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $campo_fecha_ingreso = 'b';
        $entidad_empleado = 'x';
        $result = $obj->campos_rpt($campo_fecha_ingreso,$entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("plaza.descripcion AS plaza",$result->plaza);
        $this->assertEquals("x.status AS status",$result->status);
        $this->assertEquals("x.b AS fecha_ingreso",$result->fecha_ingreso);
        $this->assertEquals("asistencia_app.fecha  AS fecha",$result->fecha);
        $this->assertEquals("asistencia_app.id AS asistencia_app_id",$result->asistencia_app_id);
        $this->assertEquals("asistencia_app.hora_entrada AS hora_entrada",$result->hora_entrada);
        $this->assertEquals("asistencia_app.asistencia AS asistencia",$result->asistencia);
        $this->assertEquals("asistencia_app.dia AS dia",$result->dia);
        $this->assertEquals("x.nombre_completo AS nombre_completo",$result->nombre_completo);
        $this->assertEquals("asistencia_app.jefe_checo AS jefe_checo",$result->jefe_checo);
        $this->assertEquals("asistencia_app.checada_en_sitio AS checador",$result->checador);
        $this->assertEquals("asistencia_app.distancia_jefe AS distancia_jefe",$result->distancia_jefe);
        $this->assertEquals("x.id AS x_id",$result->empleado_id);
        error::$en_error = false;


    }

    final public function test_empleado_esquema_fecha()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$obj = new liberator($obj);


        $campo_esquema_name = 'a';
        $ohem_id = '1';
        $fecha_limit = 'r';
        $entidad_empleado = 'c';
        $entidad_relacion = 'd';
        $result = $obj->empleado_esquema_fecha($campo_esquema_name,$entidad_empleado, $entidad_relacion,$fecha_limit,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT d.id AS d_id, esquema.a AS esquema_name, d.fecha AS esquema_fecha FROM d AS d LEFT JOIN esquema AS esquema ON esquema.id = d.esquema_id WHERE  d.c_id = 1 AND d.fecha <='r' ORDER BY d.fecha DESC LIMIT 1 ",$result);


        error::$en_error = false;


    }

    final public function test_from()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);


        $entidad_empleado = 'empleado';
        $on_emp = 'on_emp';
        $on_plaza = 'on_plaza';
        $on_oficina_trabajo = 'tu_culo';
        $result = $obj->from($entidad_empleado,$on_emp,$on_plaza,$on_oficina_trabajo);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("asistencia_app AS asistencia_app LEFT JOIN empleado AS empleado ON on_emp LEFT JOIN plaza AS plaza ON on_plaza LEFT JOIN oficina_trabajo AS oficina_trabajo ON tu_culo", $result);

        error::$en_error = false;


    }

    final public function test_froms()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);


        $entidad_empleado = 'ohem';
        $result = $obj->froms($entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("asistencia_app AS asistencia_app", $result->asistencia);
        $this->assertEquals("ohem AS ohem", $result->empleado);
        $this->assertEquals("plaza AS plaza", $result->plaza);

        error::$en_error = false;


    }

    final public function test_genera_from()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);


        $entidad_empleado = 'ohem';
        $result = $obj->genera_from($entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("asistencia_app AS asistencia_app LEFT JOIN ohem AS ohem ON ohem.id = asistencia_app.ohem_id LEFT JOIN plaza AS plaza ON plaza.id = ohem.plaza_id LEFT JOIN oficina_trabajo AS oficina_trabajo ON oficina_trabajo.id = ohem.oficina_trabajo_id", $result);

        error::$en_error = false;


    }
    final public function test_get_checada()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$seguridad = new liberator($seguridad);

        $fecha = '1';
        $ohem_id = 1;
        $result = $obj->get_checada('ohem',$fecha,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT *FROM asistencia_app WHERE asistencia_app.ohem_id = 1 AND asistencia_app.fecha = '1'",$result);
        error::$en_error = false;


        $fecha = '1';
        $ohem_id = 1;
        $result = $obj->get_checada('empleado',$fecha,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT *FROM asistencia_app WHERE asistencia_app.empleado_id = 1 AND asistencia_app.fecha = '1'",$result);
        error::$en_error = false;

    }

    final public function test_n_checadas()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$seguridad = new liberator($seguridad);

        $entidad_empleado = 'empleado';
        $fecha = '2020-01-01';
        $ohem_id = 1;
        $result = $obj->n_checadas($entidad_empleado,$fecha,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT COUNT(*) AS n_checadas FROM asistencia_app WHERE asistencia_app.empleado_id = 1 AND asistencia_app.fecha = '2020-01-01'",$result);
        error::$en_error = false;


    }

    final public function test_ons()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);


        $entidad_empleado = 'empleado';
        $result = $obj->ons($entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("empleado.id = asistencia_app.empleado_id", $result->empleado);
        $this->assertEquals("plaza.id = empleado.plaza_id", $result->plaza);

        error::$en_error = false;


    }


    final public function test_subordinados()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$seguridad = new liberator($seguridad);

        $asistencia_row = array();
        $entidad_empleado = 'a';
        $result = $obj->subordinados($asistencia_row,$entidad_empleado);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT *FROM asistencia_app WHERE asistencia_app.a_jefe_inmediato_id = -1 AND asistencia_app.fecha = '1900-01-01'",$result);
        error::$en_error = false;


        $asistencia_row = array();
        $asistencia_row['xxxx_id'] = 100;
        $asistencia_row['fecha'] = 100;
        $entidad_empleado = 'xxxx';
        $result = $obj->subordinados($asistencia_row,$entidad_empleado);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT *FROM asistencia_app WHERE asistencia_app.xxxx_jefe_inmediato_id = 100 AND asistencia_app.fecha = '100'",$result);
        error::$en_error = false;


    }

    final public function test_upd_asistencia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$seguridad = new liberator($seguridad);

        $asistencia_app_id = 1;
        $result = $obj->upd_asistencia($asistencia_app_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("UPDATE asistencia_app SET asistencia = 'activo' WHERE asistencia_app.id=1",$result);
        error::$en_error = false;



    }

    final public function test_upd_checada_en_sitio_activa()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$seguridad = new liberator($seguridad);

        $asistencia_app_id = 1;
        $result = $obj->upd_checada_en_sitio_activa($asistencia_app_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("UPDATE asistencia_app SET checada_en_sitio = 'activo' WHERE asistencia_app.id = 1",$result);
        error::$en_error = false;


    }

    final public function test_upd_dia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$seguridad = new liberator($seguridad);

        $asistencia_app_id = 1;
        $dia = 'a';
        $result = $obj->upd_dia($asistencia_app_id,$dia);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("UPDATE asistencia_app SET dia = 'a' WHERE id = 1",$result);
        error::$en_error = false;


    }

    final public function test_upd_jefe_checo_activo()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$seguridad = new liberator($seguridad);

        $asistencia_app_id = 1;
        $result = $obj->upd_jefe_checo_activo($asistencia_app_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("UPDATE asistencia_app SET jefe_checo = 'activo' WHERE id = 1",$result);
        error::$en_error = false;


    }

    final public function test_valida_asistencia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$seguridad = new liberator($seguridad);

        $entidad_empleado = '1';
        $fecha = '1';
        $ohem_id = 1;
        $result = $obj->valida_asistencia($entidad_empleado,$fecha,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;


    }

    final public function test_valida_data_asistencia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$obj = new liberator($obj);


        $fecha_final = 'c';
        $fecha_inicial = 'd';
        $campo_fecha_ingreso = 'b';
        $entidad_empleado = 'a';
        $result = $obj->valida_data_asistencia($campo_fecha_ingreso,$entidad_empleado,$fecha_final,$fecha_inicial);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);

        error::$en_error = false;


    }
    final public function test_valida_data_asistencia_reg()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$obj = new liberator($obj);


        $campos_esquema_name = 'a';
        $entidad_empleado = 'b';
        $entidad_rel_esquema = 'c';
        $fecha_limit = 'd';
        $ohem_id = '1';

        $result = $obj->valida_data_asistencia_reg($campos_esquema_name,$entidad_empleado,$entidad_rel_esquema,$fecha_limit,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);


        error::$en_error = false;


    }

    final public function test_where_fecha()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);


        $fecha_final = 'a';
        $fecha_inicial = 'b';
        $result = $obj->where_fecha($fecha_final,$fecha_inicial);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("asistencia_app.fecha BETWEEN 'b' AND 'a'", $result);

        error::$en_error = false;


    }

}
