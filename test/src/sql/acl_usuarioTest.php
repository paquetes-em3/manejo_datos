<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\acl_usuario;
use PHPUnit\Framework\TestCase;

class acl_usuarioTest extends TestCase {
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 2;

    }

    final public function test_genera_sentencia_seguridad() {

        error::$en_error = false;
        $obj = new acl_usuario();
        //$obj = new liberator($obj);¿

        $entidad = 'plaza';
        $usuario_id = 1;
        $result = $obj->obten_ids_permiso($entidad, $usuario_id);
        
        $this->assertFalse(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT IFNULL(GROUP_CONCAT(acl_usuario_plaza.plaza_id ),'0') AS plaza  FROM acl_usuario_plaza WHERE acl_usuario_plaza.usuario_id = 1 ", $result);
        
        error::$en_error = false;

        $entidad = 'plaza';
        $usuario_id = 1;
        $status = 'activo';
        $result = $obj->obten_ids_permiso($entidad, $usuario_id, $status);
        
        $this->assertFalse(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT IFNULL(GROUP_CONCAT(acl_usuario_plaza.plaza_id ),'0') AS plaza  FROM acl_usuario_plaza WHERE acl_usuario_plaza.usuario_id = 1 AND acl_usuario_plaza.status = 'activo'", $result);
        
        error::$en_error = false;

        $entidad = 'departamento';
        $usuario_id = 1;
        $status = 'activo';
        $result = $obj->obten_ids_permiso($entidad, $usuario_id, $status);
        
        $this->assertFalse(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT IFNULL(GROUP_CONCAT(acl_usuario_departamento.departamento_id ),'0') AS departamento  FROM acl_usuario_departamento WHERE acl_usuario_departamento.usuario_id = 1 AND acl_usuario_departamento.status = 'activo'", $result);
        
        error::$en_error = false;
    }

    final public function test_valida_datos_permiso() {

        error::$en_error = false;
        $obj = new acl_usuario();
        //$obj = new liberator($obj);¿

        $entidad = 'plaza';
        $usuario_id = 1;
        $result = $obj->valida_datos_permiso($entidad, $usuario_id);

        $this->assertFalse(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);

        error::$en_error = false;

    }
}
