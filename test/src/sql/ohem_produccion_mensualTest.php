<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql\ohem_produccion_mensual;
use PHPUnit\Framework\TestCase;

class ohem_produccion_mensualTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_n_ventas_personales()
    {
        error::$en_error = false;
        $obj = new ohem_produccion_mensual();
        //$seguridad = new liberator($seguridad);

        $ohem_id = 8850;
        $result = $obj->n_ventas_personales($ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT IFNULL(n_ventas_personales,0) AS 'n_ventas_personales', produccion.puesto_id AS puesto_id FROM ohem_produccion_mensual AS produccion INNER JOIN oficina ON produccion.oficina_id = oficina.id INNER JOIN tipo_oficina ON tipo_oficina.id = oficina.tipo_oficina_id WHERE produccion.periodo_comercial_id = (SELECT periodo_comercial.id FROM periodo_comercial WHERE periodo_comercial.fecha_final < (SELECT periodo_comercial.fecha_inicial FROM periodo_comercial WHERE CURDATE() BETWEEN periodo_comercial.fecha_inicial AND periodo_comercial.fecha_final) ORDER BY periodo_comercial.fecha_final DESC LIMIT 1) AND produccion.ohem_id = 8850 AND tipo_oficina.aplica_pabs = 'activo'",$result);
        error::$en_error = false;


    }

}
