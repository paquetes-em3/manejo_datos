<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\registro_percepcion;
use PHPUnit\Framework\TestCase;

class registro_percepcionTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 2;


    }

    final public function test_campos_sql()
    {
        error::$en_error = false;
        $obj = new registro_percepcion();
        //$obj = new liberator($obj);¿

        $entidad_empleado = 'ohem';
        $registro_precepcion_id = 29204;
        $result = $obj->obten_registro_percepcion($entidad_empleado, $registro_precepcion_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('SELECT registro_percepcion.id AS registro_percepcion_id,ohem.id AS ohem_id,ohem.nombre_completo AS ohem_nombre_completo,periodo_pago.id AS periodo_pago_id,periodo_pago.fecha_inicial AS periodo_pago_fecha_inicial,periodo_pago.fecha_final AS periodo_pago_fecha_final,periodicidad_pago.id AS periodicidad_pago_id,periodicidad_pago.n_elementos AS periodicidad_pago_n_elementos FROM registro_percepcion AS registro_percepcion LEFT JOIN ohem AS ohem ON ohem.id = registro_percepcion.ohem_id LEFT JOIN periodo_pago AS periodo_pago ON periodo_pago.id = registro_percepcion.periodo_pago_id LEFT JOIN periodicidad_pago AS periodicidad_pago ON periodicidad_pago.id = periodo_pago.periodicidad_pago_id WHERE registro_percepcion.id = 29204',$result);

        error::$en_error = false;

    }
    final public function test_init_campos_base()
    {
        error::$en_error = false;
        $obj = new registro_percepcion();
        $obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $result = $obj->init_campos_base($entidad_empleado);
       // print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('registro_percepcion.id',$result[0]);
        $this->assertEquals('ohem.id',$result[1]);
        $this->assertEquals('ohem.nombre_completo',$result[2]);
        $this->assertEquals('periodo_pago.id',$result[3]);
        $this->assertEquals('periodo_pago.fecha_inicial',$result[4]);
        $this->assertEquals('periodo_pago.fecha_final',$result[5]);
        $this->assertEquals('periodicidad_pago.id',$result[6]);
        $this->assertEquals('periodicidad_pago.n_elementos',$result[7]);

        error::$en_error = false;

    }
    final public function test_joins()
    {
        error::$en_error = false;
        $obj = new registro_percepcion();
        $obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $result = $obj->joins($entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('LEFT JOIN ohem AS ohem ON ohem.id = registro_percepcion.ohem_id LEFT JOIN periodo_pago AS periodo_pago ON periodo_pago.id = registro_percepcion.periodo_pago_id LEFT JOIN periodicidad_pago AS periodicidad_pago ON periodicidad_pago.id = periodo_pago.periodicidad_pago_id',$result);

        error::$en_error = false;

    }

    final public function test_obten_registro_percepcion()
    {
        error::$en_error = false;
        $obj = new registro_percepcion();
        //$obj = new liberator($obj);

        $entidad_empleado = 'x';
        $registro_percepcion_id = 1;
        $result = $obj->obten_registro_percepcion($entidad_empleado,$registro_percepcion_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('SELECT registro_percepcion.id AS registro_percepcion_id,x.id AS x_id,x.nombre_completo AS x_nombre_completo,periodo_pago.id AS periodo_pago_id,periodo_pago.fecha_inicial AS periodo_pago_fecha_inicial,periodo_pago.fecha_final AS periodo_pago_fecha_final,periodicidad_pago.id AS periodicidad_pago_id,periodicidad_pago.n_elementos AS periodicidad_pago_n_elementos FROM registro_percepcion AS registro_percepcion LEFT JOIN x AS x ON x.id = registro_percepcion.x_id LEFT JOIN periodo_pago AS periodo_pago ON periodo_pago.id = registro_percepcion.periodo_pago_id LEFT JOIN periodicidad_pago AS periodicidad_pago ON periodicidad_pago.id = periodo_pago.periodicidad_pago_id WHERE registro_percepcion.id = 1',$result);

        error::$en_error = false;

    }

}
