<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\deposito_aportaciones;
use desarrollo_em3\manejo_datos\sql\ohem_produccion_mensual;
use desarrollo_em3\manejo_datos\sql\pago;
use PHPUnit\Framework\TestCase;

class deposito_aportacionesTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_actualiza_serie()
    {
        error::$en_error = false;
        $obj = new deposito_aportaciones();
        //$obj = new liberator($obj);


        $deposito_aportaciones_id = 1;
        $serie_id = 1;
        $result = $obj->actualiza_serie($deposito_aportaciones_id,$serie_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("UPDATE deposito_aportaciones SET serie_id = 1 WHERE deposito_aportaciones.id = 1",$result);


        error::$en_error = false;


    }

    final public function test_columnas_deposito_aportacion()
    {
        error::$en_error = false;
        $obj = new deposito_aportaciones();
        $obj = new liberator($obj);


        $entidad_empleado = 'a';
        $result = $obj->columnas_deposito_aportacion($entidad_empleado);
        //PRINT_R($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('deposito_aportaciones.referencia',$result[0]);
        $this->assertEquals('deposito_aportaciones.fecha',$result[1]);
        $this->assertEquals('pago_corte.monto_total',$result[2]);
        $this->assertEquals('a.nombre_completo',$result[6]);
        $this->assertEquals('deposito_aportaciones.id',$result[12]);
        $this->assertEquals('banco.descripcion',$result[14]);

        error::$en_error = false;


    }



    final public function test_deposito_aportaciones_row()
    {
        error::$en_error = false;
        $obj = new deposito_aportaciones();
        //$obj = new liberator($obj);


        $deposito_aportaciones_id = 1;
        $result = $obj->deposito_aportaciones_row($deposito_aportaciones_id,'ohem');

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT deposito_aportaciones.referencia AS deposito_aportaciones_referencia,deposito_aportaciones.fecha AS deposito_aportaciones_fecha,pago_corte.monto_total AS pago_corte_monto_total,pago_corte.monto_depositado AS pago_corte_monto_depositado,pago_corte.monto_por_depositar AS pago_corte_monto_por_depositar,plaza.descripcion AS plaza_descripcion,ohem.nombre_completo AS ohem_nombre_completo,banco.razon_social AS banco_razon_social,pago_corte.id AS pago_corte_id,plaza.id AS plaza_id,deposito_aportaciones.cuenta_empresa_id AS deposito_aportaciones_cuenta_empresa_id,deposito_aportaciones.monto_depositado AS deposito_aportaciones_monto_depositado,deposito_aportaciones.id AS deposito_aportaciones_id,deposito_aportaciones.validado AS deposito_aportaciones_validado,banco.descripcion AS banco_descripcion,deposito_aportaciones.serie_id AS deposito_aportaciones_serie_id FROM deposito_aportaciones AS deposito_aportaciones LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = deposito_aportaciones.pago_corte_id LEFT JOIN plaza AS plaza ON plaza.id = pago_corte.plaza_id LEFT JOIN ohem AS ohem ON ohem.id = pago_corte.ohem_id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = deposito_aportaciones.cuenta_empresa_id LEFT JOIN banco AS banco ON banco.id = cuenta_empresa.banco_id WHERE deposito_aportaciones.id = 1",$result);


        error::$en_error = false;


    }



    final public function test_columnas_deposito_aportacion_sql()
    {
        error::$en_error = false;
        $obj = new deposito_aportaciones();
        $obj = new liberator($obj);


        $entidad_empleado = 'a';
        $result = $obj->columnas_deposito_aportacion_sql($entidad_empleado);
        //print_r($result);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertStringContainsString("deposito_aportaciones.referencia AS deposito_aportaciones_referencia,deposito_aportaciones.fecha AS deposito_aportaciones_fecha,pago_corte.monto_total AS pago_corte_monto_total,pago_corte.monto_depositado AS pago_corte_monto_depositado,pago_corte.monto_por_depositar AS pago_corte_monto_por_depositar,plaza.descripcion AS plaza_descripcion,a.nombre_completo AS a_nombre_completo,banco.razon_social AS banco_razon_social,pago_corte.id AS pago_corte_id,plaza.id AS plaza_id,deposito_aportaciones.cuenta_empresa_id AS deposito_aportaciones_cuenta_empresa_id,deposito_aportaciones.monto_depositado AS deposito_aportaciones_monto_depositado,deposito_aportaciones.id AS deposito_aportaciones_id,deposito_aportaciones.validado AS deposito_aportaciones_validado",$result);
        $this->assertStringContainsString("deposito_aportaciones.referencia AS deposito_aportaciones_referencia,deposito_aportaciones.fecha AS deposito_aportaciones_fecha,pago_corte.monto_total AS pago_corte_monto_total,pago_corte.monto_depositado AS pago_corte_monto_depositado,pago_corte.monto_por_depositar AS pago_corte_monto_por_depositar,plaza.descripcion AS plaza_descripcion,a.nombre_completo AS a_nombre_completo,banco.razon_social AS banco_razon_social,pago_corte.id AS pago_corte_id,plaza.id AS plaza_id,deposito_aportaciones.cuenta_empresa_id AS deposito_aportaciones_cuenta_empresa_id,deposito_aportaciones.monto_depositado AS deposito_aportaciones_monto_depositado,deposito_aportaciones.id AS deposito_aportaciones_id,deposito_aportaciones.validado AS deposito_aportaciones_validado,banco.descripcion AS banco_descripcion",$result);
        $this->assertEquals("deposito_aportaciones.referencia AS deposito_aportaciones_referencia,deposito_aportaciones.fecha AS deposito_aportaciones_fecha,pago_corte.monto_total AS pago_corte_monto_total,pago_corte.monto_depositado AS pago_corte_monto_depositado,pago_corte.monto_por_depositar AS pago_corte_monto_por_depositar,plaza.descripcion AS plaza_descripcion,a.nombre_completo AS a_nombre_completo,banco.razon_social AS banco_razon_social,pago_corte.id AS pago_corte_id,plaza.id AS plaza_id,deposito_aportaciones.cuenta_empresa_id AS deposito_aportaciones_cuenta_empresa_id,deposito_aportaciones.monto_depositado AS deposito_aportaciones_monto_depositado,deposito_aportaciones.id AS deposito_aportaciones_id,deposito_aportaciones.validado AS deposito_aportaciones_validado,banco.descripcion AS banco_descripcion,deposito_aportaciones.serie_id AS deposito_aportaciones_serie_id",$result);

        error::$en_error = false;


    }

    final public function test_depositos_por_corte_filtro()
    {
        error::$en_error = false;
        $obj = new deposito_aportaciones();
        //$obj = new liberator($obj);


        $pago_corte_id = 1;
        $result = $obj->depositos_por_corte_filtro($pago_corte_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("deposito_aportaciones.pago_corte_id = 1 AND deposito_aportaciones.status = 'activo'",$result);

        error::$en_error = false;


    }

    final public function test_joins_pago_aportaciones()
    {
        error::$en_error = false;
        $obj = new deposito_aportaciones();
        $obj = new liberator($obj);


        $entidad_empleado = 'cc';
        $result = $obj->joins_pago_aportaciones($entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("deposito_aportaciones",$result['pago_corte']);
        $this->assertEquals("pago_corte",$result['plaza']);
        $this->assertEquals("deposito_aportaciones",$result['cuenta_empresa']);
        $this->assertEquals("cuenta_empresa",$result['banco']);
        $this->assertEquals("pago_corte",$result['cc']);

        error::$en_error = false;


    }
    
    final public function test_obten_depositos_por_corte()
    {
        error::$en_error = false;
        $obj = new deposito_aportaciones();
        //$obj = new liberator($obj);

        $pago_corte_id = 72875;
        $result = $obj->obten_depositos_por_corte($pago_corte_id);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT deposito_aportaciones.id AS deposito_aportaciones_id,deposito_aportaciones.pago_corte_id AS deposito_aportaciones_pago_corte_id,deposito_aportaciones.referencia AS deposito_aportaciones_referencia,banco.razon_social AS banco_razon_social,cuenta_empresa.cuenta AS cuenta_empresa_cuenta,deposito_aportaciones.monto_depositado AS deposito_aportaciones_monto_depositado,deposito_aportaciones.validado AS deposito_aportaciones_validado,cuenta_empresa.id AS cuenta_empresa_id,plaza.id AS plaza_id,cuenta_empresa.alias AS cuenta_empresa_alias,serie.codigo AS serie_codigo,banco.descripcion AS banco_descripcion FROM deposito_aportaciones AS deposito_aportaciones  LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = deposito_aportaciones.pago_corte_id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = deposito_aportaciones.cuenta_empresa_id LEFT JOIN ohem AS ohem ON ohem.id = pago_corte.ohem_id LEFT JOIN plaza AS plaza ON plaza.id = pago_corte.plaza_id LEFT JOIN banco AS banco ON banco.id = cuenta_empresa.banco_id LEFT JOIN serie AS serie ON serie.id = deposito_aportaciones.serie_id  WHERE deposito_aportaciones.pago_corte_id = 72875",$result);
        error::$en_error = false;


    }

    final public function test_sum_deposito_por_pago_corte()
    {
        error::$en_error = false;
        $obj = new deposito_aportaciones();
        //$obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $pago_corte_id = 24450;
        $result = $obj->sum_deposito_por_pago_corte($entidad_empleado,$pago_corte_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("SELECT pago_corte.validado AS pago_corte_validado,IFNULL(SUM( deposito_aportaciones.monto_depositado ),0) AS monto_entregado FROM pago_corte AS pago_corte LEFT JOIN deposito_aportaciones AS deposito_aportaciones ON deposito_aportaciones.pago_corte_id = pago_corte.id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = deposito_aportaciones.cuenta_empresa_id LEFT JOIN ohem ON ohem.id = pago_corte.ohem_id LEFT JOIN plaza AS plaza ON plaza.id = pago_corte.plaza_id LEFT JOIN  serie AS serie ON serie.id = deposito_aportaciones.serie_id  WHERE pago_corte.id = 24450",$result);

        error::$en_error = false;


    }



}
