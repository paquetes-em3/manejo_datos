<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql\costo_estimado_det;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class costo_estimado_detTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_cp_empresa()
    {
        error::$en_error = false;
        $obj = new costo_estimado_det();
        //$obj = new liberator($obj);

        $contrato = new stdClass();
        $empresa = new stdClass();
        $empresa->empresa_id = 1;
        $contrato->plaza_id = 1;
        $result = $obj->cp_empresa($contrato,$empresa);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT cp.codigo_postal AS codigo_postal FROM empresa_plaza INNER JOIN cp ON cp.id = empresa_plaza.cp_id WHERE plaza_id = 1 AND empresa_id = 1",$result);

        error::$en_error = false;

        $contrato = new stdClass();
        $empresa = new stdClass();
        $empresa->id = 10;
        $contrato->plaza_id = 1;
        $result = $obj->cp_empresa($contrato,$empresa);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT cp.codigo_postal AS codigo_postal FROM empresa_plaza INNER JOIN cp ON cp.id = empresa_plaza.cp_id WHERE plaza_id = 1 AND empresa_id = 10",$result);
        error::$en_error = false;

    }

    final public function test_contratos_cancelados()
    {
        error::$en_error = false;
        $obj = new costo_estimado_det();
        //$obj = new liberator($obj);

        $where = 'contrato.id = 1';
        $result = $obj->contratos_cancelados($where);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato.id AS contrato_id, contrato.DocDate AS contrato_DocDate, contrato.CardName AS contrato_CardName, contrato.DocTotal AS contrato_DocTotal, contrato.PaidToDate AS contrato_PaidToDate, contrato.U_SeCont AS contrato_U_SeCont, contrato.U_FolioCont AS contrato_U_FolioCont, contrato.U_FeContCancel AS contrato_U_FeContCancel, contrato.U_FechaUltimoPago AS contrato_U_FechaUltimoPago, contrato.U_StatusCob AS contrato_U_StatusCob, empresa.razon_social AS empresa_razon_social, empresa.rfc AS empresa_rfc, factura.serie AS factura_serie, factura.folio AS factura_folio, factura.fecha AS factura_fecha, factura.saldo AS factura_saldo, factura.subtotal AS factura_subtotal, factura.total AS factura_total, factura.uuid AS factura_uuid, nota_credito_sat.id AS nota_credito_sat_id, nota_credito_sat.importe AS nota_credito_sat_importe, nota_credito_sat.iva_trasladado AS nota_credito_sat_iva_trasladado, nota_credito_sat.total AS nota_credito_sat_total, nota_credito_sat.fecha AS nota_credito_sat_fecha, nota_credito_sat.serie AS nota_credito_sat_serie, nota_credito_sat.folio AS nota_credito_sat_folio, nota_credito_sat.uuid AS nota_credito_sat_uuid, producto.id AS producto_id, producto.descripcion AS producto_descripcion, plaza.id AS plaza_id, plaza.descripcion AS plaza_descripcion FROM contrato AS contrato  LEFT JOIN empresa ON empresa.id = contrato.empresa_id  LEFT JOIN factura_contrato ON factura_contrato.contrato_id = contrato.id  LEFT JOIN factura ON factura.id = factura_contrato.factura_id  LEFT JOIN nota_cred_contrato ON nota_cred_contrato.contrato_id = contrato.id  LEFT JOIN nota_credito_sat ON nota_credito_sat.id = nota_cred_contrato.nota_credito_sat_id  LEFT JOIN plaza ON plaza.id = contrato.plaza_id  LEFT JOIN producto ON producto.id = contrato.producto_id  WHERE contrato.id = 1 GROUP BY contrato.id",$result);

        error::$en_error = false;


    }

    final public function test_serie()
    {
        error::$en_error = false;
        $obj = new costo_estimado_det();
        //$obj = new liberator($obj);

        $contrato = new stdClass();
        $contrato->plaza_id = 1;
        $empresa = new stdClass();
        $empresa->empresa_id = 1;
        $tipo_operacion_factura_id = 1;
        $result = $obj->serie($contrato,$empresa,$tipo_operacion_factura_id);
        //print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT * FROM serie_cfdi WHERE tipo_operacion_factura_id = 1 AND plaza_id = 1 AND empresa_id = 1",$result);

        error::$en_error = false;


    }


}
