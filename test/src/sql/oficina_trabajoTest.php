<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\oficina_trabajo;
use PHPUnit\Framework\TestCase;

class oficina_trabajoTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 2;


    }

    final public function test_get_oficina_trabajo_by_id()
    {
        error::$en_error = false;
        $obj = new oficina_trabajo();
        //$obj = new liberator($obj);
        $oficina_trabajo_id = 2;
        $result = $obj->get_oficina_trabajo_by_id($oficina_trabajo_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT id AS oficina_trabajo_id, IFNULL(descripcion,'') AS oficina_trabajo_descripcion FROM oficina_trabajo WHERE oficina_trabajo.id = 2",$result);

        error::$en_error = false;

    }

}
