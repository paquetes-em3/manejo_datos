<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\ohem_produccion_mensual;
use desarrollo_em3\manejo_datos\sql\pago;
use desarrollo_em3\manejo_datos\sql\sala_usuario;
use PHPUnit\Framework\TestCase;

class sala_usuarioTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_campos_seguridad()
    {
        error::$en_error = false;
        $obj = new sala_usuario();
        $obj = new liberator($obj);

        $result = $obj->campos_seguridad();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("sala_usuario.id AS sala_usuario_id,plaza.id AS plaza_id",$result);


        error::$en_error = false;


    }

    final public function test_campos_seguridad_where()
    {
        error::$en_error = false;
        $obj = new sala_usuario();
        $obj = new liberator($obj);


        $usuario_id = 2;
        $result = $obj->campos_seguridad_where($usuario_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("sala_usuario.usuario_id",$result->usuario_id->campo);
        $this->assertEquals("2",$result->usuario_id->value);

        $this->assertEquals("sala_usuario.status",$result->sala_usuario_status->campo);
        $this->assertEquals("activo",$result->sala_usuario_status->value);

        error::$en_error = false;


    }

    final public function test_columnas_seguridad()
    {
        error::$en_error = false;
        $obj = new sala_usuario();
        $obj = new liberator($obj);

        $result = $obj->columnas_seguridad();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("sala_usuario",$result->sala_usuario_id->name_entidad);
        $this->assertEquals("id",$result->sala_usuario_id->name_campo);

        $this->assertEquals("plaza",$result->plaza_id->name_entidad);
        $this->assertEquals("id",$result->plaza_id->name_campo);


        error::$en_error = false;


    }

    final public function test_params_seguridad()
    {
        error::$en_error = false;
        $obj = new sala_usuario();
        $obj = new liberator($obj);

        $result = $obj->params_seguridad();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("sala_usuario",$result->sala->right);
        $this->assertEquals("sala",$result->sala->ren_left);

        $this->assertEquals("sala",$result->plaza->right);
        $this->assertEquals("plaza",$result->plaza->ren_left);

        $this->assertEquals("sala_usuario",$result->usuario->right);
        $this->assertEquals("usuario",$result->usuario->ren_left);


        error::$en_error = false;


    }

    final public function test_sql_permiso()
    {
        error::$en_error = false;
        $obj = new sala_usuario();
        //$obj = new liberator($obj);


        $usuario_id = 2;
        $result = $obj->sql_permiso($usuario_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT sala_usuario.id AS sala_usuario_id,plaza.id AS plaza_id FROM sala_usuario AS sala_usuario LEFT JOIN sala AS sala ON sala.id = sala_usuario.sala_id LEFT JOIN plaza AS plaza ON plaza.id = sala.plaza_id LEFT JOIN usuario AS usuario ON usuario.id = sala_usuario.usuario_id WHERE sala_usuario.usuario_id = '2' AND sala_usuario.status = 'activo'",$result);

        error::$en_error = false;


    }

    final public function test_where_seguridad()
    {
        error::$en_error = false;
        $obj = new sala_usuario();
        $obj = new liberator($obj);


        $usuario_id = 2;
        $result = $obj->where_seguridad($usuario_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("sala_usuario.usuario_id = '2' AND sala_usuario.status = 'activo'",$result);


        error::$en_error = false;


    }



}
