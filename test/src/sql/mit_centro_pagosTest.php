<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql\meta_asistente;
use desarrollo_em3\manejo_datos\sql\mit_centro_pagos;
use PHPUnit\Framework\TestCase;

class mit_centro_pagosTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }



    final public function test_obten_mit_pagos_referencia()
    {
        error::$en_error = false;
        $obj = new mit_centro_pagos();
        //$seguridad = new liberator($seguridad);

        $contrato_id = 1;
        $ejercicio = 2025;
        $numero_semana = 10;
        $state = '';

        $result = $obj->obten_mit_pagos_referencia($contrato_id, $ejercicio, $numero_semana, $state);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT mit_centro_pagos.id AS mit_centro_pagos_id, mit_errores.codigo  AS mit_errores_codigo, mit_errores.descripcion  AS mit_errores_descripcion FROM mit_centro_pagos AS mit_centro_pagos LEFT JOIN contrato AS contrato ON contrato.id = mit_centro_pagos.contrato_id LEFT JOIN plaza AS plaza ON plaza.id = contrato.plaza_id LEFT JOIN mit_errores AS mit_errores ON mit_errores.codigo = mit_centro_pagos.cd_response WHERE contrato.id = '1' AND mit_centro_pagos.ejercicio = '2025' AND mit_centro_pagos.numero_semana = '10' AND mit_centro_pagos.response = ''",$result);
        error::$en_error = false;


    }

}
