<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\rel_serie;
use PHPUnit\Framework\TestCase;

class rel_serieTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 2;


    }

    final public function test_consulta_serie_duplicada_plaza() {
        error::$en_error = false;
        $obj = new rel_serie();
        //$obj = new liberator($obj);¿

        $cantidad = -1;
        $plaza_id = -28;
        $result = $obj->consulta_serie_duplicada_plaza($cantidad, $plaza_id);
        //print_r($result);exit;

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);

        error::$en_error = false;

        $cantidad = 1;
        $plaza_id = 28;
        $result = $obj->consulta_serie_duplicada_plaza($cantidad, $plaza_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT serie.codigo AS serie_codigo, COUNT( serie.codigo ) AS total_serie  FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON serie.id = rel_serie.serie_id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = rel_serie.cuenta_empresa_id LEFT JOIN empresa AS empresa ON empresa.id = cuenta_empresa.empresa_id LEFT JOIN plaza AS plaza ON plaza.id = cuenta_empresa.plaza_id LEFT JOIN banco AS banco ON banco.id = cuenta_empresa.banco_id WHERE plaza_id = 28  GROUP BY serie.codigo  HAVING total_serie > 1",$result);

        error::$en_error = false;

    }

    final public function test_init_campos_base()
    {
        error::$en_error = false;
        $obj = new rel_serie();
        $obj = new liberator($obj);

        $result = $obj->init_campos_base();
        //print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('rel_serie.id AS rel_serie_id,serie.codigo AS serie_codigo,cuenta_empresa.id AS cuenta_empresa_id,cuenta_empresa.cuenta AS cuenta_empresa_cuenta,cuenta_empresa.alias AS cuenta_empresa_alias,banco.descripcion AS banco_descripcion,banco.razon_social AS banco_razon_social',$result);

        error::$en_error = false;

    }

    final public function test_joins()
    {
        error::$en_error = false;
        $obj = new rel_serie();
        $obj = new liberator($obj);

        $result = $obj->joins();
        //print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('LEFT JOIN serie AS serie ON serie.id = rel_serie.serie_id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = rel_serie.cuenta_empresa_id LEFT JOIN empresa AS empresa ON empresa.id = cuenta_empresa.empresa_id LEFT JOIN plaza AS plaza ON plaza.id = cuenta_empresa.plaza_id LEFT JOIN banco AS banco ON banco.id = cuenta_empresa.banco_id',$result);

        error::$en_error = false;

    }

    final public function test_obten_cuenta_bancaria_con_serie() {
        error::$en_error = false;
        $obj = new rel_serie();
        //$obj = new liberator($obj);¿

        $es_contable = '';
        $plaza_id = 28;
        $result = $obj->obten_cuenta_bancaria_con_serie($plaza_id, $es_contable);
        //print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT rel_serie.id AS rel_serie_id,serie.codigo AS serie_codigo,cuenta_empresa.id AS cuenta_empresa_id,cuenta_empresa.cuenta AS cuenta_empresa_cuenta,cuenta_empresa.alias AS cuenta_empresa_alias,banco.descripcion AS banco_descripcion,banco.razon_social AS banco_razon_social FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON serie.id = rel_serie.serie_id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = rel_serie.cuenta_empresa_id LEFT JOIN empresa AS empresa ON empresa.id = cuenta_empresa.empresa_id LEFT JOIN plaza AS plaza ON plaza.id = cuenta_empresa.plaza_id LEFT JOIN banco AS banco ON banco.id = cuenta_empresa.banco_id WHERE plaza.id = 28 AND rel_serie.deposita_aportacion = 'activo'",$result);

        error::$en_error = false;

        $es_contable = 'activo';
        $plaza_id = 28;
        $result = $obj->obten_cuenta_bancaria_con_serie($plaza_id,$es_contable);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT rel_serie.id AS rel_serie_id,serie.codigo AS serie_codigo,cuenta_empresa.id AS cuenta_empresa_id,cuenta_empresa.cuenta AS cuenta_empresa_cuenta,cuenta_empresa.alias AS cuenta_empresa_alias,banco.descripcion AS banco_descripcion,banco.razon_social AS banco_razon_social FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON serie.id = rel_serie.serie_id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = rel_serie.cuenta_empresa_id LEFT JOIN empresa AS empresa ON empresa.id = cuenta_empresa.empresa_id LEFT JOIN plaza AS plaza ON plaza.id = cuenta_empresa.plaza_id LEFT JOIN banco AS banco ON banco.id = cuenta_empresa.banco_id WHERE banco.es_contable = 'activo' AND plaza.id = 28 AND rel_serie.deposita_aportacion = 'activo'",$result);

        error::$en_error = false;

    }

    final public function test_where()
    {
        error::$en_error = false;
        $obj = new rel_serie();
        $obj = new liberator($obj);

        $es_contable = '';
        $plaza_id = 28;
        $result = $obj->where($plaza_id, $es_contable);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("plaza.id = 28 AND rel_serie.deposita_aportacion = 'activo'",$result);

        error::$en_error = false;

        $es_contable = 'activo';
        $plaza_id = 28;
        $result = $obj->where($plaza_id, $es_contable);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("banco.es_contable = 'activo' AND plaza.id = 28 AND rel_serie.deposita_aportacion = 'activo'",$result);

        error::$en_error = false;

    }

}
