<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\ohem_produccion_mensual;
use desarrollo_em3\manejo_datos\sql\pago;
use desarrollo_em3\manejo_datos\sql\pago_corte;
use PHPUnit\Framework\TestCase;
use stdClass;

class pago_corteTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_campos_base()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $campo_monto_pago = 'a';
        $campo_serie = 'a';
        $campo_folio = 'a';
        $result = $obj->campos_base($campo_folio, $campo_monto_pago,$campo_serie,'ohem');
        //print_r($result);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('pago',$result[0]['entidad']);
        $this->assertEquals('a',$result[1]['campo']);
        $this->assertEquals('contrato',$result[2]['entidad']);


        error::$en_error = false;


    }

    final public function test_campos_corte_base()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $entidad_empleado = 'r';
        $result = $obj->campos_corte_base($entidad_empleado);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('plaza.id AS plaza_id, plaza.descripcion AS plaza_descripcion, r.id AS r_id, r.nombre_completo AS r_nombre_completo, pago_corte.id AS pago_corte_id, pago_corte.fecha AS pago_corte_fecha, pago_corte.monto_total AS pago_corte_monto_total,pago_corte.monto_depositado AS pago_corte_monto_depositado,pago_corte.monto_por_depositar AS pago_corte_monto_por_depositar, pago_corte.validado AS pago_corte_validado',$result);

        error::$en_error = false;


    }

    final public function test_campos_emp_sql()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $entidad_empleado = 'd';
        $result = $obj->campos_emp_sql($entidad_empleado);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('d.id AS d_id, d.nombre_completo AS d_nombre_completo',$result);


        error::$en_error = false;


    }

    final public function test_campos_empleado()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $entidad_empleado = 'z';
        $result = $obj->campos_empleado($entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("z.id AS z_id",$result->empleado_id);
        $this->assertEquals("z.nombre_completo AS z_nombre_completo",$result->nombre_completo);
        $this->assertEquals("z_id",$result->keys->empleado_id);
        $this->assertEquals("z_nombre_completo",$result->keys->nombre_completo);
        $this->assertEquals("z.id",$result->keys->empleado_id_filtro);


        error::$en_error = false;


    }

    final public function test_campos_pago_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);


        $result = $obj->campos_pago_corte();
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('pago_corte.id AS pago_corte_id',$result->id);
        $this->assertEquals('pago_corte.fecha AS pago_corte_fecha',$result->fecha);
        $this->assertEquals('pago_corte.monto_depositado AS pago_corte_monto_depositado',$result->monto_depositado);
        $this->assertEquals('pago_corte.monto_por_depositar AS pago_corte_monto_por_depositar',$result->monto_por_depositar);
        $this->assertEquals('pago_corte.validado AS pago_corte_validado',$result->validado);


        error::$en_error = false;


    }

    final public function test_campos_pago_por_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);


        $result = $obj->campos_pago_por_corte('x','DocTotal','U_SeCont','ohem');
        //print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('pago',$result->pago_id->entidad);
        $this->assertEquals('serie',$result->serie_codigo->entidad);
        $this->assertEquals('codigo',$result->serie_codigo->name_campo);
        $this->assertEquals('(SELECT serie.codigo FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie.serie_id = serie.id WHERE rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.U_SeCont) AS serie_codigo',$result->serie_codigo->sql);


        error::$en_error = false;

        $result = $obj->campos_pago_por_corte('x','monto','serie','empleado');
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('pago',$result->pago_id->entidad);
        $this->assertEquals('serie',$result->serie_codigo->entidad);
        $this->assertEquals('codigo',$result->serie_codigo->name_campo);
        $this->assertEquals('(SELECT serie.codigo FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie.serie_id = serie.id WHERE rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.serie) AS serie_codigo',$result->serie_codigo->sql);
        $this->assertEquals('contrato',$result->contrato_serie->entidad);
        $this->assertEquals('serie',$result->contrato_serie->name_campo);


        error::$en_error = false;


    }

    final public function test_campos_pagos_por_corte_rs()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $campo_monto_pago = 'a';
        $campo_serie = 'b';
        $entidad_empleado = 'c';
        $campo_folio = 'd';
        $result = $obj->campos_pagos_por_corte_rs($campo_folio, $campo_monto_pago,$campo_serie,$entidad_empleado);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('c.id AS c_id',$result->c_id);
        $this->assertEquals('(SELECT serie.codigo FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie.serie_id = serie.id WHERE rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.b) AS serie_codigo',$result->serie_codigo);


        error::$en_error = false;



    }

    final public function test_campos_pc_sql()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);


        $result = $obj->campos_pc_sql();
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('pago_corte.id AS pago_corte_id, pago_corte.fecha AS pago_corte_fecha, pago_corte.monto_total AS pago_corte_monto_total,pago_corte.monto_depositado AS pago_corte_monto_depositado,pago_corte.monto_por_depositar AS pago_corte_monto_por_depositar, pago_corte.validado AS pago_corte_validado',$result);


        error::$en_error = false;


    }

    final public function test_campos_plaza()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);


        $result = $obj->campos_plaza();
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('plaza.id AS plaza_id',$result->id);
        $this->assertEquals('plaza.descripcion AS plaza_descripcion',$result->descripcion);


        error::$en_error = false;


    }

    final public function test_campos_sql_pagos_por_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $campo_monto_pago = 'a';
        $campo_serie = 'b';
        $entidad_empleado = 'c';
        $campo_folio = 'fol';
        $result = $obj->campos_sql_pagos_por_corte($campo_folio, $campo_monto_pago,$campo_serie,$entidad_empleado);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals('pago.id AS pago_id,pago.a AS pago_a,contrato.id AS contrato_id,contrato.b AS contrato_b,pago_corte.id AS pago_corte_id,c.id AS c_id,plaza.id AS plaza_id,deposito_pago.id AS deposito_pago_id,deposito_pago.monto_depositado AS deposito_pago_monto_depositado,cuenta_empresa.id AS cuenta_empresa_id,deposito_aportaciones.id AS deposito_aportaciones_id,contrato.fol AS contrato_fol,(SELECT serie.codigo FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie.serie_id = serie.id WHERE rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.b) AS serie_codigo',$result);


        error::$en_error = false;



    }

    final public function test_campos_where_sq_pago_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);



        $campo_serie = 'xxx';
        $result = $obj->campos_where_sq_pago_corte($campo_serie);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("contrato.xxx",$result->codigo->value);


        error::$en_error = false;

    }

    final public function test_cortes()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        //$obj = new liberator($obj);
        $empleados_id = array();
        $entidad_empleado = 'a';
        $fecha_final = 'b';
        $fecha_inicial = 'c';
        $plazas_id = array();
        $result = $obj->cortes($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial,$plazas_id);
        //print_r($result);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT plaza.id AS plaza_id, plaza.descripcion AS plaza_descripcion, a.id AS a_id, a.nombre_completo AS a_nombre_completo, pago_corte.id AS pago_corte_id, pago_corte.fecha AS pago_corte_fecha, pago_corte.monto_total AS pago_corte_monto_total,pago_corte.monto_depositado AS pago_corte_monto_depositado,pago_corte.monto_por_depositar AS pago_corte_monto_por_depositar, pago_corte.validado AS pago_corte_validado FROM pago_corte AS pago_corte LEFT JOIN a AS a ON a.id = pago_corte.a_id LEFT JOIN plaza AS plaza ON plaza.id = pago_corte.plaza_id WHERE pago_corte.fecha BETWEEN 'c' AND 'b'  ",$result);


        error::$en_error = false;



    }

    final public function test_cortes_sin_validar()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        //$obj = new liberator($obj);

        $limit = 10;
        $result = $obj->cortes_sin_validar($limit);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT pago_corte.id AS id FROM pago_corte WHERE pago_corte.monto_total = 0 AND pago_corte.monto_depositado = 0 AND pago_corte.monto_por_depositar = 0 AND pago_corte.validado = 'inactivo' ORDER BY id DESC LIMIT 10",$result);


        error::$en_error = false;


    }

    final public function test_depositos()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        //$obj = new liberator($obj);

        $empleados_id = array();
        $entidad_empleado = 'ohem';
        $fecha_final = '2024-09-30';
        $fecha_inicial = '2024-09-01';
        $plazas_id = array();

        $result = $obj->depositos($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial,$plazas_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("SELECT plaza.id AS plaza_id, plaza.descripcion AS plaza_descripcion, ohem.id AS ohem_id, ohem.nombre_completo AS ohem_nombre_completo, pago_corte.id AS pago_corte_id, pago_corte.fecha AS pago_corte_fecha, pago_corte.monto_total AS pago_corte_monto_total,pago_corte.monto_depositado AS pago_corte_monto_depositado,pago_corte.monto_por_depositar AS pago_corte_monto_por_depositar, pago_corte.validado AS pago_corte_validado, cuenta_empresa.id AS cuenta_empresa_id, cuenta_empresa.cuenta AS cuenta,cuenta_empresa.alias AS cuenta_empresa_alias, empresa.razon_social AS empresa_razon_social,deposito_aportaciones.id AS deposito_aportaciones_id, deposito_aportaciones.monto_depositado AS deposito_aportaciones_monto_depositado,deposito_aportaciones.referencia AS deposito_aportaciones_referencia,deposito_aportaciones.fecha AS deposito_aportaciones_fecha, serie.codigo AS serie_codigo, serie.id AS serie_id FROM deposito_aportaciones AS deposito_aportaciones LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = deposito_aportaciones.pago_corte_id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = deposito_aportaciones.cuenta_empresa_id LEFT JOIN empresa AS empresa ON empresa.id = cuenta_empresa_id LEFT JOIN banco AS banco ON banco.id = cuenta_empresa.banco_id LEFT JOIN serie AS serie ON serie.id = deposito_aportaciones.serie_id LEFT JOIN plaza AS plaza ON plaza.id = cuenta_empresa.plaza_id LEFT JOIN ohem AS ohem ON ohem.id= pago_corte.ohem_id WHERE pago_corte.fecha BETWEEN '2024-09-01' AND '2024-09-30'  ",$result);

        error::$en_error = false;


    }

    final public function test_elementos_base_rpt()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $empleados_id = array();
        $entidad_empleado = 'd';
        $fecha_final = '1';
        $fecha_inicial = '2';
        $plazas_id = array();
        $result = $obj->elementos_base_rpt($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial, $plazas_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('plaza.id AS plaza_id, plaza.descripcion AS plaza_descripcion, d.id AS d_id, d.nombre_completo AS d_nombre_completo, pago_corte.id AS pago_corte_id, pago_corte.fecha AS pago_corte_fecha, pago_corte.monto_total AS pago_corte_monto_total,pago_corte.monto_depositado AS pago_corte_monto_depositado,pago_corte.monto_por_depositar AS pago_corte_monto_por_depositar, pago_corte.validado AS pago_corte_validado',$result->campos_corte);

        error::$en_error = false;


    }

    final public function test_init_campos()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);


        $campo_monto_pago = 'a';
        $entidad_empleado = 'x';
        $campo_serie = 'r';
        $campo_folio = 'folio';
        $result = $obj->init_campos($campo_folio,$campo_monto_pago,$campo_serie,$entidad_empleado);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("x",$result->x_id->entidad);
        $this->assertEquals("a",$result->pago_a->name_campo);


        error::$en_error = false;

    }

    final public function test_ins_rpt_pagos()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $empleados_id = array();
        $entidad_empleado = 'x';
        $plazas_id = array();
        $plazas_id[] = 'x';
        $empleados_id[] = 'y';
        $result = $obj->ins_rpt_pagos($empleados_id,$entidad_empleado,$plazas_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("AND plaza.id IN (x)",$result->plaza);
        $this->assertEquals("AND x.id IN (y)",$result->empleado);


        error::$en_error = false;


    }

    final public function test_keys_empleado()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $entidad_empleado = 'z';
        $result = $obj->keys_empleado($entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("z_id",$result->empleado_id);
        $this->assertEquals("z_nombre_completo",$result->nombre_completo);
        $this->assertEquals("z.id",$result->empleado_id_filtro);

        error::$en_error = false;


    }
    final public function test_joins_base_pago_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $entidad_base = 'a';
        $joins_base = array();
        $lefts = array();
        $lefts[] = 'x';
        $result = $obj->joins_base_pago_corte($entidad_base,$joins_base,$lefts);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("x",$result[0]['left']);
        $this->assertEquals("a",$result[0]['right']);


        error::$en_error = false;


    }

    final public function test_joins_base_pago_corte_left()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $result = $obj->joins_base_pago_corte_left('ohem');

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("contrato",$result[0]['left']);
        $this->assertEquals("pago",$result[0]['right']);

        $this->assertEquals("pago_corte",$result[1]['left']);
        $this->assertEquals("pago",$result[1]['right']);

        $this->assertEquals("ohem",$result[2]['left']);
        $this->assertEquals("pago",$result[2]['right']);

        $this->assertEquals("plaza",$result[3]['left']);
        $this->assertEquals("pago",$result[3]['right']);

        $this->assertEquals("deposito_pago",$result[4]['left']);
        $this->assertEquals("pago",$result[4]['right']);
        $this->assertEquals("pago_id",$result[4]['key_left']);
        $this->assertEquals("id",$result[4]['key_right']);

        $this->assertEquals("deposito_aportaciones",$result[5]['left']);
        $this->assertEquals("deposito_pago",$result[5]['right']);


        error::$en_error = false;


    }

    final public function test_left_joins_txt()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $result = $obj->left_joins_txt('ohem');

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN contrato AS contrato ON contrato.id = pago.contrato_id LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = pago.pago_corte_id LEFT JOIN ohem AS ohem ON ohem.id = pago.ohem_id LEFT JOIN plaza AS plaza ON plaza.id = pago.plaza_id LEFT JOIN deposito_pago AS deposito_pago ON deposito_pago.pago_id = pago.id LEFT JOIN deposito_aportaciones AS deposito_aportaciones ON deposito_aportaciones.id = deposito_pago.deposito_aportaciones_id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = deposito_aportaciones.cuenta_empresa_id",$result);

        error::$en_error = false;


    }

    final public function test_lfs_pagos()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $result = $obj->lfs_pagos('ohem');

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("contrato AS contrato ON contrato.id = pago.contrato_id",$result['contrato']);
        $this->assertEquals("pago_corte AS pago_corte ON pago_corte.id = pago.pago_corte_id",$result['pago_corte']);
        $this->assertEquals("ohem AS ohem ON ohem.id = pago.ohem_id",$result['ohem']);
        $this->assertEquals("plaza AS plaza ON plaza.id = pago.plaza_id",$result['plaza']);
        $this->assertEquals("deposito_pago AS deposito_pago ON deposito_pago.pago_id = pago.id",$result['deposito_pago']);
        $this->assertEquals("deposito_aportaciones AS deposito_aportaciones ON deposito_aportaciones.id = deposito_pago.deposito_aportaciones_id",$result['deposito_aportaciones']);

        error::$en_error = false;


    }

    final public function test_pagos_por_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        //$obj = new liberator($obj);


        $pago_corte_id = 80143;
        $order = '';
        $campo_folio = 'x';
        $result = $obj->pagos_por_corte('Canceled',$campo_folio,'DocTotal','U_Movto','U_SeCont','ohem',$order,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);


        $this->assertEquals("SELECT pago.id AS pago_id,pago.DocTotal AS pago_DocTotal,contrato.id AS contrato_id,contrato.U_SeCont AS contrato_U_SeCont,pago_corte.id AS pago_corte_id,ohem.id AS ohem_id,plaza.id AS plaza_id,deposito_pago.id AS deposito_pago_id,deposito_pago.monto_depositado AS deposito_pago_monto_depositado,cuenta_empresa.id AS cuenta_empresa_id,deposito_aportaciones.id AS deposito_aportaciones_id,contrato.x AS contrato_x,(SELECT serie.codigo FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie.serie_id = serie.id WHERE rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.U_SeCont) AS serie_codigo FROM pago AS pago LEFT JOIN contrato AS contrato ON contrato.id = pago.contrato_id LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = pago.pago_corte_id LEFT JOIN ohem AS ohem ON ohem.id = pago.ohem_id LEFT JOIN plaza AS plaza ON plaza.id = pago.plaza_id LEFT JOIN deposito_pago AS deposito_pago ON deposito_pago.pago_id = pago.id LEFT JOIN deposito_aportaciones AS deposito_aportaciones ON deposito_aportaciones.id = deposito_pago.deposito_aportaciones_id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = deposito_aportaciones.cuenta_empresa_id WHERE pago.pago_corte_id = '80143' AND pago.status = 'activo'  AND pago.Canceled = 'N' AND pago.DocTotal > 0.0",$result);


        error::$en_error = false;

        $pago_corte_id = 80143;
        $order = 'ORDER BY contrato.U_SeCont';
        $result = $obj->pagos_por_corte('Canceled',$campo_folio,'DocTotal','U_Movto','U_SeCont','ohem',$order,$pago_corte_id);



        $this->assertEquals("SELECT pago.id AS pago_id,pago.DocTotal AS pago_DocTotal,contrato.id AS contrato_id,contrato.U_SeCont AS contrato_U_SeCont,pago_corte.id AS pago_corte_id,ohem.id AS ohem_id,plaza.id AS plaza_id,deposito_pago.id AS deposito_pago_id,deposito_pago.monto_depositado AS deposito_pago_monto_depositado,cuenta_empresa.id AS cuenta_empresa_id,deposito_aportaciones.id AS deposito_aportaciones_id,contrato.x AS contrato_x,(SELECT serie.codigo FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie.serie_id = serie.id WHERE rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.U_SeCont) AS serie_codigo FROM pago AS pago LEFT JOIN contrato AS contrato ON contrato.id = pago.contrato_id LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = pago.pago_corte_id LEFT JOIN ohem AS ohem ON ohem.id = pago.ohem_id LEFT JOIN plaza AS plaza ON plaza.id = pago.plaza_id LEFT JOIN deposito_pago AS deposito_pago ON deposito_pago.pago_id = pago.id LEFT JOIN deposito_aportaciones AS deposito_aportaciones ON deposito_aportaciones.id = deposito_pago.deposito_aportaciones_id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = deposito_aportaciones.cuenta_empresa_id WHERE pago.pago_corte_id = '80143' AND pago.status = 'activo'  AND pago.Canceled = 'N' AND pago.DocTotal > 0.0 ORDER BY contrato.U_SeCont",$result);
        error::$en_error = false;
    }

    final public function test_params_pago_por_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $campo_canceled = 'Canceled';
        $campo_monto_pago = 'DocTotal';
        $campo_movto = 'U_Movto';
        $campo_serie = 'U_SeCont';
        $campo_folio = 'x';
        $entidad_empleado = 'ohem';
        $pago_corte_id = 1;
        $result = $obj->params_pago_por_corte($campo_canceled,$campo_folio,$campo_monto_pago,$campo_movto,$campo_serie,$entidad_empleado,$pago_corte_id);
        //print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("pago.pago_corte_id = '1' AND pago.status = 'activo'  AND pago.Canceled = 'N' AND pago.DocTotal > 0.0",$result->filtro);

        $this->assertEquals("pago.id AS pago_id,pago.DocTotal AS pago_DocTotal,contrato.id AS contrato_id,contrato.U_SeCont AS contrato_U_SeCont,pago_corte.id AS pago_corte_id,ohem.id AS ohem_id,plaza.id AS plaza_id,deposito_pago.id AS deposito_pago_id,deposito_pago.monto_depositado AS deposito_pago_monto_depositado,cuenta_empresa.id AS cuenta_empresa_id,deposito_aportaciones.id AS deposito_aportaciones_id,contrato.x AS contrato_x,(SELECT serie.codigo FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie.serie_id = serie.id WHERE rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.U_SeCont) AS serie_codigo",$result->campos);
        $this->assertEquals("LEFT JOIN contrato AS contrato ON contrato.id = pago.contrato_id LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = pago.pago_corte_id LEFT JOIN ohem AS ohem ON ohem.id = pago.ohem_id LEFT JOIN plaza AS plaza ON plaza.id = pago.plaza_id LEFT JOIN deposito_pago AS deposito_pago ON deposito_pago.pago_id = pago.id LEFT JOIN deposito_aportaciones AS deposito_aportaciones ON deposito_aportaciones.id = deposito_pago.deposito_aportaciones_id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = deposito_aportaciones.cuenta_empresa_id",$result->joins);

        error::$en_error = false;

        $campo_canceled = '';
        $campo_monto_pago = 'monto';
        $campo_movto = 'movimiento';
        $campo_serie = 'serie';
        $entidad_empleado = 'empleado';
        $pago_corte_id = 1;
        $result = $obj->params_pago_por_corte($campo_canceled,$campo_folio,$campo_monto_pago,$campo_movto,$campo_serie,$entidad_empleado,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("pago.pago_corte_id = '1' AND pago.status = 'activo'  AND pago.monto > 0.0",$result->filtro);

        $this->assertEquals("pago.id AS pago_id,pago.monto AS pago_monto,contrato.id AS contrato_id,contrato.serie AS contrato_serie,pago_corte.id AS pago_corte_id,empleado.id AS empleado_id,plaza.id AS plaza_id,deposito_pago.id AS deposito_pago_id,deposito_pago.monto_depositado AS deposito_pago_monto_depositado,cuenta_empresa.id AS cuenta_empresa_id,deposito_aportaciones.id AS deposito_aportaciones_id,contrato.x AS contrato_x,(SELECT serie.codigo FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie.serie_id = serie.id WHERE rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.serie) AS serie_codigo",$result->campos);
        $this->assertEquals("LEFT JOIN contrato AS contrato ON contrato.id = pago.contrato_id LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = pago.pago_corte_id LEFT JOIN empleado AS empleado ON empleado.id = pago.empleado_id LEFT JOIN plaza AS plaza ON plaza.id = pago.plaza_id LEFT JOIN deposito_pago AS deposito_pago ON deposito_pago.pago_id = pago.id LEFT JOIN deposito_aportaciones AS deposito_aportaciones ON deposito_aportaciones.id = deposito_pago.deposito_aportaciones_id LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = deposito_aportaciones.cuenta_empresa_id",$result->joins);

        error::$en_error = false;


    }

    final public function test_params_sq_pago_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $campo_serie = 'eee';
        $result = $obj->params_sq_pago_corte($campo_serie);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie.serie_id = serie.id",$result->left_join);
        $this->assertEquals("rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.eee",$result->where);


        error::$en_error = false;

    }

    final public function test_serie_codigo_sq()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $campos = new stdClass();
        $params = new stdClass();
        $params->left_join = 'left';
        $params->where = 'where';
        $result = $obj->serie_codigo_sq($campos,$params);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("serie",$result->serie_codigo->entidad);
        $this->assertEquals("codigo",$result->serie_codigo->name_campo);
        $this->assertEquals("(SELECT serie.codigo FROM left WHERE where) AS serie_codigo",$result->serie_codigo->sql);



        error::$en_error = false;

    }

    final public function test_update_montos()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        //$obj = new liberator($obj);

        $monto_depositado = 0;
        $monto_por_depositar = 0;
        $monto_total = 0;
        $pago_corte_id = 1;
        $result = $obj->update_montos($monto_depositado,$monto_por_depositar,$monto_total,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("UPDATE pago_corte SET monto_depositado = 0, monto_total = 0, monto_por_depositar = 0 WHERE pago_corte.id = 1",$result);


        error::$en_error = false;


    }

    final public function test_update_validado()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        //$obj = new liberator($obj);

        $pago_corte_id = 1;
        $result = $obj->update_validado($pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("UPDATE pago_corte SET validado = 'activo' WHERE pago_corte.id = 1",$result);


        error::$en_error = false;


    }

    final public function test_valida_data_campos()
    {
        error::$en_error = false;
        $obj = new pago_corte();
         //$obj = new liberator($obj);

        $campo_monto_pago = 'a';
        $campo_serie = 'b';
        $entidad_empleado = 'c';
        $campo_folio = 'x';
        $result = $obj->valida_data_campos($campo_folio,$campo_monto_pago,$campo_serie,$entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);


        error::$en_error = false;



    }

    final public function test_where_sq_pago_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);



        $campo_serie = 'xxx';
        $result = $obj->where_sq_pago_corte($campo_serie);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.xxx",$result);


        error::$en_error = false;

    }



}
