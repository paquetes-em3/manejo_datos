<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\sql\ohem_produccion_mensual;
use desarrollo_em3\manejo_datos\sql\pago;
use PDO;
use PHPUnit\Framework\TestCase;

class pagoTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 2;

        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_campo_nombre_empleado()
    {
        error::$en_error = false;
        $obj = new pago();
        $obj = new liberator($obj);

        $entidad_empleado = 'z';
        $result = $obj->campo_nombre_empleado($entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("z_gestor_nombre_completo",$result);

        error::$en_error = false;


    }

    final public function test_campos_base()
    {
        error::$en_error = false;
        $obj = new pago();
        $obj = new liberator($obj);


        $key_empleado_id = 'x';
        $result = $obj->campos_base($key_empleado_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("id",$result[0]);
        $this->assertEquals("status",$result[1]);
        $this->assertEquals("Canceled",$result[2]);
        $this->assertEquals("DocTotal",$result[3]);
        $this->assertEquals("x",$result[4]);
        $this->assertEquals("DocDate",$result[5]);
        $this->assertEquals("DocDueDate",$result[6]);
        $this->assertEquals("U_ComiCobranza",$result[7]);
        $this->assertEquals("U_ComiCalc",$result[8]);
        $this->assertEquals("comision_calculada",$result[9]);
        $this->assertEquals("pago_corte_id",$result[10]);
        $this->assertEquals("plaza_id",$result[11]);

        error::$en_error = false;


    }

    final public function test_campos_sql_base()
    {
        error::$en_error = false;
        $obj = new pago();
        $obj = new liberator($obj);


        $key_empleado_id = 'x';
        $result = $obj->campos_sql_base($key_empleado_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("pago.id AS pago_id,pago.status AS pago_status,pago.Canceled AS pago_Canceled,pago.DocTotal AS pago_DocTotal,pago.x AS pago_x,pago.DocDate AS pago_DocDate,pago.DocDueDate AS pago_DocDueDate,pago.U_ComiCobranza AS pago_U_ComiCobranza,pago.U_ComiCalc AS pago_U_ComiCalc,pago.comision_calculada AS pago_comision_calculada,pago.pago_corte_id AS pago_pago_corte_id,pago.plaza_id AS pago_plaza_id",$result);

        error::$en_error = false;


    }

    final public function test_get_by_id()
    {
        error::$en_error = false;
        $obj = new pago();
        //$seguridad = new liberator($seguridad);

        $pago_id = 1;
        $limit = 2;
        $key_empleado_id = 'ohem_id';
        $result = $obj->get_by_id($pago_id,$key_empleado_id,$limit);
        //print_r($result);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT pago.id AS pago_id,pago.status AS pago_status,pago.Canceled AS pago_Canceled,pago.DocTotal AS pago_DocTotal,pago.ohem_id AS pago_ohem_id,pago.DocDate AS pago_DocDate,pago.DocDueDate AS pago_DocDueDate,pago.U_ComiCobranza AS pago_U_ComiCobranza,pago.U_ComiCalc AS pago_U_ComiCalc,pago.comision_calculada AS pago_comision_calculada,pago.pago_corte_id AS pago_pago_corte_id,pago.plaza_id AS pago_plaza_id FROM pago AS pago LEFT JOIN contrato AS contrato ON contrato.id = pago.contrato_id LEFT JOIN empleado AS empleado_gestor ON empleado_gestor.id = pago.empleado_gestor_id LEFT JOIN plaza AS plaza ON plaza.id = pago.plaza_id LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = pago.pago_corte_id WHERE pago.id LIKE '%1%' ORDER BY pago.id DESC LIMIT 2;",$result);
        //$this->assertEquals("SELECT pago.id AS pago_id,pago.status AS pago_status,pago.Canceled AS pago_Canceled,pago.DocTotal AS pago_DocTotal,pago.ohem_id AS pago_ohem_id,pago.DocDate AS pago_DocDate,pago.DocDueDate AS pago_DocDueDate,pago.U_ComiCobranza AS pago_U_ComiCobranza,pago.U_ComiCalc AS pago_U_ComiCalc,pago.comision_calculada AS pago_comision_calculada,pago.pago_corte_id AS pago_pago_corte_id,pago.plaza_id AS pago_plaza_id FROM pago AS pago LEFT JOIN contrato AS contrato ON contrato.id = pago.contrato_id LEFT JOIN empleado AS empleado_gestor ON empleado_gestor.id = pago.empleado_gestor_id LEFT JOIN plaza AS plaza ON plaza.id = pago.plaza_id LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = pago.pago_corte_id WHERE pago.id LIKE '%1%' ORDER BY pago.id DESC LIMIT 2;",$result);
        error::$en_error = false;


    }

    final public function test_get_by_id_base()
    {
        error::$en_error = false;
        $obj = new pago();
        $obj = new liberator($obj);

        $pago_id = 1;
        $limit = 1;
        $key_empleado_id = 'a';
        $type_operador = 'igual';
        $result = $obj->get_by_id_base($key_empleado_id,$limit,$pago_id,$type_operador);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT pago.id AS pago_id,pago.status AS pago_status,pago.Canceled AS pago_Canceled,pago.DocTotal AS pago_DocTotal,pago.a AS pago_a,pago.DocDate AS pago_DocDate,pago.DocDueDate AS pago_DocDueDate,pago.U_ComiCobranza AS pago_U_ComiCobranza,pago.U_ComiCalc AS pago_U_ComiCalc,pago.comision_calculada AS pago_comision_calculada,pago.pago_corte_id AS pago_pago_corte_id,pago.plaza_id AS pago_plaza_id FROM pago AS pago LEFT JOIN contrato AS contrato ON contrato.id = pago.contrato_id LEFT JOIN empleado AS empleado_gestor ON empleado_gestor.id = pago.empleado_gestor_id LEFT JOIN plaza AS plaza ON plaza.id = pago.plaza_id LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = pago.pago_corte_id WHERE pago.id = '1' ORDER BY pago.id DESC LIMIT 1;",$result);

        error::$en_error = false;


    }

    final public function test_get_by_id_equals()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);

        $pago_id = 1;
        $limit = 1;
        $key_empleado_id = 'x';
        $result = $obj->get_by_id_equals($pago_id,$key_empleado_id,$limit);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT pago.id AS pago_id,pago.status AS pago_status,pago.Canceled AS pago_Canceled,pago.DocTotal AS pago_DocTotal,pago.x AS pago_x,pago.DocDate AS pago_DocDate,pago.DocDueDate AS pago_DocDueDate,pago.U_ComiCobranza AS pago_U_ComiCobranza,pago.U_ComiCalc AS pago_U_ComiCalc,pago.comision_calculada AS pago_comision_calculada,pago.pago_corte_id AS pago_pago_corte_id,pago.plaza_id AS pago_plaza_id FROM pago AS pago LEFT JOIN contrato AS contrato ON contrato.id = pago.contrato_id LEFT JOIN empleado AS empleado_gestor ON empleado_gestor.id = pago.empleado_gestor_id LEFT JOIN plaza AS plaza ON plaza.id = pago.plaza_id LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = pago.pago_corte_id WHERE pago.id = '1' ORDER BY pago.id DESC LIMIT 1;",$result);

        error::$en_error = false;


    }

    final public function test_obten_serie_faltante_entrega()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);

        $campo_monto = 'DocTotal';
        $campo_serie = 'U_SeCont';
        $pago_corte_id = 24451;
        $result = $obj->obten_serie_faltante_entrega($campo_monto,$campo_serie,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato.U_SeCont AS contrato_U_SeCont, SUM( pago.DocTotal ) AS total_entrega, IFNULL( SUM( deposito_pago.monto_depositado ), 0 ) AS monto_entregado FROM pago AS pago LEFT JOIN contrato AS contrato ON contrato.id = pago.contrato_id LEFT JOIN pago_corte ON pago.pago_corte_id = pago_corte.id LEFT JOIN deposito_pago ON pago.id = deposito_pago.pago_id  WHERE pago_corte.id = 24451 AND pago.status = 'activo' GROUP BY contrato.U_SeCont",$result);

        error::$en_error = false;

    }

    final public function test_pago_monto()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);

        $pago_id = 1;
        $result = $obj->pago_monto($pago_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('SELECT pago.id AS pago_id,pago.DocTotal AS pago_DocTotal FROM pago WHERE pago.id = 1',$result);

        error::$en_error = false;


    }

    final public function test_pagos()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);

        $contrato_id = 505238;
        $result = $obj->pagos($contrato_id, $this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("SELECT pago.id AS pago_id,pago.status AS pago_status,pago.usuario_alta_id AS pago_usuario_alta_id,pago.usuario_update_id AS pago_usuario_update_id,pago.fecha_alta AS pago_fecha_alta,pago.fecha_update AS pago_fecha_update,pago.DocEntry AS pago_DocEntry,pago.Canceled AS pago_Canceled,pago.DocDate AS pago_DocDate,pago.DocDueDate AS pago_DocDueDate,pago.CardCode AS pago_CardCode,pago.DocTotal AS pago_DocTotal,pago.Comments AS pago_Comments,pago.U_CodGestor AS pago_U_CodGestor,pago.U_SePago AS pago_U_SePago,pago.U_FolioPago AS pago_U_FolioPago,pago.U_Movto AS pago_U_Movto,pago.U_Papeleria AS pago_U_Papeleria,pago.U_ComiCobranza AS pago_U_ComiCobranza,pago.U_ComiCalc AS pago_U_ComiCalc,pago.contrato_id AS pago_contrato_id,pago.empleado_gestor_id AS pago_empleado_gestor_id,pago.ohem_gestor_id AS pago_ohem_gestor_id,pago.comision_calculada AS pago_comision_calculada,pago.plaza_id AS pago_plaza_id,pago.sincronizado_sap AS pago_sincronizado_sap,pago.comentario_sync AS pago_comentario_sync,pago.ohem_id AS pago_ohem_id,pago.pago_corte_id AS pago_pago_corte_id,pago.latitud AS pago_latitud,pago.longitud AS pago_longitud,pago.colonia AS pago_colonia,pago.folio_asesor AS pago_folio_asesor,pago.metros AS pago_metros,pago.folio_mit AS pago_folio_mit,pago.referencia_mit AS pago_referencia_mit,pago.es_offline AS pago_es_offline,pago.es_transferencia AS pago_es_transferencia, contrato.id AS contrato_id,contrato.status AS contrato_status,contrato.DocEntry AS contrato_DocEntry,contrato.CANCELED AS contrato_CANCELED,contrato.DocDate AS contrato_DocDate,contrato.CardCode AS contrato_CardCode,contrato.CardName AS contrato_CardName,contrato.Address AS contrato_Address,contrato.DocTotal AS contrato_DocTotal,contrato.PaidToDate AS contrato_PaidToDate,contrato.Comments AS contrato_Comments,contrato.SlpCode AS contrato_SlpCode,contrato.Address2 AS contrato_Address2,contrato.OwnerCode AS contrato_OwnerCode,contrato.U_StatusVtas AS contrato_U_StatusVtas,contrato.U_StatusCob AS contrato_U_StatusCob,contrato.U_SeSol AS contrato_U_SeSol,contrato.U_FolioSol AS contrato_U_FolioSol,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont,contrato.U_ImportePago AS contrato_U_ImportePago,contrato.U_FeContCancel AS contrato_U_FeContCancel,contrato.U_InvInicial AS contrato_U_InvInicial,contrato.U_ItemCode AS contrato_U_ItemCode,contrato.U_ItemName AS contrato_U_ItemName,contrato.U_CanalVta AS contrato_U_CanalVta,contrato.U_CondPago AS contrato_U_CondPago,contrato.U_Pagar AS contrato_U_Pagar,contrato.U_Papeleria AS contrato_U_Papeleria,contrato.U_FechaUltimoPago AS contrato_U_FechaUltimoPago,contrato.U_DiasSinAbono AS contrato_U_DiasSinAbono,contrato.U_FechaProximoPago AS contrato_U_FechaProximoPago,contrato.U_TipoMorosidad AS contrato_U_TipoMorosidad,contrato.U_TipoContrato AS contrato_U_TipoContrato,contrato.usuario_alta_id AS contrato_usuario_alta_id,contrato.usuario_update_id AS contrato_usuario_update_id,contrato.fecha_alta AS contrato_fecha_alta,contrato.fecha_update AS contrato_fecha_update,contrato.cliente_id AS contrato_cliente_id,contrato.plaza_id AS contrato_plaza_id,contrato.empleado_id AS contrato_empleado_id,contrato.pagado AS contrato_pagado,contrato.fecha_valida_inversion AS contrato_fecha_valida_inversion,contrato.fecha_valida_informacion AS contrato_fecha_valida_informacion,contrato.valida_inversion_inicial AS contrato_valida_inversion_inicial,contrato.valida_informacion AS contrato_valida_informacion,contrato.status_contrato_id AS contrato_status_contrato_id,contrato.aplica_modifica_sap AS contrato_aplica_modifica_sap,contrato.tipo_oficina_id AS contrato_tipo_oficina_id,contrato.pais_contrato_id AS contrato_pais_contrato_id,contrato.estado_contrato_id AS contrato_estado_contrato_id,contrato.municipio_contrato_id AS contrato_municipio_contrato_id,contrato.pais_cobranza_id AS contrato_pais_cobranza_id,contrato.estado_cobranza_id AS contrato_estado_cobranza_id,contrato.municipio_cobranza_id AS contrato_municipio_cobranza_id,contrato.cp_contrato_id AS contrato_cp_contrato_id,contrato.cp_cobranza_id AS contrato_cp_cobranza_id,contrato.producto_id AS contrato_producto_id,contrato.periodicidad_pago_id AS contrato_periodicidad_pago_id,contrato.nombre_concatenado AS contrato_nombre_concatenado,contrato.telefono AS contrato_telefono,contrato.fecha_ultima_visita AS contrato_fecha_ultima_visita,contrato.fecha_ultima_visita_cliente AS contrato_fecha_ultima_visita_cliente,contrato.fecha_status_contrato AS contrato_fecha_status_contrato,contrato.latitud AS contrato_latitud,contrato.longitud AS contrato_longitud,contrato.dv_santander AS contrato_dv_santander,contrato.dia_pago AS contrato_dia_pago,contrato.dia_pago_1 AS contrato_dia_pago_1,contrato.dia_pago_2 AS contrato_dia_pago_2,contrato.solicitud_servicios_acuerdos_id AS contrato_solicitud_servicios_acuerdos_id,contrato.ohem_callcenter_id AS contrato_ohem_callcenter_id,contrato.fecha_callcenter AS contrato_fecha_callcenter,contrato.monto_promesa_pago AS contrato_monto_promesa_pago,contrato.contrato_estadistica_id AS contrato_contrato_estadistica_id,contrato.periodicidad_local AS contrato_periodicidad_local,contrato.horario_visita AS contrato_horario_visita,contrato.fecha_reprogramada AS contrato_fecha_reprogramada,contrato.fecha_promocion AS contrato_fecha_promocion,contrato.contrato_anterior_id AS contrato_contrato_anterior_id,contrato.url_coordenadas AS contrato_url_coordenadas,contrato.whatsapp AS contrato_whatsapp,contrato.path_documentacion AS contrato_path_documentacion,contrato.conciliado_sap AS contrato_conciliado_sap,contrato.tiene_nc_sap AS contrato_tiene_nc_sap,contrato.ultima_rev_sap AS contrato_ultima_rev_sap,contrato.monto_nc_sap AS contrato_monto_nc_sap,contrato.monto_nc_em3 AS contrato_monto_nc_em3,contrato.monto_pagos_sap AS contrato_monto_pagos_sap,contrato.monto_pagos_em3 AS contrato_monto_pagos_em3,contrato.monto_nc_sap_cancel AS contrato_monto_nc_sap_cancel,contrato.monto_nc_em3_cancel AS contrato_monto_nc_em3_cancel,contrato.monto_pagos_sap_cancel AS contrato_monto_pagos_sap_cancel,contrato.monto_pagos_em3_cancel AS contrato_monto_pagos_em3_cancel,contrato.cargos_sap AS contrato_cargos_sap,contrato.cargos_em3 AS contrato_cargos_em3,contrato.diferencia_cargos AS contrato_diferencia_cargos,contrato.n_contratos_sap AS contrato_n_contratos_sap,contrato.n_contratos_em3 AS contrato_n_contratos_em3,contrato.monto_por_sincronizar AS contrato_monto_por_sincronizar,contrato.revisar_a_detalle AS contrato_revisar_a_detalle,contrato.empresa_id AS contrato_empresa_id,contrato.ultima_reg_saldo AS contrato_ultima_reg_saldo,contrato.costo_est_aplicado AS contrato_costo_est_aplicado  FROM pago AS pago LEFT JOIN contrato ON contrato.id = pago.contrato_id WHERE pago.contrato_id = 505238",$result);

        error::$en_error = false;


    }

    final public function test_pagos_por_corte_con_deposito()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);

        $campo_comentarios = 'Comments';
        $campo_fecha = 'DocDate';
        $campo_fecha_cliente = 'DocDueDate';
        $campo_folio = 'U_FolioCont';
        $campo_monto = 'DocTotal';
        $campo_nombre_cliente = 'CardName';
        $campo_serie = 'U_SeCont';
        $campo_tipo_contrato = 'U_TipoContrato';
        $entidad_empleado = 'ohem';
        $pago_corte_id = 116976;
        $entidad_cliente = '';

        $result = $obj->pagos_por_corte_con_deposito($campo_comentarios,$campo_fecha,$campo_fecha_cliente,$campo_folio,
            $campo_monto,$campo_nombre_cliente,$campo_serie,$campo_tipo_contrato,$entidad_cliente,$entidad_empleado,$pago_corte_id);
        //print_r($result);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato.U_SeCont AS contrato_U_SeCont, contrato.U_FolioCont AS contrato_U_FolioCont, contrato.CardName AS contrato_CardName, pago.DocTotal AS pago_DocTotal, pago.DocDate AS pago_DocDate, pago.DocDueDate AS pago_DocDueDate, ohem.nombre_completo AS ohem_gestor_nombre_completo, ohem.id AS ohem_gestor_id, pago.status AS pago_status, pago.id AS pago_id, pago.Comments AS pago_Comments, contrato.U_TipoContrato AS contrato_U_TipoContrato, deposito_aportaciones.referencia AS referencia,  IF(IFNULL(deposito_pago.id, -1)>0,'bg-success','') AS estilo_css FROM pago AS pago LEFT JOIN contrato AS contrato ON contrato.id = pago.contrato_id LEFT JOIN deposito_pago AS deposito_pago ON deposito_pago.pago_id = pago.id LEFT JOIN deposito_aportaciones AS deposito_aportaciones ON deposito_aportaciones.id = deposito_pago.deposito_aportaciones_id LEFT JOIN ohem AS ohem ON ohem.id = pago.ohem_gestor_id  WHERE pago.pago_corte_id = 116976 ORDER BY pago.DocTotal DESC, deposito_pago.id ASC, contrato.U_SeCont ASC",$result);

        error::$en_error = false;

        $campo_comentarios = 'comentario';
        $campo_fecha = 'fecha_corte';
        $campo_fecha_cliente = 'fecha_cliente';
        $campo_folio = 'folio';
        $campo_monto = 'monto';
        $campo_nombre_cliente = 'nombre_completo';
        $campo_serie = 'serie';
        $campo_tipo_contrato = 'tipo_contrato';
        $entidad_empleado = 'empleado';
        $pago_corte_id = 3826;
        $entidad_cliente = 'cliente';

        $result = $obj->pagos_por_corte_con_deposito($campo_comentarios,$campo_fecha,$campo_fecha_cliente,$campo_folio,
            $campo_monto,$campo_nombre_cliente,$campo_serie,$campo_tipo_contrato,$entidad_cliente,$entidad_empleado,$pago_corte_id);
        //PRINT_R($result);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato.serie AS contrato_serie, contrato.folio AS contrato_folio, cliente.nombre_completo AS cliente_nombre_completo, pago.monto AS pago_monto, pago.fecha_corte AS pago_fecha_corte, pago.fecha_cliente AS pago_fecha_cliente, empleado.nombre_completo AS empleado_nombre_completo, empleado.id AS empleado_id, pago.status AS pago_status, pago.id AS pago_id, pago.comentario AS pago_comentario, contrato.tipo_contrato AS contrato_tipo_contrato, deposito_aportaciones.referencia AS referencia,  IF(IFNULL(deposito_pago.id, -1)>0,'bg-success','') AS estilo_css FROM pago AS pago LEFT JOIN contrato AS contrato ON contrato.id = pago.contrato_id LEFT JOIN deposito_pago AS deposito_pago ON deposito_pago.pago_id = pago.id LEFT JOIN deposito_aportaciones AS deposito_aportaciones ON deposito_aportaciones.id = deposito_pago.deposito_aportaciones_id LEFT JOIN empleado AS empleado ON empleado.id = pago.empleado_id LEFT JOIN cliente AS cliente ON cliente.id = contrato.cliente_id WHERE pago.pago_corte_id = 3826 ORDER BY pago.monto DESC, deposito_pago.id ASC, contrato.serie ASC",$result);

        error::$en_error = false;

    }

    final public function test_pagos_por_corte_filtro()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);

        $pago_corte_id = 1;
        $result = $obj->pagos_por_corte_filtro('Canceled','DocTotal','U_Movto',$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("pago.pago_corte_id = '1' AND pago.status = 'activo'  AND pago.Canceled = 'N' AND pago.DocTotal > 0.0",$result);

        error::$en_error = false;


    }

    final public function test_params_get_by_id()
    {
        error::$en_error = false;
        $obj = new pago();
        $obj = new liberator($obj);


        $result = $obj->params_get_by_id();
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('pago',$result->contrato->right);
        $this->assertEquals('contrato',$result->contrato->ren_left);

        $this->assertEquals('pago',$result->empleado->right);
        $this->assertEquals('empleado_gestor',$result->empleado->ren_left);

        $this->assertEquals('pago',$result->plaza->right);
        $this->assertEquals('plaza',$result->plaza->ren_left);

        $this->assertEquals('pago',$result->pago_corte->right);
        $this->assertEquals('pago_corte',$result->pago_corte->ren_left);


        error::$en_error = false;


    }

    final public function test_suma_pago()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);

        $campo_canceled = 'Canceled';
        $campo_monto = 'a';
        $contrato_id = 1;
        $result = $obj->suma_pago($campo_canceled,$campo_monto,$contrato_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT IFNULL(SUM(pago.a), 0) AS 'suma_pago' FROM pago WHERE pago.CANCELED = 'N' AND pago.status = 'activo' AND pago.contrato_id = 1",$result);

        error::$en_error = false;


    }

    final public function test_valida_entrada_base()
    {
        error::$en_error = false;
        $obj = new pago();
        $obj = new liberator($obj);

        $pago_id = 1;
        $limit = 1;
        $result = $obj->valida_entrada_base($limit,$pago_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        error::$en_error = false;


    }

}
