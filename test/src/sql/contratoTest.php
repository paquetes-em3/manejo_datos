<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\sql\contrato;
use PDO;
use PHPUnit\Framework\TestCase;

class contratoTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }


    final public function test_obten_contratos_gestor()
    {
        error::$en_error = false;
        $obj = new contrato();
        //$obj = new liberator($obj);

        $estructura = "SAP";
        $ohem_id = 5147;
        $status = "activo";
        $result = $obj->obten_contratos_gestor($estructura,$ohem_id,$status);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato.Canceled AS contrato_Canceled,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont,contrato.DocTotal AS contrato_DocTotal,contrato.DocDate AS contrato_DocDate,cliente.U_Beneficiario AS cliente_U_Beneficiario,contrato.U_CondPago AS contrato_U_CondPago,contrato.U_ImportePago AS contrato_U_ImportePago,contrato.U_ItemName AS contrato_U_ItemName,contrato.PaidToDate AS contrato_PaidToDate,contrato.CardName AS contrato_CardName,contrato.id AS contrato_id,contrato.U_FechaUltimoPago AS contrato_U_FechaUltimoPago,contrato.U_FeContCancel AS contrato_U_FeContCancel,contrato.CardName AS contrato_CardName,contrato.U_StatusCob AS contrato_U_StatusCob,contrato.U_TipoMorosidad AS contrato_U_TipoMorosidad,contrato.U_InvInicial AS contrato_U_InvInicial,contrato.U_Papeleria AS contrato_U_Papeleria,cliente_id AS cliente_id FROM contrato AS contrato LEFT JOIN cliente AS cliente ON cliente.id = contrato.cliente_id WHERE cliente.ohem_id = 5147 AND contrato.CANCELED = 'N' AND contrato.STATUS = 'activo' ORDER BY contrato.U_SeCont, contrato.U_FolioCont",$result);

        error::$en_error = false;

        $estructura = "SAP";
        $ohem_id = 5147;
        $result = $obj->obten_contratos_gestor($estructura,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato.Canceled AS contrato_Canceled,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont,contrato.DocTotal AS contrato_DocTotal,contrato.DocDate AS contrato_DocDate,cliente.U_Beneficiario AS cliente_U_Beneficiario,contrato.U_CondPago AS contrato_U_CondPago,contrato.U_ImportePago AS contrato_U_ImportePago,contrato.U_ItemName AS contrato_U_ItemName,contrato.PaidToDate AS contrato_PaidToDate,contrato.CardName AS contrato_CardName,contrato.id AS contrato_id,contrato.U_FechaUltimoPago AS contrato_U_FechaUltimoPago,contrato.U_FeContCancel AS contrato_U_FeContCancel,contrato.CardName AS contrato_CardName,contrato.U_StatusCob AS contrato_U_StatusCob,contrato.U_TipoMorosidad AS contrato_U_TipoMorosidad,contrato.U_InvInicial AS contrato_U_InvInicial,contrato.U_Papeleria AS contrato_U_Papeleria,cliente_id AS cliente_id FROM contrato AS contrato LEFT JOIN cliente AS cliente ON cliente.id = contrato.cliente_id WHERE cliente.ohem_id = 5147 ORDER BY contrato.U_SeCont, contrato.U_FolioCont",$result);

        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $estructura = "EM3";
        $ohem_id = 544;
        $status = "activo";
        $result = $obj->obten_contratos_gestor($estructura,$ohem_id,$status);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato.serie AS contrato_serie,contrato.folio AS contrato_folio,contrato.monto_total AS contrato_monto_total,contrato.fecha AS contrato_fecha,contrato.beneficiario AS contrato_beneficiario,periodicidad_pago.descripcion AS periodicidad_pago_descripcion,contrato.monto_pagado AS contrato_monto_pagado,producto.descripcion AS producto_descripcion,contrato.monto_resto AS contrato_monto_resto,cliente.nombre_completo AS cliente_nombre_completo,contrato.id AS contrato_id,contrato.fecha_ultimo_pago AS contrato_fecha_ultimo_pago,contrato.fecha_cancelacion AS contrato_fecha_cancelacion,status_contrato.descripcion AS status_contrato_descripcion,tipo_morosidad.descripcion AS tipo_morosidad_descripcion,contrato.inversion_inicial AS contrato_inversion_inicial,contrato.papeleria AS contrato_papeleria,cliente_id AS cliente_id FROM contrato AS contrato LEFT JOIN cliente AS cliente ON cliente.id = contrato.cliente_id LEFT JOIN periodicidad_pago AS periodicidad_pago ON periodicidad_pago.id = contrato.periodicidad_pago_id LEFT JOIN status_contrato AS status_contrato ON status_contrato.id = contrato.status_contrato_id LEFT JOIN producto AS producto ON producto.id = contrato.producto_id LEFT JOIN tipo_morosidad AS tipo_morosidad ON tipo_morosidad.id = contrato.tipo_morosidad_id WHERE contrato.empleado_gestor_id = 544 AND contrato.STATUS = 'activo' ORDER BY contrato.serie, contrato.folio",$result);

        error::$en_error = false;

        $estructura = "EM3";
        $ohem_id = 544;
        $result = $obj->obten_contratos_gestor($estructura,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato.serie AS contrato_serie,contrato.folio AS contrato_folio,contrato.monto_total AS contrato_monto_total,contrato.fecha AS contrato_fecha,contrato.beneficiario AS contrato_beneficiario,periodicidad_pago.descripcion AS periodicidad_pago_descripcion,contrato.monto_pagado AS contrato_monto_pagado,producto.descripcion AS producto_descripcion,contrato.monto_resto AS contrato_monto_resto,cliente.nombre_completo AS cliente_nombre_completo,contrato.id AS contrato_id,contrato.fecha_ultimo_pago AS contrato_fecha_ultimo_pago,contrato.fecha_cancelacion AS contrato_fecha_cancelacion,status_contrato.descripcion AS status_contrato_descripcion,tipo_morosidad.descripcion AS tipo_morosidad_descripcion,contrato.inversion_inicial AS contrato_inversion_inicial,contrato.papeleria AS contrato_papeleria,cliente_id AS cliente_id FROM contrato AS contrato LEFT JOIN cliente AS cliente ON cliente.id = contrato.cliente_id LEFT JOIN periodicidad_pago AS periodicidad_pago ON periodicidad_pago.id = contrato.periodicidad_pago_id LEFT JOIN status_contrato AS status_contrato ON status_contrato.id = contrato.status_contrato_id LEFT JOIN producto AS producto ON producto.id = contrato.producto_id LEFT JOIN tipo_morosidad AS tipo_morosidad ON tipo_morosidad.id = contrato.tipo_morosidad_id WHERE contrato.empleado_gestor_id = 544 ORDER BY contrato.serie, contrato.folio",$result);

        error::$en_error = false;
    }
    final public function test_obten_datos_para_bitacora()
    {
        error::$en_error = false;
        $obj = new contrato();
        //$obj = new liberator($obj);

        $contrato_id = 1;
        $result = $obj->obten_datos_para_bitacora($contrato_id);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT status_contrato_id, Comments FROM contrato WHERE id = 1",$result);
        
        error::$en_error = false;


    }
    final public function test_init_campos_contrato_gestor()
    {
        error::$en_error = false;
        $obj = new contrato();
        $obj = new liberator($obj);

        $estructura = 'SAP';
        $result = $obj->init_campos_contrato_gestor($estructura);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("contrato.Canceled AS contrato_Canceled,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont,contrato.DocTotal AS contrato_DocTotal,contrato.DocDate AS contrato_DocDate,cliente.U_Beneficiario AS cliente_U_Beneficiario,contrato.U_CondPago AS contrato_U_CondPago,contrato.U_ImportePago AS contrato_U_ImportePago,contrato.U_ItemName AS contrato_U_ItemName,contrato.PaidToDate AS contrato_PaidToDate,contrato.CardName AS contrato_CardName,contrato.id AS contrato_id,contrato.U_FechaUltimoPago AS contrato_U_FechaUltimoPago,contrato.U_FeContCancel AS contrato_U_FeContCancel,contrato.CardName AS contrato_CardName,contrato.U_StatusCob AS contrato_U_StatusCob,contrato.U_TipoMorosidad AS contrato_U_TipoMorosidad,contrato.U_InvInicial AS contrato_U_InvInicial,contrato.U_Papeleria AS contrato_U_Papeleria,cliente_id AS cliente_id",$result);
        
        error::$en_error = false;

        $estructura = 'EM3';
        $result = $obj->init_campos_contrato_gestor($estructura);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("contrato.serie AS contrato_serie,contrato.folio AS contrato_folio,contrato.monto_total AS contrato_monto_total,contrato.fecha AS contrato_fecha,contrato.beneficiario AS contrato_beneficiario,periodicidad_pago.descripcion AS periodicidad_pago_descripcion,contrato.monto_pagado AS contrato_monto_pagado,producto.descripcion AS producto_descripcion,contrato.monto_resto AS contrato_monto_resto,cliente.nombre_completo AS cliente_nombre_completo,contrato.id AS contrato_id,contrato.fecha_ultimo_pago AS contrato_fecha_ultimo_pago,contrato.fecha_cancelacion AS contrato_fecha_cancelacion,status_contrato.descripcion AS status_contrato_descripcion,tipo_morosidad.descripcion AS tipo_morosidad_descripcion,contrato.inversion_inicial AS contrato_inversion_inicial,contrato.papeleria AS contrato_papeleria,cliente_id AS cliente_id",$result);
        
        error::$en_error = false;
    }

    final public function test_init_join_contrato_gestor()
    {
        error::$en_error = false;
        $obj = new contrato();
        $obj = new liberator($obj);

        $estructura = 'SAP';
        $result = $obj->init_join_contrato_gestor($estructura);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN cliente AS cliente ON cliente.id = contrato.cliente_id",$result);
        
        error::$en_error = false;

        $estructura = 'EM3';
        $result = $obj->init_join_contrato_gestor($estructura);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN cliente AS cliente ON cliente.id = contrato.cliente_id LEFT JOIN periodicidad_pago AS periodicidad_pago ON periodicidad_pago.id = contrato.periodicidad_pago_id LEFT JOIN status_contrato AS status_contrato ON status_contrato.id = contrato.status_contrato_id LEFT JOIN producto AS producto ON producto.id = contrato.producto_id LEFT JOIN tipo_morosidad AS tipo_morosidad ON tipo_morosidad.id = contrato.tipo_morosidad_id",$result);
        
        error::$en_error = false;
    }

    final public function test_params_sum()
    {
        error::$en_error = false;
        $obj = new contrato();
        //$obj = new liberator($obj);


        $alias = 'c';
        $campo = 'a';
        $contrato_id = '1';
        $entidad = 'b';
        $campo_canceled = 'CANCELED';
        $result = $obj->params_sum($alias, $campo_canceled,$campo,$contrato_id,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("IFNULL(SUM(b.a), 0) AS 'c'",$result->campo);
        $this->assertEquals("b.CANCELED = 'N' AND b.status = 'activo' AND b.contrato_id = 1",$result->where);
        error::$en_error = false;


    }
    final public function test_sentencias_sum()
    {
        error::$en_error = false;
        $obj = new contrato();
        $obj = new liberator($obj);


        $entidad = 'rrr';
        $contrato_id = 1;
        $campo_canceled = 'CANCELED';
        $result = $obj->sentencias_sum($campo_canceled, $contrato_id,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("rrr.status = 'activo'",$result['wh_status']);
        $this->assertEquals("rrr.CANCELED = 'N'",$result['wh_canceled']);
        $this->assertEquals("rrr.contrato_id = 1",$result['wh_contrato_id']);
        error::$en_error = false;


        $entidad = 'rrr';
        $contrato_id = 1;
        $campo_canceled = '';
        $result = $obj->sentencias_sum($campo_canceled, $contrato_id,$entidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertArrayNotHasKey('wh_canceled',$result);

        error::$en_error = false;
    }

    final public function test_update_empresa()
    {
        error::$en_error = false;
        $obj = new contrato();
        //$obj = new liberator($obj);


        $contrato_id = 1;
        $empresa_id = 1;
        $result = $obj->update_empresa($contrato_id,$empresa_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("UPDATE contrato SET empresa_id = 1 WHERE id = 1",$result);
        error::$en_error = false;

    }

    final public function test_obten_contrato_excel()
    {
        error::$en_error = false;
        $obj = new contrato();
        //$obj = new liberator($obj);

        $campos = array('contrato.id','contrato.U_StatusCob','contrato.U_SeCont','contrato.U_FolioCont');
        $campo_serie = 'U_SeCont';
        $campo_folio = 'U_FolioCont';
        $contrato = '';
        $contrato_id = 0;
        $limit = 1;
        $offset = 0;
        $result = $obj->obten_contrato_excel($campos,$campo_folio,$campo_serie,$contrato,$contrato_id,$limit,$offset);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato.id AS contrato_id,contrato.U_StatusCob AS contrato_U_StatusCob,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont FROM contrato  LIMIT 1 OFFSET 0",$result);
        error::$en_error = false;

        $campos = array('contrato.id','contrato.U_StatusCob','contrato.U_SeCont','contrato.U_FolioCont');
        $campo_serie = 'U_SeCont';
        $campo_folio = 'U_FolioCont';
        $contrato = "SALC18135";
        $contrato_id = 0;
        $limit = 1;
        $offset = 0;
        $plaza_id = 30;
        $result = $obj->obten_contrato_excel($campos,$campo_folio,$campo_serie,$contrato,$contrato_id,$limit,$offset,$plaza_id);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato.id AS contrato_id,contrato.U_StatusCob AS contrato_U_StatusCob,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont FROM contrato  LIMIT 1 OFFSET 0",$result);
        error::$en_error = false;
     
        $campos = array('contrato.id','contrato.U_StatusCob','contrato.U_SeCont','contrato.U_FolioCont');
        $campo_serie = 'U_SeCont';
        $campo_folio = 'U_FolioCont';
        $limit = 0;
        $contrato_id = 565312;
        $contrato = "SALC18135";
        $result = $obj->obten_contrato_excel($campos,$campo_folio,$campo_serie,$contrato,$contrato_id,$limit,$offset);
        //print_r($result);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato.id AS contrato_id,contrato.U_StatusCob AS contrato_U_StatusCob,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont FROM contrato WHERE contrato.id = 565312",$result);
        error::$en_error = false;
    }

    final public function test_wh_contrato_id()
    {
        error::$en_error = false;
        $obj = new contrato();
        $obj = new liberator($obj);


        $entidad = 'eee';
        $contrato_id = 1;
        $result = $obj->wh_contrato_id($contrato_id, $entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("eee.contrato_id = 1",$result);
        error::$en_error = false;


    }

    final public function test_where_sum()
    {
        error::$en_error = false;
        $obj = new contrato();
        $obj = new liberator($obj);


        $entidad = 'ssss';
        $contrato_id = 1;
        $campo_canceled = 'CANCELED';
        $result = $obj->where_sum($campo_canceled, $contrato_id,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("ssss.CANCELED = 'N' AND ssss.status = 'activo' AND ssss.contrato_id = 1",$result);
        error::$en_error = false;

        $entidad = 'ssss';
        $contrato_id = 1;
        $campo_canceled = '';
        $result = $obj->where_sum($campo_canceled, $contrato_id,$entidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("ssss.status = 'activo' AND ssss.contrato_id = 1",$result);
        error::$en_error = false;

    }

}
