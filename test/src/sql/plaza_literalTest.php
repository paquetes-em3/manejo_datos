<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql\periodo_comercial;
use desarrollo_em3\manejo_datos\sql\plaza_literal;
use PHPUnit\Framework\TestCase;

class plaza_literalTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_get_plaza()
    {
        error::$en_error = false;
        $obj = new plaza_literal();
        //$seguridad = new liberator($seguridad);

        $plaza_id = 1;
        $serie = 'x';
        $result = $obj->get_plaza($plaza_id,$serie);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT *FROM plaza_literal WHERE plaza_id = 1 AND descripcion = 'x'",$result);
        error::$en_error = false;


    }

}
