<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\sala_usuario;
use desarrollo_em3\manejo_datos\sql\usuario;
use PHPUnit\Framework\TestCase;

class usuarioTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_activa_acceso_app()
    {
        error::$en_error = false;
        $obj = new usuario();
        //$obj = new liberator($obj);

        $usuario_id = 1;
        $result = $obj->activa_acceso_app($usuario_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("UPDATE usuario SET permite_acceso_app = 'activo' WHERE id = 1",$result);


        error::$en_error = false;


    }


}
