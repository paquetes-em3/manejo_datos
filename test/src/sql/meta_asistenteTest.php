<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql\meta_asistente;
use PHPUnit\Framework\TestCase;

class meta_asistenteTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_cantidad_periodo()
    {
        error::$en_error = false;
        $obj = new meta_asistente();
        //$seguridad = new liberator($seguridad);

        $ohem_id = 1;
        $periodo_id = 1;
        $result = $obj->cantidad_periodo($ohem_id,$periodo_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT meta_asistente.cantidad FROM meta_asistente WHERE meta_asistente.periodo_comercial_id = 1 AND meta_asistente.ohem_id = 1",$result);
        error::$en_error = false;


    }

}
