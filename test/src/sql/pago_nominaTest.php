<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\ohem_produccion_mensual;
use desarrollo_em3\manejo_datos\sql\pago;
use desarrollo_em3\manejo_datos\sql\pago_nomina;
use PHPUnit\Framework\TestCase;

class pago_nominaTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_campos_sql()
    {
        error::$en_error = false;
        $obj = new pago_nomina();
        //$obj = new liberator($obj);


        $result = $obj->campos_sql();
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("pago_nomina.banco_empresa_id AS pago_nomina_banco_empresa_id",$result->banco_empresa_id);
        $this->assertEquals("pago_nomina.banco_empleado_id AS pago_nomina_banco_empleado_id",$result->banco_empleado_id);
        $this->assertEquals("banco_empresa.descripcion AS banco_empresa_descripcion",$result->banco_empresa_descripcion);
        $this->assertEquals("banco_empleado.descripcion AS banco_empleado_descripcion",$result->banco_empleado_descripcion);

        error::$en_error = false;


    }

    final public function test_join_base()
    {
        error::$en_error = false;
        $obj = new pago_nomina();
        //$obj = new liberator($obj);


        $result = $obj->join_base();
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN cuenta_empleado AS cuenta_empleado ON cuenta_empleado.id = pago_nomina.cuenta_empleado_id",$result);

        error::$en_error = false;


    }


    final public function test_join_replace()
    {
        error::$en_error = false;
        $obj = new pago_nomina();
        //$obj = new liberator($obj);


        $result = $obj->join_replace();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN cuenta_empleado AS cuenta_empleado ON cuenta_empleado.id = pago_nomina.cuenta_empleado_id LEFT JOIN banco AS banco_empresa ON banco_empresa.id = pago_nomina.banco_empresa_id LEFT JOIN banco AS banco_empleado ON banco_empleado.id = pago_nomina.banco_empleado_id",$result);

        error::$en_error = false;


    }



}
