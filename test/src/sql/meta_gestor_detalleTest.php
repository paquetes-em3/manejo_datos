<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\meta_asistente;
use desarrollo_em3\manejo_datos\sql\meta_gestor_detalle;
use PHPUnit\Framework\TestCase;

class meta_gestor_detalleTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_campos_pagos()
    {
        error::$en_error = false;
        $obj = new meta_gestor_detalle();
        $obj = new liberator($obj);
        $entidad_empleado = 'ohem';
        $campo_total = 'DocTotal';
        $result = $obj->campos_pagos($campo_total, $entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("ohem_id,contrato_id, SUM(pago.DocTotal) AS aportado",$result);

        error::$en_error = false;


    }
    final public function test_recalcula_meta_gestor_detalle()
    {
        error::$en_error = false;
        $obj = new meta_gestor_detalle();
        //$seguridad = new liberator($seguridad);
        $campo_total = 'DocTotal';
        $entidad_empleado = 'ohem';
        $campo_fecha_pago = 'DocDate';
        $campo_movimiento = 'U_Movto';
        $result = $obj->recalcula_meta_gestor_detalle($campo_fecha_pago, $campo_movimiento,$campo_total,$entidad_empleado);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("UPDATE meta_gestor_detalle AS mgd LEFT JOIN meta_gestor_concentrado AS mgc ON mgc.id = mgd.meta_gestor_concentrado_id LEFT JOIN ( SELECT ohem_id,contrato_id, SUM(pago.DocTotal) AS aportado FROM pago WHERE pago.DocDate BETWEEN :fecha_inicio AND :fecha_fin AND pago.status = 'activo' AND pago.U_Movto = 'Abono' GROUP BY ohem_id,contrato_id ) AS pago ON mgd.contrato_id = pago.contrato_id AND mgc.ohem_id = pago.ohem_id SET mgd.monto_proceso = GREATEST(0, LEAST(mgd.monto_total, COALESCE(pago.aportado, 0))), mgd.monto_de_mas = GREATEST(0, COALESCE(pago.aportado, 0) - LEAST(mgd.monto_total, COALESCE(pago.aportado, 0))) WHERE mgc.meta_gestor_id = :meta_gestor_id",$result);

        error::$en_error = false;


    }

    final public function test_valida_datos()
    {
        error::$en_error = false;
        $obj = new meta_gestor_detalle();
        //$seguridad = new liberator($seguridad);
        $campo_total = 'A';
        $entidad_empleado = 'B';
        $campo_fecha_pago = 'C';
        $campo_movimiento = 'D';
        $result = $obj->valida_datos($campo_fecha_pago,$campo_movimiento,$campo_total,$entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);



        error::$en_error = false;


    }

    final public function test_where_pago()
    {
        error::$en_error = false;
        $obj = new meta_gestor_detalle();
        $obj = new liberator($obj);

        $campo_fecha_pago = 'DocDate';
        $campo_movimiento = 'U_Movto';
        $result = $obj->where_pago($campo_fecha_pago,$campo_movimiento);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("pago.DocDate BETWEEN :fecha_inicio AND :fecha_fin AND pago.status = 'activo' AND pago.U_Movto = 'Abono'",$result);

        error::$en_error = false;

        $campo_fecha_pago = 'pago.DocDate';
        $campo_movimiento = 'pago.U_Movto';
        $result = $obj->where_pago($campo_fecha_pago,$campo_movimiento);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("pago.DocDate BETWEEN :fecha_inicio AND :fecha_fin AND pago.status = 'activo' AND pago.U_Movto = 'Abono'",$result);

        error::$en_error = false;


    }

}
