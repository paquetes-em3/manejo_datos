<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql\conf_costo_estimado;
use desarrollo_em3\manejo_datos\sql\configuracion_script;
use PDO;
use PHPUnit\Framework\TestCase;

class configuracion_scriptTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }



    final public function test_get_conf()
    {
        error::$en_error = false;
        $obj = new configuracion_script();
        //$obj = new liberator($obj);

        $numero_empresa = 1;
        $script = 'a';
        $result = $obj->get_conf($numero_empresa,$script);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT configuracion_script.en_ejecucion AS configuracion_script_en_ejecucion FROM configuracion_script WHERE configuracion_script.empresa_db = '1' AND configuracion_script.nombre = 'a'",$result);

        error::$en_error = false;


    }



}
