<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql\periodo_comercial;
use PHPUnit\Framework\TestCase;

class periodo_comercialTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_periodo_comercial_hoy()
    {
        error::$en_error = false;
        $obj = new periodo_comercial();
        //$seguridad = new liberator($seguridad);

        $hoy = '2020-01-01';
        $result = $obj->periodo_comercial_hoy($hoy);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT * FROM periodo_comercial WHERE '2020-01-01' BETWEEN fecha_inicial AND fecha_final",$result);
        error::$en_error = false;


    }

}
