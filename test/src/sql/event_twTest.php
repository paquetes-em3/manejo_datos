<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;

use desarrollo_em3\manejo_datos\sql\empleado;
use desarrollo_em3\manejo_datos\sql\event_tw;
use PHPUnit\Framework\TestCase;

class event_twTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_n_eventos()
    {
        error::$en_error = false;
        $obj = new event_tw();
        //$seguridad = new liberator($seguridad);

        $entidad_empleado = 'a';
        $fecha = '';
        $ohem_id = '-1';
        $result = $obj->n_eventos($entidad_empleado,$fecha,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT COUNT(*) AS n_eventos FROM event_tw WHERE a_id = -1 AND fecha BETWEEN '1900-01-01 00:00:00' AND '1900-01-01 23:59:59'",$result);


        error::$en_error = false;


    }



}
