<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql\meta_asistente;
use desarrollo_em3\manejo_datos\sql\meta_gestor_detalle;
use desarrollo_em3\manejo_datos\sql\nota_credito;
use PHPUnit\Framework\TestCase;

class nota_creditoTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_suma_nota_credito()
    {
        error::$en_error = false;
        $obj = new nota_credito();
        //$seguridad = new liberator($seguridad);

        $contrato_id = 1;
        $campo_canceled = 'CANCELED';
        $campo_monto = 'DocTotal';
        $result = $obj->suma_nota_credito($campo_canceled, $campo_monto,$contrato_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);


        $this->assertEquals("SELECT IFNULL(SUM(nota_credito.DocTotal), 0) AS 'suma_nota_credito' FROM nota_credito WHERE nota_credito.CANCELED = 'N' AND nota_credito.status = 'activo' AND nota_credito.contrato_id = 1",$result);

        error::$en_error = false;

        $contrato_id = 1;
        $campo_canceled = '';
        $campo_monto = 'monto';
        $result = $obj->suma_nota_credito($campo_canceled, $campo_monto,$contrato_id);


        $this->assertEquals("SELECT IFNULL(SUM(nota_credito.monto), 0) AS 'suma_nota_credito' FROM nota_credito WHERE nota_credito.status = 'activo' AND nota_credito.contrato_id = 1",$result);
        error::$en_error = false;


    }

}
