<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\sql\contrato;
use desarrollo_em3\manejo_datos\sql\contrato_domiciliado;
use PDO;
use PHPUnit\Framework\TestCase;

class contrato_domiciliadoTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_columnas()
    {
        error::$en_error = false;
        $obj = new contrato_domiciliado();
        $obj = new liberator($obj);

        $result = $obj->columnas();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("contrato_domiciliado.id AS contrato_domiciliado_id,contrato_domiciliado.number_token AS contrato_domiciliado_number_token,contrato_domiciliado.expmonth AS contrato_domiciliado_expmonth,contrato_domiciliado.expyear AS contrato_domiciliado_expyear,contrato_domiciliado.moneda AS contrato_domiciliado_moneda,contrato.id AS contrato_id,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont,contrato.CardCode AS contrato_CardCode,contrato.dv_santander AS contrato_dv_santander,contrato.CardName AS contrato_CardName,contrato.U_ImportePago AS contrato_U_ImportePago,contrato.dia_pago AS contrato_dia_pago,contrato.dia_pago_1 AS contrato_dia_pago_1,contrato.dia_pago_2 AS contrato_dia_pago_2,contrato.DocTotal AS contrato_DocTotal,contrato.PaidToDate AS contrato_PaidToDate,plaza.id AS plaza_id,plaza.descripcion AS plaza_descripcion",$result);

        error::$en_error = false;
    }

    final public function test_contratos_exe_pagos_dom()
    {
        error::$en_error = false;
        $obj = new contrato_domiciliado();
        //$obj = new liberator($obj);

        $dia_pago = 1;
        $periodicidad_pago = 'S';
        $result = $obj->contratos_exe_pagos_dom($dia_pago, $periodicidad_pago);



        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato_domiciliado.id AS contrato_domiciliado_id,contrato_domiciliado.number_token AS contrato_domiciliado_number_token,contrato_domiciliado.expmonth AS contrato_domiciliado_expmonth,contrato_domiciliado.expyear AS contrato_domiciliado_expyear,contrato_domiciliado.moneda AS contrato_domiciliado_moneda,contrato.id AS contrato_id,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont,contrato.CardCode AS contrato_CardCode,contrato.dv_santander AS contrato_dv_santander,contrato.CardName AS contrato_CardName,contrato.U_ImportePago AS contrato_U_ImportePago,contrato.dia_pago AS contrato_dia_pago,contrato.dia_pago_1 AS contrato_dia_pago_1,contrato.dia_pago_2 AS contrato_dia_pago_2,contrato.DocTotal AS contrato_DocTotal,contrato.PaidToDate AS contrato_PaidToDate,plaza.id AS plaza_id,plaza.descripcion AS plaza_descripcion FROM contrato_domiciliado LEFT JOIN contrato ON contrato_domiciliado.contrato_id = contrato.id LEFT JOIN plaza ON contrato.plaza_id = plaza.id WHERE contrato_domiciliado.status = 'activo' AND contrato.U_CondPago = 'S'  AND contrato.pagado = 'inactivo'  AND TRIM(contrato_domiciliado.number_token) <> '' AND (contrato.U_StatusCob = 'ACTIVO' OR contrato.U_StatusCob = 'OBSERVACION' OR contrato.U_StatusCob = 'PRE-CANCELADO' OR contrato.U_StatusCob = 'PROMESA PAGO' OR contrato.U_StatusCob = 'SUSPENSION TEMPORAL') AND contrato.dia_pago = '1' GROUP BY contrato.id ORDER BY plaza.id ASC",$result);

        error::$en_error = false;

        $dia_pago = 1;
        $periodicidad_pago = 'Q';
        $result = $obj->contratos_exe_pagos_dom($dia_pago, $periodicidad_pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato_domiciliado.id AS contrato_domiciliado_id,contrato_domiciliado.number_token AS contrato_domiciliado_number_token,contrato_domiciliado.expmonth AS contrato_domiciliado_expmonth,contrato_domiciliado.expyear AS contrato_domiciliado_expyear,contrato_domiciliado.moneda AS contrato_domiciliado_moneda,contrato.id AS contrato_id,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont,contrato.CardCode AS contrato_CardCode,contrato.dv_santander AS contrato_dv_santander,contrato.CardName AS contrato_CardName,contrato.U_ImportePago AS contrato_U_ImportePago,contrato.dia_pago AS contrato_dia_pago,contrato.dia_pago_1 AS contrato_dia_pago_1,contrato.dia_pago_2 AS contrato_dia_pago_2,contrato.DocTotal AS contrato_DocTotal,contrato.PaidToDate AS contrato_PaidToDate,plaza.id AS plaza_id,plaza.descripcion AS plaza_descripcion FROM contrato_domiciliado LEFT JOIN contrato ON contrato_domiciliado.contrato_id = contrato.id LEFT JOIN plaza ON contrato.plaza_id = plaza.id WHERE contrato_domiciliado.status = 'activo' AND contrato.U_CondPago = 'Q'  AND contrato.pagado = 'inactivo'  AND TRIM(contrato_domiciliado.number_token) <> '' AND (contrato.U_StatusCob = 'ACTIVO' OR contrato.U_StatusCob = 'OBSERVACION' OR contrato.U_StatusCob = 'PRE-CANCELADO' OR contrato.U_StatusCob = 'PROMESA PAGO' OR contrato.U_StatusCob = 'SUSPENSION TEMPORAL') AND (contrato.dia_pago_1 = '1' OR contrato.dia_pago_2 = '1') GROUP BY contrato.id ORDER BY plaza.id ASC",$result);


        error::$en_error = false;

        $dia_pago = 1;
        $periodicidad_pago = 'M';
        $result = $obj->contratos_exe_pagos_dom($dia_pago, $periodicidad_pago);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato_domiciliado.id AS contrato_domiciliado_id,contrato_domiciliado.number_token AS contrato_domiciliado_number_token,contrato_domiciliado.expmonth AS contrato_domiciliado_expmonth,contrato_domiciliado.expyear AS contrato_domiciliado_expyear,contrato_domiciliado.moneda AS contrato_domiciliado_moneda,contrato.id AS contrato_id,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont,contrato.CardCode AS contrato_CardCode,contrato.dv_santander AS contrato_dv_santander,contrato.CardName AS contrato_CardName,contrato.U_ImportePago AS contrato_U_ImportePago,contrato.dia_pago AS contrato_dia_pago,contrato.dia_pago_1 AS contrato_dia_pago_1,contrato.dia_pago_2 AS contrato_dia_pago_2,contrato.DocTotal AS contrato_DocTotal,contrato.PaidToDate AS contrato_PaidToDate,plaza.id AS plaza_id,plaza.descripcion AS plaza_descripcion FROM contrato_domiciliado LEFT JOIN contrato ON contrato_domiciliado.contrato_id = contrato.id LEFT JOIN plaza ON contrato.plaza_id = plaza.id WHERE contrato_domiciliado.status = 'activo' AND contrato.U_CondPago = 'M'  AND contrato.pagado = 'inactivo'  AND TRIM(contrato_domiciliado.number_token) <> '' AND (contrato.U_StatusCob = 'ACTIVO' OR contrato.U_StatusCob = 'OBSERVACION' OR contrato.U_StatusCob = 'PRE-CANCELADO' OR contrato.U_StatusCob = 'PROMESA PAGO' OR contrato.U_StatusCob = 'SUSPENSION TEMPORAL') AND contrato.dia_pago_1 = '1' GROUP BY contrato.id ORDER BY plaza.id ASC",$result);

        error::$en_error = false;


    }

    final public function test_where_base_exe()
    {
        error::$en_error = false;
        $obj = new contrato_domiciliado();
        $obj = new liberator($obj);

        $dia_pago = 16;
        $periodicidad_pago = 'Q';
        $result = $obj->where_base_exe($dia_pago, $periodicidad_pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("contrato_domiciliado.status = 'activo' AND contrato.U_CondPago = 'Q'  AND contrato.pagado = 'inactivo'  AND TRIM(contrato_domiciliado.number_token) <> '' AND (contrato.U_StatusCob = 'ACTIVO' OR contrato.U_StatusCob = 'OBSERVACION' OR contrato.U_StatusCob = 'PRE-CANCELADO' OR contrato.U_StatusCob = 'PROMESA PAGO' OR contrato.U_StatusCob = 'SUSPENSION TEMPORAL') AND (contrato.dia_pago_1 = '16' OR contrato.dia_pago_2 = '16')",$result);

        error::$en_error = false;
    }

    final public function test_where_dia_pago()
    {
        error::$en_error = false;
        $obj = new contrato_domiciliado();
        $obj = new liberator($obj);

        $dia_pago = 8;
        $periodicidad_pago = 'Q';
        $result = $obj->where_dia_pago($dia_pago,$periodicidad_pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("(contrato.dia_pago_1 = '8' OR contrato.dia_pago_2 = '8')",$result);

        error::$en_error = false;
    }

    final public function test_where_general_mit()
    {
        error::$en_error = false;
        $obj = new contrato_domiciliado();
        $obj = new liberator($obj);

        $periodicidad_pago = 'S';
        $result = $obj->where_general_mit($periodicidad_pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("contrato_domiciliado.status = 'activo' AND contrato.U_CondPago = 'S'  AND contrato.pagado = 'inactivo'  AND TRIM(contrato_domiciliado.number_token) <> '' AND (contrato.U_StatusCob = 'ACTIVO' OR contrato.U_StatusCob = 'OBSERVACION' OR contrato.U_StatusCob = 'PRE-CANCELADO' OR contrato.U_StatusCob = 'PROMESA PAGO' OR contrato.U_StatusCob = 'SUSPENSION TEMPORAL')",$result);

        error::$en_error = false;
    }

    final public function test_wr_dia_p_mes()
    {
        error::$en_error = false;
        $obj = new contrato_domiciliado();
        $obj = new liberator($obj);

        $dia_pago = 18;
        $result = $obj->wr_dia_p_mes($dia_pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("contrato.dia_pago_1 = '18'",$result);

        error::$en_error = false;
    }

    final public function test_wr_dia_p_quincena()
    {
        error::$en_error = false;
        $obj = new contrato_domiciliado();
        $obj = new liberator($obj);

        $dia_pago = 30;
        $result = $obj->wr_dia_p_quincena($dia_pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("(contrato.dia_pago_1 = '30' OR contrato.dia_pago_2 = '30')",$result);

        error::$en_error = false;
    }




    final public function test_wr_dia_p_semana()
    {
        error::$en_error = false;
        $obj = new contrato_domiciliado();
        $obj = new liberator($obj);

        $dia_pago = 7;
        $result = $obj->wr_dia_p_semana($dia_pago);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("contrato.dia_pago = '7'",$result);

        error::$en_error = false;
    }

}
