<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;

use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\sql\empleado;
use desarrollo_em3\manejo_datos\sql\esquema;
use desarrollo_em3\manejo_datos\sql\esquema_guardadito;
use desarrollo_em3\manejo_datos\sql\esquema_producto_comision;
use PDO;
use PHPUnit\Framework\TestCase;

class esquema_producto_comisionTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_where_fecha_comision()
    {
        error::$en_error = false;
        $obj = new esquema_producto_comision();
        //$obj = new liberator($obj);

        $fecha_contrato = '2020-01-01';
        $result = $obj->where_fecha_comision($fecha_contrato);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals(" AND '2020-01-01' >= esquema_producto_comision.fecha_inicio AND '2020-01-01' <= esquema_producto_comision.fecha_fin ",$result);


        error::$en_error = false;


    }




}
