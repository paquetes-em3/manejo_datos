<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\reporte_query;
use PHPUnit\Framework\TestCase;

class reporte_queryTest extends TestCase {

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
    }

    final public function test_obten_id_permisos_departamento() {

        error::$en_error = false;
        $obj = new Reporte_query();
        //$obj = new liberator($obj);¿

        $usuario_id = 2;
        $result = $obj->obten_id_permisos_departamento($usuario_id);
        
        $this->assertFalse(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT IFNULL(GROUP_CONCAT(acl_usuario_departamento.departamento_id),0) AS departamentos FROM acl_usuario_departamento WHERE acl_usuario_departamento.usuario_id = 2", $result);

        error::$en_error = false;

    }

    final public function test_obten_id_permisos_plaza() {

        error::$en_error = false;
        $obj = new Reporte_query();
        //$obj = new liberator($obj);¿

        $usuario_id = 2;
        $result = $obj->obten_id_permisos_plaza($usuario_id);
        
        $this->assertFalse(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT IFNULL(GROUP_CONCAT(acl_usuario_plaza.plaza_id),'0') AS plazas FROM acl_usuario_plaza WHERE acl_usuario_plaza.usuario_id = 2", $result);

        error::$en_error = false;

    }
}