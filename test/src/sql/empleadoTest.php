<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\sql\empleado;
use PDO;
use PHPUnit\Framework\TestCase;

class empleadoTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_sql_extra_fecha_inicio()
    {
        error::$en_error = false;
        $obj = new empleado();
        //$seguridad = new liberator($seguridad);

        $campo_fecha = 'startDate';
        $fecha_inicio_menor_a = '';
        $tabla = 'ohem';
        $result = $obj->sql_extra_fecha_inicio($campo_fecha,$fecha_inicio_menor_a,$tabla);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("",$result);
        error::$en_error = false;

        $campo_fecha = 'fecha_ingreso';
        $fecha_inicio_menor_a = '2020-01-01';
        $tabla = 'empleado';
        $result = $obj->sql_extra_fecha_inicio($campo_fecha,$fecha_inicio_menor_a,$tabla);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals(" AND empleado.fecha_ingreso <= '2020-01-01'",$result);
        error::$en_error = false;


    }

    final public function test_sql_plazas_in()
    {
        error::$en_error = false;
        $obj = new empleado();
        //$seguridad = new liberator($seguridad);


        $plazas_in = array();
        $result = $obj->sql_plazas_in($plazas_in);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("",$result);
        error::$en_error = false;
        $plazas_in = array();
        $plazas_in['x'] = '1';
        $plazas_in['y'] = '2';
        $plazas_in['xx'] = '66';
        $result = $obj->sql_plazas_in($plazas_in);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals(" IN( 1,2,66 )",$result);

        error::$en_error = false;


    }

    final public function test_get_empleado_datatable() {

        error::$en_error = false;
        $obj = new empleado();

        $conexion = (new conexion_db_monterrey());
        $this->link = $conexion->link;

        $entidad_empleado = 'ohem';
        $limit = 10;
        $offset = 0;
        $order_by = '';
        $plaza_id = '28';
        $status = 'activo';
        $wh_like = '';
        $result = $obj->get_empleado_datatable($entidad_empleado,$plaza_id,$status,$limit,$offset ,$order_by
        ,$wh_like);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("SELECT ohem.id AS ohem_id,ohem.nombre_completo AS ohem_nombre_completo,ohem.status AS ohem_status,plaza.descripcion AS plaza_descripcion FROM ohem LEFT JOIN plaza AS plaza ON ohem.plaza_id = plaza.id WHERE  plaza.id IN(28) AND ohem.status = 'activo' LIMIT 10",$result);
        error::$en_error = false;

        $entidad_empleado = 'ohem';
        $limit = 10;
        $offset = 0;
        $order_by = 'ohem_nombre_completo ASC';
        $plaza_id = '28';
        $status = 'activo';
        $wh_like = 'abigail';
        $result = $obj->get_empleado_datatable($entidad_empleado,$plaza_id,$status,$limit,$offset ,$order_by,$wh_like);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("SELECT ohem.id AS ohem_id,ohem.nombre_completo AS ohem_nombre_completo,ohem.status AS ohem_status,plaza.descripcion AS plaza_descripcion FROM ohem LEFT JOIN plaza AS plaza ON ohem.plaza_id = plaza.id WHERE  plaza.id IN(28) AND ohem.status = 'activo' AND (ohem.id LIKE '%abigail%' OR ohem.nombre_completo LIKE '%abigail%' OR plaza.descripcion LIKE '%abigail%' OR ohem.status LIKE '%abigail%')  ORDER BY ohem_nombre_completo ASC LIMIT 10", $result);
        
        error::$en_error = false;
        
    }
}
