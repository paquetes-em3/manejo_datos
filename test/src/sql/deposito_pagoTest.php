<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\sql\deposito_aportaciones;
use desarrollo_em3\manejo_datos\sql\deposito_pago;
use desarrollo_em3\manejo_datos\sql\ohem_produccion_mensual;
use desarrollo_em3\manejo_datos\sql\pago;
use PHPUnit\Framework\TestCase;

class deposito_pagoTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }


    final public function test_depositos_por_aport_filtro()
    {
        error::$en_error = false;
        $obj = new deposito_pago();
        //$obj = new liberator($obj);

        $deposito_aportaciones_id = 1;
        $result = $obj->depositos_por_aport_filtro($deposito_aportaciones_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("deposito_pago.deposito_aportaciones_id = 1 AND deposito_pago.status = 'activo'",$result);

        error::$en_error = false;


    }

    final public function test_existe_deposito_pago()
    {
        error::$en_error = false;
        $obj = new deposito_pago();
        //$obj = new liberator($obj);

        $pago_id = 1;
        $result = $obj->existe_deposito_pago($pago_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT COUNT(*) AS n_registros FROM deposito_pago WHERE deposito_pago.pago_id = 1",$result);

        error::$en_error = false;


    }


    final public function test_key_deposito_ap_id()
    {
        error::$en_error = false;
        $obj = new deposito_pago();
        $obj = new liberator($obj);

        $pago_corte_id = 1;
        $result = $obj->key_deposito_ap_id($pago_corte_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("deposito_pago.deposito_aportaciones_id = 1",$result);

        error::$en_error = false;


    }



}
