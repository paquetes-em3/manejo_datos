<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;

use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\sql\empleado;
use desarrollo_em3\manejo_datos\sql\empresa;
use desarrollo_em3\manejo_datos\sql\esquema;
use PDO;
use PHPUnit\Framework\TestCase;

class empresaTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_empresa_324()
    {
        error::$en_error = false;
        $obj = new empresa();
        //$seguridad = new liberator($seguridad);
        $result = $obj->empresa_324();
        //print_r($result);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("SELECT * FROM empresa WHERE empresa.es_324 = 'activo'",$result);

        error::$en_error = false;


    }




}
