<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;

use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\sql\empleado;
use desarrollo_em3\manejo_datos\sql\esquema;
use PDO;
use PHPUnit\Framework\TestCase;

class esquemaTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_campos_sql()
    {
        error::$en_error = false;
        $obj = new esquema();
        //$seguridad = new liberator($seguridad);
        $result = $obj->campos_sql($this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals("esquema.id AS esquema_id,esquema.Code AS esquema_Code,esquema.status AS esquema_status,esquema.usuario_alta_id AS esquema_usuario_alta_id,esquema.usuario_update_id AS esquema_usuario_update_id,esquema.fecha_alta AS esquema_fecha_alta,esquema.fecha_update AS esquema_fecha_update,esquema.Name AS esquema_Name,esquema.U_CodPuesto AS esquema_U_CodPuesto,esquema.U_DescPuesto AS esquema_U_DescPuesto,esquema.U_Activo AS esquema_U_Activo,esquema.U_InvIniMin AS esquema_U_InvIniMin,esquema.U_Papeleria AS esquema_U_Papeleria,esquema.puesto_id AS esquema_puesto_id,esquema.aplica_deduccion AS esquema_aplica_deduccion,esquema.monto_deduccion AS esquema_monto_deduccion,esquema.monto_minimo_contrato AS esquema_monto_minimo_contrato,esquema.aplica_asistente AS esquema_aplica_asistente,esquema.aplica_coordinador AS esquema_aplica_coordinador,esquema.aplica_lider AS esquema_aplica_lider,esquema.aplica_gerente AS esquema_aplica_gerente,esquema.aplica_gerente_plaza AS esquema_aplica_gerente_plaza,esquema.aplica_gerente_regional AS esquema_aplica_gerente_regional,esquema.aplica_ascenso AS esquema_aplica_ascenso,esquema.ascenso_por_produccion AS esquema_ascenso_por_produccion,esquema.aplica_descenso AS esquema_aplica_descenso,esquema.descenso_por_produccion AS esquema_descenso_por_produccion,esquema.aplica_baja AS esquema_aplica_baja,esquema.esquema_ascenso_id AS esquema_esquema_ascenso_id,esquema.esquema_descenso_id AS esquema_esquema_descenso_id,esquema.n_dias_ascenso AS esquema_n_dias_ascenso,esquema.n_dias_descenso AS esquema_n_dias_descenso,esquema.n_dias_baja AS esquema_n_dias_baja,esquema.descuento_comision_primer_venta AS esquema_descuento_comision_primer_venta,esquema.descuento_dinero_rapido AS esquema_descuento_dinero_rapido,esquema.n_dias_garantia AS esquema_n_dias_garantia,esquema.aplica_dias_garantia AS esquema_aplica_dias_garantia,esquema.aplica_ayudas_garantia AS esquema_aplica_ayudas_garantia,esquema.ayudas_garantia_personales AS esquema_ayudas_garantia_personales,esquema.ayudas_garantia_equipo AS esquema_ayudas_garantia_equipo,esquema.n_ayudas_garantia AS esquema_n_ayudas_garantia,esquema.aplica_para_alta AS esquema_aplica_para_alta,esquema.aplica_para_reingreso AS esquema_aplica_para_reingreso,esquema.clausula AS esquema_clausula,esquema.comentarios AS esquema_comentarios,esquema.salario AS esquema_salario,esquema.gasolina AS esquema_gasolina,esquema.comision_teorica AS esquema_comision_teorica,esquema.comision_equipo AS esquema_comision_equipo,esquema.monto_anticipo_venta_personal AS esquema_monto_anticipo_venta_personal,esquema.monto_anticipo_venta_equipo AS esquema_monto_anticipo_venta_equipo,esquema.comision_alterna AS esquema_comision_alterna,esquema.orden_alterno AS esquema_orden_alterno,esquema.dias_jefe_alterno AS esquema_dias_jefe_alterno,esquema.tipo_oficina_id AS esquema_tipo_oficina_id,esquema.modifica_sueldo_ohem AS esquema_modifica_sueldo_ohem,esquema.modifica_gasolina AS esquema_modifica_gasolina,esquema.aplica_faltas AS esquema_aplica_faltas,esquema.aplica_complemento_comision AS esquema_aplica_complemento_comision,esquema.es_adjunto AS esquema_es_adjunto,esquema.monto_reincorporacion_personal AS esquema_monto_reincorporacion_personal,esquema.monto_reincorporacion_equipo AS esquema_monto_reincorporacion_equipo,esquema.resta_comision_vendedor AS esquema_resta_comision_vendedor,esquema.divide_comision_vendedor_nueva AS esquema_divide_comision_vendedor_nueva,esquema.divide_comision_vendedor_reinc AS esquema_divide_comision_vendedor_reinc,esquema.orden_cadena AS esquema_orden_cadena,esquema.monto_fondo AS esquema_monto_fondo,esquema.aplica_comision_equipo AS esquema_aplica_comision_equipo,esquema.aplica_bono_cambio_esquema AS esquema_aplica_bono_cambio_esquema,esquema.activa_cadena_telefonista AS esquema_activa_cadena_telefonista,esquema.n_abonos AS esquema_n_abonos,esquema.monto_por_n_abonos AS esquema_monto_por_n_abonos,esquema.aplica_meta AS esquema_aplica_meta,esquema.aplica_complemento_personal AS esquema_aplica_complemento_personal,esquema.aplica_complemento_equipo AS esquema_aplica_complemento_equipo,esquema.valida_sin_presencia AS esquema_valida_sin_presencia,esquema.es_reclutador AS esquema_es_reclutador",$result);

        error::$en_error = false;


    }

    final public function test_sql_extra_esquema_guardadito()
    {
        error::$en_error = false;
        $obj = new esquema();
        //$seguridad = new liberator($seguridad);


        $fecha = '1';
        $result = $obj->sql_extra_esquema_guardadito($fecha);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals(" AND '1' BETWEEN esquema_guardadito.fecha_inicio AND esquema_guardadito.fecha_fin",$result);
        error::$en_error = false;



    }
    final public function test_sql_plazas_in()
    {
        error::$en_error = false;
        $obj = new empleado();
        //$seguridad = new liberator($seguridad);


        $plazas_in = array();
        $result = $obj->sql_plazas_in($plazas_in);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("",$result);
        error::$en_error = false;
        $plazas_in = array();
        $plazas_in['x'] = '1';
        $plazas_in['y'] = '2';
        $plazas_in['xx'] = '66';
        $result = $obj->sql_plazas_in($plazas_in);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals(" IN( 1,2,66 )",$result);

        error::$en_error = false;


    }


}
