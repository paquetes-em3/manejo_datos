<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use PHPUnit\Framework\TestCase;

class contrato_comisionTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_contrato_doble()
    {
        error::$en_error = false;
        $obj = new contrato_comision();
        //$seguridad = new liberator($seguridad);

        $fecha_final = '2020-01-01';
        $fecha_inicial = '1900-01-01';
        $ohem_id = 1;
        $result = $obj->contrato_doble($fecha_final,$fecha_inicial,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT SUM(IF(producto.descripcion LIKE '%PLAN DOBLE%', 2, 1) ) AS 'cantidad'  FROM contrato_comision INNER JOIN contrato ON contrato.id = contrato_comision.contrato_id AND contrato.DocDate IS NOT NULL LEFT JOIN producto ON contrato.producto_id = producto.id WHERE contrato_comision.ohem_id = 1 AND contrato.DocDate BETWEEN '1900-01-01' AND '2020-01-01'",$result);
        error::$en_error = false;


    }

}
