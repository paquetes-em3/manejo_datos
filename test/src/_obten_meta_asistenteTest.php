<?php
namespace desarrollo_em3\test\clases;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\_obten_meta_asistente;
use PHPUnit\Framework\TestCase;

class _obten_meta_asistenteTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_sqls()
    {
        error::$en_error = false;
        $obj = new _obten_meta_asistente();
        //$seguridad = new liberator($seguridad);

        $fecha_final = '2020-01-01';
        $fecha_inicial = '2020-01-01';
        $ohem_id = 1;
        $periodo_id = 1;
        $result = $obj->sqls($fecha_final, $fecha_inicial,$ohem_id,$periodo_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('SELECT meta_asistente.cantidad FROM meta_asistente WHERE meta_asistente.periodo_comercial_id = 1 AND meta_asistente.ohem_id = 1',$result->sql_meta_cantidad);
        $this->assertEquals("SELECT SUM(IF(producto.descripcion LIKE '%PLAN DOBLE%', 2, 1) ) AS 'cantidad'  FROM contrato_comision INNER JOIN contrato ON contrato.id = contrato_comision.contrato_id AND contrato.DocDate IS NOT NULL LEFT JOIN producto ON contrato.producto_id = producto.id WHERE contrato_comision.ohem_id = 1 AND contrato.DocDate BETWEEN '2020-01-01' AND '2020-01-01'",$result->sql_plan_doble);
        $this->assertEquals("SELECT IFNULL(n_ventas_personales,0) AS 'n_ventas_personales', produccion.puesto_id AS puesto_id FROM ohem_produccion_mensual AS produccion INNER JOIN oficina ON produccion.oficina_id = oficina.id INNER JOIN tipo_oficina ON tipo_oficina.id = oficina.tipo_oficina_id WHERE produccion.periodo_comercial_id = (SELECT periodo_comercial.id FROM periodo_comercial WHERE periodo_comercial.fecha_final < (SELECT periodo_comercial.fecha_inicial FROM periodo_comercial WHERE CURDATE() BETWEEN periodo_comercial.fecha_inicial AND periodo_comercial.fecha_final) ORDER BY periodo_comercial.fecha_final DESC LIMIT 1) AND produccion.ohem_id = 1 AND tipo_oficina.aplica_pabs = 'activo'",$result->sql_n_ventas_personales);
        error::$en_error = false;


    }

}
