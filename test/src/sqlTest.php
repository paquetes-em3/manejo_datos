<?php
namespace desarrollo_em3\test\clases;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\sql;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class sqlTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;

    }

    final public function test_and()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $sql = '';
        $result = $obj->and($sql);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('',$result);
        error::$en_error = false;

        $sql = 'x';
        $result = $obj->and($sql);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals(' AND ',$result);
        error::$en_error = false;

    }
    final public function test_campo()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $entidad = 'a';
        $name_campo = 'b';
        $result = $obj->campo($entidad,$name_campo);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('a.b AS a_b',$result);
        error::$en_error = false;


    }
    final public function test_campo_base_obj()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $campos = new stdClass();
        $entidad = 'a';
        $name_campo = 'v';

        $result = $obj->campo_base_obj($campos,$entidad,$name_campo);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("a",$result->a_v->entidad);
        $this->assertEquals("v",$result->a_v->name_campo);
        error::$en_error = false;


        error::$en_error = false;


    }
    final public function test_campo_nulo_0()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $campo = 'a';
        $entidad = 'b';
        $result = $obj->campo_nulo_0($campo,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("COALESCE(b.a, 0)",$result);
        error::$en_error = false;


        error::$en_error = false;


    }
    final public function test_campo_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $campo = 'a.l';
        $result = $obj->campo_sql($campo);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('a.l AS a_l',$result);
        error::$en_error = false;


    }
    final public function test_campo_sum()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $entidad = 'v';
        $campo = 'd';
        $result = $obj->campo_sum($campo,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SUM(v.d)",$result);
        error::$en_error = false;



    }
    final public function test_campo_sum_base()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $campo = 'a';
        $rename = '';
        $result = $obj->campo_sum_base($campo,$rename);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('SUM(a) AS a_suma',$result);
        error::$en_error = false;


    }
    final public function test_campo_sum_null()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $entidad = 'xx';
        $campo = 'dd';
        $result = $obj->campo_sum_null($campo,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("IFNULL(SUM(xx.dd), 0)",$result);
        error::$en_error = false;



    }
    final public function test_campo_sum_rename()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $entidad = 'd';
        $campo = 'x';
        $alias = 'dss';
        $result = $obj->campo_sum_rename($alias,$campo,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("IFNULL(SUM(d.x), 0) AS 'dss'",$result);
        error::$en_error = false;



    }

    final public function test_campos_alias()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $campos = array();
        $campos['xxx'] = array();
        $campos['xxx'][] = 'a';
        $result = $obj->campos_alias($campos);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("xxx.a AS xxx_a",$result);
        error::$en_error = false;



    }
    final public function test_campos_extra_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $campos_extra = array();
        $campos_extra[] = 'dd';
        $campos_extra[] = 'pp.ff';

        $result = $obj->campos_extra_sql($campos_extra);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals(',dd AS dd,pp.ff AS pp_ff',$result);
        error::$en_error = false;


    }
    final public function test_campos_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $campos = new stdClass();
        $campos->a = new stdClass();
        $campos->a->entidad = 'x';
        $campos->a->name_campo = 's';

        $campos->b = new stdClass();
        $campos->b->entidad = 'x';
        $campos->b->name_campo = 's';

        $result = $obj->campos_sql($campos);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("x.s AS x_s",$result->a);
        $this->assertEquals("x.s AS x_s",$result->b);
        error::$en_error = false;


    }
    final public function test_campos_sql_base()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $campos_extra = array();
        $campos_extra[] = 'rr.ee';
        $campos_extra[] = 'rr.zz';
        $result = $obj->campos_sql_base($campos_extra);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('rr.ee AS rr_ee,rr.zz AS rr_zz',$result);
        error::$en_error = false;


    }
    final public function test_campos_sql_coma()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $campos = new stdClass();
        $name_entidad = 'p';

        $campos->a = new stdClass();
        $campos->a->rename = 'x';

        $result = $obj->campos_sql_coma($campos,$name_entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("p.a AS x",$result);
        error::$en_error = false;


        error::$en_error = false;


    }
    final public function test_campos_sql_coma_bruto()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $campos_rs = new stdClass();
        $campos_rs->a = 'xxx';
        $campos_rs->b = 'rrr';
        $campos_rs->c = 'fff';

        $result = $obj->campos_sql_coma_bruto($campos_rs);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("xxx,rrr,fff",$result);
        error::$en_error = false;


        error::$en_error = false;


    }
    final public function test_campos_sql_puros()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $campos = array();
        $campos[] = 'a';
        $campos[] = 'b';
        $result = $obj->campos_sql_puros($campos);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('a,b',$result);
        error::$en_error = false;


    }
    final public function test_campos_sql_puros_with_as()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $columnas = array();
        $columnas[] = 's';
        $columnas[] = 'd';
        $result = $obj->campos_sql_puros_with_as($columnas);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("s AS s,d AS d",$result);
        error::$en_error = false;


    }
    final public function test_campos_sql_string()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $entidad = 'zx';
        $campos = array();
        $campos[] = 'x';
        $campos[] = 'y';
        $result = $obj->campos_sql_string($campos,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('zx.x AS zx_x,zx.y AS zx_y',$result);
        error::$en_error = false;


    }
    final public function test_campos_sql_string_multi_table()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $columnas = new stdClass();
        $columnas->a = new stdClass();
        $columnas->a->name_entidad = 'a';
        $columnas->a->name_campo = 'v';

        $columnas->b = new stdClass();
        $columnas->b->name_entidad = 'x';
        $columnas->b->name_campo = 'r';

        $result = $obj->campos_sql_string_multi_table($columnas);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a.v AS a_v,x.r AS x_r",$result);
        error::$en_error = false;

    }
    final public function test_columna_compare()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);


        $replaces_obj = new stdClass();
        $replaces_obj->columna = 'x';
        $result = $obj->columna_compare($replaces_obj);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('x',$result);
        error::$en_error = false;


    }
    final public function test_columna_rs()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);


        $replaces_obj = new stdClass();
        $replaces_obj->columna_replace = 'x';
        $result = $obj->columna_rs($replaces_obj);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('x',$result);
        error::$en_error = false;


    }
    final public function test_columnas_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $entidad = 'a';
        $columnas = new stdClass();
        $result = $obj->columnas_sql($columnas,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("*",$result);
        error::$en_error = false;

        $entidad = 'a';
        $columnas = new stdClass();
        $columnas->a = new stdClass();
        $columnas->a->atributo = 'x';
        $result = $obj->columnas_sql($columnas,$entidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a.x AS x",$result);

        error::$en_error = false;

        $entidad = 'a';
        $columnas = new stdClass();
        $columnas->a = new stdClass();
        $columnas->a->atributo = 'x';

        $columnas->b = new stdClass();
        $columnas->b->atributo = 'y';
        $result = $obj->columnas_sql($columnas,$entidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a.x AS x,a.y AS y",$result);
        error::$en_error = false;

    }
    final public function test_coma()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $sql = '';
        $result = $obj->coma($sql);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('',$result);
        error::$en_error = false;

        $sql = 'xx';
        $result = $obj->coma($sql);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals(',',$result);
        error::$en_error = false;

    }
    final public function test_consulta_columna_replace()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $replaces = new stdClass();
        $consulta = 'x';
        $replaces->columna = 'x';
        $replaces->columna_replace = 't';
        $result = $obj->consulta_columna_replace($consulta,$replaces);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("t",$result);
        error::$en_error = false;



    }
    final public function test_consulta_join_replace()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $replaces = new stdClass();
        $consulta = 'jcjr';
        $replaces->join = 'jc';
        $replaces->join_replace = 'join_replace';
        $result = $obj->consulta_join_replace($consulta,$replaces);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("join_replacejr",$result);
        error::$en_error = false;



    }
    final public function test_consulta_sobreescrita()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $replaces = new stdClass();
        $consulta = 'a';
        $replaces->columna = '';
        $replaces->columna_replace = '';
        $result = $obj->consulta_sobreescrita($consulta,$replaces);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a",$result);
        error::$en_error = false;

        $replaces = new stdClass();
        $consulta = 'a';
        $replaces->columna = 'a';
        $replaces->columna_replace = '';
        $result = $obj->consulta_sobreescrita($consulta,$replaces);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("",$result);

        error::$en_error = false;
        $replaces = new stdClass();
        $consulta = 'a';
        $replaces->columna = 'a';
        $replaces->columna_replace = 'a_r';
        $result = $obj->consulta_sobreescrita($consulta,$replaces);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a_r",$result);

        error::$en_error = false;
        $replaces = new stdClass();
        $consulta = 'a';
        $replaces->columna = 'a';
        $replaces->columna_replace = 'a_r';
        $replaces->join = '';
        $result = $obj->consulta_sobreescrita($consulta,$replaces);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a_r",$result);

        error::$en_error = false;
        $replaces = new stdClass();
        $consulta = 'a';
        $replaces->columna = 'a';
        $replaces->columna_replace = 'a_r';
        $replaces->join = 'b';
        $result = $obj->consulta_sobreescrita($consulta,$replaces);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a_r",$result);

        error::$en_error = false;
        $replaces = new stdClass();
        $consulta = 'ab';
        $replaces->columna = 'a';
        $replaces->columna_replace = 'a_r';
        $replaces->join = 'b';
        $result = $obj->consulta_sobreescrita($consulta,$replaces);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a_r",$result);

        error::$en_error = false;
        $replaces = new stdClass();
        $consulta = 'ab';
        $replaces->columna = 'a';
        $replaces->columna_replace = 'a_r';
        $replaces->join = 'b';
        $replaces->join_replace = 'c';
        $result = $obj->consulta_sobreescrita($consulta,$replaces);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a_rc",$result);

        error::$en_error = false;


    }
    final public function test_describe()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $entidad = 'x';
        $result = $obj->describe($entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("DESCRIBE x",$result);
        error::$en_error = false;

    }
    final public function test_entidad_base_as()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $entidad = 'ss';
        $rename = '';
        $result = $obj->entidad_base_as($entidad,$rename);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("ss AS ss",$result);
        error::$en_error = false;

    }

    final public function test_existe_by_id()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $entidad = 'a';
        $registro_id = 110;
        $result = $obj->existe_by_id($entidad,$registro_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT COUNT(*) AS n_rows FROM a WHERE id = 110",$result);
        error::$en_error = false;

    }

    final public function test_filtro_especial_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $filtro_especial = array();
        $filtro_especial['tabla.campo']['valor'] = 'a';
        $filtro_especial['tabla.campo']['operador'] = '=';

        $filtro_especial['tabla.campo2']['valor'] = 'b';
        $filtro_especial['tabla.campo2']['operador'] = '=';
        $result = $obj->filtro_especial_sql($filtro_especial);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("tabla.campo='a' AND tabla.campo2='b'",$result);
        error::$en_error = false;

        $filtro_especial = array();
        $filtro_especial['tabla.campo']['valor'] = 'a';
        $filtro_especial['tabla.campo']['operador'] = '=';
        $filtro_especial['tabla.campo']['valor_es_campo'] = true;

        $filtro_especial['tabla.campo2']['valor'] = 'b';
        $filtro_especial['tabla.campo2']['operador'] = '=';
        $result = $obj->filtro_especial_sql($filtro_especial);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("'tabla.campo'=a AND tabla.campo2='b'",$result);
        error::$en_error = false;

    }

    final public function test_filtro_rango_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $filtro_rango = array();

        $filtro_rango[0]['valor1'] = 'v1';
        $filtro_rango[0]['valor2'] = 'v2';

        $filtro_rango['x']['valor1'] = 'xv1';
        $filtro_rango['x']['valor2'] = 'xv2';
        $result = $obj->filtro_rango_sql($filtro_rango);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("0 BETWEEN 'v1' AND 'v2' AND x BETWEEN 'xv1' AND 'xv2'",$result);
        error::$en_error = false;


    }
    final public function test_genera_and()
    {
        error::$en_error = false;
        $obj = new sql();
        //$seguridad = new liberator($seguridad);


        $result = $obj->genera_and();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('',$result);
        error::$en_error = false;

        $result = $obj->genera_and();
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('',$result);

        error::$en_error = false;

        $filtro = array();
        $filtro['a'] = '';
        $result = $obj->genera_and($filtro);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a = ''",$result);

        error::$en_error = false;

        $filtro = array();
        $filtro['a'] = '';
        $filtro['b'] = '';
        $result = $obj->genera_and($filtro);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a = '' AND b = ''",$result);
        error::$en_error = false;

    }
    final public function test_genera_and_textos()
    {
        error::$en_error = false;
        $modelo = new sql();
        //$modelo = new liberator($modelo);

        $filtros = array();
        $result = $modelo->genera_and_textos($filtros);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('',$result);
        error::$en_error = false;

        $filtro = array();
        $result = $modelo->genera_and_textos($filtro);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('',$result);

        error::$en_error = false;

        $filtro = array();
        $filtro['a'] = '';
        $result = $modelo->genera_and_textos( $filtro);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a LIKE '%%'",$result);

        error::$en_error = false;

        $filtro = array();
        $filtro['a'] = '';
        $filtro['b'] = '';
        $result = $modelo->genera_and_textos($filtro);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a LIKE '%%' AND b LIKE '%%'",$result);
        error::$en_error = false;

    }
    final public function test_genera_join_replace()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);



        $params = array();
        $params[0] = new stdClass();
        $params[0]->entidad = 'x';
        $params[0]->entidad_right = 'x';

        $result = $obj->genera_join_replace($params);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN x AS x ON x.id = x.x_id",$result);
        error::$en_error = false;

        $params = array();
        $params[0] = new stdClass();
        $params[0]->entidad = 'x';
        $params[0]->entidad_right = 'x';

        $params[1] = new stdClass();
        $params[1]->entidad = 'x';
        $params[1]->entidad_right = 'x';

        $result = $obj->genera_join_replace($params);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN x AS x ON x.id = x.x_id LEFT JOIN x AS x ON x.id = x.x_id",$result);
        error::$en_error = false;



    }

    final public function test_get_data_datatable()
    {
        $obj = new sql();
        //$obj = new liberator($obj);

        $campos_base = array('contrato.id','contrato.CardName');
        $campos_like = array('contrato.id','contrato.CardName');
        $entidad = 'contrato';
        $campos_extra = array('CONCAT(contrato.U_SeCont,contrato.U_FolioCont) AS contrato');
        $join = array('LEFT JOIN plaza AS plaza ON plaza.id = contrato.plaza_id',
        'LEFT JOIN cliente AS cliente ON cliente.id = contrato.cliente_id');
        $limit = 0;
        $offset = 0;
        $order_by = 'contrato.id DESC';
        $condicion = array('contrato.status' => 'activo');
        $filtro_rango = array();
        $campos_like = array('contrato.id','contrato.CardName');
        $condicion_like = '';
        
        $result = $obj->get_data_datatable($campos_base,$campos_like,$entidad,$campos_extra,$condicion,
        $condicion_like,$filtro_rango,$join,$limit,$offset,$order_by);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT contrato.id AS contrato_id,contrato.CardName AS contrato_CardName,CONCAT(contrato.U_SeCont,contrato.U_FolioCont) AS contrato FROM contrato LEFT JOIN plaza AS plaza ON plaza.id = contrato.plaza_id LEFT JOIN cliente AS cliente ON cliente.id = contrato.cliente_id  WHERE contrato.status = 'activo' ORDER BY contrato.id DESC",$result);
        error::$en_error = false;



    }
    
    final public function test_in_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $in_sql = '';
        $value_in = 'a';

        $result = $obj->in_sql($in_sql,$value_in);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a",$result);
        error::$en_error = false;

        $in_sql = 'v';
        $value_in = 'a';

        $result = $obj->in_sql($in_sql,$value_in);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("v,a",$result);

        error::$en_error = false;


    }
    final public function test_in_sql_base()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);



        $values = array();
        $values[] = 'a';
        $values[] = 'b';
        $values[] = 'c';

        $result = $obj->in_sql_base($values);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a,b,c",$result);
        error::$en_error = false;



    }

    final public function test_in_sql_completo()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $campo = 'a';
        $con_and = false;
        $values = array();

        $result = $obj->in_sql_completo($campo,$con_and,$values);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("",$result);
        error::$en_error = false;

        $campo = 'a';
        $con_and = false;
        $values = array();
        $values[] = 'x';
        $values[] = 'y';
        $values[] = 'y';

        $result = $obj->in_sql_completo($campo,$con_and,$values);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a IN (x,y,y)",$result);
        error::$en_error = false;
        $campo = 'a';
        $con_and = true;
        $values = array();
        $values[] = 'x';
        $values[] = 'y';
        $values[] = 'y';

        $result = $obj->in_sql_completo($campo,$con_and,$values);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("AND a IN (x,y,y)",$result);

        error::$en_error = false;
        $campo = 'a';
        $con_and = true;
        $values = array();

        $result = $obj->in_sql_completo($campo,$con_and,$values);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("",$result);

        error::$en_error = false;


    }
    final public function test_init_columnas_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $entidad = 'entidad';
        $columnas = new stdClass();
        $result = $obj->init_columnas_sql($columnas,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("",$result);
        error::$en_error = false;

        $entidad = 'entidad';
        $columnas = new stdClass();
        $columnas->a = new stdClass();
        $columnas->a->atributo = 'attr1';

        $columnas->b = new stdClass();
        $columnas->b->atributo = 'attr2';
        $result = $obj->init_columnas_sql($columnas,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("entidad.attr1 AS attr1,entidad.attr2 AS attr2",$result);
        error::$en_error = false;

    }
    final public function test_init_txt_array()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $data = array();
        $key_compare = 'a';
        $data['a'] = 'xx';
        $result = $obj->init_txt_array($data,$key_compare);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("xx",$result);
        error::$en_error = false;


    }
    final public function test_init_values_array()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $data = array();
        $keys_compare = array();
        $keys_compare[] = 'a';
        $result = $obj->init_values_array($data,$keys_compare);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("",$result->a);
        error::$en_error = false;


    }
    final public function test_integra_columnas_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $columnas_sql = 'xx';
        $column = new stdClass();
        $column->atributo = 'attr';
        $entidad = 'entidad';
        $result = $obj->integra_columnas_sql($column, $columnas_sql,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("xx,entidad.attr AS attr",$result);
        error::$en_error = false;


    }

    final public function test_integra_in_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $campo = 's';
        $con_and = false;
        $in_sql = 'q';

        $result = $obj->integra_in_sql($campo,$con_and,$in_sql);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("s IN (q)",$result);
        error::$en_error = false;

        $campo = 's';
        $con_and = true;
        $in_sql = 'q';

        $result = $obj->integra_in_sql($campo,$con_and,$in_sql);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("AND s IN (q)",$result);

        error::$en_error = false;


    }
    final public function test_integra_join()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);



        $param = new stdClass();
        $param->entidad = 'c';
        $param->entidad_right = 'd';

        $result = $obj->integra_join($param);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("c AS c ON c.id = d.c_id",$result);
        error::$en_error = false;




    }
    final public function test_integra_key_in()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $campo = 'b';
        $con_and = false;
        $in_sql = 'a';

        $result = $obj->integra_key_in($campo,$con_and,$in_sql);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("b IN (a)",$result);
        error::$en_error = false;

        $campo = 'b';
        $con_and = true;
        $in_sql = 'a';

        $result = $obj->integra_key_in($campo,$con_and,$in_sql);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("AND b IN (a)",$result);

        error::$en_error = false;


    }

    final public function test_joins()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $params = new stdClass();
        $params->a = new stdClass();
        $params->a->right = 'rx';
        $params->a->ren_left = '';

        $params->b = new stdClass();
        $params->b->right = 'dd';
        $params->b->ren_left = '';
        $result = $obj->joins($params);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN a AS a ON a.id = rx.a_id LEFT JOIN b AS b ON b.id = dd.b_id",$result);
        error::$en_error = false;

    }

    final public function test_joins_replace()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $joins = array();
        $joins[] = 's';

        $result = $obj->joins_replace($joins);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN s",$result);
        error::$en_error = false;




    }

    final public function test_joins_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $joins = array();
        $joins[0]['entidad_left'] = 'left';
        $joins[0]['entidad_right'] = 'right';

        $joins[1]['entidad_left'] = 'left1';
        $joins[1]['entidad_right'] = 'right1';

        $result = $obj->joins_sql($joins);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals(' LEFT JOIN left AS left ON left.id = right.left_id  LEFT JOIN left1 AS left1 ON left1.id = right1.left1_id ',$result);
        error::$en_error = false;


    }


    final public function test_left_join_base()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $entidad_right = 'der';
        $entidad_left = 'izq';
        $rename_entidad_left = '';
        $result = $obj->left_join_base($entidad_left,$entidad_right,$rename_entidad_left);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN izq AS izq ON izq.id = der.izq_id",$result);
        error::$en_error = false;

    }

    final public function test_left_join()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $entidad = '$entidad';
        $entidad_right = '$entidad_right';
        $key_left = 'kl';
        $key_right = 'kr';
        $rename_entidad = 're';
        $result = $obj->left_join($entidad,$entidad_right,$key_left,$key_right,$rename_entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('$entidad AS re ON re.kl = $entidad_right.kr',$result);
        error::$en_error = false;

    }

    final public function test_left_join_inicial()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $entidad_left = 'left';
        $entidad_right = 'right';
        $result = $obj->left_join_inicial($entidad_left,$entidad_right);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('left AS left LEFT JOIN right AS right ON left.right_id = right.id',$result);
        error::$en_error = false;

    }

    final public function test_left_joins()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $params = array();
        $params[0] = new stdClass();
        $params[0]->entidad = 'x';
        $params[0]->entidad_right = 'g';


        $result = $obj->left_joins($params);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("x AS x ON x.id = g.x_id",$result[0]);
        error::$en_error = false;




    }

    final public function test_left_joins_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);


        $lfs = array();
        $lfs['t'] = 'x';
        $result = $obj->left_joins_sql($lfs);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN t AS t ON t.id = x.t_id",$result);
        error::$en_error = false;


        error::$en_error = false;


    }

    final public function test_left_joins_txt()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $lfs = array();
        $lfs[] = 'a';
        $lfs[] = 'b';
        $result = $obj->left_joins_txt($lfs);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LEFT JOIN a LEFT JOIN b",$result);
        error::$en_error = false;

    }

    final public function test_limit_base()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $limit = '1';
        $result = $obj->limit_base($limit);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("LIMIT 1",$result);
        error::$en_error = false;



    }

    final public function test_n_rows()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $campo = 'b';
        $entidad = 'a';
        $value = 'c';
        $result = $obj->n_rows($campo, $entidad, $value);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT COUNT(*) AS n_rows FROM a WHERE b = 'c'",$result);
        error::$en_error = false;



    }

    final public function test_obten_nombre_tabla()
    {
        error::$en_error = false;
        $modelo = new sql();
        //$modelo = new liberator($modelo);

        $tabla_original = 'a';
        $tabla_renombrada = '';
        $resultado = $modelo->obten_nombre_tabla($tabla_renombrada,$tabla_original);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('a',$resultado);

        error::$en_error = false;

        $tabla_original = 'a';
        $tabla_renombrada = 'b';
        $resultado = $modelo->obten_nombre_tabla($tabla_renombrada,$tabla_original);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('b',$resultado);

        error::$en_error = false;

        $tabla_original = '';
        $tabla_renombrada = 'b';
        $resultado = $modelo->obten_nombre_tabla($tabla_renombrada,$tabla_original);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('b',$resultado);

        error::$en_error = false;


    }

    final public function test_on_para_join()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $entidad_right = 'r';
        $entidad_left = 'l';
        $key_left = '';
        $key_right = '';
        $result = $obj->on_para_join($entidad_left,$entidad_right,$key_left,$key_right);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("l.id = r.l_id",$result);
        error::$en_error = false;

    }

    final public function test_order_by_base()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $campo = 'a';
        $type = '';
        $result = $obj->order_by_base($campo,$type);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("ORDER BY a ",$result);
        error::$en_error = false;



    }

    final public function test_param_campo_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $campo = 'x.x';
        $campos_sql = 'x';
        $result = $obj->param_campo_sql($campo,$campos_sql);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('x.x',$result->campo);
        $this->assertEquals('x.x AS x_x',$result->campo_sql);
        $this->assertEquals(',',$result->coma);
        error::$en_error = false;


    }

    final public function test_params_campo()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $campo = 'b';
        $campos_sql = 'x';
        $entidad = 'a';
        $result = $obj->params_campo($campo,$campos_sql,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('a.b AS a_b',$result->campo_sql);
        $this->assertEquals(',',$result->coma);
        error::$en_error = false;



    }

    final public function test_params_base_left()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $entidad = 'entidad';
        $entidad_right = 'entidad_right';
        $key_left = 'kl';
        $key_right = 'kr';
        $rename = 'rn';
        $result = $obj->params_base_left($entidad,$entidad_right,$key_left,$key_right,$rename);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('entidad AS rn',$result->table);
        $this->assertEquals('rn.kl = entidad_right.kr',$result->on);
        error::$en_error = false;



    }

    final public function test_params_sql_base()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $campo = 'ss';
        $limit = '11';
        $type = '';
        $result = $obj->params_sql_base($campo,$limit,$type);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("ORDER BY ss  LIMIT 11",$result);
        error::$en_error = false;



    }

    final public function test_rename_entidad()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $join = new stdClass();
        $join->rename_entidad_left = 'xxxx';
        $result = $obj->rename_entidad($join);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("xxxx",$result);
        error::$en_error = false;



    }

    final public function test_replace_join_compare()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $replaces = new stdClass();
        $replaces->join = 'x';
        $result = $obj->replace_join_compare($replaces);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("x",$result);
        error::$en_error = false;



    }

    final public function test_replace_join_rs()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $replaces = new stdClass();
        $replaces->join_replace = 'xxx';
        $result = $obj->replace_join_rs($replaces);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("xxx",$result);
        error::$en_error = false;



    }

    final public function test_rs_left()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $entidad_left = array();
        $entidad_left['left'] = 'a';
        $entidad_left['right'] = 'b';
        $result = $obj->rs_left($entidad_left);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a AS a ON a.id = b.a_id",$result);
        error::$en_error = false;


    }

    final public function test_rfs_lefts()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);

        $joins_base = array();
        $joins_base[0]['left'] = 'a';
        $joins_base[0]['right'] = 'b';

        $joins_base[1]['left'] = 'c';
        $joins_base[1]['right'] = 'a';
        $result = $obj->rfs_lefts($joins_base);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("a AS a ON a.id = b.a_id",$result['a']);
        $this->assertEquals("c AS c ON c.id = a.c_id",$result['c']);
        error::$en_error = false;


    }

    final public function test_select_base()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $campos = '';
        $entidad_origen = 'a';
        $key_id_entidad = 'r';
        $limit = '2';
        $params_join = new stdClass();
        $type_order = '';
        $value = '';
        $type_operador = 'igual';
        $result = $obj->select_base($campos,$entidad_origen,$key_id_entidad,$limit,$params_join,$type_order,$value,$type_operador);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT * FROM a AS a  WHERE r = '' ORDER BY r  LIMIT 2;",$result);
        error::$en_error = false;



    }

    final public function test_select_base_left()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $campos_sql = '';
        $entidad_left = 'el';
        $entidad_right = 'er';
        $st_where = 'x';

        $result = $obj->select_base_left($campos_sql,$entidad_left,$entidad_right,$st_where);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT * FROM el AS el LEFT JOIN er AS er ON el.er_id = er.id WHERE x;",$result);
        error::$en_error = false;



    }

    final public function test_select_base_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $entidad_base = 'c';
        $order = 'a';
        $params = new stdClass();
        $params->filtro = 'a';

        $result = $obj->select_base_sql($entidad_base,$order,$params);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT * FROM c AS c  WHERE a a",$result);
        error::$en_error = false;




    }

    final public function test_select_by_filter()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $entidad = 'a';
        $filtro_sql = 'v';
        $result = $obj->select_by_filter($entidad,$filtro_sql, $this->link,array(),array());

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('SELECT *  FROM a  WHERE v',$result);

        error::$en_error = false;
        $entidad = 'a';
        $filtro_sql = 'v';
        $campos_extra[] = 'a';
        $result = $obj->select_by_filter($entidad,$filtro_sql,$this->link,$campos_extra, array());
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('SELECT * ,a AS a FROM a  WHERE v',$result);

        error::$en_error = false;
        $entidad = 'a';
        $filtro_sql = 'v';
        $campos_extra[] = 'a.x';
        $campos_extra[] = 'f.f';
        $result = $obj->select_by_filter($entidad,$filtro_sql, $this->link,$campos_extra, array());

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('SELECT * ,a AS a,a.x AS a_x,f.f AS f_f FROM a  WHERE v',$result);
        error::$en_error = false;

        $entidad = 'contrato';
        $filtro_sql = 'v';
        $campos_extra[] = 'a.x';
        $campos_extra[] = 'f.f';
        $joins = array();
        $joins[0] = new stdClass();
        $joins[0]->entidad_left = 'a';
        $joins[0]->entidad_right = 'b';
        $result = $obj->select_by_filter($entidad,$filtro_sql, $this->link,$campos_extra,$joins);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals('SELECT contrato.id AS contrato_id,contrato.status AS contrato_status,contrato.DocEntry AS contrato_DocEntry,contrato.CANCELED AS contrato_CANCELED,contrato.DocDate AS contrato_DocDate,contrato.CardCode AS contrato_CardCode,contrato.CardName AS contrato_CardName,contrato.Address AS contrato_Address,contrato.DocTotal AS contrato_DocTotal,contrato.PaidToDate AS contrato_PaidToDate,contrato.Comments AS contrato_Comments,contrato.SlpCode AS contrato_SlpCode,contrato.Address2 AS contrato_Address2,contrato.OwnerCode AS contrato_OwnerCode,contrato.U_StatusVtas AS contrato_U_StatusVtas,contrato.U_StatusCob AS contrato_U_StatusCob,contrato.U_SeSol AS contrato_U_SeSol,contrato.U_FolioSol AS contrato_U_FolioSol,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont,contrato.U_ImportePago AS contrato_U_ImportePago,contrato.U_FeContCancel AS contrato_U_FeContCancel,contrato.U_InvInicial AS contrato_U_InvInicial,contrato.U_ItemCode AS contrato_U_ItemCode,contrato.U_ItemName AS contrato_U_ItemName,contrato.U_CanalVta AS contrato_U_CanalVta,contrato.U_CondPago AS contrato_U_CondPago,contrato.U_Pagar AS contrato_U_Pagar,contrato.U_Papeleria AS contrato_U_Papeleria,contrato.U_FechaUltimoPago AS contrato_U_FechaUltimoPago,contrato.U_DiasSinAbono AS contrato_U_DiasSinAbono,contrato.U_FechaProximoPago AS contrato_U_FechaProximoPago,contrato.U_TipoMorosidad AS contrato_U_TipoMorosidad,contrato.U_TipoContrato AS contrato_U_TipoContrato,contrato.usuario_alta_id AS contrato_usuario_alta_id,contrato.usuario_update_id AS contrato_usuario_update_id,contrato.fecha_alta AS contrato_fecha_alta,contrato.fecha_update AS contrato_fecha_update,contrato.cliente_id AS contrato_cliente_id,contrato.plaza_id AS contrato_plaza_id,contrato.empleado_id AS contrato_empleado_id,contrato.pagado AS contrato_pagado,contrato.fecha_valida_inversion AS contrato_fecha_valida_inversion,contrato.fecha_valida_informacion AS contrato_fecha_valida_informacion,contrato.valida_inversion_inicial AS contrato_valida_inversion_inicial,contrato.valida_informacion AS contrato_valida_informacion,contrato.status_contrato_id AS contrato_status_contrato_id,contrato.aplica_modifica_sap AS contrato_aplica_modifica_sap,contrato.tipo_oficina_id AS contrato_tipo_oficina_id,contrato.pais_contrato_id AS contrato_pais_contrato_id,contrato.estado_contrato_id AS contrato_estado_contrato_id,contrato.municipio_contrato_id AS contrato_municipio_contrato_id,contrato.pais_cobranza_id AS contrato_pais_cobranza_id,contrato.estado_cobranza_id AS contrato_estado_cobranza_id,contrato.municipio_cobranza_id AS contrato_municipio_cobranza_id,contrato.cp_contrato_id AS contrato_cp_contrato_id,contrato.cp_cobranza_id AS contrato_cp_cobranza_id,contrato.producto_id AS contrato_producto_id,contrato.periodicidad_pago_id AS contrato_periodicidad_pago_id,contrato.nombre_concatenado AS contrato_nombre_concatenado,contrato.telefono AS contrato_telefono,contrato.fecha_ultima_visita AS contrato_fecha_ultima_visita,contrato.fecha_ultima_visita_cliente AS contrato_fecha_ultima_visita_cliente,contrato.fecha_status_contrato AS contrato_fecha_status_contrato,contrato.latitud AS contrato_latitud,contrato.longitud AS contrato_longitud,contrato.dv_santander AS contrato_dv_santander,contrato.dia_pago AS contrato_dia_pago,contrato.dia_pago_1 AS contrato_dia_pago_1,contrato.dia_pago_2 AS contrato_dia_pago_2,contrato.solicitud_servicios_acuerdos_id AS contrato_solicitud_servicios_acuerdos_id,contrato.ohem_callcenter_id AS contrato_ohem_callcenter_id,contrato.fecha_callcenter AS contrato_fecha_callcenter,contrato.monto_promesa_pago AS contrato_monto_promesa_pago,contrato.contrato_estadistica_id AS contrato_contrato_estadistica_id,contrato.periodicidad_local AS contrato_periodicidad_local,contrato.horario_visita AS contrato_horario_visita,contrato.fecha_reprogramada AS contrato_fecha_reprogramada,contrato.fecha_promocion AS contrato_fecha_promocion,contrato.contrato_anterior_id AS contrato_contrato_anterior_id,contrato.url_coordenadas AS contrato_url_coordenadas,contrato.whatsapp AS contrato_whatsapp,contrato.path_documentacion AS contrato_path_documentacion,contrato.conciliado_sap AS contrato_conciliado_sap,contrato.tiene_nc_sap AS contrato_tiene_nc_sap,contrato.ultima_rev_sap AS contrato_ultima_rev_sap,contrato.monto_nc_sap AS contrato_monto_nc_sap,contrato.monto_nc_em3 AS contrato_monto_nc_em3,contrato.monto_pagos_sap AS contrato_monto_pagos_sap,contrato.monto_pagos_em3 AS contrato_monto_pagos_em3,contrato.monto_nc_sap_cancel AS contrato_monto_nc_sap_cancel,contrato.monto_nc_em3_cancel AS contrato_monto_nc_em3_cancel,contrato.monto_pagos_sap_cancel AS contrato_monto_pagos_sap_cancel,contrato.monto_pagos_em3_cancel AS contrato_monto_pagos_em3_cancel,contrato.cargos_sap AS contrato_cargos_sap,contrato.cargos_em3 AS contrato_cargos_em3,contrato.diferencia_cargos AS contrato_diferencia_cargos,contrato.n_contratos_sap AS contrato_n_contratos_sap,contrato.n_contratos_em3 AS contrato_n_contratos_em3,contrato.monto_por_sincronizar AS contrato_monto_por_sincronizar,contrato.revisar_a_detalle AS contrato_revisar_a_detalle,contrato.empresa_id AS contrato_empresa_id,contrato.ultima_reg_saldo AS contrato_ultima_reg_saldo,contrato.costo_est_aplicado AS contrato_costo_est_aplicado ,a AS a,a.x AS a_x,f.f AS f_f,a.x AS a_x,f.f AS f_f FROM contrato  LEFT JOIN a AS a ON a.id = b.a_id  WHERE v',$result);



        error::$en_error = false;

        $entidad = 'ohem';
        $filtro_sql = 'v';
        $campos_extra[] = 'a.x';
        $campos_extra[] = 'f.f';
        $joins = array();
        $joins[0] = new stdClass();
        $joins[0]->entidad_left = 'a';
        $joins[0]->entidad_right = 'b';

        $joins[1] = new stdClass();
        $joins[1]->entidad_left = 'c';
        $joins[1]->entidad_right = 'd';
        $result = $obj->select_by_filter($entidad,$filtro_sql, $this->link,$campos_extra,$joins);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);

        $this->assertEquals('SELECT ohem.id AS ohem_id,ohem.status AS ohem_status,ohem.usuario_alta_id AS ohem_usuario_alta_id,ohem.usuario_update_id AS ohem_usuario_update_id,ohem.fecha_alta AS ohem_fecha_alta,ohem.fecha_update AS ohem_fecha_update,ohem.empID AS ohem_empID,ohem.lastName AS ohem_lastName,ohem.firstName AS ohem_firstName,ohem.middleName AS ohem_middleName,ohem.sex AS ohem_sex,ohem.dept AS ohem_dept,ohem.branch AS ohem_branch,ohem.manager AS ohem_manager,ohem.salesPrson AS ohem_salesPrson,ohem.mobile AS ohem_mobile,ohem.homeTel AS ohem_homeTel,ohem.email AS ohem_email,ohem.startDate AS ohem_startDate,ohem.termDate AS ohem_termDate,ohem.homeStreet AS ohem_homeStreet,ohem.homeBlock AS ohem_homeBlock,ohem.homeZip AS ohem_homeZip,ohem.homeCity AS ohem_homeCity,ohem.homeCounty AS ohem_homeCounty,ohem.homeCountr AS ohem_homeCountr,ohem.homeState AS ohem_homeState,ohem.birthDate AS ohem_birthDate,ohem.brthCountr AS ohem_brthCountr,ohem.martStatus AS ohem_martStatus,ohem.nChildren AS ohem_nChildren,ohem.govID AS ohem_govID,ohem.remark AS ohem_remark,ohem.position AS ohem_position,ohem.StreetNoW AS ohem_StreetNoW,ohem.StreetNoH AS ohem_StreetNoH,ohem.ExtEmpNo AS ohem_ExtEmpNo,ohem.Active AS ohem_Active,ohem.PassIssuer AS ohem_PassIssuer,ohem.U_FeEntDoctos AS ohem_U_FeEntDoctos,ohem.U_TipoIngre AS ohem_U_TipoIngre,ohem.U_InvPor AS ohem_U_InvPor,ohem.U_EsqPago AS ohem_U_EsqPago,ohem.U_ContLab AS ohem_U_ContLab,ohem.U_PorComiGestor AS ohem_U_PorComiGestor,ohem.U_ImpComiGte AS ohem_U_ImpComiGte,ohem.U_IdInvPor AS ohem_U_IdInvPor,ohem.nombre AS ohem_nombre,ohem.apellido_p AS ohem_apellido_p,ohem.apellido_m AS ohem_apellido_m,ohem.acumulado AS ohem_acumulado,ohem.empleado_id AS ohem_empleado_id,ohem.esquema_id AS ohem_esquema_id,ohem.cont_lab_id AS ohem_cont_lab_id,ohem.pais_home_id AS ohem_pais_home_id,ohem.estado_home_id AS ohem_estado_home_id,ohem.puesto_id AS ohem_puesto_id,ohem.puesto_fiscal_id AS ohem_puesto_fiscal_id,ohem.departamento_id AS ohem_departamento_id,ohem.oubr_id AS ohem_oubr_id,ohem.empleado_jefe_inmediato_id AS ohem_empleado_jefe_inmediato_id,ohem.simpli_id AS ohem_simpli_id,ohem.sexo AS ohem_sexo,ohem.estado_civil AS ohem_estado_civil,ohem.pais_nacionalidad_id AS ohem_pais_nacionalidad_id,ohem.estado_nacionalidad_id AS ohem_estado_nacionalidad_id,ohem.oslp_id AS ohem_oslp_id,ohem.plaza_id AS ohem_plaza_id,ohem.sincronizado_sap AS ohem_sincronizado_sap,ohem.aplica_modifica_sap AS ohem_aplica_modifica_sap,ohem.comentario_sync AS ohem_comentario_sync,ohem.salario_diario AS ohem_salario_diario,ohem.salario_diario_integrado AS ohem_salario_diario_integrado,ohem.nss AS ohem_nss,ohem.fecha_alta_imss AS ohem_fecha_alta_imss,ohem.fecha_baja_imss AS ohem_fecha_baja_imss,ohem.es_administrativo AS ohem_es_administrativo,ohem.pago_semanal_infonavit AS ohem_pago_semanal_infonavit,ohem.identificador_infonavit AS ohem_identificador_infonavit,ohem.fecha_aplicacion_infonavit AS ohem_fecha_aplicacion_infonavit,ohem.oficina_id AS ohem_oficina_id,ohem.empleado_jefe_alterno_id AS ohem_empleado_jefe_alterno_id,ohem.orden_empleado_jefe_alterno AS ohem_orden_empleado_jefe_alterno,ohem.nombre_completo AS ohem_nombre_completo,ohem.comision_empleado_jefe_alterno AS ohem_comision_empleado_jefe_alterno,ohem.salario AS ohem_salario,ohem.bono_gasolina AS ohem_bono_gasolina,ohem.bono_gasolina_variable AS ohem_bono_gasolina_variable,ohem.bono_gasolina_fijo AS ohem_bono_gasolina_fijo,ohem.comision_adicional AS ohem_comision_adicional,ohem.aplica_garantia_gestor AS ohem_aplica_garantia_gestor,ohem.sindicalizado AS ohem_sindicalizado,ohem.fotografia AS ohem_fotografia,ohem.baja_por_inactividad AS ohem_baja_por_inactividad,ohem.fecha_inicio_esquema AS ohem_fecha_inicio_esquema,ohem.ohem_reclutador_id AS ohem_ohem_reclutador_id,ohem.ohem_jefe_alterno2_id AS ohem_ohem_jefe_alterno2_id,ohem.telegram_id AS ohem_telegram_id,ohem.periodicidad_pago_id AS ohem_periodicidad_pago_id,ohem.token AS ohem_token,ohem.realizo_encuesta_baja AS ohem_realizo_encuesta_baja,ohem.comentarios_baja AS ohem_comentarios_baja,ohem.estado_fiscal_id AS ohem_estado_fiscal_id,ohem.municipio_fiscal_id AS ohem_municipio_fiscal_id,ohem.cp_fiscal_id AS ohem_cp_fiscal_id,ohem.domicilio_fiscal AS ohem_domicilio_fiscal,ohem.numero_interior_fiscal AS ohem_numero_interior_fiscal,ohem.numero_exterior_fiscal AS ohem_numero_exterior_fiscal,ohem.cuenta_ventas_personales_percep AS ohem_cuenta_ventas_personales_percep,ohem.padrino_id AS ohem_padrino_id,ohem.empleado_virtual AS ohem_empleado_virtual,ohem.fuente_reclutamiento AS ohem_fuente_reclutamiento,ohem.escolaridad AS ohem_escolaridad,ohem.experiencia_ventas AS ohem_experiencia_ventas,ohem.beneficiario AS ohem_beneficiario,ohem.parentesco_beneficiario AS ohem_parentesco_beneficiario,ohem.registro_patronal AS ohem_registro_patronal,ohem.es_punta AS ohem_es_punta,ohem.pension_alimenticia AS ohem_pension_alimenticia,ohem.fonacot AS ohem_fonacot,ohem.oficina_trabajo_id AS ohem_oficina_trabajo_id ,a AS a,a.x AS a_x,f.f AS f_f,a.x AS a_x,f.f AS f_f,a.x AS a_x,f.f AS f_f FROM ohem  LEFT JOIN a AS a ON a.id = b.a_id  LEFT JOIN c AS c ON c.id = d.c_id  WHERE v',$result);
        error::$en_error = false;


    }
    final public function test_select_by_id()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $entidad = 'a';
        $registro_id = 1;
        $columnas = new stdClass();
        $result = $obj->select_by_id($columnas, $entidad,$registro_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT * FROM a WHERE a.id = 1",$result);
        error::$en_error = false;

        $entidad = 'a';
        $registro_id = 1;
        $columnas = new stdClass();
        $columnas->x = new stdClass();
        $columnas->x->atributo = 'attr1';

        $columnas->y = new stdClass();
        $columnas->y->atributo = 'attr2';
        $result = $obj->select_by_id($columnas, $entidad,$registro_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT a.attr1 AS attr1,a.attr2 AS attr2 FROM a WHERE a.id = 1",$result);
        error::$en_error = false;

        $entidad = 'a';
        $registro_id = 1;
        $columnas = new stdClass();
        $columnas->x = new stdClass();
        $columnas->x->atributo = 'attr1';

        $columnas->y = new stdClass();
        $columnas->y->atributo = 'attr2';
        $sql_where_extra = ' x';
        $result = $obj->select_by_id($columnas, $entidad,$registro_id,$sql_where_extra);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT a.attr1 AS attr1,a.attr2 AS attr2 FROM a WHERE a.id = 1  AND x",$result);
        error::$en_error = false;

    }

    final public function test_sentencia()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $filtro = array();
        $tipo_filtro = '';
        $result = $obj->sentencia($filtro,$tipo_filtro);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("",$result);
        error::$en_error = false;

        $filtro = array();
        $tipo_filtro = '';
        $filtro[] = '';
        $result = $obj->sentencia($filtro,$tipo_filtro);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("",$result);

        error::$en_error = false;


        $filtro = array();
        $tipo_filtro = 'x';
        $filtro[] = '';
        $result = $obj->sentencia($filtro,$tipo_filtro);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("",$result);
        error::$en_error = false;

        $filtro = array();
        $tipo_filtro = 'numeros';
        $filtro['tabla'] = '';
        $result = $obj->sentencia($filtro,$tipo_filtro);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("tabla = ''",$result);

        error::$en_error = false;

        $filtro = array();
        $tipo_filtro = 'textos';
        $filtro['tabla'] = '';
        $result = $obj->sentencia($filtro,$tipo_filtro);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("tabla LIKE '%%'",$result);
        error::$en_error = false;

    }

    final public function test_sentencia_equals()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $campo = 'zx';
        $value = '';
        $result = $obj->sentencia_equals($campo,$value);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("zx = ''",$result);
        error::$en_error = false;


    }

    final public function test_sentencia_like()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $campo = 'a';
        $value = 'x';
        $result = $obj->sentencia_like($campo,$value);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("a LIKE '%x%'",$result);
        error::$en_error = false;

    }

    final public function test_show_tables()
    {
        error::$en_error = false;
        $obj = new sql();
        $result = $obj->show_tables();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SHOW TABLES",$result);
        error::$en_error = false;

    }
    final public function test_sobreescribe_sql()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $replaces_consulta = array();
        $consulta = 'joincolumna';
        $transaccion = 'a';
        $replaces_consulta['a'] = array();
        $replaces_consulta['a']['join'] = 'join';
        $replaces_consulta['a']['join_replace'] = 'join_replace';

        $replaces_consulta['a']['columna'] = 'columna';
        $replaces_consulta['a']['columna_replace'] = 'columna_replace';

        $result = $obj->sobreescribe_sql($consulta,$replaces_consulta,$transaccion);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("join_replacecolumna_replace",$result);
        error::$en_error = false;


        error::$en_error = false;


    }

    final public function test_statement_where()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $campos_where = new stdClass();
        $campos_where->x  = new stdClass();
        $campos_where->x->campo  = 'x';
        $campos_where->x->value  = '';
        $result = $obj->statement_where($campos_where);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("x = ''",$result);
        error::$en_error = false;


        $campos_where = new stdClass();
        $campos_where->x  = new stdClass();
        $campos_where->x->campo  = 'x';
        $campos_where->x->value  = '';

        $campos_where->y  = new stdClass();
        $campos_where->y->campo  = 's';
        $campos_where->y->value  = 'd';
        $result = $obj->statement_where($campos_where);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("x = '' AND s = 'd'",$result);
        error::$en_error = false;
    }

    final public function test_sum_by_filter()
    {
        error::$en_error = false;
        $obj = new sql();
        $campo = 'df';
        $entidad = 'a';
        $filtro_sql = 'v';
        $result = $obj->sum_by_filter($campo,$entidad,$filtro_sql);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("SELECT SUM(df) AS total FROM a WHERE v",$result);
        error::$en_error = false;



    }

    final public function test_valida_entrada_rename()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);



        $entidad = 'v';
        $campo = 'a';
        $alias = 'c';

        $result = $obj->valida_entrada_rename($alias,$campo,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;




    }

    final public function test_valida_param()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);



        $param = new stdClass();
        $param->entidad = 'a';
        $param->entidad_right = 'a';


        $result = $obj->valida_param($param);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;




    }


    final public function test_valida_type_operador()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);


        $type_operador = '';
        $result = $obj->valida_type_operador($type_operador);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $type_operador esta vacio',$result['mensaje_limpio']);
        error::$en_error = false;

        $type_operador = 'x';
        $result = $obj->valida_type_operador($type_operador);
        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error $type_operador invalido',$result['mensaje_limpio']);

        error::$en_error = false;

        $type_operador = 'igual';
        $result = $obj->valida_type_operador($type_operador);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);
        $this->assertTrue($result);
        error::$en_error = false;
    }

    final public function test_values_in()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);


        $valor = 'a';
        $values_sql = 'x';
        $result = $obj->values_in($valor,$values_sql);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("x, 'a'", $result);
        error::$en_error = false;

    }

    final public function test_values_sql_in()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);


        $values = array();
        $values[] = 'x';
        $values[] = 'y';
        $result = $obj->values_sql_in($values);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals(" 'x', 'y'", $result);
        error::$en_error = false;

    }

    final public function test_wh_canceled_n()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $entidad = 'ddd';
        $result = $obj->wh_canceled_n($entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("ddd.CANCELED = 'N'",$result);
        error::$en_error = false;



    }


    final public function test_wh_status_act()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $entidad = 'sss';
        $result = $obj->wh_status_act($entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("sss.status = 'activo'",$result);
        error::$en_error = false;



    }

    final public function test_where()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $sentencias = array();
        $sentencias[] = 'x';
        $sentencias[] = 'y';
        $sentencias[] = 'z';
        $result = $obj->where($sentencias);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("x AND y AND z",$result);
        error::$en_error = false;


    }

    final public function test_where_base_rpt()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);
        $row_data = array();
        $result = $obj->where_base_rpt($this->link,$row_data);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("empresa.id IN (73,74) AND ",$result);
        error::$en_error = false;


        $row_data = array();
        $sufijo = '_xxx';
        $result = $obj->where_base_rpt($this->link,$row_data, $sufijo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("empresa_xxx.id IN (73,74) AND ",$result);

        error::$en_error = false;


        $row_data = array('empresa_id'=>'2');
        $sufijo = '_xxx';
        $result = $obj->where_base_rpt($this->link,$row_data, $sufijo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("empresa_xxx.id = 2 AND ",$result);
        error::$en_error = false;

        $row_data = array('empresa_id'=>'2','plaza_id'=>'3');
        $sufijo = '_xxx';
        $result = $obj->where_base_rpt($this->link,$row_data, $sufijo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("empresa_xxx.id = 2 AND plaza.id = 3 AND ",$result);


        error::$en_error = false;


    }

    final public function test_wh_entre_fechas()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);

        $campo = 'a';
        $campo_fecha_fin = 'b';
        $campo_fecha_inicio = 'c';
        $result = $obj->wh_entre_fechas($campo,$campo_fecha_fin,$campo_fecha_inicio);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('a BETWEEN c AND b',$result);
        error::$en_error = false;


    }

    final public function test_where_id()
    {
        error::$en_error = false;
        $obj = new sql();
        //$obj = new liberator($obj);


        $entidad = 'x';
        $value = 11;
        $result = $obj->where_id($entidad,$value);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("x.id = 11",$result);
        error::$en_error = false;


        error::$en_error = false;


    }

    final public function test_where_sentencia_base()
    {
        error::$en_error = false;
        $obj = new sql();
        $obj = new liberator($obj);
        $campo = 'a';
        $value = 'd';
        $type_operador = 'like';
        $result = $obj->where_sentencia_base($campo,$type_operador,$value);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("WHERE a LIKE '%d%'",$result);
        error::$en_error = false;

        $campo = 'a';
        $value = 'd';
        $type_operador = 'igual';
        $result = $obj->where_sentencia_base($campo,$type_operador,$value);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("WHERE a = 'd'",$result);

        error::$en_error = false;

    }



}
