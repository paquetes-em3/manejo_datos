<?php
namespace desarrollo_em3\test\clases;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class consultasTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_campos()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        $modelo = new liberator($modelo);

        $entidad = 'pago_corte';
        $result = $modelo->campos($entidad, $this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('id',$result[0]->Field);
        $this->assertEquals('varchar(255)',$result[1]->Type);
        $this->assertEquals('YES',$result[2]->Null);
        $this->assertEquals('CURRENT_TIMESTAMP',$result[4]->Default);
        $this->assertEquals('DEFAULT_GENERATED on update CURRENT_TIMESTAMP',$result[5]->Extra);

        error::$en_error = false;


    }

    final public function test_campos_array()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        $modelo = new liberator($modelo);


        $campos = array();
        $campos[]['Field'] = 'x';
        $campos[1]  = new stdClass();
        $campos[1]->Field  = 'z';
        $result = $modelo->campos_array($campos);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('x',$result[0]);
        $this->assertEquals('z',$result[1]);

        error::$en_error = false;


    }

    final public function test_campos_obj()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        $modelo = new liberator($modelo);
        $campos = array();
        $campos[]['Field'] = 'x';
        $result = $modelo->campos_obj($campos);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('x',$result->x->Field);

        error::$en_error = false;


    }

    final public function test_campos_rs()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        $modelo = new liberator($modelo);
        $campos = array();
        $campos[]['Field'] = 'x';
        $entidad = 'a';
        $result = $modelo->campos_rs($campos, $entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('x',$result->campos_obj->x->Field);

        error::$en_error = false;


    }

    final public function test_campos_sql()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        $modelo = new liberator($modelo);

        $entidad = 'a';
        $campos = array();
        $campos[]['Field'] = 'x';
        $campos[1] = new stdClass();
        $campos[1]->Field = 'y';
        $result = $modelo->campos_sql($campos,$entidad);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('a.x AS a_x,a.y AS a_y',$result);

        error::$en_error = false;


    }
    final public function test_exe_objs()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        $modelo = new liberator($modelo);

        $sql = /** @lang MYSQL */
            'SELECT *FROM pais WHERE id > 240';
        $result = $modelo->exe_objs($this->link, $sql);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('241',$result[0]->id);
        $this->assertEquals('Venezuela, República Bolivariana de',$result[1]->descripcion);
        $this->assertEquals('VNM',$result[2]->codigo);
        $this->assertEquals('',$result[3]->codigo_sap);

        error::$en_error = false;


    }

    final public function test_existe()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        //$modelo = new liberator($modelo);

        $campo = 'pago_id';
        $entidad = 'costo_estimado_det';
        $value = '1';
        $result = $modelo->existe($campo, $entidad,$this->link,$value);

        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($result);


        $conexion = (new conexion_db_monterrey());
        $link = $conexion->link;

        $campo = 'pago_id';
        $entidad = 'costo_estimado_det';
        $value = '1';
        $result = $modelo->existe($campo, $entidad,$link,$value);
        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($result);
        error::$en_error = false;


    }

    final public function test_existe_by_id()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        //$modelo = new liberator($modelo);

        $entidad = 'pais';
        $registro_id = '2';
        $result = $modelo->existe_by_id($entidad,$this->link,$registro_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);

        error::$en_error = false;


    }

    final public function test_get_campos()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        //$seguridad = new liberator($seguridad);

        $entidad = 'pago';
        $result = $modelo->get_campos($entidad,$this->link);
        //print_r($result);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('pago.id AS pago_id,pago.status AS pago_status,pago.usuario_alta_id AS pago_usuario_alta_id,pago.usuario_update_id AS pago_usuario_update_id,pago.fecha_alta AS pago_fecha_alta,pago.fecha_update AS pago_fecha_update,pago.DocEntry AS pago_DocEntry,pago.Canceled AS pago_Canceled,pago.DocDate AS pago_DocDate,pago.DocDueDate AS pago_DocDueDate,pago.CardCode AS pago_CardCode,pago.DocTotal AS pago_DocTotal,pago.Comments AS pago_Comments,pago.U_CodGestor AS pago_U_CodGestor,pago.U_SePago AS pago_U_SePago,pago.U_FolioPago AS pago_U_FolioPago,pago.U_Movto AS pago_U_Movto,pago.U_Papeleria AS pago_U_Papeleria,pago.U_ComiCobranza AS pago_U_ComiCobranza,pago.U_ComiCalc AS pago_U_ComiCalc,pago.contrato_id AS pago_contrato_id,pago.empleado_gestor_id AS pago_empleado_gestor_id,pago.ohem_gestor_id AS pago_ohem_gestor_id,pago.comision_calculada AS pago_comision_calculada,pago.plaza_id AS pago_plaza_id,pago.sincronizado_sap AS pago_sincronizado_sap,pago.comentario_sync AS pago_comentario_sync,pago.ohem_id AS pago_ohem_id,pago.pago_corte_id AS pago_pago_corte_id,pago.latitud AS pago_latitud,pago.longitud AS pago_longitud,pago.colonia AS pago_colonia,pago.folio_asesor AS pago_folio_asesor,pago.metros AS pago_metros,pago.folio_mit AS pago_folio_mit,pago.referencia_mit AS pago_referencia_mit,pago.es_offline AS pago_es_offline,pago.es_transferencia AS pago_es_transferencia',$result->campos_sql);

        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $entidad = 'pago';
        $result = $modelo->get_campos($entidad,$this->link);
        //print_r($result);exit;
        $this->assertEquals('pago.id AS pago_id,pago.status AS pago_status,pago.usuario_alta_id AS pago_usuario_alta_id,pago.usuario_update_id AS pago_usuario_update_id,pago.fecha_alta AS pago_fecha_alta,pago.fecha_update AS pago_fecha_update,pago.fecha_corte AS pago_fecha_corte,pago.fecha_cliente AS pago_fecha_cliente,pago.cliente_id AS pago_cliente_id,pago.monto AS pago_monto,pago.comentario AS pago_comentario,pago.serie AS pago_serie,pago.folio AS pago_folio,pago.movimiento AS pago_movimiento,pago.papeleria AS pago_papeleria,pago.comision_gestor AS pago_comision_gestor,pago.contrato_id AS pago_contrato_id,pago.comision_calculada AS pago_comision_calculada,pago.plaza_id AS pago_plaza_id,pago.sincronizado_sap AS pago_sincronizado_sap,pago.comentario_sync AS pago_comentario_sync,pago.empleado_id AS pago_empleado_id,pago.pago_corte_id AS pago_pago_corte_id,pago.latitud AS pago_latitud,pago.longitud AS pago_longitud,pago.colonia AS pago_colonia,pago.id_pabs_pago AS pago_id_pabs_pago,pago.id_pabs_contrato AS pago_id_pabs_contrato,pago.id_pabs_gestor AS pago_id_pabs_gestor,pago.id_pabs_movimiento AS pago_id_pabs_movimiento,pago.bitacora_contrato_id AS pago_bitacora_contrato_id,pago.folio_asesor AS pago_folio_asesor,pago.metros AS pago_metros,pago.folio_mit AS pago_folio_mit,pago.referencia_mit AS pago_referencia_mit,pago.es_offline AS pago_es_offline,pago.es_transferencia AS pago_es_transferencia',$result->campos_sql);

        error::$en_error = false;


    }
    final public function test_registro_bruto()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        //$seguridad = new liberator($seguridad);

        $entidad = 'pais';
        $registro_id = '100';
        $rs_array = false;
        $columnas = new stdClass();
        $result = $modelo->registro_bruto($columnas, $entidad,$registro_id,$this->link,$rs_array);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(100,$result->id);

        error::$en_error = false;

        $entidad = 'pais';
        $registro_id = '200';
        $rs_array = true;
        $result = $modelo->registro_bruto($columnas, $entidad,$registro_id,$this->link,$rs_array);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(200,$result['id']);

        error::$en_error = false;

        $entidad = 'pais';
        $registro_id = '200';
        $rs_array = true;
        $columnas = new stdClass();
        $columnas->p = new stdClass();
        $columnas->p->atributo = 'status';
        $result = $modelo->registro_bruto($columnas, $entidad,$registro_id,$this->link,$rs_array);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('activo',$result['status']);

        error::$en_error = false;


        $entidad = 'pais';
        $registro_id = '200';
        $rs_array = true;
        $columnas = new stdClass();
        $sql_where_extra = "status = 'activo'";
        $result = $modelo->registro_bruto($columnas, $entidad,$registro_id,$this->link,$rs_array, $sql_where_extra);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('San Vicente y las Granadinas',$result['descripcion']);
        error::$en_error = false;
    }

    final public function test_registros_obj()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        $modelo = new liberator($modelo);

        $registros = array();
        $registros[]['x'] = 'x';
        $result = $modelo->registros_obj($registros);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('x',$result[0]->x);

        error::$en_error = false;


    }

    final public function test_rows_by_filter()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        //$seguridad = new liberator($seguridad);

        $entidad = 'pais';
        $filtro_sql = 'pais.id > 245';
        $result = $modelo->rows_by_filter($entidad,$filtro_sql,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(246,$result[0]->id);


        error::$en_error = false;

    }

    final public function test_sum_by_filter()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        //$seguridad = new liberator($seguridad);

        $campo = 'id';
        $entidad = 'pais';
        $filtro_sql = 'pais.id > 200';
        $result = $modelo->sum_by_filter($campo,$entidad,$filtro_sql,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(11025,$result);



        error::$en_error = false;

    }

    final public function test_tables_system()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        //$seguridad = new liberator($seguridad);

        $result = $modelo->tables_system($this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('prima_vacacional',$result[281]);
        $this->assertEquals('prima_vacacional',$_SESSION['tables_sistema'][281]);

        error::$en_error = false;


    }

    final public function test_val_filter()
    {
        error::$en_error = false;
        $modelo = new consultas($this->link);
        $modelo = new liberator($modelo);

        $entidad = 'd';
        $filtro_sql = 'f';
        $result = $modelo->val_filter($entidad,$filtro_sql);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);

        error::$en_error = false;


    }



}
