<?php
namespace desarrollo_em3\test\clases;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use PHPUnit\Framework\TestCase;

class transaccionesTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_ejecuta_sql()
    {
        error::$en_error = false;
        $modelo = new transacciones($this->link);
        //$seguridad = new liberator($seguridad);

        $consulta = 'SELECT 1 FROM pais';
        $result = $modelo->ejecuta_sql($consulta);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Exito',$result['mensaje']);
        $this->assertEquals('SELECT 1 FROM pais',$result['sql']);
        error::$en_error = false;

        $consulta = 'SELECT 1 FROM pais';
        $result = $modelo->ejecuta_sql($consulta, true);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals('Exito',$result->mensaje);
        $this->assertEquals('SELECT 1 FROM pais',$result->sql);

        error::$en_error = false;

    }

    final public function test_ejecuta_consulta_segura()
    {
        error::$en_error = false;
        $modelo = new transacciones($this->link);
        //$seguridad = new liberator($seguridad);

        $sql = 'SELECT 1 FROM pais LIMIT 1';
        $result = $modelo->ejecuta_consulta_segura($sql);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('1',$result['registros'][0][1]);

        error::$en_error = false;

        $sql = 'SELECT pais.id FROM pais  WHERE pais.id = :param1 LIMIT 1';
        $parametros['param1'] = 1;

        $result = $modelo->ejecuta_consulta_segura($sql,$parametros);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('1',$result['registros'][0]['id']);
        error::$en_error = false;

    }



}
