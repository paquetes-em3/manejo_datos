<?php
namespace desarrollo_em3\test\clases;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\_valida;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use PHPUnit\Framework\TestCase;

class _validaTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_valida_base_campos_fecha()
    {
        error::$en_error = false;
        $modelo = new _valida();
        //$seguridad = new liberator($seguridad);

        $campo_fecha_inicio = 'c';
        $entidad = 'b';
        $fecha = 'a';
        $result = $modelo->valida_base_campos_fecha($campo_fecha_inicio,$entidad,$fecha);


        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);


        error::$en_error = false;


    }


    final public function test_valida_data_modelo()
    {
        error::$en_error = false;
        $modelo = new _valida();
        //$seguridad = new liberator($seguridad);

        $name_modelo = '';
        $result = $modelo->valida_data_modelo($name_modelo);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('Error modelo no puede venir vacio',$result['mensaje_limpio']);

        error::$en_error = false;

        $name_modelo = 'a';
        $result = $modelo->valida_data_modelo($name_modelo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('a',$result);


        error::$en_error = false;

    }

    final public function test_valida_entrada_dias_prueba()
    {
        error::$en_error = false;
        $modelo = new _valida();
        //$seguridad = new liberator($seguridad);

        $campo_fecha_inicio = 'c';
        $entidad = 'b';
        $fecha = 'a';
        $n_dias_prueba = 15;
        $plazas_id = array(1);
        $result = $modelo->valida_entrada_dias_prueba($campo_fecha_inicio,$entidad,$fecha,$n_dias_prueba,$plazas_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);


        error::$en_error = false;


    }

    final public function test_valida_pago_corte()
    {
        error::$en_error = false;
        $modelo = new _valida();
        //$seguridad = new liberator($seguridad);

        $campo_monto_pago = 'c';
        $campo_movto = 'c';
        $pago_corte_id = 1;
        $result = $modelo->valida_pago_corte($campo_monto_pago,$campo_movto,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);


        error::$en_error = false;


    }



}
