<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\data\_childrens;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use PHPUnit\Framework\TestCase;

class _childrensTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_genera_modelos_hijos()
    {
        error::$en_error = false;
        $modelo = new _childrens();
        //$modelo = new liberator($modelo);
        $hijo = [0 => ['filtros' => [], 'filtros_con_valor' => []]];
        $resultado = $modelo->genera_modelos_hijos($hijo);
        $this->assertTrue(error::$en_error);
        $this->assertEquals('Error $key debe ser un string', $resultado['mensaje_limpio']);

        error::$en_error = false;

        $hijo = ['modelo1' => ['filtros_con_valor' => []]];
        $resultado = $modelo->genera_modelos_hijos($hijo);
        $this->assertTrue(error::$en_error);
        $this->assertEquals('Error debe existir filtros', $resultado['mensaje_limpio']);

        error::$en_error = false;
        $hijo = ['modelo1' => ['filtros' => []]];
        $resultado = $modelo->genera_modelos_hijos($hijo);
        $this->assertTrue(error::$en_error);
        $this->assertEquals('Error debe existir filtros_con_valor', $resultado['mensaje_limpio']);

        error::$en_error = false;

        $hijo = ['modelo1' => ['filtros' => 'no_array', 'filtros_con_valor' => []]];
        $resultado = $modelo->genera_modelos_hijos($hijo);
        $this->assertTrue(error::$en_error);
        $this->assertEquals('Error debe ser array filtros', $resultado['mensaje_limpio']);

        error::$en_error = false;

        $hijo = ['modelo1' => ['filtros' => [], 'filtros_con_valor' => 'no_array']];
        $resultado = $modelo->genera_modelos_hijos($hijo);
        $this->assertTrue(error::$en_error);
        $this->assertEquals('Error debe ser array filtros_con_valor', $resultado['mensaje_limpio']);
        error::$en_error = false;

        $hijo = [
            'modelo1' => ['filtros' => ['filtro1'], 'filtros_con_valor' => ['valor1']],
            'modelo2' => ['filtros' => ['filtro2'], 'filtros_con_valor' => ['valor2']]
        ];
        $resultado = $modelo->genera_modelos_hijos($hijo);
        $this->assertEquals(['modelo1' => ['filtros' => ['filtro1'], 'filtros_con_valor' => ['valor1']],
            'modelo2' => ['filtros' => ['filtro2'], 'filtros_con_valor' => ['valor2']]], $resultado);

        error::$en_error = false;
    }

    final public function test_obten_filtro_para_hijo()
    {
        error::$en_error = false;
        $modelo = new _childrens();
        //$modelo = new liberator($modelo);
        $data_modelo = array();
        $row = array();
        $data_modelo['filtros'] = array();
        $data_modelo['filtros_con_valor'] = array();
        $resultado = $modelo->obten_filtro_para_hijo($data_modelo,$row);
        $this->assertNotTrue(error::$en_error);
        $this->assertEmpty($resultado);

        error::$en_error = false;

        $data_modelo = [
            'filtros' => ['campo1' => 'campoA', 'campo2' => 'campoB'],
            'filtros_con_valor' => ['campo3' => 'valor3']
        ];
        $row = ['campoA' => 'valorA', 'campoB' => 'valorB'];

        $expected = [
            'campo1' => 'valorA',
            'campo2' => 'valorB',
            'campo3' => 'valor3'
        ];

        $result = $modelo->obten_filtro_para_hijo($data_modelo, $row);
        $this->assertEquals($expected, $result);

        error::$en_error = false;



        $data_modelo = [
            'filtros' => ['campo1' => 'campoA'],
            'filtros_con_valor' => ['campo2' => 'valor2']
        ];
        $row = ['campoA' => 'valorA'];

        $expected = [
            'campo1' => 'valorA',
            'campo2' => 'valor2'
        ];

        $result = $modelo->obten_filtro_para_hijo($data_modelo, $row);
        $this->assertEquals($expected, $result);

        error::$en_error = false;


    }

    final public function test_valida_filtros_hijo()
    {
        error::$en_error = false;
        $obj = new _childrens();
        //$obj = new liberator($obj);
        $data_modelo = array();
        $resultado = $obj->valida_filtros_hijo($data_modelo);
        $this->assertTrue(error::$en_error);
        $this->assertEquals('Error data_modelo[filtros] no existe', $resultado['mensaje_limpio']);

        error::$en_error = false;
        $data_modelo = array();
        $data_modelo['filtros'] = array();
        $resultado = $obj->valida_filtros_hijo($data_modelo);
        $this->assertTrue(error::$en_error);
        $this->assertEquals('Error data_modelo[filtros_con_valor] no existe', $resultado['mensaje_limpio']);

        error::$en_error = false;
        $data_modelo = array();
        $data_modelo['filtros'] = array();
        $data_modelo['filtros_con_valor'] = array();
        $resultado = $obj->valida_filtros_hijo($data_modelo);

        $this->assertTrue($resultado);

        error::$en_error = false;

    }

}
