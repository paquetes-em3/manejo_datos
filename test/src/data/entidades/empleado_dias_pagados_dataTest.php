<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\data\_childrens;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use desarrollo_em3\manejo_datos\sql\empleado;
use PHPUnit\Framework\TestCase;

class empleado_dias_pagados_dataTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_columnas_get_dias_pagados()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado_dias_pagados();
        //$obj = new liberator($obj);
        $entidad_empleado = 'x';
        $resultado = $obj->columnas_get_dias_pagados($entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('x_dias_pagados_id', $resultado[0]);
        $this->assertEquals('x_dias_pagados_n_dias_pagados', $resultado[1]);


        error::$en_error = false;
    }




}
