<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\data\entidades\oficina_trabajo;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class oficina_trabajo_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;

    }

    final public function test_get_oficina_trabajo_by_id()
    {
        $_SESSION['usuario_id'] = 2;
        error::$en_error = false;
        $modelo = new oficina_trabajo();
        //$modelo = new liberator($modelo);

        $oficina_trabajo_id = 2;
        $result  = $modelo->get_oficina_trabajo_by_id($this->link,$oficina_trabajo_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('SIN OFICINA',$result[0]->oficina_trabajo_descripcion);

        error::$en_error = false;

    }

}
