<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\data\entidades\acl_usuario;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class acl_usuario_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;

    }

    final public function test_genera_sentencia_seguridad()
    {
        error::$en_error = false;
        $obj = new acl_usuario();
        //$obj = new liberator($obj);
        
        $entidad = 'plaza';
        $usuario_id = 2;
        $resultado = $obj->genera_sentencia_seguridad($entidad,$this->link,$usuario_id);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals("35,27,31,26,30,23,29,27",$resultado['ids']);
        $this->assertEquals("plaza_id IN('35,27,31,26,30,23,29,27')",$resultado['sql']);

        error::$en_error = false;

        $entidad = 'plaza';
        $usuario_id = 2;
        $status = 'activo';
        $resultado = $obj->genera_sentencia_seguridad($entidad,$this->link,$usuario_id,$status);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals("35,27,31,26,30,29,27",$resultado['ids']);
        $this->assertEquals("plaza_id IN('35,27,31,26,30,29,27')",$resultado['sql']);

        error::$en_error = false;

        $entidad = 'departamento';
        $usuario_id = 2;
        $resultado = $obj->genera_sentencia_seguridad($entidad,$this->link,$usuario_id);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('6,10,3,39',$resultado['ids']);
        $this->assertEquals("departamento_id IN('6,10,3,39')",$resultado['sql']);

        error::$en_error = false;

        $entidad = 'departamento';
        $usuario_id = 1;
        $status = 'inactivo';
        $resultado = $obj->genera_sentencia_seguridad($entidad,$this->link,$usuario_id);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('0',$resultado['ids']);
        $this->assertEquals("departamento_id IN('0')",$resultado['sql']);

        error::$en_error = false;
    }

    final public function test_obten_ids_permiso()
    {
        error::$en_error = false;
        $obj = new acl_usuario();
        $obj = new liberator($obj);
        
        $entidad = 'plaza';
        $usuario_id = 2;
        $resultado = $obj->obten_ids_permiso($entidad,$this->link,$usuario_id);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('35,27,31,26,30,23,29,27',$resultado[0]->plaza);

        error::$en_error = false;

        $entidad = 'plaza';
        $usuario_id = 2;
        $status = 'activo';
        $resultado = $obj->obten_ids_permiso($entidad,$this->link,$usuario_id,$status);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('35,27,31,26,30,29,27',$resultado[0]->plaza);

        error::$en_error = false;

        $entidad = 'departamento';
        $usuario_id = 2;
        $resultado = $obj->obten_ids_permiso($entidad,$this->link,$usuario_id);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('6,10,3,39',$resultado[0]->departamento);

        error::$en_error = false;

        $entidad = 'departamento';
        $usuario_id = 2;
        $status = 'inactivo';
        $resultado = $obj->obten_ids_permiso($entidad,$this->link,$usuario_id,$status);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('0',$resultado[0]->departamento);

        error::$en_error = false;
    }

}
