<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\data\entidades\deposito_aportaciones;
use desarrollo_em3\manejo_datos\data\entidades\pago;
use desarrollo_em3\manejo_datos\data\entidades\pago_corte;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class deposito_aportaciones_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;

    }

    private function _base_deposito(float $monto_depositado)
    {
        $deposito = new stdClass();
        $deposito->id = -1;
        $deposito->cuenta_empresa_id = 34;
        $deposito->fecha = '2024-09-24';
        $deposito->link = $this->link;
        $deposito->monto_depositado = $monto_depositado;
        $deposito->pago_corte_id = 24450;

        return $deposito;

    }

    private function _depositos_sum_deposito_por_pago_corte(array $montos_depositados)
    {
        $depositos = array();
        foreach ($montos_depositados as $monto_depositado) {

            $deposito = $this->_base_deposito($monto_depositado);
            if (error::$en_error) {
                return (new error())->error('Error al obtener base', $deposito);
            }
            $depositos[] = $deposito;
        }

        return $depositos;

    }

    private function _inserta_deposito_aportaciones(
        PDO $link, int $id = -1, int $cuenta_empresa_id = 1, string $fecha = '2020-01-01', float $monto_depositado = 0,
        int $pago_corte_id = 1, string $referencia= 'referencia', int $serie_id=1, string $status = 'activo',
        string $validado= 'inactivo')
    {
        if($id <= 0){
            $id = mt_rand(1,9999999999999);
        }

        $campos = "id, status, usuario_alta_id, usuario_update_id, pago_corte_id, ";
        $campos .= "cuenta_empresa_id, referencia, monto_depositado, fecha, validado, serie_id";
        $values = "$id, '$status', 67, 67, $pago_corte_id, ";
        $values .= "$cuenta_empresa_id, '$referencia', $monto_depositado, '$fecha', '$validado', $serie_id";

        $sql = /** @lang MYSQL */
            "INSERT INTO deposito_aportaciones ($campos) VALUES ($values);";
        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al insertar',$exe);
        }
        return $exe;


    }

    private function _inserta_deposito_aportaciones_obj($deposito, PDO $link)
    {
        $exe = $this->_inserta_deposito_aportaciones($link, $deposito->id,$deposito->cuenta_empresa_id,
            $deposito->fecha,$deposito->monto_depositado,$deposito->pago_corte_id,'referencia',1,
            'activo','inactivo');
        if(error::$en_error){
            return (new error())->error('Error al insertar',$exe);
        }

        return $exe;

    }

    private function _inserta_depositos(array $montos_depositados, PDO $link)
    {
        $depositos = $this->_depositos_sum_deposito_por_pago_corte($montos_depositados);
        if (error::$en_error) {
            return (new error())->error('Error al obtener depositos', $depositos);
        }

        $exe = $this->_inserta_depositos_aportaciones($depositos, $link);
        if (error::$en_error) {
            return (new error())->error('Error al insertar', $exe);
        }
        return $exe;

    }

    private function _inserta_depositos_aportaciones(array $depositos, PDO $link)
    {
        $exes = array();
        foreach ($depositos as $deposito) {

            $exe = $this->_inserta_deposito_aportaciones_obj($deposito, $link);
            if (error::$en_error) {
                return (new error())->error('Error al insertar', $exe);
            }
        }
        return $exes;


    }

    final public function test_actualiza_serie()
    {
        $_SESSION['usuario_id'] = 2;
        error::$en_error = false;
        $modelo = new  deposito_aportaciones();
        $modelo = new liberator($modelo);
        $this->link->beginTransaction();

        $deposito_aportaciones_id = 1;
        $serie_id = 1;
        $result  = $modelo->actualiza_serie($deposito_aportaciones_id, $this->link,$serie_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        error::$en_error = false;

        $this->link->rollBack();

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();

        $result  = $modelo->actualiza_serie($deposito_aportaciones_id, $this->link,$serie_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->link->rollBack();

    }

    final public function test_deposito_aportaciones_row()
    {
        error::$en_error = false;
        $obj = new deposito_aportaciones();
        //$obj = new liberator($obj);

        $this->link->beginTransaction();

        $recalcula = (new pago_corte())->recalcula_montos('Canceled','DocTotal','U_Movto',$this->link,80143);
        if(error::$en_error){
            $error = (new error())->error('Error',$recalcula);
            print_r($error);
            exit;
        }

        $deposito_aportaciones_id = 69;
        $resultado = $obj->deposito_aportaciones_row($deposito_aportaciones_id,'ohem', $this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject( $resultado);
        $this->assertEquals('HUGO66600', $resultado->deposito_aportaciones_referencia);
        $this->assertEquals('2024-09-10', $resultado->deposito_aportaciones_fecha);
        $this->assertEquals(13000, $resultado->pago_corte_monto_total);
        $this->assertEquals(13000, $resultado->pago_corte_monto_depositado);
        $this->assertEquals(0, $resultado->pago_corte_monto_por_depositar);
        $this->assertEquals('Irapuato', $resultado->plaza_descripcion);
        $this->assertEquals('HUGO SANCHEZ LEON', $resultado->ohem_nombre_completo);
        $this->assertEquals('BBVA Bancomer, S.A., Institución de Banca Múltiple, Grupo Financiero BBVA Bancomer', $resultado->banco_razon_social);
        $this->assertEquals(116882, $resultado->pago_corte_id);
        $this->assertEquals(26, $resultado->plaza_id);
        $this->assertEquals(47, $resultado->deposito_aportaciones_cuenta_empresa_id);
        $this->assertEquals(500, $resultado->deposito_aportaciones_monto_depositado);

        error::$en_error = false;

        $this->link->rollBack();

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();


        $sql = "INSERT INTO deposito_aportaciones (`status`, `usuario_alta_id`, `usuario_update_id`, `fecha_alta`, `fecha_update`, `pago_corte_id`, `cuenta_empresa_id`, `referencia`, 
                                   `monto_depositado`, `fecha`, `validado`) VALUES ('activo', 2, 2, 
                                                                                    '2024-08-26 14:54:55', '2024-08-26 14:54:55', 5, 1,
                                                                                    'xxxxx', 100.00, '2024-08-26', 'inactivo');";


        $exe = (new transacciones($this->link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            $error = (new error())->error('Error al insertar',$exe);
            print_r($error);
            exit;
        }

        $deposito_aportaciones_id = $this->link->lastInsertId();

        $resultado = $obj->deposito_aportaciones_row($deposito_aportaciones_id, 'empleado', $this->link);
        //print_r($resultado);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject( $resultado);
        $this->assertEquals('xxxxx', $resultado->deposito_aportaciones_referencia);
        $this->assertEquals('2024-08-26', $resultado->deposito_aportaciones_fecha);
        $this->assertEquals(0, $resultado->pago_corte_monto_total);
        $this->assertEquals(0, $resultado->pago_corte_monto_depositado);
        $this->assertEquals(0, $resultado->pago_corte_monto_por_depositar);
        $this->assertEquals('Manzanillo', $resultado->plaza_descripcion);
        $this->assertEquals('MAURICIO ROSALES MARQUEZ', $resultado->empleado_nombre_completo);
        $this->assertEquals('Scotiabank Inverlat, S.A.', $resultado->banco_razon_social);
        $this->assertEquals(5, $resultado->pago_corte_id);
        $this->assertEquals(1, $resultado->plaza_id);
        $this->assertEquals(1, $resultado->deposito_aportaciones_cuenta_empresa_id);
        $this->assertEquals(100, $resultado->deposito_aportaciones_monto_depositado);

        $this->link->rollBack();



    }

    final public function test_obten_depositos_por_corte()
    {
        error::$en_error = false;
        $obj = new deposito_aportaciones();
        //$obj = new liberator($obj);

        $pago_corte_id = 72875;
        $resultado = $obj->obten_depositos_por_corte($this->link,$pago_corte_id);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);

        error::$en_error = false;
    }

    final public function test_sum_depositos_por_corte()
    {
        error::$en_error = false;
        $obj = new deposito_aportaciones();
        //$obj = new liberator($obj);

        $this->link->beginTransaction();

        $pago_corte_id = 15968;
        $resultado = $obj->sum_depositos_por_corte($this->link,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat( $resultado);
        $this->assertEquals(0, $resultado);

        error::$en_error = false;


        $deposito_aportacion_ins = "INSERT INTO deposito_aportaciones ( status, usuario_alta_id, usuario_update_id, 
                                   fecha_alta, fecha_update, pago_corte_id, cuenta_empresa_id, referencia, 
                                   monto_depositado, fecha) VALUES ( 'activo', 2, 2, 
                                                                    '2024-08-05 14:47:52', '2024-08-05 14:47:52', 
                                                                    15968, 1, '1', 10.00, '2020-01-01');";

        $rs = (new transacciones($this->link))->ejecuta_consulta_segura($deposito_aportacion_ins);
        if(error::$en_error){
            $this->link->rollBack();
            $error = (new error())->error('Error al ejecutar sql',$rs);
            print_r($error);
            exit;
        }
        $pago_corte_id = 15968;
        $resultado = $obj->sum_depositos_por_corte($this->link,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat( $resultado);
        $this->assertEquals(10, $resultado);
        $this->link->rollBack();

    }

    final public function test_sum_deposito_por_pago_corte()
    {
        error::$en_error = false;
        $obj = new deposito_aportaciones();
        //$obj = new liberator($obj);

        $conexion = (new conexion_db_monterrey());
        $this->link = $conexion->link;

        $this->link->beginTransaction();

        $montos_depositados[] = 1200;
        $montos_depositados[] = 900;
        $montos_depositados[] = 1700;
        $montos_depositados[] = 800;

        $exe = $this->_inserta_depositos($montos_depositados, $this->link);
        if (error::$en_error) {
            $error = (new error())->error('Error al insertar depositos', $exe);
            print_r($error);
            exit;
        }


        $enditda_empledao = 'ohem';
        $pago_corte_id = 24450;
        $resultado = $obj->sum_deposito_por_pago_corte($this->link,$enditda_empledao,$pago_corte_id);
        //print_r($resultado);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertEquals(4600, $resultado[0]->monto_entregado);

        error::$en_error = false;

        $enditda_empledao = 'ohem';
        $pago_corte_id = 24451;
        $resultado = $obj->sum_deposito_por_pago_corte($this->link,$enditda_empledao,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertEquals(0, $resultado[0]->monto_entregado);

        $this->link->rollBack();
        error::$en_error = false;

    }

    final public function test_verifica_corte_deposito()
    {
        $_SESSION['usuario_id'] = 2;
        error::$en_error = false;
        $modelo = new  deposito_aportaciones();
        //$modelo = new liberator($modelo);
        $this->link->beginTransaction();


        $deposito_aportaciones_id = 70;

        $sql = "UPDATE pago_corte SET validado = 'inactivo' WHERE pago_corte.id = 116882";

        $exe = (new transacciones($this->link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            $error = (new error())->error('Error al insertar',$exe);
            print_r($error);
            exit;
        }

        $sql = "UPDATE deposito_aportaciones SET validado = 'inactivo' WHERE deposito_aportaciones.id = '$deposito_aportaciones_id'";

        $exe = (new transacciones($this->link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            $error = (new error())->error('Error al insertar',$exe);
            print_r($error);
            exit;
        }

        $result  = $modelo->verifica_corte_deposito($deposito_aportaciones_id, $this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);
        error::$en_error = false;

        $this->link->rollBack();

        $this->link->beginTransaction();

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $this->link->beginTransaction();
        $deposito_aportaciones_id = 1;

        $sql = "INSERT INTO deposito_aportaciones (id, status, usuario_alta_id, usuario_update_id, fecha_alta, fecha_update, pago_corte_id, cuenta_empresa_id, referencia, monto_depositado, fecha, validado)
VALUES (1, 'activo', 2, 2, '2024-09-05 14:50:05', '2024-09-05 14:50:05', 5, 1, '1', 1.00, '2020-01-01', 'inactivo');";

        $exe = (new transacciones($this->link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            $error = (new error())->error('Error al insertar',$exe);
            print_r($error);
            exit;
        }

        $result  = $modelo->verifica_corte_deposito($deposito_aportaciones_id, $this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);
        $this->link->rollBack();

        error::$en_error = false;

    }

    final public function test_verifica_pago_corte()
    {
        $_SESSION['usuario_id'] = 2;
        error::$en_error = false;
        $modelo = new  deposito_aportaciones();
        //$modelo = new liberator($modelo);
        $this->link->beginTransaction();
        $pago_corte_id = 116882;
        $sql = /** @lang MYSQL */
            "UPDATE pago_corte SET validado = 'inactivo' WHERE pago_corte.id = $pago_corte_id";

        $exe = (new transacciones($this->link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            $error = (new error())->error('Error al $exe',$exe);
            print_r($error);
            exit;
        }



        $result  = $modelo->verifica_pago_corte($this->link,$pago_corte_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);
        error::$en_error = false;

        $this->link->rollBack();

        $this->link->beginTransaction();

        $result  = $modelo->verifica_pago_corte($this->link,$pago_corte_id);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->link->rollBack();

    }

}
