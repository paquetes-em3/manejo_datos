<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\data\entidades\reporte_query;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class reporte_query_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;

    }

    final public function test_genera_sentencia_departamento() {
        $_SESSION['usuario_id'] = 2;
        error::$en_error = false;
        $modelo = new reporte_query();
        $this->link->beginTransaction();
        //$modelo = new liberator($modelo);
        $sql = "INSERT INTO acl_usuario_departamento (id, status, usuario_alta_id, usuario_update_id, fecha_alta, fecha_update, usuario_id, departamento_id) VALUES (999, 'activo', 2, 2, '2024-10-17 09:58:40', '2024-10-17 09:58:40', 2, 2);";
        $exe = (new consultas())->exe_objs($this->link,$sql);
        if(error::$en_error){
            $error = (new error())->error('Error al insertar pago',$exe);
            print_r($error);
            exit;
        }
        
        $usuario_id = 2;
        $result = $modelo->genera_sentencia_departamento($this->link,$usuario_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals("reporte_query.departamento_id IN(6,10,3,39,2)",$result);
        $this->link->rollBack();
        error::$en_error = false;
    }

    final public function test_genera_sentencia_plaza() {
        $_SESSION['usuario_id'] = 2;
        error::$en_error = false;
        $modelo = new reporte_query();
        //$modelo = new liberator($modelo);
        $usuario_id = 2;
        $result = $modelo->genera_sentencia_plaza($this->link,$usuario_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('reporte_query.plaza_id IN(35,27,31,26,30,23,29,27)',$result);
        
        error::$en_error = false;
    }

}
