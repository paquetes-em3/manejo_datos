<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\conexion_db;
use PDO;
use PHPUnit\Framework\TestCase;

class esquema_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_campos()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\esquema();
        //$obj = new liberator($obj);
        $entidad = 'ohem';
        $campo_fecha_inicio = 'startDate';
        $resultado = $obj->campos($this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('esquema_id', $resultado->id->rename);
        $this->assertEquals('esquema_Code', $resultado->Code->rename);
        $this->assertEquals('esquema_status', $resultado->status->rename);
        $this->assertEquals('esquema_U_CodPuesto', $resultado->U_CodPuesto->rename);
        $this->assertEquals('esquema_U_Papeleria', $resultado->U_Papeleria->rename);
        $this->assertEquals('esquema_monto_deduccion', $resultado->monto_deduccion->rename);
        $this->assertEquals('esquema_aplica_ascenso', $resultado->aplica_ascenso->rename);
        $this->assertEquals('esquema_esquema_ascenso_id', $resultado->esquema_ascenso_id->rename);
        $this->assertEquals('esquema_descuento_comision_primer_venta', $resultado->descuento_comision_primer_venta->rename);
        $this->assertEquals('esquema_ayudas_garantia_equipo', $resultado->ayudas_garantia_equipo->rename);
        $this->assertEquals('esquema_comision_equipo', $resultado->comision_equipo->rename);
        $this->assertEquals('esquema_dias_jefe_alterno', $resultado->dias_jefe_alterno->rename);
        $this->assertEquals('esquema_aplica_complemento_comision', $resultado->aplica_complemento_comision->rename);
        $this->assertEquals('esquema_divide_comision_vendedor_nueva', $resultado->divide_comision_vendedor_nueva->rename);
        $this->assertEquals('esquema_activa_cadena_telefonista', $resultado->activa_cadena_telefonista->rename);
        $this->assertEquals('esquema_aplica_complemento_personal', $resultado->aplica_complemento_personal->rename);
        $this->assertEquals('esquema_aplica_complemento_equipo', $resultado->aplica_complemento_equipo->rename);


        error::$en_error = false;
    }




}
