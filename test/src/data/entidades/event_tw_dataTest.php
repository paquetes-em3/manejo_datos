<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use PDO;
use PHPUnit\Framework\TestCase;

class event_tw_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_n_eventos()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\event_tw();
        //$obj = new liberator($obj);
        $entidad_empleado = 'ohem';
        $fecha = '';
        $ohem_id = -1;
        $resultado = $obj->n_eventos($entidad_empleado,$fecha,$this->link,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0, $resultado);


        error::$en_error = false;


        $entidad_empleado = 'ohem';
        $fecha = '2023-06-08';
        $ohem_id = 18434;
        $resultado = $obj->n_eventos($entidad_empleado,$fecha,$this->link,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(2, $resultado);
        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $entidad_empleado = 'empleado';
        $fecha = '2024-09-04';
        $ohem_id = 1463;
        $resultado = $obj->n_eventos($entidad_empleado,$fecha,$this->link,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(2, $resultado);

        error::$en_error = false;


    }

    final public function test_tiene_eventos()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\event_tw();
        //$obj = new liberator($obj);
        $entidad_empleado = 'ohem';
        $fecha = '';
        $ohem_id = -1;
        $resultado = $obj->tiene_eventos($entidad_empleado,$fecha,$this->link,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);


        error::$en_error = false;


        $entidad_empleado = 'ohem';
        $fecha = '2023-06-08';
        $ohem_id = 18434;
        $resultado = $obj->tiene_eventos($entidad_empleado,$fecha,$this->link,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue( $resultado);
        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $entidad_empleado = 'empleado';
        $fecha = '2024-09-04';
        $ohem_id = 1463;
        $resultado = $obj->tiene_eventos($entidad_empleado,$fecha,$this->link,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue( $resultado);

        error::$en_error = false;


    }


}
