<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\data\_childrens;
use desarrollo_em3\manejo_datos\data\entidades\meta_gestor_detalle;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use desarrollo_em3\manejo_datos\sql\empleado;
use PDO;
use PHPUnit\Framework\TestCase;

class meta_gestor_detalle_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {

        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_recalcula_meta_gestor_detalle()
    {
        error::$en_error = false;
        $obj = new meta_gestor_detalle();
        //$obj = new liberator($obj);
        $campo_fecha_pago = 'DocDueDate';
        $campo_movimiento = 'U_Movto';
        $campo_total = 'DocTotal';
        $entidad_empleado = 'ohem';
        $fecha_fin = '2020-01-01';
        $fecha_inicio = '2020-01-01';
        $meta_gestor_id = 1;

        $resultado = $obj->recalcula_meta_gestor_detalle($campo_fecha_pago,$campo_movimiento,$campo_total,
            $entidad_empleado,$fecha_fin,$fecha_inicio,$this->link,$meta_gestor_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("UPDATE meta_gestor_detalle AS mgd LEFT JOIN meta_gestor_concentrado AS mgc ON mgc.id = mgd.meta_gestor_concentrado_id LEFT JOIN ( SELECT ohem_id,contrato_id, SUM(pago.DocTotal) AS aportado FROM pago WHERE pago.DocDueDate BETWEEN :fecha_inicio AND :fecha_fin AND pago.status = 'activo' AND pago.U_Movto = 'Abono' GROUP BY ohem_id,contrato_id ) AS pago ON mgd.contrato_id = pago.contrato_id AND mgc.ohem_id = pago.ohem_id SET mgd.monto_proceso = GREATEST(0, LEAST(mgd.monto_total, COALESCE(pago.aportado, 0))), mgd.monto_de_mas = GREATEST(0, COALESCE(pago.aportado, 0) - LEAST(mgd.monto_total, COALESCE(pago.aportado, 0))) WHERE mgc.meta_gestor_id = :meta_gestor_id",$resultado['sql']);

        error::$en_error = false;
    }




}
