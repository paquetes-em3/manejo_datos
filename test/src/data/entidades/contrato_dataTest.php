<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\data\entidades\contrato;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class contrato_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_empresa_id()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\contrato();
        $obj = new liberator($obj);
        $rs_plaza_l = array();
        $resultado = $obj->empresa_id($rs_plaza_l);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(-1,$resultado);


        error::$en_error = false;
        $rs_plaza_l = array();
        $rs_plaza_l[0] = new stdClass();
        $rs_plaza_l[0]->empresa_id = '4';
        $resultado = $obj->empresa_id($rs_plaza_l);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(4,$resultado);
        error::$en_error = false;
    }

    final public function test_empresa_id_by_serie()
    {
        error::$en_error = false;
        $obj = new contrato();
        $obj = new liberator($obj);

        $plaza_id = 1;
        $serie = 'A';
        $result = $obj->empresa_id_by_serie($this->link,$plaza_id,$serie);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsInt($result);
        $this->assertEquals(-1,$result);

        error::$en_error = false;

        $plaza_id = 23;
        $serie = 'CELC';
        $result = $obj->empresa_id_by_serie($this->link,$plaza_id,$serie);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsInt($result);
        $this->assertEquals(5,$result);
        error::$en_error = false;
    }

    final public function test_integra_empresa()
    {
        error::$en_error = false;
        $obj = new contrato();
        //$obj = new liberator($obj);

        $this->link->beginTransaction();
        $contrato_id = 1;
        $result = $obj->integra_empresa($contrato_id, $this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsInt($result);
        $this->assertEquals(5,$result);

        $contrato = (new consultas())->registro_bruto(new stdClass(),'contrato',1,$this->link,false);
        $this->assertEquals(5,$contrato->empresa_id);

        error::$en_error = false;


        $this->link->rollBack();
    }
    final public function test_limpia_datos_ubicacion()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\contrato();
        //$obj = new liberator($obj);
        $registro = array();
        $resultado = $obj->limpia_datos_ubicacion($registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertEmpty($resultado);


        error::$en_error = false;

        $registro = array();
        $registro['latitud'] = '$88.09';
        $resultado = $obj->limpia_datos_ubicacion($registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(88.09,$resultado['latitud']);
        error::$en_error = false;
    }


    final public function test_limpia_double()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\contrato();
        $obj = new liberator($obj);
        $key_limpiar = 'x';
        $registro = array();
        $resultado = $obj->limpia_double($key_limpiar,$registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0,$resultado['x']);


        error::$en_error = false;

        $key_limpiar = 'x';
        $registro = array();
        $registro['x'] = '$$000.111,99';
        $resultado = $obj->limpia_double($key_limpiar,$registro);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(000.11199,$resultado['x']);
        error::$en_error = false;
    }


    final public function test_obten_contratos_gestor() {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\contrato();
        //$obj = new liberator($obj);
        
        $estructura = 'SAP';
        $ohem_id = 5147;
        $status = 'activo';
        $resultado = $obj->obten_contratos_gestor($estructura,$this->link,$ohem_id,$status);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(7768,$resultado[0]->contrato_U_FolioCont);
        $this->assertEquals(9800,$resultado[0]->contrato_DocTotal);
        $this->assertEquals('2007-10-05',$resultado[0]->contrato_DocDate);
        $this->assertEquals('PLAN PREMIUM',$resultado[0]->contrato_U_ItemName);

        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $estructura = 'EM3';
        $ohem_id = 544;
        $status = 'activo';
        $resultado = $obj->obten_contratos_gestor($estructura,$this->link,$ohem_id,$status);

        $this->assertNotTrue(error::$en_error);

        $this->assertEquals(1,$resultado[0]->contrato_folio);
        $this->assertEquals(0.00,$resultado[0]->contrato_monto_total);
        $this->assertEquals('2008-01-30',$resultado[0]->contrato_fecha);
        $this->assertEquals('Mensual',$resultado[0]->periodicidad_pago_descripcion);    
    }

    final public function test_obten_contrato_excel() {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\contrato();
        //$obj = new liberator($obj);

        $campos_base = array('contrato.id','contrato.U_StatusCob','contrato.U_SeCont','contrato.U_FolioCont');
        $campo_serie = 'U_SeCont';
        $campo_folio = 'U_FolioCont';
        $contrato_id = 565312;
        $contrato = "";
        $resultado = $obj->obten_contrato_excel($campos_base,$campo_folio,$campo_serie,$this->link,
        $contrato,$contrato_id);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals("SELECT contrato.id AS contrato_id,contrato.U_StatusCob AS contrato_U_StatusCob,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont FROM contrato WHERE contrato.id = 565312",$resultado['sql']);

        error::$en_error = false;

        $campos = array('contrato.id','contrato.U_StatusCob','contrato.U_SeCont','contrato.U_FolioCont');
        $campo_serie = 'U_SeCont';
        $campo_folio = 'U_FolioCont';
        $contrato = "SALC18135";
        $contrato_id = 0;
        $limit = 1;
        $offset = 0;
        $plaza_id = 30;
        $resultado = $obj->obten_contrato_excel($campos_base,$campo_folio,$campo_serie,$this->link,
        $contrato,$contrato_id,$limit,$offset,$plaza_id);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals("SELECT contrato.id AS contrato_id,contrato.U_StatusCob AS contrato_U_StatusCob,contrato.U_SeCont AS contrato_U_SeCont,contrato.U_FolioCont AS contrato_U_FolioCont FROM contrato  LIMIT 1 OFFSET 0",$resultado['sql']);

        error::$en_error = false;
    }
}
