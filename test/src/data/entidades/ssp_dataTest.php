<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\data\_childrens;
use PDO;
use PHPUnit\Framework\TestCase;

class ssp_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_get_data_datatable()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\ssp();
        $conexion = (new conexion_db());
        $this->link = $conexion->link;
        $obj = new liberator($obj);

        $campos_base = array('contrato.id','contrato.CardName');
        $campos_extra = array('CONCAT(contrato.U_SeCont,contrato.U_FolioCont) AS contrato');
        $campos_like = array('contrato.id','contrato.CardName','CONCAT(contrato.U_SeCont,contrato.U_FolioCont)');
        $entidad = 'contrato';
        
        $condicion = array('contrato.status' => 'activo');
        $condicion_like = 'abigail';
        $filtro_rango = array();
        $join = array('LEFT JOIN plaza AS plaza ON plaza.id = contrato.plaza_id',
        'LEFT JOIN cliente AS cliente ON cliente.id = contrato.cliente_id');
        $limit = 10;
        $offset = 0;
        $order_by = 'CONCAT(contrato.U_SeCont,contrato.U_FolioCont) ASC';

        $resultado = $obj->get_data_datatable($campos_base,$campos_like,$entidad,$this->link,$campos_extra,
        $condicion,$condicion_like,$filtro_rango,$join,$limit,$offset,$order_by);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(36848, $resultado[0]->contrato_id);
        $this->assertEquals('CRUZ DIOSDADO ABIGAIL', $resultado[0]->contrato_CardName);
        $this->assertEquals('A1113', $resultado[0]->contrato);

        error::$en_error = false;
    }

    final public function test_service_side_processing()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\ssp();
        $conexion = (new conexion_db());
        $this->link = $conexion->link;
        //$obj = new liberator($obj);

        $filtros = ['wh_like' => 'abigail','order_by' => [0=>["column"=> "1","dir"=> "asc"]],
            'columns' => [
                0=>["data"=> "contrato_id","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                1=>["data"=> "CONCAT(contrato.U_SeCont,contrato.U_FolioCont)","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                2=>["data"=> "plaza_descripcion","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                3=>["data"=> "contrato_status","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                4=>["data"=> "acciones","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]]
        ]];
        
        $campos_base = array('contrato.id','contrato.CardName');
        $campos_extra = array('CONCAT(contrato.U_SeCont,contrato.U_FolioCont) AS contrato');
        $campos_like = array('contrato.id','contrato.CardName','CONCAT(contrato.U_SeCont,contrato.U_FolioCont)');
        $entidad = 'contrato';
        $join = array('LEFT JOIN plaza AS plaza ON plaza.id = contrato.plaza_id',
        'LEFT JOIN cliente AS cliente ON cliente.id = contrato.cliente_id');
        $condicion = array('contrato.status' => 'activo');
        $filtro_rango = array();
        $length = 30;
        $start = 0;

        $resultado = $obj->service_side_processing($campos_base,$campos_like,$entidad,$filtros,
        $this->link,$campos_extra,$condicion,$filtro_rango,$join,$length,$start);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(36848,$resultado[0]->contrato_id);
        $this->assertEquals('CRUZ DIOSDADO ABIGAIL',$resultado[0]->contrato_CardName);
        $this->assertEquals('A1113',$resultado[0]->contrato);

        error::$en_error = false;
    }
}