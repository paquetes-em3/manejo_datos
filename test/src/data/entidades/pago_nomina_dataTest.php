<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\data\_childrens;
use desarrollo_em3\manejo_datos\data\entidades\pago_nomina;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use desarrollo_em3\manejo_datos\sql\empleado;
use PHPUnit\Framework\TestCase;

class pago_nomina_dataTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }
    final public function test_campos_replace()
    {
        error::$en_error = false;
        $obj = new pago_nomina();
        //$obj = new liberator($obj);
        $resultado = $obj->campos_replace();
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject( $resultado);
        $this->assertEquals('pago_nomina', $resultado->banco_empresa_id->entidad);
        $this->assertEquals('banco_empresa_id', $resultado->banco_empresa_id->name_campo);

        $this->assertEquals('pago_nomina', $resultado->banco_empleado_id->entidad);
        $this->assertEquals('banco_empleado_id', $resultado->banco_empleado_id->name_campo);

        $this->assertEquals('banco_empresa', $resultado->banco_empresa_descripcion->entidad);
        $this->assertEquals('descripcion', $resultado->banco_empresa_descripcion->name_campo);

        $this->assertEquals('banco_empleado', $resultado->banco_empleado_descripcion->entidad);
        $this->assertEquals('descripcion', $resultado->banco_empleado_descripcion->name_campo);



        error::$en_error = false;
    }


    final public function test_params_replaces()
    {
        error::$en_error = false;
        $obj = new pago_nomina();
        //$obj = new liberator($obj);
        $resultado = $obj->params_replaces();
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertIsObject($resultado[0]);
        $this->assertIsObject($resultado[1]);
        $this->assertIsObject($resultado[2]);
        $this->assertEquals('cuenta_empleado',$resultado[0]->entidad);
        $this->assertEquals('pago_nomina',$resultado[0]->entidad_right);

        $this->assertEquals('banco',$resultado[1]->entidad);
        $this->assertEquals('pago_nomina',$resultado[1]->entidad_right);
        $this->assertEquals('banco_empresa',$resultado[1]->rename_entidad);

        $this->assertEquals('banco',$resultado[2]->entidad);
        $this->assertEquals('pago_nomina',$resultado[2]->entidad_right);
        $this->assertEquals('banco_empleado',$resultado[2]->rename_entidad);


        error::$en_error = false;
    }
    final public function test_columnas_filtro_empleado_admin()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        $obj = new liberator($obj);
        $entidad = 'empleado';
        $resultado = $obj->columnas_filtro_empleado_admin($entidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('empleado_id', $resultado[0]);
        $this->assertEquals('empleado_salario', $resultado[1]);
        $this->assertEquals('empleado_bono_gasolina', $resultado[2]);
        $this->assertEquals('empleado_pago_semanal_infonavit', $resultado[3]);
        $this->assertEquals('empleado_aplica_garantia_gestor', $resultado[4]);
        $this->assertEquals('empleado_salario_diario', $resultado[5]);
        $this->assertEquals('empleado_nombre_completo', $resultado[6]);
        $this->assertEquals('puesto_id', $resultado[7]);
        $this->assertEquals('puesto_descripcion', $resultado[8]);
        $this->assertEquals('plaza_id', $resultado[9]);
        $this->assertEquals('plaza_descripcion', $resultado[10]);
        $this->assertEquals('departamento_id', $resultado[11]);
        $this->assertEquals('departamento_descripcion', $resultado[12]);
        $this->assertEquals('puesto_dias_descanso', $resultado[13]);

        error::$en_error = false;

        $entidad = 'ohem';
        $campos_add[] = 'ohem_pension_alimenticia';
        $campos_add[] = 'ohem_fonacot';
        $resultado = $obj->columnas_filtro_empleado_admin($entidad,$campos_add);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('ohem_id', $resultado[0]);
        $this->assertEquals('ohem_salario', $resultado[1]);
        $this->assertEquals('ohem_bono_gasolina', $resultado[2]);
        $this->assertEquals('ohem_pago_semanal_infonavit', $resultado[3]);
        $this->assertEquals('ohem_aplica_garantia_gestor', $resultado[4]);
        $this->assertEquals('ohem_salario_diario', $resultado[5]);
        $this->assertEquals('ohem_nombre_completo', $resultado[6]);
        $this->assertEquals('puesto_id', $resultado[7]);
        $this->assertEquals('puesto_descripcion', $resultado[8]);
        $this->assertEquals('plaza_id', $resultado[9]);
        $this->assertEquals('plaza_descripcion', $resultado[10]);
        $this->assertEquals('departamento_id', $resultado[11]);
        $this->assertEquals('departamento_descripcion', $resultado[12]);
        $this->assertEquals('puesto_dias_descanso', $resultado[13]);
        $this->assertEquals('ohem_pension_alimenticia', $resultado[14]);
        $this->assertEquals('ohem_fonacot', $resultado[15]);

        error::$en_error = false;
    }

    final public function test_filtro_empleado_admin()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        $obj = new liberator($obj);
        $departamento_id = -1;
        $plaza_id = 1;
        $entidad = 'empleado';
        $resultado = $obj->filtro_empleado_admin($departamento_id, $plaza_id,$entidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1, $resultado['plaza.id']);
        $this->assertEquals('activo', $resultado['empleado.status']);
        $this->assertEquals('activo', $resultado['empleado.es_administrativo']);

        error::$en_error = false;
        $departamento_id = 10;
        $plaza_id = 1;
        $entidad = 'ohem';
        $resultado = $obj->filtro_empleado_admin($departamento_id, $plaza_id,$entidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1, $resultado['plaza.id']);
        $this->assertEquals('activo', $resultado['ohem.status']);
        $this->assertEquals('activo', $resultado['ohem.es_administrativo']);
        $this->assertEquals(10, $resultado['departamento.id']);

        error::$en_error = false;
    }

    final public function test_filtro_especial_dias_prueba()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        $obj = new liberator($obj);
        $sql_plazas_in = 'DATOS IN';
        $n_dias_prueba = 1;
        $fecha = '2020-01-01';
        $campo_fecha_inicio = 'startDate';
        $entidad = 'ohem';
        $resultado = $obj->filtro_especial_dias_prueba($campo_fecha_inicio,$entidad,$fecha,$n_dias_prueba,$sql_plazas_in);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('=', $resultado["DATEDIFF('2020-01-01', ohem.startDate)"]['operador']);
        $this->assertEquals(1, $resultado["DATEDIFF('2020-01-01', ohem.startDate)"]['valor']);

        error::$en_error = false;
    }

    final public function test_get_filtro_especial_dias_prueba()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        $obj = new liberator($obj);

        $n_dias_prueba = 10;
        $fecha = '2020-01-01';
        $campo_fecha_inicio = 'startDate';
        $entidad = 'ohem';
        $plazas_id = array('1',2);
        $resultado = $obj->get_filtro_especial_dias_prueba($campo_fecha_inicio,$entidad,$fecha,$n_dias_prueba,$plazas_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('=', $resultado["DATEDIFF('2020-01-01', ohem.startDate)"]['operador']);
        $this->assertEquals(10, $resultado["DATEDIFF('2020-01-01', ohem.startDate)"]['valor']);
        $this->assertEquals("IN( 1,2 )", $resultado["plaza.id"]['operador']);

        error::$en_error = false;
    }

    final public function test_params_filtro_dias_prueba()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        //$obj = new liberator($obj);

        $n_dias_prueba = 1;
        $fecha = '2020-01-01';
        $campo_fecha_inicio = 'startDate';
        $entidad = 'ohem';
        $plazas_id = array(3,5,7);
        $resultado = $obj->params_filtro_dias_prueba($campo_fecha_inicio,$entidad,$fecha,$n_dias_prueba,$plazas_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('=', $resultado->filtro_especial["DATEDIFF('2020-01-01', ohem.startDate)"]['operador']);
        $this->assertEquals(1, $resultado->filtro_especial["DATEDIFF('2020-01-01', ohem.startDate)"]['valor']);


        error::$en_error = false;
    }

    final public function test_params_filtro_empleado_adm()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        //$obj = new liberator($obj);
        $departamento_id = -1;
        $plaza_id = 1;
        $entidad = 'empleado';
        $campo_fecha = 'fecha_ingreso';
        $campos_add = array();
        $fecha_inicio_menor_a = '2020-01-01';
        $resultado = $obj->params_filtro_empleado_adm($campo_fecha,$campos_add,$departamento_id, $entidad,$fecha_inicio_menor_a,$plaza_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1, $resultado->filtro['plaza.id']);
        $this->assertEquals('activo', $resultado->filtro['empleado.status']);
        $this->assertEquals('activo', $resultado->filtro['empleado.es_administrativo']);
        $this->assertCount(14, $resultado->columnas_basicas);
        $this->assertEquals(" AND empleado.fecha_ingreso <= '2020-01-01'", $resultado->sql_where_string);


        error::$en_error = false;

        $departamento_id = -1;
        $plaza_id = 1;
        $entidad = 'ohem';
        $campo_fecha = 'fecha_ingreso';
        $campos_add = array('ohem_pension_alimenticia','ohem_fonacot');
        $fecha_inicio_menor_a = '2020-01-01';
        $resultado = $obj->params_filtro_empleado_adm($campo_fecha,$campos_add,$departamento_id, $entidad,$fecha_inicio_menor_a,$plaza_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1, $resultado->filtro['plaza.id']);
        $this->assertEquals('activo', $resultado->filtro['ohem.status']);
        $this->assertEquals('activo', $resultado->filtro['ohem.es_administrativo']);
        $this->assertCount(16, $resultado->columnas_basicas);
        $this->assertEquals(" AND ohem.fecha_ingreso <= '2020-01-01'", $resultado->sql_where_string);
        error::$en_error = false;


    }



}
