<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\data\entidades\pago;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class pago_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_datos_transferencia()
    {
        error::$en_error = false;
        $conexion = (new conexion_db_monterrey());
        $this->link = $conexion->link;
        $obj = new pago();
        //$obj = new liberator($obj);
        $registro = array();
        $resultado = $obj->datos_transferencia($registro);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject( $resultado);
        $this->assertEquals('', $resultado->referencia);
        $this->assertEquals(-1, $resultado->rel_serie_id);

        error::$en_error = false;
    }

    final public function test_estructura()
    {
        error::$en_error = false;
        $conexion = (new conexion_db_monterrey());
        $this->link = $conexion->link;
        $obj = new pago();
        $obj = new liberator($obj);
        $tipo = 'EM3';
        $resultado = $obj->estructura($tipo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject( $resultado);
        $this->assertEquals('folio', $resultado->campo_folio);
        $this->assertEquals('nombre_completo', $resultado->campo_nombre_cliente);

        error::$en_error = false;
    }

    final public function test_pago()
    {
        error::$en_error = false;
        $conexion = (new conexion_db_monterrey());
        $this->link = $conexion->link;
        $obj = new pago();
        $pago_id = 4403405;
        $resultado = $obj->pago($this->link,$pago_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject( $resultado);
        $this->assertEquals('2024-07-22', $resultado->DocDate);

        error::$en_error = false;
    }
    final public function test_pago_corte_entrega_faltante()
    {
        error::$en_error = false;
        $conexion = (new conexion_db_monterrey());
        $this->link = $conexion->link;
        $obj = new pago();
        //$obj = new liberator($obj);
        $campo_monto = 'DocTotal';
        $campo_serie = 'U_SeCont';
        $pago_corte_id = 24451;
        $resultado = $obj->pago_corte_entrega_faltante($campo_monto,$campo_serie,$pago_corte_id,$this->link);
        //print_r($resultado);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);

        error::$en_error = false;
    }
    final public function test_pago_corte_id()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);
        $pago_id = 4925401;
        $resultado = $obj->pago_corte_id($this->link,$pago_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject( $resultado);
        $this->assertEquals(4925401, $resultado->id);
        $this->assertEquals(15968, $resultado->pago_corte_id);

        error::$en_error = false;
    }

    final public function test_pago_monto()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);
        $pago_id = 1;
        $resultado = $obj->pago_monto($this->link,$pago_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject( $resultado);
        $this->assertEquals(1, $resultado->pago_id);
        $this->assertEquals(100, $resultado->pago_DocTotal);

        error::$en_error = false;
    }
    final public function test_pagos_por_corte()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);
        $pago_corte_id = 29819;
        $campos_extra = array();
        $joins = array();
        $resultado = $obj->pagos_por_corte('Canceled','DocTotal','U_Movto',$this->link,$pago_corte_id,$campos_extra,$joins);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertEquals(6040324, $resultado[0]->id);
        $this->assertEquals(100, $resultado[0]->DocTotal);

        error::$en_error = false;

        $pago_corte_id = 29819;
        $campos_extra = array();
        $joins = array();
        $campos_extra[] = 'contrato.U_SeCont';
        $joins[0]['entidad_left'] = 'contrato';
        $joins[0]['entidad_right'] = 'pago';
        $resultado = $obj->pagos_por_corte('Canceled','DocTotal','U_Movto',$this->link,$pago_corte_id,$campos_extra,$joins);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertEquals(6040324, $resultado[0]->pago_id);
        $this->assertEquals(100, $resultado[0]->pago_DocTotal);
        $this->assertEquals('SALC', $resultado[0]->contrato_U_SeCont);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $pago_corte_id = 3856;
        $campos_extra = array();
        $joins = array();
        $campos_extra[] = 'contrato.serie';
        $joins[0]['entidad_left'] = 'contrato';
        $joins[0]['entidad_right'] = 'pago';
        $resultado = $obj->pagos_por_corte('','monto','movimiento',$this->link,$pago_corte_id,$campos_extra,$joins);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertEquals(1101405, $resultado[0]->pago_id);
        $this->assertEquals(200, $resultado[0]->pago_monto);
        $this->assertEquals('MANC', $resultado[0]->contrato_serie);

        error::$en_error = false;
    }

    final public function test_pagos_por_corte_con_deposito()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);
        $pago_corte_id = 116687;
        $resultado = $obj->pagos_por_corte_con_deposito($this->link,$pago_corte_id,'sAp');

        $end = (end($resultado));

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertEquals(0, $end['pago_DocTotal']);
        $this->assertEquals(500, $resultado[0]['pago_DocTotal']);

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $pago_corte_id = 3806;
        $resultado = $obj->pagos_por_corte_con_deposito($this->link,$pago_corte_id,'eM3');
        //print_r($resultado);exit;
        $end = (end($resultado));

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertEquals(1098325, $end['pago_id']);
        $this->assertEquals(2556, $resultado[0]['contrato_folio']);
        error::$en_error = false;


    }


    final public function test_pagos_por_id()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);
        $key_empleado_id = 'ohem_id';
        $limit = '7';
        $pago_id = 1;
        $resultado = $obj->pagos_por_id($key_empleado_id, $limit, $this->link, $pago_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertCount(8, $resultado);

        error::$en_error = false;
    }

    final public function test_result_base()
    {
        error::$en_error = false;
        $obj = new pago();
        $obj = new liberator($obj);
        $rows = new stdClass();
        $rows->rows_unicos = array();
        $rows->rows_like = array();
        $resultado = $obj->result_base($rows);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);

        error::$en_error = false;
    }

    final public function test_result_by_id()
    {
        error::$en_error = false;
        $obj = new pago();
        $obj = new liberator($obj);
        $key_empleado_id = 'ohem_id';
        $limit = '3';
        $pago_id = 1;
        $resultado = $obj->result_by_id($key_empleado_id, $limit, $this->link, $pago_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertCount(4, $resultado);

        error::$en_error = false;
    }

    final public function test_rows_by_id()
    {
        error::$en_error = false;
        $obj = new pago();
        $obj = new liberator($obj);
        $key_empleado_id = 'ohem_id';
        $limit = '111';
        $pago_id = 1;
        $resultado = $obj->rows_by_id($key_empleado_id,$limit,$this->link,$pago_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);

        error::$en_error = false;
    }

    final public function test_rows_by_id_like()
    {
        error::$en_error = false;
        $obj = new pago();
        $obj = new liberator($obj);
        $key_empleado_id = 'ohem_id';
        $limit = '1';
        $pago_id = 1;
        $resultado = $obj->rows_by_id_like($key_empleado_id,$limit,$this->link,$pago_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);

        error::$en_error = false;
    }

    final public function test_rows_id()
    {
        error::$en_error = false;
        $obj = new pago();
        $obj = new liberator($obj);
        $key_empleado_id = 'ohem_id';
        $limit = '5';
        $pago_id = 1;
        $resultado = $obj->rows_id($key_empleado_id,$limit,$this->link,$pago_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject( $resultado);
        $this->assertCount(5, $resultado->rows_like);

        error::$en_error = false;
    }

    final public function test_sum_pagos_por_corte()
    {
        error::$en_error = false;
        $obj = new pago();
        //$obj = new liberator($obj);
        $pago_corte_id = 15968;
        $resultado = $obj->sum_pagos_por_corte('Canceled','DocTotal','U_Movto',
            $this->link,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat( $resultado);
        $this->assertEquals(6500, $resultado);

        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $pago_corte_id = 3461;
        $resultado = $obj->sum_pagos_por_corte('','monto','movimiento',
            $this->link,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat( $resultado);
        $this->assertEquals(11460, $resultado);

        error::$en_error = false;


    }

}
