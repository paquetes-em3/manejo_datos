<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\data\_childrens;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use desarrollo_em3\manejo_datos\sql\empleado;
use PDO;
use PHPUnit\Framework\TestCase;

class empleado_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_columnas_filtro_dias_prueba()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        $obj = new liberator($obj);
        $entidad = 'ohem';
        $campo_fecha_inicio = 'startDate';
        $resultado = $obj->columnas_filtro_dias_prueba($campo_fecha_inicio, $entidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('ohem_id', $resultado[0]);
        $this->assertEquals('ohem_nombre_completo', $resultado[1]);
        $this->assertEquals('ohem_startDate', $resultado[2]);
        $this->assertEquals('departamento_id', $resultado[3]);
        $this->assertEquals('puesto_id', $resultado[4]);


        error::$en_error = false;
    }
    final public function test_columnas_filtro_empleado_admin()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        $obj = new liberator($obj);
        $entidad = 'empleado';
        $resultado = $obj->columnas_filtro_empleado_admin($entidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('empleado_id', $resultado[0]);
        $this->assertEquals('empleado_salario', $resultado[1]);
        $this->assertEquals('empleado_bono_gasolina', $resultado[2]);
        $this->assertEquals('empleado_pago_semanal_infonavit', $resultado[3]);
        $this->assertEquals('empleado_aplica_garantia_gestor', $resultado[4]);
        $this->assertEquals('empleado_salario_diario', $resultado[5]);
        $this->assertEquals('empleado_nombre_completo', $resultado[6]);
        $this->assertEquals('puesto_id', $resultado[7]);
        $this->assertEquals('puesto_descripcion', $resultado[8]);
        $this->assertEquals('plaza_id', $resultado[9]);
        $this->assertEquals('plaza_descripcion', $resultado[10]);
        $this->assertEquals('departamento_id', $resultado[11]);
        $this->assertEquals('departamento_descripcion', $resultado[12]);
        $this->assertEquals('puesto_dias_descanso', $resultado[13]);

        error::$en_error = false;

        $entidad = 'ohem';
        $campos_add[] = 'ohem_pension_alimenticia';
        $campos_add[] = 'ohem_fonacot';
        $resultado = $obj->columnas_filtro_empleado_admin($entidad,$campos_add);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('ohem_id', $resultado[0]);
        $this->assertEquals('ohem_salario', $resultado[1]);
        $this->assertEquals('ohem_bono_gasolina', $resultado[2]);
        $this->assertEquals('ohem_pago_semanal_infonavit', $resultado[3]);
        $this->assertEquals('ohem_aplica_garantia_gestor', $resultado[4]);
        $this->assertEquals('ohem_salario_diario', $resultado[5]);
        $this->assertEquals('ohem_nombre_completo', $resultado[6]);
        $this->assertEquals('puesto_id', $resultado[7]);
        $this->assertEquals('puesto_descripcion', $resultado[8]);
        $this->assertEquals('plaza_id', $resultado[9]);
        $this->assertEquals('plaza_descripcion', $resultado[10]);
        $this->assertEquals('departamento_id', $resultado[11]);
        $this->assertEquals('departamento_descripcion', $resultado[12]);
        $this->assertEquals('puesto_dias_descanso', $resultado[13]);
        $this->assertEquals('ohem_pension_alimenticia', $resultado[14]);
        $this->assertEquals('ohem_fonacot', $resultado[15]);

        error::$en_error = false;
    }

    final public function test_filtro_empleado_admin()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        $obj = new liberator($obj);
        $departamento_id = -1;
        $plaza_id = 1;
        $entidad = 'empleado';
        $resultado = $obj->filtro_empleado_admin($departamento_id, $plaza_id,$entidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1, $resultado['plaza.id']);
        $this->assertEquals('activo', $resultado['empleado.status']);
        $this->assertEquals('activo', $resultado['empleado.es_administrativo']);

        error::$en_error = false;
        $departamento_id = 10;
        $plaza_id = 1;
        $entidad = 'ohem';
        $resultado = $obj->filtro_empleado_admin($departamento_id, $plaza_id,$entidad);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1, $resultado['plaza.id']);
        $this->assertEquals('activo', $resultado['ohem.status']);
        $this->assertEquals('activo', $resultado['ohem.es_administrativo']);
        $this->assertEquals(10, $resultado['departamento.id']);

        error::$en_error = false;
    }

    final public function test_filtro_especial_dias_prueba()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        $obj = new liberator($obj);
        $sql_plazas_in = 'DATOS IN';
        $n_dias_prueba = 1;
        $fecha = '2020-01-01';
        $campo_fecha_inicio = 'startDate';
        $entidad = 'ohem';
        $resultado = $obj->filtro_especial_dias_prueba($campo_fecha_inicio,$entidad,$fecha,$n_dias_prueba,$sql_plazas_in);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('=', $resultado["DATEDIFF('2020-01-01', ohem.startDate)"]['operador']);
        $this->assertEquals(1, $resultado["DATEDIFF('2020-01-01', ohem.startDate)"]['valor']);

        error::$en_error = false;
    }
    final public function test_get_empleado_datatable()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        $conexion = (new conexion_db_monterrey());
        $this->link = $conexion->link;
        $obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $limit = 10;
        $offset = 0;
        $order_by = 'ohem_nombre_completo ASC';
        $plaza_id = '28';
        $status = 'activo';
        $wh_like = 'abigail';
        $resultado = $obj->get_empleado_datatable($entidad_empleado,$this->link,$plaza_id,$limit,$offset ,$order_by,
        $status,$wh_like);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(17386, $resultado[0]->ohem_id);
        $this->assertEquals('BLANCA ABIGAIL COSTILLA GUTIERREZ', $resultado[0]->ohem_nombre_completo);
        $this->assertEquals('activo', $resultado[0]->ohem_status);
        $this->assertEquals('Monterrey', $resultado[0]->plaza_descripcion);

        error::$en_error = false;


        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $entidad_empleado = 'empleado';
        $limit = 10;
        $offset = 0;
        $order_by = '';
        $plaza_id = '1';
        $status = 'activo';
        $wh_like = 'abigail';
        $resultado = $obj->get_empleado_datatable($entidad_empleado,$this->link,$plaza_id,$limit,$offset ,$order_by,
        $status,$wh_like);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1411, $resultado[0]->empleado_id);
        $this->assertEquals('MIRIAM ABIGAIL AGUILAR ALVARADO (IRA)', $resultado[0]->empleado_nombre_completo);
        $this->assertEquals('activo', $resultado[0]->empleado_status);
        $this->assertEquals('Manzanillo', $resultado[0]->plaza_descripcion);

        error::$en_error = false;
    }
    final public function test_get_filtro_especial_dias_prueba()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        $obj = new liberator($obj);

        $n_dias_prueba = 10;
        $fecha = '2020-01-01';
        $campo_fecha_inicio = 'startDate';
        $entidad = 'ohem';
        $plazas_id = array('1',2);
        $resultado = $obj->get_filtro_especial_dias_prueba($campo_fecha_inicio,$entidad,$fecha,$n_dias_prueba,$plazas_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('=', $resultado["DATEDIFF('2020-01-01', ohem.startDate)"]['operador']);
        $this->assertEquals(10, $resultado["DATEDIFF('2020-01-01', ohem.startDate)"]['valor']);
        $this->assertEquals("IN( 1,2 )", $resultado["plaza.id"]['operador']);

        error::$en_error = false;
    }

    final public function test_params_filtro_dias_prueba()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        //$obj = new liberator($obj);

        $n_dias_prueba = 1;
        $fecha = '2020-01-01';
        $campo_fecha_inicio = 'startDate';
        $entidad = 'ohem';
        $plazas_id = array(3,5,7);
        $resultado = $obj->params_filtro_dias_prueba($campo_fecha_inicio,$entidad,$fecha,$n_dias_prueba,$plazas_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('=', $resultado->filtro_especial["DATEDIFF('2020-01-01', ohem.startDate)"]['operador']);
        $this->assertEquals(1, $resultado->filtro_especial["DATEDIFF('2020-01-01', ohem.startDate)"]['valor']);


        error::$en_error = false;
    }

    final public function test_params_filtro_empleado_adm()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        //$obj = new liberator($obj);
        $departamento_id = -1;
        $plaza_id = 1;
        $entidad = 'empleado';
        $campo_fecha = 'fecha_ingreso';
        $campos_add = array();
        $fecha_inicio_menor_a = '2020-01-01';
        $resultado = $obj->params_filtro_empleado_adm($campo_fecha,$campos_add,$departamento_id, $entidad,$fecha_inicio_menor_a,$plaza_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1, $resultado->filtro['plaza.id']);
        $this->assertEquals('activo', $resultado->filtro['empleado.status']);
        $this->assertEquals('activo', $resultado->filtro['empleado.es_administrativo']);
        $this->assertCount(14, $resultado->columnas_basicas);
        $this->assertEquals(" AND empleado.fecha_ingreso <= '2020-01-01'", $resultado->sql_where_string);


        error::$en_error = false;

        $departamento_id = -1;
        $plaza_id = 1;
        $entidad = 'ohem';
        $campo_fecha = 'fecha_ingreso';
        $campos_add = array('ohem_pension_alimenticia','ohem_fonacot');
        $fecha_inicio_menor_a = '2020-01-01';
        $resultado = $obj->params_filtro_empleado_adm($campo_fecha,$campos_add,$departamento_id, $entidad,$fecha_inicio_menor_a,$plaza_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1, $resultado->filtro['plaza.id']);
        $this->assertEquals('activo', $resultado->filtro['ohem.status']);
        $this->assertEquals('activo', $resultado->filtro['ohem.es_administrativo']);
        $this->assertCount(16, $resultado->columnas_basicas);
        $this->assertEquals(" AND ohem.fecha_ingreso <= '2020-01-01'", $resultado->sql_where_string);
        error::$en_error = false;


    }

    final public function test_genera_sentencia_order()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        $conexion = (new conexion_db_monterrey());
        $this->link = $conexion->link;
        $obj = new liberator($obj);

        $order = [0=>["column"=> "1","dir"=> "asc"]];
        $columns = [
                0=>["data"=> "ohem_id","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                1=>["data"=> "ohem_nombre_completo","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                2=>["data"=> "plaza_descripcion","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                3=>["data"=> "ohem_status","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                4=>["data"=> "acciones","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]]
        ];
        $resultado = $obj->genera_sentencia_order($order,$columns);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($resultado);
        $this->assertEquals('ohem_nombre_completo asc',$resultado);

        error::$en_error = false;

        $order = [];
        $columns = [
                0=>["data"=> "ohem_id","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                1=>["data"=> "ohem_nombre_completo","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                2=>["data"=> "plaza_descripcion","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                3=>["data"=> "ohem_status","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                4=>["data"=> "acciones","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]]
        ];
        $resultado = $obj->genera_sentencia_order($order,$columns);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($resultado);
        $this->assertEquals('',$resultado);

        error::$en_error = false;
    }

    final public function test_ssp()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empleado();
        $conexion = (new conexion_db_monterrey());
        $this->link = $conexion->link;
        //$obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $length = 25;
        $start = 0;
        $filtros = ['wh_like' => 'abigail','order_by' => [0=>["column"=> "1","dir"=> "asc"]],
            'columns' => [
                0=>["data"=> "ohem_id","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                1=>["data"=> "ohem_nombre_completo","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                2=>["data"=> "plaza_descripcion","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                3=>["data"=> "ohem_status","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]],
                4=>["data"=> "acciones","name"=> "","searchable"=> "true","orderable"=> "true","search"=> ["value"=> "","regex"=> "false"]]
        ]];
        $plaza_id = '28';
        $status = 'activo';
        $resultado = $obj->ssp($entidad_empleado,$filtros,$this->link,$plaza_id,$length,$start,$status);
        
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(17386,$resultado[0]->ohem_id);
        $this->assertEquals('BLANCA ABIGAIL COSTILLA GUTIERREZ',$resultado[0]->ohem_nombre_completo);
        $this->assertEquals('activo',$resultado[0]->ohem_status);
        $this->assertEquals('Monterrey',$resultado[0]->plaza_descripcion);

        error::$en_error = false;
    }

}
