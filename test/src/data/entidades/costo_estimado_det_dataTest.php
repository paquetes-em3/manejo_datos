<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\data\entidades\costo_estimado_det;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class costo_estimado_det_dataTest extends TestCase
{

    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;

        $conexion = (new conexion_db());
        $this->link = $conexion->link;




    }

    final public function test_concepto_ins()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\costo_estimado_det();
        //$obj = new liberator($obj);

        $datos_fc = new stdClass();
        $datos_fc->descripcion = 'DESCRIPCION';
        $factura_id = -1;
        $montos = new stdClass();
        $montos->precio_unitario = 10;
        $montos->iva = 0;
        $resultado = $obj->concepto_ins($datos_fc,$factura_id,$montos);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(10,$resultado['precio_unitario']);


        error::$en_error = false;


    }

    final public function test_contratos_genera()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\costo_estimado_det();
        $obj = new liberator($obj);

        $contratos_genera = array();
        $evento = '';
        $resultado = $obj->contratos_genera($contratos_genera,$evento);

        $this->assertNotTrue(error::$en_error);
        $this->assertEmpty($resultado);

        error::$en_error = false;

        $contratos_genera = array();
        $evento = 'genera';
        $resultado = $obj->contratos_genera($contratos_genera,$evento);
        $this->assertNotTrue(error::$en_error);
        $this->assertEmpty($resultado);


        error::$en_error = false;

        $contratos_genera = array();
        $evento = 'genera';
        $_GET['contrato_id'] = 'x';
        $resultado = $obj->contratos_genera($contratos_genera,$evento);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('x',$resultado[0]);
        error::$en_error = false;


    }

    final public function test_contratos_genera_init()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\costo_estimado_det();
        $obj = new liberator($obj);

        $resultado = $obj->contratos_genera_init();

        $this->assertNotTrue(error::$en_error);
        $this->assertEmpty($resultado);

        error::$en_error = false;

        $_POST['contratos_genera'] = array();
        $resultado = $obj->contratos_genera_init();
        $this->assertNotTrue(error::$en_error);
        $this->assertEmpty($resultado);


        error::$en_error = false;

        $_POST['contratos_genera'] = array('x');
        $resultado = $obj->contratos_genera_init();

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('x',$resultado[0]);

        error::$en_error = false;


    }

    final public function test_contratos_timbra()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\costo_estimado_det();
        $obj = new liberator($obj);

        $contratos_timbra = array();
        $evento = '';
        $resultado = $obj->contratos_timbra($contratos_timbra,$evento);

        $this->assertNotTrue(error::$en_error);
        $this->assertEmpty($resultado);

        error::$en_error = false;
        $_POST = ARRAY();
        $_GET = ARRAY();
        $contratos_timbra = array();
        $evento = 'timbra';
        $resultado = $obj->contratos_timbra($contratos_timbra,$evento);
        $this->assertNotTrue(error::$en_error);
        $this->assertEmpty($resultado);


        error::$en_error = false;

        $contratos_timbra = array();
        $evento = 'timbra';
        $_GET['contrato_id'] = 'x';
        $resultado = $obj->contratos_timbra($contratos_timbra,$evento);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('x',$resultado[0]);
        error::$en_error = false;


    }

    final public function test_contratos_timbra_init()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\costo_estimado_det();
        $obj = new liberator($obj);

        $resultado = $obj->contratos_timbra_init();

        $this->assertNotTrue(error::$en_error);
        $this->assertEmpty($resultado);

        error::$en_error = false;

        $_POST['contratos_timbra'] = array();
        $resultado = $obj->contratos_timbra_init();
        $this->assertNotTrue(error::$en_error);
        $this->assertEmpty($resultado);


        error::$en_error = false;

        $_POST['contratos_timbra'] = array('x');
        $resultado = $obj->contratos_timbra_init();

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('x',$resultado[0]);

        error::$en_error = false;


    }

    final public function test_contratos_transaccion()
    {
        error::$en_error = false;
        $obj = new costo_estimado_det();
        $obj = new liberator($obj);

        $contratos_genera = array();
        $contratos_timbra = array();

        $_GET['contrato_id'] = 1;
        $_GET['evento'] = 'timbra';
        $result = $obj->contratos_transaccion($contratos_genera, $contratos_timbra);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(1,$result->contratos_timbra[0]);

        error::$en_error = false;


    }

    final public function test_contratos_unicos()
    {
        error::$en_error = false;
        $obj = new costo_estimado_det();
        $obj = new liberator($obj);

        $contratos_genera = array();
        $contratos_timbra = array();

        $_GET['contrato_id'] = 1;
        $_GET['evento'] = 'timbra';
        $_POST['contratos_genera'] = array('x');
        $result = $obj->contratos_unicos($contratos_genera, $contratos_timbra);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);

        error::$en_error = false;


    }

    final public function test_cp_empresa()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\costo_estimado_det();
        $obj = new liberator($obj);
        $contrato = new stdClass();
        $empresa = new stdClass();
        $empresa->empresa_id = 73;
        $contrato->plaza_id = 27;
        $resultado = $obj->cp_empresa($contrato, $empresa, $this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(37266, $resultado->codigo_postal);

        error::$en_error = false;



    }

    final public function test_datos_contrato()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\costo_estimado_det();
        $obj = new liberator($obj);
        $contrato_id = 1;
        $resultado = $obj->datos_contrato($contrato_id,$this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject( $resultado);
        $this->assertIsObject( $resultado->contrato);
        $this->assertIsObject( $resultado->empresa);

        error::$en_error = false;



    }

    final public function test_evento()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\costo_estimado_det();
        $obj = new liberator($obj);

        UNSET($_GET['evento']);
        $resultado = $obj->evento();


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($resultado);
        $this->assertEquals('',$resultado);

        error::$en_error = false;
        $_GET['evento'] = 'x';
        $resultado = $obj->evento();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($resultado);
        $this->assertEquals('x',$resultado);

        error::$en_error = false;



    }

    final public function test_registro_new_genera()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\costo_estimado_det();
        //$obj = new liberator($obj);
        $conf_costo_estimado_id = -1;
        $porcentaje_iva = 0;
        $row_en_proceso = array();
        $row_en_proceso['pago_id'] = 1;
        $resultado = $obj->registro_new_genera($conf_costo_estimado_id,$this->link,$porcentaje_iva, $row_en_proceso);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(2020, $resultado->registro_new['year']);
        $this->assertNotTrue( $resultado->existe);

        error::$en_error = false;

        $conf_costo_estimado_id = 14;
        $porcentaje_iva = .16;
        $row_en_proceso = array();
        $row_en_proceso['pago_id'] = 1;
        $resultado = $obj->registro_new_genera($conf_costo_estimado_id,$this->link,$porcentaje_iva, $row_en_proceso);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(100.77, $resultado->registro_new['costo']);
        $this->assertNotTrue( $resultado->existe);

        error::$en_error = false;



    }





}
