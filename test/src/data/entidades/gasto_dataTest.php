<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use PHPUnit\Framework\TestCase;

class gasto_dataTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }
    final public function test_columnas_basicas()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\gasto();
        $obj = new liberator($obj);
        $columna = 'a';
        $resultado = $obj->columnas_basicas($columna);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);

        error::$en_error = false;

    }

    final public function test_get_columnas_basicas()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\gasto();
        $obj = new liberator($obj);
        $columna = 'x';
        $columnas_cascada = array();
        $validacion_cascada = array();
        $resultado = $obj->get_columnas_basicas($columna,$columnas_cascada,$validacion_cascada);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('gasto_id',$resultado[0]);
        $this->assertEquals('gasto_x',$resultado[1]);
        $this->assertEquals('unidad_negocio_id',$resultado[2]);
        $this->assertEquals('forma_pago_id',$resultado[3]);

        error::$en_error = false;

        $columna = 'y';
        $columnas_cascada = array();
        $validacion_cascada = array();
        $columnas_cascada[] = '';
        $resultado = $obj->get_columnas_basicas($columna,$columnas_cascada,$validacion_cascada);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('gasto_id',$resultado[0]);
        $this->assertEquals('gasto_y',$resultado[1]);
        $this->assertEquals('unidad_negocio_id',$resultado[2]);
        $this->assertEquals('forma_pago_id',$resultado[3]);

        error::$en_error = false;

        $columna = 'y';
        $columnas_cascada = array();
        $validacion_cascada = array();
        $columnas_cascada[] = '';
        $validacion_cascada[] = '';
        $resultado = $obj->get_columnas_basicas($columna,$columnas_cascada,$validacion_cascada);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('gasto_id',$resultado[0]);
        $this->assertEquals('gasto_y',$resultado[1]);
        $this->assertEquals('unidad_negocio_id',$resultado[2]);
        $this->assertEquals('forma_pago_id',$resultado[3]);

        error::$en_error = false;

        $columna = 'y';
        $columnas_cascada = array();
        $validacion_cascada = array();
        $columnas_cascada[] = 'xx';
        $validacion_cascada['y'] = 'x';
        $resultado = $obj->get_columnas_basicas($columna,$columnas_cascada,$validacion_cascada);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('gasto_id',$resultado[0]);
        $this->assertEquals('gasto_y',$resultado[1]);
        $this->assertEquals('unidad_negocio_id',$resultado[2]);
        $this->assertEquals('forma_pago_id',$resultado[3]);
        $this->assertEquals('xx',$resultado[4]);
        error::$en_error = false;
    }





}
