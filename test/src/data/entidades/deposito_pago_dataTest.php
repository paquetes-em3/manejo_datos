<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class deposito_pago_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_deposito_pago_ins()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\deposito_pago();
        $obj = new liberator($obj);

        $this->link->beginTransaction();

        $pago = array();
        $_POST['pago_id'] = 1;
        $_POST['deposito_aportaciones_id'] = 1;
        $pago['DocTotal'] = 0;
        $resultado = $obj->deposito_pago_ins('DocTotal',$pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertEquals(1, $resultado['pago_id']);
        $this->assertEquals(1, $resultado['deposito_aportaciones_id']);
        $this->assertEquals(0, $resultado['monto_depositado']);

        error::$en_error = false;

        $this->link->rollBack();

    }

    final public function test_deposito_pago_ins_base()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\deposito_pago();
        //$obj = new liberator($obj);

        $this->link->beginTransaction();

        $deposito_aportaciones_id = 1;
        $pago = new stdClass();
        $pago->pago_id = 1;
        $pago->pago_DocTotal = -1;
        $resultado = $obj->deposito_pago_ins_base('DocTotal',$deposito_aportaciones_id, $pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertEquals(1, $resultado['pago_id']);
        $this->assertEquals(1, $resultado['deposito_aportaciones_id']);
        $this->assertEquals(-1, $resultado['monto_depositado']);

        error::$en_error = false;

        $this->link->rollBack();

    }




    final public function test_sum_depositos_por_aportacion()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\deposito_pago();
        //$obj = new liberator($obj);

        $this->link->beginTransaction();

        $del = "DELETE FROM deposito_pago WHERE deposito_pago.pago_id = 14059175";
        $exe = (new transacciones($this->link))->ejecuta_consulta_segura($del);
        if(error::$en_error){
            $error = (new error())->error('Error al eliminar pago',$exe);
            print_r($error);
            exit;
        }

        $deposito_aportacion_id = 69;
        $resultado = $obj->sum_depositos_por_aportacion($deposito_aportacion_id,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat( $resultado);
        $this->assertEquals(400, $resultado);

        error::$en_error = false;

        $alta_deposito_pago = "INSERT INTO deposito_pago (status,usuario_alta_id,usuario_update_id,pago_id, 
                           deposito_aportaciones_id, monto_depositado) VALUES('activo','2','2','13163339',$deposito_aportacion_id,200)";

        $exe = (new consultas())->exe_objs($this->link,$alta_deposito_pago);
        if(error::$en_error){
            $error = (new error())->error('Error al insertar pago',$exe);
            print_r($error);
            exit;
        }

        $deposito_aportacion_id = 100000177;
        $resultado = $obj->sum_depositos_por_aportacion($deposito_aportacion_id,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat( $resultado);
        $this->assertEquals(0, $resultado);


        $this->link->rollBack();

    }

    final public function test_existe_deposito_pago()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\deposito_pago();
        //$obj = new liberator($obj);

        $this->link->beginTransaction();

        $pago_id = 14065822;
        $resultado = $obj->existe_deposito_pago($pago_id,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool( $resultado);
        $this->assertTrue($resultado);

        error::$en_error = false;


        $this->link->rollBack();

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $this->link->beginTransaction();

        $sql = "INSERT INTO deposito_aportaciones (id, status, usuario_alta_id, usuario_update_id, fecha_alta, fecha_update, pago_corte_id, cuenta_empresa_id, referencia, monto_depositado, fecha, validado) VALUES (1, 'activo', 2, 2, '2024-09-06 11:25:14', '2024-09-06 11:25:14', 5, 1, 'XXX', 1.00, '1900-01-01', 'inactivo');";
        $exe = (new transacciones($this->link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            $error = (new error())->error('Error al insertar pago',$exe);
            print_r($error);
            exit;
        }

        $sql = "INSERT INTO deposito_pago (id, status, usuario_alta_id, usuario_update_id, fecha_alta, fecha_update, pago_id, deposito_aportaciones_id, monto_depositado) VALUES (1, 'activo', 2, 2, 'CURRENT_TIMESTAMP', 'CURRENT_TIMESTAMP', 5, 1, 1);";
        $exe = (new transacciones($this->link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            $error = (new error())->error('Error al insertar pago',$exe);
            print_r($error);
            exit;
        }

        $pago_id = 5;
        $resultado = $obj->existe_deposito_pago($pago_id,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool( $resultado);
        $this->assertTrue($resultado);
        error::$en_error = false;

    }

    final public function test_genera_deposito_pago_ins()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\deposito_pago();
        //$obj = new liberator($obj);

        $this->link->beginTransaction();

        $_POST['pago_id'] = 1;
        $_POST['deposito_aportaciones_id'] = 1;
        $resultado = $obj->genera_deposito_pago_ins('DocTotal',$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertEquals(1, $resultado['pago_id']);
        $this->assertEquals(1, $resultado['deposito_aportaciones_id']);
        $this->assertEquals(100, $resultado['monto_depositado']);

        error::$en_error = false;

        $this->link->rollBack();


        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $this->link->beginTransaction();

        $resultado = $obj->genera_deposito_pago_ins('monto',$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray( $resultado);
        $this->assertEquals(1, $resultado['pago_id']);
        $this->assertEquals(1, $resultado['deposito_aportaciones_id']);
        $this->assertEquals(70, $resultado['monto_depositado']);
        $this->link->rollBack();

    }

}
