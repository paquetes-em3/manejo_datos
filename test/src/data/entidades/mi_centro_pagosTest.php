<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\data\entidades\meta_gestor;
use desarrollo_em3\manejo_datos\data\entidades\mit_centro_pagos;
use PDO;
use PHPUnit\Framework\TestCase;

class mi_centro_pagosTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {

        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_existen_pagos_aprobados()
    {
        error::$en_error = false;
        $obj = new mit_centro_pagos();
        //$seguridad = new liberator($seguridad);

        $contrato_id = 1;
        $ejercicio = 2025;
        $numero_semana = 10;
        $result = $obj->existen_pagos_aprobados($contrato_id,$ejercicio, $this->link,$numero_semana);

        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($result);
        error::$en_error = false;

        $contrato_id = 543024;
        $ejercicio = 2025;
        $numero_semana = 3;
        $result = $obj->existen_pagos_aprobados($contrato_id,$ejercicio, $this->link,$numero_semana);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);
        error::$en_error = false;

    }

    final public function test_obten_mit_pagos_referencia()
    {
        error::$en_error = false;
        $obj = new mit_centro_pagos();
        $obj = new liberator($obj);
        $contrato_id = 524313;
        $ejercicio = 2025;
        $numero_semana = 3;
        $state = 'approved';
        $resultado = $obj->obten_mit_pagos_referencia($contrato_id, $ejercicio, $this->link, $numero_semana, $state);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(321815,$resultado->mit_centro_pagos_id);

        error::$en_error = false;

        $contrato_id = 543024;
        $ejercicio = 2025;
        $numero_semana = 3;
        $state = 'denied';
        $resultado = $obj->obten_mit_pagos_referencia($contrato_id, $ejercicio, $this->link, $numero_semana, $state);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(321819,$resultado->mit_centro_pagos_id);
        error::$en_error = false;
    }


}
