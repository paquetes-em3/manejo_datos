<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\data\entidades\registro_percepcion;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class registro_percepcion_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 2;
        $conexion = (new conexion_db_monterrey());
        $this->link = $conexion->link;

    }

    final public function test_obten_registro_percepcion()
    {
        $_SESSION['usuario_id'] = 2;
        error::$en_error = false;
        $modelo = new registro_percepcion();
        //$modelo = new liberator($modelo);

        $entidad_empleado = 'ohem';
        $registro_precepcion_id = -29204;
        $result  = $modelo->obten_registro_percepcion($entidad_empleado,$this->link,$registro_precepcion_id);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);
        error::$en_error = false;

        $entidad_empleado = 'ohem';
        $registro_precepcion_id = 29204;
        $result  = $modelo->obten_registro_percepcion($entidad_empleado,$this->link,$registro_precepcion_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(29204,$result[0]->registro_percepcion_id);
        $this->assertEquals('MARIA DEL CARMEN ALONSO DE LA GARZA',$result[0]->ohem_nombre_completo);
        $this->assertEquals('2024-09-03',$result[0]->periodo_pago_fecha_inicial);
        $this->assertEquals('2024-09-09',$result[0]->periodo_pago_fecha_final);
        error::$en_error = false;

    }

}
