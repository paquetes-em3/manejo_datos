<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\data\_childrens;
use desarrollo_em3\manejo_datos\data\entidades\meta_gestor;
use desarrollo_em3\manejo_datos\data\entidades\meta_gestor_detalle;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use desarrollo_em3\manejo_datos\sql\empleado;
use PDO;
use PHPUnit\Framework\TestCase;

class meta_gestor_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {

        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_ajusta_cobrado_total()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $indice = 5;
        $row = array();
        $rows_out = array();
        $row['COBRADO TOTAL'] = 10;
        $resultado = $obj->ajusta_cobrado_total($indice,$row,$rows_out);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(10,$resultado[5]['COBRADO CONTRATOS EXTRA']);

        error::$en_error = false;
    }

    final public function test_ajusta_totales()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $row = array();
        $totales = array();
        $resultado = $obj->ajusta_totales($row,$totales);
        //print_r($resultado);exit;
        $this->assertNotTrue(error::$en_error);

        $this->assertEquals(0,$resultado['CONTRATOS CONGELADOS']);
        $this->assertEquals(0,$resultado['CARTERA ACTUAL']);
        $this->assertEquals(0,$resultado['CONTRATOS NUEVOS']);
        $this->assertEquals(0,$resultado['META']);
        $this->assertEquals(0,$resultado['COBRADO TOTAL']);
        $this->assertEquals(0,$resultado['COBRADO META']);
        $this->assertEquals(0,$resultado['COBRADO META DE MAS']);
        $this->assertEquals(0,$resultado['COBRADO CONTRATOS NUEVOS']);
        $this->assertEquals(0,$resultado['COBRADO CONTRATOS EXTRA']);

        error::$en_error = false;
    }
    final public function test_asigna_cobrado_total()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $indice = 0;
        $row = array();
        $rows_out = array();
        $resultado = $obj->asigna_cobrado_total($indice,$row,$rows_out);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0,$resultado[0]['COBRADO CONTRATOS EXTRA']);

        error::$en_error = false;
    }
    final public function test_data_gestor()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $gestor = 'A';
        $gestor_id = 'B';
        $row_new = array();
        $resultado = $obj->data_gestor($gestor,$gestor_id,$row_new);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('B',$resultado['GESTOR ID']);
        $this->assertEquals('A',$resultado['GESTOR']);

        error::$en_error = false;
    }

    final public function test_data_meta()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $r_meta = array();
        $row_new = array();
        $resultado = $obj->data_meta($r_meta,$row_new);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('SIN PLAZA',$resultado['PLAZA']);
        $this->assertEquals('SIN FECHA INICIAL',$resultado['FECHA INICIO']);
        $this->assertEquals('SIN FECHA FINAL',$resultado['FECHA FIN']);

        error::$en_error = false;
    }

    final public function test_ejecuta_consulta_segura()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $sql = 'SELECT *FROM pais';
        $contexto = '';
        $resultado = $obj->ejecuta_consulta_segura($this->link,$sql,$contexto);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);

        error::$en_error = false;
    }
    final public function test_existe_cliente()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $cliente = array();
        $rows = array();
        $rows[] = '';
        $cliente['ohem_id'] = 1;
        $rows[]['GESTOR ID'] = 1;
        $resultado = $obj->existe_cliente($cliente,'ohem', $rows);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);

        error::$en_error = false;
    }

    final public function test_gestor_id()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $cliente = array();

        $resultado = $obj->gestor_id($cliente);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("S/A",$resultado);

        error::$en_error = false;
        $cliente = array();
        $cliente['ohem_id'] = 'x';
        $resultado = $obj->gestor_id($cliente);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("x",$resultado);
        error::$en_error = false;
    }

    final public function test_get_value()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $key = 'a';
        $row = array();
        $resultado = $obj->get_value($key,$row);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("POR ASIGNAR",$resultado);

        error::$en_error = false;
        $key = 'PORCENTAJE META GENERAL';
        $row = ['COBRADO TOTAL' => 10, 'META' => 1000];
        $resultado = $obj->get_value($key,$row);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1,$resultado);
        error::$en_error = false;
    }

    final public function test_inicializa_totales()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);


        $resultado = $obj->inicializa_totales();
        //print_r($resultado);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('',$resultado[0]);
        $this->assertEquals('',$resultado[1]);
        $this->assertEquals('',$resultado[2]);
        $this->assertEquals('',$resultado[3]);
        $this->assertEquals('',$resultado[4]);
        $this->assertEquals(0,$resultado['CONTRATOS CONGELADOS']);
        $this->assertEquals(0,$resultado['CARTERA ACTUAL']);
        $this->assertEquals(0,$resultado['CONTRATOS NUEVOS']);
        $this->assertEquals(0,$resultado['META']);
        $this->assertEquals(0,$resultado['COBRADO TOTAL']);
        $this->assertEquals(0,$resultado['COBRADO META']);
        $this->assertEquals(0,$resultado['COBRADO META DE MAS']);
        $this->assertEquals(0,$resultado['COBRADO CONTRATOS NUEVOS']);
        $this->assertEquals(0,$resultado['COBRADO CONTRATOS EXTRA']);

        error::$en_error = false;
    }

    final public function test_init_data_row_gestor()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $cliente = array();
        $rows = array();
        $entidad_empleado = '';
        $rows[]['GESTOR ID'] = '1';
        $cliente['ohem_id'] = 1;
        $resultado = $obj->init_data_row_gestor($cliente, $entidad_empleado, $rows);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0,$resultado[0]['CARTERA ACTUAL']);

        error::$en_error = false;

    }

    final public function test_init_rows()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $entidad_empleado = '';
        $r_meta = array();
        $sql_clientes = "SELECT 'GESTOR' AS GESTOR, 'n_contratos' AS n_contratos, 'n_contratos_nuevos' AS n_contratos_nuevos";
        $sql_metas = "SELECT *FROM pais";

        $r_meta['plaza_descripcion'] = 'plaza_descripcion';
        $r_meta['meta_gestor_fecha_inicio'] = 'meta_gestor_fecha_inicio';
        $r_meta['meta_gestor_fecha_fin'] = 'meta_gestor_fecha_fin';

        $resultado = $obj->init_rows($entidad_empleado,$this->link,$r_meta,$sql_clientes,$sql_metas);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('plaza_descripcion',$resultado[249]['PLAZA']);

        error::$en_error = false;

    }

    final public function test_init_rows_out()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $rows = array();
        $rows[] = array();
        $resultado = $obj->init_rows_out($rows);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);

        error::$en_error = false;
    }

    final public function test_init_totales()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $resultado = $obj->init_totales();
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('',$resultado[0]);
        $this->assertEquals('',$resultado[1]);
        $this->assertEquals('',$resultado[2]);
        $this->assertEquals('',$resultado[3]);
        $this->assertEquals('',$resultado[4]);


        error::$en_error = false;
    }

    final public function test_init_totales_0()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $totales = array();
        $resultado = $obj->init_totales_0($totales);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0,$resultado['CONTRATOS CONGELADOS']);
        $this->assertEquals(0,$resultado['CARTERA ACTUAL']);
        $this->assertEquals(0,$resultado['CONTRATOS NUEVOS']);
        $this->assertEquals(0,$resultado['META']);
        $this->assertEquals(0,$resultado['COBRADO TOTAL']);
        $this->assertEquals(0,$resultado['COBRADO META']);
        $this->assertEquals(0,$resultado['COBRADO META DE MAS']);
        $this->assertEquals(0,$resultado['COBRADO CONTRATOS NUEVOS']);
        $this->assertEquals(0,$resultado['COBRADO CONTRATOS EXTRA']);

        error::$en_error = false;
    }

    final public function test_integra_cobrado_total()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $indice = 1;
        $row = array();
        $rows_out = array();
        $row['COBRADO TOTAL'] = 500;
        $resultado = $obj->integra_cobrado_total($indice,$row,$rows_out);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(500,$resultado[1]['COBRADO CONTRATOS EXTRA']);

        error::$en_error = false;
    }

    final public function test_integra_keys()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $order_keys = array();
        $row = array();
        $row_new = array();

        $order_keys[] = 'z';

        $resultado = $obj->integra_keys($order_keys,$row,$row_new);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("POR ASIGNAR",$resultado['z']);

        error::$en_error = false;

        $row = ARRAY();
        $row_new = array();
        $order_keys['PORCENTAJE META'] = 100;
        $resultado = $obj->integra_keys($order_keys,$row,$row_new);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("POR ASIGNAR",$resultado['z']);

        error::$en_error = false;
    }

    final public function test_integra_row_new()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $cliente = array();
        $cliente['GESTOR'] = 'GESTOR';
        $cliente['n_contratos'] = 'n_contratos';
        $cliente['n_contratos_nuevos'] = 'n_contratos_nuevos';
        $r_meta = array();
        $r_meta['plaza_descripcion'] = 'pd';
        $r_meta['meta_gestor_fecha_inicio'] = 'meta_gestor_fecha_inicio';
        $r_meta['meta_gestor_fecha_fin'] = 'meta_gestor_fecha_fin';
        $entidad_empleado = '';
        $resultado = $obj->integra_row_new($cliente, $entidad_empleado,$r_meta);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("pd",$resultado['PLAZA']);

        error::$en_error = false;

    }

    final public function test_integra_row_pagos()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $entidad_empleado = '';
        $pagos_rows = array();
        $pagos_rows[] = array();
        $r_meta = array();
        $rows = array();
        $resultado = $obj->integra_row_pagos($entidad_empleado,$pagos_rows,$r_meta,$rows);
        //print_r($resultado);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("0",$resultado[0]['COBRADO TOTAL']);

        error::$en_error = false;

    }

    final public function test_integra_totales()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $rows_out = array();
        $resultado = $obj->integra_totales($rows_out);

        $this->assertNotTrue(error::$en_error);

        $this->assertEquals(0,$resultado[2]['CONTRATOS CONGELADOS']);
        $this->assertEquals(0,$resultado[2]['CARTERA ACTUAL']);
        $this->assertEquals(0,$resultado[2]['CONTRATOS NUEVOS']);
        $this->assertEquals(0,$resultado[2]['META']);
        $this->assertEquals(0,$resultado[2]['COBRADO TOTAL']);
        $this->assertEquals(0,$resultado[2]['COBRADO META']);
        $this->assertEquals(0,$resultado[2]['COBRADO META DE MAS']);
        $this->assertEquals(0,$resultado[2]['COBRADO CONTRATOS NUEVOS']);
        $this->assertEquals(0,$resultado[2]['COBRADO CONTRATOS EXTRA']);

        error::$en_error = false;
    }

    final public function test_integra_value()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $key = 'a';
        $row = array();
        $resultado = $obj->integra_value($key,$row);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("POR ASIGNAR",$resultado['a']);

        error::$en_error = false;
        $key = 'CONTRATOS CONGELADOS';
        $row = array();
        $resultado = $obj->integra_value($key,$row);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0,$resultado['CONTRATOS CONGELADOS']);
        error::$en_error = false;
    }

    final public function test_integra_value_key()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $key = 'z';
        $row = array();
        $row_new = array();
        $resultado = $obj->integra_value_key($key,$row,$row_new);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("POR ASIGNAR",$resultado['z']);

        error::$en_error = false;
        $key = 'COBRADO TOTAL';
        $row = ['COBRADO META' => 100, 'META' => 200];
        $row_new = array();
        $resultado = $obj->integra_value_key($key,$row,$row_new);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0,$resultado['COBRADO TOTAL']);
        error::$en_error = false;
    }

    final public function test_integra_value_nuevo()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);
        $key = 'a';
        $row = array();
        $resultado = $obj->integra_value_nuevo($key,$row);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("POR ASIGNAR",$resultado['a']);

        error::$en_error = false;
        $key = 'CONTRATOS CONGELADOS';
        $row = array();
        $resultado = $obj->integra_value_nuevo($key,$row);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0,$resultado['CONTRATOS CONGELADOS']);
        error::$en_error = false;

        error::$en_error = false;
        $key = 'CONTRATOS CONGELADOS';
        $row  = ['COBRADO META' => 100, 'META' => 200];
        $resultado = $obj->integra_value_nuevo($key,$row);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0,$resultado['CONTRATOS CONGELADOS']);
        error::$en_error = false;
    }

    final public function test_keys_cero()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $resultado = $obj->keys_cero();
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("CONTRATOS CONGELADOS",$resultado[0]);
        $this->assertEquals("CARTERA ACTUAL",$resultado[1]);
        $this->assertEquals("CONTRATOS NUEVOS",$resultado[2]);
        $this->assertEquals("META",$resultado[3]);
        $this->assertEquals("COBRADO TOTAL",$resultado[4]);
        $this->assertEquals("COBRADO META",$resultado[5]);
        $this->assertEquals("COBRADO META DE MAS",$resultado[6]);
        $this->assertEquals("COBRADO CONTRATOS NUEVOS",$resultado[7]);
        $this->assertEquals("COBRADO CONTRATOS EXTRA",$resultado[8]);
        $this->assertEquals("PORCENTAJE META GENERAL",$resultado[9]);
        $this->assertEquals("PORCENTAJE META REAL",$resultado[10]);

        error::$en_error = false;

    }

    final public function test_porcentaje_meta()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $row = array();
        $key_base = 'x';
        $row['META'] = 100;
        $row['x'] = 10;
        $resultado = $obj->porcentaje_meta($key_base, $row);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(10, $resultado);

        error::$en_error = false;

    }

    final public function test_result_meta()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        //$obj = new liberator($obj);

        $entidad_empleado = '';
        $pagos_rows = array();
        $r_meta = array();
        $sql_clientes = "SELECT '1' AS GESTOR, '10' AS n_contratos, '15' AS n_contratos_nuevos FROM pais";
        $sql_metas = 'SELECT *FROM pais';
        $r_meta['plaza_descripcion'] = 'plaza';
        $r_meta['meta_gestor_fecha_inicio'] = 'meta_gestor_fecha_inicio';
        $r_meta['meta_gestor_fecha_fin'] = 'meta_gestor_fecha_fin';

        $resultado = $obj->result_meta($entidad_empleado,$this->link,$pagos_rows,$r_meta,$sql_clientes,$sql_metas);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);

        error::$en_error = false;

    }

    final public function test_rows_cliente()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $clientes_rows = array();
        $entidad_empleado = '';
        $r_meta = array();
        $r_meta['plaza_descripcion'] = 'plaza_descripcion';
        $r_meta['meta_gestor_fecha_inicio'] = 'meta_gestor_fecha_inicio';
        $r_meta['meta_gestor_fecha_fin'] = 'meta_gestor_fecha_fin';
        $rows = array();

        $clientes_rows[0]['GESTOR'] = 'GESTOR';
        $clientes_rows[0]['n_contratos'] = 'n_contratos';
        $clientes_rows[0]['n_contratos_nuevos'] = 'n_contratos_nuevos';

        $clientes_rows[1]['GESTOR'] = 'GESTOR';
        $clientes_rows[1]['n_contratos'] = 'n_contratos';
        $clientes_rows[1]['n_contratos_nuevos'] = 'n_contratos_nuevos';

        $clientes_rows[2]['GESTOR'] = 'GESTOR';
        $clientes_rows[2]['n_contratos'] = 'n_contratos';
        $clientes_rows[2]['n_contratos_nuevos'] = 'n_contratos_nuevos';
        $clientes_rows[2]['ohem_id'] = 1;

        $clientes_rows[3]['GESTOR'] = 'GESTOR';
        $clientes_rows[3]['n_contratos'] = 'n_contratos';
        $clientes_rows[3]['n_contratos_nuevos'] = 'n_contratos_nuevos';
        $clientes_rows[3]['ohem_id'] = 1;

        $rows[0]['GESTOR ID'] = 1;

        $resultado = $obj->rows_cliente($clientes_rows,$entidad_empleado,$r_meta, $rows);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1, $resultado[0]['GESTOR ID']);

        error::$en_error = false;

    }

    final public function test_row_new()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $cliente = array();
        $gestor_id = -1;
        $r_meta = array();
        $r_meta['plaza_descripcion'] = '';
        $r_meta['meta_gestor_fecha_inicio'] = '';
        $r_meta['meta_gestor_fecha_fin'] = '';

        $cliente['GESTOR'] = 'A';
        $cliente['n_contratos'] = '';
        $cliente['n_contratos_nuevos'] = '';
        $resultado = $obj->row_new($cliente,$gestor_id,$r_meta);


        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('', $resultado['PLAZA']);

        error::$en_error = false;

    }

    final public function test_row_new_0()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $row_new = array();
        $resultado = $obj->row_new_0($row_new);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('0', $resultado['CONTRATOS CONGELADOS']);
        $this->assertEquals('0', $resultado['META']);
        $this->assertEquals('0', $resultado['COBRADO META']);
        $this->assertEquals('0', $resultado['COBRADO META DE MAS']);
        $this->assertEquals('0', $resultado['COBRADO CONTRATOS NUEVOS']);
        $this->assertEquals('0', $resultado['COBRADO CONTRATOS EXTRA']);

        error::$en_error = false;

    }

    final public function test_row_new_base()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $gestor = 'A';
        $gestor_id = 'B';
        $r_meta = array();
        $resultado = $obj->row_new_base($gestor,$gestor_id, $r_meta);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('SIN PLAZA', $resultado['PLAZA']);
        $this->assertEquals('SIN FECHA INICIAL', $resultado['FECHA INICIO']);
        $this->assertEquals('SIN FECHA FINAL', $resultado['FECHA FIN']);
        $this->assertEquals('B', $resultado['GESTOR ID']);
        $this->assertEquals('A', $resultado['GESTOR']);
        $this->assertEquals('0', $resultado['CONTRATOS CONGELADOS']);
        $this->assertEquals('0', $resultado['META']);
        $this->assertEquals('0', $resultado['COBRADO META']);
        $this->assertEquals('0', $resultado['COBRADO META DE MAS']);
        $this->assertEquals('0', $resultado['COBRADO CONTRATOS NUEVOS']);
        $this->assertEquals('0', $resultado['COBRADO CONTRATOS EXTRA']);
        error::$en_error = false;

    }

    final public function test_rows_cobrado_total()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $rows_out = array();

        $rows_out[0]['META'] = 0;
        $rows_out[0]['COBRADO TOTAL'] = 10;
        $resultado = $obj->rows_cobrado_total($rows_out);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(10,$resultado[0]['COBRADO CONTRATOS EXTRA']);
        $this->assertEquals(10,$resultado[0]['COBRADO TOTAL']);
        $this->assertEquals(0,$resultado[0]['META']);

        error::$en_error = false;
    }

    final public function test_rows_out()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $rows = array();
        $resultado = $obj->rows_out($rows);

        $this->assertNotTrue(error::$en_error);

        $this->assertEquals(0,$resultado[2]['CONTRATOS CONGELADOS']);
        $this->assertEquals(0,$resultado[2]['CARTERA ACTUAL']);
        $this->assertEquals(0,$resultado[2]['CONTRATOS NUEVOS']);
        $this->assertEquals(0,$resultado[2]['META']);
        $this->assertEquals(0,$resultado[2]['COBRADO TOTAL']);
        $this->assertEquals(0,$resultado[2]['COBRADO META']);
        $this->assertEquals(0,$resultado[2]['COBRADO META DE MAS']);
        $this->assertEquals(0,$resultado[2]['COBRADO CONTRATOS NUEVOS']);
        $this->assertEquals(0,$resultado[2]['COBRADO CONTRATOS EXTRA']);

        error::$en_error = false;
    }

    final public function test_valida_meta()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $r_meta = array();
        $r_meta['plaza_descripcion'] = 'plaza_descripcion';
        $r_meta['meta_gestor_fecha_inicio'] = 'meta_gestor_fecha_inicio';
        $r_meta['meta_gestor_fecha_fin'] = 'meta_gestor_fecha_fin';
        $resultado = $obj->valida_meta($r_meta);


        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);

        error::$en_error = false;

    }

    final public function test_valida_row()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $cliente = array();
        $r_meta = array();
        $r_meta['plaza_descripcion'] = 'plaza_descripcion';
        $r_meta['meta_gestor_fecha_inicio'] = 'meta_gestor_fecha_inicio';
        $r_meta['meta_gestor_fecha_fin'] = 'meta_gestor_fecha_fin';
        $cliente['GESTOR'] = '';
        $cliente['n_contratos'] = '';
        $cliente['n_contratos_nuevos'] = '';
        $resultado = $obj->valida_row($cliente,$r_meta);


        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);

        $cliente = array();
        $r_meta = array();
        $r_meta['plaza_descripcion'] = 'plaza_descripcion';
        $r_meta['meta_gestor_fecha_inicio'] = 'meta_gestor_fecha_inicio';
        $r_meta['meta_gestor_fecha_fin'] = 'meta_gestor_fecha_fin';

        $cliente['n_contratos'] = '';
        $cliente['n_contratos_nuevos'] = '';
        $resultado = $obj->valida_row($cliente,$r_meta);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);
        error::$en_error = false;

    }

    final public function test_value()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);

        $key = 'a';
        $keys_cero = array();
        $row = array();
        $resultado = $obj->value($key,$keys_cero,$row);


        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("POR ASIGNAR",$resultado);

        error::$en_error = false;

        $key = 'a';
        $keys_cero = array();
        $row = array();

        $keys_cero[0] = 'a';

        $resultado = $obj->value($key,$keys_cero,$row);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0,$resultado);

        error::$en_error = false;

        $key = 'PORCENTAJE META GENERAL';
        $keys_cero = array();
        $row = array();
        $row['COBRADO META'] = 100;
        $row['META'] = 1000;
        $row['COBRADO TOTAL'] = 100;

        $keys_cero[0] = 'a';

        $resultado = $obj->value($key,$keys_cero,$row);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(10,$resultado);
        error::$en_error = false;


    }

    final public function test_value_porcentaje_meta()
    {
        error::$en_error = false;
        $obj = new meta_gestor();
        $obj = new liberator($obj);


        $key_base = 'X';
        $row = array();
        $row['META'] = 100;
        $row['X'] = 10;
        $resultado = $obj->value_porcentaje_meta($key_base,$row);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(10, $resultado);

        error::$en_error = false;

    }






}
