<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\data\entidades\asistencia_app;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class aistencia_app_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_actualiza_distancia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);


        $asistencia_app_id = 1;
        $distancia_jefe = -1;
        $result = $obj->actualiza_distancia($asistencia_app_id,$distancia_jefe, $this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("UPDATE asistencia_app SET distancia_jefe = -1 WHERE id = 1",$result['sql']);

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $asistencia_app_id = 1;
        $distancia_jefe = -1;
        $result = $obj->actualiza_distancia($asistencia_app_id,$distancia_jefe, $this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("UPDATE asistencia_app SET distancia_jefe = -1 WHERE id = 1",$result['sql']);


        error::$en_error = false;

        $asistencia_app_id = 1;
        $distancia_jefe = 10;
        $result = $obj->actualiza_distancia($asistencia_app_id,$distancia_jefe, $this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("UPDATE asistencia_app SET distancia_jefe = 10 WHERE id = 1",$result['sql']);
        error::$en_error = false;


    }

    final public function test_actualiza_distancia_jefe()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);


        $asistencia_app_id = 1;
        $entidad_empleado = 'a';
        $result = $obj->actualiza_distancia_jefe($asistencia_app_id,$entidad_empleado, $this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("UPDATE asistencia_app SET distancia_jefe = -1 WHERE id = 1",$result->upd['sql']);

        error::$en_error = false;


        $asistencia_app_id = 61188;
        $entidad_empleado = 'ohem';
        $result = $obj->actualiza_distancia_jefe($asistencia_app_id,$entidad_empleado, $this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("UPDATE asistencia_app SET distancia_jefe = 16.812393405378 WHERE id = 61188",$result->upd['sql']);



        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $asistencia_app_id = 56;
        $entidad_empleado = 'a';
        $result = $obj->actualiza_distancia_jefe($asistencia_app_id,$entidad_empleado, $this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("UPDATE asistencia_app SET distancia_jefe = -1 WHERE id = 56",$result->upd['sql']);


        error::$en_error = false;


        $asistencia_app_id = 3072;
        $entidad_empleado = 'empleado';
        $result = $obj->actualiza_distancia_jefe($asistencia_app_id,$entidad_empleado, $this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals("UPDATE asistencia_app SET distancia_jefe = 14.615919639901 WHERE id = 3072",$result->upd['sql']);

        error::$en_error = false;

    }

    final public function test_actualiza_en_sitio()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);
        $entidad_empleado = 'ohem';
        $fecha = '2024-09-02';
        $ohem_id = 23691;
        $resultado = $obj->actualiza_en_sitio($entidad_empleado,$fecha,$this->link,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("UPDATE asistencia_app SET checada_en_sitio = 'activo' WHERE asistencia_app.id = 60015",$resultado['sql']);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $entidad_empleado = 'empleado';
        $fecha = '2024-09-04';
        $ohem_id = 1401;
        $resultado = $obj->actualiza_en_sitio($entidad_empleado,$fecha,$this->link,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("UPDATE asistencia_app SET checada_en_sitio = 'activo' WHERE asistencia_app.id = 3063",$resultado['sql']);

        error::$en_error = false;
    }

    final public function test_ajusta_asistencia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);
        $asistencia_app = new stdClass();
        $asistencia_app->checada_en_sitio = 'activo';
        $asistencia_app->jefe_checo = 'activo';
        $asistencia_app->distancia_jefe = '1';
        $asistencia_app->id = '1';
        $resultado = $obj->ajusta_asistencia($asistencia_app,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $resultado = $obj->ajusta_asistencia($asistencia_app,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);

        error::$en_error = false;
    }

    final public function test_ajusta_asistencias()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);
        $asistencias = array();
        $asistencias[0] = new stdClass();
        $asistencias[0]->id = '1';
        $entidad_empleado = 'ohem';
        $resultado = $obj->ajusta_asistencias($asistencias,$entidad_empleado,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado[0]);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $asistencias = array();
        $asistencias[0] = new stdClass();
        $asistencias[0]->id = '56';
        $entidad_empleado = 'empleado';
        $resultado = $obj->ajusta_asistencias($asistencias,$entidad_empleado,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado[0]);

        error::$en_error = false;
    }

    final public function test_ajusta_asistencias_por_dia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $fecha = '2024-09-01';
        $resultado = $obj->ajusta_asistencias_por_dia($entidad_empleado,$fecha,$this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado[0]);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $fecha = '2023-04-06';
        $entidad_empleado = 'empleado';
        $resultado = $obj->ajusta_asistencias_por_dia($entidad_empleado,$fecha,$this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado[0]);


        error::$en_error = false;
    }

    final public function test_ajusta_distancia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $asistencia_app_id = 59582;
        $result = $obj->ajusta_distancia($asistencia_app_id,$entidad_empleado,$this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(-1,$result->asistencia_row['distancia_jefe']);

        error::$en_error = false;
        $entidad_empleado = 'ohem';
        $asistencia_app_id = 61173;
        $result = $obj->ajusta_distancia($asistencia_app_id,$entidad_empleado,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(11,$result->asistencia_row['distancia_jefe']);
        error::$en_error = false;



        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $entidad_empleado = 'empleado';
        $asistencia_app_id = 3050;
        $result = $obj->ajusta_distancia($asistencia_app_id,$entidad_empleado,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(-1,$result->asistencia_row['distancia_jefe']);

        error::$en_error = false;


        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $entidad_empleado = 'empleado';
        $asistencia_app_id = 3053;
        $result = $obj->ajusta_distancia($asistencia_app_id,$entidad_empleado,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(266,$result->asistencia_row['distancia_jefe']);
        error::$en_error = false;


        $entidad_empleado = 'empleado';
        $asistencia_app_id = 3074;
        $result = $obj->ajusta_distancia($asistencia_app_id,$entidad_empleado,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(16,$result->asistencia_row['distancia_jefe']);
        error::$en_error = false;


    }

    final public function test_ajusta_distancia_recursive()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $data = new stdClass();
        $data->asistencia_row = array();
        $result = $obj->ajusta_distancia_recursive($data,$entidad_empleado,$this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);


        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $entidad_empleado = 'empleado';
        $result = $obj->ajusta_distancia_recursive($data,$entidad_empleado,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        error::$en_error = false;

        $entidad_empleado = 'empleado';
        $data->asistencia_row = array();
        $data->asistencia_row['empleado_id'] = 1339;
        $data->asistencia_row['fecha'] = '2024-09-05';
        $result = $obj->ajusta_distancia_recursive($data,$entidad_empleado,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(15,$result['a_su_cargo'][0]->asistencia_row['distancia_jefe']);
        error::$en_error = false;

    }

    final public function test_asistencia_app_id()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);
        $entidad_empleado = 'ohem';
        $fecha = '2024-09-02';
        $ohem_id = 23691;
        $resultado = $obj->asistencia_app_id($entidad_empleado,$fecha,$this->link,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(60015,$resultado);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $entidad_empleado = 'empleado';
        $fecha = '2024-09-04';
        $ohem_id = 1401;
        $resultado = $obj->asistencia_app_id($entidad_empleado,$fecha,$this->link,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(3063,$resultado);

        error::$en_error = false;
    }

    final public function test_asistencia_conteo_dias() {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$obj = new liberator($obj);
        $_SESSION['numero_empresa'] = 2;
        $conexion = (new conexion_db_monterrey());
        $this->link = $conexion->link;

        $entidad_empleado = 'ohem';
        $fecha_inicial = '2024-09-03';
        $fecha_final = '2024-09-09';
        $ohem_id = 20388;
        $status = 'inactivo';
        $resultado = $obj->asistencia_conteo_dias($entidad_empleado,$fecha_inicial, $fecha_final, $this->link, $ohem_id, $status);
        //print_r($resultado);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsInt($resultado);
        $this->assertEquals(6,$resultado);

        error::$en_error = false;

        $entidad_empleado = 'ohem';
        $fecha_inicial = '2024-09-03';
        $fecha_final = '2024-09-09';
        $ohem_id = 20388;
        $status = 'activo';
        $resultado = $obj->asistencia_conteo_dias($entidad_empleado,$fecha_inicial, $fecha_final, $this->link, $ohem_id, $status);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsInt($resultado);
        $this->assertEquals(0,$resultado);

        error::$en_error = false;
    }

    final public function test_asistencia_jefe_upd()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $asistencia_app_id = 1;
        $entidad_empleado = 'a';
        $resultado = $obj->asistencia_jefe_upd($asistencia_app_id,$entidad_empleado,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("18743",$resultado->asistencia_row->ohem_id);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $asistencia_app_id = 56;
        $entidad_empleado = 'a';
        $resultado = $obj->asistencia_jefe_upd($asistencia_app_id,$entidad_empleado,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("1253",$resultado->asistencia_row->empleado_id);

        error::$en_error = false;
    }

    final public function test_asistencia_transacciona_dia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $asistencia_app = new stdClass();
        $asistencia_app->dia = 'POR ASIGNAR';
        $asistencia_app->fecha = '2024-09-27';
        $asistencia_app->id = '1';

        $resultado = $obj->asistencia_transacciona_dia($asistencia_app,$this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("UPDATE asistencia_app SET dia = 'VIERNES' WHERE id = 1",$resultado['sql']);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $asistencia_app = new stdClass();
        $asistencia_app->dia = 'POR ASIGNAR';
        $asistencia_app->fecha = '2024-09-26';
        $asistencia_app->id = '1';

        $resultado = $obj->asistencia_transacciona_dia($asistencia_app,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("UPDATE asistencia_app SET dia = 'JUEVES' WHERE id = 1",$resultado['sql']);
        error::$en_error = false;
    }

    final public function test_asistencia_valida()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);


        $asistencia_app = new stdClass();
        $asistencia_app->checada_en_sitio = 'activo';
        $asistencia_app->jefe_checo = 'activo';
        $asistencia_app->distancia_jefe = 199;
        $result = $obj->asistencia_valida($asistencia_app);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);

        error::$en_error = false;


    }

    final public function test_asistencia_valida_transacciona()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $asistencia_app = new stdClass();
        $asistencia_app->asistencia = 'inactivo';

        $resultado = $obj->asistencia_valida_transacciona($asistencia_app,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertFalse($resultado);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $asistencia_app = new stdClass();
        $asistencia_app->asistencia = 'inactivo';

        $resultado = $obj->asistencia_valida_transacciona($asistencia_app,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertFalse($resultado);

        error::$en_error = false;
    }

    final public function test_asistencias_periodo()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $campo_fecha_ingreso = 'startDate';
        $entidad_empleado = 'ohem';
        $fecha_final = '2024-09-20';
        $fecha_inicial = '2024-09-16';

        $resultado = $obj->asistencias_periodo($campo_fecha_ingreso,$entidad_empleado,$fecha_final,$fecha_inicial,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertIsObject($resultado[0]);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $campo_fecha_ingreso = 'fecha_ingreso';
        $entidad_empleado = 'empleado';
        $fecha_final = '2024-09-20';
        $fecha_inicial = '2024-09-16';

        $resultado = $obj->asistencias_periodo($campo_fecha_ingreso,$entidad_empleado,$fecha_final,$fecha_inicial,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertIsObject($resultado[0]);

        error::$en_error = false;
    }

    final public function test_asistencias_por_dia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $fecha = '2024-09-01';
        $resultado = $obj->asistencias_por_dia($fecha,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado[0]);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $fecha = '2023-04-06';
        $resultado = $obj->asistencias_por_dia($fecha,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado[0]);


        error::$en_error = false;
    }

    final public function test_asistencias_recientes()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);



        $resultado = $obj->asistencias_recientes($this->link,0);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $resultado = $obj->asistencias_recientes($this->link,0);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);



        error::$en_error = false;
    }

    final public function test_checada_con_dato()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $asistencia_dia = new stdClass();
        $asistencia_dia->distancia_jefe = 100;
        $empleado = array();
        $dia = 'x';
        $resultado = $obj->checada_con_dato($asistencia_dia,$dia,$empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(100,$resultado['x_distancia_jefe']);


        error::$en_error = false;

        $asistencia_dia = new stdClass();
        $asistencia_dia->distancia_jefe = 100;
        $asistencia_dia->hora_entrada = 500;
        $empleado = array();
        $dia = 'x';
        $resultado = $obj->checada_con_dato($asistencia_dia,$dia,$empleado);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(100,$resultado['x_distancia_jefe']);
        $this->assertEquals(500,$resultado['x_APP_AS']);

        error::$en_error = false;

    }

    final public function test_data_dia_pago()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$obj = new liberator($obj);

        $asistencia_app_id = 1;
        $entidad_empleado = 'ohem';
        $resultado = $obj->data_dia_pago($asistencia_app_id,$entidad_empleado,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $asistencia_app_id = 56;
        $entidad_empleado = 'empleado';
        $resultado = $obj->data_dia_pago($asistencia_app_id,$entidad_empleado,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);

        error::$en_error = false;
    }

    final public function test_dia_espaniol()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);


        $fecha = '2024-09-15';
        $result = $obj->dia_espaniol($fecha);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('DOMINGO', $result);

        error::$en_error = false;

        $fecha = '2024-09-16';
        $result = $obj->dia_espaniol($fecha);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('LUNES', $result);

        error::$en_error = false;

        $fecha = '2024-09-17';
        $result = $obj->dia_espaniol($fecha);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('MARTES', $result);

        error::$en_error = false;

        $fecha = '2024-09-18';
        $result = $obj->dia_espaniol($fecha);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('MIERCOLES', $result);

        error::$en_error = false;

        $fecha = '2024-09-19';
        $result = $obj->dia_espaniol($fecha);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('JUEVES', $result);

        error::$en_error = false;

        $fecha = '2024-09-20';
        $result = $obj->dia_espaniol($fecha);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('VIERNES', $result);

        error::$en_error = false;

        $fecha = '2024-09-21';
        $result = $obj->dia_espaniol($fecha);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('SABADO', $result);

        error::$en_error = false;

        $fecha = '2024-09-22';
        $result = $obj->dia_espaniol($fecha);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('DOMINGO', $result);

        error::$en_error = false;

    }

    final public function test_dias_periodo()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $fecha_final = '2024-11-20';
        $fecha_inicial = '2024-11-17';

        $resultado = $obj->dias_periodo($fecha_final,$fecha_inicial);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsInt($resultado);
        $this->assertEquals(4,$resultado);


        error::$en_error = false;


    }

    final public function test_distancia_jefe()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $fecha = '2024-09-02';
        $latitud_empleado = 20.5877471;
        $longitud_empleado = -100.3755556;
        $ohem_jefe_inmediato_id = 23308;
        $result = $obj->distancia_jefe($entidad_empleado, $fecha, $this->link,$latitud_empleado,$longitud_empleado,$ohem_jefe_inmediato_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(22.4565, round($result,4));
        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $entidad_empleado = 'empleado';
        $fecha = '2024-09-05';
        $latitud_empleado = 19.104393;
        $longitud_empleado = -104.3317866;
        $ohem_jefe_inmediato_id = 1339;
        $result = $obj->distancia_jefe($entidad_empleado, $fecha, $this->link,$latitud_empleado,$longitud_empleado,$ohem_jefe_inmediato_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(14.6159, round($result,4));
        error::$en_error = false;

    }

    final public function test_distancia_valida()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);
        $asistencia_app = new stdClass();
        $resultado = $obj->distancia_valida($asistencia_app);


        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);


        error::$en_error = false;

        $asistencia_app = new stdClass();
        $asistencia_app->distancia_jefe = 10;
        $resultado = $obj->distancia_valida($asistencia_app);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);

        error::$en_error = false;
    }

    final public function test_empleados_actuales_asistencia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $empleados_full = array();
        $entidad_empleado = 'ohem';
        $empleados_full[10] = '';
        $resultado = $obj->empleados_actuales_asistencia($empleados_full,$entidad_empleado, $this->link);
        //print_r($resultado);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(10,$resultado[10]->id);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $empleados_full = array();
        $entidad_empleado = 'empleado';
        $empleados_full[10] = '';
        $resultado = $obj->empleados_actuales_asistencia($empleados_full,$entidad_empleado, $this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(10,$resultado[10]->id);


        error::$en_error = false;
    }

    final public function test_empleados_full_periodo_asistencia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $asistencias = array();
        $asistencias[] = '';
        $entidad_empleado = 'ohem';
        $asistencias[0] = new stdClass();
        $asistencias[0]->ohem_id = '1';

        $asistencias[1] = new stdClass();
        $asistencias[1]->ohem_id = '1';

        $asistencias[2] = new stdClass();
        $asistencias[2]->ohem_id = '2';

        $resultado = $obj->empleados_full_periodo_asistencia($asistencias, $entidad_empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(1,$resultado[1][0]->ohem_id);
        $this->assertEquals(1,$resultado[1][1]->ohem_id);
        $this->assertEquals(2,$resultado[2][0]->ohem_id);


        error::$en_error = false;
    }

    final public function test_esquema_empleado_fecha()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $campo_esquema_name = 'Name';
        $entidad_empleado = 'ohem';
        $fecha_limite = '2024-06-23';
        $ohem_id = '22021';
        $entidad_relacion  ='empleado_esquema';

        $resultado = $obj->esquema_empleado_fecha($campo_esquema_name,$entidad_empleado, $entidad_relacion,
            $fecha_limite,$this->link,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(17100,$resultado['empleado_esquema_id']);
        $this->assertEquals("COORDINADOR PABS (MES 2)",$resultado['esquema_name']);
        $this->assertEquals("2024-06-05",$resultado['esquema_fecha']);



        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $campo_esquema_name = 'nombre';
        $entidad_empleado = 'empleado';
        $fecha_limite = '2024-09-03';
        $ohem_id = '1491';
        $entidad_relacion = 'esquema_empleado';
        $resultado = $obj->esquema_empleado_fecha($campo_esquema_name,$entidad_empleado, $entidad_relacion,$fecha_limite,$this->link,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(565,$resultado['esquema_empleado_id']);
        $this->assertEquals("PLAN ARRANQUE PLUS",$resultado['esquema_name']);
        $this->assertEquals("2024-09-03",$resultado['esquema_fecha']);



        error::$en_error = false;
    }

    final public function test_esquemas_historia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $campo_esquema_name = 'Name';
        $entidad_empleado = 'ohem';
        $entidad_rel_esquema = 'empleado_esquema';
        $primer_checada = new stdClass();
        $ultima_checada = new stdClass();

        $primer_checada->fecha = '2024-09-22';
        $primer_checada->ohem_id = '24081';

        $ultima_checada->fecha = '2024-09-30';
        $ultima_checada->ohem_id = '24081';

        $resultado = $obj->esquemas_historia($campo_esquema_name,$entidad_empleado,$entidad_rel_esquema,$this->link,$primer_checada,$ultima_checada);


        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("19933",$resultado->esquema_primer_checada['empleado_esquema_id']);
        $this->assertEquals("COORDINADOR PABS (MES 1)",$resultado->esquema_primer_checada['esquema_name']);
        $this->assertEquals("2024-09-13",$resultado->esquema_primer_checada['esquema_fecha']);

        $this->assertEquals("19933",$resultado->esquema_ultima_checada['empleado_esquema_id']);
        $this->assertEquals("COORDINADOR PABS (MES 1)",$resultado->esquema_ultima_checada['esquema_name']);
        $this->assertEquals("2024-09-13",$resultado->esquema_ultima_checada['esquema_fecha']);

        $this->assertEquals("19933",$resultado->esquema_actual['empleado_esquema_id']);
        $this->assertEquals("COORDINADOR PABS (MES 1)",$resultado->esquema_actual['esquema_name']);
        $this->assertEquals("2024-09-13",$resultado->esquema_actual['esquema_fecha']);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $campo_esquema_name = 'nombre';
        $entidad_empleado = 'empleado';
        $entidad_rel_esquema = 'esquema_empleado';
        $primer_checada = new stdClass();
        $ultima_checada = new stdClass();

        $primer_checada->fecha = '2024-09-22';
        $primer_checada->empleado_id = '1489';

        $ultima_checada->fecha = '2024-09-30';
        $ultima_checada->empleado_id = '1489';

        $resultado = $obj->esquemas_historia($campo_esquema_name,$entidad_empleado,$entidad_rel_esquema,$this->link,$primer_checada,$ultima_checada);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("563",$resultado->esquema_primer_checada['esquema_empleado_id']);
        $this->assertEquals("PLAN ARRANQUE PLUS",$resultado->esquema_primer_checada['esquema_name']);
        $this->assertEquals("2024-08-30",$resultado->esquema_primer_checada['esquema_fecha']);



        error::$en_error = false;
    }

    final public function test_exe_dia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $asistencia_app = new stdClass();
        $asistencia_app->fecha = 'a';
        $asistencia_app->id = '1';
        $resultado = $obj->exe_dia($asistencia_app, $this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("UPDATE asistencia_app SET dia = 'JUEVES' WHERE id = 1",$resultado['sql']);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $asistencia_app = new stdClass();
        $asistencia_app->fecha = 'a';
        $asistencia_app->id = '1';
        $resultado = $obj->exe_dia($asistencia_app, $this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("UPDATE asistencia_app SET dia = 'JUEVES' WHERE id = 1",$resultado['sql']);

        error::$en_error = false;
    }

    final public function test_get_checada()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);
        $entidad_empleado = 'ohem';
        $fecha = '2020-01-01';
        $ohem_id = 1;
        $resultado = $obj->get_checada($entidad_empleado,$fecha,$this->link,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(-9999, $resultado->latitud);
        $this->assertEquals(-9999, $resultado->longitud);

        error::$en_error = false;

        $entidad_empleado = 'ohem';
        $fecha = '2022-11-11';
        $ohem_id = 10163;
        $resultado = $obj->get_checada($entidad_empleado,$fecha,$this->link,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(20.5224511, $resultado->latitud);
        $this->assertEquals(-100.8234214, $resultado->longitud);
        error::$en_error = false;


        error::$en_error = false;

        $entidad_empleado = 'ohem';
        $fecha = '2024-09-05';
        $ohem_id = 23874;
        $resultado = $obj->get_checada($entidad_empleado,$fecha,$this->link,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(22.1015867, $resultado->latitud);
        $this->assertEquals(-100.9444751, $resultado->longitud);

        error::$en_error = false;


        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $entidad_empleado = 'empleado';
        $fecha = '2024-09-05';
        $ohem_id = 1401;
        $resultado = $obj->get_checada($entidad_empleado,$fecha,$this->link,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(19.1045619, $resultado->latitud);
        $this->assertEquals(-104.3320408, $resultado->longitud);
        error::$en_error = false;


    }

    final public function test_get_dias_periodo()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $fecha_final = '2024-11-15';
        $fecha_inicial = '2024-11-20';

        $resultado = $obj->get_dias_periodo($fecha_final,$fecha_inicial);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('MIERCOLES',$resultado['2024-11-20']);
        $this->assertEquals('JUEVES',$resultado['2024-11-21']);
        $this->assertEquals('VIERNES',$resultado['2024-11-22']);
        $this->assertEquals('SABADO',$resultado['2024-11-23']);
        $this->assertEquals('DOMINGO',$resultado['2024-11-24']);
        $this->assertEquals('LUNES',$resultado['2024-11-25']);


        error::$en_error = false;


    }

    final public function test_get_distancia_jefe()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $registro = array();
        $registro['fecha'] = '2024-09-02';
        $registro['ohem_id'] = 23691;
        $registro['ohem_jefe_inmediato_id'] = 23308;
        $registro['latitud'] = 20.5877471;
        $registro['longitud'] = -100.3755556;
        $result = $obj->get_distancia_jefe($entidad_empleado,$this->link,$registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(22.4565, round($result,4));
        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $entidad_empleado = 'empleado';
        $registro = array();
        $registro['fecha'] = '2024-09-05';
        $registro['empleado_id'] = 1478;
        $registro['empleado_jefe_inmediato_id'] = 1339;
        $registro['latitud'] = 19.1043228;
        $registro['longitud'] = -104.3318836;
        $result = $obj->get_distancia_jefe($entidad_empleado,$this->link,$registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(15.5778, round($result,4));

        error::$en_error = false;

        $entidad_empleado = 'empleado';
        $registro = array();
        $registro['fecha'] = '2024-09-05';
        $registro['empleado_id'] = 1401;
        $registro['empleado_jefe_inmediato_id'] = 1339;
        $registro['latitud'] = 19.1045619;
        $registro['longitud'] = -104.3320408;
        $result = $obj->get_distancia_jefe($entidad_empleado,$this->link,$registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        $this->assertEquals(45.0016, round($result,4));
        error::$en_error = false;

    }

    final public function test_get_empleados_full_rpt()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $campo_fecha_ingreso = 'startDate';
        $entidad_empleado = 'ohem';
        $periodo_pago = new stdClass();
        $periodo_pago->fecha_final = '2024-09-01';
        $periodo_pago->fecha_inicial = '2024-09-01';

        $resultado = $obj->get_empleados_full_rpt($campo_fecha_ingreso,$entidad_empleado,$this->link,$periodo_pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('inactivo',$resultado[23409][0]->asistencia);
        $this->assertEquals('DOMINGO',$resultado[23409][0]->dia);
        $this->assertEquals('23409',$resultado[23409][0]->ohem_id);

        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $campo_fecha_ingreso = 'fecha_ingreso';
        $entidad_empleado = 'empleado';
        $periodo_pago = new stdClass();
        $periodo_pago->fecha_final = '2024-09-23';
        $periodo_pago->fecha_inicial = '2024-09-23';

        $resultado = $obj->get_empleados_full_rpt($campo_fecha_ingreso,$entidad_empleado,$this->link,$periodo_pago);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('inactivo',$resultado[1494][0]->asistencia);
        $this->assertEquals('POR ASIGNAR',$resultado[1494][0]->dia);
        $this->assertEquals('1494',$resultado[1494][0]->empleado_id);
        error::$en_error = false;
    }

    final public function test_init_data_checada()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $empleado = array();
        $dia = 'z';
        $resultado = $obj->init_data_checada($dia,$empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('',$resultado['z_APP_AS']);
        $this->assertEquals('inactivo',$resultado['z_JEFE_CHECO']);
        $this->assertEquals('inactivo',$resultado['z_checador']);
        $this->assertEquals('-1',$resultado['z_distancia_jefe']);
        $this->assertEquals('inactivo',$resultado['z_aplica_pago']);


        error::$en_error = false;


    }

    final public function test_init_params()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);
        $registro = array();
        $entidad_empleado = 'ohem';
        $resultado = $obj->init_params($entidad_empleado,$registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(-1, $resultado->latitud);
        $this->assertEquals(-1, $resultado->longitud);
        $this->assertEquals(-1, $resultado->ohem_jefe_inmediato_id);
        $this->assertEquals('', $resultado->fecha);

        error::$en_error = false;

        $registro = new stdClass();
        $resultado = $obj->init_params($entidad_empleado,$registro);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(-1, $resultado->latitud);
        $this->assertEquals(-1, $resultado->longitud);
        $this->assertEquals(-1, $resultado->ohem_jefe_inmediato_id);
        $this->assertEquals('', $resultado->fecha);

        error::$en_error = false;

        $registro = new stdClass();
        $registro->latitud = 10;
        $resultado = $obj->init_params($entidad_empleado,$registro);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(10, $resultado->latitud);
        $this->assertEquals(-1, $resultado->longitud);
        $this->assertEquals(-1, $resultado->ohem_jefe_inmediato_id);
        $this->assertEquals('', $resultado->fecha);


        error::$en_error = false;

        $registro = new stdClass();
        $registro->latitud = 1500;
        $registro->longitud = 100;
        $registro->ohem_jefe_inmediato_id = 2222;
        $registro->fecha = '2020-01-01';
        $resultado = $obj->init_params($entidad_empleado,$registro);
        $this->assertEquals(1500, $resultado->latitud);
        $this->assertEquals(100, $resultado->longitud);
        $this->assertEquals(2222, $resultado->ohem_jefe_inmediato_id);
        $this->assertEquals('2020-01-01', $resultado->fecha);
        error::$en_error = false;

        error::$en_error = false;

        $registro = new stdClass();
        $registro->latitud = 1500;
        $registro->longitud = 100;
        $registro->x_jefe_inmediato_id = 2222;
        $registro->fecha = '2020-01-01';
        $entidad_empleado = 'x';
        $resultado = $obj->init_params($entidad_empleado,$registro);

        $this->assertEquals(1500, $resultado->latitud);
        $this->assertEquals(100, $resultado->longitud);
        $this->assertEquals(2222, $resultado->ohem_jefe_inmediato_id);
        $this->assertEquals('2020-01-01', $resultado->fecha);

        error::$en_error = false;


    }

    final public function test_integra_checada_dia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $asistencias_empleado = array();
        $dia = 'a';
        $dia_fecha = 'c';
        $empleado = array();

        $asistencias_empleado[0] = new stdClass();
        $asistencias_empleado[0]->fecha = 'c';

        $resultado = $obj->integra_checada_dia($asistencias_empleado, $dia,$dia_fecha,$empleado);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(-1,$resultado['a_distancia_jefe']);


        error::$en_error = false;

    }

    final public function test_integra_data_dia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $dia_recorrido = 2;
        $dias = array();
        $fecha_inicial = '2024-11-19';

        $resultado = $obj->integra_data_dia($dia_recorrido,$dias,$fecha_inicial);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('JUEVES',$resultado['2024-11-21']);


        error::$en_error = false;


    }

    final public function test_integra_dias_checada()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $empleado = array();
        $fecha_final = '2024-11-10';
        $fecha_inicial = '2024-11-15';

        $resultado = $obj->integra_dias_checada($empleado,$fecha_final, $fecha_inicial);
        //print_r($resultado);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('2024-11-15',$resultado['VIERNES']);
        $this->assertEquals('2024-11-16',$resultado['SABADO']);


        error::$en_error = false;


    }

    final public function test_parametros_reporte_asistencia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $campo_fecha_ingreso = 'startDate';
        $entidad_empleado = 'ohem';
        $periodo_pago_id = 580;
        $resultado = $obj->parametros_reporte_asistencia($campo_fecha_ingreso,$entidad_empleado,'',
            '',$this->link,$periodo_pago_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals("2024 Semanal 2024-09-17 - 2024-09-23",$resultado->periodo_pago->descripcion);
        $this->assertEquals("Querétaro",$resultado->empleados_full[23552][0]->plaza);
        $this->assertEquals("MARTES",$resultado->empleados_full[23552][0]->dia);
        $this->assertEquals("LEON",$resultado->empleados_actual[21366]->homeCity);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $campo_fecha_ingreso = 'fecha_ingreso';
        $entidad_empleado = 'empleado';
        $periodo_pago_id = 132;
        $resultado = $obj->parametros_reporte_asistencia($campo_fecha_ingreso,$entidad_empleado,'','',$this->link,$periodo_pago_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals("2024 Semanal 2024-08-27 - 2024-09-02",$resultado->periodo_pago->descripcion);
        $this->assertEquals("Manzanillo",$resultado->empleados_full[1478][0]->plaza);
        $this->assertEquals("POR ASIGNAR",$resultado->empleados_full[1478][0]->dia);
        $this->assertEquals("1478",$resultado->empleados_actual[1478]->id);



        error::$en_error = false;
    }

    final public function test_parametros_rpt()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $campo_esquema_name = 'id';
        $empleados_full = array();
        $entidad_empleado = 'ohem';
        $entidad_rel_esquema  ='empleado_esquema';
        $ohem_id = 1;

        $empleados_full[$ohem_id][0] = new stdClass();
        $empleados_full[$ohem_id][0]->fecha = '2020-11-11';
        $empleados_full[$ohem_id][0]->ohem_id = '1000';

        $resultado = $obj->parametros_rpt($campo_esquema_name,$empleados_full,$entidad_empleado,$entidad_rel_esquema,$this->link,$ohem_id);

        //print_r($resultado);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals("2020-11-11",$resultado->ultima_checada->fecha);
        $this->assertEquals("1000",$resultado->ultima_checada->ohem_id);
        $this->assertEquals("2020-11-11",$resultado->primer_checada->fecha);
        $this->assertEquals("1000",$resultado->primer_checada->ohem_id);
        $this->assertEquals("1",$resultado->esquemas->esquema_primer_checada['empleado_esquema_id']);



        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $campo_esquema_name = 'id';
        $empleados_full = array();
        $entidad_empleado = 'empleado';
        $entidad_rel_esquema  ='esquema_empleado';
        $ohem_id = 1;

        $empleados_full[$ohem_id][0] = new stdClass();
        $empleados_full[$ohem_id][0]->fecha = '2022-02-09';
        $empleados_full[$ohem_id][0]->empleado_id = '306';

        $resultado = $obj->parametros_rpt($campo_esquema_name,$empleados_full,$entidad_empleado,$entidad_rel_esquema,$this->link,$ohem_id);



        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals("2022-02-09",$resultado->ultima_checada->fecha);
        $this->assertEquals("306",$resultado->ultima_checada->empleado_id);
        $this->assertEquals("2022-02-09",$resultado->primer_checada->fecha);
        $this->assertEquals("306",$resultado->primer_checada->empleado_id);
        $this->assertEquals("2",$resultado->esquemas->esquema_primer_checada['esquema_empleado_id']);



        error::$en_error = false;
    }

    final public function test_periodo_pago()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $fecha_final = '01';
        $fecha_inicial = '02';
        $periodo_pago_id = -1;

        $resultado = $obj->periodo_pago($fecha_final,$fecha_inicial,$this->link,$periodo_pago_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals("01",$resultado->fecha_final);
        $this->assertEquals("02",$resultado->fecha_inicial);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $periodo_pago_id = 1;

        $resultado = $obj->periodo_pago($fecha_final,$fecha_inicial,$this->link,$periodo_pago_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals("2022-04-11",$resultado->fecha_final);
        $this->assertEquals("2022-04-05",$resultado->fecha_inicial);
        error::$en_error = false;



        error::$en_error = false;
    }

    final public function test_reporte_asistencia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        //$obj = new liberator($obj);


        $campo_fecha_ingreso = 'startDate';
        $entidad_empleado = 'ohem';
        $periodo_pago_id = 432;
        $estructura = 'SAP';
        $resultado = $obj->reporte_asistencia($campo_fecha_ingreso, $entidad_empleado, $estructura,
            '','', $this->link,$periodo_pago_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);


        error::$en_error = false;
    }

    final public function test_params_checada()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $fecha = '2020-01-01';
        $ohem_id = 1;
        $result = $obj->params_checada($entidad_empleado,$fecha,$this->link,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertObjectNotHasProperty('latitud',$result->checada);
        $this->assertFalse($result->tiene_checada);
        error::$en_error = false;

        $entidad_empleado = 'ohem';
        $fecha = '2022-11-11';
        $ohem_id = 10163;
        $result = $obj->params_checada($entidad_empleado,$fecha,$this->link,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertObjectHasProperty('latitud',$result->checada);
        $this->assertObjectHasProperty('longitud',$result->checada);
        $this->assertTrue($result->tiene_checada);
        error::$en_error = false;



        error::$en_error = false;


        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $entidad_empleado = 'empleado';
        $fecha = '2024-09-05';
        $ohem_id = 1401;
        $result = $obj->params_checada($entidad_empleado,$fecha,$this->link,$ohem_id);

        $this->assertTrue($result->tiene_checada);
        $this->assertEquals(19.1045619,$result->checada->latitud);
        $this->assertEquals(-104.3320408,$result->checada->longitud);
        $this->assertEquals(1339,$result->checada->empleado_jefe_inmediato_id);
        error::$en_error = false;

    }

    final public function test_params_checada_jefe()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $entidad_empleado = 'empleado';
        $fecha = '';
        $ohem_id = -1;
        $result = $obj->params_checada_jefe($entidad_empleado,$fecha,$this->link,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);

        error::$en_error = false;
        $entidad_empleado = 'ohem';
        $fecha = '2022-11-11';
        $ohem_id = 10163;
        $result = $obj->params_checada_jefe($entidad_empleado,$fecha,$this->link,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(-100.8234214, $result->checada->longitud);

        error::$en_error = false;


        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $entidad_empleado = 'empleado';
        $fecha = '2024-09-05';
        $ohem_id = 1339;
        $result = $obj->params_checada_jefe($entidad_empleado,$fecha,$this->link,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(-104.3317477, $result->checada->longitud);

        error::$en_error = false;

    }

    final public function test_params_distancia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $entidad_empleado = 'a';
        $asistencia_app_id = 1;
        $result = $obj->params_distancia($asistencia_app_id,$entidad_empleado,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);

        error::$en_error = false;


        $entidad_empleado = 'ohem';
        $asistencia_app_id = 61185;
        $result = $obj->params_distancia($asistencia_app_id,$entidad_empleado,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(42.97,round($result->distancia_jefe,2));


        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $entidad_empleado = 'a';
        $asistencia_app_id = 56;
        $result = $obj->params_distancia($asistencia_app_id,$entidad_empleado,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);


        error::$en_error = false;


        $entidad_empleado = 'empleado';
        $asistencia_app_id = 3073;
        $result = $obj->params_distancia($asistencia_app_id,$entidad_empleado,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertEquals(45.00,round($result->distancia_jefe,2));
        error::$en_error = false;

    }

    final public function test_row_checada_rpt()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);


        $empleado_actual = new stdClass();
        $empleado_actual->empID = 1;
        $empleado_actual->startDate = '2020-01-01';
        $empleado_actual->status = 'STATUS';
        $empleado_actual->nombre_completo = 'name';
        $empleados_full = array();
        $estructura = '  sap ';
        $ohem_id = 24299;
        $empleados_full[24299][0] = new stdClass();
        $empleados_full[24299][0]->fecha = '2024-10-07';
        $empleados_full[24299][0]->ohem_id = '24299';
        $empleados_full[24299][0]->plaza = 'PLAZA';
        $empleados_full[24299][0]->status = 'PRIMER STATUS';

        $result = $obj->row_checada_rpt($empleado_actual,$empleados_full, $estructura,$this->link,$ohem_id);



        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(24299,$result['ohem_id']);
        $this->assertEquals('PRIMER STATUS',$result['status_inicio_periodo']);
        $this->assertEquals('STATUS',$result['status_actual']);
        $this->assertEquals('2024-10-22',$result['esquema_actual_fecha']);


        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $empleado_actual = new stdClass();
        $empleado_actual->empID = 1;
        $empleado_actual->fecha_ingreso = '2020-01-01';
        $empleado_actual->status = 'STATUS';
        $empleado_actual->nombre_completo = 'name';
        $empleados_full = array();
        $estructura = '  em3 ';
        $ohem_id = 1481;
        $empleados_full[1481][0] = new stdClass();
        $empleados_full[1481][0]->fecha = '2024-09-25';
        $empleados_full[1481][0]->empleado_id = '1481';
        $empleados_full[1481][0]->plaza = 'PLAZA';
        $empleados_full[1481][0]->status = 'PRIMER STATUS';

        $result = $obj->row_checada_rpt($empleado_actual,$empleados_full, $estructura,$this->link,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(1481,$result['ohem_id']);
        $this->assertEquals('PRIMER STATUS',$result['status_inicio_periodo']);
        $this->assertEquals('STATUS',$result['status_actual']);
        $this->assertEquals('2024-09-25',$result['esquema_actual_fecha']);

        error::$en_error = false;

    }
    final public function test_subordinados()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $asistencia_row = array();
        $asistencia_row['ohem_jefe_inmediato_id'] = 1660;
        $asistencia_row['ohem_id'] = 1660;
        $asistencia_row['fecha'] = '2024-09-05';
        $result = $obj->subordinados($asistencia_row,$entidad_empleado,$this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(23874,$result[0]->ohem_id);


        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $entidad_empleado = 'empleado';
        $asistencia_row = array();
        $asistencia_row['empleado_jefe_inmediato_id'] = 1339;
        $asistencia_row['empleado_id'] = 1339;
        $asistencia_row['fecha'] = '2024-09-05';
        $result = $obj->subordinados($asistencia_row,$entidad_empleado,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals(1481,$result[0]->empleado_id);
        $this->assertEquals(1401,$result[1]->empleado_id);
        $this->assertEquals(1478,$result[2]->empleado_id);

        error::$en_error = false;

    }

    final public function test_tiene_checada()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $asistencias_empleado = array();
        $dia_fecha = 'a';

        $asistencias_empleado[] = new stdClass();

        $resultado = $obj->tiene_checada($asistencias_empleado,$dia_fecha);

        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);

        error::$en_error = false;

        $asistencias_empleado = array();
        $dia_fecha = 'a';

        $asistencias_empleado[0] = new stdClass();
        $asistencias_empleado[0]->fecha = 'a';

        $resultado = $obj->tiene_checada($asistencias_empleado,$dia_fecha);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);
        error::$en_error = false;

    }

    final public function test_tiene_checada_app()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);
        $entidad_empleado = 'ohem';
        $fecha = '2024-09-02';
        $ohem_id = 23691;
        $resultado = $obj->tiene_checada_app($entidad_empleado,$fecha,$this->link,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $entidad_empleado = 'empleado';
        $fecha = '2024-09-04';
        $ohem_id = 1401;
        $resultado = $obj->tiene_checada_app($entidad_empleado,$fecha,$this->link,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);

        error::$en_error = false;
    }

    final public function test_tiene_jefe()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $entidad_empleado = 'a';
        $asistencia_row = new stdClass();
        $asistencia_row->a_jefe_inmediato_id = '1';
        $result = $obj->tiene_jefe($asistencia_row,$entidad_empleado);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($result);

        error::$en_error = false;

    }

    final public function test_upd_asistencia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $asistencia_app_id = 1;
        $resultado = $obj->upd_asistencia($asistencia_app_id,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("UPDATE asistencia_app SET asistencia = 'activo' WHERE asistencia_app.id=1",$resultado['sql']);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $asistencia_app_id = 1;
        $resultado = $obj->upd_asistencia($asistencia_app_id,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("UPDATE asistencia_app SET asistencia = 'activo' WHERE asistencia_app.id=1",$resultado['sql']);

        error::$en_error = false;
    }

    final public function test_upd_checada_con_jefe()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);
        $asistencia_app = new stdClass();
        $entidad_empleado = 'a';
        $resultado = $obj->upd_checada_con_jefe($asistencia_app, $entidad_empleado,$this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertFalse($resultado->tiene_jefe);


        error::$en_error = false;

        $asistencia_app = new stdClass();
        $entidad_empleado = 'ohem';
        $asistencia_app->ohem_jefe_inmediato_id = 1;
        $asistencia_app->fecha = '2020-01-01';
        $asistencia_app->id = 1;
        $resultado = $obj->upd_checada_con_jefe($asistencia_app, $entidad_empleado,$this->link);
       // print_r($resultado);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado->tiene_checada);

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $asistencia_app = new stdClass();
        $entidad_empleado = 'empleado';
        $asistencia_app->empleado_jefe_inmediato_id = 1;
        $asistencia_app->fecha = '2020-01-01';
        $asistencia_app->id = 1;
        $resultado = $obj->upd_checada_con_jefe($asistencia_app, $entidad_empleado,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado->tiene_checada);
        error::$en_error = false;
    }

    final public function test_upd_checada_desde_asis()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);
        $entidad_empleado = 'ohem';
        $asistencia_app_id = 1;
        $resultado = $obj->upd_checada_desde_asis($asistencia_app_id,$entidad_empleado,$this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertEmpty($resultado);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $entidad_empleado = 'empleado';
        $asistencia_app_id = 56;
        $resultado = $obj->upd_checada_desde_asis($asistencia_app_id,$entidad_empleado,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertEmpty($resultado);

        error::$en_error = false;
    }

    final public function test_upd_checada_en_sitio_activa()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $asistencia_app_id = 61191;
        $result = $obj->upd_checada_en_sitio_activa($asistencia_app_id,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("UPDATE asistencia_app SET checada_en_sitio = 'activo' WHERE asistencia_app.id = 61191", $result['sql']);
        error::$en_error = false;

        $this->link->rollBack();
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();
        $asistencia_app_id = 3063;
        $result = $obj->upd_checada_en_sitio_activa($asistencia_app_id,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals("UPDATE asistencia_app SET checada_en_sitio = 'activo' WHERE asistencia_app.id = 3063", $result['sql']);
        $this->link->rollBack();
        error::$en_error = false;
    }

    final public function test_upd_checada_jefe()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $asistencia_app_id = 1;
        $resultado = $obj->upd_checada_jefe($asistencia_app_id,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("UPDATE asistencia_app SET jefe_checo = 'activo' WHERE id = 1",$resultado['sql']);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $asistencia_app_id = 1;
        $resultado = $obj->upd_checada_jefe($asistencia_app_id,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("UPDATE asistencia_app SET jefe_checo = 'activo' WHERE id = 1",$resultado['sql']);

        error::$en_error = false;
    }



    final public function test_upd_checada_jefe_activa()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $entidad_empleado = 'ohem';
        $asistencia_row = new stdClass();
        $asistencia_row->fecha = '2024-08-31';
        $asistencia_row->ohem_jefe_inmediato_id = 1736;
        $asistencia_row->id = 61173;
        $result = $obj->upd_checada_jefe_activa($asistencia_row,$entidad_empleado,$this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertNotTrue($result->tiene_checada);

        error::$en_error = false;
        $entidad_empleado = 'ohem';
        $asistencia_row = new stdClass();
        $asistencia_row->fecha = '2024-09-05';
        $asistencia_row->ohem_jefe_inmediato_id = 22556;
        $asistencia_row->id = 61173;
        $result = $obj->upd_checada_jefe_activa($asistencia_row,$entidad_empleado,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertTrue($result->tiene_checada);
        $this->assertEquals("UPDATE asistencia_app SET jefe_checo = 'activo' WHERE id = 61173",$result->upd['sql']);

        error::$en_error = false;



        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $entidad_empleado = 'empleado';


        $asistencia_row = new stdClass();
        $asistencia_row->fecha = '2024-09-03';
        $asistencia_row->empleado_jefe_inmediato_id = 1148;
        $asistencia_row->id = 3050;


        $result = $obj->upd_checada_jefe_activa($asistencia_row,$entidad_empleado,$this->link);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertNotTrue($result->tiene_checada);

        error::$en_error = false;


        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $entidad_empleado = 'empleado';
        $asistencia_row = new stdClass();
        $asistencia_row->fecha = '2024-09-03';
        $asistencia_row->empleado_jefe_inmediato_id = 1339;
        $asistencia_row->id = 3053;

        $result = $obj->upd_checada_jefe_activa($asistencia_row,$entidad_empleado,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertTrue($result->tiene_checada);
        $this->assertEquals("UPDATE asistencia_app SET jefe_checo = 'activo' WHERE id = 3053",$result->upd['sql']);

        error::$en_error = false;


        $entidad_empleado = 'empleado';
        $asistencia_row = new stdClass();
        $asistencia_row->fecha = '2024-09-03';
        $asistencia_row->empleado_jefe_inmediato_id = 1339;
        $asistencia_row->id = 3074;
        $result = $obj->upd_checada_jefe_activa($asistencia_row,$entidad_empleado,$this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($result);
        $this->assertTrue($result->tiene_checada);
        $this->assertEquals("UPDATE asistencia_app SET jefe_checo = 'activo' WHERE id = 3074",$result->upd['sql']);
        error::$en_error = false;


    }

    final public function test_valida_asistencia()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $asistencia_app = new stdClass();
        $asistencia_app->fecha = '2024-09-03';
        $asistencia_app->id = '1';


        $resultado = $obj->valida_asistencia($asistencia_app);


        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);


        error::$en_error = false;

    }

    final public function test_valida_checada()
    {
        error::$en_error = false;
        $obj = new asistencia_app();
        $obj = new liberator($obj);

        $entidad_empleado = 'a';
        $asistencia_row = new stdClass();
        $asistencia_row->fecha = 'b';
        $asistencia_row->a_jefe_inmediato_id = '1';
        $asistencia_row->id = '1';

        $result = $obj->valida_checada($asistencia_row,$entidad_empleado);


        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($result);

        error::$en_error = false;

    }




}
