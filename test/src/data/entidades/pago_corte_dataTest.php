<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\data\entidades\pago_corte;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class pago_corte_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }


    final public function test_aplica_validacion()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $pago_corte_id = 116902;
        $resultado = $obj->aplica_validacion($this->link,$pago_corte_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);
        $this->assertIsBool($resultado);
        error::$en_error = false;


        $this->link->rollBack();

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();

        $pago_corte_id = 3856;
        $resultado = $obj->aplica_validacion($this->link,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);
        $this->assertIsBool($resultado);


        $this->link->rollBack();
        error::$en_error = false;
    }

    final public function test_aplica_validado()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $pago_corte_id = 1;
        $aplica_validar_pago_corte = true;
        $resultado = $obj->aplica_validado($aplica_validar_pago_corte,$this->link,$pago_corte_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);
        $this->assertIsBool($resultado);
        error::$en_error = false;

        $pago_corte_id = 116909;
        $aplica_validar_pago_corte = true;
        $resultado = $obj->aplica_validado($aplica_validar_pago_corte,$this->link,$pago_corte_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);
        $this->assertIsBool($resultado);

        $this->link->rollBack();

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();

        $pago_corte_id = 3856;
        $resultado = $obj->aplica_validado($aplica_validar_pago_corte,$this->link,$pago_corte_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);
        $this->assertIsBool($resultado);


        $this->link->rollBack();
        error::$en_error = false;
    }

    final public function test_aplica_validado_transaccion()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $pago_corte_id = 1;
        $pago_corte = new stdClass();
        $pago_corte->monto_por_depositar = 0;
        $pago_corte->monto_depositado = 10;
        $pago_corte->monto_total = 10;
        $resultado = $obj->aplica_validado_transaccion($this->link,$pago_corte,$pago_corte_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);
        $this->assertIsBool($resultado);
        error::$en_error = false;

        $pago_corte_id = 116981;
        $resultado = $obj->aplica_validado_transaccion($this->link,$pago_corte,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);
        $this->assertIsBool($resultado);

        $this->link->rollBack();

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();

        $pago_corte_id = 3856;
        $resultado = $obj->aplica_validado_transaccion($this->link,$pago_corte,$pago_corte_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);
        $this->assertIsBool($resultado);


        $this->link->rollBack();
        error::$en_error = false;
    }

    final public function test_aplica_validar_pago_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $pago_corte = new stdClass();
        $pago_corte->monto_por_depositar = '0';
        $pago_corte->monto_depositado = '1';
        $pago_corte->monto_total = '1';
        $resultado = $obj->aplica_validar_pago_corte($pago_corte);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);

        error::$en_error = false;
    }

    final public function test_cortes_sin_validar()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);

        $limit = 10;
        $resultado = $obj->cortes_sin_validar($this->link,$limit);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(137209,$resultado[0]->id);

        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $limit = 10;
        $resultado = $obj->cortes_sin_validar($this->link,$limit);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(4475,$resultado[0]->id);
        error::$en_error = false;
    }

    final public function test_depositos_validados()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $pago_corte_id = 1;
        $resultado = $obj->depositos_validados($this->link,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);
        $this->assertIsBool($resultado);
        error::$en_error = false;

        $pago_corte_id = 116882;
        $resultado = $obj->depositos_validados($this->link,$pago_corte_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);
        $this->assertIsBool($resultado);

        $this->link->rollBack();

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();

        $pago_corte_id = 3856;
        $resultado = $obj->depositos_validados($this->link,$pago_corte_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);
        $this->assertIsBool($resultado);


        $this->link->rollBack();
        error::$en_error = false;
    }

    final public function test_init_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        //$obj = new liberator($obj);
        $this->link->beginTransaction();
        $pago_corte_id = 116882;
        $campo_canceled = 'Canceled';
        $campo_monto_pago = 'DocTotal';
        $campo_movto = 'U_Movto';
        $resultado = $obj->init_corte($campo_canceled,$campo_monto_pago,$campo_movto,$this->link,$pago_corte_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals("UPDATE pago_corte SET monto_depositado = 13000, monto_total = 13000, monto_por_depositar = 0 WHERE pago_corte.id = 116882",$resultado->recalcula['sql']);
        error::$en_error = false;


        $this->link->rollBack();

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();

        $pago_corte_id = 3856;
        $campo_canceled = '';
        $campo_monto_pago = 'monto';
        $campo_movto = 'movimiento';
        $resultado = $obj->init_corte($campo_canceled,$campo_monto_pago,$campo_movto,$this->link,$pago_corte_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals("UPDATE pago_corte SET monto_depositado = 0, monto_total = 2400, monto_por_depositar = 2400 WHERE pago_corte.id = 3856",$resultado->recalcula['sql']);


        $this->link->rollBack();
        error::$en_error = false;
    }

    final public function test_integra_serie_faltante()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $campo_serie = 'a';
        $faltante = array();
        $faltante['contrato_a'] = '';
        $index = 0;
        $link = 'a';
        $serie_duplicada = array();
        $r_deposito_faltante = array();
        $resultado = $obj->integra_serie_faltante($campo_serie,$faltante,$index,$link,$r_deposito_faltante,$serie_duplicada);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('activo',$resultado[0]['inserta_deposito']);
        $this->assertEquals('a',$resultado[0]['href']);

        error::$en_error = false;

        $campo_serie = 'a';
        $faltante = array();
        $index = 0;
        $link = 'a';
        $serie_duplicada = array();
        $r_deposito_faltante = array();
        $serie_duplicada[0]['serie_codigo'] = 'a';
        $faltante['contrato_a'] = 'a';
        $resultado = $obj->integra_serie_faltante($campo_serie,$faltante,$index,$link,$r_deposito_faltante,$serie_duplicada);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('inactivo',$resultado[0]['inserta_deposito']);
        $this->assertEquals('a',$resultado[0]['href']);
        error::$en_error = false;

    }

    final public function test_link_deposito_aportacion()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $registro_id = 1;
        $session_id = 1;
        $resultado = $obj->link_deposito_aportacion($registro_id,$session_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($resultado);
        $this->assertEquals("index.php?seccion=pago_corte&accion=deposito_aportacion_por_literal&session_id=1&registro_id=1",$resultado);
        error::$en_error = false;


        $this->link->rollBack();

        error::$en_error = false;

    }

    final public function test_montos()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $campo_monto_pago = 'DocTotal';
        $campo_canceled = 'Canceled';
        $campo_movimiento = 'U_Movto';
        $pago_corte_id = 116902;
        $resultado = $obj->montos($campo_canceled,$campo_monto_pago,$campo_movimiento,$this->link,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(7900, $resultado->monto_depositado);
        $this->assertEquals(7900, $resultado->monto_total);
        $this->assertEquals(0, $resultado->monto_por_depositar);

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $campo_monto_pago = 'monto';
        $campo_canceled = '';
        $campo_movimiento = 'movimiento';
        $pago_corte_id = 3857;
        $resultado = $obj->montos($campo_canceled,$campo_monto_pago,$campo_movimiento,$this->link,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0, $resultado->monto_depositado);
        $this->assertEquals(3650, $resultado->monto_total);
        $this->assertEquals(3650, $resultado->monto_por_depositar);
        error::$en_error = false;
    }

    final public function test_pago_corte_base()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        //$obj = new liberator($obj);
        $deposito_pago_id = -1;
        $deposito_aportaciones_id = 77;
        $resultado = $obj->pago_corte_base($deposito_aportaciones_id,$deposito_pago_id, $this->link);


        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(116909, $resultado['id']);
        $this->assertEquals(22239, $resultado['ohem_id']);
        $this->assertEquals(400, $resultado['deposito_aportaciones_monto_depositado']);



        error::$en_error = false;
    }

    final public function test_pagos_por_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        //$obj = new liberator($obj);
        $this->link->beginTransaction();
        $pago_corte_id = 80143;

        $order = 'ORDER BY contrato.U_SeCont ASC, contrato.id DESC';
        $resultado = $obj->pagos_por_corte($this->link,$order,$pago_corte_id,'SAP');


        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(10434970,$resultado[0]->pago_id);
        error::$en_error = false;


        $this->link->rollBack();

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();

        $pago_corte_id = 3856;

        $order = 'ORDER BY contrato.serie';
        $resultado = $obj->pagos_por_corte($this->link,$order,$pago_corte_id, 'em3');
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('MANFF',$resultado[3]->contrato_serie);
        $this->link->rollBack();
        error::$en_error = false;
    }

    final public function test_pagos_por_corte_con_serie()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        //$obj = new liberator($obj);

        $campo_canceled = 'Canceled';
        $campo_monto_pago = 'DocTotal';
        $campo_movimiento = 'U_Movto';
        $pago_corte_id = 13156;
        $resultado = $obj->pagos_por_corte_con_serie($campo_canceled,$campo_monto_pago,$campo_movimiento,'U_SeCont',$this->link,$pago_corte_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(4686220, $resultado[0]->pago_id);

        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $campo_canceled = '';
        $campo_monto_pago = 'monto';
        $campo_movimiento = 'movimiento';
        $pago_corte_id = 3852;
        $resultado = $obj->pagos_por_corte_con_serie($campo_canceled,$campo_monto_pago,$campo_movimiento,'serie',$this->link,$pago_corte_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1101305, $resultado[0]->pago_id);
        error::$en_error = false;
    }

    final public function test_recalcula_cortes_sin_validar()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        //$obj = new liberator($obj);
        $this->link->beginTransaction();
        $campo_canceled = '';
        $campo_monto_pago = 'DocTotal';
        $campo_movto = 'U_Movto';
        $limit = '10';
        $resultado = $obj->recalcula_cortes_sin_validar($campo_canceled,$campo_monto_pago,$campo_movto,$this->link,$limit);
        $this->assertIsObject($resultado[0]);

        $this->link->rollBack();

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();

        $campo_canceled = '';
        $campo_monto_pago = 'monto';
        $campo_movto = 'movimiento';
        $limit = '10';
        $resultado = $obj->recalcula_cortes_sin_validar($campo_canceled,$campo_monto_pago,$campo_movto,$this->link,$limit);
        $this->assertIsObject($resultado[0]);

        $this->link->rollBack();
        error::$en_error = false;
    }

    final public function test_params_em3()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $resultado = $obj->params_em3();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals('',$resultado->campo_canceled);
        $this->assertEquals('folio',$resultado->campo_folio);
        $this->assertEquals('monto',$resultado->campo_monto_pago);
        $this->assertEquals('movimiento',$resultado->campo_movto);
        $this->assertEquals('serie',$resultado->campo_serie);
        $this->assertEquals('empleado',$resultado->entidad_empleado);
        error::$en_error = false;


    }

    final public function test_params_modelo()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $modelo_datos = 'em3';
        $resultado = $obj->params_modelo($modelo_datos);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals('',$resultado->campo_canceled);
        $this->assertEquals('folio',$resultado->campo_folio);
        $this->assertEquals('monto',$resultado->campo_monto_pago);
        $this->assertEquals('movimiento',$resultado->campo_movto);
        $this->assertEquals('serie',$resultado->campo_serie);
        $this->assertEquals('empleado',$resultado->entidad_empleado);
        error::$en_error = false;


    }

    final public function test_params_sap()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $resultado = $obj->params_sap();


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals('Canceled',$resultado->campo_canceled);
        $this->assertEquals('U_FolioCont',$resultado->campo_folio);
        $this->assertEquals('DocTotal',$resultado->campo_monto_pago);
        $this->assertEquals('U_Movto',$resultado->campo_movto);
        $this->assertEquals('U_SeCont',$resultado->campo_serie);
        $this->assertEquals('ohem',$resultado->entidad_empleado);
        error::$en_error = false;


    }

    final public function test_row_deposito_literal()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $campo_serie = 'x';
        $faltante = array();
        $index = 0;
        $r_deposito_faltante = array();
        $serie = array();
        $serie['serie_codigo'] = 'A';
        $faltante['contrato_x'] = 'A';
        $resultado = $obj->row_deposito_literal($campo_serie,$faltante,$index,$r_deposito_faltante,$serie);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('inactivo',$resultado[0]['inserta_deposito']);


        error::$en_error = false;
    }

    final public function test_todos_validados()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $deposito_aportaciones = array();
        $deposito_aportaciones[]['validado'] = 'activo';
        $deposito_aportaciones[]['validado'] = 'activo';
        $deposito_aportaciones[]['validado'] = 'activo';
        $deposito_aportaciones[]['validado'] = 'activo';
        $resultado = $obj->todos_validados($deposito_aportaciones);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);

        $deposito_aportaciones = array();
        $deposito_aportaciones[]['validado'] = 'activo';
        $deposito_aportaciones[]['validado'] = 'activo';
        $deposito_aportaciones[]['validado'] = 'inactivo';
        $deposito_aportaciones[]['validado'] = 'activo';
        $resultado = $obj->todos_validados($deposito_aportaciones);
        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);

        error::$en_error = false;
    }


    final public function test_update_montos()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $monto_depositado = '0';
        $monto_por_depositar = '0';
        $monto_total = '0';
        $pago_corte_id = 80143;
        $resultado = $obj->update_montos($this->link,$monto_depositado,$monto_por_depositar,$monto_total,$pago_corte_id);
        $this->link->rollBack();

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("UPDATE pago_corte SET monto_depositado = 0, monto_total = 0, monto_por_depositar = 0 WHERE pago_corte.id = 80143", $resultado['sql']);

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();

        $pago_corte_id = 3857;
        $resultado = $obj->update_montos($this->link,$monto_depositado,$monto_por_depositar,$monto_total,$pago_corte_id);
        $this->assertEquals("UPDATE pago_corte SET monto_depositado = 0, monto_total = 0, monto_por_depositar = 0 WHERE pago_corte.id = 3857", $resultado['sql']);
        $this->link->rollBack();
        error::$en_error = false;
    }

    final public function test_valida_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        $obj = new liberator($obj);
        $this->link->beginTransaction();
        $pago_corte_id = 116909;
        $resultado = $obj->valida_corte($this->link,$pago_corte_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);
        $this->assertIsBool($resultado);
        error::$en_error = false;


        $this->link->rollBack();

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();

        $pago_corte_id = 3856;
        $resultado = $obj->valida_corte($this->link,$pago_corte_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);
        $this->assertIsBool($resultado);


        $this->link->rollBack();
        error::$en_error = false;
    }



    final public function test_validar_pago_corte()
    {
        error::$en_error = false;
        $obj = new pago_corte();
        //$obj = new liberator($obj);
        $this->link->beginTransaction();
        $pago_corte_id = 116909;
        $resultado = $obj->validar_pago_corte($this->link,$pago_corte_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals('activo',$resultado->validado);
        error::$en_error = false;


        $this->link->rollBack();

        error::$en_error = false;
        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $this->link->beginTransaction();

        $pago_corte_id = 3856;
        $resultado = $obj->validar_pago_corte($this->link,$pago_corte_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals('inactivo',$resultado->validado);


        $this->link->rollBack();
        error::$en_error = false;
    }




}
