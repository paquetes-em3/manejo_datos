<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\data\_childrens;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use desarrollo_em3\manejo_datos\sql\empleado;
use PHPUnit\Framework\TestCase;
use stdClass;

class contrato_comision_dataTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;




    }


    /**
     * por finalizar
     */
    final public function test_aplica_comision_alterna()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\contrato_comision();
        //$obj = new liberator($obj);
        $cadena = array();
        $data_comi_alt = new stdClass();
        $key_empleado_id = 'a';
        $cadena['a'] = 'x';
        $resultado = $obj->aplica_comision_alterna($cadena,$data_comi_alt,$key_empleado_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);



        error::$en_error = false;

        $cadena = array();
        $data_comi_alt = new stdClass();
        $key_empleado_id = 'ohem_id';
        $cadena['ohem_id'] = 1;
        $data_comi_alt->empleado_jefe_inmediato_id = 1;
        $data_comi_alt->aplica_comision_adjunto = true;
        $data_comi_alt->aplica_coordinador = true;
        $resultado = $obj->aplica_comision_alterna($cadena,$data_comi_alt,$key_empleado_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue( $resultado);

        error::$en_error = false;
    }

    final public function test_data_comi_alt()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\contrato_comision();
        //$obj = new liberator($obj);
        $key_empleado_id = 'ohem_id';
        $registros_cadena_comision = array();
        $registros_cadena_comision[0]['esquema_es_adjunto'] = 'inactivo';
        $resultado = $obj->data_comi_alt($key_empleado_id,$registros_cadena_comision);

        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado->aplica_comision_adjunto);
        $this->assertFalse( $resultado->aplica_coordinador);


        error::$en_error = false;

        $key_empleado_id = 'empleado_id';
        $registros_cadena_comision = array();
        $registros_cadena_comision[0]['esquema_es_adjunto'] = 'activo';
        $registros_cadena_comision[0]['empleado_id'] = '1';
        $registros_cadena_comision[0]['esquema_aplica_coordinador'] = 'activo';
        $registros_cadena_comision[0]['empleado_jefe_inmediato_id'] = '1';
        $resultado = $obj->data_comi_alt($key_empleado_id,$registros_cadena_comision);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue( $resultado->aplica_comision_adjunto);
        $this->assertTrue( $resultado->aplica_coordinador);

        error::$en_error = false;
    }

    final public function test_soy_coord_ajunto()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\contrato_comision();
        $obj = new liberator($obj);

        error::$en_error = false;

        $data_comi_alt = new stdClass();
        $resultado = $obj->soy_coord_ajunto($data_comi_alt);

        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);
        error::$en_error = false;


        $data_comi_alt = new stdClass();
        $data_comi_alt->aplica_comision_adjunto = '';
        $resultado = $obj->soy_coord_ajunto($data_comi_alt);
        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);
        error::$en_error = false;



        $data_comi_alt = new stdClass();
        $data_comi_alt->aplica_comision_adjunto = '';
        $data_comi_alt->aplica_coordinador = '';
        $resultado = $obj->soy_coord_ajunto($data_comi_alt);
        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);

        error::$en_error = false;

        $data_comi_alt = new stdClass();
        $data_comi_alt->aplica_comision_adjunto = false;
        $data_comi_alt->aplica_coordinador = '';
        $resultado = $obj->soy_coord_ajunto($data_comi_alt);
        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);

        error::$en_error = false;

        $data_comi_alt = new stdClass();
        $data_comi_alt->aplica_comision_adjunto = false;
        $data_comi_alt->aplica_coordinador = false;
        $resultado = $obj->soy_coord_ajunto($data_comi_alt);
        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);

        error::$en_error = false;

        $data_comi_alt = new stdClass();
        $data_comi_alt->aplica_comision_adjunto = true;
        $data_comi_alt->aplica_coordinador = false;
        $resultado = $obj->soy_coord_ajunto($data_comi_alt);
        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);

        error::$en_error = false;

        $data_comi_alt = new stdClass();
        $data_comi_alt->aplica_comision_adjunto = true;
        $data_comi_alt->aplica_coordinador = true;
        $resultado = $obj->soy_coord_ajunto($data_comi_alt);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue( $resultado);
        error::$en_error = false;
    }

    final public function test_es_comision_alterna()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\contrato_comision();
        $obj = new liberator($obj);

        error::$en_error = false;

        $data_comi_alt = new stdClass();
        $cadena = array();
        $cadena['ohem_id'] = 1;
        $key_empleado_id = 'ohem_id';
        $tiene_jefe_inmediato = false;
        $resultado = $obj->es_comision_alterna($cadena,$data_comi_alt,$key_empleado_id,$tiene_jefe_inmediato);

        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);

        error::$en_error = false;
        $data_comi_alt = new stdClass();
        $cadena = array();
        $key_empleado_id = 'ohem_id';
        $cadena['ohem_id'] = 1;
        $tiene_jefe_inmediato = true;
        $resultado = $obj->es_comision_alterna($cadena,$data_comi_alt,$key_empleado_id,$tiene_jefe_inmediato);
        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);

        error::$en_error = false;
        $data_comi_alt = new stdClass();
        $cadena = array();
        $key_empleado_id = 'ohem_id';
        $cadena['ohem_id'] = 1;
        $tiene_jefe_inmediato = true;
        $data_comi_alt->empleado_jefe_inmediato_id = 10;
        $resultado = $obj->es_comision_alterna($cadena,$data_comi_alt,$key_empleado_id,$tiene_jefe_inmediato);
        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);

        error::$en_error = false;
        $data_comi_alt = new stdClass();
        $cadena = array();
        $key_empleado_id = 'ohem_id';
        $cadena['ohem_id'] = 1;
        $tiene_jefe_inmediato = true;
        $data_comi_alt->empleado_jefe_inmediato_id = 1;
        $data_comi_alt->aplica_comision_adjunto = false;
        $data_comi_alt->aplica_coordinador = false;
        $resultado = $obj->es_comision_alterna($cadena,$data_comi_alt,$key_empleado_id,$tiene_jefe_inmediato);
        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);

        error::$en_error = false;
        $data_comi_alt = new stdClass();
        $cadena = array();
        $key_empleado_id = 'ohem_id';
        $cadena['ohem_id'] = 1;
        $tiene_jefe_inmediato = true;
        $data_comi_alt->empleado_jefe_inmediato_id = 1;
        $data_comi_alt->aplica_comision_adjunto = true;
        $data_comi_alt->aplica_coordinador = true;
        $resultado = $obj->es_comision_alterna($cadena,$data_comi_alt,$key_empleado_id,$tiene_jefe_inmediato);
        $this->assertNotTrue(error::$en_error);
        $this->assertTrue( $resultado);

        error::$en_error = false;
    }

    final public function test_soy_jefe()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\contrato_comision();
        $obj = new liberator($obj);

        $data_comi_alt = new stdClass();
        $cadena = array();
        $key_empleado_id = 'empleado_id';
        $cadena['empleado_id'] = 10;
        $resultado = $obj->soy_jefe($cadena,$data_comi_alt,$key_empleado_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);
        error::$en_error = false;
        $data_comi_alt = new stdClass();
        $cadena = array();
        $key_empleado_id = 'empleado_id';
        $cadena['empleado_id'] = 10;
        $data_comi_alt->empleado_jefe_inmediato_id = 1;
        $resultado = $obj->soy_jefe($cadena,$data_comi_alt,$key_empleado_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);

        error::$en_error = false;
        $data_comi_alt = new stdClass();
        $cadena = array();
        $key_empleado_id = 'empleado_id';
        $cadena['empleado_id'] = 10;
        $data_comi_alt->empleado_jefe_inmediato_id = '10';
        $resultado = $obj->soy_jefe($cadena,$data_comi_alt,$key_empleado_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue( $resultado);

        error::$en_error = false;
    }

    final public function test_tiene_jefe_inmediato()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\contrato_comision();
        $obj = new liberator($obj);

        $data_comi_alt = new stdClass();
        $resultado = $obj->tiene_jefe_inmediato($data_comi_alt);

        $this->assertNotTrue(error::$en_error);
        $this->assertFalse( $resultado);


        error::$en_error = false;
        $data_comi_alt = new stdClass();
        $data_comi_alt->empleado_jefe_inmediato_id = 'x';
        $resultado = $obj->tiene_jefe_inmediato($data_comi_alt);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue( $resultado);

        error::$en_error = false;
    }



}
