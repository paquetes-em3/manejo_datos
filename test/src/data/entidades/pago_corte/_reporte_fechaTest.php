<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\data\_childrens;
use desarrollo_em3\manejo_datos\data\entidades\contrato_comision\_data_comi_alterna;
use desarrollo_em3\manejo_datos\data\entidades\pago_corte\_reporte_fecha;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use desarrollo_em3\manejo_datos\sql\empleado;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class _reporte_fechaTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_acumula_cuentas_totales()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $cuentas = [
            'reporte_financiero' => 'Cuenta de reporte financiero',
            'reporte_operativo' => 'Cuenta de reporte operativo',
            'reporte_test' => 'Cuenta de reporte operativo'
        ];
        $empleados = [
            123 => (object)[
                'depositos' => [
                    (object)['cuenta_empresa_alias' => 'reporte_financiero', 'deposito_aportaciones_monto_depositado' => 100],
                    (object)['cuenta_empresa_alias' => 'reporte_operativo', 'deposito_aportaciones_monto_depositado' => 200],
                    (object)['cuenta_empresa_alias' => 'reporte_operativo', 'deposito_aportaciones_monto_depositado' => 200],
                    (object)['cuenta_empresa_alias' => 'reporte_test', 'deposito_aportaciones_monto_depositado' => 800],
                    (object)['cuenta_empresa_alias' => 'reporte_test', 'deposito_aportaciones_monto_depositado' => 500]
                ]
            ]
        ];


        $resultado = $obj->acumula_cuentas_totales($cuentas,$empleados);



        $this->assertNotTrue(error::$en_error);
        $this->assertEquals("reporte_financiero", $resultado[123]->depositos[0]->cuenta_empresa_alias);
        $this->assertEquals(100, $resultado[123]->depositos[0]->deposito_aportaciones_monto_depositado);

        $this->assertEquals("reporte_operativo", $resultado[123]->depositos[1]->cuenta_empresa_alias);
        $this->assertEquals(200, $resultado[123]->depositos[1]->deposito_aportaciones_monto_depositado);

        $this->assertEquals("reporte_operativo", $resultado[123]->depositos[2]->cuenta_empresa_alias);
        $this->assertEquals(200, $resultado[123]->depositos[1]->deposito_aportaciones_monto_depositado);

        $this->assertEquals(100, $resultado[123]->reporte_financiero->deposito_aportaciones_monto_depositado);
        $this->assertEquals(400, $resultado[123]->reporte_operativo->deposito_aportaciones_monto_depositado);

        $this->assertEquals(1300, $resultado[123]->reporte_test->deposito_aportaciones_monto_depositado);


        error::$en_error = false;

    }
    final public function test_acumula_monto_cuenta()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $alias = 'a';
        $deposito = new stdClass();
        $deposito->cuenta_empresa_alias = 'a';
        $empleados = array();
        $empleados[1] = new stdClass();
        $empleados[1]->a = new stdClass();
        $empleados[1]->a->deposito_aportaciones_monto_depositado = 10;
        $ohem_id = 1;
        $deposito->deposito_aportaciones_monto_depositado = 10;
        $resultado = $obj->acumula_monto_cuenta($alias,$deposito,$empleados,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(20, $resultado[1]->a->deposito_aportaciones_monto_depositado);


        error::$en_error = false;

    }

    final public function test_acumula_monto_cuenta_deposito()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $alias = 'z';
        $empleado = new stdClass();
        $empleados = array();
        $ohem_id = 0;
        $empleado->depositos = array();
        $empleado->depositos[0] = new stdClass();
        $empleado->depositos[0]->cuenta_empresa_alias = 'z';
        $empleado->depositos[0]->deposito_aportaciones_monto_depositado = '0';

        $empleado->depositos[1] = new stdClass();
        $empleado->depositos[1]->cuenta_empresa_alias = 'z';
        $empleado->depositos[1]->deposito_aportaciones_monto_depositado = '1';

        $empleado->depositos[2] = new stdClass();
        $empleado->depositos[2]->cuenta_empresa_alias = 'z';
        $empleado->depositos[2]->deposito_aportaciones_monto_depositado = '1';

        $empleado->depositos[3] = new stdClass();
        $empleado->depositos[3]->cuenta_empresa_alias = 'y';
        $empleado->depositos[3]->deposito_aportaciones_monto_depositado = '1';

        $empleados[0] = new stdClass();
        $empleados[0]->z = new stdClass();
        $empleados[0]->z->deposito_aportaciones_monto_depositado = 1;



        $resultado = $obj->acumula_monto_cuenta_deposito($alias,$empleado,$empleados,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(3, $resultado[0]->z->deposito_aportaciones_monto_depositado);


        error::$en_error = false;

    }

    final public function test_acumula_monto_dep()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $alias = 'a';
        $deposito = new stdClass();
        $empleados = array();
        $ohem_id = 0;
        $empleados[0] = new stdClass();
        $empleados[0]->a = new stdClass();
        $empleados[0]->a->deposito_aportaciones_monto_depositado = '1';
        $deposito->deposito_aportaciones_monto_depositado = 1;

        $resultado = $obj->acumula_monto_dep($alias,$deposito,$empleados,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(2, $resultado[0]->a->deposito_aportaciones_monto_depositado);


        error::$en_error = false;

    }

    final public function test_acumula_monto_cuentas()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $cuentas = array();
        $empleado = new stdClass();
        $empleados = array();
        $ohem_id = 1;

        $cuentas['x'] = '';
        $empleado->depositos = array();
        $empleado->depositos['x'] = new stdClass();
        $empleado->depositos['x']->cuenta_empresa_alias = 'A';
        $empleado->depositos['x']->deposito_aportaciones_monto_depositado = '1';

        $empleados['1'] = new stdClass();
        $empleados['1']->x = new stdClass();
        $empleados['1']->x->deposito_aportaciones_monto_depositado = '0';


        $resultado = $obj->acumula_monto_cuentas($cuentas,$empleado,$empleados,$ohem_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0, $resultado[1]->x->deposito_aportaciones_monto_depositado);


        error::$en_error = false;

    }

    final public function test_acumula_totales()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);

        $corte = new stdClass();
        $datos = new stdClass();
        $ohem_id = 100;
        $corte->pago_corte_monto_total = 50;
        $resultado = $obj->acumula_totales($corte,$datos,$ohem_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals(50,$resultado->empleados[100]->pago_corte_monto_total);
        $this->assertEquals(0,$resultado->empleados[100]->pago_corte_monto_por_depositar);
        $this->assertEquals(0,$resultado->empleados[100]->pago_corte_monto_depositado);

        error::$en_error = false;

    }

    final public function test_acumula_totales_corte()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);

        $empleado = new stdClass();
        $datos = new stdClass();
        $ohem_id = -1;

        $empleado->cortes[0] = new stdClass();
        $empleado->cortes[0]->pago_corte_monto_por_depositar = 300;

        $resultado = $obj->acumula_totales_corte($empleado,$datos,$ohem_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals(0,$resultado->empleados[-1]->pago_corte_monto_total);
        $this->assertEquals(300,$resultado->empleados[-1]->pago_corte_monto_por_depositar);
        $this->assertEquals(0,$resultado->empleados[-1]->pago_corte_monto_depositado);

        error::$en_error = false;

    }

    final public function test_cortes()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $empleados_id = array();
        $entidad_empleado = 'ohem';
        $fecha_final = '2024-09-30';
        $fecha_inicial = '2024-09-01';
        $plazas_id = array();
        $empleados_id[] = '16968';
        $empleados_id[] = '23408';

        $plazas_id[] = '23';
        $resultado = $obj->cortes($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial,$this->link,$plazas_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);

        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;
        $empleados_id = array();
        $entidad_empleado = 'empleado';
        $fecha_final = '2024-09-30';
        $fecha_inicial = '2024-09-01';
        $plazas_id = array();


        $resultado = $obj->cortes($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial,$this->link,$plazas_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        error::$en_error = false;


    }

    final public function test_cuenta_by_empleado()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $cuentas = array();
        $ohem_id = 1;
        $empleado = new stdClass();
        $empleado->depositos = array();
        $empleado->depositos[0] = new stdClass();
        $empleado->depositos[0]->cuenta_empresa_alias = 'x';
        $empleado->depositos[1] = new stdClass();
        $empleado->depositos[1]->cuenta_empresa_alias = 'x';
        $empleado->depositos[2] = new stdClass();
        $empleado->depositos[2]->cuenta_empresa_alias = 'y';

        $resultado = $obj->cuenta_by_empleado($cuentas,$empleado,$ohem_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado['x']);
        $this->assertIsObject($resultado['y']);


        error::$en_error = false;

    }

    final public function test_cuenta_nueva()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $cuenta_empresa_alias = 'z';
        $cuentas = array();
        $cuentas['z'] = '';
        $cuentas['b'] = '';

        $resultado = $obj->cuenta_nueva($cuenta_empresa_alias,$cuentas);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado['z']);


        error::$en_error = false;

    }

    final public function test_datos_base_rpt()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $empleados_id = array();
        $entidad_empleado = 'ohem';
        $fecha_final = '2024-09-30';
        $fecha_inicial = '2024-09-10';
        $plazas_id = array();

        $resultado = $obj->datos_base_rpt($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial,$this->link,$plazas_id);
        //print_r($resultado);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(8400, $resultado->cortes[110]->pago_corte_monto_total);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $empleados_id = array();
        $entidad_empleado = 'empleado';
        $fecha_final = '2024-09-30';
        $fecha_inicial = '2024-09-10';
        $plazas_id = array();

        $resultado = $obj->datos_base_rpt($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial,$this->link,$plazas_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(4200, $resultado->cortes[1]->pago_corte_monto_por_depositar);
        error::$en_error = false;
    }

    final public function test_datos_reporte_corte()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);

        $datos = new stdClass();
        $datos->empleados[0] = new stdClass();
        $datos->empleados[0]->cortes = array();
        $datos->empleados[0]->cortes[0] = new stdClass();
        $datos->empleados[0]->cortes[0]->pago_corte_monto_depositado = 687;

        $datos->empleados[0]->cortes[1] = new stdClass();
        $datos->empleados[0]->cortes[1]->pago_corte_monto_depositado = 300;

        $datos->empleados[0]->cortes[2] = new stdClass();
        $datos->empleados[0]->cortes[2]->pago_corte_monto_por_depositar = 555;

        $resultado = $obj->datos_reporte_corte($datos);


        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(687, $resultado->empleados[0]->cortes[0]->pago_corte_monto_depositado);
        $this->assertEquals(0, $resultado->empleados[0]->cortes[0]->pago_corte_monto_por_depositar);
        $this->assertEquals(0, $resultado->empleados[0]->cortes[0]->pago_corte_monto_total);

        $this->assertEquals(0, $resultado->empleados[0]->pago_corte_monto_total);
        $this->assertEquals(555, $resultado->empleados[0]->pago_corte_monto_por_depositar);
        $this->assertEquals(987, $resultado->empleados[0]->pago_corte_monto_depositado);




        error::$en_error = false;
    }

    final public function test_depositos()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $empleados_id = array();
        $entidad_empleado = 'ohem';
        $fecha_final = '2024-09-30';
        $fecha_inicial = '2024-09-01';
        $plazas_id = array();

        $resultado = $obj->depositos($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial,$this->link,$plazas_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(13000, $resultado[0]->pago_corte_monto_depositado);


        error::$en_error = false;

        $conexion = (new conexion_db_mzo());
        $this->link = $conexion->link;

        $empleados_id = array();
        $entidad_empleado = 'empleado';
        $fecha_final = '2024-09-30';
        $fecha_inicial = '2024-09-01';
        $plazas_id = array();

        $resultado = $obj->depositos($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial,$this->link,$plazas_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        error::$en_error = false;
    }

    final public function test_format_json()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);

        $cuenta_alias = 'x';
        $empleado_rpt = array();
        $resultado = $obj->format_json($cuenta_alias,$empleado_rpt);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0,$resultado['cuentas']['x']);


        error::$en_error = false;

    }

    final public function test_inicializa_empleados_rpt()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $cortes = array();
        $depositos = array();

        $cortes[0] = new stdClass();
        $cortes[0]->ohem_id = 1;
        $cortes[0]->ohem_nombre_completo = 1;
        $cortes[0]->plaza_descripcion = 1;
        $cortes[0]->plaza_id = 1;

        $depositos[2] = new stdClass();
        $depositos[2]->ohem_id = 2;
        $depositos[2]->ohem_nombre_completo = 2;
        $depositos[2]->plaza_descripcion = 2;
        $depositos[2]->plaza_id = 2;

        $resultado = $obj->inicializa_empleados_rpt($cortes,$depositos);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado[1]);
        $this->assertIsObject($resultado[2]);


        error::$en_error = false;

    }

    final public function test_init_cuenta_empleado()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $alias = 'x';
        $empleados = array();
        $ohem_id = 1;
        $resultado = $obj->init_cuenta_empleado($alias,$empleados,$ohem_id);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(0,$resultado[1]->x->deposito_aportaciones_monto_depositado);
        error::$en_error = false;

    }
    final public function test_init_cuenta_nueva()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $cuenta_empresa_alias = 'b';
        $cuentas = array();
        $ohem_id = 1;

        $resultado = $obj->init_cuenta_nueva($cuenta_empresa_alias,$cuentas,$ohem_id);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado['b']);


        error::$en_error = false;

    }

    final public function test_init_empleado_rpt()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $base = new stdClass();
        $base->ohem_id = '1';
        $base->ohem_nombre_completo = 'c';
        $base->plaza_descripcion = 'r';
        $base->plaza_id = '1';

        $resultado = $obj->init_empleado_rpt($base);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);


        error::$en_error = false;

    }

    final public function test_integra_cuenta()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $cuenta_alias = 'a';
        $empleado = new stdClass();
        $empleado_rpt = array();

        $resultado = $obj->integra_cuenta($cuenta_alias,$empleado, $empleado_rpt,'');


        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(0,$resultado['a']);

        error::$en_error = false;

        $cuenta_alias = 'a';
        $empleado = new stdClass();
        $empleado_rpt = array();
        $empleado->a = new stdClass();
        $empleado->a->deposito_aportaciones_monto_depositado = 15;

        $resultado = $obj->integra_cuenta($cuenta_alias,$empleado, $empleado_rpt,'');

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(15,$resultado['a']);
        error::$en_error = false;

        $cuenta_alias = 'a';
        $empleado = new stdClass();
        $empleado_rpt = array();
        $empleado->a = new stdClass();
        $empleado->a->deposito_aportaciones_monto_depositado = 15;

        $resultado = $obj->integra_cuenta($cuenta_alias,$empleado, $empleado_rpt,'JSON');
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(15,$resultado['a']);
        $this->assertEquals(15,$resultado['cuentas' ]['a']);
        error::$en_error = false;


    }

    final public function test_integra_cuenta_empleado()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $alias = 'z';
        $empleados = array();
        $ohem_id = 1000;
        $resultado = $obj->integra_cuenta_empleado($alias,$empleados,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(0,$resultado[1000]->z->deposito_aportaciones_monto_depositado);
        error::$en_error = false;

    }

    final public function test_integra_cuenta_por_empleado()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $cuentas = array();
        $empleados = array();
        $ohem_id = 111;
        $cuentas['xxx'] = '';
        $cuentas['yyyy'] = '';
        $resultado = $obj->integra_cuenta_por_empleado($cuentas,$empleados,$ohem_id);
       // print_r($resultado);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(0,$resultado[111]->xxx->deposito_aportaciones_monto_depositado);
        $this->assertEquals(0,$resultado[111]->yyyy->deposito_aportaciones_monto_depositado);
        error::$en_error = false;

    }

    final public function test_integra_cuentas()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $cuentas = array();
        $empleados = array();
        $empleados[123] = new stdClass();
        $empleados[456] = new stdClass();
        $cuentas['sss'] = '';
        $cuentas['zzz'] = '';
        $cuentas['yyy'] = '';
        $resultado = $obj->integra_cuentas($cuentas,$empleados);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(0,$resultado[123]->sss->deposito_aportaciones_monto_depositado);
        $this->assertEquals(0,$resultado[123]->zzz->deposito_aportaciones_monto_depositado);
        $this->assertEquals(0,$resultado[123]->yyy->deposito_aportaciones_monto_depositado);
        $this->assertEquals(0,$resultado[456]->sss->deposito_aportaciones_monto_depositado);
        $this->assertEquals(0,$resultado[456]->zzz->deposito_aportaciones_monto_depositado);
        $this->assertEquals(0,$resultado[456]->yyy->deposito_aportaciones_monto_depositado);

        error::$en_error = false;

    }

    final public function test_integra_cuentas_empleado()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $cuenta_alias = 'b';
        $empleado = new stdClass();
        $empleado_rpt = array();

        $resultado = $obj->integra_cuentas_empleado($cuenta_alias,$empleado, $empleado_rpt,'');

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(0,$resultado['b']);

        error::$en_error = false;

        $cuenta_alias = 'b';
        $empleado = new stdClass();
        $empleado_rpt = array();
        $empleado_rpt['b'] = '2';

        $resultado = $obj->integra_cuentas_empleado($cuenta_alias,$empleado, $empleado_rpt,'');
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(2,$resultado['b']);

        error::$en_error = false;

        $cuenta_alias = 'b';
        $empleado = new stdClass();
        $empleado_rpt = array();
        $empleado_rpt['b'] = '2';
        $empleado->b = new stdClass();
        $empleado->b->deposito_aportaciones_monto_depositado =20;

        $resultado = $obj->integra_cuentas_empleado($cuenta_alias,$empleado, $empleado_rpt,'');

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(20,$resultado['b']);
        error::$en_error = false;

        $cuenta_alias = 'b';
        $empleado = new stdClass();
        $empleado_rpt = array();
        $empleado_rpt['b'] = '2';
        $empleado->b = new stdClass();
        $empleado->b->deposito_aportaciones_monto_depositado =20;

        $resultado = $obj->integra_cuentas_empleado($cuenta_alias,$empleado, $empleado_rpt,'JSON');
        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(20,$resultado['b']);
        $this->assertEquals(20,$resultado['cuentas']['b']);
        error::$en_error = false;



    }

    final public function test_integra_empleado_rpt()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $base = new stdClass();
        $base->ohem_id = '1';
        $base->ohem_nombre_completo = '2';
        $base->plaza_descripcion = '3';
        $base->plaza_id = '4';
        $empleados = array();
        $resultado = $obj->integra_empleado_rpt($base,$empleados);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado[1]);


        error::$en_error = false;

    }

    final public function test_integra_empleado_rpt_new()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $base = new stdClass();
        $base->ohem_id = '1';
        $base->ohem_nombre_completo = '1';
        $base->plaza_descripcion = '1';
        $base->plaza_id = '1';
        $empleados = array();
        $resultado = $obj->integra_empleado_rpt_new($base,$empleados);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado[1]);


        error::$en_error = false;

    }

    final public function test_integra_empleados_rpt_news()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $empleados = array();
        $rows = array();
        $rows[0] = new stdClass();
        $rows[0]->ohem_id = 1;
        $rows[0]->ohem_nombre_completo = 1;
        $rows[0]->plaza_descripcion = 1;
        $rows[0]->plaza_id = 1;
        $resultado = $obj->integra_empleados_rpt_news($empleados,$rows);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado[1]);


        error::$en_error = false;

    }

    final public function test_integra_key_rpt()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);

        $empleado = new stdClass();
        $empleado_rpt = array();
        $key_integra = 'a';
        $empleado->a = '';
        $resultado = $obj->integra_key_rpt($empleado,$empleado_rpt,$key_integra);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('',$resultado['a']);

        error::$en_error = false;



    }

    final public function test_integra_row_rpt()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);

        $empleados = array();
        $name_var_row = 'v';
        $ohem_id = '1';
        $row = new stdClass();
        $empleados[1] = new stdClass();
        $resultado = $obj->integra_row_rpt($empleados,$name_var_row,$ohem_id,$row);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado[1]);


        error::$en_error = false;

    }

    final public function test_integra_row_rpt_new()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);

        $empleados = array();
        $name_var_row = 'x';
        $ohem_id = 1;
        $row = new stdClass();
        $row->ohem_id = 1;
        $empleados[1]= new stdClass();
        $resultado = $obj->integra_row_rpt_new($empleados,$name_var_row,$ohem_id,$row);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado[1]);


        error::$en_error = false;

    }

    final public function test_integra_rows_new()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);

        $datos = new stdClass();
        $empleados = array();
        $name_var_row = 'cortes';
        $ohem_id = 1;
        $datos->cortes = array();
        $datos->cortes[1] = new stdClass();
        $datos->cortes[1]->ohem_id = 1;
        $empleados[1] = new stdClass();
        $resultado = $obj->integra_rows_new($datos,$empleados,$name_var_row,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);


        error::$en_error = false;

    }

    final public function test_integra_total_cuentas()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);

        $datos = new stdClass();
        $empleado = new stdClass();
        $empleado_rpt = array();
        $format = '';
        $datos->cuentas = array();
        $datos->cuentas['x'] = '';
        $datos->cuentas['y'] = '';
        $empleado->x = new stdClass();
        $empleado->x->deposito_aportaciones_monto_depositado = 1;

        $empleado->y = new stdClass();
        $empleado->y->deposito_aportaciones_monto_depositado = 2;

        $resultado = $obj->integra_total_cuentas($datos,$empleado,$empleado_rpt,$format);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(1,$resultado['x']);
        $this->assertEquals(2,$resultado['y']);

        error::$en_error = false;

        $datos = new stdClass();
        $empleado = new stdClass();
        $empleado_rpt = array();
        $format = 'JSON';
        $datos->cuentas = array();
        $datos->cuentas['x'] = '';
        $datos->cuentas['y'] = '';
        $empleado->x = new stdClass();
        $empleado->x->deposito_aportaciones_monto_depositado = 1;

        $empleado->y = new stdClass();
        $empleado->y->deposito_aportaciones_monto_depositado = 2;

        $resultado = $obj->integra_total_cuentas($datos,$empleado,$empleado_rpt,$format);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals(1,$resultado['x']);
        $this->assertEquals(2,$resultado['y']);
        $this->assertEquals(1,$resultado['cuentas']['x']);
        $this->assertEquals(2,$resultado['cuentas']['y']);
        error::$en_error = false;



    }

    final public function test_key_rpt()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);

        $resultado = $obj->key_rpt();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('ohem_id',$resultado[0]);
        $this->assertEquals('ohem_nombre_completo',$resultado[1]);
        $this->assertEquals('plaza_id',$resultado[2]);
        $this->assertEquals('plaza_descripcion',$resultado[3]);
        $this->assertEquals('n_cortes',$resultado[4]);
        $this->assertEquals('n_depositos',$resultado[5]);
        $this->assertEquals('pago_corte_monto_total',$resultado[6]);
        $this->assertEquals('pago_corte_monto_por_depositar',$resultado[7]);
        $this->assertEquals('pago_corte_monto_depositado',$resultado[8]);

        error::$en_error = false;



    }

    final public function test_valida_sum_deposito()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $alias = 'b';
        $deposito = new stdClass();
        $empleados = array();
        $ohem_id = 1;
        $deposito->cuenta_empresa_alias = 'a';
        $empleados[1] = new stdClass();
        $empleados[1]->b = new stdClass();
        $empleados[1]->b->deposito_aportaciones_monto_depositado = '2';

        $deposito->deposito_aportaciones_monto_depositado = '1';

        $resultado = $obj->valida_sum_deposito($alias,$deposito,$empleados,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsBool($resultado);
        $this->assertTrue($resultado);

        error::$en_error = false;



    }
    final public function test_valida_dato_reporte()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $alias = 'a';
        $deposito = new stdClass();
        $deposito->cuenta_empresa_alias = 'a';
        $empleados = array();
        $empleados[1] = new stdClass();
        $empleados[1]->a = new stdClass();
        $empleados[1]->a->deposito_aportaciones_monto_depositado = 10;
        $ohem_id = 1;
        $deposito->deposito_aportaciones_monto_depositado = 10;
        $resultado = $obj->valida_dato_reporte($alias,$deposito,$empleados,$ohem_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);


        error::$en_error = false;

    }

    final public function test_valida_fechas()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        //$obj = new liberator($obj);
        $entidad_empleado = 'x';
        $fecha_final = 'y';
        $fecha_inicial = 'z';

        $resultado = $obj->valida_fechas($entidad_empleado,$fecha_final,$fecha_inicial);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);

        error::$en_error = false;

    }

    final public function test_valida_rpt_base()
    {
        error::$en_error = false;
        $obj = new _reporte_fecha();
        $obj = new liberator($obj);
        $base = new stdClass();
        $base->ohem_id = 1;
        $base->ohem_nombre_completo = 1;
        $base->plaza_descripcion = 1;
        $base->plaza_id = 1;
        $resultado = $obj->valida_rpt_base($base);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);

        error::$en_error = false;




    }



}
