<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\data\_childrens;
use desarrollo_em3\manejo_datos\data\entidades\contrato_comision\_data_comi_alterna;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use desarrollo_em3\manejo_datos\sql\empleado;
use PHPUnit\Framework\TestCase;
use stdClass;

class _data_comi_alternaTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_data_comi_alt_es_adjunto()
    {
        error::$en_error = false;
        $obj = new _data_comi_alterna();
        //$obj = new liberator($obj);

        $cadena = array();
        $key_empleado_id = 'empleado_id';
        $registros_cadena_comision = array();
        $resultado = $obj->data_comi_alt_es_adjunto($cadena,$key_empleado_id,$registros_cadena_comision);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals('-1', $resultado->empleado_jefe_inmediato_id);
        $this->assertFalse( $resultado->aplica_coordinador);

        error::$en_error = false;

        $cadena = array();
        $key_empleado_id = 'empleado_id';
        $registros_cadena_comision = array();
        $registros_cadena_comision[0]['empleado_id'] = 1;
        $registros_cadena_comision[0]['esquema_aplica_coordinador'] = 'inactivo';
        $resultado = $obj->data_comi_alt_es_adjunto($cadena,$key_empleado_id,$registros_cadena_comision);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals('-1', $resultado->empleado_jefe_inmediato_id);
        $this->assertFalse( $resultado->aplica_coordinador);

        error::$en_error = false;

        $cadena = array();
        $key_empleado_id = 'empleado_id';
        $registros_cadena_comision = array();
        $registros_cadena_comision[0]['empleado_id'] = 1;
        $registros_cadena_comision[0]['esquema_aplica_coordinador'] = 'activo';
        $resultado = $obj->data_comi_alt_es_adjunto($cadena,$key_empleado_id,$registros_cadena_comision);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals('-1', $resultado->empleado_jefe_inmediato_id);
        $this->assertFalse( $resultado->aplica_coordinador);

        error::$en_error = false;

        $cadena = array();
        $key_empleado_id = 'empleado_id';
        $registros_cadena_comision = array();
        $registros_cadena_comision[0]['empleado_id'] = 1;
        $registros_cadena_comision[0]['esquema_aplica_coordinador'] = 'activo';
        $cadena['empleado_id'] = 1;
        $cadena['empleado_jefe_inmediato_id'] = 1;
        $resultado = $obj->data_comi_alt_es_adjunto($cadena,$key_empleado_id,$registros_cadena_comision);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals('1', $resultado->empleado_jefe_inmediato_id);
        $this->assertTrue( $resultado->aplica_coordinador);

        error::$en_error = false;
    }

    final public function test_data_comi_alterna_aplica_cord()
    {
        error::$en_error = false;
        $obj = new _data_comi_alterna();
        $obj = new liberator($obj);

        $esquema_aplica_coordinador = 'activo';
        $data_comi_alt = new stdClass();
        $resultado = $obj->data_comi_alterna_aplica_cord($data_comi_alt,$esquema_aplica_coordinador);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertTrue( $resultado->aplica_coordinador);

        error::$en_error = false;

        $esquema_aplica_coordinador = 'inactivo';
        $data_comi_alt = new stdClass();
        $resultado = $obj->data_comi_alterna_aplica_cord($data_comi_alt,$esquema_aplica_coordinador);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertNotTrue( $resultado->aplica_coordinador);

        error::$en_error = false;
    }

    final public function test_data_comi_alterna_encontro_jefe()
    {
        error::$en_error = false;
        $obj = new _data_comi_alterna();
        $obj = new liberator($obj);

        $data_comi_alt = new stdClass();
        $registro_comision = array();
        $key_empleado_id = 'empleado_id';
        $registro_comision['empleado_id'] = '';
        $registro_comision['esquema_aplica_coordinador'] = '';
        $data_comi_alt->empleado_jefe_inmediato_id = '';
        $resultado = $obj->data_comi_alterna_encontro_jefe($data_comi_alt,$key_empleado_id,$registro_comision);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertNotTrue( $resultado->aplica_coordinador);
        $this->assertTrue( $resultado->encontrado);
        $this->assertEquals('', $resultado->empleado_jefe_inmediato_id);


        error::$en_error = false;

        $data_comi_alt = new stdClass();
        $registro_comision = array();
        $key_empleado_id = 'empleado_id';
        $registro_comision['empleado_id'] = '1';
        $registro_comision['esquema_aplica_coordinador'] = '';
        $data_comi_alt->empleado_jefe_inmediato_id = '';
        $resultado = $obj->data_comi_alterna_encontro_jefe($data_comi_alt,$key_empleado_id,$registro_comision);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertNotTrue( $resultado->encontrado);
        $this->assertEquals('', $resultado->empleado_jefe_inmediato_id);



        error::$en_error = false;

        $data_comi_alt = new stdClass();
        $registro_comision = array();
        $key_empleado_id = 'empleado_id';
        $registro_comision['empleado_id'] = '1';
        $registro_comision['esquema_aplica_coordinador'] = '';
        $data_comi_alt->empleado_jefe_inmediato_id = '1';
        $resultado = $obj->data_comi_alterna_encontro_jefe($data_comi_alt,$key_empleado_id,$registro_comision);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertTrue( $resultado->encontrado);
        $this->assertEquals('1', $resultado->empleado_jefe_inmediato_id);
        $this->assertFalse( $resultado->aplica_coordinador);

        error::$en_error = false;

        $data_comi_alt = new stdClass();
        $registro_comision = array();
        $key_empleado_id = 'empleado_id';
        $registro_comision['empleado_id'] = '1';
        $registro_comision['esquema_aplica_coordinador'] = 'inactivo';
        $data_comi_alt->empleado_jefe_inmediato_id = '1';
        $resultado = $obj->data_comi_alterna_encontro_jefe($data_comi_alt,$key_empleado_id,$registro_comision);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertTrue( $resultado->encontrado);
        $this->assertEquals('1', $resultado->empleado_jefe_inmediato_id);
        $this->assertFalse( $resultado->aplica_coordinador);

        error::$en_error = false;

        $data_comi_alt = new stdClass();
        $registro_comision = array();
        $key_empleado_id = 'empleado_id';
        $registro_comision['empleado_id'] = '1';
        $registro_comision['esquema_aplica_coordinador'] = 'activo';
        $data_comi_alt->empleado_jefe_inmediato_id = '1';
        $resultado = $obj->data_comi_alterna_encontro_jefe($data_comi_alt,$key_empleado_id,$registro_comision);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertTrue( $resultado->encontrado);
        $this->assertEquals('1', $resultado->empleado_jefe_inmediato_id);
        $this->assertTrue( $resultado->aplica_coordinador);

        error::$en_error = false;
    }

    final public function test_data_comi_alterna_es_coord()
    {
        error::$en_error = false;
        $obj = new _data_comi_alterna();
        $obj = new liberator($obj);

        $data_comi_alt = new stdClass();
        $key_empleado_id = 'ohem_id';
        $registros_cadena_comision = array();
        $registros_cadena_comision[0]['ohem_id'] = '';
        $registros_cadena_comision[0]['esquema_aplica_coordinador'] = '';
        $data_comi_alt->empleado_jefe_inmediato_id  ='';
        $resultado = $obj->data_comi_alterna_es_coord($data_comi_alt,$key_empleado_id,$registros_cadena_comision);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertTrue( $resultado->encontrado);
        $this->assertEquals('', $resultado->empleado_jefe_inmediato_id);
        $this->assertFalse( $resultado->aplica_coordinador);

        error::$en_error = false;

        $data_comi_alt = new stdClass();
        $key_empleado_id = 'ohem_id';
        $registros_cadena_comision = array();
        $registros_cadena_comision[0]['ohem_id'] = '';
        $registros_cadena_comision[0]['esquema_aplica_coordinador'] = 'activo';
        $data_comi_alt->empleado_jefe_inmediato_id  ='';
        $resultado = $obj->data_comi_alterna_es_coord($data_comi_alt,$key_empleado_id,$registros_cadena_comision);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertTrue( $resultado->aplica_coordinador);
        $this->assertTrue( $resultado->encontrado);
        $this->assertEquals('', $resultado->empleado_jefe_inmediato_id);


        error::$en_error = false;
    }

    final public function test_data_comi_alterna_init()
    {
        error::$en_error = false;
        $obj = new _data_comi_alterna();
        //$obj = new liberator($obj);
        $resultado = $obj->data_comi_alterna_init();

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertFalse( $resultado->aplica_comision_adjunto);
        $this->assertEquals(-1, $resultado->empleado_jefe_inmediato_id);
        $this->assertFalse( $resultado->aplica_coordinador);

        error::$en_error = false;
    }
    final public function test_data_comi_alterna_jefe()
    {
        error::$en_error = false;
        $obj = new _data_comi_alterna();
        $obj = new liberator($obj);

        $empleado_jefe_inmediato_id = -1;
        $resultado = $obj->data_comi_alterna_jefe($empleado_jefe_inmediato_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertTrue( $resultado->aplica_comision_adjunto);
        $this->assertEquals(-1, $resultado->empleado_jefe_inmediato_id);
        $this->assertFalse( $resultado->aplica_coordinador);

        error::$en_error = false;
    }



}
