<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\data\entidades\conf_costo_estimado;
use desarrollo_em3\manejo_datos\data\entidades\costo_estimado_det;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class conf_costo_estimado_dataTest extends TestCase
{

    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;

        $conexion = (new conexion_db());
        $this->link = $conexion->link;




    }



    final public function test_costo_estimado()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\conf_costo_estimado();
        $obj = new liberator($obj);
        $conf_costo_estimado = new stdClass();
        $datos = new stdClass();
        $resultado = $obj->costo_estimado($conf_costo_estimado, $datos, 0);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0, $resultado);

        error::$en_error = false;

        $conf_costo_estimado = new stdClass();
        $datos = new stdClass();
        $conf_costo_estimado->costo = 24900;
        $resultado = $obj->costo_estimado($conf_costo_estimado, $datos, 0);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0, $resultado);

        error::$en_error = false;

        $conf_costo_estimado = new stdClass();
        $datos = new stdClass();
        $conf_costo_estimado->costo = 24900;
        $datos->contrato = new stdClass();
        $datos->contrato->DocTotal = 20000;
        $datos->pago = new stdClass();
        $datos->pago->DocTotal = 100;
        $resultado = $obj->costo_estimado($conf_costo_estimado, $datos, 0);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(124.5, $resultado);
        error::$en_error = false;

        $conf_costo_estimado = new stdClass();
        $datos = new stdClass();
        $conf_costo_estimado->costo = 9000;
        $datos->contrato = new stdClass();
        $datos->contrato->DocTotal = 11600;
        $datos->pago = new stdClass();
        $datos->pago->DocTotal = 11600;
        $resultado = $obj->costo_estimado($conf_costo_estimado, $datos, .16);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(9000, $resultado);
        error::$en_error = false;


        $conf_costo_estimado = new stdClass();
        $datos = new stdClass();
        $conf_costo_estimado->costo = 9000;
        $datos->contrato = new stdClass();
        $datos->contrato->DocTotal = 11600;
        $datos->pago = new stdClass();
        $datos->pago->DocTotal = 100;
        $resultado = $obj->costo_estimado($conf_costo_estimado, $datos, .16);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(77.59, $resultado);
        error::$en_error = false;



    }

    final public function test_costo_sin_iva()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\conf_costo_estimado();
        $obj = new liberator($obj);
        $costo_estimado = 10440;
        $porcentaje_iva = 16;
        $precio = 11600;
        $total_pago = 100;
        $resultado = $obj->costo_sin_iva($costo_estimado,$porcentaje_iva, $precio,$total_pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(90, $resultado);

        error::$en_error = false;

    }

    final public function test_datos()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\conf_costo_estimado();
        $obj = new liberator($obj);
        $pago_id = 1;
        $resultado = $obj->datos($this->link,$pago_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1, $resultado->pago->id);
        $this->assertEquals(412353, $resultado->contrato->id);

        error::$en_error = false;


    }

    final public function test_fecha_desglosada()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\conf_costo_estimado();
        $obj = new liberator($obj);
        $pago_fecha = '2020-01-01';
        $resultado = $obj->fecha_desglosada($pago_fecha);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(2020, $resultado->year);
        $this->assertEquals('2020-01-01', $resultado->pago_fecha);
        $this->assertEquals('01', $resultado->mes);

        error::$en_error = false;

    }

    final public function test_get_conf_id()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\conf_costo_estimado();
        //$obj = new liberator($obj);

        $this->link->beginTransaction();

        $sql  = "INSERT INTO conf_costo_estimado (`descripcion`, `status`, `usuario_alta_id`, `usuario_update_id`, `fecha_alta`, `fecha_update`, `empresa_id`, `fecha_inicial`, `fecha_fin`, `costo`, `producto_id`,`plaza_id`) VALUES ('GRUPO ASISTENCIAL MEMORY - PLAN IMPERIAL 2023 - GRUPO ASISTENCIAL MEMORY INICIA 2019 MES ENERO FINALIZA 2020 MES FEBRERO', 'activo', 2, 2, '2024-12-05 14:59:31', '2024-12-09 13:41:09', 52, '2024-01-01', '2024-12-31', 24900.00, 22,26);";
        $exe = (new transacciones($this->link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            $error = (new error())->error('Error al insertar conf',$exe);
            print_r($error);
            exit;
        }

        $pago_id = 23779803;
        $resultado = $obj->get_conf_id($this->link,$pago_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsInt( $resultado);

        error::$en_error = false;

        $this->link->rollBack();


    }

    final public function test_id()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\conf_costo_estimado();
        $obj = new liberator($obj);
        $contrato = new stdClass();
        $pago = new stdClass();
        $contrato->empresa = 1;
        $resultado = $obj->id($contrato,$this->link,$pago);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(-1, $resultado);

        error::$en_error = false;


    }

    final public function test_integra_costo()
    {
        error::$en_error = false;
        $this->link->beginTransaction();
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\conf_costo_estimado();
        $obj = new liberator($obj);
        $conf_costo_estimado_id = -1;
        $datos = new stdClass();
        $registro = array();
        $resultado = $obj->integra_costo($conf_costo_estimado_id,$datos,$this->link,0,$registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0, $resultado['costo']);

        error::$en_error = false;


        $sql  = "INSERT INTO conf_costo_estimado (`descripcion`, `status`, `usuario_alta_id`, `usuario_update_id`, `fecha_alta`, `fecha_update`, `empresa_id`, `fecha_inicial`, `fecha_fin`, `costo`, `producto_id`,`plaza_id`) VALUES ('GRUPO ASISTENCIAL MEMORY - PLAN IMPERIAL 2023 - GRUPO ASISTENCIAL MEMORY INICIA 2019 MES ENERO FINALIZA 2020 MES FEBRERO', 'activo', 2, 2, '2024-12-05 14:59:31', '2024-12-09 13:41:09', 52, '2024-01-01', '2024-12-31', 18000.00, 22,26);";
        $exe = (new transacciones($this->link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            $error = (new error())->error('Error al insertar conf',$exe);
            print_r($error);
            exit;
        }
        $conf_costo_estimado_id = $this->link->lastInsertId();

        $datos = new stdClass();
        $registro = array();
        $datos->pago = new stdClass();
        $datos->contrato = new stdClass();
        $datos->pago->DocTotal = 100;
        $datos->contrato->DocTotal = 20000;
        $resultado = $obj->integra_costo($conf_costo_estimado_id,$datos,$this->link,0,$registro);
        //print_r($resultado);;exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(90, $resultado['costo']);

        error::$en_error = false;


        $datos = new stdClass();
        $registro = array();
        $datos->pago = new stdClass();
        $datos->contrato = new stdClass();
        $datos->pago->DocTotal = 23200;
        $datos->contrato->DocTotal = 23200;
        $resultado = $obj->integra_costo($conf_costo_estimado_id,$datos,$this->link,0,$registro);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(18000.88, $resultado['costo']);
        $this->link->rollBack();

    }

    final public function test_integra_datos_fecha()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\conf_costo_estimado();
        $obj = new liberator($obj);
        $datos = new stdClass();
        $registro = array();
        $datos->pago = new stdClass();
        $datos->pago->DocDueDate = '2024-01-01';

        $resultado = $obj->integra_datos_fecha($datos,$registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(2024, $resultado['year']);
        $this->assertEquals('2024-01-01', $resultado['fecha']);
        $this->assertEquals('01', $resultado['mes']);

        error::$en_error = false;

    }

    final public function test_integra_datos_generales()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\conf_costo_estimado();
        $obj = new liberator($obj);
        $conf_costo_estimado_id = -1;
        $datos = new stdClass();
        $registro = array();
        $resultado = $obj->integra_datos_generales($conf_costo_estimado_id,$datos,$this->link,$registro);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(-1, $resultado['contrato_id']);

        error::$en_error = false;

    }

    final public function test_row()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\conf_costo_estimado();
        //$obj = new liberator($obj);

        $this->link->beginTransaction();

        $sql  = "INSERT INTO conf_costo_estimado (`descripcion`, `status`, `usuario_alta_id`, `usuario_update_id`, `fecha_alta`, `fecha_update`, `empresa_id`, `fecha_inicial`, `fecha_fin`, `costo`, `producto_id`,`plaza_id`) VALUES ('GRUPO ASISTENCIAL MEMORY - PLAN IMPERIAL 2023 - GRUPO ASISTENCIAL MEMORY INICIA 2019 MES ENERO FINALIZA 2020 MES FEBRERO', 'activo', 2, 2, '2024-12-05 14:59:31', '2024-12-09 13:41:09', 52, '2024-01-01', '2024-12-31', 24900.00, 22,26);";
        $exe = (new transacciones($this->link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            $error = (new error())->error('Error al insertar conf',$exe);
            print_r($error);
            exit;
        }

        $conf_costo_estimado_id = $this->link->lastInsertId();
        $registro = array();
        $registro['pago_id'] = 23779803;
        $resultado = $obj->row($conf_costo_estimado_id,$this->link,.16,$registro);


        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(0, $resultado['costo']);
        $this->assertEquals(2024, $resultado['year']);
        $this->assertEquals(11, $resultado['mes']);

        error::$en_error = false;

        $this->link->rollBack();

    }



}
