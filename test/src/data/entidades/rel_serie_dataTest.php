<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\conexion_db_monterrey;
use desarrollo_em3\manejo_datos\conexion_db_mzo;
use desarrollo_em3\manejo_datos\data\entidades\rel_serie;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class rel_serie_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 2;
        $conexion = (new conexion_db_monterrey());
        $this->link = $conexion->link;

    }

    final public function test_consulta_serie_duplicada_plaza()
    {
        $_SESSION['usuario_id'] = 2;
        error::$en_error = false;
        $modelo = new rel_serie();
        //$modelo = new liberator($modelo);

        $this->link->beginTransaction();

        $sql = "INSERT INTO rel_serie (id, status, usuario_alta_id, usuario_update_id, fecha_alta, fecha_update, serie_id, cuenta_empresa_id, deposita_aportacion) VALUES (9999, 'activo', 67, 67, '2024-09-20 17:12:42', '2024-09-20 17:12:42', 1, 38, 'activo');";

        $exe = (new transacciones($this->link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            $error = (new error())->error('Error al ejecutar sql',$exe);
            print_r($error);
            exit;
        }
        $cantidad = 1;
        $plaza_id = 28;
        $result  = $modelo->consulta_serie_duplicada_plaza($cantidad,$this->link,$plaza_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertEquals('MTYC',$result[0]->serie_codigo);
        $this->assertEquals(2,$result[0]->total_serie);
        $this->link->rollBack();
        error::$en_error = false;

    }

    final public function test_obten_cuenta_bancaria_con_serie()
    {
        $_SESSION['usuario_id'] = 2;
        error::$en_error = false;
        $modelo = new rel_serie();
        //$modelo = new liberator($modelo);

        $plaza_id = -28;
        $result  = $modelo->obten_cuenta_bancaria_con_serie($this->link,$plaza_id);

        $this->assertTrue(error::$en_error);
        $this->assertIsArray($result);

        error::$en_error = false;

        $plaza_id = 28;
        $result  = $modelo->obten_cuenta_bancaria_con_serie($this->link,$plaza_id);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);

        error::$en_error = false;

        $plaza_id = 28;
        $es_contable = 'inactivo';
        $result  = $modelo->obten_cuenta_bancaria_con_serie($this->link,$plaza_id,$es_contable);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($result);
        $this->assertIsNumeric($result[0]->rel_serie_id);
        $this->assertIsString($result[0]->serie_codigo);
        $this->assertEquals(35,$result[0]->cuenta_empresa_id);
        $this->assertEquals('65508537660',$result[0]->cuenta_empresa_cuenta);
        $this->assertEquals('SANTANDER',$result[0]->banco_descripcion);
        $this->assertEquals('Banco Santander (México), S.A., Institución de Banca Múltiple, Grupo Financiero Santander',$result[0]->banco_razon_social);

        error::$en_error = false;

    }

}
