<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\conexion_db;
use PDO;
use PHPUnit\Framework\TestCase;

class empresa_dataTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_empresas_324()
    {
        error::$en_error = false;
        $obj = new \desarrollo_em3\manejo_datos\data\entidades\empresa();
        //$obj = new liberator($obj);
        $resultado = $obj->empresas_324($this->link);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(73, $resultado[0]->id);
        $this->assertEquals(74, $resultado[1]->id);


        error::$en_error = false;
    }


}
