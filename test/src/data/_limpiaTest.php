<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\data\_childrens;
use desarrollo_em3\manejo_datos\data\_limpia;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use PHPUnit\Framework\TestCase;

class _limpiaTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_limpia_double()
    {
        error::$en_error = false;
        $modelo = new _limpia();
        //$modelo = new liberator($modelo);
        $var = '';
        $resultado = $modelo->limpia_double($var);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('', $resultado);

        error::$en_error = false;

        $var = '-11,1188.09$$88,';
        $resultado = $modelo->limpia_double($var);
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('-111188.0988', $resultado);

        error::$en_error = false;
    }


}
