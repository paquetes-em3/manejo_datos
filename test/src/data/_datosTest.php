<?php
namespace desarrollo_em3\test\clases\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\conexion_db;
use desarrollo_em3\manejo_datos\data\_datos;
use PDO;
use PHPUnit\Framework\TestCase;

class _datosTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {

        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';

        $_SESSION['numero_empresa'] = 1;
        $conexion = (new conexion_db());
        $this->link = $conexion->link;


    }

    final public function test_campos_entidad()
    {
        error::$en_error = false;
        $obj = new _datos();
        //$obj = new liberator($obj);

        $entidad = 'esquema';
        $resultado = $obj->campos_entidad($entidad, $this->link);

        //print_r($resultado);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals('esquema_id',$resultado->id->rename);
        $this->assertEquals('esquema_puesto_id',$resultado->puesto_id->rename);

        error::$en_error = false;
    }
    final public function test_existe_by_campo()
    {
        error::$en_error = false;
        $obj = new _datos();
        $obj = new liberator($obj);
        $key_compare = 'a';
        $rows = array();
        $val_compare = 'x';
        $rows[] = array();
        $resultado = $obj->existe_by_campo($key_compare,$rows,$val_compare);

        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);

        error::$en_error = false;

        $key_compare = 'a';
        $rows = array();
        $val_compare = 'x';
        $rows[]['a'] = '';
        $resultado = $obj->existe_by_campo($key_compare,$rows,$val_compare);
        $this->assertNotTrue(error::$en_error);
        $this->assertNotTrue($resultado);

        error::$en_error = false;

        $key_compare = 'a';
        $rows = array();
        $val_compare = 'x';
        $rows[]['a'] = 'x';
        $resultado = $obj->existe_by_campo($key_compare,$rows,$val_compare);

        $this->assertNotTrue(error::$en_error);
        $this->assertTrue($resultado);

        error::$en_error = false;
    }

    final public function test_filters_fecha()
    {
        error::$en_error = false;
        $obj = new _datos();
        $obj = new liberator($obj);
        $row_data = array();
        $resultado = $obj->filters_fecha($this->link,$row_data);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(1900,$resultado->years->inicial);
        $this->assertEquals(2025,$resultado->years->final);
        $this->assertEquals(01,$resultado->meses->inicial);
        $this->assertEquals(03,$resultado->meses->final);

        error::$en_error = false;

        $row_data = array();
        $row_data['ejercicio_inicial'] = 2;
        $resultado = $obj->filters_fecha($this->link,$row_data);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals(2020,$resultado->years->inicial);
        $this->assertEquals(2025,$resultado->years->final);
        $this->assertEquals(01,$resultado->meses->inicial);
        $this->assertEquals(03,$resultado->meses->final);
        error::$en_error = false;
    }

    final public function test_filtros_fecha()
    {
        error::$en_error = false;
        $obj = new _datos();
        //$obj = new liberator($obj);
        $row_data = array();
        $resultado = $obj->filtros_fecha($this->link,$row_data);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('1900-01-01',$resultado->fecha_inicial);
        $this->assertEquals('2025-03-31',$resultado->fecha_final);


        error::$en_error = false;

        $row_data = array();
        $row_data['ejercicio_inicial'] = 2;
        $row_data['ejercicio_final'] = 2;
        $resultado = $obj->filtros_fecha($this->link,$row_data);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('2020-01-01',$resultado->fecha_inicial);
        $this->assertEquals('2020-03-31',$resultado->fecha_final);
        error::$en_error = false;
    }

    final public function test_integra_a_result()
    {
        error::$en_error = false;
        $obj = new _datos();
        $obj = new liberator($obj);
        $rows_compare = array();
        $key_compare = 'a';
        $rows = array();
        $rows_new = array();

        $resultado = $obj->integra_a_result($key_compare,$rows_compare,$rows,$rows_new);


        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('',$resultado[0]['a']);

        error::$en_error = false;

    }

    final public function test_meses_filter()
    {
        error::$en_error = false;
        $obj = new _datos();
        $obj = new liberator($obj);
        $row_data = array();
        $resultado = $obj->meses_filter($row_data);
        //print_r($resultado);exit;
        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('01',$resultado->inicial);
        $this->assertEquals('03',$resultado->final);

        error::$en_error = false;

        $row_data = array();
        $row_data['mes_inicial'] = '5';
        $row_data['mes_final'] = '12';
        $resultado = $obj->meses_filter($row_data);

        $this->assertNotTrue(error::$en_error);
        $this->assertEquals('05',$resultado->inicial);
        $this->assertEquals('12',$resultado->final);
        error::$en_error = false;

    }

    final public function test_rows_news()
    {
        error::$en_error = false;
        $obj = new _datos();
        //$obj = new liberator($obj);

        $key_compare = 'r';
        $result_reasigna = array();
        $result_reasigna[] = array('r'=>'xx');
        $resultado = $obj->rows_news($key_compare,$result_reasigna);

        //print_r($resultado);exit;

        $this->assertNotTrue(error::$en_error);
        $this->assertIsArray($resultado);
        $this->assertEquals('xx',$resultado[0]['r']);

        error::$en_error = false;
    }

    final public function test_years_filter()
    {
        error::$en_error = false;
        $obj = new _datos();
        $obj = new liberator($obj);

        $row_data = array();
        $resultado = $obj->years_filter($this->link,$row_data);


        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals(1900,$resultado->inicial);
        $this->assertEquals(2025,$resultado->final);

        error::$en_error = false;

        $row_data = array();
        $row_data['ejercicio_inicial'] = '1';
        $resultado = $obj->years_filter($this->link,$row_data);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsObject($resultado);
        $this->assertEquals(2019,$resultado->inicial);
        $this->assertEquals(2025,$resultado->final);

        error::$en_error = false;
    }


}
