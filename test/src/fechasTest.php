<?php
namespace desarrollo_em3\test\clases;


use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\manejo_datos\fechas;
use desarrollo_em3\manejo_datos\sql;
use PHPUnit\Framework\TestCase;
use stdClass;

class fechasTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;


    }

    final public function test_get_fecha_ini_periodo_sem()
    {
        error::$en_error = false;
        $obj = new fechas();
        //$obj = new liberator($obj);
;
        $fecha_fin = '2024-08-04';

        $number_dia_ini_periodo = 0;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-08-04',$result);
        error::$en_error = false;

        $fecha_fin = '2024-08-04';

        $number_dia_ini_periodo = 1;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-07-29',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-04';

        $number_dia_ini_periodo = 2;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-07-30',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-04';

        $number_dia_ini_periodo = 3;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-07-31',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-04';

        $number_dia_ini_periodo = 4;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-08-01',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-04';

        $number_dia_ini_periodo = 5;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-08-02',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-04';

        $number_dia_ini_periodo = 6;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-08-03',$result);


        error::$en_error = false;

        $fecha_fin = '2024-08-05';

        $number_dia_ini_periodo = 0;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-08-04',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-05';

        $number_dia_ini_periodo = 1;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);

        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-08-05',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-05';

        $number_dia_ini_periodo = 2;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-07-30',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-05';

        $number_dia_ini_periodo = 3;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-07-31',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-05';

        $number_dia_ini_periodo = 4;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-08-01',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-05';

        $number_dia_ini_periodo = 5;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-08-02',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-05';

        $number_dia_ini_periodo = 6;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-08-03',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-06';

        $number_dia_ini_periodo = 0;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-08-04',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-06';

        $number_dia_ini_periodo = 1;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-08-05',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-06';

        $number_dia_ini_periodo = 2;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-08-06',$result);

        error::$en_error = false;

        $fecha_fin = '2024-08-06';

        $number_dia_ini_periodo = 3;
        $result = $obj->get_fecha_ini_periodo_sem($fecha_fin,$number_dia_ini_periodo);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsString($result);
        $this->assertEquals('2024-07-31',$result);

        error::$en_error = false;


    }






}
