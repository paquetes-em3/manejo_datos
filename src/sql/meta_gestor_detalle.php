<?php
namespace desarrollo_em3\manejo_datos\sql;



use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;

class meta_gestor_detalle{

    /**
     * EM3
     * Genera una lista de campos SQL para una consulta relacionada con pagos.
     *
     * Esta función valida los parámetros, genera una lista de campos básicos incluyendo
     * `entidad_empleado_id` y `contrato_id`, y añade un campo calculado que aplica la función `SUM`
     * al campo total especificado.
     *
     * @param string $campo_total El nombre del campo que será sumado. No debe estar vacío.
     * @param string $entidad_empleado El nombre de la entidad del empleado. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con los campos y el cálculo de la suma. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar campos válidos
     * $campo_total = 'monto';
     * $entidad_empleado = 'empleado';
     * $resultado = $this->campos_pagos($campo_total, $entidad_empleado);
     * echo $resultado;
     * // Resultado:
     * // empleado_id, contrato_id, SUM(pago.monto) AS aportado
     *
     * // Ejemplo 2: Campo total vacío (error)
     * $campo_total = '';
     * $entidad_empleado = 'empleado';
     * $resultado = $this->campos_pagos($campo_total, $entidad_empleado);
     * // Resultado:
     * // ['error' => 'Error $campo_total esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Entidad empleado vacía (error)
     * $campo_total = 'monto';
     * $entidad_empleado = '';
     * $resultado = $this->campos_pagos($campo_total, $entidad_empleado);
     * // Resultado:
     * // ['error' => 'Error $entidad_empleado esta vacio', 'data' => '']
     * ```
     */
    private function campos_pagos(string $campo_total, string $entidad_empleado)
    {
        // Validar que $campo_total no esté vacío
        $campo_total = trim($campo_total);
        if ($campo_total === '') {
            return (new error())->error('Error $campo_total esta vacio', $campo_total);
        }

        // Asegurar que $campo_total tiene el prefijo 'pago' si es necesario
        $parts = explode('pago', $campo_total);
        if (count($parts) === 1) {
            $campo_total = "pago.$campo_total";
        }

        // Validar que $entidad_empleado no esté vacío
        $entidad_empleado = trim($entidad_empleado);
        if ($entidad_empleado === '') {
            return (new error())->error('Error $entidad_empleado esta vacio', $entidad_empleado);
        }

        // Generar campos básicos
        $campos[] = $entidad_empleado . '_id';
        $campos[] = 'contrato_id';
        $campos_id = (new sql())->campos_sql_puros($campos);
        if (error::$en_error) {
            return (new error())->error('Error al generar campos', $campos_id);
        }

        // Generar el campo con la suma del campo total
        $campo_aportado = (new sql())->campo_sum_base($campo_total, 'aportado');
        if (error::$en_error) {
            return (new error())->error('Error al generar $campo_aportado', $campo_aportado);
        }

        // Retornar los campos concatenados
        return "$campos_id, $campo_aportado";
    }


    /**
     * EM3
     * Recalcula los valores asociados a las metas del gestor en detalle.
     *
     * Esta función valida los datos de entrada, genera las estructuras SQL necesarias para recalcular
     * los valores asociados a las metas del gestor en detalle, y retorna una consulta SQL `UPDATE` para realizar
     * los ajustes correspondientes.
     *
     * @param string $campo_fecha_pago El nombre del campo que representa la fecha de pago. No debe estar vacío.
     * @param string $campo_movimiento El nombre del campo que representa el movimiento del pago. No debe estar vacío.
     * @param string $campo_total El nombre del campo que representa el total del pago. No debe estar vacío.
     * @param string $entidad_empleado El nombre de la entidad o tabla relacionada con empleados. No debe estar vacío.
     *
     * @return string|array Devuelve una consulta SQL `UPDATE` para recalcular las metas del gestor en detalle.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar consulta SQL para recalcular metas
     * $campo_fecha_pago = 'fecha_pago';
     * $campo_movimiento = 'movimiento';
     * $campo_total = 'total';
     * $entidad_empleado = 'empleado';
     * $resultado = $this->recalcula_meta_gestor_detalle($campo_fecha_pago, $campo_movimiento, $campo_total, $entidad_empleado);
     * echo $resultado;
     * // Resultado:
     * // UPDATE meta_gestor_detalle ...
     *
     * // Ejemplo 2: Error en validación de datos
     * $campo_fecha_pago = '';
     * $campo_movimiento = 'movimiento';
     * $campo_total = 'total';
     * $entidad_empleado = 'empleado';
     * $resultado = $this->recalcula_meta_gestor_detalle($campo_fecha_pago, $campo_movimiento, $campo_total, $entidad_empleado);
     * // Resultado:
     * // ['error' => 'Error al validar datos', 'data' => [...]]
     * ```
     */

    final public function recalcula_meta_gestor_detalle(string $campo_fecha_pago, string $campo_movimiento,
                                                        string $campo_total, string $entidad_empleado)
    {

        $valida = $this->valida_datos($campo_fecha_pago,$campo_movimiento,$campo_total,$entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al validar datos', $valida);
        }

        $campo_fecha_pago = trim($campo_fecha_pago);
        $campo_movimiento = trim($campo_movimiento);
        $campo_total = trim($campo_total);
        $entidad_empleado = trim($entidad_empleado);


        $entidad_mgd = (new sql())->entidad_base_as('meta_gestor_detalle','mgd');
        if(error::$en_error){
            return (new error())->error('Error al generar entidad upd',$entidad_mgd);
        }
        $entidad_mgc = (new sql())->entidad_base_as('meta_gestor_concentrado','mgc');
        if(error::$en_error){
            return (new error())->error('Error al generar entidad upd',$entidad_mgc);
        }

        $on_metas = (new sql())->on_para_join('mgc','mgd','id',
            'meta_gestor_concentrado_id');
        if(error::$en_error){
            return (new error())->error('Error al generar $on',$on_metas);
        }
        $on_pago = (new sql())->on_para_join('mgd','pago','contrato_id',
            'contrato_id');
        if(error::$en_error){
            return (new error())->error('Error al generar $on',$on_pago);
        }

        $key_empleado_id = $entidad_empleado.'_id';
        $on_empleado = (new sql())->on_para_join('mgc','pago',$key_empleado_id,
            $key_empleado_id);
        if(error::$en_error){
            return (new error())->error('Error al generar $on',$on_empleado);
        }

        $campos_pago = $this->campos_pagos($campo_total,$entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al generar $campos_pago',$campos_pago);
        }

        $where_pago = $this->where_pago($campo_fecha_pago, $campo_movimiento);
        if(error::$en_error){
            return (new error())->error('Error al generar $where_pago',$where_pago);
        }

        $pago_aportado = (new sql())->campo_nulo_0('aportado','pago');
        if(error::$en_error){
            return (new error())->error('Error al generar $pago_aportado',$pago_aportado);
        }

        $monto_total = "LEAST(mgd.monto_total, $pago_aportado)";

        $campos[] = 'ohem_id';
        $campos[] = 'contrato_id';
        $campos_id = (new sql())->campos_sql_puros($campos);
        if(error::$en_error){
            return (new error())->error('Error al generar campos',$campos_id);
        }
        $sentencia_pago = /** @lang MYSQL */
            "SELECT $campos_pago FROM pago WHERE $where_pago GROUP BY $campos_id";

        $asigna_monto_proceso = "mgd.monto_proceso = GREATEST(0, $monto_total)";
        $asigna_monto_de_mas = "mgd.monto_de_mas = GREATEST(0, $pago_aportado - $monto_total)";

        $wh_gestor = "mgc.meta_gestor_id = :meta_gestor_id";

        $on_lf_pago = "ON $on_pago AND $on_empleado";

        $lf_pago = "LEFT JOIN ( $sentencia_pago ) AS pago $on_lf_pago";
        $asignacion_values = "$asigna_monto_proceso, $asigna_monto_de_mas";
        $join_data = "LEFT JOIN $entidad_mgc ON $on_metas $lf_pago";

        return /** @lang MYSQL */ "UPDATE $entidad_mgd $join_data SET $asignacion_values WHERE $wh_gestor";
        
    }

    /**
     * EM3
     * Valida que los datos requeridos no estén vacíos.
     *
     * Esta función verifica que los valores de los campos `$campo_fecha_pago`, `$campo_movimiento`,
     * `$campo_total`, y `$entidad_empleado` no estén vacíos. Si alguno de ellos está vacío, retorna
     * un error con detalles.
     *
     * @param string $campo_fecha_pago El nombre o valor del campo de fecha de pago. No debe estar vacío.
     * @param string $campo_movimiento El nombre o valor del campo de movimiento. No debe estar vacío.
     * @param string $campo_total El nombre o valor del campo total. No debe estar vacío.
     * @param string $entidad_empleado El nombre de la entidad o tabla relacionada con empleados. No debe estar vacío.
     *
     * @return bool|array Devuelve `true` si todos los campos son válidos. En caso de error, devuelve un array
     *                    con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Validación exitosa
     * $campo_fecha_pago = 'fecha_pago';
     * $campo_movimiento = 'movimiento';
     * $campo_total = 'total';
     * $entidad_empleado = 'empleados';
     * $resultado = $this->valida_datos($campo_fecha_pago, $campo_movimiento, $campo_total, $entidad_empleado);
     * echo $resultado ? 'Validación exitosa' : 'Error';
     * // Resultado:
     * // Validación exitosa
     *
     * // Ejemplo 2: Campo total vacío (error)
     * $campo_fecha_pago = 'fecha_pago';
     * $campo_movimiento = 'movimiento';
     * $campo_total = '';
     * $entidad_empleado = 'empleados';
     * $resultado = $this->valida_datos($campo_fecha_pago, $campo_movimiento, $campo_total, $entidad_empleado);
     * // Resultado:
     * // ['error' => 'Error $campo_total esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Entidad de empleado vacía (error)
     * $campo_fecha_pago = 'fecha_pago';
     * $campo_movimiento = 'movimiento';
     * $campo_total = 'total';
     * $entidad_empleado = '';
     * $resultado = $this->valida_datos($campo_fecha_pago, $campo_movimiento, $campo_total, $entidad_empleado);
     * // Resultado:
     * // ['error' => 'Error $entidad_empleado esta vacio', 'data' => '']
     * ```
     */
    final public function valida_datos(
        string $campo_fecha_pago, string $campo_movimiento, string $campo_total, string $entidad_empleado
    ) {
        // Validar que el campo total no esté vacío
        $campo_total = trim($campo_total);
        if ($campo_total === '') {
            return (new error())->error('Error $campo_total esta vacio', $campo_total);
        }

        // Validar que la entidad de empleado no esté vacía
        $entidad_empleado = trim($entidad_empleado);
        if ($entidad_empleado === '') {
            return (new error())->error('Error $entidad_empleado esta vacio', $entidad_empleado);
        }

        // Validar que el campo de fecha de pago no esté vacío
        $campo_fecha_pago = trim($campo_fecha_pago);
        if ($campo_fecha_pago === '') {
            return (new error())->error('Error $campo_fecha_pago esta vacio', $campo_fecha_pago);
        }

        // Validar que el campo de movimiento no esté vacío
        $campo_movimiento = trim($campo_movimiento);
        if ($campo_movimiento === '') {
            return (new error())->error('Error $campo_movimiento esta vacio', $campo_movimiento);
        }

        // Si todas las validaciones pasan, retornar true
        return true;
    }


    /**
     * EM3
     * Genera una cláusula SQL `WHERE` para filtrar pagos activos y de tipo "Abono".
     *
     * Esta función valida los campos de fecha de pago y movimiento, genera una cláusula SQL `BETWEEN`
     * para filtrar por rango de fechas, y combina condiciones adicionales para el estado y el tipo de movimiento.
     *
     * @param string $campo_fecha_pago El nombre del campo que representa la fecha de pago. No debe estar vacío.
     * @param string $campo_movimiento El nombre del campo que representa el movimiento del pago. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `WHERE` generada. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula WHERE válida
     * $campo_fecha_pago = 'fecha_pago';
     * $campo_movimiento = 'movimiento';
     * $resultado = $this->where_pago($campo_fecha_pago, $campo_movimiento);
     * echo $resultado;
     * // Resultado:
     * // pago.fecha_pago BETWEEN :fecha_inicio AND :fecha_fin AND pago.status = 'activo' AND pago.movimiento = 'Abono'
     *
     * // Ejemplo 2: Campo de fecha vacío (error)
     * $campo_fecha_pago = '';
     * $campo_movimiento = 'movimiento';
     * $resultado = $this->where_pago($campo_fecha_pago, $campo_movimiento);
     * // Resultado:
     * // ['error' => 'Error $campo_fecha_pago esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Campo de movimiento vacío (error)
     * $campo_fecha_pago = 'fecha_pago';
     * $campo_movimiento = '';
     * $resultado = $this->where_pago($campo_fecha_pago, $campo_movimiento);
     * // Resultado:
     * // ['error' => 'Error $campo_movimiento esta vacio', 'data' => '']
     * ```
     */
    private function where_pago(string $campo_fecha_pago, string $campo_movimiento)
    {
        // Validar que el campo de fecha de pago no esté vacío
        $campo_fecha_pago = trim($campo_fecha_pago);
        if ($campo_fecha_pago === '') {
            return (new error())->error('Error $campo_fecha_pago esta vacio', $campo_fecha_pago);
        }

        // Agregar prefijo "pago." si no está presente
        $parts = explode('pago', $campo_fecha_pago);
        if (count($parts) === 1) {
            $campo_fecha_pago = "pago.$campo_fecha_pago";
        }

        // Validar que el campo de movimiento no esté vacío
        $campo_movimiento = trim($campo_movimiento);
        if ($campo_movimiento === '') {
            return (new error())->error('Error $campo_movimiento esta vacio', $campo_movimiento);
        }

        // Agregar prefijo "pago." si no está presente
        $parts = explode('pago', $campo_movimiento);
        if (count($parts) === 1) {
            $campo_movimiento = "pago.$campo_movimiento";
        }

        // Generar cláusula WHERE para rango de fechas
        $wh_fecha = (new sql())->wh_entre_fechas($campo_fecha_pago, ':fecha_fin', ':fecha_inicio');
        if (error::$en_error) {
            return (new error())->error('Error al generar $wh_fecha', $wh_fecha);
        }

        // Cláusula WHERE para estado activo y tipo de movimiento
        $wh_status = "pago.status = 'activo'";
        $wh_movto = "$campo_movimiento = 'Abono'";

        // Combinar todas las cláusulas WHERE
        return "$wh_fecha AND $wh_status AND $wh_movto";
    }


}
