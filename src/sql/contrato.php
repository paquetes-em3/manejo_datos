<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use stdClass;

class contrato{



    /**
     * EM3
     * Inicializa los campos SQL para consultas relacionadas con contratos de gestor, adaptándose a la estructura especificada.
     *
     * Esta función selecciona un conjunto de campos base dependiendo de la estructura (`SAP` o no) y los convierte
     * en una lista SQL con alias utilizando `campos_sql_puros_with_as`.
     *
     * @param string $estructura Define la estructura de datos. Puede ser:
     *                           - `SAP`: Utiliza los campos definidos para la estructura SAP.
     *                           - Cualquier otro valor: Utiliza los campos definidos para estructuras no SAP.
     *
     * @return string|array Devuelve una cadena SQL con los campos generados, separados por comas y con alias.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar campos para la estructura SAP
     * $estructura = 'SAP';
     * $resultado = $this->init_campos_contrato_gestor($estructura);
     * echo $resultado;
     * // Resultado:
     * // contrato.Canceled AS contrato_Canceled, contrato.U_SeCont AS contrato_U_SeCont, ...
     *
     * // Ejemplo: Generar campos para una estructura no SAP
     * $estructura = 'EM3';
     * $resultado = $this->init_campos_contrato_gestor($estructura);
     * echo $resultado;
     * // Resultado:
     * // contrato.serie AS contrato_serie, contrato.folio AS contrato_folio, ...
     *
     * // Ejemplo 3: Error al generar los campos
     * // Supongamos que ocurre un error en `campos_sql_puros_with_as`.
     * $estructura = 'SAP';
     * $resultado = $this->init_campos_contrato_gestor($estructura);
     * // Resultado:
     * // ['error' => 'Error al generar $campos', 'data' => [...]]
     * ```
     */
    private function init_campos_contrato_gestor($estructura)
    {
        // Seleccionar los campos base dependiendo de la estructura
        if ($estructura === 'SAP') {
            $campos_base = array(
                'contrato.Canceled', 'contrato.U_SeCont', 'contrato.U_FolioCont',
                'contrato.DocTotal', 'contrato.DocDate', 'cliente.U_Beneficiario', 'contrato.U_CondPago',
                'contrato.U_ImportePago', 'contrato.U_ItemName', 'contrato.PaidToDate', 'contrato.CardName',
                'contrato.id', 'contrato.U_FechaUltimoPago', 'contrato.U_FeContCancel', 'contrato.CardName',
                'contrato.U_StatusCob', 'contrato.U_TipoMorosidad', 'contrato.U_InvInicial', 'contrato.U_Papeleria',
                'cliente_id'
            );
        } else {
            $campos_base = array(
                'contrato.serie', 'contrato.folio', 'contrato.monto_total',
                'contrato.fecha', 'contrato.beneficiario', 'periodicidad_pago.descripcion',
                'contrato.monto_pagado', 'producto.descripcion', 'contrato.monto_resto', 'cliente.nombre_completo',
                'contrato.id', 'contrato.fecha_ultimo_pago', 'contrato.fecha_cancelacion', 'status_contrato.descripcion',
                'tipo_morosidad.descripcion', 'contrato.inversion_inicial', 'contrato.papeleria', 'cliente_id'
            );
        }

        // Generar los campos SQL con alias
        $campos = (new sql())->campos_sql_puros_with_as($campos_base);
        if (error::$en_error) {
            return (new error())->error('Error al generar $campos', $campos);
        }

        // Retornar los campos generados
        return $campos;
    }


    private function init_join_contrato_gestor($estructura) {
        $params = array();
        $params[0] = new stdClass();
        $params[0]->entidad = 'cliente';
        $params[0]->entidad_right = 'contrato';
        if($estructura !== 'SAP') {

            $params[1] = new stdClass();
            $params[1]->entidad = 'periodicidad_pago';
            $params[1]->entidad_right = 'contrato';

            $params[2] = new stdClass();
            $params[2]->entidad = 'status_contrato';
            $params[2]->entidad_right = 'contrato';

            $params[3] = new stdClass();
            $params[3]->entidad = 'producto';
            $params[3]->entidad_right = 'contrato';

            $params[4] = new stdClass();
            $params[4]->entidad = 'tipo_morosidad';
            $params[4]->entidad_right = 'contrato';
        }

        $result = (new sql())->genera_join_replace($params);
        if(error::$en_error){
            return (new error())->error('Error generar join', $result);
        }

        return $result;
    }

    final public function obten_contratos_gestor(string $estructura, int $ohem_id, string $status = '') {
        if($ohem_id <= 0) {
            return (new error())->error('Error $ohem_id debe ser mayor a 0',$ohem_id);
        }
        $estructura = strtoupper(trim($estructura));
        if($estructura === ''){
            return  (new error())->error('Error $estructura esta vacia',$estructura);
        }
        $estructuras_valida = array('SAP','EM3');
        if(!in_array($estructura,$estructuras_valida)){
            return  (new error())->error('Error $estructura invalida',$estructuras_valida);
        }

        $campos = $this->init_campos_contrato_gestor($estructura);
        if(error::$en_error){
            return (new error())->error('Error al generar $campos', $campos);
        }

        $from = 'contrato AS contrato';
        $joins = $this->init_join_contrato_gestor($estructura);
        if(error::$en_error){
            return (new error())->error('Error generar $joins', $joins);
        }
        $lf_from = $from . ' ' . $joins;
        $or = '';

        $order_by = 'contrato.U_SeCont, contrato.U_FolioCont';
        if($estructura === 'EM3') {
            $order_by = 'contrato.serie, contrato.folio';
        }

        $where = $this->where_contrato_gestor($estructura,$ohem_id,$status);
        if(error::$en_error){
            return (new error())->error('Error generar $joins', $where);
        }

        return /** @lang MYSQL */ "SELECT $campos FROM $lf_from WHERE $where ORDER BY $order_by";
    }

    /**
     * EM3
     * Obtiene datos de un contrato para la bitácora.
     *
     * Esta función genera una consulta SQL para obtener el ID del estado del contrato (`status_contrato_id`)
     * y los comentarios (`Comments`) de un contrato específico, identificado por su ID (`$contrato_id`).
     * La consulta se utiliza para registrar información relevante sobre el contrato en la bitácora
     * del sistema, como cambios de estado o comentarios adicionales.
     *
     * @param int $contrato_id El ID del contrato del cual se desean obtener los datos para la bitácora. Debe ser un valor entero positivo.
     *
     * @return string|array Devuelve una cadena SQL con la consulta generada si el `$contrato_id` es válido.
     *                      En caso de que el `$contrato_id` sea menor o igual a 0, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Obtener datos para la bitácora de un contrato
     * $contrato_id = 123;
     * $sql = $this->obten_datos_para_bitacora($contrato_id);
     * echo $sql;
     * // Resultado:
     * // SELECT status_contrato_id, Comments FROM contrato WHERE id = 123
     *
     * // Ejemplo con error:
     * $contrato_id = 0; // ID inválido
     * $sql = $this->obten_datos_para_bitacora($contrato_id);
     * // $sql contendrá un array de error.
     * ```
     */
    final public function obten_datos_para_bitacora(int $contrato_id)
    {
        if ($contrato_id <= 0) {
            return (new error())->error('Error $contrato_id debe ser mayor a 0', $contrato_id);
        }
        return /** @lang MYSQL */ "SELECT status_contrato_id, Comments FROM contrato WHERE id = $contrato_id";

    }

    /**
     * EM3
     * Genera los parámetros necesarios para realizar una consulta SQL que calcule la suma de un campo con filtros.
     *
     * Esta función valida los datos de entrada, construye la expresión SQL para sumar un campo con un alias,
     * genera las condiciones `WHERE` necesarias, y retorna un objeto con estos parámetros.
     *
     * @param string $alias El alias que se asignará al resultado de la suma. No debe estar vacío.
     * @param string $campo_canceled El nombre del campo que indica si un registro está cancelado. Puede estar vacío.
     * @param string $campo_monto El nombre del campo que será sumado. No debe estar vacío.
     * @param int $contrato_id El identificador del contrato. Debe ser mayor a 0.
     * @param string $entidad El nombre de la entidad (tabla) donde se realizará la suma. No debe estar vacío.
     *
     * @return stdClass|array Devuelve un objeto con los siguientes parámetros:
     *                        - `campo`: La expresión SQL para la suma del campo con alias.
     *                        - `where`: La cláusula SQL `WHERE` generada.
     *                        En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar parámetros válidos para suma
     * $alias = 'suma_monto';
     * $campo_canceled = 'Canceled';
     * $campo_monto = 'DocTotal';
     * $contrato_id = 123;
     * $entidad = 'contratos';
     * $resultado = $this->params_sum($alias, $campo_canceled, $campo_monto, $contrato_id, $entidad);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [campo] => "IFNULL(SUM(contratos.DocTotal), 0) AS 'suma_monto'"
     * //     [where] => "contratos.status = 'activo' AND contratos.CANCELED = 'N' AND contratos.contrato_id = 123"
     * // )
     *
     * // Ejemplo 2: Error por contrato_id inválido
     * $contrato_id = 0;
     * $resultado = $this->params_sum($alias, $campo_canceled, $campo_monto, $contrato_id, $entidad);
     * // Resultado:
     * // ['error' => 'Error $contrato_id debe ser mayor a 0', 'data' => 0]
     *
     * // Ejemplo 3: Error por alias vacío
     * $alias = '';
     * $resultado = $this->params_sum($alias, $campo_canceled, $campo_monto, $contrato_id, $entidad);
     * // Resultado:
     * // ['error' => 'Error al validar datos de entrada', 'data' => [...]]
     * ```
     */
    final public function params_sum(
        string $alias,
        string $campo_canceled,
        string $campo_monto,
        int $contrato_id,
        string $entidad
    ) {
        // Validar que contrato_id sea mayor a 0
        if ($contrato_id <= 0) {
            return (new error())->error('Error $contrato_id debe ser mayor a 0', $contrato_id);
        }

        // Limpiar el campo de cancelación
        $campo_canceled = trim($campo_canceled);

        // Validar los datos de entrada para la suma
        $valida = (new sql())->valida_entrada_rename($alias, $campo_monto, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos de entrada', $valida);
        }

        // Generar la expresión SQL para la suma con alias
        $campo_doc_total = (new sql())->campo_sum_rename($alias, $campo_monto, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $campo_doc_total);
        }

        // Generar la cláusula WHERE
        $where = (new contrato())->where_sum($campo_canceled, $contrato_id, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $where', $where);
        }

        // Construir y retornar los parámetros
        $params = new stdClass();
        $params->campo = $campo_doc_total;
        $params->where = $where;

        return $params;
    }



    /**
     * EM3
     * Genera un array de condiciones SQL (`WHERE`) para sumar datos en una entidad, incluyendo estado activo,
     * cancelación y un identificador de contrato.
     *
     * Esta función valida los parámetros de entrada, genera dinámicamente las condiciones SQL necesarias,
     * y las organiza en un array con claves descriptivas.
     *
     * @param string $campo_canceled El nombre del campo que indica si un registro está cancelado. Puede estar vacío.
     * @param int $contrato_id El identificador del contrato. Debe ser mayor a 0.
     * @param string $entidad El nombre de la entidad (tabla) donde se aplicarán las condiciones. No debe estar vacío.
     *
     * @return array Devuelve un array con las condiciones SQL generadas:
     *               - `wh_status`: Condición para registros con estado activo.
     *               - `wh_canceled` (opcional): Condición para registros no cancelados, si `$campo_canceled` está definido.
     *               - `wh_contrato_id`: Condición para filtrar por `contrato_id`.
     *               En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar condiciones WHERE válidas
     * $campo_canceled = 'Canceled';
     * $contrato_id = 123;
     * $entidad = 'contratos';
     * $resultado = $this->sentencias_sum($campo_canceled, $contrato_id, $entidad);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     'wh_status' => 'contratos.status = "activo"',
     * //     'wh_canceled' => 'contratos.CANCELED = "N"',
     * //     'wh_contrato_id' => 'contratos.contrato_id = 123',
     * // ]
     *
     * // Ejemplo 2: Sin campo de cancelación
     * $campo_canceled = '';
     * $contrato_id = 123;
     * $entidad = 'contratos';
     * $resultado = $this->sentencias_sum($campo_canceled, $contrato_id, $entidad);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     'wh_status' => 'contratos.status = "activo"',
     * //     'wh_contrato_id' => 'contratos.contrato_id = 123',
     * // ]
     *
     * // Ejemplo 3: Error por contrato_id inválido
     * $contrato_id = 0;
     * $resultado = $this->sentencias_sum($campo_canceled, $contrato_id, $entidad);
     * // Resultado:
     * // ['error' => 'Error $contrato_id debe ser mayor a 0', 'data' => 0]
     *
     * // Ejemplo 4: Error por entidad vacía
     * $entidad = '';
     * $resultado = $this->sentencias_sum($campo_canceled, $contrato_id, $entidad);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacio', 'data' => '']
     * ```
     */
    private function sentencias_sum(string $campo_canceled, int $contrato_id, string $entidad): array
    {
        // Validar que contrato_id sea mayor a 0
        if ($contrato_id <= 0) {
            return (new error())->error('Error $contrato_id debe ser mayor a 0', $contrato_id);
        }

        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        // Generar condición para estado activo
        $wh_status = (new sql())->wh_status_act($entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $wh_status);
        }

        // Inicializar el array de condiciones
        $sentencias = [];

        // Generar condición para registros no cancelados (opcional)
        $campo_canceled = trim($campo_canceled);
        if ($campo_canceled !== '') {
            $wh_canceled = (new sql())->wh_canceled_n($entidad);
            if (error::$en_error) {
                return (new error())->error('Error al obtener sql', $wh_canceled);
            }
            $sentencias['wh_canceled'] = $wh_canceled;
        }

        // Generar condición para contrato_id
        $wh_contrato_id = $this->wh_contrato_id($contrato_id, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $wh_contrato_id);
        }

        // Agregar las condiciones al array
        $sentencias['wh_status'] = $wh_status;
        $sentencias['wh_contrato_id'] = $wh_contrato_id;

        // Retornar las condiciones generadas
        return $sentencias;
    }


    final public function sql_upd_monto_pago(string $entidad, int $id, float $monto_pago_default, float $monto_pago_min, stdClass $row_contrato): string
    {
        $sql_upd_monto_pago = '';
        if($monto_pago_min > 0.0){
            if((float)$row_contrato->U_ImportePago < $monto_pago_min){
                $sql_upd_monto_pago = "UPDATE $entidad SET U_ImportePago = '$monto_pago_default' WHERE id = $id";
            }
        }

        return $sql_upd_monto_pago;

    }

    final public function tiene_nc_sap_upd(int $contrato_id, PDO $link): array
    {
        $sql = /** @lang MYSQL */
            "UPDATE contrato SET tiene_nc_sap = 'activo' WHERE contrato.id = $contrato_id";
        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al ajusta si tiene nc',$exe);
        }
        return $exe;
    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para actualizar el ID de la empresa asociada a un contrato.
     *
     * Esta función valida los parámetros de entrada y construye una consulta SQL para actualizar
     * el campo `empresa_id` de la tabla `contrato` en función del ID del contrato proporcionado.
     *
     * @param int $contrato_id ID del contrato que se desea actualizar. Debe ser mayor a 0.
     * @param int $empresa_id ID de la empresa que se asignará al contrato. Debe ser mayor a 0.
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada si los parámetros son válidos.
     *                      En caso de error, devuelve un arreglo con información detallada del fallo.
     *
     * Ejemplo de uso:
     * ```php
     * $contrato_id = 123;
     * $empresa_id = 456;
     * $sql = $this->update_empresa($contrato_id, $empresa_id);
     * if (isset($sql['error'])) {
     *     echo $sql['error'];
     * } else {
     *     echo $sql; // Consulta SQL generada
     * }
     * ```
     *
     * Salida esperada (ejemplo):
     * ```sql
     * UPDATE contrato SET empresa_id = 456 WHERE id = 123
     * ```
     *
     * Errores posibles:
     * - `['error' => 'Error $contrato_id debe ser mayor a 0', 'detalle' => $contrato_id]` si `$contrato_id` es menor o igual a 0.
     * - `['error' => 'Error $empresa_id debe ser mayor a 0', 'detalle' => $empresa_id]` si `$empresa_id` es menor o igual a 0.
     */
    final public function update_empresa(int $contrato_id, int $empresa_id)
    {
        if($contrato_id <= 0){
            return (new error())->error('Error $contrato_id debe ser mayor a 0',$contrato_id);
        }
        if($empresa_id <= 0){
            return (new error())->error('Error $empresa_id debe ser mayor a 0',$empresa_id);
        }
        return /** @lang MYSQL */"UPDATE contrato SET empresa_id = $empresa_id WHERE id = $contrato_id";

    }

    final public function ultima_rev_sap_upd(int $contrato_id, PDO $link): array
    {
        $ultima_rev_sap = date('Y-m-d H:i:s');
        $sql = /** @lang MYSQL */
            "UPDATE contrato SET ultima_rev_sap = '$ultima_rev_sap' WHERE contrato.id = $contrato_id";
        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al ajustar ultima rev',$exe);
        }
        return $exe;

    }

    final public function obten_contrato_excel(array $campos_base, string $campo_folio,string $campo_serie,
    string $contrato = '', int $contrato_id = 0,int $limit = 0, int $offset = -1,int $plaza_id = 0) {
        
        if(empty($campos_base)) {
            return (new error())->error('Error campos vacios',$campos_base);
        }
        $sql_limit = "";
        if($limit > 0) {
            $sql_limit = " LIMIT $limit";
            if($offset > -1) {
                $sql_limit .= " OFFSET $offset";
            }
        }

        $campos = (new sql())->campos_sql_puros_with_as($campos_base);
        if (error::$en_error) {
            return (new error())->error('Error al generar $campos', $campos);
        }
        $wh = "";
        if(trim($contrato) !== '' && $contrato_id < 0) {
            $folio = preg_replace('/[^0-9]/', '', $contrato);
            $serie = preg_replace('/[^a-zA-Z]/', '', $contrato);
            $wh = "WHERE contrato.$campo_serie = '$serie' AND contrato.$campo_folio = '$folio'";
            if($plaza_id > 0) {
             $wh .= " AND contrato.plaza_id = $plaza_id";
            }
        }
        if($contrato_id > 0) {
            $wh = "WHERE contrato.id = $contrato_id";
        }

        $sentencia_wh = $wh . "" . $sql_limit;
        return /** @lang MYSQL */ "SELECT $campos FROM contrato $sentencia_wh";
    }
    
    /**
     * EM3
     * Genera una condición SQL `WHERE` para filtrar por `contrato_id` en una entidad específica.
     *
     * Esta función valida los parámetros de entrada y construye una expresión SQL con el formato
     * `entidad.contrato_id = contrato_id`.
     *
     * @param int $contrato_id El identificador del contrato. Debe ser mayor a 0.
     * @param string $entidad El nombre de la entidad (tabla) donde se encuentra el campo `contrato_id`. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la condición `WHERE`. En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar condición WHERE válida
     * $contrato_id = 123;
     * $entidad = 'contratos';
     * $resultado = $this->wh_contrato_id($contrato_id, $entidad);
     * echo $resultado;
     * // Resultado:
     * // contratos.contrato_id = 123
     *
     * // Ejemplo 2: Error por contrato_id inválido
     * $contrato_id = 0;
     * $entidad = 'contratos';
     * $resultado = $this->wh_contrato_id($contrato_id, $entidad);
     * // Resultado:
     * // ['error' => 'Error $contrato_id debe ser mayor a 0', 'data' => 0]
     *
     * // Ejemplo 3: Error por entidad vacía
     * $contrato_id = 123;
     * $entidad = '';
     * $resultado = $this->wh_contrato_id($contrato_id, $entidad);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacio', 'data' => '']
     * ```
     */
    private function wh_contrato_id(int $contrato_id, string $entidad)
    {
        // Validar que contrato_id sea mayor a 0
        if ($contrato_id <= 0) {
            return (new error())->error('Error $contrato_id debe ser mayor a 0', $contrato_id);
        }

        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        // Retornar la condición SQL
        return "$entidad.contrato_id = $contrato_id";
    }


    private function where_contrato_gestor($estructura,$ohem_id,$status) {
        if($ohem_id <= 0){
            return (new error())->error('Error $ohem_id debe ser mayor a 0',$ohem_id);
        }
        $estructuras_valida = array('SAP','EM3');
        if(!in_array($estructura,$estructuras_valida)){
            return  (new error())->error('Error $estructura invalida',$estructuras_valida);
        }
        if($estructura === 'SAP') {
            $where = "cliente.ohem_id = $ohem_id";
            if($status === 'activo'){
                $where .= " AND contrato.CANCELED = 'N'";
            }
            if($status === 'inactivo'){
                $where .= " AND contrato.CANCELED = 'Y'";
            }
        } else {
            $where = "contrato.empleado_gestor_id = $ohem_id";
        }

        $status = trim($status);
        if($status ==! ''){
            $where .= " AND contrato.STATUS = '$status'";
        }

        return $where;
    }

    /**
     * EM3
     * Genera una cláusula SQL `WHERE` para sumar datos en una entidad, basada en filtros de estado, cancelación y contrato.
     *
     * Esta función valida los parámetros de entrada, genera las condiciones necesarias utilizando `sentencias_sum`,
     * y las combina en una cláusula `WHERE`.
     *
     * @param string $campo_canceled El nombre del campo que indica si un registro está cancelado. Puede estar vacío.
     * @param int $contrato_id El identificador del contrato. Debe ser mayor a 0.
     * @param string $entidad El nombre de la entidad (tabla) donde se aplicarán las condiciones. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `WHERE` generada. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula WHERE válida
     * $campo_canceled = 'Canceled';
     * $contrato_id = 123;
     * $entidad = 'contratos';
     * $resultado = $this->where_sum($campo_canceled, $contrato_id, $entidad);
     * echo $resultado;
     * // Resultado:
     * // contratos.status = "activo" AND contratos.CANCELED = "N" AND contratos.contrato_id = 123
     *
     * // Ejemplo 2: Sin campo de cancelación
     * $campo_canceled = '';
     * $contrato_id = 123;
     * $entidad = 'contratos';
     * $resultado = $this->where_sum($campo_canceled, $contrato_id, $entidad);
     * echo $resultado;
     * // Resultado:
     * // contratos.status = "activo" AND contratos.contrato_id = 123
     *
     * // Ejemplo 3: Error por contrato_id inválido
     * $contrato_id = 0;
     * $resultado = $this->where_sum($campo_canceled, $contrato_id, $entidad);
     * // Resultado:
     * // ['error' => 'Error $contrato_id debe ser mayor a 0', 'data' => 0]
     *
     * // Ejemplo 4: Error por entidad vacía
     * $entidad = '';
     * $resultado = $this->where_sum($campo_canceled, $contrato_id, $entidad);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacio', 'data' => '']
     * ```
     */
    private function where_sum(string $campo_canceled, int $contrato_id, string $entidad)
    {
        // Validar que contrato_id sea mayor a 0
        if ($contrato_id <= 0) {
            return (new error())->error('Error $contrato_id debe ser mayor a 0', $contrato_id);
        }

        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        // Generar las condiciones para el WHERE
        $campo_canceled = trim($campo_canceled);
        $sentencias = (new contrato())->sentencias_sum($campo_canceled, $contrato_id, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $sentencias', $sentencias);
        }

        // Combinar las condiciones en una cláusula WHERE
        $where = (new sql())->where($sentencias);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $where', $where);
        }

        // Retornar la cláusula WHERE generada
        return $where;
    }

}
