<?php
namespace desarrollo_em3\manejo_datos\sql;

class empresa{
    /**
     * EM3
     * Genera una consulta SQL para obtener las empresas con el atributo `es_324 = 'activo'`.
     *
     * Este método devuelve una cadena SQL que selecciona todas las columnas de la tabla `empresa`
     * donde el campo `es_324` está marcado como 'activo'. Se puede utilizar en una consulta PDO
     * para recuperar empresas que cumplen con este criterio.
     *
     * @return string Consulta SQL `SELECT` para obtener empresas activas bajo la condición `es_324 = 'activo'`.
     *
     * @example
     * ```php
     * // Obtener la consulta SQL
     * $sql = (new empresa())->empresa_324();
     * echo $sql;
     * ```
     * **Salida esperada:**
     * ```sql
     * SELECT * FROM empresa WHERE empresa.es_324 = 'activo'
     * ```
     *
     * @example
     * ```php
     * // Ejecutar la consulta con PDO
     * $pdo = new PDO("mysql:host=localhost;dbname=mi_base", "usuario", "contraseña");
     * $query = $pdo->query((new empresa())->empresa_324());
     * $empresas = $query->fetchAll(PDO::FETCH_OBJ);
     * print_r($empresas);
     * ```
     * **Salida esperada:**
     * ```php
     * Array
     * (
     *     [0] => stdClass Object
     *         (
     *             [id] => 1
     *             [nombre] => Empresa A
     *             [es_324] => activo
     *         )
     *     [1] => stdClass Object
     *         (
     *             [id] => 2
     *             [nombre] => Empresa B
     *             [es_324] => activo
     *         )
     * )
     * ```
     */
    final public function empresa_324(): string
    {
        return /** @lang MYSQL */ "SELECT * FROM empresa WHERE empresa.es_324 = 'activo'";
    }


}
