<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;
use PDO;

class esquema{


    private string $name_entidad = 'esquema';

    /**
     * EM3
     * Obtiene y genera una cadena SQL con los campos de la entidad `esquema`, separada por comas.
     *
     * Esta función obtiene los campos de la entidad `esquema`, los formatea en una cadena de SQL con alias
     * y devuelve el resultado. En caso de error en cualquier paso del proceso, devuelve un array con los detalles del error.
     *
     * ### Proceso:
     * 1. Obtiene los campos de la entidad `esquema` utilizando la función `campos()`.
     * 2. Genera la cadena SQL con los nombres de los campos separados por comas, utilizando `campos_sql_coma()`.
     * 3. Devuelve la cadena SQL con los campos formateados.
     *
     * @param PDO $link Instancia de PDO para realizar la consulta en la base de datos.
     *
     * @return string|array Retorna una cadena de texto con los campos de la entidad `esquema` en formato SQL.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener los campos de la entidad esquema en formato SQL
     * $link = new PDO(...); // Conexión a la base de datos
     * $esquema = new \desarrollo_em3\manejo_datos\sql\esquema();
     * $resultado = $esquema->campos_sql($link);
     * echo $resultado;
     *
     * // Salida esperada:
     * // "esquema.id AS esquema_id, esquema.nombre AS esquema_nombre, esquema.descripcion AS esquema_descripcion"
     * ```
     *
     * ```php
     * // Ejemplo 2: Error al obtener los campos de la entidad
     * $link = new PDO(...); // Conexión inválida o fallida
     * $esquema = new \desarrollo_em3\manejo_datos\sql\esquema();
     * $resultado = $esquema->campos_sql($link);
     *
     * // Salida esperada (error):
     * // ['error' => 'Error al obtener $campos', 'data' => [...]]
     * ```
     */
    final public function campos_sql(PDO $link)
    {
        // Obtener los campos de la entidad `esquema`
        $campos = (new \desarrollo_em3\manejo_datos\data\entidades\esquema())->campos($link);
        if(error::$en_error){
            return (new error())->error('Error al obtener $campos', $campos);
        }

        // Generar la cadena SQL con los campos formateados
        $sql = (new sql())->campos_sql_coma($campos, $this->name_entidad);
        if(error::$en_error){
            return (new error())->error('Error al generar $campos', $sql);
        }

        // Retornar la cadena SQL con los campos formateados
        return $sql;
    }


    /**
     * EM3
     * Genera una condición SQL para filtrar registros de `esquema_guardadito` por un rango de fechas.
     *
     * Esta función construye una condición SQL `AND 'fecha' BETWEEN esquema_guardadito.fecha_inicio AND esquema_guardadito.fecha_fin`
     * para verificar si la fecha proporcionada está dentro del rango definido en los campos `fecha_inicio` y `fecha_fin` de la tabla.
     * Si la fecha ingresada está vacía, se devuelve un objeto de error.
     *
     * @param string $fecha Fecha en formato `YYYY-MM-DD`. No debe estar vacía.
     *
     * @return string|array Devuelve una cadena SQL con la condición `BETWEEN` para filtrar registros por fecha.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar condición SQL con una fecha válida
     * $fecha = '2024-02-01';
     * $sql_extra = (new esquema())->sql_extra_esquema_guardadito($fecha);
     * echo $sql_extra;
     *
     * // Salida esperada:
     * // " AND '2024-02-01' BETWEEN esquema_guardadito.fecha_inicio AND esquema_guardadito.fecha_fin"
     * ```
     *
     * ```php
     * // Ejemplo 2: Error por fecha vacía
     * $fecha = '';
     * $sql_extra = (new esquema())->sql_extra_esquema_guardadito($fecha);
     *
     * // Salida esperada (error):
     * // ['error' => 'Error $fecha esta vacia', 'data' => '']
     * ```
     */
    final public function sql_extra_esquema_guardadito(string $fecha)
    {
        // Validar que la fecha no esté vacía
        $fecha = trim($fecha);
        if ($fecha === '') {
            return (new error())->error('Error $fecha esta vacia', $fecha);
        }

        // Generar la condición SQL para filtrar por fecha
        return " AND '$fecha' BETWEEN esquema_guardadito.fecha_inicio AND esquema_guardadito.fecha_fin";
    }



}
