<?php
namespace desarrollo_em3\manejo_datos\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;
use stdClass;

class oficina_trabajo{

    private string $key_id_original = 'oficina_trabajo.id';
    private string $name_entidad = 'oficina_trabajo';

    /**
     * TRASLADADO
     * Obtiene los datos de una oficina de trabajo por su ID.
     *
     * Esta función genera una consulta SQL para obtener el ID y la descripción de una oficina de trabajo
     * utilizando su identificador único. Si el ID proporcionado es menor o igual a 0, devuelve un error.
     *
     * @param int $oficina_trabajo_id El ID de la oficina de trabajo a consultar.
     *
     * @return string|array La sentencia SQL que selecciona el ID y la descripción de la oficina de trabajo.
     * Si el ID es menor o igual a 0, se devuelve un error.
     *
     * @throws error Si el ID proporcionado es menor o igual a 0.
     */
    final public function get_oficina_trabajo_by_id(int $oficina_trabajo_id) {
        if($oficina_trabajo_id <= 0) {
            return (new error())->error('Error oficina_trabajo es menor a 0',$oficina_trabajo_id);
        }

        $id_cmp = "id AS oficina_trabajo_id";
        $desc_cmp = "IFNULL(descripcion,'') AS oficina_trabajo_descripcion";

        $wh = "oficina_trabajo.id = $oficina_trabajo_id";

        return /** @lang   MYSQL */ "SELECT $id_cmp, $desc_cmp FROM oficina_trabajo WHERE $wh";
    }
}
