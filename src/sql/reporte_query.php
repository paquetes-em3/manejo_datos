<?php
namespace desarrollo_em3\manejo_datos\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;
use stdClass;

class reporte_query{

    private string $key_id_original = 'reporte_query.id';
    private string $name_entidad = 'reporte_query';

    public function obten_id_permisos_departamento(int $usuario_id) {
        if($usuario_id <= 0){  
            return (new error())->error('Error $usuario_id es menor a 0',$usuario_id);
        }

        $campos = "IFNULL(GROUP_CONCAT(acl_usuario_departamento.departamento_id)," . "0" . ") AS departamentos";
        $from = "acl_usuario_departamento";
        $where = "acl_usuario_departamento.usuario_id = $usuario_id";

        return /** @lang MYSQL */ "SELECT $campos FROM $from WHERE $where";
    }

    public function obten_id_permisos_plaza(int $usuario_id) {
        if($usuario_id <= 0){  
            return (new error())->error('Error $usuario_id es menor a 0',$usuario_id);
        }

        $campos = "IFNULL(GROUP_CONCAT(acl_usuario_plaza.plaza_id),'0') AS plazas";
        $from = "acl_usuario_plaza";
        $where = "acl_usuario_plaza.usuario_id = $usuario_id";

        return /** @lang MYSQL */ "SELECT $campos FROM $from WHERE $where";
    }
}