<?php
namespace desarrollo_em3\manejo_datos\sql;


use desarrollo_em3\error\error;
use stdClass;

class configuracion_script{


    private function en_ejecucion(string $numero_empresa, string $script): string
    {

        $set = "configuracion_script.en_ejecucion = 'activo'";
        $wh_n_empresa = "configuracion_script.empresa_db = '$numero_empresa'";
        $wh_name_sc = "configuracion_script.nombre = '$script'";

        return /** @lang MYSQL */ "UPDATE configuracion_script SET $set WHERE $wh_n_empresa AND $wh_name_sc";


    }

    private function finalizado(string $numero_empresa, string $script): string
    {

        $set = "configuracion_script.en_ejecucion = 'inactivo'";
        $wh_n_empresa = "configuracion_script.empresa_db = '$numero_empresa'";
        $wh_name_sc = "configuracion_script.nombre = '$script'";

        return /** @lang MYSQL */ "UPDATE configuracion_script SET $set WHERE $wh_n_empresa AND $wh_name_sc";
    }

    final public function get_conf(string $numero_empresa, string $script)
    {
        $numero_empresa = trim($numero_empresa);
        if($numero_empresa === ''){
            return (new error())->error('Error $numero_empresa esta vacia',$numero_empresa);
        }
        if((int)$numero_empresa <= 0){
            return (new error())->error('Error $numero_empresa debe ser mayor a 0',$numero_empresa);
        }
        $script = trim($script);
        if($script === ''){
            return (new error())->error('Error $script esta vacio',$script);
        }
        $en_exe = "configuracion_script.en_ejecucion AS configuracion_script_en_ejecucion";
        $wh_n_empresa = "configuracion_script.empresa_db = '$numero_empresa'";
        $wh_name_sc = "configuracion_script.nombre = '$script'";
        return /** @lang MYSQL */ "SELECT $en_exe FROM configuracion_script WHERE $wh_n_empresa AND $wh_name_sc";

    }

    final public function sqls(string $numero_empresa, string $script){

        $get = $this->get_conf($numero_empresa, $script);
        if(error::$en_error){
            return (new error())->error('Error al obtener conf',$get);
        }
        $finalizado = $this->finalizado($numero_empresa, $script);
        if(error::$en_error){
            return (new error())->error('Error al obtener $finalizado',$finalizado);
        }
        $en_ejecucion = $this->en_ejecucion($numero_empresa, $script);
        if(error::$en_error){
            return (new error())->error('Error al obtener $en_ejecucion',$en_ejecucion);
        }

        $sqls = new stdClass();
        $sqls->get = $get;
        $sqls->finalizado = $finalizado;
        $sqls->en_ejecucion = $en_ejecucion;

        return $sqls;

    }


}
