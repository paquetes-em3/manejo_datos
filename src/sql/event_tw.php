<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;

class event_tw{

    private string $name_entidad = 'event_tw';

    /**
     * TRASLADADO
     * Genera una consulta SQL para contar el número de eventos asociados a un empleado en una fecha específica.
     *
     * Esta función valida que el nombre de la entidad del empleado no esté vacío. Si la fecha está vacía,
     * se establece un valor predeterminado de '1900-01-01'. Luego, genera una consulta SQL que cuenta los registros
     * en la tabla correspondiente donde coinciden el ID del empleado y la fecha especificada. Si el nombre de la entidad
     * del empleado está vacío, devuelve un error.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param string $fecha Fecha del evento (en formato 'YYYY-MM-DD').
     * @param int $ohem_id ID del empleado para el cual se están contando los eventos.
     *
     * @return string|array Devuelve una consulta SQL para contar el número de eventos o un array de error si ocurre algún problema.
     */
    final public function n_eventos(string $entidad_empleado, string $fecha, int $ohem_id)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error entidad empleado esta vacia', $entidad_empleado);
        }
        $fecha = trim($fecha);
        if($fecha === ''){
            $fecha = '1900-01-01';
        }
        $key_empleado_id = $entidad_empleado.'_id';
        $wh_id = "$key_empleado_id = $ohem_id";

        $fecha_ini = $fecha.' 00:00:00';
        $fecha_fin = $fecha.' 23:59:59';

        $wh_fecha = "fecha BETWEEN '$fecha_ini' AND '$fecha_fin'";

        return /** @lang MYSQL */ "SELECT COUNT(*) AS n_eventos FROM $this->name_entidad WHERE $wh_id AND $wh_fecha";

    }


}
