<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;
use stdClass;

class acl_usuario {

    /**
     * TOTAL
     * Obtiene una consulta SQL para obtener los IDs relacionados con permisos de una entidad específica para un usuario dado.
     *
     * Esta función construye una consulta SQL que selecciona los IDs relacionados con la entidad especificada para el usuario
     * proporcionado. La consulta puede ser filtrada por un estado opcional si se especifica.
     *
     * @param string $entidad La entidad para la cual se desean obtener los IDs. No puede ser una cadena vacía.
     * @param int $usuario_id El ID del usuario para quien se están obteniendo los permisos. Debe ser un valor mayor a 0.
     * @param string $status (Opcional) Estado para filtrar la consulta. Si se proporciona, se agrega un filtro adicional
     *                       en la cláusula `WHERE`.
     *
     * @return string|array Retorna una cadena SQL con la consulta construida si la validación es exitosa,
     *               o un array de error si alguna de las validaciones falla.
     *
     * @throws error Si la entidad está vacía o si la validación de los datos de permiso falla.
     *
     * @access public
     * @final
     */
    final public function obten_ids_permiso(string $entidad, int $usuario_id,string $status = '') {
        if(trim($entidad) === '') {
            return (new error())->error('Error no puede ser vacio',$entidad);
        }
        $valida = $this->valida_datos_permiso($entidad,$usuario_id);
        if(error::$en_error){
            return  (new error())->error('Error al validar datos',$valida);
        }
        
        $from = "acl_usuario_"."$entidad";
        $campo_id = "$entidad"."_id";
        $campo = "IFNULL(GROUP_CONCAT($from.$campo_id ),'0') AS $entidad ";
        $wh_status = '';
        if(trim($status) !== '') {
            $wh_status = "AND $from.status = " . "'" . $status . "'";
        }
        $where = "$from.usuario_id = $usuario_id";

        return /** @lang   MYSQL */ "SELECT $campo FROM $from WHERE $where $wh_status";
    }

    /**
     * TOTAL
     * Valida los datos proporcionados para permisos de acceso.
     *
     * Esta función valida que la entidad proporcionada sea una cadena no vacía y que el ID del usuario sea un valor
     * mayor a 0. Además, verifica si la entidad pertenece a un conjunto permitido de valores (`departamento` o `plaza`).
     * Si todas las validaciones son correctas, devuelve `true`; en caso contrario, devuelve un array con el detalle del error.
     *
     * @param string $entidad La entidad que se desea validar (debe ser una cadena no vacía).
     *                        Valores válidos: 'departamento' o 'plaza'.
     * @param int $usuario_id ID del usuario. Debe ser un valor mayor a 0.
     *
     * @return true|array Retorna `true` si todas las validaciones son exitosas, o un array de error si ocurre alguna falla.
     *
     * @throws error Si la entidad está vacía, el usuario ID es menor o igual a 0, o la entidad no es válida.
     *
     * @access public
     * @final
     */
    final public function valida_datos_permiso(string $entidad, int $usuario_id)
    {
        if(trim($entidad) === '') {
            return (new error())->error('Error no puede ser vacio',$entidad);
        }
        if($usuario_id <= 0) {
            return (new error())->error('Error $usuario_id es menor a 0',$usuario_id);
        }
        $entidad_valida = array('departamento','plaza');
        if(!in_array($entidad,$entidad_valida)){
            return  (new error())->error('Error $estructura invalida',$entidad_valida);
        }
        return true;

    }
}