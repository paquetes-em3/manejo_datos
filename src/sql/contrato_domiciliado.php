<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;

class contrato_domiciliado{


    private function columnas()
    {
        $columnas_basicas = ['contrato_domiciliado.id', 'contrato_domiciliado.number_token',
            'contrato_domiciliado.expmonth','contrato_domiciliado.expyear', 'contrato_domiciliado.moneda',
            'contrato.id', 'contrato.U_SeCont', 'contrato.U_FolioCont', 'contrato.CardCode', 'contrato.dv_santander',
            'contrato.CardName', 'contrato.U_ImportePago', 'contrato.dia_pago', 'contrato.dia_pago_1',
            'contrato.dia_pago_2', 'contrato.DocTotal', 'contrato.PaidToDate',
            'plaza.id', 'plaza.descripcion'];

        $columnas_sql = "";
        foreach ($columnas_basicas as $columna_basica) {
            $alias = str_replace('.','_',$columna_basica);
            $columna_sql = $columna_basica . " AS " . $alias;
            $coma = (new sql())->coma($columnas_sql);
            if(error::$en_error){
                return (new error())->error('Error al generar coma', $coma);
            }

            $columnas_sql.=$coma.$columna_sql;
        }
        return $columnas_sql;

    }

    final public function contratos_exe_pagos_dom(int $dia_pago, string $periodicidad_pago)
    {
        $periodicidad_pago = trim($periodicidad_pago);
        if($periodicidad_pago === ''){
            return (new error())->error('Error $periodicidad_pago esta vacio', $periodicidad_pago);
        }

        $periodicidades_validas = array('S','M','Q');
        if(!in_array($periodicidad_pago,$periodicidades_validas)){
            return (new error())->error('Error $periodicidad_pago invalida', $periodicidades_validas);
        }

        if($dia_pago <= 0){
            return (new error())->error('Error dia_pago debe ser mayor a 0', $dia_pago);
        }
        if($dia_pago >= 32){
            return (new error())->error('Error dia_pago debe ser menor a 32', $dia_pago);
        }
        $columnas = $this->columnas();
        if(error::$en_error){
            return (new error())->error('Error al generar $columnas', $columnas);
        }
        $where = $this->where_base_exe($dia_pago, $periodicidad_pago);
        if(error::$en_error){
            return (new error())->error('Error al generar $where', $where);
        }

        $sql = "SELECT $columnas FROM contrato_domiciliado LEFT JOIN contrato ON contrato_domiciliado.contrato_id = contrato.id LEFT JOIN plaza ON contrato.plaza_id = plaza.id WHERE $where GROUP BY contrato.id ORDER BY plaza.id ASC";

        return $sql;

    }

    final public function es_domiciliado(int $contrato_id, string $alias = '')
    {
        $st_wh = (new sql())->wh_status_act('contrato_domiciliado');
        if(error::$en_error){
            return (new error())->error('Error al generar statement',$st_wh);
        }
        $alias = trim($alias);
        $alias_sq = '';
        if($alias !== '') {
            $alias_sq = "AS $alias";
        }

        $contrato_id_st = "contrato_domiciliado.contrato_id = $contrato_id";
        return /** @lang MYSQL */ "SELECT COUNT(*) $alias_sq FROM contrato_domiciliado WHERE $contrato_id_st AND $st_wh";

    }

    private function where_dia_pago(int $dia_pago, string $periodicidad_pago)
    {
        $periodicidad_pago = trim($periodicidad_pago);
        if($periodicidad_pago === ''){
            return (new error())->error('Error $periodicidad_pago esta vacio', $periodicidad_pago);
        }

        $periodicidades_validas = array('S','M','Q');

        if(!in_array($periodicidad_pago,$periodicidades_validas)){
            return (new error())->error('Error $periodicidad_pago invalida', $periodicidades_validas);
        }

        if($dia_pago <= 0){
            return (new error())->error('Error dia_pago debe ser mayor a 0', $dia_pago);
        }
        if($dia_pago >= 32){
            return (new error())->error('Error dia_pago debe ser menor a 32', $dia_pago);
        }


        $where_dia_pago = "";

        if($periodicidad_pago === 'S'){
            $where_dia_pago = $this->wr_dia_p_semana($dia_pago);
            if(error::$en_error){
                return (new error())->error('Error al obtener $where_dia_pago', $where_dia_pago);
            }
        }

        if($periodicidad_pago === 'Q'){
            $where_dia_pago = $this->wr_dia_p_quincena($dia_pago);
            if(error::$en_error){
                return (new error())->error('Error al obtener $where_dia_pago', $where_dia_pago);
            }
        }

        if($periodicidad_pago === 'M'){
            $where_dia_pago = $this->wr_dia_p_mes($dia_pago);
            if(error::$en_error){
                return (new error())->error('Error al obtener $where_dia_pago', $where_dia_pago);
            }
        }

        return $where_dia_pago;

    }


    private function where_general_mit(string $periodicidad_pago)
    {

        $periodicidad_pago = trim($periodicidad_pago);
        if($periodicidad_pago === ''){
            return (new error())->error('Error $periodicidad_pago esta vacio', $periodicidad_pago);
        }

        $periodicidades_validas = array('S','M','Q');
        if(!in_array($periodicidad_pago,$periodicidades_validas)){
            return (new error())->error('Error $periodicidad_pago invalida', $periodicidades_validas);
        }

        $where = "contrato_domiciliado.status = 'activo'";
        $where .= " AND contrato.U_CondPago = '$periodicidad_pago' ";
        $where .= " AND contrato.pagado = 'inactivo' ";
        $where .= " AND TRIM(contrato_domiciliado.number_token) <> ''";

        $where_st = "contrato.U_StatusCob = 'ACTIVO'";
        $where_st .= " OR contrato.U_StatusCob = 'OBSERVACION'";
        $where_st .= " OR contrato.U_StatusCob = 'PRE-CANCELADO'";
        $where_st .= " OR contrato.U_StatusCob = 'PROMESA PAGO'";
        $where_st .= " OR contrato.U_StatusCob = 'SUSPENSION TEMPORAL'";

        return "$where AND ($where_st)";


    }

    private function where_base_exe(int $dia_pago, string $periodicidad_pago)
    {

        $periodicidad_pago = trim($periodicidad_pago);
        if($periodicidad_pago === ''){
            return (new error())->error('Error $periodicidad_pago esta vacio', $periodicidad_pago);
        }

        $periodicidades_validas = array('S','M','Q');
        if(!in_array($periodicidad_pago,$periodicidades_validas)){
            return (new error())->error('Error $periodicidad_pago invalida', $periodicidades_validas);
        }

        if($dia_pago <= 0){
            return (new error())->error('Error dia_pago debe ser mayor a 0', $dia_pago);
        }
        if($dia_pago >= 32){
            return (new error())->error('Error dia_pago debe ser menor a 32', $dia_pago);
        }

        $where_ini = $this->where_general_mit($periodicidad_pago);
        if(error::$en_error){
            return (new error())->error('Error al obtener where_general_mit', $where_ini);
        }

        $where_dia = $this->where_dia_pago($dia_pago,$periodicidad_pago);
        if(error::$en_error){
            return (new error())->error('Error al obtener where_general_mit', $where_dia);
        }

        return "$where_ini AND $where_dia";

    }
    private function wr_dia_p_quincena(int $dia_pago)
    {
        if($dia_pago <= 0){
            return (new error())->error('Error dia_pago debe ser mayor a 0', $dia_pago);
        }
        if($dia_pago >= 32){
            return (new error())->error('Error dia_pago debe ser menor a 32', $dia_pago);
        }
        return "(contrato.dia_pago_1 = '$dia_pago' OR contrato.dia_pago_2 = '$dia_pago')";

    }

    private function wr_dia_p_mes(int $dia_pago)
    {
        if($dia_pago <= 0){
            return (new error())->error('Error dia_pago debe ser mayor a 0', $dia_pago);
        }
        if($dia_pago >= 32){
            return (new error())->error('Error dia_pago debe ser menor a 32', $dia_pago);
        }
        return "contrato.dia_pago_1 = '$dia_pago'";

    }

    private function wr_dia_p_semana(int $dia_pago)
    {
        if($dia_pago <= 0){
            return (new error())->error('Error dia_pago debe ser mayor a 0', $dia_pago);
        }
        if($dia_pago >= 8){
            return (new error())->error('Error dia_pago debe ser menor a 8', $dia_pago);
        }
        return "contrato.dia_pago = '$dia_pago'";

    }




}
