<?php
namespace desarrollo_em3\manejo_datos\sql;




use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;
use PDO;
use stdClass;

class periodo_pago{


    final public function periodos(PDO $link, bool $en_bruto = false, int $ejercicio_id = -1,
                                   string $fecha_final = '9999-12-31', string $fecha_inicial = '1900-01-01',
                                   int $mes_id = -1, string $order_by = '', int $periodicidad_pago_id = -1,
                                   string $status = 'activo')
    {


        $st_identificadores = $this->st_identificadores($ejercicio_id,$mes_id,$periodicidad_pago_id);
        if(error::$en_error){
            return (new error())->error('Error al generar $st_identificadores', $st_identificadores);
        }


        $st_fechas = $this->st_fechas_where($fecha_final, $fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al generar statement', $st_fechas);
        }

        $st_status = "status='$status'";

        $where = "$st_status $st_identificadores $st_fechas";

        $columnas_sql = "*";
        if(!$en_bruto){
            $columnas = (new consultas())->get_campos('periodo_pago',$link);
            if(error::$en_error){
                return (new error())->error('Error al obtener campos', $columnas);
            }
            $columnas_sql = $columnas->campos_sql;

        }

        return /** @lang MYSQL */ "SELECT $columnas_sql FROM periodo_pago WHERE $where $order_by";

    }

    private function st_ejercicio(int $ejercicio_id): string
    {
        $st_ejercicio = '';
        if($ejercicio_id > 0){
            $st_ejercicio = 'AND ejercicio_id = '.$ejercicio_id;
        }
        return $st_ejercicio;

    }

    private function st_fechas(string $fecha_final, string $fecha_inicial): stdClass
    {
        $st_fecha_final = "AND fecha_final <= '$fecha_final'";
        $st_fecha_inicial = "AND fecha_inicial >= '$fecha_inicial'";

        $data = new stdClass();
        $data->st_fecha_final = $st_fecha_final;
        $data->st_fecha_inicial = $st_fecha_inicial;

        return $data;


    }

    private function st_fechas_where(string $fecha_final, string $fecha_inicial)
    {
        $fechas = $this->st_fechas($fecha_final, $fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al generar statement', $fechas);
        }

        return "$fechas->st_fecha_final $fechas->st_fecha_inicial";

    }

    private function st_identificadores(int $ejercicio_id, int $mes_id, int $periodicidad_pago_id)
    {
        $st_ejercicio = $this->st_ejercicio($ejercicio_id);
        if(error::$en_error){
            return (new error())->error('Error al generar statement', $st_ejercicio);
        }
        $st_periodicidad_pago = $this->st_periodicidad_pago($periodicidad_pago_id);
        if(error::$en_error){
            return (new error())->error('Error al generar statement', $st_periodicidad_pago);
        }
        $st_mes = $this->st_mes($mes_id);
        if(error::$en_error){
            return (new error())->error('Error al generar statement', $st_mes);
        }

        return "$st_ejercicio $st_periodicidad_pago  $st_mes";

    }

    private function st_mes(int $mes_id): string
    {
        $st_mes = '';
        if($mes_id > 0){
            $st_mes = 'AND mes_id = '.$mes_id;
        }

        return $st_mes;

    }

    private function st_periodicidad_pago(int $periodicidad_pago_id): string
    {
        $st_periodicidad_pago = '';
        if($periodicidad_pago_id > 0){
            $st_periodicidad_pago = 'AND periodicidad_pago_id = '.$periodicidad_pago_id;
        }
        return $st_periodicidad_pago;

    }


}
