<?php
namespace desarrollo_em3\manejo_datos\sql;




use desarrollo_em3\error\error;

class usuario{


    /**
     * TRASLADADO
     * Genera una consulta SQL para activar el acceso a la aplicación de un usuario.
     *
     * Esta función devuelve una sentencia SQL que actualiza el campo `permite_acceso_app`
     * del usuario especificado, estableciéndolo como `'activo'`.
     *
     * @param int $usuario_id ID del usuario al que se le desea activar el acceso a la aplicación.
     *                        Debe ser mayor a 0.
     *
     * @return string|array Devuelve la consulta SQL para activar el acceso a la aplicación.
     *                Si el `$usuario_id` no es válido, devuelve un error detallado.
     *
     * Ejemplo de uso:
     * ```php
     * $usuario_id = 5;
     * $sql = $this->activa_acceso_app($usuario_id);
     *
     * if (is_array($sql) && isset($sql['error'])) {
     *     echo "Error: " . $sql['mensaje'];
     * } else {
     *     echo "Consulta generada: $sql";
     * }
     * ```
     *
     * Proceso:
     * 1. Valida que `$usuario_id` sea mayor a 0.
     * 2. Genera la consulta SQL para actualizar el campo `permite_acceso_app` del usuario.
     *
     * Errores posibles:
     * - `['error' => 'Error $usuario_id debe ser mayor a 0', 'detalle' => $usuario_id]` si `$usuario_id` es menor o igual a 0.
     */
    final public function activa_acceso_app(int $usuario_id)
    {
        if($usuario_id <= 0){
            return (new error())->error('Error $usuario_id debe ser mayor a 0', $usuario_id);
        }
        return /** @lang MYSQL */ "UPDATE usuario SET permite_acceso_app = 'activo' WHERE id = $usuario_id";

    }


}
