<?php
namespace desarrollo_em3\manejo_datos\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;
use stdClass;

class nota_credito{

    final public function inserta_nota_credito(stdClass $row)
    {
        $campos[] = 'status';
        $campos[] = 'usuario_alta_id';
        $campos[] = 'usuario_update_id';
        $campos[] = 'DocEntry';
        $campos[] = 'DocNum';
        $campos[] = 'DocDate';
        $campos[] = 'DocTotal';
        $campos[] = 'CANCELED';
        $campos[] = 'CardCode';
        $campos[] = 'Ref1';
        $campos[] = 'Comments';
        $campos[] = 'TransId';
        $campos[] = 'contrato_id';
        $campos[] = 'plaza_id';

        $campos_ins = '';
        $campos_ins_val = '';
        foreach ($campos as $campo){
            $coma = (new sql())->coma($campos_ins);
            if(error::$en_error){
                return (new error())->error('Error al integrar campo', $coma);
            }
            $campos_ins .= $coma.$campo;

            $coma = (new sql())->coma($campos_ins_val);
            if(error::$en_error){
                return (new error())->error('Error al integrar campo', $coma);
            }
            $val = addslashes(trim($row->$campo));
            $campos_ins_val .= $coma."'$val'";

        }

        return /** @lang MYSQL */ "INSERT INTO nota_credito ($campos_ins) VALUES ($campos_ins_val); ";

    }


    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener la suma de los montos de notas de crédito, filtrada por el estado de cancelación y el contrato.
     *
     * Esta función valida que el ID del contrato sea mayor a 0 y que el nombre del campo de monto no esté vacío. Luego,
     * genera los parámetros necesarios utilizando la función `params_sum` de la clase `contrato`, y finalmente genera
     * una consulta SQL para obtener la suma de los montos de las notas de crédito correspondientes al contrato y estado de cancelación
     * proporcionados. Si ocurre algún error durante el proceso de validación o generación de los parámetros, se devuelve un array
     * con los detalles del error.
     *
     * @param string $campo_canceled Campo que indica si la nota de crédito está cancelada o no.
     * @param string $campo_monto Campo que contiene el monto de la nota de crédito.
     * @param int $contrato_id ID del contrato al que pertenecen las notas de crédito.
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada o un array de error si ocurre algún problema.
     */
    final public function suma_nota_credito(string $campo_canceled,string $campo_monto, int $contrato_id)
    {
        if($contrato_id <= 0){
            return (new error())->error('Error $contrato_id debe ser mayor a 0',$contrato_id);
        }
        $campo_canceled = trim($campo_canceled);
        $campo_monto = trim($campo_monto);
        if($campo_monto === ''){
            return (new error())->error('Error $campo_monto esta vacio',$campo_monto);
        }
        $params = (new contrato())->params_sum('suma_nota_credito', $campo_canceled,$campo_monto,$contrato_id,
            'nota_credito');
        if(error::$en_error){
            return (new error())->error('Error al obtener parametros',$params);
        }
        return /** @lang MYSQL */ "SELECT $params->campo FROM nota_credito WHERE $params->where";

    }


}
