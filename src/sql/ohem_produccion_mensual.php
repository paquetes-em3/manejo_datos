<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;

class ohem_produccion_mensual{

    /**
     * FIN
     * Genera una consulta SQL para obtener el número de ventas personales de un empleado en el periodo comercial anterior.
     *
     * Esta función genera una consulta SQL que calcula el número de ventas personales (n_ventas_personales) de un empleado
     * (identificado por $ohem_id) en el periodo comercial anterior al actual. Verifica que el ID del empleado sea mayor a 0
     * antes de generar la consulta.
     *
     * @param int $ohem_id El ID del empleado. Debe ser mayor a 0.
     *
     * @return string|array La consulta SQL generada. Si $ohem_id no es válido, devuelve un array con el mensaje de error correspondiente.
     */
    final public function n_ventas_personales(int $ohem_id)
    {
        if($ohem_id<=0){
            return (new error())->error('Error ohem_id debe ser mayor a 0',$ohem_id);
        }
        $n_ventas_personales = "IFNULL(n_ventas_personales,0) AS 'n_ventas_personales'";
        $puesto_id = "produccion.puesto_id AS puesto_id";
        $campos = "$n_ventas_personales, $puesto_id";
        $join_oficina = "INNER JOIN oficina ON produccion.oficina_id = oficina.id";
        $join_tipo_oficina = "INNER JOIN tipo_oficina ON tipo_oficina.id = oficina.tipo_oficina_id";
        $produccion_mensual = "ohem_produccion_mensual AS produccion";
        $from = "FROM $produccion_mensual $join_oficina $join_tipo_oficina";
        $fecha_inicial = "periodo_comercial.fecha_inicial";
        $where_sq = "CURDATE() BETWEEN periodo_comercial.fecha_inicial AND periodo_comercial.fecha_final";
        $sq = /** @lang MYSQL */
            "SELECT $fecha_inicial FROM periodo_comercial WHERE $where_sq";
        $order = "ORDER BY periodo_comercial.fecha_final DESC";
        $periodo_comercial_id = "periodo_comercial.id";
        $where_sq_in = "WHERE periodo_comercial.fecha_final < ($sq) $order LIMIT 1";
        $sq_base = /** @lang MYSQL */
            "SELECT $periodo_comercial_id FROM periodo_comercial $where_sq_in";
        $condicion_sq = "produccion.periodo_comercial_id = ($sq_base)";
        $condicion_ohem_id = "produccion.ohem_id = $ohem_id";
        $condicion_aplica_pabs = "tipo_oficina.aplica_pabs = 'activo'";
        $where = "$condicion_sq AND $condicion_ohem_id AND $condicion_aplica_pabs";

        return /** @lang MYSQL */ "SELECT $campos $from WHERE $where";
    }
}
