<?php
namespace desarrollo_em3\manejo_datos\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\_valida;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\data\entidades\pago_corte\_reporte_fecha;
use desarrollo_em3\manejo_datos\sql;
use stdClass;

class pago_corte{

    private string $key_id_original = 'pago_corte.id';

    /**
     * EM3
     * Genera un array de configuraciones base para campos, validando previamente los datos de entrada.
     *
     * Esta función valida los campos de entrada y genera un array con configuraciones base para cada campo,
     * asociando cada campo con su entidad correspondiente.
     *
     * @param string $campo_folio El nombre del campo que representa el folio en la entidad "contrato". No debe estar vacío.
     * @param string $campo_monto_pago El nombre del campo que representa el monto del pago en la entidad "pago". No debe estar vacío.
     * @param string $campo_serie El nombre del campo que representa la serie en la entidad "contrato". No debe estar vacío.
     * @param string $entidad_empleado El nombre de la entidad o tabla relacionada con empleados. No debe estar vacío.
     *
     * @return array Devuelve un array con configuraciones de campos base. Cada entrada contiene:
     *                     - `entidad`: El nombre de la entidad (tabla).
     *                     - `campo`: El nombre del campo en la entidad.
     *                     En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar configuraciones base para campos
     * $campo_folio = 'folio';
     * $campo_monto_pago = 'monto';
     * $campo_serie = 'serie';
     * $entidad_empleado = 'empleado';
     * $resultado = $this->campos_base($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     ['entidad' => 'pago', 'campo' => 'id'],
     * //     ['entidad' => 'pago', 'campo' => 'monto'],
     * //     ['entidad' => 'contrato', 'campo' => 'id'],
     * //     ['entidad' => 'contrato', 'campo' => 'serie'],
     * //     ...
     * // ]
     *
     * // Ejemplo 2: Error por campo vacío
     * $campo_folio = '';
     * $resultado = $this->campos_base($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * // Resultado:
     * // ['error' => 'Error al validar datos', 'data' => [...]]
     * ```
     */
    private function campos_base(
        string $campo_folio,
        string $campo_monto_pago,
        string $campo_serie,
        string $entidad_empleado
    ): array {
        // Validar los datos de entrada
        $valida = $this->valida_data_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Generar configuraciones base para los campos
        $campos_base = array();

        $campos_base[] = ['entidad' => 'pago', 'campo' => 'id'];
        $campos_base[] = ['entidad' => 'pago', 'campo' => $campo_monto_pago];
        $campos_base[] = ['entidad' => 'contrato', 'campo' => 'id'];
        $campos_base[] = ['entidad' => 'contrato', 'campo' => $campo_serie];
        $campos_base[] = ['entidad' => 'pago_corte', 'campo' => 'id'];
        $campos_base[] = ['entidad' => $entidad_empleado, 'campo' => 'id'];
        $campos_base[] = ['entidad' => 'plaza', 'campo' => 'id'];
        $campos_base[] = ['entidad' => 'deposito_pago', 'campo' => 'id'];
        $campos_base[] = ['entidad' => 'deposito_pago', 'campo' => 'monto_depositado'];
        $campos_base[] = ['entidad' => 'cuenta_empresa', 'campo' => 'id'];
        $campos_base[] = ['entidad' => 'deposito_aportaciones', 'campo' => 'id'];
        $campos_base[] = ['entidad' => 'contrato', 'campo' => $campo_folio];

        // Retornar el array de configuraciones base
        return $campos_base;
    }


    /**
     * TRASLADADO
     * Genera una cadena SQL con los campos del empleado (ID y nombre completo) para su uso en una consulta.
     *
     * La función toma el nombre de la entidad de empleado, lo valida y luego genera los campos SQL para el empleado
     * (ID y nombre completo) utilizando la función `campos_empleado`. Si algún paso falla, devuelve un error.
     *
     * @param string $entidad_empleado El nombre de la entidad de empleado.
     *
     * @return string|array Devuelve una cadena SQL con los campos del empleado (empleado_id, nombre_completo).
     *                      Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function campos_emp_sql(string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }
        $campos_emp = $this->campos_empleado($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al integrar $campos_emp', $campos_emp);
        }

        return "$campos_emp->empleado_id, $campos_emp->nombre_completo";

    }

    /**
     * TRASLADADO
     * Genera y devuelve los campos relacionados con un empleado, incluyendo el ID y el nombre completo.
     *
     * La función toma el nombre de la entidad de empleado, lo valida y luego genera las claves relacionadas
     * con el empleado (ID y nombre completo) usando la función `keys_empleado`. Si algún paso falla, devuelve
     * un error. Al final, devuelve un objeto con los campos generados.
     *
     * @param string $entidad_empleado El nombre de la entidad de empleado.
     *
     * @return stdClass|array Devuelve un objeto con los campos del empleado generados (empleado_id, nombre_completo, keys).
     *                        Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function campos_empleado(string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }

        $keys_emp = $this->keys_empleado($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al integrar $keys_emp', $keys_emp);
        }

        $empleado_id_cmp = "$entidad_empleado.id AS $keys_emp->empleado_id";
        $empleado_nombre_completo_cmp = "$entidad_empleado.nombre_completo AS $keys_emp->nombre_completo";

        $campos = new stdClass();
        $campos->empleado_id = $empleado_id_cmp;
        $campos->nombre_completo = $empleado_nombre_completo_cmp;
        $campos->keys = $keys_emp;

        return $campos;

    }

    /**
     * TRASLADADO
     * Genera un objeto con los campos relacionados al pago de corte en formato SQL.
     *
     * La función crea un objeto `stdClass` con los campos clave de la entidad `pago_corte` (ID, fecha, montos y validación),
     * asignando a cada campo un alias en formato SQL, utilizando la propiedad `name_entidad` para nombrar los campos.
     * Los campos se generan con el formato `name_entidad.campo AS name_entidad_campo`.
     *
     * @return stdClass Devuelve un objeto con los campos del pago de corte en formato SQL.
     */
    private function campos_pago_corte(): stdClass
    {
        $keys[] = 'id';
        $keys[] = 'fecha';
        $keys[] = 'monto_total';
        $keys[] = 'monto_depositado';
        $keys[] = 'monto_por_depositar';
        $keys[] = 'validado';

        $campos = new stdClass();
        foreach ($keys as $campo){
            $campos->$campo = "$this->name_entidad.$campo AS $this->name_entidad"."_$campo";
        }
        return $campos;

    }

    /**
     * TRASLADADO
     * Genera una cadena SQL con los campos base del corte, incluyendo los campos de la plaza, empleado y pago de corte.
     *
     * La función toma como parámetro el nombre de la entidad de empleado, valida que no esté vacía y luego llama a otras
     * funciones para obtener los campos SQL de la entidad `empleado`, `pago_corte` y `plaza`. Los campos se concatenan
     * en una sola cadena que se devuelve como resultado.
     *
     * @param string $entidad_empleado El nombre de la entidad de empleado.
     *
     * @return string|array Devuelve una cadena SQL con los campos de la plaza, empleado y pago de corte.
     *                      Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function campos_corte_base(string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }
        $campos_emp_sql = $this->campos_emp_sql($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al integrar $campos_emp', $campos_emp_sql);
        }

        $campos_pc_sql = $this->campos_pc_sql();
        if(error::$en_error){
            return (new error())->error('Error al integrar $campos_pc_sql', $campos_pc_sql);
        }

        $campos_plaza = $this->campos_plaza();
        if(error::$en_error){
            return (new error())->error('Error al integrar $campos_plaza', $campos_plaza);
        }

        return "$campos_plaza->id, $campos_plaza->descripcion, $campos_emp_sql, $campos_pc_sql";

    }

    /**
     * EM3
     * Genera y estructura los campos necesarios para una consulta de pagos por corte.
     *
     * Esta función valida los datos de entrada, inicializa los campos requeridos, genera parámetros para subconsultas
     * relacionadas con pagos por corte, y agrega subconsultas específicas al conjunto de campos.
     *
     * @param string $campo_folio El nombre del campo que representa el folio en la entidad "contrato". No debe estar vacío.
     * @param string $campo_monto_pago El nombre del campo que representa el monto del pago en la entidad "pago". No debe estar vacío.
     * @param string $campo_serie El nombre del campo que representa la serie en la entidad "contrato". No debe estar vacío.
     * @param string $entidad_empleado El nombre de la entidad o tabla relacionada con empleados. No debe estar vacío.
     *
     * @return stdClass|array Devuelve un objeto con los campos estructurados, incluyendo subconsultas específicas.
     *                        En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar campos para consulta de pagos por corte
     * $campo_folio = 'folio';
     * $campo_monto_pago = 'monto';
     * $campo_serie = 'serie';
     * $entidad_empleado = 'empleado';
     * $resultado = $this->campos_pago_por_corte($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [pago_id] => stdClass Object (
     * //         [entidad] => 'pago'
     * //         [name_campo] => 'id'
     * //     ),
     * //     [serie_codigo] => stdClass Object (
     * //         [entidad] => 'serie'
     * //         [name_campo] => 'codigo'
     * //         [sql] => '(SELECT serie.codigo FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie_id = serie.id WHERE rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.serie) AS serie_codigo'
     * //     ),
     * //     ...
     * // )
     *
     * // Ejemplo 2: Error en validación de datos
     * $campo_folio = '';
     * $resultado = $this->campos_pago_por_corte($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * // Resultado:
     * // ['error' => 'Error al validar datos', 'data' => [...]]
     * ```
     */
    private function campos_pago_por_corte(
        string $campo_folio,
        string $campo_monto_pago,
        string $campo_serie,
        string $entidad_empleado
    ) {
        // Validar los datos de entrada
        $valida = $this->valida_data_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Inicializar los campos base
        $campos = $this->init_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al maquetar $campos', $campos);
        }

        // Generar parámetros para la subconsulta de pago por corte
        $params = $this->params_sq_pago_corte($campo_serie);
        if (error::$en_error) {
            return (new error())->error('Error al maquetar $params', $params);
        }

        // Agregar la subconsulta de serie al conjunto de campos
        $campos = $this->serie_codigo_sq($campos, $params);
        if (error::$en_error) {
            return (new error())->error('Error al maquetar $campos', $campos);
        }

        // Retornar el objeto con los campos generados
        return $campos;
    }


    /**
     * EM3
     * Genera una lista estructurada de campos SQL para consultas de pagos por corte.
     *
     * Esta función valida los datos de entrada, construye los campos necesarios para la consulta de pagos por corte,
     * y los convierte a un formato estructurado que puede ser utilizado en consultas SQL dinámicas.
     *
     * @param string $campo_folio El nombre del campo que representa el folio en la entidad "contrato". No debe estar vacío.
     * @param string $campo_monto_pago El nombre del campo que representa el monto del pago en la entidad "pago". No debe estar vacío.
     * @param string $campo_serie El nombre del campo que representa la serie en la entidad "contrato". No debe estar vacío.
     * @param string $entidad_empleado El nombre de la entidad o tabla relacionada con empleados. No debe estar vacío.
     *
     * @return stdClass|array Devuelve un objeto con los campos SQL estructurados para la consulta. En caso de error,
     *                        devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar campos SQL estructurados para pagos por corte
     * $campo_folio = 'folio';
     * $campo_monto_pago = 'monto';
     * $campo_serie = 'serie';
     * $entidad_empleado = 'empleado';
     * $resultado = $this->campos_pagos_por_corte_rs($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [pago_id] => 'pago.id AS pago_id',
     * //     [serie_codigo] => '(SELECT serie.codigo FROM rel_serie LEFT JOIN serie ON rel_serie_id = serie.id WHERE ...) AS serie_codigo',
     * //     ...
     * // )
     *
     * // Ejemplo 2: Error en validación de datos
     * $campo_folio = '';
     * $resultado = $this->campos_pagos_por_corte_rs($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * // Resultado:
     * // ['error' => 'Error al validar datos', 'data' => [...]]
     * ```
     */
    private function campos_pagos_por_corte_rs(
        string $campo_folio,
        string $campo_monto_pago,
        string $campo_serie,
        string $entidad_empleado
    ) {
        // Valida los datos de entrada
        $valida = $this->valida_data_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Construye los campos requeridos para la consulta
        $campos = $this->campos_pago_por_corte($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campos', $campos);
        }

        // Genera los campos SQL en formato estructurado
        $campos_rs = (new sql())->campos_sql($campos);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campos_rs', $campos_rs);
        }

        // Retorna los campos SQL estructurados
        return $campos_rs;
    }



    /**
     * TRASLADADO
     * Genera una cadena SQL con los campos del pago de corte, formateados para ser usados en una consulta SQL.
     *
     * La función obtiene los campos del pago de corte usando `campos_pago_corte()` y luego los formatea en una cadena
     * SQL, separando cada campo por comas. Si ocurre un error al obtener los campos, devuelve un array con los detalles del fallo.
     *
     * @return string|array Devuelve una cadena SQL con los campos del pago de corte (id, fecha, montos y validado).
     *                      Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function campos_pc_sql()
    {
        $campos_pc = $this->campos_pago_corte();
        if(error::$en_error){
            return (new error())->error('Error al integrar $campos_pc', $campos_pc);
        }

        $campos_pc_sql = "$campos_pc->id, $campos_pc->fecha, $campos_pc->monto_total,$campos_pc->monto_depositado,";
        $campos_pc_sql .= "$campos_pc->monto_por_depositar, $campos_pc->validado";

        return $campos_pc_sql;

    }

    /**
     * TRASLADADO
     * Genera un objeto con los campos relacionados a la entidad `plaza` en formato SQL.
     *
     * La función crea un objeto `stdClass` con los campos clave de la entidad `plaza`, incluyendo el ID y la descripción,
     * y asigna a cada campo un alias en formato SQL.
     *
     * @return stdClass Devuelve un objeto con los campos de la entidad `plaza` en formato SQL.
     */
    private function campos_plaza(): stdClass
    {
        $plaza_id_cmp = "plaza.id AS plaza_id";
        $plaza_descripcion_cmp = "plaza.descripcion AS plaza_descripcion";

        $campos = new stdClass();
        $campos->id = $plaza_id_cmp;
        $campos->descripcion = $plaza_descripcion_cmp;

        return$campos;

    }

    /**
     * EM3
     * Genera una cadena de campos SQL separados por comas para una consulta de pagos por corte.
     *
     * Esta función valida los datos de entrada, estructura los campos necesarios para la consulta de pagos por corte,
     * y convierte los campos estructurados en una cadena SQL separada por comas.
     *
     * @param string $campo_folio El nombre del campo que representa el folio en la entidad "contrato". No debe estar vacío.
     * @param string $campo_monto_pago El nombre del campo que representa el monto del pago en la entidad "pago". No debe estar vacío.
     * @param string $campo_serie El nombre del campo que representa la serie en la entidad "contrato". No debe estar vacío.
     * @param string $entidad_empleado El nombre de la entidad o tabla relacionada con empleados. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena con los campos SQL separados por comas. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar campos SQL válidos para pagos por corte
     * $campo_folio = 'folio';
     * $campo_monto_pago = 'monto';
     * $campo_serie = 'serie';
     * $entidad_empleado = 'empleado';
     * $resultado = $this->campos_sql_pagos_por_corte($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * echo $resultado;
     * // Resultado:
     * // pago.id AS pago_id, contrato.folio AS contrato_folio, (SELECT serie.codigo FROM ...) AS serie_codigo
     *
     * // Ejemplo 2: Error en validación de datos
     * $campo_folio = '';
     * $resultado = $this->campos_sql_pagos_por_corte($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * // Resultado:
     * // ['error' => 'Error al validar datos', 'data' => [...]]
     * ```
     */
    private function campos_sql_pagos_por_corte(
        string $campo_folio,
        string $campo_monto_pago,
        string $campo_serie,
        string $entidad_empleado
    ) {
        // Valida los datos de entrada
        $valida = $this->valida_data_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Obtiene los campos estructurados
        $campos_rs = $this->campos_pagos_por_corte_rs($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campos_rs', $campos_rs);
        }

        // Convierte los campos estructurados a una cadena separada por comas
        $campos_sql = (new sql())->campos_sql_coma_bruto($campos_rs);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campos_sql', $campos_sql);
        }

        // Retorna la cadena SQL de campos separados por comas
        return $campos_sql;
    }



    /**
     * EM3
     * Genera un objeto con configuraciones de condiciones `WHERE` para una subconsulta de pago por corte.
     *
     * Esta función valida el campo de serie, luego crea un objeto con las configuraciones de las condiciones `WHERE`
     * necesarias para filtrar pagos por corte en una subconsulta SQL. Cada condición incluye el nombre del campo,
     * el valor, y si el valor es un campo (`value_es_campo`).
     *
     * @param string $campo_serie El nombre del campo que representa la serie en la entidad "contrato". No debe estar vacío.
     *
     * @return stdClass|array Devuelve un objeto con las configuraciones `WHERE` generadas. En caso de error,
     *                        devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar configuraciones WHERE válidas
     * $campo_serie = 'serie';
     * $resultado = $this->campos_where_sq_pago_corte($campo_serie);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [cuenta] => stdClass Object (
     * //         [campo] => 'rel_serie.cuenta_empresa_id'
     * //         [value] => 'cuenta_empresa.id'
     * //         [value_es_campo] => true
     * //     ),
     * //     [codigo] => stdClass Object (
     * //         [campo] => 'serie.codigo'
     * //         [value] => 'contrato.serie'
     * //         [value_es_campo] => true
     * //     )
     * // )
     *
     * // Ejemplo 2: Campo de serie vacío (error)
     * $campo_serie = '';
     * $resultado = $this->campos_where_sq_pago_corte($campo_serie);
     * // Resultado:
     * // ['error' => 'Error $campo_serie esta vacio', 'data' => '']
     * ```
     */
    private function campos_where_sq_pago_corte(string $campo_serie)
    {
        // Validar que el campo de serie no esté vacío
        $campo_serie = trim($campo_serie);
        if ($campo_serie === '') {
            return (new error())->error('Error $campo_serie esta vacio', $campo_serie);
        }

        // Inicializar el objeto para las condiciones WHERE
        $campos_where = new stdClass();

        // Configuración para la cuenta
        $campos_where->cuenta = new stdClass();
        $campos_where->cuenta->campo = 'rel_serie.cuenta_empresa_id';
        $campos_where->cuenta->value = 'cuenta_empresa.id';
        $campos_where->cuenta->value_es_campo = true;

        // Configuración para el código de la serie
        $campos_where->codigo = new stdClass();
        $campos_where->codigo->campo = 'serie.codigo';
        $campos_where->codigo->value = 'contrato.' . $campo_serie;
        $campos_where->codigo->value_es_campo = true;

        // Retornar las configuraciones WHERE generadas
        return $campos_where;
    }


    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener los cortes de pago de empleados en plazas dentro de un rango de fechas.
     *
     * La función valida que los parámetros proporcionados no estén vacíos y luego genera una consulta SQL
     * que obtiene los cortes de pago para un conjunto de empleados en plazas específicas dentro de un rango de fechas.
     * Integra las cláusulas `IN` para filtrar por los IDs de los empleados y las plazas.
     * Si ocurre algún error durante el proceso, devuelve un array con los detalles del fallo.
     *
     * @param array $empleados_id Un array de IDs de los empleados a incluir en la consulta.
     * @param string $entidad_empleado El nombre de la entidad de empleado.
     * @param string $fecha_final La fecha final del rango de búsqueda.
     * @param string $fecha_inicial La fecha inicial del rango de búsqueda.
     * @param array $plazas_id Un array de IDs de las plazas a incluir en la consulta.
     *
     * @return string|array Devuelve la consulta SQL generada. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    final public function cortes(
        array $empleados_id, string $entidad_empleado, string $fecha_final, string $fecha_inicial, array $plazas_id)
    {
        $valida = (new _reporte_fecha())->valida_fechas($entidad_empleado,$fecha_final,$fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al validar datos', $valida);
        }

        $elementos = $this->elementos_base_rpt($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial, $plazas_id);
        if(error::$en_error){
            return (new error())->error('Error al integrar $elementos', $elementos);
        }

        $from_pago_corte = "FROM pago_corte AS pago_corte";

        $tb_emp = "$entidad_empleado AS $entidad_empleado";
        $tb_plaza = "plaza AS plaza";

        $on_pc = $elementos->keys_emp->empleado_id_filtro." = pago_corte.".$elementos->keys_emp->empleado_id;

        $on_plaza = "plaza.id = pago_corte.plaza_id";

        $bw_fechas = "'$fecha_inicial' AND '$fecha_final'";
        $wh_fechas = "pago_corte.fecha BETWEEN $bw_fechas";

        $where = "$wh_fechas ".$elementos->ins->plaza." ".$elementos->ins->empleado;

        $rels = "LEFT JOIN $tb_emp ON $on_pc LEFT JOIN $tb_plaza ON $on_plaza";

        return /** @lang MYSQL */ "SELECT $elementos->campos_corte $from_pago_corte $rels WHERE $where";

    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener los cortes de pago que no han sido validados.
     *
     * Esta función construye una consulta SQL que selecciona los cortes de pago en los que
     * los montos total, depositado y por depositar son 0, y el estado de validación es "inactivo".
     * La consulta está limitada por el parámetro `$limit`, que define el número máximo de registros
     * que se devolverán. Si el límite es menor que 0, se devuelve un error.
     *
     * @param int $limit [opcional] Número máximo de registros a devolver. El valor predeterminado es 10.
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada si el proceso es exitoso,
     *                      o un array de error si el límite es menor que 0.
     */
    final public function cortes_sin_validar(int $limit = 10)
    {
        if($limit < 0){
            return (new error())->error('Error limit debe ser mayor a 0', $limit);
        }
        $pago_corte_id = "pago_corte.id AS id";
        $monto_total = "pago_corte.monto_total";
        $monto_depositado = "pago_corte.monto_depositado";
        $monto_por_depositar = "pago_corte.monto_por_depositar";
        $validado = "pago_corte.validado";

        $wh_monto_total = "$monto_total = 0";
        $wh_monto_depositado = "$monto_depositado = 0";
        $wh_monto_por_depositar = "$monto_por_depositar = 0";
        $wh_validado = "$validado = 'inactivo'";

        $where = "$wh_monto_total AND $wh_monto_depositado AND $wh_monto_por_depositar AND $wh_validado";

        return /** @lang MYSQL */ "SELECT $pago_corte_id FROM pago_corte WHERE $where ORDER BY id DESC LIMIT $limit";

    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener los depósitos realizados en un rango de fechas, filtrados por empleados y plazas.
     *
     * Esta función valida las fechas proporcionadas, genera los elementos base para el reporte (como campos, claves y filtros),
     * y luego construye una consulta SQL que incluye las relaciones necesarias entre las tablas `pago_corte`, `deposito_aportaciones`,
     * `cuenta_empresa`, `empresa`, `banco`, `serie`, y `plaza`, además de la entidad de empleados.
     *
     * @param array $empleados_id       Lista de IDs de empleados para filtrar el reporte.
     * @param string $entidad_empleado  Nombre de la entidad de empleado.
     * @param string $fecha_final       Fecha final del reporte en formato 'Y-m-d'.
     * @param string $fecha_inicial     Fecha inicial del reporte en formato 'Y-m-d'.
     * @param array $plazas_id          Lista de IDs de plazas para filtrar el reporte.
     *
     * @return string|array Devuelve una cadena SQL para obtener los depósitos. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    final public function depositos(
        array $empleados_id, string $entidad_empleado, string $fecha_final, string $fecha_inicial, array $plazas_id)
    {

        $valida = (new _reporte_fecha())->valida_fechas($entidad_empleado,$fecha_final,$fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al validar datos', $valida);
        }

        $elementos = $this->elementos_base_rpt($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial, $plazas_id);
        if(error::$en_error){
            return (new error())->error('Error al integrar $elementos', $elementos);
        }

        $from_pago_corte = "pago_corte AS pago_corte";
        $tb_emp = "$entidad_empleado AS $entidad_empleado";

        $on_pc = $elementos->keys_emp->empleado_id_filtro ."= pago_corte.".$elementos->keys_emp->empleado_id;

        $campos_cuenta_empresa = "cuenta_empresa.id AS cuenta_empresa_id, cuenta_empresa.cuenta AS cuenta,";
        $campos_cuenta_empresa .= "cuenta_empresa.alias AS cuenta_empresa_alias";

        $campos_serie = "serie.codigo AS serie_codigo, serie.id AS serie_id";

        $campos_dep_aport = "deposito_aportaciones.id AS deposito_aportaciones_id, ";
        $campos_dep_aport .= "deposito_aportaciones.monto_depositado AS deposito_aportaciones_monto_depositado,";
        $campos_dep_aport .= "deposito_aportaciones.referencia AS deposito_aportaciones_referencia,";
        $campos_dep_aport .= "deposito_aportaciones.fecha AS deposito_aportaciones_fecha";

        $campos_sql = "$elementos->campos_corte, $campos_cuenta_empresa, empresa.razon_social AS empresa_razon_social,";
        $campos_sql .= "$campos_dep_aport, $campos_serie";

        $bw_fechas = "'$fecha_inicial' AND '$fecha_final'";
        $wh_fechas = "pago_corte.fecha BETWEEN $bw_fechas";
        $where = "$wh_fechas ".$elementos->ins->plaza." ".$elementos->ins->empleado;

        $lf_cuenta_emp = "cuenta_empresa AS cuenta_empresa";
        $lf_empresa = "empresa AS empresa";
        $lf_banco = "banco AS banco";
        $lf_serie = "serie AS serie";
        $lf_plaza = "plaza AS plaza";

        $on_pcid = "pago_corte.id = deposito_aportaciones.pago_corte_id";
        $on_ceid = "cuenta_empresa.id = deposito_aportaciones.cuenta_empresa_id";
        $on_empid = "empresa.id = cuenta_empresa_id";
        $on_banid = "banco.id = cuenta_empresa.banco_id";
        $on_serid = "serie.id = deposito_aportaciones.serie_id";
        $on_plid = "plaza.id = cuenta_empresa.plaza_id";

        $lf_pc_sql = "LEFT JOIN $from_pago_corte ON $on_pcid";
        $lf_ce_sql = "LEFT JOIN $lf_cuenta_emp ON $on_ceid";
        $lf_emp_sql = "LEFT JOIN $lf_empresa ON $on_empid";
        $lf_ban_sql = "LEFT JOIN $lf_banco ON $on_banid";
        $lf_ser_sql = "LEFT JOIN $lf_serie ON $on_serid";
        $lf_pla_sql = "LEFT JOIN $lf_plaza ON $on_plid";
        $lf_tb_sql = "LEFT JOIN $tb_emp ON $on_pc";

        $tb_dep = "deposito_aportaciones AS deposito_aportaciones";

        $joins = "$tb_dep $lf_pc_sql $lf_ce_sql $lf_emp_sql $lf_ban_sql $lf_ser_sql $lf_pla_sql $lf_tb_sql";


        return /** @lang MYSQL */ "SELECT $campos_sql FROM $joins WHERE $where";
    }

    /**
     * TRASLADADO
     * Genera los elementos base para un reporte, incluyendo los campos de corte, las claves de empleado y los filtros SQL.
     *
     * La función valida las fechas proporcionadas, luego genera los elementos base necesarios para el reporte:
     * los filtros SQL (utilizando empleados y plazas), los campos SQL de corte, y las claves de empleado.
     * Si ocurre algún error durante el proceso, devuelve un array con los detalles del fallo.
     *
     * @param array $empleados_id       Lista de IDs de empleados a incluir en el reporte.
     * @param string $entidad_empleado  Nombre de la entidad de empleado.
     * @param string $fecha_final       Fecha final del reporte.
     * @param string $fecha_inicial     Fecha inicial del reporte.
     * @param array $plazas_id          Lista de IDs de plazas a incluir en el reporte.
     *
     * @return stdClass|array Devuelve un objeto con los elementos necesarios para el reporte (filtros, campos y claves).
     *                        Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function elementos_base_rpt(
        array $empleados_id, string $entidad_empleado, string $fecha_final, string $fecha_inicial, array $plazas_id)
    {
        $valida = (new _reporte_fecha())->valida_fechas($entidad_empleado,$fecha_final,$fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al validar datos', $valida);
        }

        $ins = $this->ins_rpt_pagos($empleados_id,$entidad_empleado,$plazas_id);
        if(error::$en_error){
            return (new error())->error('Error al integrar in sql', $ins);
        }

        $campos_corte = $this->campos_corte_base($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al integrar $campos_corte', $campos_corte);
        }

        $keys_emp = $this->keys_empleado($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al integrar $keys_emp', $keys_emp);
        }

        $elementos = new stdClass();
        $elementos->ins = $ins;
        $elementos->campos_corte = $campos_corte;
        $elementos->keys_emp = $keys_emp;

        return $elementos;

    }

    /**
     * TRASLADADO
     * Construye un arreglo de uniones (joins) entre una entidad base y un conjunto de entidades secundarias.
     *
     * Esta función toma una entidad base y un conjunto de entidades secundarias (lefts),
     * y construye un arreglo de uniones (`joins_base`) donde cada entidad secundaria
     * se une a la entidad base. Si la entidad base o alguna de las entidades secundarias
     * están vacías, se devuelve un objeto de error.
     *
     * @param string $entidad_base La entidad base con la cual se realizarán las uniones.
     * @param array $joins_base Arreglo que almacenará las uniones creadas.
     * @param array $lefts Arreglo de entidades secundarias que se unirán con la entidad base.
     *
     * @return array|error Retorna el arreglo `joins_base` actualizado con las nuevas uniones,
     *                     o un objeto de error si alguna de las entidades está vacía.
     */
    private function joins_base_pago_corte(string $entidad_base, array $joins_base, array $lefts): array
    {
        $entidad_base = trim($entidad_base);
        if($entidad_base === ''){
            return (new error())->error('Error $entidad_base esta vacia',$entidad_base);
        }
        foreach ($lefts as $left){
            $left = trim($left);
            if($left === ''){
                return (new error())->error('Error $left esta vacia',$left);
            }
            $join['left'] = $left;
            $join['right'] = $entidad_base;
            $joins_base[] = $join;
        }

        return $joins_base;

    }

    /**
     * TRASLADADO
     * Genera un array de joins para una consulta SQL relacionada con pagos por corte, utilizando la entidad del empleado.
     *
     * Esta función valida que el nombre de la entidad del empleado no esté vacío. Luego, crea una lista de entidades
     * para los `LEFT JOINs` necesarios en la consulta. Posteriormente, utiliza la función `joins_base_pago_corte`
     * para generar los joins básicos, y añade las relaciones adicionales entre `deposito_pago`, `deposito_aportaciones`
     * y `cuenta_empresa`. Si alguna validación o generación de joins falla, se devuelve un array con los detalles del error.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     *
     * @return array Devuelve un array con los `LEFT JOINs` generados si el proceso es exitoso,
     *                     o un array de error si ocurre algún problema durante la validación o generación de los joins.
     */
    private function joins_base_pago_corte_left(string $entidad_empleado): array
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $lefts = array();
        $lefts[] = 'contrato';
        $lefts[] = 'pago_corte';
        $lefts[] = $entidad_empleado;
        $lefts[] = 'plaza';

        $joins_base = array();
        $joins_base = $this->joins_base_pago_corte('pago',$joins_base,$lefts);
        if(error::$en_error){
            return (new error())->error('Error al obtener $joins_base', $joins_base);
        }

        $joins_base[4]['left'] = 'deposito_pago';
        $joins_base[4]['right'] = 'pago';
        $joins_base[4]['key_left'] = 'pago_id';
        $joins_base[4]['key_right'] = 'id';

        $joins_base[5]['left'] = 'deposito_aportaciones';
        $joins_base[5]['right'] = 'deposito_pago';

        $joins_base[6]['left'] = 'cuenta_empresa';
        $joins_base[6]['right'] = 'deposito_aportaciones';


        return $joins_base;
    }


    /**
     * EM3
     * Inicializa un objeto con configuraciones detalladas de campos y sus entidades correspondientes.
     *
     * Esta función valida los datos de entrada, genera configuraciones base para los campos, y los estructura
     * en un objeto estándar (`stdClass`) utilizando el método `campo_base_obj`.
     *
     * @param string $campo_folio El nombre del campo que representa el folio en la entidad "contrato". No debe estar vacío.
     * @param string $campo_monto_pago El nombre del campo que representa el monto del pago en la entidad "pago". No debe estar vacío.
     * @param string $campo_serie El nombre del campo que representa la serie en la entidad "contrato". No debe estar vacío.
     * @param string $entidad_empleado El nombre de la entidad o tabla relacionada con empleados. No debe estar vacío.
     *
     * @return stdClass|array Devuelve un objeto estándar con las configuraciones de los campos inicializados. Cada propiedad
     *                        del objeto representa un campo y contiene un objeto con `entidad` y `name_campo`. En caso de error,
     *                        devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Inicializar configuraciones de campos
     * $campo_folio = 'folio';
     * $campo_monto_pago = 'monto';
     * $campo_serie = 'serie';
     * $entidad_empleado = 'empleado';
     * $resultado = $this->init_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [pago_id] => stdClass Object (
     * //         [entidad] => 'pago'
     * //         [name_campo] => 'id'
     * //     ),
     * //     [pago_monto] => stdClass Object (
     * //         [entidad] => 'pago'
     * //         [name_campo] => 'monto'
     * //     ),
     * //     ...
     * // )
     *
     * // Ejemplo 2: Error en validación de datos
     * $campo_folio = '';
     * $resultado = $this->init_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * // Resultado:
     * // ['error' => 'Error al validar datos', 'data' => [...]]
     * ```
     */
    private function init_campos(
        string $campo_folio,
        string $campo_monto_pago,
        string $campo_serie,
        string $entidad_empleado
    ) {
        // Validar los datos de entrada
        $valida = $this->valida_data_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Inicializar el objeto para los campos
        $campos = new stdClass();

        // Generar configuraciones base para los campos
        $campos_base = $this->campos_base($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al maquetar $campos_base', $campos_base);
        }

        // Convertir las configuraciones base en un objeto detallado
        foreach ($campos_base as $campo) {
            $campos = (new sql())->campo_base_obj($campos, $campo['entidad'], $campo['campo']);
            if (error::$en_error) {
                return (new error())->error('Error al maquetar $campos', $campos);
            }
        }

        // Retornar el objeto de configuraciones inicializadas
        return $campos;
    }


    /**
     * TRASLADADO
     * Genera las consultas SQL necesarias para integrar reportes de pagos filtrados por empleados y plazas.
     *
     * La función valida que el nombre de la entidad de empleado no esté vacío. Luego, genera las cláusulas
     * SQL `IN` para los IDs de empleados y plazas, utilizando las funciones `in_sql_completo` de la clase `sql`.
     * Devuelve un objeto con las consultas SQL generadas.
     *
     * @param array $empleados_id Un array de IDs de los empleados para los que se generará el reporte.
     * @param string $entidad_empleado El nombre de la entidad de empleado.
     * @param array $plazas_id Un array de IDs de las plazas para filtrar los pagos.
     *
     * @return stdClass|array Devuelve un objeto con las consultas SQL generadas para empleados y plazas.
     *                        Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function ins_rpt_pagos(array $empleados_id, string $entidad_empleado, array $plazas_id)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }
        $in_plaza_sql = (new sql())->in_sql_completo('plaza.id',true,$plazas_id);
        if(error::$en_error){
            return (new error())->error('Error al integrar key $in_plaza_sql', $in_plaza_sql);
        }

        $in_empleado_sql = (new sql())->in_sql_completo($entidad_empleado.'.id',true,$empleados_id);
        if(error::$en_error){
            return (new error())->error('Error al integrar key $in_empleado_sql', $in_empleado_sql);
        }

        $ins = new stdClass();
        $ins->plaza = $in_plaza_sql;
        $ins->empleado = $in_empleado_sql;
        return $ins;

    }

    /**
     * TRASLADADO
     * Genera un conjunto de claves relacionadas con un empleado a partir de una entidad de empleado.
     *
     * La función toma el nombre de la entidad de empleado y genera varias claves asociadas, como el ID del empleado,
     * el nombre completo y una clave para filtrar el ID del empleado en consultas SQL. Si el nombre de la entidad está vacío,
     * se devuelve un error.
     *
     * @param string $entidad_empleado El nombre de la entidad de empleado.
     *
     * @return stdClass|array Devuelve un objeto con las claves generadas (empleado_id, nombre_completo, empleado_id_filtro).
     *                        Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function keys_empleado(string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }
        $key_empleado_id = $entidad_empleado.'_id';
        $key_empleado_nombre_completo = $entidad_empleado.'_nombre_completo';
        $key_empleado_id_fil = $entidad_empleado.'.id';

        $keys = new stdClass();
        $keys->empleado_id = $key_empleado_id;
        $keys->nombre_completo = $key_empleado_nombre_completo;
        $keys->empleado_id_filtro = $key_empleado_id_fil;

        return $keys;


    }


    /**
     * TRASLADADO
     * Genera las sentencias de `LEFT JOIN` en formato de texto para una consulta relacionada con pagos por corte.
     *
     * Esta función valida que el nombre de la entidad del empleado no esté vacío. Luego, llama a la función
     * `lfs_pagos` para obtener los joins necesarios y finalmente convierte esos joins en formato de texto
     * utilizando la función `left_joins_txt` de la clase `sql`. Si alguna validación o generación de joins
     * falla, se devuelve un array con los detalles del error.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     *
     * @return string|array Devuelve una cadena con las sentencias `LEFT JOIN` en formato de texto si el proceso es exitoso,
     *                      o un array de error si ocurre algún problema durante la validación o generación de los joins.
     */
    private function left_joins_txt(string $entidad_empleado){
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $lfs = $this->lfs_pagos($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al obtener $lf_contrato', $lfs);
        }

        $lf_joins = (new sql())->left_joins_txt($lfs);
        if(error::$en_error){
            return (new error())->error('Error al obtener $lf_joins', $lf_joins);
        }

        return $lf_joins;


    }

    /**
     * TRASLADADO
     * Genera los `LEFT JOINs` necesarios para una consulta relacionada con pagos por corte.
     *
     * Esta función valida que el nombre de la entidad del empleado no esté vacío. Luego, utiliza la función
     * `joins_base_pago_corte_left` para obtener los joins básicos necesarios para la consulta. Finalmente,
     * genera las sentencias `LEFT JOIN` correspondientes utilizando la función `rfs_lefts`. Si alguna validación
     * o generación de joins falla, se devuelve un array con los detalles del error.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     *
     * @return array Devuelve un array con las sentencias `LEFT JOIN` generadas si el proceso es exitoso,
     *                     o un array de error si ocurre algún problema durante la validación o generación de los joins.
     */
    private function lfs_pagos(string $entidad_empleado): array
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $joins_base = $this->joins_base_pago_corte_left($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al obtener $joins_base', $joins_base);
        }

        $lfs = (new sql())->rfs_lefts($joins_base);
        if(error::$en_error){
            return (new error())->error('Error al obtener $lf_contrato', $lfs);
        }
        return $lfs;

    }
    private string $name_entidad = 'pago_corte';


    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener pagos por corte.
     *
     * Este método valida los datos de entrada, prepara los parámetros necesarios para la consulta, y
     * construye la sentencia SQL base para obtener los pagos por corte.
     *
     * @param string $campo_canceled Campo que indica si el pago está cancelado.
     * @param string $campo_folio Campo que representa el folio del contrato.
     * @param string $campo_monto_pago Campo que representa el monto del pago.
     * @param string $campo_movto Campo que representa el movimiento.
     * @param string $campo_serie Campo que representa la serie del contrato.
     * @param string $entidad_empleado Nombre de la entidad relacionada con empleados.
     * @param string $order Orden en que se presentarán los resultados de la consulta.
     * @param int $pago_corte_id ID del corte de pago para filtrar los resultados.
     *
     * @return string|array Devuelve la consulta SQL generada como una cadena. En caso de error, devuelve un objeto `error`.
     *
     * @throws error Si:
     *               - Los datos de entrada no son válidos.
     *               - Ocurre un error al generar los parámetros de la consulta.
     *               - Falla la construcción de la consulta SQL.
     *
     * @example
     * ```php
     * $campo_canceled = 'pago.cancelado';
     * $campo_folio = 'contrato.folio';
     * $campo_monto_pago = 'pago.monto';
     * $campo_movto = 'pago.movimiento';
     * $campo_serie = 'contrato.serie';
     * $entidad_empleado = 'empleado';
     * $order = 'pago.fecha DESC';
     * $pago_corte_id = 123;
     *
     * $sql = $this->pagos_por_corte(
     *     $campo_canceled,
     *     $campo_folio,
     *     $campo_monto_pago,
     *     $campo_movto,
     *     $campo_serie,
     *     $entidad_empleado,
     *     $order,
     *     $pago_corte_id
     * );
     *
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al generar la consulta SQL.";
     * } else {
     *     echo $sql;
     *     // Salida esperada:
     *     // SELECT pago.* FROM pago LEFT JOIN ... WHERE pago.corte_id = 123 ...
     * }
     * ```
     */
    final public function pagos_por_corte(
        string $campo_canceled,
        string $campo_folio,
        string $campo_monto_pago,
        string $campo_movto,
        string $campo_serie,
        string $entidad_empleado,
        string $order,
        int $pago_corte_id
    ) {
        // Valida los datos relacionados con el corte de pago
        $valida = (new _valida())->valida_pago_corte($campo_monto_pago, $campo_movto, $pago_corte_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar pago corte', $valida);
        }

        // Valida los datos generales
        $valida = $this->valida_data_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Genera los parámetros para la consulta
        $params = $this->params_pago_por_corte(
            $campo_canceled,
            $campo_folio,
            $campo_monto_pago,
            $campo_movto,
            $campo_serie,
            $entidad_empleado,
            $pago_corte_id
        );
        if (error::$en_error) {
            return (new error())->error('Error al obtener $params', $params);
        }

        // Construye la consulta SQL base
        $sql = (new sql())->select_base_sql('pago', $order, $params);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $sql', $sql);
        }

        return $sql;
    }


    /**
     * TRASLADADO
     * Genera los parámetros necesarios para la consulta de pagos por corte.
     *
     * Este método valida los datos de entrada, prepara los filtros, genera los campos SQL necesarios,
     * y construye las relaciones `JOIN` para realizar una consulta sobre los pagos por corte.
     *
     * @param string $campo_canceled Nombre del campo que indica si el pago está cancelado.
     * @param string $campo_folio Nombre del campo que representa el folio.
     * @param string $campo_monto_pago Nombre del campo que representa el monto del pago.
     * @param string $campo_movto Nombre del campo que representa el movimiento.
     * @param string $campo_serie Nombre del campo que representa la serie.
     * @param string $entidad_empleado Nombre de la entidad relacionada con empleados.
     * @param int $pago_corte_id ID del corte de pago para filtrar los resultados.
     *
     * @return stdClass|array Devuelve un objeto con los parámetros de la consulta (`filtro`, `campos`, `joins`).
     *                        En caso de error, devuelve un objeto `error`.
     *
     * @throws error Si:
     *               - Los parámetros de entrada no son válidos.
     *               - Ocurre un error al validar los datos.
     *               - Falla la generación de filtros, campos, o relaciones `JOIN`.
     *
     * @example
     * ```php
     * $campo_canceled = 'pago.cancelado';
     * $campo_folio = 'contrato.folio';
     * $campo_monto_pago = 'pago.monto';
     * $campo_movto = 'pago.movimiento';
     * $campo_serie = 'contrato.serie';
     * $entidad_empleado = 'empleado';
     * $pago_corte_id = 123;
     *
     * $params = $this->params_pago_por_corte($campo_canceled, $campo_folio, $campo_monto_pago, $campo_movto, $campo_serie, $entidad_empleado, $pago_corte_id);
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al generar los parámetros.";
     * } else {
     *     print_r($params);
     *     // Salida esperada:
     *     // stdClass Object
     *     // (
     *     //     [filtro] => "pago.cancelado = 'NO' AND pago.monto > 0 ..."
     *     //     [campos] => "contrato.folio AS contrato_folio, pago.monto AS pago_monto, ..."
     *     //     [joins] => "LEFT JOIN empleado ON empleado.id = contrato.empleado_id ..."
     *     // )
     * }
     * ```
     */
    private function params_pago_por_corte(
        string $campo_canceled,
        string $campo_folio,
        string $campo_monto_pago,
        string $campo_movto,
        string $campo_serie,
        string $entidad_empleado,
        int $pago_corte_id
    ) {
        // Valida los datos del corte de pago
        $valida = (new _valida())->valida_pago_corte($campo_monto_pago, $campo_movto, $pago_corte_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar pago corte', $valida);
        }

        // Valida los datos generales
        $valida = $this->valida_data_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Genera los filtros para la consulta
        $pagos_por_corte_filtro = (new pago())->pagos_por_corte_filtro($campo_canceled, $campo_monto_pago,
            $campo_movto, $pago_corte_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener filtros de pago', $pagos_por_corte_filtro);
        }

        // Genera los campos SQL necesarios
        $campos_sql = $this->campos_sql_pagos_por_corte($campo_folio, $campo_monto_pago, $campo_serie,
            $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campos_sql', $campos_sql);
        }

        // Genera las relaciones LEFT JOIN
        $lf_joins = $this->left_joins_txt($entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $lf_joins', $lf_joins);
        }

        // Prepara los parámetros como un objeto
        $params = new stdClass();
        $params->filtro = $pagos_por_corte_filtro;
        $params->campos = $campos_sql;
        $params->joins = $lf_joins;

        return $params;
    }


    /**
     * EM3
     * Genera los parámetros necesarios para una subconsulta de pago por corte.
     *
     * Esta función valida el campo de serie, genera una cláusula `LEFT JOIN` y una cláusula `WHERE`,
     * y los encapsula en un objeto estándar (`stdClass`) que se puede usar para construir una subconsulta SQL.
     *
     * @param string $campo_serie El nombre del campo que representa la serie en la entidad "contrato". No debe estar vacío.
     *
     * @return stdClass|array Devuelve un objeto con los parámetros generados para la subconsulta. Incluye:
     *                        - `left_join`: La cláusula `LEFT JOIN` generada.
     *                        - `where`: La cláusula `WHERE` generada.
     *                        En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar parámetros válidos para la subconsulta
     * $campo_serie = 'serie';
     * $resultado = $this->params_sq_pago_corte($campo_serie);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [left_join] => 'rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie_id = serie.id'
     * //     [where] => 'rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.serie'
     * // )
     *
     * // Ejemplo 2: Campo de serie vacío (error)
     * $campo_serie = '';
     * $resultado = $this->params_sq_pago_corte($campo_serie);
     * // Resultado:
     * // ['error' => 'Error $campo_serie esta vacio', 'data' => '']
     * ```
     */
    private function params_sq_pago_corte(string $campo_serie)
    {
        // Validar que el campo de serie no esté vacío
        $campo_serie = trim($campo_serie);
        if ($campo_serie === '') {
            return (new error())->error('Error $campo_serie esta vacio', $campo_serie);
        }

        // Generar la cláusula LEFT JOIN
        $lj_serie = (new sql())->left_join_inicial('rel_serie', 'serie');
        if (error::$en_error) {
            return (new error())->error('Error al maquetar left join', $lj_serie);
        }

        // Generar la cláusula WHERE
        $where = $this->where_sq_pago_corte($campo_serie);
        if (error::$en_error) {
            return (new error())->error('Error al maquetar $where', $where);
        }

        // Crear el objeto de parámetros
        $params = new stdClass();
        $params->left_join = $lj_serie;
        $params->where = $where;

        // Retornar los parámetros generados
        return $params;
    }


    /**
     * EM3
     * Genera una subconsulta SQL para obtener el código de serie, y la agrega a un objeto de campos.
     *
     * Esta función valida los parámetros de entrada, construye una subconsulta SQL basada en los parámetros
     * `left_join` y `where` de `$params`, y agrega la subconsulta al objeto `$campos` bajo la clave `serie_codigo`.
     *
     * @param stdClass $campos Un objeto que contendrá los campos generados, incluyendo la subconsulta de serie.
     * @param stdClass $params Un objeto que contiene los parámetros necesarios para construir la subconsulta:
     *                         - `left_join` (string): La cláusula `LEFT JOIN` para la subconsulta.
     *                         - `where` (string): La cláusula `WHERE` para la subconsulta.
     *
     * @return stdClass|array Devuelve el objeto `$campos` con la subconsulta `serie_codigo` agregada. En caso de error,
     *                        devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar subconsulta de serie válida
     * $campos = new stdClass();
     * $params = new stdClass();
     * $params->left_join = 'rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie_id = serie.id';
     * $params->where = 'rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.serie';
     * $resultado = $this->serie_codigo_sq($campos, $params);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [serie_codigo] => stdClass Object (
     * //         [entidad] => 'serie'
     * //         [name_campo] => 'codigo'
     * //         [sql] => '(SELECT serie.codigo FROM rel_serie AS rel_serie LEFT JOIN serie AS serie ON rel_serie_id = serie.id WHERE rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.serie) AS serie_codigo'
     * //     )
     * // )
     *
     * // Ejemplo 2: Parámetro left_join vacío (error)
     * $campos = new stdClass();
     * $params = new stdClass();
     * $params->left_join = '';
     * $params->where = 'rel_serie.cuenta_empresa_id = cuenta_empresa.id';
     * $resultado = $this->serie_codigo_sq($campos, $params);
     * // Resultado:
     * // ['error' => 'Error $params->left_join esta vacio', 'data' => [...]]
     * ```
     */
    private function serie_codigo_sq(stdClass $campos, stdClass $params)
    {
        // Validar que el parámetro left_join exista y no esté vacío
        if (!isset($params->left_join)) {
            return (new error())->error('Error $params->left_join no existe', $params);
        }
        if (trim($params->left_join) === '') {
            return (new error())->error('Error $params->left_join esta vacio', $params);
        }

        // Validar que el parámetro where exista y no esté vacío
        if (!isset($params->where)) {
            return (new error())->error('Error $params->where no existe', $params);
        }
        if (trim($params->where) === '') {
            return (new error())->error('Error $params->where esta vacio', $params);
        }

        // Crear el campo serie_codigo
        $campos->serie_codigo = new stdClass();
        $campos->serie_codigo->entidad = 'serie';
        $campos->serie_codigo->name_campo = 'codigo';

        // Construir la subconsulta SQL
        $sql = /** @lang MYSQL */
            "SELECT serie.codigo FROM $params->left_join WHERE $params->where";
        $campos->serie_codigo->sql = "($sql) AS serie_codigo";

        // Retornar el objeto campos con la subconsulta agregada
        return $campos;
    }


    /**
     * TRASLADADO
     * Genera una consulta SQL para actualizar los montos de un corte de pago específico.
     *
     * Esta función valida que el ID del pago de corte sea mayor que 0, luego construye una
     * consulta SQL `UPDATE` para actualizar los campos `monto_depositado`, `monto_total`,
     * y `monto_por_depositar` en la tabla `pago_corte` para el registro especificado por
     * el ID del corte de pago. Si la validación o la construcción de la consulta falla, se
     * devuelve un array con los detalles del error.
     *
     * @param float $monto_depositado El nuevo valor para el campo `monto_depositado`.
     * @param float $monto_por_depositar El nuevo valor para el campo `monto_por_depositar`.
     * @param float $monto_total El nuevo valor para el campo `monto_total`.
     * @param int $pago_corte_id ID del pago de corte que se está actualizando.
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada si el proceso es exitoso,
     *                      o un array de error si alguna validación o construcción de la consulta falla.
     */
    final public function update_montos(float $monto_depositado, float $monto_por_depositar, float $monto_total,
                                        int $pago_corte_id)
    {
        if($pago_corte_id <= 0){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0',$pago_corte_id);
        }
        $monto_depositado_upd = "monto_depositado = $monto_depositado";
        $monto_total_upd = "monto_total = $monto_total";
        $monto_por_depositar_upd = "monto_por_depositar = $monto_por_depositar";

        $where_id = (new sql())->where_id($this->name_entidad,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al maquetar $where_id',$where_id);
        }

        $updates = "$monto_depositado_upd, $monto_total_upd, $monto_por_depositar_upd";

        return /** @lang MYSQL */ "UPDATE pago_corte SET $updates WHERE $where_id";

    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para marcar un corte de pago como validado.
     *
     * Esta función valida que el ID del corte de pago (`pago_corte_id`) sea mayor que 0. Luego, construye
     * una consulta SQL `UPDATE` para cambiar el estado del campo `validado` a 'activo' para el corte de
     * pago especificado. Si ocurre un error durante la validación o la construcción de la consulta,
     * se devuelve un array con los detalles del error.
     *
     * @param int $pago_corte_id ID del corte de pago que se va a actualizar.
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada si el proceso es exitoso,
     *                      o un array de error si alguna validación o construcción de la consulta falla.
     */
    final public function update_validado(int $pago_corte_id)
    {
        if($pago_corte_id <= 0){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0',$pago_corte_id);
        }
        $validado_upd = "validado = 'activo'";
        $updates = "$validado_upd";
        $where_id = (new sql())->where_id($this->name_entidad,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al maquetar $where_id',$where_id);
        }
        return /** @lang MYSQL */ "UPDATE pago_corte SET $updates WHERE $where_id";

    }


    /**
     * EM3
     * Valida que los campos requeridos para una operación no estén vacíos.
     *
     * Esta función verifica que los valores de los campos `$campo_folio`, `$campo_monto_pago`, `$campo_serie` y `$entidad_empleado`
     * no estén vacíos. Si alguno de ellos está vacío, retorna un error con detalles.
     *
     * @param string $campo_folio El nombre o valor del campo de folio. No debe estar vacío.
     * @param string $campo_monto_pago El nombre o valor del campo de monto de pago. No debe estar vacío.
     * @param string $campo_serie El nombre o valor del campo de serie. No debe estar vacío.
     * @param string $entidad_empleado El nombre de la entidad o tabla relacionada con el empleado. No debe estar vacío.
     *
     * @return bool|array Devuelve `true` si todos los campos son válidos. En caso de error, devuelve un array
     *                    con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Validación exitosa
     * $campo_folio = 'folio';
     * $campo_monto_pago = 'monto_pago';
     * $campo_serie = 'serie';
     * $entidad_empleado = 'empleados';
     * $resultado = $this->valida_data_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * echo $resultado ? 'Validación exitosa' : 'Error';
     * // Resultado:
     * // Validación exitosa
     *
     * // Ejemplo 2: Campo de folio vacío (error)
     * $campo_folio = '';
     * $campo_monto_pago = 'monto_pago';
     * $campo_serie = 'serie';
     * $entidad_empleado = 'empleados';
     * $resultado = $this->valida_data_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * // Resultado:
     * // ['error' => 'Error $campo_folio esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Campo de monto de pago vacío (error)
     * $campo_folio = 'folio';
     * $campo_monto_pago = '';
     * $campo_serie = 'serie';
     * $entidad_empleado = 'empleados';
     * $resultado = $this->valida_data_campos($campo_folio, $campo_monto_pago, $campo_serie, $entidad_empleado);
     * // Resultado:
     * // ['error' => 'Error $campo_monto_pago esta vacio', 'data' => '']
     * ```
     */
    final public function valida_data_campos(
        string $campo_folio, string $campo_monto_pago, string $campo_serie, string $entidad_empleado
    ) {
        // Validar que el campo de monto de pago no esté vacío
        $campo_monto_pago = trim($campo_monto_pago);
        if ($campo_monto_pago === '') {
            return (new error())->error('Error $campo_monto_pago esta vacio', $campo_monto_pago);
        }

        // Validar que la entidad de empleado no esté vacía
        $entidad_empleado = trim($entidad_empleado);
        if ($entidad_empleado === '') {
            return (new error())->error('Error $entidad_empleado esta vacio', $entidad_empleado);
        }

        // Validar que el campo de serie no esté vacío
        $campo_serie = trim($campo_serie);
        if ($campo_serie === '') {
            return (new error())->error('Error $campo_serie esta vacio', $campo_serie);
        }

        // Validar que el campo de folio no esté vacío
        $campo_folio = trim($campo_folio);
        if ($campo_folio === '') {
            return (new error())->error('Error $campo_folio esta vacio', $campo_folio);
        }

        // Si todas las validaciones pasan, retornar true
        return true;
    }


    /**
     * EM3
     * Genera una cláusula `WHERE` para una subconsulta de pago por corte.
     *
     * Esta función valida el campo de serie, genera las configuraciones `WHERE` necesarias utilizando la función
     * `campos_where_sq_pago_corte`, y construye la cláusula `WHERE` final a partir de estas configuraciones.
     *
     * @param string $campo_serie El nombre del campo que representa la serie en la entidad "contrato". No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `WHERE` generada. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula WHERE válida
     * $campo_serie = 'serie';
     * $resultado = $this->where_sq_pago_corte($campo_serie);
     * echo $resultado;
     * // Resultado:
     * // rel_serie.cuenta_empresa_id = cuenta_empresa.id AND serie.codigo = contrato.serie
     *
     * // Ejemplo 2: Campo de serie vacío (error)
     * $campo_serie = '';
     * $resultado = $this->where_sq_pago_corte($campo_serie);
     * // Resultado:
     * // ['error' => 'Error $campo_serie esta vacio', 'data' => '']
     * ```
     */
    private function where_sq_pago_corte(string $campo_serie)
    {
        // Validar que el campo de serie no esté vacío
        $campo_serie = trim($campo_serie);
        if ($campo_serie === '') {
            return (new error())->error('Error $campo_serie esta vacio', $campo_serie);
        }

        // Generar configuraciones WHERE
        $campos_where = $this->campos_where_sq_pago_corte($campo_serie);
        if (error::$en_error) {
            return (new error())->error('Error al maquetar $campos_where', $campos_where);
        }

        // Construir la cláusula WHERE final
        $where = (new sql())->statement_where($campos_where);
        if (error::$en_error) {
            return (new error())->error('Error al maquetar $where', $where);
        }

        // Retornar la cláusula WHERE
        return $where;
    }



}
