<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;

class periodo_comercial{

    /**
     * FIN
     * Obtiene el periodo comercial actual basado en la fecha de hoy.
     *
     * Esta función valida que la fecha proporcionada no esté vacía y construye una consulta SQL
     * para obtener el periodo comercial actual en el que la fecha proporcionada cae entre las fechas
     * inicial y final del periodo comercial.
     *
     * @param string $hoy Fecha actual en formato 'YYYY-MM-DD'.
     *
     * @return string|array Devuelve una cadena con la consulta SQL si la validación es exitosa.
     *                      En caso de error, devuelve un array con el mensaje de error correspondiente.
     */
    final public function periodo_comercial_hoy(string $hoy)
    {
        $hoy = trim($hoy);
        if($hoy === ''){
            return (new error())->error('Error hoy esta vacia', $hoy);
        }
        return /** @lang MYSQL */ "SELECT * FROM periodo_comercial WHERE '$hoy' BETWEEN fecha_inicial AND fecha_final";

    }
}
