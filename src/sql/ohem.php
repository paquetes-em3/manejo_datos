<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;
use PDO;

class ohem{


    private string $name_entidad = 'ohem';
    final public function campos_sql(PDO $link)
    {
        $campos = (new \desarrollo_em3\manejo_datos\data\entidades\ohem())->campos($link);
        if(error::$en_error){
            return (new error())->error('Error al obtener $campos',$campos);
        }

        $sql = (new sql())->campos_sql_coma($campos, $this->name_entidad);
        if(error::$en_error){
            return (new error())->error('Error al generar $campos',$sql);
        }

        return $sql;


    }

    final public function sql_extra_esquema_guardadito(string $fecha): string
    {
        return " AND '$fecha' BETWEEN esquema_guardadito.fecha_inicio AND esquema_guardadito.fecha_fin";

    }


}
