<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;
use PDO;
use stdClass;

class costo_estimado_det{




    final public function contratos_cancelados(string $where)
    {

        $where = trim($where);
        if($where === ''){
            return (new error())->error('Error where esta vacio', $where);
        }

        $campos['contrato'] = array('id','DocDate','CardName','DocTotal','PaidToDate','U_SeCont','U_FolioCont',
            'U_FeContCancel','U_FechaUltimoPago','U_StatusCob');

        $campos['empresa'] = array('razon_social','rfc');

        $campos['factura'] = array('serie','folio','fecha','saldo','subtotal','total','uuid');

        $campos['nota_credito_sat'] = array('id','importe','iva_trasladado','total','fecha','serie','folio','uuid');

        $campos['producto'] = array('id','descripcion');
        $campos['plaza'] = array('id','descripcion');


        $campos_sql = (new sql())->campos_alias($campos);
        if (error::$en_error) {
            return (new error())->error('Error al generar $campos_sql', $campos_sql);
        }


        $lefts[] = "empresa ON empresa.id = contrato.empresa_id";
        $lefts[] = "factura_contrato ON factura_contrato.contrato_id = contrato.id";
        $lefts[] = "factura ON factura.id = factura_contrato.factura_id";
        $lefts[] = "nota_cred_contrato ON nota_cred_contrato.contrato_id = contrato.id";
        $lefts[] = "nota_credito_sat ON nota_credito_sat.id = nota_cred_contrato.nota_credito_sat_id";
        $lefts[] = "plaza ON plaza.id = contrato.plaza_id";
        $lefts[] = "producto ON producto.id = contrato.producto_id";

        $lefts_sql = '';
        foreach ($lefts as $left) {
            $lefts_sql.=" LEFT JOIN $left ";
        }

        $from = "FROM contrato AS contrato $lefts_sql";

        return /** @lang MYSQL */ "SELECT $campos_sql $from WHERE $where GROUP BY contrato.id";

    }

    final public function contratos_realizados(stdClass  $fechas, string $where)
    {
        $where = trim($where);
        if($where === ''){
            return (new error())->error('Error where esta vacio', $where);
        }

        $campos['contrato'] = array('id','DocDate','CardName','DocTotal','PaidToDate','U_SeCont','U_FolioCont',
            'U_FeContCancel','U_FechaUltimoPago','U_StatusCob');


        $campos['empresa_324'] = array('razon_social','rfc');
        $campos['empresa_facturara_expedicion'] = array('razon_social','rfc');
        $campos['empresa_facturara_actual'] = array('razon_social','rfc');

        $campos['factura_emitida'] = array('id','serie','folio','fecha','saldo','subtotal','total','uuid');
        $campos['factura_inicial'] = array('id','serie','folio','fecha','saldo','subtotal','total','uuid');

        $campos['producto'] = array('id','descripcion');
        $campos['plaza'] = array('id','descripcion');

        $campos['conf_costo_estimado_actual'] = array('id','costo','fecha_inicial','fecha_fin');
        $campos['conf_costo_estimado_expedicion'] = array('id','costo','fecha_inicial','fecha_fin');
        $campos['solicitud_servicios_acuerdos'] = array('fecha_expedicion');


        $campos_sql = (new sql())->campos_alias($campos);
        if (error::$en_error) {
            return (new error())->error('Error al generar $campos_sql', $campos_sql);
        }

        $lf_costo_estimado_actual = $this->lf_costo_estimado('CURDATE()','_actual');
        if (error::$en_error) {
            return (new error())->error('Error al generar $lf_costo_estimado_actual', $lf_costo_estimado_actual);
        }

        $lf_costo_estimado_expedicion = $this->lf_costo_estimado('solicitud_servicios_acuerdos.fecha_expedicion','_expedicion');
        if (error::$en_error) {
            return (new error())->error('Error al generar $lf_costo_estimado_expedicion', $lf_costo_estimado_expedicion);
        }

        $lefts[] = "empresa AS empresa_324 ON empresa_324.id = contrato.empresa_id";
        $lefts[] = "factura_contrato ON factura_contrato.contrato_id = contrato.id";
        $lefts[] = "factura AS factura_inicial ON factura_inicial.id = factura_contrato.factura_id";
        $lefts[] = "plaza ON plaza.id = contrato.plaza_id";
        $lefts[] = "producto ON producto.id = contrato.producto_id";
        $lefts[] = "solicitud_servicios_acuerdos ON solicitud_servicios_acuerdos.contrato_id = contrato.id";
        $lefts[] = $lf_costo_estimado_actual;
        $lefts[] = $lf_costo_estimado_expedicion;
        $lefts[] = "conf_fc_funeraria AS conf_fc_funeraria_expedicion ON conf_fc_funeraria_expedicion.conf_costo_estimado_id = conf_costo_estimado_expedicion.id";
        $lefts[] = "conf_fc_funeraria AS conf_fc_funeraria_actual ON conf_fc_funeraria_actual.conf_costo_estimado_id = conf_costo_estimado_actual.id";
        $lefts[] = "empresa AS empresa_facturara_expedicion ON empresa_facturara_expedicion.id = conf_fc_funeraria_expedicion.empresa_id";
        $lefts[] = "empresa AS empresa_facturara_actual ON empresa_facturara_actual.id = conf_fc_funeraria_actual.empresa_id";
        $lefts[] = "factura_fune ON factura_fune.contrato_id = contrato.id";
        $lefts[] = "factura AS factura_emitida ON factura_emitida.id = factura_fune.factura_id";

        $lefts_sql = '';
        foreach ($lefts as $left) {
            $lefts_sql.=" LEFT JOIN $left ";
        }

        $fec  = "'$fechas->fecha_inicial' AND '$fechas->fecha_final'";
        $wh_sc = "solicitud_servicios_acuerdos.fecha_expedicion BETWEEN $fec";
        $wh_sc_st = "AND solicitud_servicios_acuerdos.status = 'activo' ";
        $wh_em = "AND empresa_324.es_324 = 'activo'";
        $wh_fc = "AND factura_inicial.uuid IS NOT NULL";

        $where_sql = "$where $wh_sc $wh_sc_st $wh_em $wh_fc";


        return /** @lang MYSQL */ "SELECT $campos_sql FROM contrato $lefts_sql WHERE $where_sql";

    }

    /**
     * EM3
     * Genera una consulta SQL para obtener el código postal de una empresa en relación con una plaza específica.
     *
     * Esta función valida la existencia y valores de los parámetros `$contrato->plaza_id` y `$empresa->id` o `$empresa->empresa_id`.
     * Si alguno de estos valores no existe o es inválido, se genera un error.
     * Si los valores son válidos, la función retorna una consulta SQL que selecciona el código postal desde la tabla `empresa_plaza`.
     *
     * @param stdClass $contrato Objeto con información del contrato. Debe contener la propiedad `plaza_id` con un ID mayor a 0.
     * @param stdClass $empresa  Objeto con información de la empresa. Puede contener `id` o `empresa_id` como identificador válido.
     *
     * @return string|array Retorna una consulta SQL para obtener el código postal si los parámetros son correctos.
     *                      En caso de error, devuelve un array con el mensaje de error y los datos asociados.
     *
     * @example
     * ```php
     * $contrato = new stdClass();
     * $contrato->plaza_id = 5;
     *
     * $empresa = new stdClass();
     * $empresa->id = 10;
     *
     * $sql = (new costo_estimado_det())->cp_empresa($contrato, $empresa);
     * echo $sql;
     * // Output: "SELECT cp.codigo_postal AS codigo_postal FROM empresa_plaza INNER JOIN cp ON cp.id = empresa_plaza.cp_id WHERE plaza_id = 5 AND empresa_id = 10"
     * ```
     *
     * @example
     * ```php
     * $contrato = new stdClass();
     * $contrato->plaza_id = 5;
     *
     * $empresa = new stdClass();
     * $empresa->empresa_id = 10;
     *
     * $sql = (new costo_estimado_det())->cp_empresa($contrato, $empresa);
     * echo $sql;
     * // Output: "SELECT cp.codigo_postal AS codigo_postal FROM empresa_plaza INNER JOIN cp ON cp.id = empresa_plaza.cp_id WHERE plaza_id = 5 AND empresa_id = 10"
     * ```
     *
     * @example (Caso de error: Empresa sin ID válido)
     * ```php
     * $contrato = new stdClass();
     * $contrato->plaza_id = 5;
     *
     * $empresa = new stdClass();
     *
     * $resultado = (new costo_estimado_det())->cp_empresa($contrato, $empresa);
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [error] => Error is no existe $empresa->id debe existir $empresa->empresa_id
     * //     [data] => stdClass Object ()
     * // )
     * ```
     *
     * @example (Caso de error: Plaza sin ID válido)
     * ```php
     * $contrato = new stdClass();
     * $contrato->plaza_id = 0;
     *
     * $empresa = new stdClass();
     * $empresa->id = 10;
     *
     * $resultado = (new costo_estimado_det())->cp_empresa($contrato, $empresa);
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [error] => Error is no existe $contrato->plaza_id debe ser mayor a 0
     * //     [data] => stdClass Object ( [plaza_id] => 0 )
     * // )
     * ```
     */
    final public function cp_empresa(stdClass $contrato, stdClass $empresa)
    {
        if (!isset($empresa->id)) {
            if (!isset($empresa->empresa_id)) {
                return (new error())->error(
                    'Error is no existe $empresa->id debe existir $empresa->empresa_id', $empresa
                );
            }
            if ((int)$empresa->empresa_id <= 0) {
                return (new error())->error('Error is no existe $empresa->id debe ser mayor a 0', $empresa);
            }
            $empresa->id = $empresa->empresa_id;
        }

        if (!isset($empresa->empresa_id)) {
            $empresa->empresa_id = $empresa->id;
        }

        if (!isset($contrato->plaza_id)) {
            return (new error())->error('Error $contrato->plaza_id debe existir', $contrato);
        }

        if ((int)$contrato->plaza_id <= 0) {
            return (new error())->error('Error is no existe $contrato->plaza_id debe ser mayor a 0', $contrato);
        }

        if ((int)$empresa->id <= 0) {
            return (new error())->error('Error is no existe $empresa->id debe ser mayor a 0', $empresa);
        }

        $cp = "cp.codigo_postal AS codigo_postal";
        $join = "cp ON cp.id = empresa_plaza.cp_id";
        $wr_plaza = "plaza_id = $contrato->plaza_id";
        $wr_empresa = "empresa_id = $empresa->id";

        return /** @lang MYSQL */ "SELECT $cp FROM empresa_plaza INNER JOIN $join WHERE $wr_plaza AND $wr_empresa";
    }


    final public function factura_fune(int $contrato_id): string
    {
        return /** @lang MYSQL */ "SELECT *FROM factura_fune WHERE factura_fune.contrato_id = $contrato_id";

    }

    private function lf_costo_estimado(string $campo_fecha_compare, string $sufijo): string
    {
        $alias = "conf_costo_estimado$sufijo";
        return "conf_costo_estimado AS $alias ON $alias.empresa_id = empresa_324.id 
            AND $alias.plaza_id = contrato.plaza_id AND contrato.producto_id = $alias.producto_id 
            AND $campo_fecha_compare BETWEEN $alias.fecha_inicial AND $alias.fecha_fin";
    }

    /**
     * REG
     * Obtiene la serie de facturación basada en la operación, plaza y empresa.
     *
     * Esta función valida la existencia y valores de los parámetros `contrato->plaza_id`, `empresa->empresa_id`
     * y `tipo_operacion_factura_id`. Luego, genera una consulta SQL para buscar la serie CFDI correspondiente.
     *
     * @param stdClass $contrato Objeto con información del contrato. Debe contener `plaza_id` con un valor mayor a 0.
     * @param stdClass $empresa  Objeto con información de la empresa. Debe contener `empresa_id` con un valor mayor a 0.
     * @param int $tipo_operacion_factura_id ID del tipo de operación de factura. Debe ser mayor a 0.
     *
     * @return string|array Retorna una consulta SQL que recupera la serie de facturación.
     *                      En caso de error, devuelve un array con el mensaje de error y los datos asociados.
     *
     * @example
     * ```php
     * $contrato = new stdClass();
     * $contrato->plaza_id = 7;
     *
     * $empresa = new stdClass();
     * $empresa->empresa_id = 10;
     *
     * $tipo_operacion_factura_id = 3;
     *
     * $sql = (new costo_estimado_det())->serie($contrato, $empresa, $tipo_operacion_factura_id);
     * echo $sql;
     * // Output: "SELECT * FROM serie_cfdi WHERE tipo_operacion_factura_id = 3 AND plaza_id = 7 AND empresa_id = 10"
     * ```
     *
     * @example (Caso de error: `tipo_operacion_factura_id` menor o igual a 0)
     * ```php
     * $contrato = new stdClass();
     * $contrato->plaza_id = 7;
     *
     * $empresa = new stdClass();
     * $empresa->empresa_id = 10;
     *
     * $tipo_operacion_factura_id = 0;
     *
     * $resultado = (new costo_estimado_det())->serie($contrato, $empresa, $tipo_operacion_factura_id);
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [error] => Error $tipo_operacion_factura_id debe ser mayor a 0
     * //     [data] => 0
     * // )
     * ```
     *
     * @example (Caso de error: `contrato->plaza_id` no definido)
     * ```php
     * $contrato = new stdClass();
     * // Falta definir $contrato->plaza_id
     *
     * $empresa = new stdClass();
     * $empresa->empresa_id = 10;
     *
     * $tipo_operacion_factura_id = 3;
     *
     * $resultado = (new costo_estimado_det())->serie($contrato, $empresa, $tipo_operacion_factura_id);
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [error] => Error $contrato->plaza_id debe ser mayor a 0
     * //     [data] => stdClass Object ()
     * // )
     * ```
     */
    final public function serie(stdClass $contrato, stdClass $empresa, int $tipo_operacion_factura_id)
    {
        // Validación del ID de operación de factura
        if ($tipo_operacion_factura_id <= 0) {
            return (new error())->error('Error $tipo_operacion_factura_id debe ser mayor a 0',
                $tipo_operacion_factura_id);
        }

        // Validación de la existencia de plaza_id en el contrato
        if (!isset($contrato->plaza_id)) {
            return (new error())->error('Error $contrato->plaza_id debe ser mayor a 0', $contrato);
        }

        // Validación de la existencia de empresa_id en la empresa
        if (!isset($empresa->empresa_id)) {
            return (new error())->error('Error $empresa->empresa_id debe ser mayor a 0', $empresa);
        }

        // Validación de que plaza_id sea mayor a 0
        if ((int)$contrato->plaza_id <= 0) {
            return (new error())->error('Error $contrato->plaza_id debe ser mayor a 0', $contrato);
        }

        // Validación de que empresa_id sea mayor a 0
        if ((int)$empresa->empresa_id <= 0) {
            return (new error())->error('Error $empresa->empresa_id debe ser mayor a 0', $empresa);
        }

        // Construcción de la consulta SQL
        $wr_to = "tipo_operacion_factura_id = $tipo_operacion_factura_id";
        $wr_plaza = "plaza_id = $contrato->plaza_id";
        $wr_empresa = "empresa_id = $empresa->empresa_id";

        return /** @lang MYSQL */ "SELECT * FROM serie_cfdi WHERE $wr_to AND $wr_plaza AND $wr_empresa";
    }


    final public function solicitud_servicio(int $contrato_id): string
    {
        $where = "contrato_id = $contrato_id AND status = 'activo'";
        return /** @lang MYSQL */ "SELECT *FROM solicitud_servicios_acuerdos WHERE $where";

    }


}
