<?php
namespace desarrollo_em3\manejo_datos\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;
use stdClass;


class pago_nomina{

    /**
     * TRASLADADO
     * Genera un objeto `stdClass` con las cadenas SQL correspondientes a los campos de la entidad `pago_nomina`.
     *
     * Esta función obtiene los campos de la entidad `pago_nomina` utilizando la función `campos_replace` y luego
     * genera las cadenas SQL correspondientes a cada campo utilizando la función `campos_sql`. Si ocurre algún error
     * durante la obtención o procesamiento de los campos, se devuelve un array con los detalles del error.
     *
     * @return stdClass|array Devuelve un objeto `stdClass` con las cadenas SQL para cada campo si el proceso es exitoso,
     *                        o un array de error si ocurre algún problema durante la generación de las cadenas SQL.
     */
    final public function campos_sql()
    {
        $campos = (new \desarrollo_em3\manejo_datos\data\entidades\pago_nomina())->campos_replace();
        if(error::$en_error){
            return (new error())->error('Error al integrar $campos',$campos);
        }

        $campos_sql = (new sql())->campos_sql($campos);
        if(error::$en_error){
            return (new error())->error('Error al integrar $campos_sql',$campos_sql);
        }

        return $campos_sql;

    }

    /**
     * TRASLADADO
     * Genera una cláusula LEFT JOIN para una consulta SQL.
     *
     * Esta función prepara y devuelve una cláusula LEFT JOIN entre las tablas `cuenta_empleado` y `pago_nomina`.
     * Valida si ha ocurrido algún error durante la construcción de la cláusula JOIN.
     *
     * @return string|array Cláusula LEFT JOIN construida.
     */
    final public function join_base()
    {
        $lj_cuenta_empleado_full = (new sql())->left_join('cuenta_empleado','pago_nomina');
        if(error::$en_error){
            return (new error())->error('Error al integrar $lj_cuenta_empleado_full',$lj_cuenta_empleado_full);
        }
        $lj_cuenta_empleado_join = "LEFT JOIN $lj_cuenta_empleado_full";

        return "$lj_cuenta_empleado_join";

    }

    /**
     * TRASLADADO
     * Genera una cadena de instrucciones SQL `JOIN` utilizando parámetros de la clase `pago_nomina`.
     *
     * Esta función obtiene los parámetros necesarios para generar las instrucciones SQL `JOIN` a través de la función
     * `params_replaces` de la clase `pago_nomina`. Luego, utiliza la función `genera_join_replace` para crear la cadena
     * de instrucciones `JOIN`. Si ocurre algún error durante estos procesos, se captura y se retorna un mensaje de error.
     *
     * @return string|array Retorna una cadena de instrucciones `JOIN` concatenadas, o un array de error si ocurre algún problema.
     */
    final public function join_replace()
    {
        $params = (new \desarrollo_em3\manejo_datos\data\entidades\pago_nomina())->params_replaces();
        if(error::$en_error){
            return (new error())->error('Error al integrar $params',$params);
        }


        $join_replace = (new sql())->genera_join_replace($params);
        if(error::$en_error){
            return (new error())->error('Error al integrar $join_replace',$join_replace);
        }

        return $join_replace;

    }


}
