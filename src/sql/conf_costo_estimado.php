<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;
use stdClass;

class conf_costo_estimado{

    /**
     * REG
     * Obtiene la configuración de costo estimado actual para una empresa y contrato específico.
     *
     * Esta función valida la existencia y valores de los parámetros `empresa_324->id`, `contrato->producto_id`
     * y `contrato->plaza_id`. Luego, genera una consulta SQL que busca el costo estimado actual en función
     * de la empresa, producto y plaza, considerando la fecha actual (`CURDATE()`).
     *
     * @param stdClass $contrato Objeto con información del contrato. Debe contener `producto_id` y `plaza_id` con valores mayores a 0.
     * @param stdClass $empresa_324 Objeto con información de la empresa. Debe contener `id` con un valor mayor a 0.
     *
     * @return string|array Retorna una consulta SQL que recupera la configuración de costo estimado actual.
     *                      En caso de error, devuelve un array con el mensaje de error y los datos asociados.
     *
     * @example
     * ```php
     * $contrato = new stdClass();
     * $contrato->producto_id = 15;
     * $contrato->plaza_id = 7;
     *
     * $empresa_324 = new stdClass();
     * $empresa_324->id = 3;
     *
     * $sql = (new conf_costo_estimado())->costo_estimado_actual($contrato, $empresa_324);
     * echo $sql;
     * // Output: "SELECT empresa.id AS empresa_id, conf_costo_estimado.costo FROM conf_costo_estimado
     * //          LEFT JOIN conf_fc_funeraria ON conf_fc_funeraria.conf_costo_estimado_id = conf_costo_estimado.id
     * //          LEFT JOIN empresa ON conf_fc_funeraria.empresa_id = empresa.id
     * //          WHERE conf_costo_estimado.empresa_id = 3
     * //          AND conf_costo_estimado.producto_id = 15
     * //          AND conf_costo_estimado.plaza_id = 7
     * //          AND CURDATE() BETWEEN conf_costo_estimado.fecha_inicial AND conf_costo_estimado.fecha_fin"
     * ```
     *
     * @example (Caso de error: `empresa_324->id` no definido)
     * ```php
     * $contrato = new stdClass();
     * $contrato->producto_id = 15;
     * $contrato->plaza_id = 7;
     *
     * $empresa_324 = new stdClass();
     *
     * $resultado = (new conf_costo_estimado())->costo_estimado_actual($contrato, $empresa_324);
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [error] => Error $empresa_324->id debe existir
     * //     [data] => stdClass Object ()
     * // )
     * ```
     *
     * @example (Caso de error: `contrato->producto_id` menor o igual a 0)
     * ```php
     * $contrato = new stdClass();
     * $contrato->producto_id = 0;
     * $contrato->plaza_id = 7;
     *
     * $empresa_324 = new stdClass();
     * $empresa_324->id = 3;
     *
     * $resultado = (new conf_costo_estimado())->costo_estimado_actual($contrato, $empresa_324);
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [error] => Error $contrato->producto_id debe ser mayor a 0
     * //     [data] => stdClass Object ( [producto_id] => 0 )
     * // )
     * ```
     */
    final public function costo_estimado_actual(stdClass $contrato, stdClass $empresa_324)
    {
        // Validación de existencia y valores de los parámetros
        if (!isset($empresa_324->id)) {
            return (new error())->error('Error $empresa_324->id debe existir', $empresa_324);
        }
        if ((int)$empresa_324->id <= 0) {
            return (new error())->error('Error $empresa_324->id debe ser mayor a 0', $empresa_324);
        }

        if (!isset($contrato->producto_id)) {
            return (new error())->error('Error $contrato->producto_id debe existir', $contrato);
        }
        if ((int)$contrato->producto_id <= 0) {
            return (new error())->error('Error $contrato->producto_id debe ser mayor a 0', $contrato);
        }

        if (!isset($contrato->plaza_id)) {
            return (new error())->error('Error $contrato->plaza_id debe existir', $contrato);
        }
        if ((int)$contrato->plaza_id <= 0) {
            return (new error())->error('Error $contrato->plaza_id debe ser mayor a 0', $contrato);
        }

        // Campos seleccionados en la consulta
        $campos = "empresa.id AS empresa_id, conf_costo_estimado.costo";

        // Condiciones de JOIN
        $on_fune = "conf_fc_funeraria.conf_costo_estimado_id = conf_costo_estimado.id";
        $on_emp = "conf_fc_funeraria.empresa_id = empresa.id";

        // Condiciones WHERE
        $wh_empresa = "conf_costo_estimado.empresa_id = $empresa_324->id";
        $wh_producto = "conf_costo_estimado.producto_id = $contrato->producto_id";
        $wh_plaza = "conf_costo_estimado.plaza_id = $contrato->plaza_id";
        $wh_fechas = "CURDATE() BETWEEN conf_costo_estimado.fecha_inicial AND conf_costo_estimado.fecha_fin";

        $where = "$wh_empresa AND $wh_producto AND $wh_plaza AND $wh_fechas";

        // LEFT JOINs
        $lf_fune = "LEFT JOIN conf_fc_funeraria ON $on_fune";
        $lf_emp = "LEFT JOIN empresa ON $on_emp";
        $from = "conf_costo_estimado $lf_fune $lf_emp";

        // Retorno de la consulta SQL
        return /** @lang MYSQL */ "SELECT $campos FROM $from WHERE $where";
    }



    /**
     * EM3
     * Obtiene la configuración de costo estimado basada en los parámetros proporcionados.
     *
     * Este método valida los parámetros básicos como empresa, producto y plaza antes de generar
     * una consulta SQL para buscar la configuración de costo estimado correspondiente a una fecha de pago específica.
     *
     * @param int $empresa_id ID de la empresa. Debe ser mayor a 0.
     *        - Ejemplo válido: `3`
     *        - Ejemplo inválido: `0` (retornará un error)
     * @param string $fecha_pago Fecha de pago en formato 'YYYY-MM-DD'. No debe estar vacía.
     *        - Ejemplo válido: `'2024-12-15'`
     *        - Ejemplo inválido: `''` (retornará un error)
     * @param int $plaza_id ID de la plaza. Debe ser mayor a 0.
     *        - Ejemplo válido: `5`
     *        - Ejemplo inválido: `-2` (retornará un error)
     * @param int $producto_id ID del producto. Debe ser mayor a 0.
     *        - Ejemplo válido: `10`
     *        - Ejemplo inválido: `0` (retornará un error)
     *
     * @return string|array Consulta SQL que recupera la configuración de costo estimado según los parámetros.
     *                      Si ocurre un error en la validación, devuelve un objeto de error.
     *
     * @throws error Lanza un error si alguno de los parámetros no cumple con los requisitos o
     *               si la fecha de pago está vacía.
     *
     * Ejemplo de uso:
     * ```php
     * $empresa_id = 3;
     * $fecha_pago = '2024-12-15';
     * $plaza_id = 5;
     * $producto_id = 10;
     *
     * $sql = $this->get_conf($empresa_id, $fecha_pago, $plaza_id, $producto_id);
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error en la validación.";
     * } else {
     *     echo "Consulta generada: " . $sql;
     * }
     * ```
     */
    final public function get_conf(int $empresa_id, string $fecha_pago, int $plaza_id, int $producto_id)
    {
        // Valida que los IDs de empresa, producto y plaza sean correctos
        $valida = $this->valida_base($empresa_id, $producto_id, $plaza_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Limpia la fecha de pago y verifica que no esté vacía
        $fecha_pago = trim($fecha_pago);
        if ($fecha_pago === '') {
            return (new error())->error('Error: $fecha_pago está vacía', $fecha_pago);
        }

        // Construcción de condiciones WHERE para la consulta
        $wh_empresa_id = "conf_costo_estimado.empresa_id = $empresa_id";
        $wh_producto_id = "conf_costo_estimado.producto_id = $producto_id";
        $wh_plaza_id = "conf_costo_estimado.plaza_id = $plaza_id";
        $wh_fecha = "'$fecha_pago' BETWEEN conf_costo_estimado.fecha_inicial AND conf_costo_estimado.fecha_fin";

        // Combina todas las condiciones en la cláusula WHERE
        $where = "$wh_empresa_id AND $wh_producto_id AND $wh_plaza_id AND $wh_fecha";

        // Retorna la consulta SQL generada
        return /** @lang MYSQL */ "SELECT * FROM conf_costo_estimado AS conf_costo_estimado WHERE $where";
    }



    /**
     * EM3
     * Valida que los identificadores de empresa, producto y plaza sean mayores a 0.
     *
     * Esta función verifica que los valores de `$empresa_id`, `$producto_id` y `$plaza_id`
     * sean números enteros positivos mayores a 0. Si alguno de los valores es inválido,
     * retorna un objeto de error detallando el problema.
     *
     * @param int $empresa_id ID de la empresa. Debe ser mayor a 0.
     *        - Ejemplo válido: `5`
     *        - Ejemplo inválido: `0` (retornará un error)
     * @param int $producto_id ID del producto. Debe ser mayor a 0.
     *        - Ejemplo válido: `12`
     *        - Ejemplo inválido: `-3` (retornará un error)
     * @param int $plaza_id ID de la plaza. Debe ser mayor a 0.
     *        - Ejemplo válido: `7`
     *        - Ejemplo inválido: `0` (retornará un error)
     *
     * @return bool|array Retorna `true` si todos los valores son válidos.
     *                    En caso de error, devuelve un objeto `error`.
     *
     * @throws error Retorna un error si:
     *         - `$empresa_id` es menor o igual a 0.
     *         - `$producto_id` es menor o igual a 0.
     *         - `$plaza_id` es menor o igual a 0.
     *
     * Ejemplo de uso:
     * ```php
     * $validacion = $this->valida_base(3, 10, 5);
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error en la validación.";
     * } else {
     *     echo "Validación correcta.";
     * }
     * ```
     */
    private function valida_base(int $empresa_id, int $producto_id, int $plaza_id)
    {
        // Verifica que el ID de la empresa sea válido
        if ($empresa_id <= 0) {
            return (new error())->error('Error: empresa_id debe ser mayor a 0', $empresa_id);
        }

        // Verifica que el ID del producto sea válido
        if ($producto_id <= 0) {
            return (new error())->error('Error: producto_id debe ser mayor a 0', $producto_id);
        }

        // Verifica que el ID de la plaza sea válido
        if ($plaza_id <= 0) {
            return (new error())->error('Error: plaza_id debe ser mayor a 0', $plaza_id);
        }

        return true;
    }



}
