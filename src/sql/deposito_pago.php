<?php
namespace desarrollo_em3\manejo_datos\sql;


use desarrollo_em3\error\error;

class deposito_pago{

    private string $key_id_original = 'deposito_pago.id';
    private string $name_entidad = 'deposito_pago';

    /**
     * TRASLADADO
     * Verifica si existe un depósito de pago asociado a un ID de pago específico.
     *
     * Esta función valida que el ID del pago (`$pago_id`) sea mayor a 0. Si es válido, genera una consulta SQL
     * que cuenta los registros en la entidad correspondiente que tienen el mismo ID de pago. Si el ID del pago
     * es menor o igual a 0, se devuelve un error.
     *
     * @param int $pago_id ID del pago que se está verificando.
     *
     * @return string|array Devuelve una consulta SQL que cuenta los registros asociados al ID de pago,
     *                      o un array de error si el ID es inválido.
     */
    final public function existe_deposito_pago(int $pago_id)
    {
        if($pago_id <= 0){
            return (new error())->error('Error $pago_id es menor a 0',$pago_id);
        }
        $wh_id = "$this->name_entidad.pago_id = $pago_id";
        return /** @lang MYSQL */ "SELECT COUNT(*) AS n_registros FROM $this->name_entidad WHERE $wh_id";

    }

    /**
     * TRASLADADO
     * Genera una clave de condición SQL para la columna `deposito_aportaciones_id`.
     *
     * Este método crea una clave de condición SQL basada en el valor proporcionado
     * de `deposito_aportaciones_id`. Si el valor es menor o igual a 0, devuelve un
     * error indicando que el ID debe ser mayor a 0.
     *
     * @param string $deposito_aportaciones_id El ID del depósito de aportaciones.
     *
     * @return string|array Una cadena que representa la condición SQL para la
     *                      columna `deposito_aportaciones_id`. En caso de error,
     *                      devuelve un arreglo con la información del error.
     */
    private function key_deposito_ap_id(string $deposito_aportaciones_id)
    {
        if($deposito_aportaciones_id <= 0){
            return (new error())->error('Error $deposito_aportaciones_id debe ser mayor a 0',
                $deposito_aportaciones_id);
        }
        return "$this->name_entidad.deposito_aportaciones_id = $deposito_aportaciones_id";
    }

    /**
     * TRASLADADO
     * Genera una condición SQL para filtrar depósitos de aportaciones activos.
     *
     * Este método crea una condición SQL que filtra los registros de la entidad
     * basada en el `deposito_aportaciones_id` proporcionado y en el estado
     * `activo`. Si el ID es menor o igual a 0, o si ocurre un error al generar la
     * clave de comparación, se devuelve un error.
     *
     * @param int $deposito_aportaciones_id El ID del depósito de aportaciones a filtrar.
     *
     * @return string|array Una cadena que representa la condición SQL para filtrar
     *                      depósitos de aportaciones activos. En caso de error,
     *                      devuelve un arreglo con la información del error.
     */
    final public function depositos_por_aport_filtro(int $deposito_aportaciones_id)
    {
        if($deposito_aportaciones_id <= 0){
            return (new error())->error('Error $deposito_aportaciones_id debe ser mayor a 0',
                $deposito_aportaciones_id);
        }
        $key_dep_id = $this->key_deposito_ap_id($deposito_aportaciones_id);
        if(error::$en_error){
            return (new error())->error('Error al integra valor de comparacion',$key_dep_id);
        }
        return "$key_dep_id AND $this->name_entidad.status = 'activo'";

    }


}
