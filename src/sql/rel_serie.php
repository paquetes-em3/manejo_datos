<?php
namespace desarrollo_em3\manejo_datos\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;
use stdClass;

class rel_serie{

    private string $key_id_original = 'rel_serie.id';
    private string $name_entidad = 'rel_serie';

    /**
     * TRASLADADO
     * Genera una consulta SQL para verificar si existen series duplicadas en una plaza.
     *
     * Esta función crea una consulta SQL que selecciona códigos de serie y cuenta cuántas veces aparecen en una plaza
     * específica. Luego, agrupa los resultados por código de serie y aplica un filtro `HAVING` para devolver solo
     * aquellos que aparecen más veces que la cantidad especificada.
     *
     * @param int $cantidad El número mínimo de veces que una serie debe aparecer para considerarse duplicada. Debe ser mayor a 0.
     * @param int $plaza_id El ID de la plaza donde se realiza la búsqueda de series duplicadas. Debe ser mayor a 0.
     *
     * @return array|string Devuelve una cadena que representa la consulta SQL generada. Si ocurre un error, se devuelve un mensaje de error.
     */
    final public function consulta_serie_duplicada_plaza(int $cantidad, int $plaza_id) {
        if($cantidad <= 0) {
            return (new error())->error('Error $cantidad es menor a 0',$cantidad);
        }
        if($plaza_id <= 0) {
            return (new error())->error('Error $plaza_id es menor a 0',$plaza_id);
        }

        $campos = "serie.codigo AS serie_codigo, COUNT( serie.codigo ) AS total_serie ";
        $from = "rel_serie AS rel_serie";
        $joins = $this->joins();
        if(error::$en_error){
            return (new error())->error('Error generar $joins', $joins);
        }

        $lf_from = $from . ' ' . $joins;
        $where = "plaza_id = $plaza_id ";
        $group_by = "GROUP BY serie.codigo ";
        $having = "HAVING total_serie > $cantidad";

        $wh_group_have = $where . ' ' . $group_by . ' ' . $having;

        return /** @lang   MYSQL */ "SELECT $campos FROM $lf_from WHERE $wh_group_have";
    }

    /**
     * TRASLADADO
     * Inicializa y genera una cadena de campos SQL formateados con alias (`AS`),
     * a partir de una lista de campos base que incluyen campos de la serie, cuenta de empresa y banco.
     *
     * @return string|array Devuelve una cadena con los campos SQL generados, formateados con alias.
     *                      Si ocurre un error, devuelve un array con los detalles del error.
     */
    private function init_campos_base() {
        $campos_base =  array('rel_serie.id', 'serie.codigo', 'cuenta_empresa.id', 'cuenta_empresa.cuenta',
            'cuenta_empresa.alias', 'banco.descripcion', 'banco.razon_social');

        $campos = (new sql())->campos_sql_puros_with_as($campos_base);
        if(error::$en_error){
            return (new error())->error('Error al generar $campos', $campos);
        }

        return $campos;
    }

    private function joins() {
        $params = array();
        $params[0] = new stdClass();
        $params[0]->entidad = 'serie';
        $params[0]->entidad_right = 'rel_serie';

        $params[1] = new stdClass();
        $params[1]->entidad = 'cuenta_empresa';
        $params[1]->entidad_right = 'rel_serie';

        $params[2] = new stdClass();
        $params[2]->entidad = 'empresa';
        $params[2]->entidad_right = 'cuenta_empresa';

        $params[3] = new stdClass();
        $params[3]->entidad = 'plaza';
        $params[3]->entidad_right = 'cuenta_empresa';

        $params[4] = new stdClass();
        $params[4]->entidad = 'banco';
        $params[4]->entidad_right = 'cuenta_empresa';

        $result = (new sql())->genera_join_replace($params);
        if(error::$en_error){
            return (new error())->error('Error generar join', $result);
        }

        return $result;
    }

    final public function obten_cuenta_bancaria_con_serie(int $plaza_id, string $es_contable) {
        if($plaza_id <= 0){
            return (new error())->error('Error $plaza_id es menor a 0',$plaza_id);
        }

        $campos = $this->init_campos_base();
        if(error::$en_error){
            return (new error())->error('Error al generar $campos', $campos);
        }

        $from = 'rel_serie AS rel_serie';

        $joins = $this->joins();
        if(error::$en_error){
            return (new error())->error('Error generar $joins', $joins);
        }

        $from_lj = $from . ' ' . $joins;
        $where = $this->where($plaza_id, $es_contable);

        return /** @lang    MySQL */ "SELECT $campos FROM $from_lj WHERE $where";
    }

    private function where(int $plaza_id, string $es_contable) {
        if($plaza_id <= 0){
            return (new error())->error('Error $plaza_id es menor a 0',$plaza_id);
        }
        $sentencias = array();
        if($es_contable !== ''){
            $sentencias['es_contable'] = "banco.es_contable = '$es_contable'";
        }
        $sentencias['plaza_id'] = "plaza.id = $plaza_id";
        $sentencias['deposita_aportacion'] = "rel_serie.deposita_aportacion = 'activo'";

        $where = (new sql())->where($sentencias);
        if(error::$en_error){
            return (new error())->error('Error generar $where', $where);
        }

        return $where;
    }

}
