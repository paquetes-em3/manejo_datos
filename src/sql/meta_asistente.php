<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;

class meta_asistente{

    /**
     * FIN
     * Genera una consulta SQL para obtener la cantidad de un periodo específico para un empleado.
     *
     * Esta función genera una consulta SQL para obtener la cantidad asociada a un empleado
     * (identificado por $ohem_id) en un periodo comercial específico (identificado por $periodo_id).
     * Verifica que ambos IDs sean mayores a 0 antes de generar la consulta.
     *
     * @param int $ohem_id El ID del empleado. Debe ser mayor a 0.
     * @param int $periodo_id El ID del periodo comercial. Debe ser mayor a 0.
     *
     * @return string|array La consulta SQL generada. Si alguno de los parámetros no es válido, devuelve un array con
     * el mensaje de error correspondiente.
     */
    final public function cantidad_periodo(int $ohem_id, int $periodo_id)
    {
        if($ohem_id <= 0){
            return (new error())->error('Error ohem_id debe ser mayor a 0',$ohem_id);
        }
        if($periodo_id <= 0){
            return (new error())->error('Error $periodo_id debe ser mayor a 0',$periodo_id);
        }
        $cantidad = "meta_asistente.cantidad";
        $where_periodo_id = "meta_asistente.periodo_comercial_id = $periodo_id";
        $where_ohem_id = "meta_asistente.ohem_id = $ohem_id";
        return /** @lang MYSQL */ "SELECT $cantidad FROM meta_asistente WHERE $where_periodo_id AND $where_ohem_id";

    }
}
