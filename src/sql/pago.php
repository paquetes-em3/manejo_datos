<?php
namespace desarrollo_em3\manejo_datos\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\_valida;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;
use PDO;
use stdClass;

class pago{

    private string $key_id_original = 'pago.id';
    private string $name_entidad = 'pago';

    /**
     * TRASLADADO
     * Obtiene el nombre completo del campo del empleado basado en la entidad.
     *
     * Esta función genera el nombre del campo que representa el nombre completo de un empleado basado en la entidad
     * proporcionada. Si la entidad es "empleado", retorna el nombre específico para esa entidad. De lo contrario,
     * devuelve el campo estándar de "gestor".
     *
     * @param string $entidad_empleado La entidad del empleado, como 'empleado' u otra entidad relevante.
     *
     * @return string|array El nombre del campo correspondiente al nombre completo del empleado o gestor.
     * @throws error Si la entidad proporcionada está vacía.
     */
    private function campo_nombre_empleado(string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacio',$entidad_empleado);
        }
        $campo_nombre_empleado = $entidad_empleado.'_gestor_nombre_completo';

        if($entidad_empleado === 'empleado'){
            $campo_nombre_empleado = $entidad_empleado.'_nombre_completo';
        }
        return $campo_nombre_empleado;

    }

    /**
     * EM3
     * Devuelve una lista base de campos SQL, incluyendo un identificador de empleado dinámico.
     *
     * Esta función genera un arreglo con nombres de campos, validando que el identificador del empleado
     * (`$key_empleado_id`) no esté vacío. Es útil para definir un conjunto estándar de campos en consultas SQL.
     *
     * @param string $key_empleado_id El nombre del campo asociado al identificador del empleado. No puede estar vacío.
     *
     * @return array Devuelve un arreglo con los nombres de los campos:
     *               - `id`
     *               - `status`
     *               - `Canceled`
     *               - `DocTotal`
     *               - `$key_empleado_id` (identificador dinámico del empleado)
     *               - `DocDate`
     *               - `DocDueDate`
     *               - `U_ComiCobranza`
     *               - `U_ComiCalc`
     *               - `comision_calculada`
     *               - `pago_corte_id`
     *               - `plaza_id`
     *               En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar campos base con un identificador de empleado
     * $key_empleado_id = 'empleado_id';
     * $resultado = $this->campos_base($key_empleado_id);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     'id', 'status', 'Canceled', 'DocTotal', 'empleado_id',
     * //     'DocDate', 'DocDueDate', 'U_ComiCobranza', 'U_ComiCalc',
     * //     'comision_calculada', 'pago_corte_id', 'plaza_id'
     * // ]
     *
     * // Ejemplo 2: Error por identificador de empleado vacío
     * $key_empleado_id = '';
     * $resultado = $this->campos_base($key_empleado_id);
     * // Resultado:
     * // ['error' => 'Error $key_empleado_id esta vacio', 'data' => '']
     * ```
     */
    private function campos_base(string $key_empleado_id): array
    {
        // Validar que el identificador del empleado no esté vacío
        $key_empleado_id = trim($key_empleado_id);
        if ($key_empleado_id === '') {
            return (new error())->error('Error $key_empleado_id esta vacio', $key_empleado_id);
        }

        // Retornar el arreglo con los campos base
        return array(
            'id',
            'status',
            'Canceled',
            'DocTotal',
            $key_empleado_id,
            'DocDate',
            'DocDueDate',
            'U_ComiCobranza',
            'U_ComiCalc',
            'comision_calculada',
            'pago_corte_id',
            'plaza_id'
        );
    }


    /**
     * EM3
     * Genera una cadena de campos SQL basados en una lista predeterminada, asociando cada campo con la entidad actual.
     *
     * Esta función utiliza una lista estándar de campos generada por `campos_base`, valida el identificador del empleado,
     * y construye una cadena de campos SQL en formato `entidad.campo` mediante `campos_sql_string`.
     *
     * @param string $key_empleado_id El nombre del campo asociado al identificador del empleado. No puede estar vacío.
     *
     * @return string|array Devuelve una cadena de campos SQL en el formato `entidad.campo, entidad.campo`.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar campos SQL con un identificador de empleado
     * $key_empleado_id = 'empleado_id';
     * $this->name_entidad = 'contrato';
     * $resultado = $this->campos_sql_base($key_empleado_id);
     * echo $resultado;
     * // Resultado:
     * // contrato.id, contrato.status, contrato.Canceled, contrato.DocTotal,
     * // contrato.empleado_id, contrato.DocDate, contrato.DocDueDate,
     * // contrato.U_ComiCobranza, contrato.U_ComiCalc, contrato.comision_calculada,
     * // contrato.pago_corte_id, contrato.plaza_id
     *
     * // Ejemplo 2: Error por identificador de empleado vacío
     * $key_empleado_id = '';
     * $resultado = $this->campos_sql_base($key_empleado_id);
     * // Resultado:
     * // ['error' => 'Error $key_empleado_id esta vacio', 'data' => '']
     * ```
     */
    private function campos_sql_base(string $key_empleado_id)
    {
        // Validar que el identificador del empleado no esté vacío
        $key_empleado_id = trim($key_empleado_id);
        if ($key_empleado_id === '') {
            return (new error())->error('Error $key_empleado_id esta vacio', $key_empleado_id);
        }

        // Obtener la lista de campos base
        $campos = $this->campos_base($key_empleado_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campos', $campos);
        }

        // Generar la cadena de campos SQL en formato `entidad.campo`
        $campos_sql = (new sql())->campos_sql_string($campos, $this->name_entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campos_sql', $campos_sql);
        }

        // Retornar la cadena de campos SQL generada
        return $campos_sql;
    }


    /**
     * EM3
     * Genera una consulta SQL para obtener registros de una entidad utilizando un operador `LIKE`.
     *
     * Esta función valida los parámetros de entrada, construye dinámicamente los campos y relaciones necesarias,
     * y genera una consulta SQL completa utilizando el operador `LIKE` para la condición `WHERE`.
     *
     * @param int $pago_id El identificador del pago a buscar. Debe ser mayor a 0.
     * @param string $key_empleado_id El nombre del campo asociado al identificador del empleado. No puede estar vacío.
     * @param int $limit El número máximo de resultados a devolver. Por defecto, es 20. Debe ser mayor a 0.
     *
     * @return string|array Devuelve una consulta SQL completa en el formato:
     *                      `SELECT campos FROM entidad JOINs WHERE campo LIKE 'valor%' ORDER BY campo DESC LIMIT valor;`.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar una consulta SELECT con operador LIKE
     * $pago_id = 12345;
     * $key_empleado_id = 'empleado_id';
     * $limit = 10;
     * $resultado = $this->get_by_id($pago_id, $key_empleado_id, $limit);
     * echo $resultado;
     * // Resultado:
     * // SELECT contrato.id, contrato.status, contrato.DocTotal, ... FROM contrato
     * // LEFT JOIN ... WHERE contrato.empleado_id LIKE '12345%' ORDER BY contrato.empleado_id DESC LIMIT 10;
     *
     * // Ejemplo 2: Error por límite inválido
     * $pago_id = 12345;
     * $key_empleado_id = 'empleado_id';
     * $limit = 0;
     * $resultado = $this->get_by_id($pago_id, $key_empleado_id, $limit);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error al validar entrada', 'data' => ...]
     *
     * // Ejemplo 3: Error por identificador de empleado vacío
     * $pago_id = 12345;
     * $key_empleado_id = '';
     * $limit = 10;
     * $resultado = $this->get_by_id($pago_id, $key_empleado_id, $limit);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $key_empleado_id esta vacio', 'data' => '']
     * ```
     */
    final public function get_by_id(int $pago_id, string $key_empleado_id, int $limit = 20)
    {
        // Validar los parámetros básicos de entrada
        $valida = $this->valida_entrada_base($limit, $pago_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar entrada', $valida);
        }

        // Validar que el identificador del empleado no esté vacío
        $key_empleado_id = trim($key_empleado_id);
        if ($key_empleado_id === '') {
            return (new error())->error('Error $key_empleado_id esta vacio', $key_empleado_id);
        }

        // Generar la consulta SQL utilizando el operador LIKE
        $select = $this->get_by_id_base($key_empleado_id, $limit, $pago_id, 'LIKE');
        if (error::$en_error) {
            return (new error())->error('Error al obtener $params_sql', $select);
        }

        // Retornar la consulta SQL generada
        return $select;
    }


    /**
     * EM3
     * Genera una consulta SQL `SELECT` para obtener registros de una entidad basada en filtros y relaciones específicas.
     *
     * Esta función valida los parámetros de entrada, construye los campos y las relaciones necesarias, y genera una consulta SQL
     * completa utilizando una configuración dinámica.
     *
     * @param string $key_empleado_id El nombre del campo asociado al identificador del empleado. No puede estar vacío.
     * @param int $limit El número máximo de resultados a devolver. Debe ser mayor a 0.
     * @param int $pago_id El identificador del pago a buscar. Debe ser mayor a 0.
     * @param string $type_operador El operador a utilizar en la condición `WHERE` (`LIKE` o `IGUAL`).
     *
     * @return string|array Devuelve una consulta SQL completa en el formato:
     *                      `SELECT campos FROM entidad JOINs WHERE condición ORDER BY campo LIMIT valor;`.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar una consulta SELECT con relaciones y filtros
     * $key_empleado_id = 'empleado_id';
     * $limit = 10;
     * $pago_id = 12345;
     * $type_operador = 'IGUAL';
     * $resultado = $this->get_by_id_base($key_empleado_id, $limit, $pago_id, $type_operador);
     * echo $resultado;
     * // Resultado:
     * // SELECT contrato.id, contrato.status, contrato.DocTotal, ... FROM contrato
     * // LEFT JOIN ... WHERE pago_id = '12345' ORDER BY pago_id DESC LIMIT 10;
     *
     * // Ejemplo 2: Error por límite inválido
     * $key_empleado_id = 'empleado_id';
     * $limit = 0;
     * $pago_id = 12345;
     * $type_operador = 'IGUAL';
     * $resultado = $this->get_by_id_base($key_empleado_id, $limit, $pago_id, $type_operador);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $limit debe ser mayor a 0', 'data' => 0]
     * ```
     */
    private function get_by_id_base(string $key_empleado_id, int $limit, int $pago_id, string $type_operador)
    {
        // Validar que el identificador del empleado no esté vacío
        $key_empleado_id = trim($key_empleado_id);
        if ($key_empleado_id === '') {
            return (new error())->error('Error $key_empleado_id esta vacio', $key_empleado_id);
        }

        // Validar el operador
        $valida = (new sql())->valida_type_operador($type_operador);
        if (error::$en_error) {
            return (new error())->error('Error al validar $type_operador', $valida);
        }

        // Validar que el ID del pago sea mayor a 0
        if ($pago_id <= 0) {
            return (new error())->error('Error $pago_id debe ser mayor a 0', $pago_id);
        }

        // Validar que el límite sea mayor a 0
        if ($limit <= 0) {
            return (new error())->error('Error $limit debe ser mayor a 0', $limit);
        }

        // Obtener los parámetros de las relaciones
        $params = $this->params_get_by_id();
        if (error::$en_error) {
            return (new error())->error('Error al obtener $params', $params);
        }

        // Construir los campos SQL base
        $campos_sql = $this->campos_sql_base($key_empleado_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $params', $campos_sql);
        }

        // Generar la consulta SELECT completa
        $select = (new sql())->select_base(
            $campos_sql,
            $this->name_entidad,
            $this->key_id_original,
            $limit,
            $params,
            'DESC',
            $pago_id,
            $type_operador
        );
        if (error::$en_error) {
            return (new error())->error('Error al obtener $params_sql', $select);
        }

        // Retornar la consulta SELECT generada
        return $select;
    }


    /**
     * EM3
     * Genera una consulta SQL para obtener registros de una entidad basándose en un ID específico y un operador `=` (IGUAL).
     *
     * Esta función valida los parámetros de entrada, construye dinámicamente los campos y relaciones necesarias,
     * y genera una consulta SQL completa utilizando el operador `IGUAL` para la condición `WHERE`.
     *
     * @param int $pago_id El identificador del pago a buscar. Debe ser mayor a 0.
     * @param string $key_empleado_id El nombre del campo asociado al identificador del empleado. No puede estar vacío.
     * @param int $limit El número máximo de resultados a devolver. Por defecto, es 1. Debe ser mayor a 0.
     *
     * @return string|array Devuelve una consulta SQL completa en el formato:
     *                      `SELECT campos FROM entidad JOINs WHERE campo = 'valor' ORDER BY campo DESC LIMIT valor;`.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar una consulta SELECT con operador IGUAL
     * $pago_id = 12345;
     * $key_empleado_id = 'empleado_id';
     * $limit = 5;
     * $resultado = $this->get_by_id_equals($pago_id, $key_empleado_id, $limit);
     * echo $resultado;
     * // Resultado:
     * // SELECT contrato.id, contrato.status, contrato.DocTotal, ... FROM contrato
     * // LEFT JOIN ... WHERE contrato.empleado_id = '12345' ORDER BY contrato.empleado_id DESC LIMIT 5;
     *
     * // Ejemplo 2: Error por ID de pago inválido
     * $pago_id = -1;
     * $key_empleado_id = 'empleado_id';
     * $limit = 5;
     * $resultado = $this->get_by_id_equals($pago_id, $key_empleado_id, $limit);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error al validar entrada', 'data' => ...]
     *
     * // Ejemplo 3: Error por identificador de empleado vacío
     * $pago_id = 12345;
     * $key_empleado_id = '';
     * $limit = 5;
     * $resultado = $this->get_by_id_equals($pago_id, $key_empleado_id, $limit);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $key_empleado_id esta vacio', 'data' => '']
     * ```
     */
    final public function get_by_id_equals(int $pago_id, string $key_empleado_id, int $limit = 1)
    {
        // Validar los parámetros básicos de entrada
        $valida = $this->valida_entrada_base($limit, $pago_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar entrada', $valida);
        }

        // Validar que el identificador del empleado no esté vacío
        $key_empleado_id = trim($key_empleado_id);
        if ($key_empleado_id === '') {
            return (new error())->error('Error $key_empleado_id esta vacio', $key_empleado_id);
        }

        // Generar la consulta SQL utilizando el operador IGUAL
        $select = $this->get_by_id_base($key_empleado_id, $limit, $pago_id, 'IGUAL');
        if (error::$en_error) {
            return (new error())->error('Error al obtener $params_sql', $select);
        }

        // Retornar la consulta SQL generada
        return $select;
    }


    private function key_empleado_id(string $entidad_empleado): string
    {
        $entidad_empleado = trim($entidad_empleado);
        $key_empleado_id = $entidad_empleado.'_gestor_id';
        if($entidad_empleado === 'empleado'){
            $key_empleado_id = $entidad_empleado.'_id';
        }

        return $key_empleado_id;

    }

    private function left_join_cliente(string $entidad_cliente): string
    {
        $entidad_cliente = trim($entidad_cliente);
        $left_join_cliente = '';
        if($entidad_cliente !== ''){
            $left_join_cliente = 'LEFT JOIN cliente AS cliente ON cliente.id = contrato.cliente_id';
        }
        return $left_join_cliente;

    }

    private function nom_cte(string $campo_nombre_cliente, string $entidad_cliente): string
    {
        $entidad_cliente =trim($entidad_cliente);
        $nom_cte = "contrato.$campo_nombre_cliente AS contrato_$campo_nombre_cliente";
        if($entidad_cliente !== ''){
            $nom_cte = $entidad_cliente.'.nombre_completo AS cliente_nombre_completo';
        }
        return $nom_cte;

    }

    /**
     * Obtiene información de series faltantes para entregas según el ID de corte de pago especificado.
     *
     * Este método genera una consulta SQL para recuperar los montos totales relacionados con las entregas.
     * y montos depositados para un límite de pago determinado. También garantiza que el pago
     * El ID de corte es válido (mayor que 0).
     *
     * @param string $campo_monto El nombre de la columna que representa el monto en la tabla de pagos.
     * @param string $campo_serie El nombre de la columna de serie en la tabla de contratos.
     * @param int $pago_corte_id El ID del límite de pago para filtrar resultados. Debe ser mayor que 0.
     *
     * @return array|string La consulta SQL generada como una cadena.
     *
     * @throws Error Si $pago_corte_id es menor que 0.
     */
    final public function obten_serie_faltante_entrega(string $campo_monto, string $campo_serie, int $pago_corte_id)
    {
        if ($pago_corte_id < 0) {
            return (new error())->error('Error $pago_id debe ser mayor a 0',$pago_corte_id);
        }

        $campo_contrato = "contrato.$campo_serie AS contrato_$campo_serie";
        $total_entrega = "SUM( pago.$campo_monto ) AS total_entrega";
        $monto_entregado = "IFNULL( SUM( deposito_pago.monto_depositado ), 0 ) AS monto_entregado";

        $campos = "$campo_contrato, $total_entrega, $monto_entregado";
        $from = "pago AS pago";

        $lj_contrato = "contrato AS contrato ON contrato.id = pago.contrato_id";
        $lj_pago_corte = "pago_corte ON pago.pago_corte_id = pago_corte.id";
        $lj_deposito_pago = "deposito_pago ON pago.id = deposito_pago.pago_id ";

        $from_lf = "$from LEFT JOIN $lj_contrato LEFT JOIN $lj_pago_corte LEFT JOIN $lj_deposito_pago";
        $where = "pago_corte.id = $pago_corte_id";
        $where .= " AND pago.status = 'activo'";
        $group_by = "contrato.$campo_serie";

        return /** @lang MYSQL */"SELECT $campos FROM $from_lf WHERE $where GROUP BY $group_by";
    }

    /**
     * TRASLADADO
     * Obtiene el monto del pago basado en el ID del pago proporcionado.
     *
     * Esta función genera una consulta SQL que recupera el total del pago
     * correspondiente al ID del pago proporcionado.
     *
     * @param int $pago_id El ID del pago. Debe ser un valor entero mayor a 0.
     * @return string|array Devuelve una cadena SQL para recuperar el monto del pago o un objeto de error si el ID es inválido.
     */
    final public function pago_monto(int $pago_id)
    {
        if($pago_id <= 0){
            return (new error())->error('Error $pago_id debe ser mayor a 0',$pago_id);
        }
        $pago_id_campo = "pago.id AS pago_id";
        $pago_DocTotal_campo = "pago.DocTotal AS pago_DocTotal";
        return /** @lang MYSQL */ "SELECT $pago_id_campo,$pago_DocTotal_campo FROM pago WHERE pago.id = $pago_id";

    }

    final public function pagos(int $contrato_id, PDO $link)
    {
        if($contrato_id <= 0){
            return (new error())->error('Error $contrato_id debe ser mayor a 0',$contrato_id);
        }
        $pagos_colum = (new consultas())->get_campos('pago',$link);
        if(error::$en_error){
            return (new error())->error('Error al obtener campos pago',$pagos_colum);
        }

        $contrato_column = (new consultas())->get_campos('contrato',$link);
        if(error::$en_error){
            return (new error())->error('Error al obtener campos pago',$pagos_colum);
        }


        return /** @lang MYSQL */ "SELECT $pagos_colum->campos_sql, $contrato_column->campos_sql  FROM pago AS pago LEFT JOIN contrato ON contrato.id = pago.contrato_id WHERE pago.contrato_id = $contrato_id";

    }

    final public function pagos_simplificados(int $contrato_id, bool $puros_id): string
    {

        $select = "*";
        if($puros_id){
            $select = 'id';
        }
        return /** @lang MYSQL */ "SELECT $select FROM pago AS pago WHERE contrato_id = $contrato_id";

    }

    final public function pagos_por_corte_con_deposito(
        string $campo_comentarios, string $campo_fecha, string $campo_fecha_cliente, string $campo_folio,
        string $campo_monto, string $campo_nombre_cliente, string $campo_serie, string $campo_tipo_contrato,
        string $entidad_cliente, string $entidad_empleado, int $pago_corte_id)
    {


        $params_val[] = 'campo_comentarios';
        $params_val[] = 'campo_fecha';
        $params_val[] = 'campo_fecha_cliente';
        $params_val[] = 'campo_folio';
        $params_val[] = 'campo_monto';
        $params_val[] = 'campo_nombre_cliente';
        $params_val[] = 'campo_serie';
        $params_val[] = 'campo_tipo_contrato';
        $params_val[] = 'entidad_empleado';

        foreach ($params_val as $param){
            $value_param = trim($$param);
            if($value_param === ''){
                return (new error())->error('Error '.$param.' esta vacio', $value_param);
            }
        }
        if($pago_corte_id <= 0){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0', $pago_corte_id);
        }

        $entidad_cliente = trim($entidad_cliente);


        $nom_cte = $this->nom_cte($campo_nombre_cliente,$entidad_cliente);
        if(error::$en_error){
            return (new error())->error('Error al generar $nom_cte',$nom_cte);
        }

        $left_join_cliente = $this->left_join_cliente($entidad_cliente);
        if(error::$en_error){
            return (new error())->error('Error al generar join',$left_join_cliente);
        }

        $key_empleado_id = $this->key_empleado_id($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al generar $key_empleado_id',$key_empleado_id);
        }

        $campo_nombre_empleado = $this->campo_nombre_empleado($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al generar $campo_nombre_empleado',$campo_nombre_empleado);
        }

        $serie = "contrato.$campo_serie AS contrato_$campo_serie";
        $folio = "contrato.$campo_folio AS contrato_$campo_folio";

        $monto = "pago.$campo_monto AS pago_$campo_monto";
        $fecha = "pago.$campo_fecha AS pago_$campo_fecha";
        $fecha_cte = "pago.$campo_fecha_cliente AS pago_$campo_fecha_cliente";
        $nombre_emp = "$entidad_empleado.nombre_completo AS $campo_nombre_empleado";
        $emp_id = "$entidad_empleado.id AS $key_empleado_id";
        $status = "pago.status AS pago_status";
        $id = "pago.id AS pago_id";
        $comentarios = "pago.$campo_comentarios AS pago_$campo_comentarios";
        $ref = "deposito_aportaciones.referencia AS referencia";
        $tc = "contrato.$campo_tipo_contrato AS contrato_$campo_tipo_contrato";
        $estilo = " IF(IFNULL(deposito_pago.id, -1)>0,'bg-success','') AS estilo_css";

        $campos = "$serie, $folio, $nom_cte, $monto, $fecha, $fecha_cte, $nombre_emp, $emp_id, $status, ";
        $campos.= "$id, $comentarios, $tc, $ref, $estilo";
        $pago_lf = "pago AS pago";
        $contrato_lf = "contrato AS contrato";
        $deposito_lf = "deposito_pago AS deposito_pago";
        $deposito_apor_lf = "deposito_aportaciones AS deposito_aportaciones";
        $emp_lf = "$entidad_empleado AS $entidad_empleado";

        $on_contrato = "contrato.id = pago.contrato_id";
        $on_deposito = "deposito_pago.pago_id = pago.id";
        $on_deposito_ap = "deposito_aportaciones.id = deposito_pago.deposito_aportaciones_id";
        $on_emp = "$entidad_empleado.id = pago.$key_empleado_id";

        $lf_contrato = "$contrato_lf ON $on_contrato";
        $lf_deposito = "$deposito_lf ON $on_deposito";
        $lf_apor = "$deposito_apor_lf ON $on_deposito_ap";
        $lf_emp = "$emp_lf ON $on_emp";

        $from = "$pago_lf LEFT JOIN $lf_contrato LEFT JOIN $lf_deposito LEFT JOIN $lf_apor ";
        $from .= "LEFT JOIN $lf_emp $left_join_cliente";

        $wh_pc = "pago.pago_corte_id = $pago_corte_id";

        $or_monto = "pago.$campo_monto DESC";
        $or_dp = "deposito_pago.id ASC";
        $or_serie = "contrato.$campo_serie ASC";

        $order = "$or_monto, $or_dp, $or_serie";

        return /** @lang MYSQL */ "SELECT $campos FROM $from WHERE $wh_pc ORDER BY $order";

    }


    /**
     * EM3
     * Genera un filtro SQL `WHERE` para pagos por corte con condiciones específicas.
     *
     * Esta función valida los datos de entrada, genera las condiciones SQL necesarias (como estado activo,
     * cancelación, y monto mayor a cero), y las combina en una cláusula `WHERE` que filtra pagos por corte.
     *
     * @param string $campo_canceled El nombre del campo que indica si un pago está cancelado. Puede estar vacío.
     * @param string $campo_monto_pago El nombre del campo que representa el monto del pago. No debe estar vacío.
     * @param string $campo_movto El nombre del campo que representa el movimiento del pago. No debe estar vacío.
     * @param int $pago_corte_id El identificador del pago por corte. Debe ser mayor a 0.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `WHERE` generada. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar filtro WHERE válido para pagos por corte
     * $campo_canceled = 'Canceled';
     * $campo_monto_pago = 'DocTotal';
     * $campo_movto = 'Movto';
     * $pago_corte_id = 123;
     * $resultado = $this->pagos_por_corte_filtro($campo_canceled, $campo_monto_pago, $campo_movto, $pago_corte_id);
     * echo $resultado;
     * // Resultado:
     * // pago.pago_corte_id = 123 AND pago.status = 'activo' AND pago.Canceled = 'N' AND pago.DocTotal > 0.0
     *
     * // Ejemplo 2: Campo cancelación vacío
     * $campo_canceled = '';
     * $resultado = $this->pagos_por_corte_filtro($campo_canceled, $campo_monto_pago, $campo_movto, $pago_corte_id);
     * echo $resultado;
     * // Resultado:
     * // pago.pago_corte_id = 123 AND pago.status = 'activo' AND pago.DocTotal > 0.0
     *
     * // Ejemplo 3: Error en validación de datos
     * $pago_corte_id = 0;
     * $resultado = $this->pagos_por_corte_filtro($campo_canceled, $campo_monto_pago, $campo_movto, $pago_corte_id);
     * // Resultado:
     * // ['error' => 'Error al validar pago corte', 'data' => [...]]
     * ```
     */
    final public function pagos_por_corte_filtro(
        string $campo_canceled,
        string $campo_monto_pago,
        string $campo_movto,
        int $pago_corte_id
    ) {
        // Validar los datos de entrada
        $valida = (new _valida())->valida_pago_corte($campo_monto_pago, $campo_movto, $pago_corte_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar pago corte', $valida);
        }

        // Generar condición para el identificador de pago por corte
        $st_pago_corte = (new sql())->sentencia_equals('pago.pago_corte_id', $pago_corte_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $st_pago_corte', $st_pago_corte);
        }

        // Generar condición para el estado activo
        $st_status = (new sql())->wh_status_act($this->name_entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $st_status', $st_status);
        }

        // Generar condición para pagos no cancelados (opcional)
        $campo_canceled = trim($campo_canceled);
        $st_pago_canceled = '';
        if ($campo_canceled !== '') {
            $st_pago_canceled = (new sql())->sentencia_equals('pago.' . $campo_canceled, 'N');
            if (error::$en_error) {
                return (new error())->error('Error al obtener $st_pago_corte', $st_pago_corte);
            }

            $st_pago_canceled = " AND $st_pago_canceled";
        }

        // Generar condición para montos mayores a 0
        $cmp_monto_pago = "pago.$campo_monto_pago > 0.0";

        // Combinar todas las condiciones y retornar el filtro
        return "$st_pago_corte AND $st_status $st_pago_canceled AND $cmp_monto_pago";
    }


    /**
     * EM3
     * Genera un conjunto de parámetros para definir relaciones `JOIN` entre la entidad principal y otras entidades.
     *
     * Esta función crea un objeto `stdClass` que especifica las relaciones `JOIN` necesarias para una consulta SQL.
     * Cada relación incluye el nombre de la entidad relacionada (`right`) y su alias (`ren_left`).
     *
     * @return stdClass Devuelve un objeto con las relaciones `JOIN` configuradas:
     *                  - `contrato`: Relaciona la entidad principal con `contrato`.
     *                  - `empleado`: Relaciona la entidad principal con `empleado_gestor`.
     *                  - `plaza`: Relaciona la entidad principal con `plaza`.
     *                  - `pago_corte`: Relaciona la entidad principal con `pago_corte`.
     *
     * @example
     * ```php
     * // Ejemplo: Obtener los parámetros de relaciones JOIN
     * $this->name_entidad = 'gasto';
     * $resultado = $this->params_get_by_id();
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object
     * // (
     * //     [contrato] => stdClass Object
     * //         (
     * //             [right] => gasto
     * //             [ren_left] => contrato
     * //         )
     * //     [empleado] => stdClass Object
     * //         (
     * //             [right] => gasto
     * //             [ren_left] => empleado_gestor
     * //         )
     * //     [plaza] => stdClass Object
     * //         (
     * //             [right] => gasto
     * //             [ren_left] => plaza
     * //         )
     * //     [pago_corte] => stdClass Object
     * //         (
     * //             [right] => gasto
     * //             [ren_left] => pago_corte
     * //         )
     * // )
     * ```
     */
    private function params_get_by_id(): stdClass
    {
        // Crear el objeto de parámetros para las relaciones JOIN
        $params = new stdClass();

        // Relación con contrato
        $params->contrato = new stdClass();
        $params->contrato->right = $this->name_entidad;
        $params->contrato->ren_left = 'contrato';

        // Relación con empleado_gestor
        $params->empleado = new stdClass();
        $params->empleado->right = $this->name_entidad;
        $params->empleado->ren_left = 'empleado_gestor';

        // Relación con plaza
        $params->plaza = new stdClass();
        $params->plaza->right = $this->name_entidad;
        $params->plaza->ren_left = 'plaza';

        // Relación con pago_corte
        $params->pago_corte = new stdClass();
        $params->pago_corte->right = $this->name_entidad;
        $params->pago_corte->ren_left = 'pago_corte';

        // Retornar el objeto de parámetros
        return $params;
    }


    /**
     * EM3
     * Genera una consulta SQL para calcular la suma de un campo en la entidad `pago`, aplicando filtros específicos.
     *
     * Esta función valida los parámetros de entrada, utiliza `params_sum` para construir los parámetros necesarios,
     * y genera una consulta SQL `SELECT` que calcula la suma del campo con un alias.
     *
     * @param string $campo_canceled El nombre del campo que indica si un registro está cancelado. Puede estar vacío.
     * @param string $campo_monto El nombre del campo que será sumado. No debe estar vacío.
     * @param int $contrato_id El identificador del contrato. Debe ser mayor a 0.
     *
     * @return string|array Devuelve una cadena SQL con la consulta generada. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar consulta para sumar montos
     * $campo_canceled = 'Canceled';
     * $campo_monto = 'DocTotal';
     * $contrato_id = 123;
     * $resultado = $this->suma_pago($campo_canceled, $campo_monto, $contrato_id);
     * echo $resultado;
     * // Resultado:
     * // SELECT IFNULL(SUM(pago.DocTotal), 0) AS 'suma_pago' FROM pago WHERE pago.status = 'activo'
     * // AND pago.CANCELED = 'N' AND pago.contrato_id = 123
     *
     * // Ejemplo 2: Error por contrato_id inválido
     * $contrato_id = 0;
     * $resultado = $this->suma_pago($campo_canceled, $campo_monto, $contrato_id);
     * // Resultado:
     * // ['error' => 'Error $contrato_id debe ser mayor a 0', 'data' => 0]
     *
     * // Ejemplo 3: Error por campo vacío
     * $campo_monto = '';
     * $resultado = $this->suma_pago($campo_canceled, $campo_monto, $contrato_id);
     * // Resultado:
     * // ['error' => 'Error $campo_monto esta vacio', 'data' => '']
     * ```
     */
    final public function suma_pago(string $campo_canceled, string $campo_monto, int $contrato_id)
    {
        // Validar que contrato_id sea mayor a 0
        if ($contrato_id <= 0) {
            return (new error())->error('Error $contrato_id debe ser mayor a 0', $contrato_id);
        }

        // Validar que el campo de monto no esté vacío
        $campo_monto = trim($campo_monto);
        if ($campo_monto === '') {
            return (new error())->error('Error $campo_monto esta vacio', $campo_monto);
        }

        // Obtener los parámetros necesarios para la consulta
        $params = (new contrato())->params_sum('suma_pago', $campo_canceled, $campo_monto, $contrato_id, 'pago');
        if (error::$en_error) {
            return (new error())->error('Error al obtener parametros', $params);
        }

        // Generar y retornar la consulta SQL
        return /** @lang MYSQL */ "SELECT $params->campo FROM pago WHERE $params->where";
    }


    /**
     * EM3
     * Valida los parámetros básicos de entrada para consultas SQL o procesos relacionados.
     *
     * Esta función verifica que los valores de `$limit` y `$pago_id` sean mayores a 0, garantizando que sean válidos
     * para su uso en consultas SQL o lógica adicional.
     *
     * @param int $limit El número máximo de resultados a devolver. Debe ser mayor a 0.
     * @param int $pago_id El identificador del pago a buscar. Debe ser mayor a 0.
     *
     * @return bool|array Devuelve `true` si ambos parámetros son válidos.
     *                    En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Validar entrada con valores válidos
     * $limit = 10;
     * $pago_id = 12345;
     * $resultado = $this->valida_entrada_base($limit, $pago_id);
     * echo $resultado; // true
     *
     * // Ejemplo 2: Error por límite inválido
     * $limit = 0;
     * $pago_id = 12345;
     * $resultado = $this->valida_entrada_base($limit, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $limit debe ser mayor a 0', 'data' => 0]
     *
     * // Ejemplo 3: Error por ID de pago inválido
     * $limit = 10;
     * $pago_id = -5;
     * $resultado = $this->valida_entrada_base($limit, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $pago_id debe ser mayor a 0', 'data' => -5]
     * ```
     */
    final public function valida_entrada_base(int $limit, int $pago_id)
    {
        // Validar que el ID del pago sea mayor a 0
        if ($pago_id <= 0) {
            return (new error())->error('Error $pago_id debe ser mayor a 0', $pago_id);
        }

        // Validar que el límite sea mayor a 0
        if ($limit <= 0) {
            return (new error())->error('Error $limit debe ser mayor a 0', $limit);
        }

        // Retornar true si ambos parámetros son válidos
        return true;
    }



}
