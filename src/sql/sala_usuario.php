<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;
use stdClass;

class sala_usuario{

    private string $name_entidad = 'sala_usuario';


    /**
     * EM3
     * Genera una lista de columnas SQL relacionadas con la seguridad, separadas por comas.
     *
     * Esta función obtiene las columnas clave relacionadas con la seguridad a través de `columnas_seguridad`,
     * las valida, y las formatea en una cadena SQL con alias en el formato `entidad.campo AS alias`.
     *
     * @return string|array Devuelve una cadena SQL con las columnas generadas, separadas por comas.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar columnas SQL de seguridad
     * $this->name_entidad = 'sala_usuario';
     * $resultado = $this->campos_seguridad();
     * echo $resultado;
     * // Resultado:
     * // sala_usuario.id AS sala_usuario_id, plaza.id AS plaza_id
     *
     * // Ejemplo 2: Error al obtener columnas de seguridad
     * // Supongamos que ocurre un error en `columnas_seguridad`.
     * $resultado = $this->campos_seguridad();
     * // Resultado:
     * // ['error' => 'Error al obtener $columnas', 'data' => [...]]
     *
     * // Ejemplo 3: Error al generar las columnas SQL
     * // Supongamos que ocurre un error en `campos_sql_string_multi_table`.
     * $resultado = $this->campos_seguridad();
     * // Resultado:
     * // ['error' => 'Error al obtener $campo_sql', 'data' => [...]]
     * ```
     */
    private function campos_seguridad()
    {
        // Obtener las columnas clave relacionadas con la seguridad
        $columnas = $this->columnas_seguridad();
        if (error::$en_error) {
            return (new error())->error('Error al obtener $columnas', $columnas);
        }

        // Generar la lista de columnas SQL
        $campos_sql = (new sql())->campos_sql_string_multi_table($columnas);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campo_sql', $campos_sql);
        }

        // Retornar la lista de columnas SQL generada
        return $campos_sql;
    }


    /**
     * EM3
     * Genera un conjunto de condiciones (`WHERE`) de seguridad para consultas relacionadas con usuarios y salas.
     *
     * Esta función valida que el ID del usuario sea mayor a 0 y crea un objeto con las condiciones necesarias para filtrar
     * registros según el estado del usuario y de las salas.
     *
     * @param int $usuario_id El identificador del usuario. Debe ser mayor a 0.
     *
     * @return stdClass|array Devuelve un objeto donde:
     *                        - Cada propiedad representa una condición con las claves:
     *                          - `campo`: El nombre del campo en la base de datos.
     *                          - `value`: El valor con el cual se filtrará.
     *                        En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar condiciones WHERE para un usuario
     * $usuario_id = 123;
     * $resultado = $this->campos_seguridad_where($usuario_id);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [usuario_id] => stdClass Object (
     * //         [campo] => sala_usuario.usuario_id
     * //         [value] => 123
     * //     ),
     * //     [sala_usuario_status] => stdClass Object (
     * //         [campo] => sala_usuario.status
     * //         [value] => activo
     * //     )
     * // )
     *
     * // Ejemplo 2: Error por usuario_id inválido
     * $usuario_id = 0;
     * $resultado = $this->campos_seguridad_where($usuario_id);
     * // Resultado:
     * // ['error' => 'Error $usuario_id debe ser mayor a 0', 'data' => 0]
     * ```
     */
    private function campos_seguridad_where(int $usuario_id)
    {
        // Validar que el ID del usuario sea mayor a 0
        if ($usuario_id <= 0) {
            return (new error())->error('Error $usuario_id debe ser mayor a 0', $usuario_id);
        }

        // Crear el objeto de condiciones
        $campos_where = new stdClass();

        // Condición para el usuario_id
        $campos_where->usuario_id = new stdClass();
        $campos_where->usuario_id->campo = 'sala_usuario.usuario_id';
        $campos_where->usuario_id->value = $usuario_id;

        // Condición para el estado del usuario en la sala
        $campos_where->sala_usuario_status = new stdClass();
        $campos_where->sala_usuario_status->campo = 'sala_usuario.status';
        $campos_where->sala_usuario_status->value = 'activo';

        // Retornar el objeto con las condiciones WHERE
        return $campos_where;
    }


    /**
     * EM3
     * Genera un objeto con las columnas relacionadas con la seguridad para consultas SQL.
     *
     * Esta función crea un objeto `stdClass` que define columnas clave para aplicar restricciones de seguridad
     * en las consultas, incluyendo las entidades y los nombres de los campos asociados.
     *
     * @return stdClass Devuelve un objeto donde:
     *                  - Cada propiedad es una columna clave para la seguridad.
     *                  - Cada columna es un objeto con las siguientes propiedades:
     *                    - `name_entidad`: El nombre de la entidad (tabla) asociada a la columna.
     *                    - `name_campo`: El nombre del campo dentro de la entidad.
     *
     * @example
     * ```php
     * // Ejemplo: Obtener columnas de seguridad
     * $this->name_entidad = 'sala_usuario';
     * $resultado = $this->columnas_seguridad();
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [sala_usuario_id] => stdClass Object (
     * //         [name_entidad] => sala_usuario
     * //         [name_campo] => id
     * //     ),
     * //     [plaza_id] => stdClass Object (
     * //         [name_entidad] => plaza
     * //         [name_campo] => id
     * //     )
     * // )
     * ```
     */
    private function columnas_seguridad(): stdClass
    {
        // Crear el objeto que contendrá las columnas de seguridad
        $columnas = new stdClass();

        // Definir la columna relacionada con sala_usuario
        $columnas->sala_usuario_id = new stdClass();
        $columnas->sala_usuario_id->name_entidad = $this->name_entidad;
        $columnas->sala_usuario_id->name_campo = 'id';

        // Definir la columna relacionada con plaza
        $columnas->plaza_id = new stdClass();
        $columnas->plaza_id->name_entidad = 'plaza';
        $columnas->plaza_id->name_campo = 'id';

        // Retornar las columnas de seguridad
        return $columnas;
    }


    /**
     * EM3
     * Genera un objeto con parámetros de seguridad para construir uniones (joins) en consultas SQL.
     *
     * Esta función devuelve un objeto que define las relaciones entre tablas para aplicar restricciones de seguridad.
     * Cada relación incluye el nombre de la tabla derecha (`right`) y un alias para la tabla izquierda (`ren_left`).
     *
     * @return stdClass Devuelve un objeto con los parámetros de seguridad. Cada propiedad es un conjunto de parámetros
     *                  que incluye:
     *                  - `right`: El nombre de la tabla derecha en la relación.
     *                  - `ren_left`: El alias de la tabla izquierda en la relación.
     *
     * @example
     * ```php
     * // Ejemplo: Obtener los parámetros de seguridad
     * $resultado = $this->params_seguridad();
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [sala] => stdClass Object (
     * //         [right] => sala_usuario
     * //         [ren_left] => sala
     * //     ),
     * //     [plaza] => stdClass Object (
     * //         [right] => sala
     * //         [ren_left] => plaza
     * //     ),
     * //     [usuario] => stdClass Object (
     * //         [right] => sala_usuario
     * //         [ren_left] => usuario
     * //     )
     * // )
     * ```
     */
    private function params_seguridad(): stdClass
    {
        // Crear el objeto que contendrá los parámetros de seguridad
        $params = new stdClass();

        // Definir los parámetros para la relación de sala
        $params->sala = new stdClass();
        $params->sala->right = 'sala_usuario';
        $params->sala->ren_left = 'sala';

        // Definir los parámetros para la relación de plaza
        $params->plaza = new stdClass();
        $params->plaza->right = 'sala';
        $params->plaza->ren_left = 'plaza';

        // Definir los parámetros para la relación de usuario
        $params->usuario = new stdClass();
        $params->usuario->right = 'sala_usuario';
        $params->usuario->ren_left = 'usuario';

        // Retornar los parámetros de seguridad
        return $params;
    }



    /**
     * EM3
     * Genera una consulta SQL para obtener permisos de un usuario basados en restricciones de seguridad.
     *
     * Esta función valida el ID del usuario, genera las condiciones de seguridad (`WHERE`),
     * las columnas relevantes (`SELECT`), las uniones (`JOIN`), y construye una consulta SQL completa.
     *
     * @param int $usuario_id El identificador del usuario. Debe ser mayor a 0.
     *
     * @return string|array Devuelve una cadena SQL con la consulta generada. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar consulta SQL para un usuario
     * $usuario_id = 123;
     * $this->name_entidad = 'sala_usuario';
     * $resultado = $this->sql_permiso($usuario_id);
     * echo $resultado;
     * // Resultado:
     * // SELECT sala_usuario.id AS sala_usuario_id, plaza.id AS plaza_id
     * // FROM sala_usuario AS sala_usuario
     * // LEFT JOIN sala AS sala ON sala.id = sala_usuario.sala_id
     * // LEFT JOIN plaza AS plaza ON plaza.id = sala.plaza_id
     * // WHERE sala_usuario.usuario_id = 123 AND sala_usuario.status = 'activo'
     *
     * // Ejemplo 2: Error por usuario_id inválido
     * $usuario_id = 0;
     * $resultado = $this->sql_permiso($usuario_id);
     * // Resultado:
     * // ['error' => 'Error $usuario_id debe ser mayor a 0', 'data' => 0]
     * ```
     */
    final public function sql_permiso(int $usuario_id)
    {
        // Validar que el ID del usuario sea mayor a 0
        if ($usuario_id <= 0) {
            return (new error())->error('Error $usuario_id debe ser mayor a 0', $usuario_id);
        }

        // Generar la cláusula WHERE de seguridad
        $st_where = $this->where_seguridad($usuario_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $statement', $st_where);
        }

        // Generar las columnas relevantes para la consulta
        $campos_sql = $this->campos_seguridad();
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campos_sql', $campos_sql);
        }

        // Obtener la entidad base con alias
        $sala_usuario_sql = (new sql())->entidad_base_as($this->name_entidad, $this->name_entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $sala_usuario_sql', $sala_usuario_sql);
        }

        // Obtener los parámetros de seguridad para las uniones
        $params = $this->params_seguridad();
        if (error::$en_error) {
            return (new error())->error('Error al obtener $params', $params);
        }

        // Generar las cláusulas JOIN
        $joins = (new sql())->joins($params);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $joins', $joins);
        }

        // Construir y retornar la consulta SQL completa
        return /** @lang MYSQL */ "SELECT $campos_sql FROM $sala_usuario_sql $joins WHERE $st_where";
    }



    /**
     * EM3
     * Genera una cláusula SQL `WHERE` de seguridad basada en un usuario específico.
     *
     * Esta función valida el ID del usuario, obtiene las condiciones de seguridad relacionadas con el usuario
     * utilizando `campos_seguridad_where`, y las convierte en una cláusula SQL `WHERE` válida.
     *
     * @param int $usuario_id El identificador del usuario. Debe ser mayor a 0.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `WHERE` generada. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula WHERE para un usuario válido
     * $usuario_id = 123;
     * $resultado = $this->where_seguridad($usuario_id);
     * echo $resultado;
     * // Resultado:
     * // sala_usuario.usuario_id = 123 AND sala_usuario.status = 'activo'
     *
     * // Ejemplo 2: Error por usuario_id inválido
     * $usuario_id = 0;
     * $resultado = $this->where_seguridad($usuario_id);
     * // Resultado:
     * // ['error' => 'Error $usuario_id debe ser mayor a 0', 'data' => 0]
     *
     * // Ejemplo 3: Error al generar las condiciones WHERE
     * $usuario_id = 123;
     * // Supongamos que ocurre un error en `statement_where`.
     * $resultado = $this->where_seguridad($usuario_id);
     * // Resultado:
     * // ['error' => 'Error al obtener $statement', 'data' => [...]]
     * ```
     */
    private function where_seguridad(int $usuario_id)
    {
        // Validar que el ID del usuario sea mayor a 0
        if ($usuario_id <= 0) {
            return (new error())->error('Error $usuario_id debe ser mayor a 0', $usuario_id);
        }

        // Obtener las condiciones de seguridad
        $campos_where = $this->campos_seguridad_where($usuario_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campos_where', $campos_where);
        }

        // Generar la cláusula SQL WHERE
        $st_where = (new sql())->statement_where($campos_where);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $statement', $st_where);
        }

        // Retornar la cláusula WHERE generada
        return $st_where;
    }


}
