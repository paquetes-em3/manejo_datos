<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;
use PDO;

class esquema_guardadito{

    private string $name_entidad = 'esquema_guardadito';

    /**
     * Genera una cadena SQL que selecciona los campos de la entidad con sus alias correspondientes.
     *
     * @param PDO $link Conexión PDO a la base de datos.
     * @return string|array La cadena SQL con los campos y sus alias, o un objeto de error en caso de fallo.
     */
    final public function campos_sql(PDO $link)
    {
        $campos = (new \desarrollo_em3\manejo_datos\data\entidades\esquema_guardadito())->campos($link);
        if(error::$en_error){
            return (new error())->error('Error al obtener $campos',$campos);
        }

        $sql = (new sql())->campos_sql_coma($campos, $this->name_entidad);
        if(error::$en_error){
            return (new error())->error('Error al generar $campos',$sql);
        }

        return $sql;


    }
    final public function sql_por_fecha_id(int $esquema_id, string $fecha, PDO $link)
    {

        $fecha = trim($fecha);
        if($fecha === ''){
            return (new error())->error('Error $fecha esta vacia',$fecha);
        }
        if($esquema_id <= 0){
            return (new error())->error('Error $esquema_id debe ser mayor a 0', $esquema_id);
        }

        $sql_extra = (new esquema())->sql_extra_esquema_guardadito($fecha);
        if(error::$en_error) {
            return (new error())->error('Error al obtener $sql_extra', $sql_extra);
        }

        $campos_esquema = (new esquema())->campos_sql($link);
        if(error::$en_error) {
            return (new error())->error('Error al obtener $campos_esquema', $campos_esquema);
        }

        $campos_esquema_guardadito = $this->campos_sql($link);
        if(error::$en_error) {
            return (new error())->error('Error al obtener $campos_esquema_guardadito',
                $campos_esquema_guardadito);
        }

        $campos_full = "$campos_esquema_guardadito,$campos_esquema";

        $esquema_guardadito_ent = (new sql())->entidad_base_as('esquema_guardadito',
            'esquema_guardadito');
        if(error::$en_error) {
            return (new error())->error('Error al obtener $esquema_guardadito_ent', $esquema_guardadito_ent);
        }

        $esquema_ent = (new sql())->entidad_base_as('esquema','esquema');
        if(error::$en_error) {
            return (new error())->error('Error al obtener $esquema_ent', $esquema_ent);
        }

        $on_esquema = (new sql())->on_para_join('esquema','esquema_guardadito','',
            '');
        if(error::$en_error) {
            return (new error())->error('Error al obtener $on_esquema', $on_esquema);
        }

        $st_esquema_id = (new sql())->sentencia_equals('esquema.id',$esquema_id);
        if(error::$en_error) {
            return (new error())->error('Error al obtener $st_esquema_id', $st_esquema_id);
        }

        $st_status = (new sql())->wh_status_act('esquema_guardadito');
        if(error::$en_error) {
            return (new error())->error('Error al obtener $st_status', $st_status);
        }

        $where = "$st_esquema_id AND $st_status $sql_extra";

        return "SELECT $campos_full FROM $esquema_guardadito_ent LEFT JOIN $esquema_ent ON $on_esquema WHERE $where";

    }


}
