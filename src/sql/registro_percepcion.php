<?php
namespace desarrollo_em3\manejo_datos\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;
use PDO;
use stdClass;

class registro_percepcion{

    /**
     * TRASLADADO
     * Inicializa un array con los campos base para una entidad de empleado.
     *
     * La función genera un array de campos relacionados con un empleado, incluyendo su ID, nombre completo,
     * y otros campos relacionados con el periodo de pago y la periodicidad. Si el nombre de la entidad de empleado
     * está vacío, devuelve un error.
     *
     * @param string $entidad_empleado El nombre de la entidad de empleado.
     *
     * @return array Devuelve un array con los campos base para la entidad de empleado.
     *                     Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function init_campos_base(string $entidad_empleado): array
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacio', $entidad_empleado);
        }
        return array("registro_percepcion.id", "$entidad_empleado.id", "$entidad_empleado.nombre_completo",
            "periodo_pago.id", "periodo_pago.fecha_inicial", "periodo_pago.fecha_final", "periodicidad_pago.id",
            "periodicidad_pago.n_elementos");
    }

    /**
     * TRASLADADO
     * Genera las cláusulas `JOIN` SQL necesarias para relacionar entidades específicas.
     *
     * Esta función define una serie de relaciones entre entidades mediante un arreglo de parámetros
     * y utiliza la clase `sql` para generar las cláusulas `JOIN` correspondientes.
     *
     * @param string $entidad_empleado Nombre de la entidad (tabla) asociada a los empleados.
     *                                 Por ejemplo: `'empleados'`.
     *
     * @return string|array Devuelve una cadena con las cláusulas `JOIN` generadas si el proceso es exitoso.
     *                      En caso de error, devuelve un arreglo con información detallada del fallo.
     *
     * Ejemplo de uso:
     * ```php
     * $entidad_empleado = 'empleados';
     * $joins = $this->joins($entidad_empleado);
     * if (isset($joins['error'])) {
     *     echo $joins['error'];
     * } else {
     *     echo $joins; // Cláusulas JOIN generadas.
     * }
     * ```
     *
     * Salida esperada (ejemplo):
     * ```sql
     * JOIN empleados ON empleados.id = registro_percepcion.empleado_id
     * JOIN periodo_pago ON periodo_pago.id = registro_percepcion.periodo_pago_id
     * JOIN periodicidad_pago ON periodicidad_pago.id = periodo_pago.periodicidad_pago_id
     * ```
     *
     * Errores posibles:
     * - `['error' => 'Error generar join', 'detalle' => $result]` si falla la generación de las cláusulas `JOIN`.
     */
    private function joins(string $entidad_empleado) {

        $params = array();
        $params[0] = new stdClass();
        $params[0]->entidad = "$entidad_empleado";
        $params[0]->entidad_right = 'registro_percepcion';

        $params[1] = new stdClass();
        $params[1]->entidad = 'periodo_pago';
        $params[1]->entidad_right = 'registro_percepcion';

        $params[2] = new stdClass();
        $params[2]->entidad = 'periodicidad_pago';
        $params[2]->entidad_right = 'periodo_pago';

        $result = (new sql())->genera_join_replace($params);
        if(error::$en_error){
            return (new error())->error('Error generar join', $result);
        }

        return $result;
    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener información detallada de un registro de percepción específico.
     *
     * Esta función construye dinámicamente una consulta SQL con los campos, `JOINs`, y filtros necesarios
     * para obtener un registro de percepción identificado por su ID.
     *
     * @param string $entidad_empleado Nombre de la entidad (tabla) asociada al empleado.
     *                                 Ejemplo: `'empleados'`.
     * @param int $registro_percepcion_id ID del registro de percepción que se desea obtener.
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada si el proceso es exitoso.
     *                      En caso de error, devuelve un arreglo con información detallada del fallo.
     *
     * Ejemplo de uso:
     * ```php
     * $entidad_empleado = 'empleados';
     * $registro_percepcion_id = 123;
     * $sql = $this->obten_registro_percepcion($entidad_empleado, $registro_percepcion_id);
     * if (isset($sql['error'])) {
     *     echo $sql['error'];
     * } else {
     *     echo $sql; // Consulta SQL generada
     * }
     * ```
     *
     * Salida esperada (ejemplo):
     * ```sql
     * SELECT empleados.id AS empleado_id, empleados.nombre AS empleado_nombre, ...
     * FROM registro_percepcion AS registro_percepcion
     * JOIN empleados ON empleados.id = registro_percepcion.empleado_id
     * JOIN periodo_pago ON periodo_pago.id = registro_percepcion.periodo_pago_id
     * JOIN periodicidad_pago ON periodicidad_pago.id = periodo_pago.periodicidad_pago_id
     * WHERE registro_percepcion.id = 123
     * ```
     *
     * @throws error Si `$registro_percepcion_id` es menor o igual a 0.
     * @throws error Si `$entidad_empleado` está vacío.
     * @throws error Si ocurre un fallo en la generación de los campos base, campos SQL, o `JOINs`.
     */
    final public function obten_registro_percepcion(string $entidad_empleado, int $registro_percepcion_id) {
        if($registro_percepcion_id <= 0){
            return (new error())->error('Error $registro_percepcion_id debe ser mayor a 0',
                $registro_percepcion_id);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacio', $entidad_empleado);
        }

        $campos_base = $this->init_campos_base($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error generar $campos_base', $campos_base);
        }

        $campos = (new sql())->campos_sql_puros_with_as($campos_base);
        if(error::$en_error){
            return (new error())->error('Error generar $campos', $campos);
        }

        $from = 'registro_percepcion AS registro_percepcion';
        $joins = $this->joins($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error generar $joins', $joins);
        }

        $lj_from = $from . ' ' . $joins;

        $where = "registro_percepcion.id = $registro_percepcion_id";
        return /** @lang MYSQL */  "SELECT $campos FROM $lj_from WHERE $where";
    }

}