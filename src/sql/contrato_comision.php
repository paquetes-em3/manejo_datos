<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;

class contrato_comision{

    /**
     * FIN
     * Genera una consulta SQL para obtener la cantidad de contratos dobles de un empleado en un periodo específico.
     *
     * Esta función genera una consulta SQL para calcular la cantidad de contratos dobles (productos con la descripción 'PLAN DOBLE')
     * asociados a un empleado (identificado por $ohem_id) en un periodo específico (definido por $fecha_inicial y $fecha_final).
     * Verifica que las fechas no estén vacías y que el ID del empleado sea mayor a 0 antes de generar la consulta.
     *
     * @param string $fecha_final La fecha final del periodo. No debe estar vacía.
     * @param string $fecha_inicial La fecha inicial del periodo. No debe estar vacía.
     * @param int $ohem_id El ID del empleado. Debe ser mayor a 0.
     *
     * @return string|array La consulta SQL generada. Si alguno de los parámetros no es válido, devuelve un array con el mensaje de error correspondiente.
     */
    final public function contrato_doble(string $fecha_final, string $fecha_inicial, int $ohem_id)
    {
        $fecha_inicial = trim($fecha_inicial);
        if($fecha_inicial === ''){
            return (new error())->error('Error fecha_inicial esta vacia',$fecha_inicial);
        }
        $fecha_final = trim($fecha_final);
        if($fecha_final === ''){
            return (new error())->error('Error $fecha_final esta vacia',$fecha_final);
        }
        if($ohem_id <= 0){
            return (new error())->error('Error $ohem_id debe ser mayor a 0',$ohem_id);
        }

        $cantidad = "SUM(IF(producto.descripcion LIKE '%PLAN DOBLE%', 2, 1) ) AS 'cantidad'";
        $on_contrato_id = "contrato.id = contrato_comision.contrato_id";
        $on_contrato_doc_date = "contrato.DocDate IS NOT NULL";

        $join_producto = "producto ON contrato.producto_id = producto.id";

        $where_ohem = "contrato_comision.ohem_id = $ohem_id";
        $where_fecha = "contrato.DocDate BETWEEN '$fecha_inicial' AND '$fecha_final'";
        $join_contrato = "contrato ON $on_contrato_id AND $on_contrato_doc_date";
        $where = "WHERE $where_ohem AND $where_fecha";
        $joins = "INNER JOIN $join_contrato LEFT JOIN $join_producto";

        return /** @lang MYSQL */ "SELECT $cantidad  FROM contrato_comision $joins $where";

    }
}
