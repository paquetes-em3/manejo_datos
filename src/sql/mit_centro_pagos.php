<?php
namespace desarrollo_em3\manejo_datos\sql;




use desarrollo_em3\error\error;

class mit_centro_pagos{

    final public function obten_mit_pagos_referencia(
        int $contrato_id, int $ejercicio, int $numero_semana, string $state){

        if($contrato_id <= 0){
            return (new error())->error('Error contrato_id debe ser mayor a 0',$contrato_id);
        }

        if($ejercicio <= 0){
            return (new error())->error('Error $ejercicio debe ser mayor a 0',$ejercicio);
        }

        if($numero_semana <= 0){
            return (new error())->error('Error $numero_semana debe ser mayor a 0',$numero_semana);
        }
        $state = trim($state);

        $mit_centro_pagos_id = "mit_centro_pagos.id AS mit_centro_pagos_id";
        $mit_errores_codigo = "mit_errores.codigo  AS mit_errores_codigo";
        $mit_errores_descripcion = "mit_errores.descripcion  AS mit_errores_descripcion";

        $mit_centro_pagos = "mit_centro_pagos AS mit_centro_pagos";
        $contrato = "contrato AS contrato";
        $plaza = "plaza AS plaza";
        $mit_errores = "mit_errores AS mit_errores";

        $on_cont_mit = "contrato.id = mit_centro_pagos.contrato_id";
        $on_pla_cont = "plaza.id = contrato.plaza_id";
        $on_mit = "mit_errores.codigo = mit_centro_pagos.cd_response";

        $wh_contrato = "contrato.id = '$contrato_id'";
        $wh_ejercicio = "mit_centro_pagos.ejercicio = '$ejercicio'";
        $wh_sem = "mit_centro_pagos.numero_semana = '$numero_semana'";
        $wh_rsp = "mit_centro_pagos.response = '$state'";

        $lf_contrato = "$contrato ON $on_cont_mit";
        $lf_plaza = "$plaza ON $on_pla_cont";
        $lf_mit = "$mit_errores ON $on_mit";
        $from = "$mit_centro_pagos LEFT JOIN $lf_contrato LEFT JOIN $lf_plaza LEFT JOIN $lf_mit";

        $campos = "$mit_centro_pagos_id, $mit_errores_codigo, $mit_errores_descripcion";

        $where = "$wh_contrato AND $wh_ejercicio AND $wh_sem AND $wh_rsp";

        return /** @lang MYSQL */ "SELECT $campos FROM $from WHERE $where";

    }


}
