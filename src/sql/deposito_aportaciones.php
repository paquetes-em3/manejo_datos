<?php
namespace desarrollo_em3\manejo_datos\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;

class deposito_aportaciones{

    private string $key_id_original = 'deposito_aportaciones.id';
    private string $name_entidad = 'deposito_aportaciones';

    /**
     * TRASLADADO
     * Actualiza la serie de un registro en la entidad correspondiente.
     *
     * Valida que los parámetros `$deposito_aportaciones_id` y `$serie_id` sean mayores a 0. Si las validaciones pasan,
     * genera una consulta SQL para actualizar el campo `serie_id` en la entidad definida por `$this->name_entidad`.
     *
     * @param int $deposito_aportaciones_id ID del depósito de aportaciones, debe ser mayor a 0.
     * @param int $serie_id ID de la serie a actualizar, debe ser mayor a 0.
     *
     * @return string|array Devuelve la consulta SQL generada para actualizar la serie, o un array de error en caso de falla.
     */
    final public function actualiza_serie(int $deposito_aportaciones_id, int $serie_id)
    {
        if($deposito_aportaciones_id <= 0){
            return(new error())->error('Error $deposito_aportaciones_id debe ser mayor a 0',
                $deposito_aportaciones_id);
        }
        if($serie_id <= 0){
            return(new error())->error('Error $serie_id debe ser mayor a 0',$serie_id);
        }
        $serie_id_set = "serie_id = $serie_id";
        $where = "$this->key_id_original = $deposito_aportaciones_id";
        return /** @lang MYSQL */ "UPDATE $this->name_entidad SET $serie_id_set WHERE $where";
    }

    /**
     * TRASLADADO
     * Genera un array con las columnas necesarias para la consulta de depósito de aportaciones.
     *
     * Esta función recibe el nombre de la entidad del empleado y valida que no esté vacío.
     * Si la validación es exitosa, retorna un array con las columnas requeridas para la consulta,
     * que incluyen información sobre la referencia del depósito, fecha, montos relacionados,
     * descripción de la plaza, nombre completo del empleado, y datos del banco, entre otros.
     *
     * @param string $entidad_empleado El nombre de la entidad del empleado. No debe estar vacío.
     *
     * @return array Retorna un array con las columnas requeridas para la consulta si el nombre de la entidad es válido.
     *                     Si ocurre un error, retorna un array con los detalles del fallo.
     */
    private function columnas_deposito_aportacion(string $entidad_empleado): array
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return(new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        return array('deposito_aportaciones.referencia','deposito_aportaciones.fecha','pago_corte.monto_total',
            'pago_corte.monto_depositado','pago_corte.monto_por_depositar','plaza.descripcion',
            $entidad_empleado.'.nombre_completo','banco.razon_social', 'pago_corte.id','plaza.id',
            'deposito_aportaciones.cuenta_empresa_id','deposito_aportaciones.monto_depositado',
            'deposito_aportaciones.id','deposito_aportaciones.validado','banco.descripcion',
            'deposito_aportaciones.serie_id');

    }


    /**
     * TRASLADADO
     * Genera una cadena SQL con las columnas necesarias para la consulta de depósito de aportaciones.
     *
     * Esta función recibe el nombre de la entidad del empleado y valida que no esté vacío. Si la validación es exitosa,
     * llama a la función `columnas_deposito_aportacion` para obtener un array de columnas. Luego, genera una cadena SQL
     * con las columnas utilizando la función `campos_sql_puros_with_as` de la clase `sql`. Si ocurre algún error en el
     * proceso, la función retorna un array de error con los detalles.
     *
     * @param string $entidad_empleado El nombre de la entidad del empleado. No debe estar vacío.
     *
     * @return string|array Retorna una cadena SQL con las columnas necesarias si el proceso es exitoso.
     *                      Si ocurre un error, retorna un array con los detalles del fallo.
     */
    private function columnas_deposito_aportacion_sql(string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return(new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $columnas = $this->columnas_deposito_aportacion($entidad_empleado);
        if(error::$en_error){
            return(new error())->error('Error al obtener $columnas',$columnas);
        }

        $campos_sql = (new sql())->campos_sql_puros_with_as($columnas);
        if(error::$en_error){
            return(new error())->error('Error al obtener $campos_sql',$campos_sql);
        }

        return $campos_sql;

    }


    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener una fila específica de la tabla `deposito_aportaciones`.
     *
     * Esta función valida que el ID de `deposito_aportaciones` sea mayor a 0 y que el nombre de la entidad del empleado no esté vacío.
     * Luego, genera las columnas necesarias para la consulta mediante la función `columnas_deposito_aportacion_sql` y los `LEFT JOINs`
     * mediante la función `joins_pago_aportaciones`. También genera la cláusula `WHERE` basada en el ID proporcionado.
     *
     * Si ocurre algún error en cualquiera de estos pasos, la función retorna un array de error con los detalles.
     *
     * @param int $deposito_aportaciones_id El ID del depósito de aportaciones. Debe ser mayor a 0.
     * @param string $entidad_empleado El nombre de la entidad del empleado. No debe estar vacío.
     *
     * @return string|array Retorna una cadena SQL con la consulta generada si el proceso es exitoso.
     *                      Si ocurre un error, retorna un array con los detalles del fallo.
     */
    final public function deposito_aportaciones_row(int $deposito_aportaciones_id, string $entidad_empleado)
    {
        if($deposito_aportaciones_id <= 0){
            return(new error())->error('Error $deposito_aportaciones_id es menor a 0',
                $deposito_aportaciones_id);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return(new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }

        $campos_sql = $this->columnas_deposito_aportacion_sql($entidad_empleado);
        if(error::$en_error){
            return(new error())->error('Error al obtener $campos_sql',$campos_sql);
        }

        $lfs = $this->joins_pago_aportaciones($entidad_empleado);
        if(error::$en_error){
            return(new error())->error('Error al obtener $lfs',$lfs);
        }
        $lfs_sql = (new sql())->left_joins_sql($lfs);
        if(error::$en_error){
            return(new error())->error('Error al obtener $lfs_sql',$lfs_sql);
        }

        $where_id = (new sql())->where_id($this->name_entidad,$deposito_aportaciones_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $where_id',$where_id);
        }

        $from = "$this->name_entidad AS $this->name_entidad";

        return /** @lang MYSQL */ "SELECT $campos_sql FROM $from $lfs_sql WHERE $where_id";

    }


    /**
     * TRASLADADO
     * Genera el sql de generacion de un filtro con status = activo y id de pago corte igual al identificador del pago
     * corte
     * @param int $pago_corte_id Identificador de la entidad pago_corte.id
     * @return array|string Un string con el SQL generado o un error en forma de array
     */
    final public function depositos_por_corte_filtro(int $pago_corte_id)
    {
        if($pago_corte_id <= 0){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0',$pago_corte_id);
        }
        return "$this->name_entidad.pago_corte_id = $pago_corte_id AND $this->name_entidad.status = 'activo'";

    }

    /**
     * TRASLADADO
     * Genera un array de uniones (joins) para pagos y aportaciones basado en la entidad de empleado.
     *
     * Esta función valida que el nombre de la entidad de empleado no esté vacío y luego construye un array de
     * uniones (joins) que describe las relaciones entre diferentes tablas relacionadas con pagos y aportaciones.
     *
     * @param string $entidad_empleado Nombre de la entidad de empleado que se utilizará en las uniones.
     *
     * @return array Retorna un array que contiene las uniones (joins) o un array de error si la validación falla.
     */
    private function joins_pago_aportaciones(string $entidad_empleado): array
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return(new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $lfs['pago_corte'] = 'deposito_aportaciones';
        $lfs['plaza'] = 'pago_corte';
        $lfs[$entidad_empleado] = 'pago_corte';
        $lfs['cuenta_empresa'] = 'deposito_aportaciones';
        $lfs['banco'] = 'cuenta_empresa';
        return $lfs;

    }

    final public function obten_depositos_por_corte(int $pago_corte_id) {
        if($pago_corte_id <= 0){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0',$pago_corte_id);
        }
        
        $campos_base = array(
            'deposito_aportaciones.id', 'deposito_aportaciones.pago_corte_id', 'deposito_aportaciones.referencia', 'banco.razon_social',
            'cuenta_empresa.cuenta', 'deposito_aportaciones.monto_depositado', 'deposito_aportaciones.validado', 'cuenta_empresa.id',
            'plaza.id','cuenta_empresa.alias','serie.codigo','banco.descripcion'
        );

        $campos_format = (new sql())->campos_sql_puros_with_as($campos_base);
        if(error::$en_error){
            return (new error())->error('Error al generar $campos', $campos_format);
        }
        $campos = $campos_format;
        $from = "deposito_aportaciones AS deposito_aportaciones ";
        $joins = "LEFT JOIN pago_corte AS pago_corte ON pago_corte.id = deposito_aportaciones.pago_corte_id ";
        $joins .= "LEFT JOIN cuenta_empresa AS cuenta_empresa ON cuenta_empresa.id = deposito_aportaciones.cuenta_empresa_id ";
        $joins .= "LEFT JOIN ohem AS ohem ON ohem.id = pago_corte.ohem_id ";
        $joins .= "LEFT JOIN plaza AS plaza ON plaza.id = pago_corte.plaza_id ";
        $joins .= "LEFT JOIN banco AS banco ON banco.id = cuenta_empresa.banco_id ";
        $joins .= "LEFT JOIN serie AS serie ON serie.id = deposito_aportaciones.serie_id ";
        $lj_from = $from . " " . $joins;
        $where = "deposito_aportaciones.pago_corte_id = $pago_corte_id";
        return /** @lang MYSQL */ "SELECT $campos FROM $lj_from WHERE $where";
    }

    final public function series_cuenta_plaza(int $cuenta_empresa_id, int $plaza_id): string
    {
        $rel_serie_id_cmp = "rel_serie.id AS rel_serie_id";
        $serie_id_cmp = "rel_serie.serie_id AS serie_id";
        $cuenta_empresa_id_cmp = "cuenta_empresa.id AS cuenta_empresa_id";

        $cmps = "$rel_serie_id_cmp, $serie_id_cmp, $cuenta_empresa_id_cmp";

        $rel_serie_tb = "rel_serie AS rel_serie";
        $cuenta_empresa_tb = "cuenta_empresa AS cuenta_empresa";
        $on = "cuenta_empresa.id = rel_serie.cuenta_empresa_id";

        $from = "$rel_serie_tb LEFT JOIN $cuenta_empresa_tb ON $on";

        $wh_plaza = "cuenta_empresa.plaza_id = $plaza_id";
        $wh_cuenta_empresa = "cuenta_empresa.id = $cuenta_empresa_id";
        return /** @lang MYSQL */ "SELECT $cmps FROM $from WHERE $wh_plaza AND $wh_cuenta_empresa";

    }

    final public function sum_deposito_por_pago_corte(string $entidad_empleado, int $pago_corte_id) {

        $monto_entregado = "IFNULL(SUM( deposito_aportaciones.monto_depositado ),0) AS monto_entregado";
        $campos = "pago_corte.validado AS pago_corte_validado,$monto_entregado";
        $on_cuenta_empresa = "cuenta_empresa.id = deposito_aportaciones.cuenta_empresa_id";

        $from = "pago_corte AS pago_corte";

        $id_empleado = $entidad_empleado . "_id";
        $lj_pago_corte = "deposito_aportaciones AS deposito_aportaciones ON deposito_aportaciones.pago_corte_id = pago_corte.id";
        $lj_cuenta_empresa = "cuenta_empresa AS cuenta_empresa ON $on_cuenta_empresa";
        $lj_empleado = "$entidad_empleado ON $entidad_empleado.id = pago_corte.$id_empleado";
        $lj_plaza = "plaza AS plaza ON plaza.id = pago_corte.plaza_id";
        $lj_serie = " serie AS serie ON serie.id = deposito_aportaciones.serie_id";

        $lf_from = "$from LEFT JOIN $lj_pago_corte ";
        $lf_from .= "LEFT JOIN $lj_cuenta_empresa ";
        $lf_from .= "LEFT JOIN $lj_empleado ";
        $lf_from .= "LEFT JOIN $lj_plaza ";
        $lf_from .= "LEFT JOIN $lj_serie ";

        $where = "pago_corte.id = $pago_corte_id";

        return /** @lang MYSQL */ "SELECT $campos FROM $lf_from WHERE $where";
    }
}
