<?php
namespace desarrollo_em3\manejo_datos\sql;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;
use stdClass;

class asistencia_app{

    /**
     * TRASLADADO
     * Actualiza la distancia del jefe en el registro de asistencia.
     *
     * Esta función genera una sentencia SQL para actualizar la columna `distancia_jefe` en un registro de asistencia
     * específico, basado en el ID del registro de asistencia proporcionado.
     *
     * @param int $asistencia_app_id ID del registro de asistencia en la aplicación. Debe ser mayor a 0.
     * @param float $distancia_jefe La distancia calculada entre el empleado y su jefe inmediato.
     *
     * @return string|array Sentencia SQL para ejecutar la actualización de la distancia del jefe en la tabla `asistencia_app`.
     * Si el ID es inválido, devuelve un array con los detalles del error.
     */
    final public function actualiza_distancia(int $asistencia_app_id, float $distancia_jefe)
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id es menor a 0',$asistencia_app_id);
        }
        $set =  "distancia_jefe = $distancia_jefe";
        return /** @lang MYSQL */ "UPDATE asistencia_app SET $set WHERE id = $asistencia_app_id";
    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener el ID de un registro de asistencia en la aplicación basado en un empleado y una fecha.
     *
     * Esta función valida los parámetros de asistencia (`$entidad_empleado`, `$fecha` y `$ohem_id`) utilizando `valida_asistencia`.
     * Si los datos son válidos, genera una consulta SQL para obtener el ID del registro de asistencia de la tabla `asistencia_app`
     * en función de la entidad del empleado, su ID y la fecha proporcionada. Si la validación falla, devuelve un array con los
     * detalles del error.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param string $fecha Fecha de la asistencia (en formato 'YYYY-MM-DD').
     * @param int $ohem_id ID del empleado (referenciado en la tabla `asistencia_app`).
     *
     * @return string|array Devuelve una consulta SQL que obtiene el ID de asistencia o un array de error si ocurre algún problema.
     */
    final public function asistencia_app_id(string $entidad_empleado, string $fecha, int $ohem_id)
    {
        $valida = $this->valida_asistencia($entidad_empleado,$fecha,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al validar asistencia',$valida);
        }

        $key_id = $entidad_empleado.'_id';
        $compare = "asistencia_app.$key_id = $ohem_id";
        $key_id_base = "asistencia_app.id AS asistencia_app_id";
        $where = "$compare AND asistencia_app.fecha = '$fecha'";
        return /** @lang MYSQL */ "SELECT $key_id_base FROM asistencia_app WHERE $where";

    }

    final public function asistencia_conteo_dias(string $entidad_empleado, string $fecha_inicial,
                                              string $fecha_final, int $ohem_id, string $status) {
        if($ohem_id <= 0){
            return (new error())->error('Error $ohem_id es menor a 0',$ohem_id);
        }

        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }

        $campo = "COUNT(*) AS dias";
        $from = "asistencia_app AS asistencia_app";
        $where = "asistencia_app.$entidad_empleado" . "_id = $ohem_id ";
        $w_fecha = $this->where_fecha($fecha_final,$fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al obtener $w_fecha', $w_fecha);
        }
        $where .= "AND asistencia_app.asistencia = '$status' AND ";
        $where .= $w_fecha;
        return /** @lang    MYSQL */ "SELECT $campo FROM $from WHERE $where";
    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener las asistencias de empleados dentro de un período específico.
     *
     * La función valida que los parámetros proporcionados no estén vacíos, incluyendo el campo de fecha de ingreso,
     * la entidad del empleado, y las fechas de inicio y fin del período. Luego, genera una consulta SQL que incluye
     * los campos del reporte, las uniones necesarias (joins) y una cláusula WHERE que filtra las asistencias
     * dentro del rango de fechas especificado. Si ocurre algún error durante el proceso, devuelve un array con
     * los detalles del fallo.
     *
     * @param string $campo_fecha_ingreso El nombre del campo de la fecha de ingreso del empleado.
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     * @param string $fecha_final La fecha final del período (en formato `YYYY-MM-DD`).
     * @param string $fecha_inicial La fecha inicial del período (en formato `YYYY-MM-DD`).
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada para obtener las asistencias.
     *                      Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    final public function asistencias_periodo(string $campo_fecha_ingreso, string $entidad_empleado,
                                              string $fecha_final, string $fecha_inicial)
    {

        $valida = $this->valida_data_asistencia($campo_fecha_ingreso, $entidad_empleado,$fecha_final,$fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al validar datos', $valida);
        }

        $campos = $this->campos_reporte($campo_fecha_ingreso,$entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al obtener $campos', $campos);
        }
        $from = $this->genera_from($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al obtener $from', $from);
        }
        $w_fecha = $this->where_fecha($fecha_final,$fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al obtener $w_fecha', $w_fecha);
        }

        $order = "asistencia_app.fecha ASC";

        return /** @lang MYSQL */ "SELECT $campos FROM $from WHERE $w_fecha ORDER BY $order";

    }

    /**
     * TRASLADADO
     * Obtiene todas las asistencias correspondientes a una fecha específica.
     *
     * La función valida que la fecha proporcionada no esté vacía y luego genera una consulta SQL para obtener
     * todos los registros de asistencia de la tabla `asistencia_app` que coincidan con esa fecha.
     * Si la fecha está vacía, se devuelve un array de error con los detalles del fallo.
     *
     * @param string $fecha La fecha en formato `YYYY-MM-DD` para la cual se quieren obtener las asistencias.
     *
     * @return string|array Devuelve la consulta SQL que selecciona las asistencias por fecha.
     *                      Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    final public function asistencias_por_dias(string $fecha)
    {
        $fecha = trim($fecha);
        if($fecha === ''){
            return (new error())->error('Error fecha esta vacia',$fecha);
        }
        return /** @lang MYSQL */ "SELECT *FROM asistencia_app WHERE fecha = '" . $fecha . "'";

    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener las asistencias recientes de los últimos dos días, que aún estén inactivas.
     *
     * La función crea una consulta SQL para seleccionar las asistencias de la tabla `asistencia_app` donde el estado de la
     * asistencia es 'inactivo' y la fecha es dentro de los últimos dos días. Los resultados se ordenan por fecha descendente
     * y se aplican los parámetros de límite y desplazamiento (`LIMIT` y `OFFSET`).
     *
     * @param int $offset El número de registros a omitir (desplazamiento) en la consulta SQL.
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada para obtener las asistencias recientes.
     */
    final public function asistencias_recientes(int $offset)
    {
        if($offset < 0){
            return (new error())->error('Error $offset debe ser mayor a 0',$offset);
        }
        $hoy = date("Y-m-d");
        $hace_dos_dias =  date("Y-m-d",strtotime($hoy."- 2 days"));

        $wh_asistencia = "asistencia = 'inactivo'";
        $wh_fecha = "fecha>='$hace_dos_dias'";
        $order = "asistencia_app.fecha";
        $limit = "LIMIT 10";
        $offset_sql = "OFFSET  $offset";
        $where = "$wh_asistencia AND $wh_fecha";

        return /** @lang MYSQL */ "SELECT *FROM asistencia_app WHERE $where ORDER BY $order DESC $limit $offset_sql";
    }

    /**
     * TRASLADADO
     * Genera una expresión SQL para obtener el ID de un empleado con alias basado en la entidad proporcionada.
     *
     * La función valida que el nombre de la entidad del empleado no esté vacío. Luego genera una expresión SQL
     * que selecciona el campo `id` de la entidad del empleado y lo asigna a un alias basado en el nombre de la entidad.
     * Si la entidad está vacía, se devuelve un array con los detalles del error.
     *
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     *
     * @return string|array Devuelve una cadena con la expresión SQL para seleccionar el ID del empleado con alias.
     *                      Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function campo_empleado_id(string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }
        $key_empleado_id = $entidad_empleado.'_id';
        return "$entidad_empleado.id AS $key_empleado_id";

    }

    /**
     * TRASLADADO
     * Genera un conjunto de campos SQL en formato puro para un reporte basado en los datos de asistencia de empleados.
     *
     * La función valida que los parámetros `$campo_fecha_ingreso` y `$entidad_empleado` no estén vacíos,
     * luego utiliza la función `campos_rpt` para generar los campos del reporte y convierte estos campos a su formato SQL puro.
     * Si ocurre algún error durante el proceso, se devuelve un array con los detalles del fallo.
     *
     * @param string $campo_fecha_ingreso El nombre del campo de la fecha de ingreso del empleado.
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     *
     * @return string|array Devuelve una cadena con los campos SQL en formato puro para el reporte.
     *                      Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function campos_reporte(string $campo_fecha_ingreso, string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }
        $campo_fecha_ingreso = trim($campo_fecha_ingreso);
        if($campo_fecha_ingreso === ''){
            return (new error())->error('Error $campo_fecha_ingreso esta vacia', $campo_fecha_ingreso);
        }

        $campos_data = $this->campos_rpt($campo_fecha_ingreso,$entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al obtener $campos_data', $campos_data);
        }

        $campos = (new sql())->campos_sql_puros((array)$campos_data);
        if(error::$en_error){
            return (new error())->error('Error al obtener $campos', $campos);
        }

        return $campos;

    }

    /**
     * TRASLADADO
     * Genera un conjunto de campos SQL para un reporte basado en los datos de asistencia de empleados.
     *
     * La función valida que los parámetros `$campo_fecha_ingreso` y `$entidad_empleado` no estén vacíos, y luego
     * construye un conjunto de campos SQL con alias para ser utilizados en un reporte de asistencia.
     * Si ocurre algún error durante el proceso de validación o la generación de los campos, devuelve un array con los detalles del fallo.
     *
     * @param string $campo_fecha_ingreso El nombre del campo de la fecha de ingreso del empleado.
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     *
     * @return stdClass|array Devuelve un objeto `stdClass` con los campos SQL generados para el reporte.
     *                        Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function campos_rpt(string $campo_fecha_ingreso, string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }
        $campo_fecha_ingreso = trim($campo_fecha_ingreso);
        if($campo_fecha_ingreso === ''){
            return (new error())->error('Error $campo_fecha_ingreso esta vacia', $campo_fecha_ingreso);
        }

        $empleado_id = $this->campo_empleado_id($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al obtener campo', $empleado_id);
        }

        $plaza = "plaza.descripcion AS plaza";
        $status = "$entidad_empleado.status AS status";
        $fecha_ingreso = "$entidad_empleado.$campo_fecha_ingreso AS fecha_ingreso";
        $fecha = "asistencia_app.fecha  AS fecha";
        $asistencia_app_id = "asistencia_app.id AS asistencia_app_id";
        $hora_entrada = "asistencia_app.hora_entrada AS hora_entrada";
        $asistencia = "asistencia_app.asistencia AS asistencia";
        $dia = "asistencia_app.dia AS dia";
        $nombre_completo = "$entidad_empleado.nombre_completo AS nombre_completo";
        $jefe_checo = "asistencia_app.jefe_checo AS jefe_checo";
        $checador = "asistencia_app.checada_en_sitio AS checador";
        $distancia_jefe = "asistencia_app.distancia_jefe AS distancia_jefe";
        $oficina_trabajo_id = "oficina_trabajo.id AS oficina_trabajo_id";
        $oficina_trabajo_descripcion = "oficina_trabajo.descripcion AS oficina_trabajo_descripcion";

        $campos = new stdClass();
        $campos->plaza = $plaza;
        $campos->status = $status;
        $campos->fecha_ingreso = $fecha_ingreso;
        $campos->fecha = $fecha;
        $campos->asistencia_app_id = $asistencia_app_id;
        $campos->hora_entrada = $hora_entrada;
        $campos->asistencia = $asistencia;
        $campos->dia = $dia;
        $campos->nombre_completo = $nombre_completo;
        $campos->jefe_checo = $jefe_checo;
        $campos->checador = $checador;
        $campos->distancia_jefe = $distancia_jefe;
        $campos->empleado_id = $empleado_id;
        $campos->oficina_trabajo_id = $oficina_trabajo_id;
        $campos->oficina_trabajo_descripcion = $oficina_trabajo_descripcion;

        return $campos;

    }


    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener el esquema más reciente de un empleado antes de una fecha límite específica.
     *
     * La función valida los parámetros de entrada (nombre del campo del esquema, entidad del empleado, entidad relacionada,
     * fecha límite e ID del empleado). Luego genera una consulta SQL que selecciona el esquema más reciente del empleado
     * antes de la fecha límite, realizando un LEFT JOIN entre las tablas del esquema y la entidad relacionada. Si ocurre
     * algún error durante el proceso, devuelve un array con los detalles del fallo.
     *
     * @param string $campo_esquema_name El nombre del campo del esquema que se desea obtener.
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     * @param string $entidad_rel_esquema El nombre de la entidad relacionada con el esquema.
     * @param string $fecha_limite La fecha límite hasta la cual se desea obtener el esquema del empleado (en formato `YYYY-MM-DD`).
     * @param int $ohem_id El ID del empleado, que debe ser mayor a 0.
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada para obtener el esquema del empleado.
     *                      Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    final public function empleado_esquema_fecha(
        string $campo_esquema_name, string $entidad_empleado, string $entidad_rel_esquema, string $fecha_limite, int $ohem_id)
    {

        $valida = $this->valida_data_asistencia_reg(
            $campo_esquema_name,$entidad_empleado,  $entidad_rel_esquema,$fecha_limite,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error validar datos', $valida);
        }

        $key_empleado_id = $entidad_empleado.'_id';
        $key_rel_id = $entidad_rel_esquema.'_id';
        $emp_esq_id_cmp = "$entidad_rel_esquema.id AS $key_rel_id";
        $esquema_name_cmp = "esquema.$campo_esquema_name AS esquema_name";
        $esquema_fecha_cmp = "$entidad_rel_esquema.fecha AS esquema_fecha";
        $fecha_cmp = "$entidad_rel_esquema.fecha";

        $empleado_esquema_tb = "$entidad_rel_esquema AS $entidad_rel_esquema";
        $esquema_tb = "esquema AS esquema";

        $campos = "$emp_esq_id_cmp, $esquema_name_cmp, $esquema_fecha_cmp";
        $wh_ohem = "$entidad_rel_esquema.$key_empleado_id = $ohem_id";
        $wh_fecha = "$entidad_rel_esquema.fecha <='$fecha_limite'";

        $on_esquema = "esquema.id = $entidad_rel_esquema.esquema_id";

        $from = "$empleado_esquema_tb LEFT JOIN $esquema_tb ON $on_esquema";
        $where = "$wh_ohem AND $wh_fecha";

        return /** @lang MYSQL */ "SELECT $campos FROM $from WHERE  $where ORDER BY $fecha_cmp DESC LIMIT 1 ";

    }

    /**
     * TRASLADADO
     * Genera la sentencia FROM y los LEFT JOINs para una consulta SQL, incluyendo las condiciones ON.
     *
     * La función valida que los parámetros `$entidad_empleado`, `$on_emp` y `$on_plaza` no estén vacíos. Luego obtiene
     * las sentencias FROM y genera los LEFT JOINs basados en las condiciones ON proporcionadas. Si ocurre algún error,
     * devuelve un array con los detalles del fallo.
     *
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     * @param string $on_emp La condición ON para unir la entidad del empleado con la tabla `asistencia_app`.
     * @param string $on_plaza La condición ON para unir la entidad del empleado con la tabla `plaza`.
     *
     * @return string|array Devuelve una cadena con la sentencia FROM y los LEFT JOINs generados.
     *                      Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function from(string $entidad_empleado, string $on_emp, string $on_plaza, string $on_oficina_trabajo)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $on_emp = trim($on_emp);
        if($on_emp === ''){
            return (new error())->error('Error $on_emp esta vacia',$on_emp);
        }
        $on_plaza = trim($on_plaza);
        if($on_plaza === ''){
            return (new error())->error('Error $on_plaza esta vacia',$on_plaza);
        }

        $on_oficina_trabajo = trim($on_oficina_trabajo);
        if($on_oficina_trabajo === ''){
            return (new error())->error('Error $on_oficina esta vacia',$on_oficina_trabajo);
        }

        $froms = $this->froms($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al obtener $froms', $froms);
        }
        $lf_emp = "$froms->empleado ON $on_emp";
        $lf_plaza = "$froms->plaza ON $on_plaza";
        $lf_oficina = "$froms->oficina_trabajo ON $on_oficina_trabajo";
        $lsf = "LEFT JOIN $lf_emp LEFT JOIN $lf_plaza LEFT JOIN $lf_oficina";
        return "$froms->asistencia $lsf";

    }

    /**
     * TRASLADADO
     * Genera las sentencias FROM para las uniones SQL basadas en la entidad del empleado.
     *
     * La función valida que el nombre de la entidad del empleado no esté vacío. Luego genera las sentencias FROM
     * para las tablas `asistencia_app`, la entidad del empleado y `plaza`, cada una con un alias correspondiente.
     * Si ocurre algún error, devuelve un array con los detalles del fallo.
     *
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     *
     * @return stdClass|array Devuelve un objeto `stdClass` que contiene las sentencias FROM generadas para las uniones SQL.
     *                        Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function froms(string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }

        $fr_asistencia = "asistencia_app AS asistencia_app";
        $fr_empleado = "$entidad_empleado AS $entidad_empleado";
        $fr_plaza = "plaza AS plaza";
        $fr_oficina_trabajo = "oficina_trabajo AS oficina_trabajo";
        $frm = new stdClass();
        $frm->asistencia = $fr_asistencia;
        $frm->empleado = $fr_empleado;
        $frm->plaza = $fr_plaza;
        $frm->oficina_trabajo = $fr_oficina_trabajo;

        return $frm;

    }

    /**
     * TRASLADADO
     * Genera la sentencia FROM completa con las uniones (joins) y las condiciones ON para una consulta SQL.
     *
     * La función valida que el nombre de la entidad del empleado no esté vacío, luego obtiene las condiciones ON
     * y genera la sentencia FROM completa, incluyendo los LEFT JOINs correspondientes. Si ocurre algún error
     * durante el proceso, devuelve un array con los detalles del fallo.
     *
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     *
     * @return string|array Devuelve una cadena con la sentencia FROM y los LEFT JOINs generados para la consulta SQL.
     *                      Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function genera_from(string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }

        $ons = $this->ons($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al obtener $ons', $ons);
        }
        $from = $this->from($entidad_empleado,$ons->empleado,$ons->plaza,$ons->oficina_trabajo);
        if(error::$en_error){
            return (new error())->error('Error al obtener $from', $from);
        }

        return $from;

    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener la checada de asistencia de un empleado en una fecha específica.
     *
     * Esta función valida que los parámetros proporcionados (entidad del empleado, fecha y ID) sean correctos
     * mediante la función `valida_asistencia`. Si la validación es exitosa, se genera una consulta SQL que busca
     * en la tabla `asistencia_app` el registro de asistencia del empleado correspondiente a la fecha especificada.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param string $fecha Fecha en la que se busca la checada de asistencia.
     * @param int $ohem_id ID del empleado en la entidad de empleados.
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada o un array de error si ocurre algún problema durante la validación.
     */
    final public function get_checada(string $entidad_empleado, string $fecha, int $ohem_id)
    {
        $valida = $this->valida_asistencia($entidad_empleado,$fecha,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al validar asistencia',$valida);
        }

        $key_id = $entidad_empleado.'_id';
        $compare = "asistencia_app.$key_id = $ohem_id";
        return /** @lang MYSQL */ "SELECT *FROM asistencia_app WHERE $compare AND asistencia_app.fecha = '$fecha'";

    }


    /**
     * TRASLADADO
     * Obtiene el número de checadas de un empleado en una fecha específica.
     *
     * Esta función valida los datos de asistencia del empleado y genera una consulta SQL para contar
     * el número de checadas del empleado en una fecha dada.
     *
     * @param string $entidad_empleado Nombre de la entidad del empleado. No debe estar vacía.
     * @param string $fecha Fecha en la que se busca el número de checadas del empleado. Debe estar en formato 'YYYY-MM-DD'.
     * @param int $ohem_id ID del empleado (ohem_id). Debe ser mayor a 0.
     *
     * @return string|array Devuelve una cadena SQL que cuenta el número de checadas del empleado en la fecha especificada.
     *                Si ocurre un error, devuelve un array con detalles del error.
     */
    final public function n_checadas(string $entidad_empleado, string $fecha, int $ohem_id)
    {
        $valida = $this->valida_asistencia($entidad_empleado,$fecha,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al validar asistencia',$valida);
        }

        $key_id = $entidad_empleado.'_id';
        $compare = "asistencia_app.$key_id = $ohem_id";
        $wh_fecha = "asistencia_app.fecha = '$fecha'";
        return /** @lang MYSQL */ "SELECT COUNT(*) AS n_checadas FROM asistencia_app WHERE $compare AND $wh_fecha";

    }

    /**
     * TRASLADADO
     * Genera las condiciones ON para las uniones (joins) SQL basadas en la entidad del empleado.
     *
     * La función valida que el nombre de la entidad del empleado no esté vacío. Luego genera las condiciones
     * ON para las uniones (joins) entre la entidad del empleado y la tabla `asistencia_app`, así como la entidad `plaza`.
     * Si ocurre algún error, devuelve un array con los detalles del fallo.
     *
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     *
     * @return stdClass|array Devuelve un objeto `stdClass` que contiene las condiciones ON para las uniones SQL.
     *                        Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function ons(string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $key_empleado_id = $entidad_empleado.'_id';
        $on_emp = "$entidad_empleado.id = asistencia_app.$key_empleado_id";
        $on_plaza = "plaza.id = $entidad_empleado.plaza_id";
        $on_oficina_trabajo = "oficina_trabajo.id = $entidad_empleado.oficina_trabajo_id";

        $ons = new stdClass();
        $ons->empleado = $on_emp;
        $ons->plaza = $on_plaza;
        $ons->oficina_trabajo = $on_oficina_trabajo;

        return $ons;

    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para obtener los subordinados de un empleado en un registro de asistencia.
     *
     * Esta función valida que el nombre de la entidad del empleado no esté vacío. Luego, utiliza los datos del
     * registro de asistencia para construir una consulta SQL que busca los subordinados del empleado basándose en
     * el ID del jefe inmediato y la fecha del registro. Si no se proporciona un ID de empleado o una fecha, se
     * asignan valores predeterminados.
     *
     * @param array $asistencia_row Fila de datos del registro de asistencia.
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada para obtener los subordinados o un array de error si ocurre algún problema.
     */
    final public function subordinados(array $asistencia_row, string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }

        $key_jefe_id = $entidad_empleado."_jefe_inmediato_id";
        $key_empleado_id = $entidad_empleado.'_id';

        if(!isset($asistencia_row[$key_empleado_id])){
            $asistencia_row[$key_empleado_id] = -1;
        }
        if(!isset($asistencia_row['fecha'])){
            $asistencia_row['fecha'] = '1900-01-01';
        }

        $wh_jefe = "asistencia_app.$key_jefe_id = $asistencia_row[$key_empleado_id]";
        $wh_fecha = "asistencia_app.fecha = '$asistencia_row[fecha]'";
        return /** @lang MYSQL */ "SELECT *FROM asistencia_app WHERE $wh_jefe AND $wh_fecha";

    }

    /**
     * TRASLADADO
     * Actualiza el estado de asistencia en la tabla `asistencia_app` a 'activo'.
     *
     * La función actualiza el estado de asistencia en la tabla `asistencia_app` para el registro con el ID proporcionado.
     * Valida que el `$asistencia_app_id` sea mayor a 0. Si el ID es válido, genera y devuelve la consulta SQL correspondiente.
     * Si el ID no es válido, devuelve un array de error.
     *
     * @param int $asistencia_app_id El ID del registro en `asistencia_app`, debe ser mayor a 0.
     *
     * @return string|array Devuelve la consulta SQL para actualizar el estado de asistencia a 'activo', o un array de error si el ID es inválido.
     */
    final public function upd_asistencia(int $asistencia_app_id)
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id debe ser mayor a 0',$asistencia_app_id);
        }
        $set = "asistencia = 'activo'";
        return /** @lang MYSQL */ "UPDATE asistencia_app SET $set WHERE asistencia_app.id=$asistencia_app_id";

    }

    /**
     * TRASLADADO
     * Genera una consulta SQL para actualizar el estado de "checada_en_sitio" a 'activo' en un registro de asistencia.
     *
     * Esta función valida que el ID de la asistencia sea mayor a 0. Si el ID es válido, genera una consulta SQL para
     * actualizar el campo `checada_en_sitio` en la tabla `asistencia_app` y establece su valor a 'activo'. Si el
     * ID de la asistencia es menor o igual a 0, devuelve un array de error.
     *
     * @param int $asistencia_app_id ID del registro de asistencia que se va a actualizar.
     *
     * @return string|array Devuelve una consulta SQL para actualizar el campo `checada_en_sitio` o un array de error
     *                      si el ID de la asistencia es inválido.
     */
    final public function upd_checada_en_sitio_activa(int $asistencia_app_id)
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id debe ser mayor a 0',$asistencia_app_id);
        }
        $set = "checada_en_sitio = 'activo'";
        return /** @lang MYSQL */ "UPDATE asistencia_app SET $set WHERE asistencia_app.id = $asistencia_app_id";

    }

    /**
     * TRASLADADO
     * Actualiza el campo 'dia' en la tabla 'asistencia_app' para un registro específico.
     *
     * Esta función recibe un ID de la tabla `asistencia_app` y un valor para el día.
     * Valida que el día no esté vacío y que el ID sea mayor que 0, luego genera una consulta
     * SQL para actualizar el campo `dia` en la base de datos.
     *
     * @param int $asistencia_app_id El ID del registro en la tabla `asistencia_app` que se desea actualizar. Debe ser mayor a 0.
     * @param string $dia El valor del día que se va a establecer. No debe estar vacío.
     *
     * @return string|array Retorna la consulta SQL si los parámetros son válidos. Si alguno de los parámetros no es válido,
     *                      retorna un array de error con los detalles del fallo.
     */
    final public function upd_dia(int $asistencia_app_id, string $dia)
    {
        $dia = trim($dia);
        if($dia === ''){
            return (new error())->error('Error $dia esta vacio',$dia);
        }
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id debe ser mayor a 0',$asistencia_app_id);
        }
        return /** @lang MYSQL */ "UPDATE asistencia_app SET dia = '$dia' WHERE id = $asistencia_app_id";

    }

    /**
     * TRASLADADO
     * Actualiza el campo `jefe_checo` a 'activo' en la tabla `asistencia_app` para un registro específico.
     *
     * Esta función genera una consulta SQL para actualizar el campo `jefe_checo` a 'activo' en el registro
     * de la tabla `asistencia_app` correspondiente al ID proporcionado.
     *
     * @param int $asistencia_app_id ID del registro en la tabla `asistencia_app`. Debe ser mayor a 0.
     *
     * @return string|array Consulta SQL para realizar la actualización en la tabla `asistencia_app`.
     *                Si el ID es inválido, retorna un array con detalles del error.
     */
    final public function upd_jefe_checo_activo(int $asistencia_app_id)
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id debe ser mayor a 0',$asistencia_app_id);
        }
        return /** @lang MYSQL */ "UPDATE asistencia_app SET jefe_checo = 'activo' WHERE id = $asistencia_app_id";

    }

    /**
     * TRASLADADO
     * Valida los parámetros de asistencia de un empleado.
     *
     * Esta función verifica que los parámetros proporcionados no estén vacíos
     * y que el ID del empleado sea un valor positivo. Si algún parámetro no es válido,
     * retorna un error.
     *
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado', 'usuario').
     * @param string $fecha La fecha de la asistencia en formato 'YYYY-MM-DD'.
     * @param int $ohem_id El ID del empleado en la base de datos.
     *
     * @return bool|array Retorna true si los parámetros son válidos, o un array con detalles del error en caso de falla.
     */
    final public function valida_asistencia(string $entidad_empleado, string $fecha, int $ohem_id)
    {
        $fecha = trim($fecha);
        if($fecha === ''){
            return (new error())->error('Error fecha esta vacia',$fecha);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        if($ohem_id <= 0){
            return (new error())->error('Error $ohem_id debe ser mayor a 0',$ohem_id);
        }
        return true;

    }

    /**
     * TRASLADADO
     * Valida los datos de entrada para las consultas de asistencia, asegurando que los parámetros requeridos no estén vacíos.
     *
     * La función verifica que el nombre de la entidad del empleado, el campo de fecha de ingreso,
     * y las fechas inicial y final no estén vacíos. Si alguno de estos parámetros está vacío, devuelve un error con los detalles.
     * Si todos los parámetros son válidos, devuelve `true`.
     *
     * @param string $campo_fecha_ingreso El nombre del campo de la fecha de ingreso del empleado.
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     * @param string $fecha_final La fecha final del período (en formato `YYYY-MM-DD`).
     * @param string $fecha_inicial La fecha inicial del período (en formato `YYYY-MM-DD`).
     *
     * @return bool|array Devuelve `true` si todos los parámetros son válidos. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    final public function valida_data_asistencia(
        string $campo_fecha_ingreso, string $entidad_empleado, string $fecha_final, string $fecha_inicial)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }
        $campo_fecha_ingreso = trim($campo_fecha_ingreso);
        if($campo_fecha_ingreso === ''){
            return (new error())->error('Error $campo_fecha_ingreso esta vacia', $campo_fecha_ingreso);
        }
        $fecha_final = trim($fecha_final);
        if($fecha_final === ''){
            return (new error())->error('Error $fecha_final esta vacia',$fecha_final);
        }
        $fecha_inicial = trim($fecha_inicial);
        if($fecha_inicial === ''){
            return (new error())->error('Error $fecha_inicial esta vacia',$fecha_inicial);
        }
        return true;

    }

    /**
     * TRASLADADO
     * Valida los datos de entrada para el registro de asistencia, asegurando que los parámetros proporcionados no estén vacíos y que el ID del empleado sea válido.
     *
     * La función verifica que el nombre del campo del esquema, el nombre de la entidad del empleado,
     * la entidad relacionada con el esquema, la fecha límite y el ID del empleado no estén vacíos y sean válidos.
     * Si algún parámetro no es válido, devuelve un error con los detalles.
     *
     * @param string $campo_esquema_name El nombre del campo del esquema que se desea validar.
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     * @param string $entidad_rel_esquema El nombre de la entidad relacionada con el esquema.
     * @param string $fecha_limite La fecha límite hasta la cual se está validando (en formato `YYYY-MM-DD`).
     * @param int $ohem_id El ID del empleado, que debe ser mayor a 0.
     *
     * @return bool|array Devuelve `true` si todos los parámetros son válidos. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    final public function valida_data_asistencia_reg(
        string $campo_esquema_name, string $entidad_empleado, string $entidad_rel_esquema, string $fecha_limite,
        int $ohem_id)
    {
        $campo_esquema_name = trim($campo_esquema_name);
        if($campo_esquema_name === ''){
            return (new error())->error('Error $campo_esquema_name esta vacio', $campo_esquema_name);
        }
        if($ohem_id <= 0){
            return (new error())->error('Error $ohem_id debe ser mayor a 0', $ohem_id);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacio', $entidad_empleado);
        }
        $entidad_rel_esquema = trim($entidad_rel_esquema);
        if($entidad_rel_esquema === ''){
            return (new error())->error('Error $entidad_rel_esquema esta vacio', $entidad_rel_esquema);
        }
        $fecha_limite = trim($fecha_limite);
        if($fecha_limite === ''){
            return (new error())->error('Error $fecha_limite esta vacio', $fecha_limite);
        }

        return true;

    }

    /**
     * TRASLADADO
     * Genera una cláusula WHERE SQL para filtrar registros por un rango de fechas.
     *
     * La función valida que las fechas proporcionadas no estén vacías. Luego, genera una cláusula SQL `BETWEEN`
     * para filtrar los registros de la tabla `asistencia_app` basados en un rango de fechas.
     * Si ocurre algún error durante el proceso, devuelve un array con los detalles del fallo.
     *
     * @param string $fecha_final La fecha final del rango (en formato `YYYY-MM-DD`).
     * @param string $fecha_inicial La fecha inicial del rango (en formato `YYYY-MM-DD`).
     *
     * @return string|array Devuelve una cadena con la cláusula WHERE SQL que filtra por el rango de fechas.
     *                      Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function where_fecha(string $fecha_final, string $fecha_inicial)
    {
        $fecha_final = trim($fecha_final);
        if($fecha_final === ''){
            return (new error())->error('Error $fecha_final esta vacia',$fecha_final);
        }
        $fecha_inicial = trim($fecha_inicial);
        if($fecha_inicial === ''){
            return (new error())->error('Error $fecha_inicial esta vacia',$fecha_inicial);
        }
        $btw = "'$fecha_inicial' AND '$fecha_final'";
        return "asistencia_app.fecha BETWEEN $btw";

    }
}
