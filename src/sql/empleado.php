<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql;

class empleado{

    private function init_campos_base(string $entidad_empleado) {
        $campos_base =  array("$entidad_empleado.id","$entidad_empleado.nombre_completo",
        "$entidad_empleado.status","plaza.descripcion");

        $campos = (new sql())->campos_sql_puros_with_as($campos_base);
        if(error::$en_error){
            return (new error())->error('Error al generar $campos', $campos);
        }

        return $campos;
    }

    /**
     * Genera una consulta SQL para obtener una lista de empleados en formato datatable.
     *
     * @param string $entidad_empleado  Nombre de la entidad que representa al empleado. No puede estar vacío.
     * @param string $plaza_id  ID(s) de la(s) plaza(s) para filtrar empleados. No puede estar vacío.
     * @param string $status  Estado del empleado para filtrar los resultados. Opcional.
     * @param int $limit  Límite máximo de resultados a obtener. Por defecto es 0 (sin límite).
     * @param int $offset  Desplazamiento desde el cual iniciar la consulta. Por defecto es 0.
     * @param string $order_by  Columna por la cual ordenar los resultados. Opcional.
     * @param string $wh_like  Cadena para buscar coincidencias parciales en los campos especificados.
     *
     * @return string|array  Devuelve la consulta SQL como cadena si no hay errores, o un objeto de error en caso de fallo en la validación o generación de la consulta.
     */
    public function get_empleado_datatable(string $entidad_empleado,string $plaza_id,string $status,int $limit = 0,int $offset = 0, string $order_by = '',
                                            string $wh_like = '') 
    {
        if(trim($entidad_empleado) === '') {
            return (new error())->error('Error $entidad_empleado no puede ser vacio', $entidad_empleado);
        }
        if(trim($plaza_id) === '') {
            return (new error())->error('Error $plaza_id no puede ser vacio', $plaza_id);
        }

        $campos = $this->init_campos_base($entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al generar $campos', $campos);
        }
        $join = "LEFT JOIN plaza AS plaza ON $entidad_empleado.plaza_id = plaza.id";
        
        $where = " plaza.id IN($plaza_id)";
        if(trim($status) !== '') {
            $where .= " AND $entidad_empleado.status = '$status'";
        }
        if(trim($wh_like) !== '') {
            $where .= " AND ($entidad_empleado.id LIKE '%$wh_like%' OR $entidad_empleado.nombre_completo LIKE '%$wh_like%'";
            $where .= " OR plaza.descripcion LIKE '%$wh_like%' OR $entidad_empleado.status LIKE '%$wh_like%') ";
        }
        
        $filters = "";
        if(trim($order_by) !== '') {
            $filters .= " ORDER BY $order_by";
        }
        if($limit > 0) {
            $filters .= " LIMIT $limit";
        }
        if($offset > 0) {
            $filters .= " OFFSET $offset";
        }

        $lf_from = $entidad_empleado . " " . $join;

        $where .= $filters;
        return /**@lang MYSQL */ "SELECT $campos FROM $lf_from WHERE $where";
    }

    /**
     * TRASLADADO
     * Genera una cláusula SQL para verificar que una fecha en una tabla es menor o igual a una fecha específica.
     *
     * @param string $campo_fecha El nombre del campo de fecha en la tabla.
     * @param string $fecha_inicio_menor_a La fecha límite superior para la comparación.
     * @param string $tabla El nombre de la tabla que contiene el campo de fecha.
     *
     * @return string|array Una cláusula SQL con la condición WHERE.
     * @throws error Si $campo_fecha o $tabla están vacíos.
     */
    final public function sql_extra_fecha_inicio(string $campo_fecha, string $fecha_inicio_menor_a,
                                                 string $tabla)
    {
        $campo_fecha = trim($campo_fecha);
        if($campo_fecha === ''){
            return (new error())->error('Error $campo_fecha debe tener info',$campo_fecha);
        }
        $tabla = trim($tabla);
        if($tabla === ''){
            return (new error())->error('Error $tabla debe tener info',$tabla);
        }
        $fecha_inicio_menor_a = trim($fecha_inicio_menor_a);
        $sql_where_string = '';
        if($fecha_inicio_menor_a !== ''){
            $sql_where_string = " AND $tabla.$campo_fecha <= '$fecha_inicio_menor_a'";
        }

        return $sql_where_string;

    }

    /**
     * EM3
     * Genera una cláusula SQL `IN` para un conjunto de identificadores de plazas.
     *
     * Esta función valida un array de identificadores de plazas (`plazas_id`), asegurándose de que cada identificador
     * sea un número entero no vacío, y construye una cláusula SQL `IN` con los valores proporcionados.
     *
     * @param array $plazas_id Un array de identificadores de plazas. Cada elemento debe ser un número entero no vacío.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `IN` generada. Si el array está vacío, devuelve
     *                      una cadena vacía. En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula `IN` válida
     * $plazas_id = [1, 2, 3, 4];
     * $resultado = $this->sql_plazas_in($plazas_id);
     * echo $resultado;
     * // Resultado:
     * // IN( 1,2,3,4 )
     *
     * // Ejemplo 2: Identificador vacío (error)
     * $plazas_id = [1, '', 3];
     * $resultado = $this->sql_plazas_in($plazas_id);
     * // Resultado:
     * // ['error' => 'Error plaza_id esta vacia', 'data' => [...]]
     *
     * // Ejemplo 3: Identificador no numérico (error)
     * $plazas_id = [1, 'dos', 3];
     * $resultado = $this->sql_plazas_in($plazas_id);
     * // Resultado:
     * // ['error' => 'Error $plaza_id debe ser un entero', 'data' => 'dos']
     *
     * // Ejemplo 4: Array vacío
     * $plazas_id = [];
     * $resultado = $this->sql_plazas_in($plazas_id);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     * ```
     */
    final public function sql_plazas_in(array $plazas_id)
    {
        // Inicializar la cadena SQL
        $sql = '';

        // Verificar si el array tiene elementos
        if (count($plazas_id) > 0) {
            $sql_in = '';

            // Recorrer cada identificador de plaza
            foreach ($plazas_id as $plaza_id) {
                // Validar que el identificador no esté vacío
                $plaza_id = trim($plaza_id);
                if ($plaza_id === '') {
                    return (new error())->error('Error plaza_id esta vacia', $plazas_id);
                }

                // Validar que el identificador sea numérico
                if (!is_numeric($plaza_id)) {
                    return (new error())->error('Error $plaza_id debe ser un entero', $plaza_id);
                }

                // Determinar si se debe agregar una coma como separador
                $sql_in = trim($sql_in);
                $coma = $sql_in === '' ? '' : ',';

                // Concatenar el identificador al SQL
                $sql_in .= $coma . $plaza_id;
            }

            // Construir la cláusula IN
            $sql = " IN( " . $sql_in . " )";
        }

        // Retornar la cláusula SQL generada o una cadena vacía
        return $sql;
    }

}
