<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;

class esquema_producto_comision{

    private string $name_entidad = 'esquema_producto_comision';

    final public function where_fecha_comision(string $fecha_contrato)
    {
        $fecha_contrato = trim($fecha_contrato);
        if($fecha_contrato === ''){
            return (new error())->error('Error fecha de contrato esta vacia', $fecha_contrato);
        }
        $sql_where = " AND '$fecha_contrato' >= esquema_producto_comision.fecha_inicio AND";
        $sql_where .= " '$fecha_contrato' <= esquema_producto_comision.fecha_fin ";

        return $sql_where;

    }


}
