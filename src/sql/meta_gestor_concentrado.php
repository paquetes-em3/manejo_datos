<?php
namespace desarrollo_em3\manejo_datos\sql;

use desarrollo_em3\error\error;
use stdClass;

/**
 * Clase meta_gestor_concentrado
 *
 * Esta clase proporciona métodos para la manipulación, validación, y generación de consultas relacionadas
 * con la tabla `meta_gestor_concentrado`. Incluye funcionalidades para:
 *
 * - Validar campos asociados a contratos y pagos.
 * - Generar consultas SQL complejas para cálculos de metas, pagos y relaciones con otras entidades.
 * - Aplicar restricciones de seguridad y control de acceso a través de filtros dinámicos.
 * - Manejar errores en tiempo de ejecución mediante la estructura `error`.
 *
 * ### Principales Funcionalidades
 * - **Generación de Consultas SQL**:
 *   - Construcción de consultas dinámicas con JOINs, filtros, y cálculos SUM para operaciones específicas.
 *   - Generación de condiciones WHERE y cláusulas SET para operaciones UPDATE.
 *
 * - **Validación y Manipulación de Datos**:
 *   - Validación de parámetros y generación de campos con prefijos específicos (`contrato.`, `pago.`).
 *   - Inicialización y formateo de campos relacionados con contratos y pagos.
 *
 * - **Integración con Seguridad**:
 *   - Filtros de seguridad basados en roles, permisos de usuarios y restricciones por departamento, plazas y unidades de negocio.
 *
 * ### Requisitos
 * - Requiere PHP 7.4 o superior.
 * - Depende de las clases:
 *   - `desarrollo_em3\error\error`: Manejo de errores.
 *   - `stdClass`: Utilizado para estructuras de datos dinámicas.
 *
 * ### Ejemplo de Uso
 * ```php
 * use desarrollo_em3\manejo_datos\sql\meta_gestor_concentrado;
 *
 * // Crear una instancia de la clase
 * $metaGestor = new meta_gestor_concentrado();
 *
 * // Generar una consulta SQL para recalcular metas
 * $consulta = $metaGestor->recalcula_meta(
 *     'contrato.fecha_inicio',
 *     'pago.fecha',
 *     'pago.movimiento',
 *     'pago.total',
 *     'empleado'
 * );
 *
 * if (is_array($consulta) && isset($consulta['error'])) {
 *     // Manejar el error
 *     echo "Error: " . $consulta['error'];
 * } else {
 *     // Ejecutar la consulta
 *     echo $consulta;
 * }
 * ```
 *
 * ### Métodos Clave
 * - `campo_date_contrato(string $campo_date_contrato)`
 * - `campo_fecha_pago(string $campo_fecha_pago)`
 * - `tabla_base(string $campo_date_contrato, ...)`
 * - `table_pagos(string $campo_date_contrato, ...)`
 * - `recalcula_meta(string $campo_date_contrato, ...)`
 *
 * @package desarrollo_em3\manejo_datos\sql
 */
class meta_gestor_concentrado{


    /**
     * EM3
     * Genera y valida el campo de fecha para un contrato.
     *
     * Esta función valida que el nombre del campo de fecha del contrato no esté vacío.
     * Luego, utiliza la función `init_campo` para asegurarse de que el campo tenga el prefijo `contrato.`
     * y valida posibles errores en el proceso.
     *
     * @param string $campo_date_contrato El nombre del campo de fecha del contrato a validar y procesar.
     *
     * @return string|array Devuelve el nombre del campo con el prefijo `contrato.` agregado,
     *                      o un array de error si el campo está vacío o si ocurre un error en `init_campo`.
     *
     * @example
     * ```php
     * // Ejemplo 1: Campo válido sin prefijo
     * $campo = $this->campo_date_contrato('fecha_inicio');
     * // Resultado: 'contrato.fecha_inicio'
     *
     * // Ejemplo 2: Campo válido con prefijo
     * $campo = $this->campo_date_contrato('contrato.fecha_fin');
     * // Resultado: 'contrato.fecha_fin'
     *
     * // Ejemplo 3: Campo vacío
     * $campo = $this->campo_date_contrato('');
     * // Resultado: ['error' => 'Error $campo_date_contrato esta vacio', 'data' => '']
     *
     * // Ejemplo 4: Error en init_campo
     * $campo = $this->campo_date_contrato('fecha_inicio');
     * // Suponiendo que init_campo genera un error, el resultado sería:
     * // ['error' => 'Error generar campo date contrato', 'data' => 'contrato.fecha_inicio']
     * ```
     */
    private function campo_date_contrato(string $campo_date_contrato)
    {
        // Elimina espacios en blanco alrededor del nombre del campo
        $campo_date_contrato = trim($campo_date_contrato);
        if ($campo_date_contrato === '') {
            // Retorna un error si el campo está vacío
            return (new error())->error('Error $campo_date_contrato esta vacio', $campo_date_contrato);
        }

        // Utiliza init_campo para validar y agregar el prefijo "contrato."
        $campo_date_contrato = $this->init_campo($campo_date_contrato, 'contrato');
        if (error::$en_error) {
            // Retorna un error si init_campo falla
            return (new error())->error('Error generar campo date contrato', $campo_date_contrato);
        }

        // Devuelve el campo con el prefijo validado
        return $campo_date_contrato;
    }


    /**
     * EM3
     * Genera y valida el campo de fecha para un pago.
     *
     * Esta función valida que el nombre del campo de fecha de pago no esté vacío.
     * Luego, utiliza la función `init_campo` para asegurarse de que el campo tenga el prefijo `pago.`
     * y valida posibles errores en el proceso.
     *
     * @param string $campo_fecha_pago El nombre del campo de fecha de pago a validar y procesar.
     *
     * @return string|array Devuelve el nombre del campo con el prefijo `pago.` agregado,
     *                      o un array de error si el campo está vacío o si ocurre un error en `init_campo`.
     *
     * @example
     * ```php
     * // Ejemplo 1: Campo válido sin prefijo
     * $campo = $this->campo_fecha_pago('fecha');
     * // Resultado: 'pago.fecha'
     *
     * // Ejemplo 2: Campo válido con prefijo
     * $campo = $this->campo_fecha_pago('pago.fecha_final');
     * // Resultado: 'pago.fecha_final'
     *
     * // Ejemplo 3: Campo vacío
     * $campo = $this->campo_fecha_pago('');
     * // Resultado: ['error' => 'Error $campo_fecha_pago esta vacio', 'data' => '']
     *
     * // Ejemplo 4: Error en init_campo
     * $campo = $this->campo_fecha_pago('fecha_inicio');
     * // Suponiendo que init_campo genera un error, el resultado sería:
     * // ['error' => 'Error generar $campo_fecha_pago', 'data' => 'pago.fecha_inicio']
     * ```
     */
    private function campo_fecha_pago(string $campo_fecha_pago)
    {
        // Elimina espacios en blanco alrededor del nombre del campo
        $campo_fecha_pago = trim($campo_fecha_pago);
        if ($campo_fecha_pago === '') {
            // Retorna un error si el campo está vacío
            return (new error())->error('Error $campo_fecha_pago esta vacio', $campo_fecha_pago);
        }

        // Utiliza init_campo para validar y agregar el prefijo "pago."
        $campo_fecha_pago = $this->init_campo($campo_fecha_pago, 'pago');
        if (error::$en_error) {
            // Retorna un error si init_campo falla
            return (new error())->error('Error generar $campo_fecha_pago', $campo_fecha_pago);
        }

        // Devuelve el campo con el prefijo validado
        return $campo_fecha_pago;
    }


    /**
     * EM3
     * Valida y asegura que el nombre del campo de movimiento tenga el prefijo adecuado.
     *
     * Esta función valida que el nombre del campo `$campo_movto` no esté vacío. Además,
     * utiliza la función `init_campo` para agregar el prefijo `pago.` al nombre del campo,
     * si es necesario. Si ocurre algún error, devuelve un array con el detalle del mismo.
     *
     * @param string $campo_movto El nombre del campo de movimiento a validar y procesar.
     *
     * @return string|array Devuelve el nombre del campo con el prefijo `pago.` agregado,
     *                      o un array de error si el campo está vacío o si ocurre un error en `init_campo`.
     *
     * @example
     * ```php
     * // Ejemplo 1: Campo válido sin prefijo
     * $campo = $this->campo_movto('movimiento');
     * // Resultado: 'pago.movimiento'
     *
     * // Ejemplo 2: Campo válido con prefijo
     * $campo = $this->campo_movto('pago.movimiento');
     * // Resultado: 'pago.movimiento'
     *
     * // Ejemplo 3: Campo vacío
     * $campo = $this->campo_movto('');
     * // Resultado: ['error' => 'Error $campo_movto esta vacio', 'data' => '']
     *
     * // Ejemplo 4: Error en init_campo
     * $campo = $this->campo_movto('movimiento');
     * // Suponiendo que init_campo genera un error, el resultado sería:
     * // ['error' => 'Error generar $campo_movto', 'data' => 'pago.movimiento']
     * ```
     */
    private function campo_movto(string $campo_movto)
    {
        // Elimina espacios al inicio y al final del nombre del campo
        $campo_movto = trim($campo_movto);

        // Valida que el campo no esté vacío
        if ($campo_movto === '') {
            // Retorna un error si el campo está vacío
            return (new error())->error('Error $campo_movto esta vacio', $campo_movto);
        }

        // Valida y agrega el prefijo 'pago.' al campo
        $campo_movto = $this->init_campo($campo_movto, 'pago');
        if (error::$en_error) {
            // Retorna un error si ocurre un problema en init_campo
            return (new error())->error('Error generar $campo_movto', $campo_movto);
        }

        // Devuelve el campo validado con el prefijo adecuado
        return $campo_movto;
    }


    /**
     * EM3
     * Valida y asegura que el nombre del campo de pago total tenga el prefijo adecuado.
     *
     * Esta función valida que el nombre del campo `$campo_pago_total` no esté vacío y
     * utiliza la función `init_campo` para agregar el prefijo `pago.` si es necesario.
     * Si ocurre algún error, devuelve un array de error.
     *
     * @param string $campo_pago_total El nombre del campo de pago total a validar y procesar.
     *
     * @return string|array Devuelve el nombre del campo con el prefijo `pago.` agregado,
     *                      o un array de error si el campo está vacío o si ocurre un error en `init_campo`.
     *
     * @example
     * ```php
     * // Ejemplo 1: Campo válido sin prefijo
     * $campo = $this->campo_pago_total('total');
     * // Resultado: 'pago.total'
     *
     * // Ejemplo 2: Campo válido con prefijo
     * $campo = $this->campo_pago_total('pago.total');
     * // Resultado: 'pago.total'
     *
     * // Ejemplo 3: Campo vacío
     * $campo = $this->campo_pago_total('');
     * // Resultado: ['error' => 'Error $campo_pago_total esta vacio', 'data' => '']
     *
     * // Ejemplo 4: Error en init_campo
     * $campo = $this->campo_pago_total('total');
     * // Suponiendo que init_campo genera un error, el resultado sería:
     * // ['error' => 'Error generar $campo_pago_total', 'data' => 'pago.total']
     * ```
     */
    private function campo_pago_total(string $campo_pago_total)
    {
        // Elimina espacios en blanco alrededor del nombre del campo
        $campo_pago_total = trim($campo_pago_total);
        if ($campo_pago_total === '') {
            // Retorna un error si el campo está vacío
            return (new error())->error('Error $campo_pago_total esta vacio', $campo_pago_total);
        }

        // Utiliza init_campo para validar y agregar el prefijo "pago."
        $campo_pago_total = $this->init_campo($campo_pago_total, 'pago');
        if (error::$en_error) {
            // Retorna un error si init_campo falla
            return (new error())->error('Error generar $campo_pago_total', $campo_pago_total);
        }

        // Devuelve el campo con el prefijo validado
        return $campo_pago_total;
    }


    /**
     * EM3
     * Genera una cadena con las asignaciones SQL para una cláusula `SET`.
     *
     * Esta función construye y retorna una lista de asignaciones SQL, separadas por comas,
     * para ser utilizadas en una cláusula `SET` durante una operación de actualización (`UPDATE`).
     * Las asignaciones enlazan los campos de la tabla `meta_gestor_concentrado` con los valores
     * correspondientes de la tabla derivada `tabla2`.
     *
     * @return string Devuelve una cadena con las asignaciones de campos para la cláusula `SET`.
     *
     * @example
     * ```php
     * // Ejemplo 1: Usar la cadena en una consulta UPDATE
     * $set_clause = $this->campos_set();
     * $query = "UPDATE meta_gestor_concentrado
     *           SET $set_clause
     *           WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id";
     * // Resultado:
     * // UPDATE meta_gestor_concentrado
     * // SET meta_gestor_concentrado.meta_proceso = tabla2.monto_proceso,
     * //     meta_gestor_concentrado.monto_de_mas = tabla2.monto_de_mas,
     * //     meta_gestor_concentrado.monto_nuevo = tabla2.total_tipo_nuevo,
     * //     meta_gestor_concentrado.monto_contratos_extra = tabla2.total_tipo_viejo
     * // WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id
     * ```
     */
    private function campos_set(): string
    {
        // Asignaciones de campos para la cláusula SET
        $monto_proceso = "meta_gestor_concentrado.meta_proceso = tabla2.monto_proceso";
        $monto_de_mas = "meta_gestor_concentrado.monto_de_mas = tabla2.monto_de_mas";
        $monto_nuevo = "meta_gestor_concentrado.monto_nuevo = tabla2.total_tipo_nuevo";
        $monto_contratos_extra = "meta_gestor_concentrado.monto_contratos_extra = tabla2.total_tipo_viejo";

        // Retornar la cadena con las asignaciones separadas por comas
        return "$monto_proceso, $monto_de_mas, $monto_nuevo, $monto_contratos_extra";
    }


    /**
     * EM3
     * Genera una cláusula SQL `JOIN` con una tabla derivada.
     *
     * Esta función valida los datos de entrada, genera una tabla derivada mediante
     * la función `table_base_2`, y construye una cláusula `JOIN` utilizando la condición
     * proporcionada por `on_base`.
     *
     * @param string $campo_date_contrato El nombre del campo de fecha del contrato.
     * @param string $campo_fecha_pago El nombre del campo de fecha de pago.
     * @param string $campo_movto El nombre del campo de movimiento.
     * @param string $campo_pago_total El nombre del campo de total de pago.
     * @param string $entidad_empleado El nombre de la entidad del empleado.
     *
     * @return string|array Devuelve la cláusula SQL `JOIN` generada como una cadena.
     *                      Si ocurre un error en cualquier paso, devuelve un array con
     *                      detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Parámetros válidos
     * $join_clause = $this->join_upd(
     *     'contrato.fecha_inicio',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total',
     *     'empleado'
     * );
     * // Resultado:
     * // (subconsulta de table_base_2) AS tabla2
     * // ON meta_gestor_concentrado.id = tabla2.meta_gestor_concentrado_id
     *
     * // Ejemplo 2: Error en los datos de entrada
     * $join_clause = $this->join_upd(
     *     '',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total',
     *     'empleado'
     * );
     * // Resultado:
     * // ['error' => 'Error al $valida datos', 'data' => [...]]
     *
     * // Ejemplo 3: Error al generar table_base_2
     * $join_clause = $this->join_upd(
     *     'contrato.fecha_inicio',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total',
     *     'empleado'
     * );
     * // Resultado:
     * // ['error' => 'Error al generar table 2', 'data' => [...]]
     * ```
     */
    private function join_upd(
        string $campo_date_contrato,
        string $campo_fecha_pago,
        string $campo_movto,
        string $campo_pago_total,
        string $entidad_empleado
    ) {
        // Validar los datos de entrada
        $valida = $this->valida_datos(
            $campo_date_contrato,
            $campo_fecha_pago,
            $campo_movto,
            $campo_pago_total,
            $entidad_empleado,
            true
        );
        if (error::$en_error) {
            return (new error())->error('Error al $valida datos', $valida);
        }

        // Generar la tabla base derivada
        $table_base_2 = $this->table_base_2(
            $campo_date_contrato,
            $campo_fecha_pago,
            $campo_movto,
            $campo_pago_total,
            $entidad_empleado
        );
        if (error::$en_error) {
            return (new error())->error('Error al generar table 2', $table_base_2);
        }

        // Generar la condición ON para el JOIN
        $on_base = $this->on_base();
        if (error::$en_error) {
            return (new error())->error('Error al generar $on_base', $on_base);
        }

        // Construir y retornar la cláusula JOIN
        return "($table_base_2) AS tabla2 ON $on_base";
    }


    /**
     * EM3
     * Inicializa un nombre de campo asegurándose de que tenga un separador prefijo.
     *
     * Esta función valida que los parámetros `$campo_name` y `$separator` no estén vacíos.
     * Si el nombre del campo no contiene el separador indicado, se le agrega como prefijo.
     *
     * @param string $campo_name El nombre del campo a inicializar.
     * @param string $separator El separador que se asegura como prefijo en el nombre del campo.
     *
     * @return string|array Devuelve el nombre del campo con el separador asegurado como prefijo,
     *                      o un array de error si alguno de los parámetros está vacío.
     *
     * @example
     * ```php
     * // Ejemplo 1: Campo ya tiene el separador
     * $campo = $this->init_campo('tabla.campo', 'tabla');
     * // Resultado: 'tabla.campo'
     *
     * // Ejemplo 2: Campo sin separador
     * $campo = $this->init_campo('campo', 'tabla');
     * // Resultado: 'tabla.campo'
     *
     * // Ejemplo 3: Campo vacío
     * $campo = $this->init_campo('', 'tabla');
     * // Resultado: ['error' => 'Error $campo_name esta vacio', 'data' => '']
     *
     * // Ejemplo 4: Separador vacío
     * $campo = $this->init_campo('campo', '');
     * // Resultado: ['error' => 'Error $separator esta vacio', 'data' => '']
     * ```
     */
    private function init_campo(string $campo_name, string $separator)
    {
        // Elimina espacios en blanco alrededor del nombre del campo
        $campo_name = trim($campo_name);
        if ($campo_name === '') {
            // Retorna error si el nombre del campo está vacío
            return (new error())->error('Error $campo_name esta vacio', $campo_name);
        }

        // Elimina espacios en blanco alrededor del separador
        $separator = trim($separator);
        if ($separator === '') {
            // Retorna error si el separador está vacío
            return (new error())->error('Error $separator esta vacio', $separator);
        }

        // Divide el nombre del campo por el separador
        $parts = explode("$separator.", $campo_name);
        if (count($parts) === 1) {
            // Agrega el separador como prefijo si no está presente
            $campo_name = "$separator.$campo_name";
        }

        // Devuelve el nombre del campo con el separador asegurado
        return $campo_name;
    }


    /**
     * EM3
     * Inicializa y valida los campos relacionados con pagos y contratos.
     *
     * Esta función valida los datos de entrada y formatea cada campo usando funciones especializadas.
     * Si algún campo no pasa las validaciones, devuelve un array de error. En caso de éxito,
     * retorna un objeto con los campos inicializados y formateados.
     *
     * @param string $campo_date_contrato El nombre del campo de fecha del contrato a inicializar.
     * @param string $campo_fecha_pago El nombre del campo de fecha de pago a inicializar.
     * @param string $campo_movto El nombre del campo de movimiento a inicializar.
     * @param string $campo_pago_total El nombre del campo de total de pago a inicializar.
     *
     * @return stdClass|array Devuelve un objeto con los campos inicializados si todo es exitoso,
     *                        o un array de error si ocurre algún problema durante las validaciones o inicializaciones.
     *
     * @example
     * ```php
     * // Ejemplo 1: Todos los campos válidos
     * $campos = $this->init_campos(
     *     'contrato.fecha_inicio',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total'
     * );
     * // Resultado:
     * // stdClass {
     * //     campo_pago_total: 'pago.total',
     * //     campo_date_contrato: 'contrato.fecha_inicio',
     * //     campo_fecha_pago: 'pago.fecha',
     * //     campo_movto: 'pago.movimiento'
     * // }
     *
     * // Ejemplo 2: Error en la validación de datos
     * $campos = $this->init_campos(
     *     '',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total'
     * );
     * // Resultado:
     * // ['error' => 'Error $campo_date_contrato está vacío', 'data' => '']
     *
     * // Ejemplo 3: Error al inicializar un campo
     * $campos = $this->init_campos(
     *     'contrato.fecha_inicio',
     *     '',
     *     'pago.movimiento',
     *     'pago.total'
     * );
     * // Resultado:
     * // ['error' => 'Error al generar $campo_fecha_pago', 'data' => '']
     * ```
     */
    private function init_campos(
        string $campo_date_contrato,
        string $campo_fecha_pago,
        string $campo_movto,
        string $campo_pago_total
    ) {
        // Validar los datos de entrada
        $valida = $this->valida_datos(
            $campo_date_contrato,
            $campo_fecha_pago,
            $campo_movto,
            $campo_pago_total,
            '',
            false
        );
        if (error::$en_error) {
            return (new error())->error('Error al $valida datos', $valida);
        }

        // Inicializar y formatear el campo de pago total
        $campo_pago_total = $this->campo_pago_total($campo_pago_total);
        if (error::$en_error) {
            return (new error())->error('Error al generar $campo_pago_total', $campo_pago_total);
        }

        // Inicializar y formatear el campo de fecha del contrato
        $campo_date_contrato = $this->campo_date_contrato($campo_date_contrato);
        if (error::$en_error) {
            return (new error())->error('Error al generar $campo_date_contrato', $campo_date_contrato);
        }

        // Inicializar y formatear el campo de fecha de pago
        $campo_fecha_pago = $this->campo_fecha_pago($campo_fecha_pago);
        if (error::$en_error) {
            return (new error())->error('Error al generar $campo_fecha_pago', $campo_fecha_pago);
        }

        // Inicializar y formatear el campo de movimiento
        $campo_movto = $this->campo_movto($campo_movto);
        if (error::$en_error) {
            return (new error())->error('Error al generar $campo_movto', $campo_movto);
        }

        // Crear y devolver un objeto con los campos inicializados
        $campos = new stdClass();
        $campos->campo_pago_total = $campo_pago_total;
        $campos->campo_date_contrato = $campo_date_contrato;
        $campos->campo_fecha_pago = $campo_fecha_pago;
        $campos->campo_movto = $campo_movto;

        return $campos;
    }


    /**
     * EM3
     * Genera la condición ON para una cláusula SQL de unión (JOIN).
     *
     * Esta función devuelve una condición básica para unir la tabla `meta_gestor_concentrado`
     * con otra tabla (`tabla2`) basada en el campo `meta_gestor_concentrado_id`.
     *
     * @return string Devuelve una cadena que representa la condición `ON` para una unión SQL.
     *
     * @example
     * ```php
     * // Ejemplo 1: Usar la condición ON en una consulta SQL
     * $on_condition = $this->on_base();
     * $query = "SELECT * FROM meta_gestor_concentrado
     *           INNER JOIN tabla2 ON $on_condition";
     * // Resultado:
     * // SELECT * FROM meta_gestor_concentrado
     * // INNER JOIN tabla2 ON meta_gestor_concentrado.id = tabla2.meta_gestor_concentrado_id
     *
     * // Ejemplo 2: Construcción dinámica con otras funciones
     * $on_condition = $this->on_base();
     * $additional_filter = "WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id";
     * $query = "SELECT * FROM meta_gestor_concentrado
     *           INNER JOIN tabla2 ON $on_condition $additional_filter";
     * // Resultado:
     * // SELECT * FROM meta_gestor_concentrado
     * // INNER JOIN tabla2 ON meta_gestor_concentrado.id = tabla2.meta_gestor_concentrado_id
     * // WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id
     * ```
     */
    private function on_base(): string
    {
        // Retorna la condición de unión para la cláusula ON
        return "meta_gestor_concentrado.id = tabla2.meta_gestor_concentrado_id";
    }




    /**
     * EM3
     * Genera una consulta SQL para la tabla base de pagos, incluyendo cálculos y uniones necesarias.
     *
     * Esta función valida los datos de entrada, inicializa los campos requeridos, genera subconsultas
     * y define las uniones y condiciones necesarias para construir una consulta SQL. La consulta
     * incluye cálculos de totales y clasificaciones por tipo de pago.
     *
     * @param string $campo_date_contrato El nombre del campo de fecha del contrato.
     * @param string $campo_fecha_pago El nombre del campo de fecha de pago.
     * @param string $campo_movto El nombre del campo de movimiento.
     * @param string $campo_pago_total El nombre del campo de total de pago.
     * @param string $entidad_empleado El nombre de la entidad del empleado.
     *
     * @return string|array Devuelve la consulta SQL generada como cadena. Si ocurre un error en
     *                      cualquier paso, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Parámetros válidos
     * $query = $this->tabla_base(
     *     'contrato.fecha_inicio',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total',
     *     'empleado'
     * );
     * // Resultado:
     * // SELECT pago.empleado_id, SUM(pago.total) AS total_pago,
     * //        SUM(IF (contrato.fecha_inicio BETWEEN :fecha_inicio AND :fecha_fin, pago.total, 0)) AS tipo_nuevo,
     * //        SUM(IF (contrato.fecha_inicio < :fecha_inicio, pago.total, 0)) AS tipo_viejo
     * // FROM pago
     * // LEFT JOIN (subconsulta de meta_gestor_detalle) AS meta_gestor_detalle
     * // ON meta_gestor_detalle.contrato_id = pago.contrato_id
     * // AND pago.empleado_id = meta_gestor_detalle.empleado_id
     * // LEFT JOIN contrato ON contrato.id = pago.contrato_id
     * // WHERE pago.`status` = 'activo'
     * // AND pago.fecha BETWEEN :fecha_inicio AND :fecha_fin
     * // AND pago.total > 0
     * // AND pago.movimiento = 'Abono'
     * // AND meta_gestor_detalle.contrato_id IS NULL
     * // GROUP BY pago.empleado_id
     *
     * // Ejemplo 2: Error en los parámetros de entrada
     * $query = $this->tabla_base('', 'pago.fecha', 'pago.movimiento', 'pago.total', 'empleado');
     * // Resultado:
     * // ['error' => 'Error al $valida datos', 'data' => [...] ]
     * ```
     */
    private function tabla_base(
        string $campo_date_contrato,
        string $campo_fecha_pago,
        string $campo_movto,
        string $campo_pago_total,
        string $entidad_empleado
    ) {
        // Validar los datos de entrada
        $valida = $this->valida_datos(
            $campo_date_contrato, $campo_fecha_pago, $campo_movto, $campo_pago_total, $entidad_empleado,
            true
        );
        if (error::$en_error) {
            return (new error())->error('Error al $valida datos', $valida);
        }

        // Generar subconsulta para meta_gestor_detalle
        $table_meta_gestor_detalle = $this->table_meta_gestor_detalle($entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al generar table_detalle', $table_meta_gestor_detalle);
        }

        // Inicializar y validar los campos requeridos
        $campos = $this->init_campos($campo_date_contrato, $campo_fecha_pago, $campo_movto, $campo_pago_total);
        if (error::$en_error) {
            return (new error())->error('Error al generar $campos', $campos);
        }

        // Definición de campos y cálculos
        $key_empleado_id = $entidad_empleado . '_id';
        $empleado_id = "pago.$key_empleado_id";
        $campo_total_pago = "SUM($campos->campo_pago_total) AS total_pago";
        $between = "BETWEEN :fecha_inicio AND :fecha_fin";

        $if_nuevo = "IF ($campos->campo_date_contrato $between, $campos->campo_pago_total, 0)";
        $if_viejo = "IF($campos->campo_date_contrato < :fecha_inicio, $campos->campo_pago_total, 0)";

        $campo_tipo_nuevo = "SUM($if_nuevo) AS tipo_nuevo";
        $campo_tipo_viejo = "SUM($if_viejo) AS tipo_viejo";
        $campos_sql = "$empleado_id, $campo_total_pago, $campo_tipo_nuevo, $campo_tipo_viejo";

        // Definición de uniones y condiciones
        $meta_gestor_detalle = "($table_meta_gestor_detalle) AS meta_gestor_detalle";
        $on_contrato_id = "meta_gestor_detalle.contrato_id = pago.contrato_id";
        $on_empleado_id = "pago.$key_empleado_id = meta_gestor_detalle.$key_empleado_id";
        $lj_mgd = "$meta_gestor_detalle ON $on_contrato_id AND $on_empleado_id";
        $lj_contrato = "contrato ON contrato.id = pago.contrato_id";
        $from = "FROM pago LEFT JOIN $lj_mgd LEFT JOIN $lj_contrato";

        // Condiciones WHERE
        $status = "pago.`status` = 'activo'";
        $fecha_pago = "$campos->campo_fecha_pago BETWEEN :fecha_inicio AND :fecha_fin";
        $total = "$campos->campo_pago_total > 0";
        $movto = "$campos->campo_movto = 'Abono'";
        $contrato_id = "meta_gestor_detalle.contrato_id IS NULL";
        $where = "$status AND $fecha_pago AND $total AND $movto AND $contrato_id";

        // Generar y devolver la consulta SQL
        return /** @lang MYSQL */ "SELECT $campos_sql $from WHERE $where GROUP BY $empleado_id";
    }



    /**
     * EM3
     * Genera una consulta SQL que combina detalles de metas y pagos.
     *
     * Esta función valida los datos de entrada, genera consultas intermedias para los
     * detalles de metas y pagos, y las combina con la tabla principal `meta_gestor_concentrado`.
     *
     * @param string $campo_date_contrato El nombre del campo de fecha del contrato.
     * @param string $campo_fecha_pago El nombre del campo de fecha de pago.
     * @param string $campo_movto El nombre del campo de movimiento.
     * @param string $campo_pago_total El nombre del campo de total de pago.
     * @param string $entidad_empleado El nombre de la entidad del empleado.
     *
     * @return string|array Devuelve la consulta SQL generada como cadena. Si ocurre un error
     *                      en cualquier paso, devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Parámetros válidos
     * $query = $this->table_base_2(
     *     'contrato.fecha_inicio',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total',
     *     'empleado'
     * );
     * // Resultado:
     * // SELECT meta_gestor_concentrado.id AS 'meta_gestor_concentrado_id',
     * //        meta_gestor_concentrado.empleado_id,
     * //        meta_gestor_concentrado.meta_proceso,
     * //        detalle.monto_proceso,
     * //        detalle.monto_de_mas,
     * //        pagos.total_tipo_nuevo,
     * //        pagos.total_tipo_viejo
     * // FROM meta_gestor_concentrado
     * // LEFT JOIN (subconsulta de table_detalle) AS detalle
     * // ON detalle.empleado_id = meta_gestor_concentrado.empleado_id
     * // LEFT JOIN (subconsulta de table_pagos) AS pagos
     * // ON pagos.empleado_id = meta_gestor_concentrado.empleado_id
     * // WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id
     *
     * // Ejemplo 2: Error en los datos de entrada
     * $query = $this->table_base_2('', 'pago.fecha', 'pago.movimiento', 'pago.total', 'empleado');
     * // Resultado:
     * // ['error' => 'Error al $valida datos', 'data' => [...] ]
     * ```
     */
    private function table_base_2(
        string $campo_date_contrato,
        string $campo_fecha_pago,
        string $campo_movto,
        string $campo_pago_total,
        string $entidad_empleado
    ) {
        // Validar los datos de entrada
        $valida = $this->valida_datos(
            $campo_date_contrato,
            $campo_fecha_pago,
            $campo_movto,
            $campo_pago_total,
            $entidad_empleado,
            true
        );
        if (error::$en_error) {
            return (new error())->error('Error al $valida datos', $valida);
        }

        // Generar la consulta para detalles de metas
        $table_detalle = $this->table_detalle($entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al generar table_detalle', $table_detalle);
        }

        // Generar la consulta para pagos
        $table_pagos = $this->table_pagos(
            $campo_date_contrato,
            $campo_fecha_pago,
            $campo_movto,
            $campo_pago_total,
            $entidad_empleado
        );
        if (error::$en_error) {
            return (new error())->error('Error al generar $table_pagos', $table_pagos);
        }

        // Definir los campos y alias
        $key_empleado_id = $entidad_empleado . '_id';
        $meta_gestor_concentrado_id = "meta_gestor_concentrado.id AS 'meta_gestor_concentrado_id'";
        $empleado_id = "meta_gestor_concentrado.$key_empleado_id";
        $meta_proceso = "meta_gestor_concentrado.meta_proceso";
        $monto_proceso = "detalle.monto_proceso";
        $monto_de_mas = "detalle.monto_de_mas";
        $total_tipo_nuevo = "pagos.total_tipo_nuevo";
        $total_tipo_viejo = "pagos.total_tipo_viejo";

        // Subconsultas como tablas
        $detalle = "($table_detalle) AS detalle";
        $pagos = "($table_pagos) AS pagos";

        // Definir las condiciones de unión
        $on_detalle = "detalle.$key_empleado_id = meta_gestor_concentrado.$key_empleado_id";
        $on_pagos = "pagos.$key_empleado_id = meta_gestor_concentrado.$key_empleado_id";

        // Definir la cláusula FROM con uniones
        $from = "FROM meta_gestor_concentrado LEFT JOIN $detalle ON $on_detalle LEFT JOIN $pagos ON $on_pagos";

        // Definir la condición WHERE
        $where = "WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id";

        // Construir los campos seleccionados
        $campos = "$meta_gestor_concentrado_id, $empleado_id, $meta_proceso, $monto_proceso, $monto_de_mas,";
        $campos .= " $total_tipo_nuevo, $total_tipo_viejo";

        // Retornar la consulta SQL final
        return /** @lang MYSQL */ "SELECT $campos $from $where";
    }


    /**
     * EM3
     * Genera una consulta SQL para obtener detalles de metas del gestor.
     *
     * Esta función construye una consulta SQL para calcular sumas relacionadas con
     * los campos `monto_proceso` y `monto_de_mas` agrupados por el empleado. Valida
     * que el parámetro `$entidad_empleado` no esté vacío y construye dinámicamente
     * las columnas y condiciones de la consulta.
     *
     * @param string $entidad_empleado El nombre de la entidad del empleado, que se utiliza
     *                                 para generar dinámicamente las claves y agrupar los datos.
     *
     * @return string|array Devuelve la consulta SQL generada como cadena. Si el
     *                      parámetro `$entidad_empleado` está vacío, devuelve un
     *                      array con el error correspondiente.
     *
     * @example
     * ```php
     * // Ejemplo 1: Parámetro válido
     * $query = $this->table_detalle('empleado');
     * // Resultado:
     * // SELECT empleado_id,
     * //        SUM(meta_gestor_detalle.monto_proceso) AS monto_proceso,
     * //        SUM(meta_gestor_detalle.monto_de_mas) AS monto_de_mas
     * // FROM meta_gestor_detalle
     * // INNER JOIN meta_gestor_concentrado
     * // ON meta_gestor_detalle.meta_gestor_concentrado_id = meta_gestor_concentrado.id
     * // WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id
     * // GROUP BY empleado_id
     *
     * // Ejemplo 2: Parámetro vacío
     * $query = $this->table_detalle('');
     * // Resultado:
     * // ['error' => 'Error entidad empleado esta vacia', 'data' => '']
     * ```
     */
    private function table_detalle(string $entidad_empleado)
    {
        // Eliminar espacios en blanco del parámetro
        $entidad_empleado = trim($entidad_empleado);

        // Validar que el parámetro no esté vacío
        if ($entidad_empleado === '') {
            return (new error())->error('Error entidad empleado esta vacia', $entidad_empleado);
        }

        // Definiciones de campos y sumas
        $sum_monto_proceso = "SUM( meta_gestor_detalle.monto_proceso ) AS monto_proceso";
        $sum_monto_de_mas = "SUM( meta_gestor_detalle.monto_de_mas ) AS monto_de_mas";

        // Condiciones de unión y filtro
        $on = "meta_gestor_detalle.meta_gestor_concentrado_id = meta_gestor_concentrado.id ";
        $where = "meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id";

        // Construcción dinámica del campo clave
        $key_empleado = $entidad_empleado . '_id';

        // Definición de campos y origen
        $campos = "$key_empleado, $sum_monto_proceso, $sum_monto_de_mas";
        $from = "meta_gestor_detalle INNER JOIN meta_gestor_concentrado ON $on";

        // Retornar la consulta SQL generada
        return /** @lang MYSQL */ "SELECT $campos FROM $from WHERE $where GROUP BY $key_empleado ";
    }


    /**
     * EM3
     * Genera una consulta SQL para obtener detalles de la meta del gestor.
     *
     * Esta función construye una consulta SQL que incluye una cláusula `LEFT JOIN`
     * entre las tablas `meta_gestor_detalle` y `meta_gestor_concentrado`. Valida que
     * el parámetro `$entidad_empleado` no esté vacío y utiliza este valor para construir
     * dinámicamente las columnas y condiciones de la consulta.
     *
     * @param string $entidad_empleado El nombre de la entidad del empleado.
     *                                 Este valor debe incluir el sufijo `_id`.
     *
     * @return string|array Devuelve una cadena SQL con la consulta generada. Si el
     *                      parámetro `$entidad_empleado` está vacío, devuelve un
     *                      array de error con el mensaje correspondiente.
     *
     * @example
     * ```php
     * // Ejemplo 1: Entidad válida
     * $query = $this->table_meta_gestor_detalle('empleado');
     * // Resultado:
     * // SELECT mgd.contrato_id, mgc.empleado_id
     * // FROM meta_gestor_detalle AS mgd
     * // LEFT JOIN meta_gestor_concentrado AS mgc
     * // ON mgd.meta_gestor_concentrado_id = mgc.id
     * // WHERE mgc.meta_gestor_id = :meta_gestor_id
     * // GROUP BY mgc.empleado_id, mgd.contrato_id
     *
     * // Ejemplo 2: Entidad vacía
     * $query = $this->table_meta_gestor_detalle('');
     * // Resultado:
     * // ['error' => 'Error $entidad_empleado esta vacia', 'data' => '']
     * ```
     */
    private function table_meta_gestor_detalle(string $entidad_empleado)
    {
        // Elimina espacios en blanco del parámetro
        $entidad_empleado = trim($entidad_empleado);

        // Valida que el parámetro no esté vacío
        if ($entidad_empleado === '') {
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }

        // Construye dinámicamente las columnas y claves basadas en la entidad del empleado
        $key_empleado_id = $entidad_empleado . '_id';

        $contrato_id = "mgd.contrato_id";
        $empleado_id = "mgc.$key_empleado_id";

        // Cláusulas SQL
        $on = "ON mgd.meta_gestor_concentrado_id = mgc.id";
        $where = "WHERE mgc.meta_gestor_id = :meta_gestor_id";
        $mgd = "meta_gestor_detalle AS mgd";
        $mgc = "meta_gestor_concentrado AS mgc";

        // Campos y agrupaciones
        $campos = "$contrato_id, $empleado_id";
        $group = "GROUP BY $empleado_id, $contrato_id";

        // Retorna la consulta SQL generada
        return /** @lang MYSQL */ "SELECT $campos FROM $mgd LEFT JOIN $mgc $on $where $group";
    }




    /**
     * EM3
     * Genera una consulta SQL para calcular totales de pagos clasificados como "nuevo" y "viejo".
     *
     * Esta función valida los datos de entrada, utiliza la función `tabla_base` para generar
     * una tabla intermedia y construye una consulta SQL que agrupa y ordena los resultados
     * por empleado, calculando los totales de pagos según su tipo.
     *
     * @param string $campo_date_contrato El nombre del campo de fecha del contrato.
     * @param string $campo_fecha_pago El nombre del campo de fecha de pago.
     * @param string $campo_movto El nombre del campo de movimiento.
     * @param string $campo_pago_total El nombre del campo de total de pago.
     * @param string $entidad_empleado El nombre de la entidad del empleado.
     *
     * @return string|array Devuelve la consulta SQL generada como cadena. Si ocurre un error
     *                      en cualquier paso, devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Parámetros válidos
     * $query = $this->table_pagos(
     *     'contrato.fecha_inicio',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total',
     *     'empleado'
     * );
     * // Resultado:
     * // SELECT tabla.empleado_id,
     * //        SUM(tabla.tipo_nuevo) AS total_tipo_nuevo,
     * //        SUM(tabla.tipo_viejo) AS total_tipo_viejo
     * // FROM (
     * //     (consulta generada por tabla_base)
     * // ) AS tabla
     * // GROUP BY tabla.empleado_id
     * // ORDER BY tabla.empleado_id
     *
     * // Ejemplo 2: Error en los datos de entrada
     * $query = $this->table_pagos(
     *     '',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total',
     *     'empleado'
     * );
     * // Resultado:
     * // ['error' => 'Error al $valida datos', 'data' => [...] ]
     * ```
     */
    private function table_pagos(
        string $campo_date_contrato,
        string $campo_fecha_pago,
        string $campo_movto,
        string $campo_pago_total,
        string $entidad_empleado
    ) {
        // Validar los datos de entrada
        $valida = $this->valida_datos(
            $campo_date_contrato,
            $campo_fecha_pago,
            $campo_movto,
            $campo_pago_total,
            $entidad_empleado,
            true
        );
        if (error::$en_error) {
            return (new error())->error('Error al $valida datos', $valida);
        }

        // Generar la tabla base para cálculos
        $tabla_base = $this->tabla_base(
            $campo_date_contrato,
            $campo_fecha_pago,
            $campo_movto,
            $campo_pago_total,
            $entidad_empleado
        );
        if (error::$en_error) {
            return (new error())->error('Error al generar table_pagos', $tabla_base);
        }

        // Definir los campos para la consulta
        $key_empleado_id = $entidad_empleado . '_id';
        $empleado_id = "tabla.$key_empleado_id";
        $tipo_nuevo = "SUM(tabla.tipo_nuevo) AS total_tipo_nuevo";
        $tipo_viejo = "SUM(tabla.tipo_viejo) AS total_tipo_viejo";

        $campos = "$empleado_id, $tipo_nuevo, $tipo_viejo";

        // Construir la consulta SQL
        $from_t = "FROM ($tabla_base) AS tabla";

        // Retornar la consulta SQL final
        return /** @lang MYSQL */ "SELECT $campos $from_t GROUP BY $empleado_id ORDER BY $empleado_id";
    }



    /**
     * EM3
     * Recalcula las metas del gestor mediante una consulta SQL `UPDATE`.
     *
     * Esta función valida los datos de entrada, genera las cláusulas `SET` y `JOIN` necesarias,
     * y construye una consulta SQL para actualizar los datos en la tabla `meta_gestor_concentrado`.
     *
     * @param string $campo_date_contrato El nombre del campo de fecha del contrato.
     * @param string $campo_fecha_pago El nombre del campo de fecha de pago.
     * @param string $campo_movto El nombre del campo de movimiento.
     * @param string $campo_pago_total El nombre del campo de total de pago.
     * @param string $entidad_empleado El nombre de la entidad del empleado.
     *
     * @return string|array Devuelve la consulta SQL `UPDATE` generada como una cadena. Si ocurre un error
     *                      en cualquier paso, devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Parámetros válidos
     * $query = $this->recalcula_meta(
     *     'contrato.fecha_inicio',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total',
     *     'empleado'
     * );
     * // Resultado:
     * // UPDATE meta_gestor_concentrado
     * // LEFT JOIN (subconsulta de join_upd) AS tabla2
     * // SET meta_gestor_concentrado.meta_proceso = tabla2.monto_proceso,
     * //     meta_gestor_concentrado.monto_de_mas = tabla2.monto_de_mas,
     * //     meta_gestor_concentrado.monto_nuevo = tabla2.total_tipo_nuevo,
     * //     meta_gestor_concentrado.monto_contratos_extra = tabla2.total_tipo_viejo
     * // WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id
     *
     * // Ejemplo 2: Error en los datos de entrada
     * $query = $this->recalcula_meta('', 'pago.fecha', 'pago.movimiento', 'pago.total', 'empleado');
     * // Resultado:
     * // ['error' => 'Error al $valida datos', 'data' => [...]]
     *
     * // Ejemplo 3: Error al generar campos SET
     * $query = $this->recalcula_meta(
     *     'contrato.fecha_inicio',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total',
     *     'empleado'
     * );
     * // Resultado:
     * // ['error' => 'Error al generar campos', 'data' => [...]]
     * ```
     */
    final public function recalcula_meta(
        string $campo_date_contrato,
        string $campo_fecha_pago,
        string $campo_movto,
        string $campo_pago_total,
        string $entidad_empleado
    ) {
        // Validar los datos de entrada
        $valida = $this->valida_datos(
            $campo_date_contrato,
            $campo_fecha_pago,
            $campo_movto,
            $campo_pago_total,
            $entidad_empleado,
            true
        );
        if (error::$en_error) {
            return (new error())->error('Error al $valida datos', $valida);
        }

        // Generar los campos SET
        $campos_set = $this->campos_set();
        if (error::$en_error) {
            return (new error())->error('Error al generar campos', $campos_set);
        }

        // Generar la cláusula JOIN
        $join = $this->join_upd(
            $campo_date_contrato,
            $campo_fecha_pago,
            $campo_movto,
            $campo_pago_total,
            $entidad_empleado
        );
        if (error::$en_error) {
            return (new error())->error('Error al generar $join', $join);
        }

        // Generar la cláusula WHERE
        $statement = $this->statement_mgc();

        // Construir y retornar la consulta SQL
        return /** @lang MYSQL */ "UPDATE meta_gestor_concentrado LEFT JOIN $join SET $campos_set WHERE $statement";
    }


    /**
     * EM3
     * Genera una condición SQL para una cláusula `WHERE`.
     *
     * Esta función devuelve una cadena que representa una condición SQL básica
     * que compara el campo `meta_gestor_id` de la tabla `meta_gestor_concentrado`
     * con un marcador de posición (`:meta_gestor_id`), generalmente utilizada
     * en una cláusula `WHERE` para filtrar datos por `meta_gestor_id`.
     *
     * @return string Devuelve una cadena que representa la condición SQL.
     *
     * @example
     * ```php
     * // Ejemplo 1: Usar la condición en una consulta SELECT
     * $where_condition = $this->statement_mgc();
     * $query = "SELECT * FROM meta_gestor_concentrado WHERE $where_condition";
     * // Resultado:
     * // SELECT * FROM meta_gestor_concentrado WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id
     *
     * // Ejemplo 2: Usar la condición en una consulta UPDATE
     * $set_clause = "meta_gestor_concentrado.meta_proceso = tabla2.monto_proceso";
     * $where_condition = $this->statement_mgc();
     * $query = "UPDATE meta_gestor_concentrado SET $set_clause WHERE $where_condition";
     * // Resultado:
     * // UPDATE meta_gestor_concentrado SET meta_gestor_concentrado.meta_proceso = tabla2.monto_proceso
     * // WHERE meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id
     * ```
     */
    private function statement_mgc(): string
    {
        // Retorna la condición para el campo meta_gestor_id
        return "meta_gestor_concentrado.meta_gestor_id = :meta_gestor_id";
    }



    /**
     * EM3
     * Valida los datos esenciales para operaciones relacionadas con contratos y pagos.
     *
     * Esta función realiza una serie de validaciones para asegurarse de que los parámetros
     * requeridos no estén vacíos. Si alguno de los campos es inválido, devuelve un array de error.
     *
     * @param string $campo_date_contrato El nombre del campo de fecha del contrato a validar.
     * @param string $campo_fecha_pago El nombre del campo de fecha de pago a validar.
     * @param string $campo_movto El nombre del campo de movimiento a validar.
     * @param string $campo_pago_total El nombre del campo que representa el total del pago.
     * @param string $entidad_empleado El nombre de la entidad del empleado a validar.
     * @param bool $valida_entidad_empleado Indica si se debe validar la entidad del empleado.
     *
     * @return bool|array Devuelve `true` si todas las validaciones son exitosas,
     *                    o un array de error si alguna falla.
     *
     * @example
     * ```php
     * // Ejemplo 1: Todos los parámetros válidos
     * $resultado = $this->valida_datos(
     *     'contrato.fecha_inicio',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total',
     *     'empleado',
     *     true
     * );
     * // Resultado: true
     *
     * // Ejemplo 2: Entidad del empleado vacía
     * $resultado = $this->valida_datos(
     *     'contrato.fecha_inicio',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total',
     *     '',
     *     true
     * );
     * // Resultado: ['error' => 'Error $entidad_empleado está vacía', 'data' => '']
     *
     * // Ejemplo 3: Campo de fecha de contrato vacío
     * $resultado = $this->valida_datos(
     *     '',
     *     'pago.fecha',
     *     'pago.movimiento',
     *     'pago.total',
     *     'empleado',
     *     true
     * );
     * // Resultado: ['error' => 'Error $campo_date_contrato está vacío', 'data' => '']
     * ```
     */
    final public function valida_datos(
        string $campo_date_contrato,
        string $campo_fecha_pago,
        string $campo_movto,
        string $campo_pago_total,
        string $entidad_empleado,
        bool $valida_entidad_empleado
    ) {
        // Validar entidad del empleado si aplica
        if ($valida_entidad_empleado) {
            $entidad_empleado = trim($entidad_empleado);
            if ($entidad_empleado === '') {
                return (new error())->error('Error $entidad_empleado está vacía', $entidad_empleado);
            }
        }

        // Validar campo de total de pago
        $campo_pago_total = trim($campo_pago_total);
        if ($campo_pago_total === '') {
            return (new error())->error('Error $campo_pago_total está vacío', $campo_pago_total);
        }

        // Validar campo de fecha de contrato
        $campo_date_contrato = trim($campo_date_contrato);
        if ($campo_date_contrato === '') {
            return (new error())->error('Error $campo_date_contrato está vacío', $campo_date_contrato);
        }

        // Validar campo de movimiento
        $campo_movto = trim($campo_movto);
        if ($campo_movto === '') {
            return (new error())->error('Error $campo_movto está vacío', $campo_movto);
        }

        // Validar campo de fecha de pago
        $campo_fecha_pago = trim($campo_fecha_pago);
        if ($campo_fecha_pago === '') {
            return (new error())->error('Error $campo_fecha_pago está vacío', $campo_fecha_pago);
        }

        // Si todas las validaciones pasan, retorna true
        return true;
    }


}
