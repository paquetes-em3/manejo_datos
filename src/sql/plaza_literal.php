<?php
namespace desarrollo_em3\manejo_datos\sql;
use desarrollo_em3\error\error;

class plaza_literal{

    /**
     * FIN
     * Genera una consulta SQL para obtener información de una plaza específica basada en su ID y serie.
     *
     * Esta función construye una consulta SQL para buscar una plaza en la tabla `plaza_literal`
     * que coincida con un ID de plaza y una descripción de serie proporcionados.
     *
     * @param int $plaza_id ID de la plaza que se desea consultar. Debe ser mayor a 0.
     * @param string $serie Descripción de la serie asociada a la plaza. No puede estar vacía.
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada si los parámetros son válidos.
     *                      En caso de error, devuelve un arreglo con información detallada del fallo.
     *
     * Ejemplo de uso:
     * ```php
     * $plaza_id = 123;
     * $serie = 'Serie A';
     * $sql = $this->get_plaza($plaza_id, $serie);
     * if (isset($sql['error'])) {
     *     echo $sql['error'];
     * } else {
     *     echo $sql; // Consulta SQL generada
     * }
     * ```
     *
     * Salida esperada (ejemplo):
     * ```sql
     * SELECT * FROM plaza_literal WHERE plaza_id = 123 AND descripcion = 'Serie A'
     * ```
     *
     * @throws error Si `$plaza_id` es menor o igual a 0.
     * @throws error Si `$serie` está vacío.
     */
    final public function get_plaza(int $plaza_id, string $serie)
    {
        if($plaza_id <= ''){
            return (new error())->error('Error plaza_id debe ser mayor a 0', $plaza_id);
        }
        $serie = trim($serie);
        if($serie === ''){
            return (new error())->error('Error $serie esta vacia', $serie);
        }
        return /** @lang MYSQL */ "SELECT *FROM plaza_literal WHERE plaza_id = $plaza_id AND descripcion = '$serie'";
    }

}
