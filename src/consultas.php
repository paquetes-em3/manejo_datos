<?php
namespace desarrollo_em3\manejo_datos;

use desarrollo_em3\error\error;
use desarrollo_em3\error\valida;
use PDO;
use stdClass;

class consultas
{

    /**
     * EM3
     * Obtiene los campos de una entidad (tabla) mediante una consulta SQL `DESCRIBE`.
     *
     * Esta función valida el nombre de la tabla (`$entidad`), genera una consulta `DESCRIBE` para
     * obtener la estructura de la tabla, ejecuta la consulta, y retorna los resultados como un array.
     *
     * @param string $entidad El nombre de la entidad (tabla) a describir. No debe estar vacío.
     * @param PDO $link Una instancia de PDO para ejecutar la consulta.
     *
     * @return array Devuelve un array con los campos de la tabla si la consulta es exitosa. Cada
     *               campo es representado como un objeto (`stdClass`). Si ocurre un error, retorna
     *               un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener los campos de una tabla
     * $entidad = 'empleados';
     * $campos = $this->campos($entidad, $pdo);
     * if (isset($campos['error'])) {
     *     echo "Error: " . $campos['mensaje'];
     * } else {
     *     foreach ($campos as $campo) {
     *         echo "Columna: " . $campo->Field . " Tipo: " . $campo->Type . PHP_EOL;
     *     }
     * }
     *
     * // Ejemplo 2: Error al proporcionar un nombre vacío
     * $entidad = '';
     * $campos = $this->campos($entidad, $pdo);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Tabla inexistente
     * $entidad = 'tabla_inexistente';
     * $campos = $this->campos($entidad, $pdo);
     * // Resultado:
     * // ['error' => 'Error al ejecutar sql', 'data' => [...]]
     * ```
     */
    private function campos(string $entidad, PDO $link): array
    {
        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Generar la consulta DESCRIBE
        $describe = (new sql())->describe($entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $describe);
        }

        // Ejecutar la consulta y obtener los resultados como objetos
        $exe = $this->exe_objs($link, $describe);
        if (error::$en_error) {
            return (new error())->error('Error al ejecutar sql', $exe);
        }

        // Retornar los resultados
        return $exe;
    }


    /**
     * EM3
     * Extrae los nombres de los campos (`Field`) de un array de objetos o arrays y los devuelve como un array simple.
     *
     * Esta función recorre un array de campos, valida que cada campo tenga la clave o propiedad `Field`,
     * y retorna un array con los valores de `Field`. Si algún campo no cumple con las validaciones,
     * devuelve un array con los detalles del error.
     *
     * @param array $campos Un array de campos, donde cada elemento debe ser un array o un objeto que contenga la clave `Field`.
     *
     * @return array Devuelve un array con los valores de `Field` si todos los campos son válidos. En caso de error,
     *               devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Array válido de campos
     * $campos = [
     *     (object)['Field' => 'id'],
     *     (object)['Field' => 'nombre'],
     *     (object)['Field' => 'edad']
     * ];
     * $resultado = $this->campos_array($campos);
     * print_r($resultado);
     * // Resultado:
     * // ['id', 'nombre', 'edad']
     *
     * // Ejemplo 2: Array con un campo faltante
     * $campos = [
     *     (object)['Field' => 'id'],
     *     (object)['Field' => 'nombre'],
     *     (object)[] // Falta la clave `Field`
     * ];
     * $resultado = $this->campos_array($campos);
     * // Resultado:
     * // ['error' => 'Error $campo->Field no existe', 'data' => [...]]
     *
     * // Ejemplo 3: Campo vacío
     * $campos = [
     *     (object)['Field' => 'id'],
     *     (object)['Field' => '']
     * ];
     * $resultado = $this->campos_array($campos);
     * // Resultado:
     * // ['error' => 'Error $campo->Field esta vacio', 'data' => [...]]
     * ```
     */
    private function campos_array(array $campos): array
    {
        // Inicializar el array para los nombres de campos
        $campos_array = array();

        // Recorrer cada elemento en el array de campos
        foreach ($campos as $campo) {
            // Convertir a objeto si es un array
            if (is_array($campo)) {
                $campo = (object)$campo;
            }

            // Validar que el campo tenga la propiedad `Field`
            if (!isset($campo->Field)) {
                return (new error())->error('Error $campo->Field no existe', $campo);
            }

            // Validar que el valor de `Field` no esté vacío
            if (trim($campo->Field) === '') {
                return (new error())->error('Error $campo->Field esta vacio', $campo);
            }

            // Agregar el valor de `Field` al array de resultados
            $campos_array[] = $campo->Field;
        }

        // Retornar el array con los valores de `Field`
        return $campos_array;
    }


    /**
     * EM3
     * Convierte un array de campos en un objeto donde cada campo es una propiedad con su valor como un objeto.
     *
     * Esta función recorre un array de campos, valida que cada campo contenga la clave `Field`, y construye
     * un objeto (`stdClass`) donde cada propiedad corresponde al valor de `Field`, y el valor de la propiedad
     * es el objeto original del campo.
     *
     * @param array $campos Un array de campos, donde cada elemento debe ser un array o un objeto que contenga la clave `Field`.
     *
     * @return stdClass|array Devuelve un objeto donde cada clave corresponde a un valor único de `Field`.
     *                        En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Array válido de campos
     * $campos = [
     *     (object)['Field' => 'id', 'Type' => 'int'],
     *     (object)['Field' => 'nombre', 'Type' => 'varchar(255)'],
     *     (object)['Field' => 'edad', 'Type' => 'int']
     * ];
     * $resultado = $this->campos_obj($campos);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [id] => stdClass Object (
     * //         [Field] => id
     * //         [Type] => int
     * //     )
     * //     [nombre] => stdClass Object (
     * //         [Field] => nombre
     * //         [Type] => varchar(255)
     * //     )
     * //     [edad] => stdClass Object (
     * //         [Field] => edad
     * //         [Type] => int
     * //     )
     * // )
     *
     * // Ejemplo 2: Error por falta de la clave `Field`
     * $campos = [
     *     (object)['Field' => 'id'],
     *     (object)['field' => 'nombre'] // Clave incorrecta
     * ];
     * $resultado = $this->campos_obj($campos);
     * // Resultado:
     * // ['error' => 'Error $campo->Field no existe', 'data' => [...]]
     *
     * // Ejemplo 3: Campo vacío
     * $campos = [
     *     (object)['Field' => ''],
     *     (object)['Field' => 'edad']
     * ];
     * $resultado = $this->campos_obj($campos);
     * // Resultado:
     * // ['error' => 'Error $campo->Field esta vacio', 'data' => [...]]
     * ```
     */
    private function campos_obj(array $campos)
    {
        // Inicializar el objeto que contendrá los campos
        $campos_obj = new stdClass();

        // Recorrer cada elemento en el array de campos
        foreach ($campos as $campo) {
            // Convertir a objeto si es un array
            if (is_array($campo)) {
                $campo = (object)$campo;
            }

            // Validar que el campo tenga la propiedad `Field`
            if (!isset($campo->Field)) {
                return (new error())->error('Error $campo->Field no existe', $campo);
            }

            // Validar que el valor de `Field` no esté vacío
            $field = trim($campo->Field);
            if ($field === '') {
                return (new error())->error('Error $campo->Field esta vacio', $campo);
            }

            // Agregar el campo como una propiedad del objeto resultado
            $key_obj = $campo->Field;
            $campos_obj->$key_obj = (object)$campo;
        }

        // Retornar el objeto con los campos procesados
        return $campos_obj;
    }


    /**
     * EM3
     * Procesa un conjunto de campos y devuelve un objeto con representaciones de los campos en diferentes formatos.
     *
     * Esta función toma un array de campos y el nombre de una entidad, valida los datos, y construye un objeto
     * que incluye los campos en los siguientes formatos:
     * - `campos_array`: Un array simple con los nombres de los campos (`Field`).
     * - `campos_obj`: Un objeto donde cada campo es una propiedad cuyo valor es el objeto original del campo.
     * - `campos_sql`: Una cadena SQL con los campos formateados para ser utilizados en consultas.
     *
     * @param array $campos Un array de campos, donde cada elemento debe incluir la clave `Field`.
     * @param string $entidad El nombre de la entidad (tabla) a la que pertenecen los campos.
     *
     * @return stdClass|array Devuelve un objeto con las claves:
     *                        - `campos_array`: Un array con los nombres de los campos (`Field`).
     *                        - `campos_obj`: Un objeto con los campos indexados por `Field`.
     *                        - `campos_sql`: Una cadena con los campos formateados para SQL.
     *                        En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Procesar campos válidos
     * $campos = [
     *     (object)['Field' => 'id'],
     *     (object)['Field' => 'nombre'],
     *     (object)['Field' => 'edad']
     * ];
     * $entidad = 'empleados';
     * $resultado = $this->campos_rs($campos, $entidad);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [campos_array] => ['id', 'nombre', 'edad'],
     * //     [campos_obj] => stdClass Object (
     * //         [id] => stdClass Object ( ... ),
     * //         [nombre] => stdClass Object ( ... ),
     * //         [edad] => stdClass Object ( ... )
     * //     ),
     * //     [campos_sql] => "empleados.id AS empleados_id, empleados.nombre AS empleados_nombre, empleados.edad AS empleados_edad"
     * // )
     *
     * // Ejemplo 2: Entidad vacía (error)
     * $campos = [
     *     (object)['Field' => 'id']
     * ];
     * $entidad = '';
     * $resultado = $this->campos_rs($campos, $entidad);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Campo sin clave `Field`
     * $campos = [
     *     (object)['Field' => 'id'],
     *     (object)['field' => 'nombre'] // Clave incorrecta
     * ];
     * $entidad = 'empleados';
     * $resultado = $this->campos_rs($campos, $entidad);
     * // Resultado:
     * // ['error' => 'Error al obtener $campos_sql', 'data' => [...]]
     * ```
     */
    private function campos_rs(array $campos, string $entidad)
    {
        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Generar la representación SQL de los campos
        $campos_sql = $this->campos_sql($campos, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campos_sql', $campos_sql);
        }

        // Generar el array de nombres de campos
        $campos_array = $this->campos_array($campos);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campos_sql', $campos_array);
        }

        // Generar el objeto con los campos
        $campos_obj = $this->campos_obj($campos);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campos_sql', $campos_array);
        }

        // Crear el objeto de resultados
        $rs = new stdClass();
        $rs->campos_array = $campos_array;
        $rs->campos_obj = $campos_obj;
        $rs->campos_sql = $campos_sql;

        // Retornar el objeto con los resultados
        return $rs;
    }


    /**
     * EM3
     * Genera una cadena SQL que lista los campos de una entidad con sus alias.
     *
     * Esta función toma un array de campos (con la clave `Field`) y una entidad, valida los datos,
     * y construye una cadena SQL que incluye los nombres completos de los campos con alias. Los
     * alias se generan reemplazando los puntos (`.`) en el nombre completo por guiones bajos (`_`).
     *
     * @param array $campos Un array de campos, donde cada campo debe incluir la clave `Field`.
     * @param string $entidad El nombre de la entidad (tabla) a la que pertenecen los campos.
     *
     * @return string|array Devuelve una cadena con la lista de campos formateados para SQL.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar campos SQL válidos
     * $campos = [
     *     ['Field' => 'id'],
     *     ['Field' => 'nombre'],
     *     ['Field' => 'edad']
     * ];
     * $entidad = 'empleados';
     * $resultado = $this->campos_sql($campos, $entidad);
     * echo $resultado;
     * // Resultado:
     * // empleados.id AS empleados_id, empleados.nombre AS empleados_nombre, empleados.edad AS empleados_edad
     *
     * // Ejemplo 2: Entidad vacía (error)
     * $campos = [
     *     ['Field' => 'id'],
     *     ['Field' => 'nombre']
     * ];
     * $entidad = '';
     * $resultado = $this->campos_sql($campos, $entidad);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Campo sin la clave `Field` (error)
     * $campos = [
     *     ['field' => 'id'], // Error en la clave
     *     ['Field' => 'nombre']
     * ];
     * $entidad = 'empleados';
     * $resultado = $this->campos_sql($campos, $entidad);
     * // Resultado:
     * // ['error' => 'Error al validar campo', 'data' => [...]]
     * ```
     */
    private function campos_sql(array $campos, string $entidad)
    {
        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Inicializar la cadena de campos SQL
        $campos_sql = '';

        // Recorrer cada campo y construir la declaración SQL
        foreach ($campos as $campo) {
            // Validar que el campo incluya la clave `Field`
            $keys = ['Field'];
            $valida = (new valida())->valida_keys($keys, $campo);
            if (error::$en_error) {
                return (new error())->error('Error al validar campo', $valida);
            }

            // Asegurarse de que el campo sea un objeto
            if (!is_object($campo)) {
                $campo = (object)$campo;
            }

            // Construir el nombre del campo completo con alias
            $key_obj = $campo->Field;
            $coma = (new sql())->coma($campos_sql);
            if (error::$en_error) {
                return (new error())->error('Error al obtener coma', $coma);
            }

            $campo_sql = (new sql())->campo_sql($entidad . '.' . $key_obj);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $campo_sql', $campo_sql);
            }

            // Concatenar el campo con su alias
            $campos_sql .= $coma . $campo_sql;
        }

        // Retornar la cadena de campos SQL generada
        return $campos_sql;
    }

    final public function exe_array(PDO $link, string $sql): array
    {
        // Validar que la consulta SQL no esté vacía
        if ($sql === '') {
            return (new error())->error('Error sql esta vacio', $sql);
        }

        // Ejecutar la consulta de forma segura
        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if (error::$en_error) {
            return (new error())->error('Error al ejecutar sql', $exe);
        }
        return $exe['registros'];
    }


    /**
     * EM3
     * Ejecuta una consulta SQL segura y devuelve los registros como objetos.
     *
     * Esta función utiliza una instancia de PDO para ejecutar una consulta SQL de forma segura,
     * convierte los resultados obtenidos en objetos (`stdClass`), y retorna un array con los registros.
     * Si ocurre un error en cualquier paso, devuelve un array con los detalles del error.
     *
     * @param PDO $link Una instancia de PDO para ejecutar la consulta SQL.
     * @param string $sql La consulta SQL a ejecutar. No debe estar vacía.
     *
     * @return array Devuelve un array con los registros convertidos en objetos (`stdClass`) si la consulta
     *               se ejecuta correctamente. En caso de error, devuelve un array con las claves:
     *               - `error`: Un indicador de error.
     *               - `mensaje`: Detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Ejecutar una consulta válida
     * $sql = "SELECT * FROM empleados WHERE activo = 1";
     * $resultado = $this->exe_objs($pdo, $sql);
     * if (isset($resultado['error'])) {
     *     echo "Error: " . $resultado['mensaje'];
     * } else {
     *     foreach ($resultado as $registro) {
     *         echo "Empleado: " . $registro->nombre;
     *     }
     * }
     *
     * // Ejemplo 2: Consulta vacía
     * $sql = "";
     * $resultado = $this->exe_objs($pdo, $sql);
     * // Resultado:
     * // ['error' => 'Error sql esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error al ejecutar la consulta
     * $sql = "SELECT * FROM tabla_inexistente";
     * $resultado = $this->exe_objs($pdo, $sql);
     * // Resultado:
     * // ['error' => 'Error al ejecutar sql', 'data' => [...]]
     * ```
     */
    final public function exe_objs(PDO $link, string $sql): array
    {
        // Validar que la consulta SQL no esté vacía
        if ($sql === '') {
            return (new error())->error('Error sql esta vacio', $sql);
        }

        // Ejecutar la consulta de forma segura
        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if (error::$en_error) {
            return (new error())->error('Error al ejecutar sql', $exe);
        }

        // Convertir los registros en objetos
        $registros = $this->registros_obj($exe['registros']);
        if (error::$en_error) {
            return (new error())->error('Error al convertir en objetos', $registros);
        }

        // Retornar los registros como objetos
        return $registros;
    }


    /**
     * EM3
     * Verifica si un registro existe en una entidad (tabla) en la base de datos.
     *
     * Esta función valida los parámetros proporcionados, genera una consulta SQL para contar registros
     * donde un campo específico tiene un valor determinado, ejecuta la consulta y devuelve un valor
     * booleano que indica si el registro existe.
     *
     * @param string $campo El nombre del campo que se usará en la condición `WHERE`. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla) donde se realizará la búsqueda. No debe estar vacía.
     * @param PDO $link Una instancia de PDO para ejecutar la consulta.
     * @param string $value El valor que se buscará en el campo. No debe estar vacío.
     *
     * @return bool|array Devuelve `true` si el registro existe y `false` si no existe. En caso de error,
     *                    devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Verificar si un registro existe
     * $campo = 'status';
     * $entidad = 'empleados';
     * $value = 'activo';
     * $resultado = $this->existe($campo, $entidad, $pdo, $value);
     * echo $resultado ? 'Existe' : 'No existe';
     * // Resultado: Existe (o No existe)
     *
     * // Ejemplo 2: Campo vacío (error)
     * $campo = '';
     * $entidad = 'empleados';
     * $value = 'activo';
     * $resultado = $this->existe($campo, $entidad, $pdo, $value);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Tabla inexistente (error)
     * $campo = 'status';
     * $entidad = 'tabla_inexistente';
     * $value = 'activo';
     * $resultado = $this->existe($campo, $entidad, $pdo, $value);
     * // Resultado:
     * // ['error' => 'Error al ejecutar sql', 'data' => [...]]
     * ```
     */
    final public function existe(string $campo, string $entidad, PDO $link, string $value)
    {
        // Elimina espacios en blanco alrededor de los parámetros
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        $campo = trim($campo);
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacia', $campo);
        }

        $value = trim($value);
        if ($value === '') {
            return (new error())->error('Error $value esta vacia', $value);
        }

        // Genera la consulta SQL para contar registros
        $sql = (new sql())->n_rows($campo, $entidad, $value);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $sql);
        }

        // Ejecuta la consulta SQL
        $exe = $this->exe_objs($link, $sql);
        if (error::$en_error) {
            return (new error())->error('Error al ejecutar sql', $exe);
        }

        // Determina si el registro existe
        $existe = false;
        if ($exe[0]->n_rows > 0) {
            $existe = true;
        }

        return $existe;
    }


    /**
     * TRASLADADO
     * Verifica la existencia de un registro específico en una tabla de la base de datos.
     *
     * Esta función realiza los siguientes pasos:
     * 1. Valida que el nombre de la entidad no esté vacío.
     * 2. Genera una consulta SQL para verificar la existencia del registro en la tabla especificada.
     * 3. Ejecuta la consulta utilizando una conexión PDO y devuelve `true` si el registro existe o `false` en caso contrario.
     *
     * @param string $entidad Nombre de la tabla en la base de datos donde se verificará la existencia del registro.
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $registro_id ID del registro que se desea verificar. Debe ser mayor a 0.
     *
     * @return bool|array Devuelve `true` si el registro existe, `false` si no existe, o un arreglo de error en caso de fallo.
     *
     * Errores posibles:
     * - `['error' => 'Error $entidad esta vacia', 'detalle' => $entidad]` si el nombre de la entidad está vacío.
     * - `['error' => 'Error $registro_id debe ser mayor a 0', 'detalle' => $registro_id]` si el ID del registro es menor o igual a 0.
     * - `['error' => 'Error al generar sql', 'detalle' => $sql]` si ocurre un fallo al generar la consulta SQL.
     * - `['error' => 'Error al ejecutar sql', 'detalle' => $exe]` si ocurre un fallo al ejecutar la consulta SQL.
     */
    final public function existe_by_id(string $entidad, PDO $link, int $registro_id )
    {
        $entidad = trim($entidad);
        if($entidad === ''){
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }
        if($registro_id <= 0){
            return (new error())->error('Error $registro_id debe ser mayor a 0', $registro_id);
        }
        $sql = (new sql())->existe_by_id($entidad,$registro_id);
        if(error::$en_error){
            return (new error())->error('Error al generar sql',$sql);
        }

        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al ejecutar sql',$exe);
        }
        $existe = false;
        if((int)$exe['registros'][0]['n_rows'] > 0){
            $existe = true;
        }
        return $existe;

    }

    /**
     * EM3
     * Obtiene y procesa los campos de una entidad desde la base de datos.
     *
     * Esta función toma el nombre de una entidad (tabla) y una conexión PDO, valida los datos,
     * obtiene los campos de la entidad mediante una consulta `DESCRIBE`, y procesa los resultados
     * para generar diferentes representaciones de los campos:
     * - `campos_array`: Un array con los nombres de los campos (`Field`).
     * - `campos_obj`: Un objeto donde cada campo es una propiedad indexada por `Field`.
     * - `campos_sql`: Una cadena SQL con los campos formateados y alias.
     *
     * @param string $entidad El nombre de la entidad (tabla) de la cual se obtendrán los campos. No debe estar vacío.
     * @param PDO $link Una instancia de PDO para ejecutar las consultas SQL.
     *
     * @return stdClass|array Devuelve un objeto con las representaciones de los campos si todo es exitoso:
     *                        - `campos_array`: Array de nombres de los campos.
     *                        - `campos_obj`: Objeto con los campos indexados por `Field`.
     *                        - `campos_sql`: Cadena SQL con los campos formateados.
     *                        En caso de error, devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener campos de una tabla válida
     * $entidad = 'empleados';
     * $resultado = $this->get_campos($entidad, $pdo);
     * if (isset($resultado->error)) {
     *     echo "Error: " . $resultado['mensaje'];
     * } else {
     *     print_r($resultado->campos_array); // Array de nombres de campos
     *     print_r($resultado->campos_obj);  // Objeto de campos
     *     echo $resultado->campos_sql;      // Cadena SQL
     * }
     *
     * // Ejemplo 2: Entidad vacía
     * $entidad = '';
     * $resultado = $this->get_campos($entidad, $pdo);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Tabla inexistente
     * $entidad = 'tabla_inexistente';
     * $resultado = $this->get_campos($entidad, $pdo);
     * // Resultado:
     * // ['error' => 'Error al obtener campos', 'data' => [...]]
     * ```
     */
    final public function get_campos(string $entidad, PDO $link)
    {
        // Validar que el nombre de la entidad no esté vacío
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Obtener los campos de la entidad mediante consulta DESCRIBE
        $campos = $this->campos($entidad, $link);
        if (error::$en_error) {
            return (new error())->error('Error al obtener campos', $campos);
        }

        // Procesar los campos en diferentes formatos
        $rs = $this->campos_rs($campos, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $rs', $rs);
        }

        // Retornar el objeto con las representaciones de los campos
        return $rs;
    }



    /**
     * EM3
     * Obtiene un registro específico de una entidad en la base de datos.
     *
     * Esta función realiza una consulta SQL para recuperar un registro específico de una tabla en la base de datos,
     * utilizando su ID como filtro principal. Los resultados se pueden devolver como un arreglo asociativo o como un objeto.
     *
     * @param stdClass $columnas Un objeto que define las columnas que se deben incluir en la consulta.
     *                           Debe contener nombres de columnas válidos de la entidad proporcionada.
     * @param string $entidad Nombre de la tabla en la base de datos desde la que se obtendrá el registro.
     *                        Debe ser un nombre válido y no estar vacío.
     * @param int $registro_id ID del registro que se desea seleccionar. Debe ser mayor a 0.
     * @param PDO $link Conexión activa a la base de datos mediante PDO.
     * @param bool $rs_array (Opcional) Si es `true`, devuelve el resultado como un arreglo asociativo.
     *                       Si es `false`, devuelve el resultado como un objeto (`stdClass`). Por defecto es `true`.
     * @param string $sql_where_extra (Opcional) Condiciones adicionales para el filtro SQL.
     *                                Estas condiciones se concatenarán con el operador `AND`.
     *
     * @return array|stdClass Devuelve el registro solicitado como un arreglo asociativo o como un objeto, dependiendo de `$rs_array`.
     *                        En caso de error, devuelve un objeto `error`.
     *
     * @throws error Retorna un error si:
     *         - `$entidad` está vacío.
     *         - `$registro_id` es menor o igual a 0.
     *         - Ocurre un fallo al generar la consulta SQL.
     *         - La consulta no encuentra registros.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener un registro en formato de arreglo asociativo
     * $columnas = new stdClass();
     * $columnas->campo1 = 'campo1';
     * $columnas->campo2 = 'campo2';
     *
     * $entidad = 'mi_tabla';
     * $registro_id = 123;
     * $link = new PDO('mysql:host=localhost;dbname=mi_base', 'usuario', 'password');
     *
     * $registro = $this->registro_bruto($columnas, $entidad, $registro_id, $link, true);
     *
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al obtener el registro.";
     * } else {
     *     print_r($registro); // Muestra el registro solicitado en formato array asociativo.
     * }
     * ```
     *
     * @example
     * ```php
     * // Ejemplo 2: Obtener un registro como un objeto stdClass
     * $registro = $this->registro_bruto($columnas, $entidad, $registro_id, $link, false);
     * print_r($registro); // Muestra el registro solicitado en formato objeto.
     * ```
     *
     * @example
     * ```php
     * // Ejemplo 3: Error al buscar un registro inexistente
     * $registro_id = 9999; // ID no existente
     * $registro = $this->registro_bruto($columnas, $entidad, $registro_id, $link);
     *
     * if (isset($registro['error'])) {
     *     echo "Error: " . $registro['mensaje']; // Mensaje de error
     * }
     * ```
     *
     * Proceso:
     * 1. Valida que `$entidad` no esté vacío.
     * 2. Valida que `$registro_id` sea mayor a 0.
     * 3. Genera la consulta SQL llamando a `select_by_id`.
     * 4. Ejecuta la consulta SQL utilizando la conexión PDO proporcionada.
     * 5. Valida que exista al menos un registro en el resultado.
     * 6. Devuelve el registro como un arreglo o como un objeto, dependiendo del valor de `$rs_array`.
     *
     * Consideraciones:
     * - Asegúrate de que `$columnas` contenga nombres de columnas válidos y correctamente mapeados.
     * - Maneja la conexión a la base de datos con cuidado para evitar errores de conexión.
     * - Si `$sql_where_extra` no es seguro, podría dar lugar a inyecciones SQL.
     *
     * Errores posibles:
     * - `['error' => 'Error $entidad esta vacia', 'detalle' => $entidad]` si `$entidad` está vacío.
     * - `['error' => 'Error $registro_id es menor a 0', 'detalle' => $registro_id]` si `$registro_id` es menor o igual a 0.
     * - `['error' => 'Error al obtener sql', 'detalle' => $sql]` si falla la generación de la consulta SQL.
     * - `['error' => 'Error no existe el registro', 'detalle' => $exe]` si no se encuentra ningún registro.
     */
    final public function registro_bruto(
        stdClass $columnas,
        string $entidad,
        int $registro_id,
        PDO $link,
        bool $rs_array = true,
        string $sql_where_extra = ''
    ) {
        // Valida que $entidad no esté vacío
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Valida que $registro_id sea mayor a 0
        if ($registro_id <= 0) {
            return (new error())->error('Error $registro_id es menor a 0', $registro_id);
        }

        // Genera la consulta SQL
        $sql = (new sql())->select_by_id($columnas, $entidad, $registro_id, $sql_where_extra);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $sql);
        }

        // Ejecuta la consulta SQL
        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if (error::$en_error) {
            return (new error())->error('Error al ejecutar sql', $exe);
        }

        // Valida que exista al menos un registro
        if ((int)$exe['n_registros'] === 0) {
            return (new error())->error('Error no existe el registro', $exe);
        }

        // Obtiene el primer registro del resultado
        $result = $exe['registros'][0];

        // Convierte a objeto si $rs_array es falso
        if (!$rs_array) {
            $result = (object)$result;
        }

        return $result;
    }



    /**
     * EM3
     * Convierte un array de registros asociativos en un array de objetos.
     *
     * Esta función recorre un array de registros, donde cada registro es un array asociativo,
     * y los convierte en objetos (`stdClass`). Devuelve un array con los registros convertidos.
     *
     * @param array $registros Un array de registros asociativos, donde cada registro es un array clave-valor.
     *
     * @return array Devuelve un array donde cada registro es un objeto (`stdClass`).
     *
     * @example
     * ```php
     * // Ejemplo 1: Convertir registros en objetos
     * $registros = [
     *     ['id' => 1, 'nombre' => 'Juan', 'edad' => 30],
     *     ['id' => 2, 'nombre' => 'Ana', 'edad' => 25]
     * ];
     * $resultado = $this->registros_obj($registros);
     * // Resultado:
     * // [
     * //     (object)['id' => 1, 'nombre' => 'Juan', 'edad' => 30],
     * //     (object)['id' => 2, 'nombre' => 'Ana', 'edad' => 25]
     * // ]
     *
     * // Ejemplo 2: Array vacío
     * $registros = [];
     * $resultado = $this->registros_obj($registros);
     * // Resultado:
     * // []
     * ```
     */
    private function registros_obj(array $registros): array
    {
        // Convertir cada registro en un objeto
        foreach ($registros as $indice => $registro) {
            $registros[$indice] = (object)$registro;
        }

        // Retornar el array de objetos
        return $registros;
    }


    /**
     * EM3
     * Obtiene filas de una tabla en función de un filtro, con soporte para campos adicionales y uniones SQL.
     *
     * Esta función valida los parámetros de entrada, genera dinámicamente una consulta SQL `SELECT`,
     * la ejecuta y devuelve los registros resultantes como un array de objetos.
     *
     * @param string $entidad El nombre de la entidad (tabla) base. No debe estar vacío.
     * @param string $filtro_sql La cláusula `WHERE` que define los filtros de la consulta. No debe estar vacía.
     * @param PDO $link Una conexión PDO válida para ejecutar la consulta.
     * @param array $campos_extra Un array opcional de campos adicionales que deben incluirse en la consulta.
     * @param array $joins Un array opcional de configuraciones para generar las uniones SQL (`LEFT JOIN`).
     *
     * @return array Devuelve un array de registros obtenidos como objetos. En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener registros con uniones y campos extra
     * $entidad = 'empleados';
     * $filtro_sql = 'empleados.status = "activo"';
     * $campos_extra = ['empleados.nombre', 'departamentos.nombre AS departamento_nombre'];
     * $joins = [
     *     ['entidad_left' => 'empleados', 'entidad_right' => 'departamentos', 'rename_entidad' => 'emp']
     * ];
     * $resultado = $this->rows_by_filter($entidad, $filtro_sql, $link, $campos_extra, $joins);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     (object) ['nombre' => 'Juan', 'departamento_nombre' => 'Finanzas'],
     * //     (object) ['nombre' => 'Ana', 'departamento_nombre' => 'Recursos Humanos'],
     * // ]
     *
     * // Ejemplo 2: Error por entidad vacía
     * $entidad = '';
     * $resultado = $this->rows_by_filter($entidad, $filtro_sql, $link, $campos_extra, $joins);
     * // Resultado:
     * // ['error' => 'Error al validar datos', 'data' => [...]]
     * ```
     */
    final public function rows_by_filter(
        string $entidad,
        string $filtro_sql,
        PDO $link,
        array $campos_extra = [],
        array $joins = []
    ): array {
        // Validar los parámetros de entrada
        $valida = $this->val_filter($entidad, $filtro_sql);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Generar la consulta SQL
        $sql = (new sql())->select_by_filter($entidad, $filtro_sql, $link, $campos_extra, $joins);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $sql);
        }

        // Ejecutar la consulta y obtener los registros como objetos
        $registros = $this->exe_objs($link, $sql);
        if (error::$en_error) {
            return (new error())->error('Error al convertir en objetos', $registros);
        }

        // Retornar los registros obtenidos
        return $registros;
    }


    /**
     * TRASLADADO
     * Calcula la suma de un campo específico en una entidad filtrada.
     *
     * Esta función valida los datos de entrada, construye una consulta SQL para sumar
     * un campo específico en una entidad que cumple con un filtro SQL determinado,
     * ejecuta la consulta y devuelve la suma redondeada a dos decimales. Si hay algún
     * error durante el proceso, se devuelve un array con el error correspondiente.
     *
     * @param string $campo El nombre del campo a sumar.
     * @param string $entidad El nombre de la entidad (tabla) en la base de datos.
     * @param string $filtro_sql El filtro SQL a aplicar a la entidad.
     * @param PDO $link Conexión a la base de datos.
     *
     * @return float|array La suma del campo redondeada a dos decimales o un array de error.
     */
    final public function sum_by_filter(string $campo, string $entidad, string $filtro_sql, PDO $link){

        $valida = $this->val_filter($entidad,$filtro_sql);
        if(error::$en_error){
            return (new error())->error('Error al validar datos',$valida);
        }
        $campo = trim($campo);
        if($campo === ''){
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        $sql = (new sql())->sum_by_filter($campo, $entidad, $filtro_sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener sql',$sql);
        }

        $registros = $this->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al convertir en objetos',$registros);
        }

        if(!isset($registros[0]->total)){
            $registros[0]->total = 0;
        }

        return round($registros[0]->total,2);
    }

    /**
     * EM3
     * Obtiene las tablas del sistema desde la base de datos.
     *
     * Esta función utiliza una consulta `SHOW TABLES` para obtener las tablas disponibles en la base de datos.
     * Si las tablas ya están almacenadas en la sesión (`$_SESSION['tables_sistema']`), las recupera de allí
     * para mejorar el rendimiento. De lo contrario, ejecuta la consulta, procesa los resultados, y almacena
     * las tablas en la sesión.
     *
     * @param PDO $link Una instancia de PDO para ejecutar la consulta SQL.
     *
     * @return array Devuelve un array con los nombres de las tablas en la base de datos. En caso de error,
     *               devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener las tablas de la base de datos
     * $resultado = $this->tables_system($pdo);
     * if (isset($resultado['error'])) {
     *     echo "Error: " . $resultado['mensaje'];
     * } else {
     *     print_r($resultado);
     *     // Resultado:
     *     // ['tabla1', 'tabla2', 'tabla3']
     * }
     *
     * // Ejemplo 2: Tablas ya almacenadas en la sesión
     * $_SESSION['tables_sistema'] = ['tabla1', 'tabla2', 'tabla3'];
     * $resultado = $this->tables_system($pdo);
     * print_r($resultado);
     * // Resultado:
     * // ['tabla1', 'tabla2', 'tabla3']
     * ```
     */
    final public function tables_system(PDO $link): array
    {
        // Generar la consulta SQL para obtener las tablas
        $sql = (new sql())->show_tables();
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $sql);
        }

        // Verificar si las tablas ya están en la sesión
        if (isset($_SESSION['tables_sistema'])) {
            $tables = $_SESSION['tables_sistema'];
        } else {
            // Ejecutar la consulta para obtener las tablas
            $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
            if (error::$en_error) {
                return (new error())->error('Error al ejecutar sql', $exe);
            }

            // Procesar los resultados y extraer los nombres de las tablas
            $tables_bruto = $exe['registros'];
            $tables = array();
            foreach ($tables_bruto as $table_data) {
                foreach ($table_data as $table) {
                    $tables[] = trim($table);
                }
            }

            // Almacenar las tablas en la sesión para futuras consultas
            $_SESSION['tables_sistema'] = $tables;
        }

        // Retornar la lista de tablas
        return $tables;
    }


    /**
     * EM3
     * Valida los parámetros de una entidad y un filtro SQL.
     *
     * Esta función verifica que los valores de `$entidad` y `$filtro_sql` no estén vacíos tras ser limpiados.
     * Es útil para asegurar que los parámetros requeridos sean válidos antes de construir consultas SQL.
     *
     * @param string $entidad El nombre de la entidad (tabla) que se va a usar en la consulta. No debe estar vacío.
     * @param string $filtro_sql La cláusula SQL del filtro que se aplicará a la entidad. No debe estar vacío.
     *
     * @return bool|array Devuelve `true` si ambos parámetros son válidos. En caso de error, devuelve un array
     *                    con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Validación exitosa
     * $entidad = 'empleados';
     * $filtro_sql = 'status = "activo"';
     * $resultado = $this->val_filter($entidad, $filtro_sql);
     * echo $resultado ? 'Validación exitosa' : 'Error';
     * // Resultado:
     * // Validación exitosa
     *
     * // Ejemplo 2: Entidad vacía (error)
     * $entidad = '';
     * $filtro_sql = 'status = "activo"';
     * $resultado = $this->val_filter($entidad, $filtro_sql);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Filtro SQL vacío (error)
     * $entidad = 'empleados';
     * $filtro_sql = '';
     * $resultado = $this->val_filter($entidad, $filtro_sql);
     * // Resultado:
     * // ['error' => 'Error $filtro_sql esta vacia', 'data' => '']
     * ```
     */
    private function val_filter(string $entidad, string $filtro_sql)
    {
        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Validar que el filtro SQL no esté vacío
        $filtro_sql = trim($filtro_sql);
        if ($filtro_sql === '') {
            return (new error())->error('Error $filtro_sql esta vacia', $filtro_sql);
        }

        // Retornar true si ambos parámetros son válidos
        return true;
    }


}
