<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use stdClass;

class rel_serie
{

    private string $key_base_id = 'rel_serie_id';

    /**
     * TRASLADADO
     * Consulta si existe una serie duplicada en una plaza.
     *
     * Esta función se utiliza para verificar si una serie está duplicada en una plaza específica.
     * Primero, valida los parámetros recibidos para asegurarse de que sean mayores a 0.
     * Luego, genera y ejecuta una consulta SQL para comprobar la existencia de duplicados.
     * Si ocurre algún error durante el proceso, devuelve un mensaje de error.
     *
     * @param int $cantidad La cantidad de series a verificar, debe ser mayor a 0.
     * @param PDO $link Objeto PDO que representa la conexión a la base de datos.
     * @param int $plaza_id El ID de la plaza donde se verificará la serie, debe ser mayor a 0.
     *
     * @return array El resultado de la ejecución de la consulta. Si ocurre un error, se devuelve un mensaje de error.
     */
    final public function consulta_serie_duplicada_plaza(int $cantidad,PDO $link, int $plaza_id): array {
        if($cantidad <= 0) {
            return (new error())->error('Error $cantidad es menor a 0',$cantidad);
        }
        if($plaza_id <= 0) {
            return (new error())->error('Error $plaza_id es menor a 0',$plaza_id);
        }

        $sql = (new sql\rel_serie())->consulta_serie_duplicada_plaza($cantidad,$plaza_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }


        return $exe;
    }

    /**
     *
     * Obtiene la cuenta bancaria relacionada a una serie específica para una plaza.
     *
     * Esta función realiza varias validaciones, como comprobar que el ID de la plaza sea mayor a 0.
     * También ejecuta una consulta SQL para obtener la cuenta bancaria vinculada a la serie de la plaza
     * y su relación con contabilidad, si es necesario.
     *
     * @param PDO $link        Conexión a la base de datos utilizando PDO.
     * @param int $plaza_id    ID de la plaza para la cual se desea obtener la cuenta bancaria. Debe ser mayor a 0.
     * @param string $es_contable (Opcional) Especifica si se requiere obtener la cuenta contable relacionada. Si está vacío, se obtienen todas.
     *
     * @return array    Retorna un arreglo con los resultados obtenidos de la base de datos, o un objeto de error en caso de falla.
     */
    final public function obten_cuenta_bancaria_con_serie(PDO $link, int $plaza_id, string $es_contable = ''): array
    {
        if($plaza_id <= 0){
            return (new error())->error('Error $plaza_id es menor a 0',$plaza_id);
        }

        $sql = (new sql\rel_serie())->obten_cuenta_bancaria_con_serie($plaza_id,$es_contable);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }

        return $exe;
    }
}
