<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;

class usuario
{

    private string $key_base_id = 'usuario_id';

    final public function activa_acceso_app(PDO $link, int $usuario_id): array
    {
        if($usuario_id <= 0){
            return (new error())->error('Error $usuario_id debe ser mayor a 0', $usuario_id);
        }

        $sql = (new \desarrollo_em3\manejo_datos\sql\usuario())->activa_acceso_app($usuario_id);
        if(error::$en_error){
            return (new error())->error('Error al generar sql', $sql);
        }

        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al activar acceso app', $exe);
        }

        return $exe;


    }


}
