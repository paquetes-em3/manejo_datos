<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use stdClass;

class meta_gestor_detalle
{

    /**
     * EM3
     * Recalcula las metas del gestor en detalle, basándose en un rango de fechas y parámetros específicos.
     *
     * Esta función valida los datos de entrada, genera una consulta SQL para recalcular las metas, y ejecuta
     * la consulta en una conexión PDO, devolviendo los resultados.
     *
     * @param string $campo_fecha_pago El nombre del campo que representa la fecha de pago. No debe estar vacío.
     * @param string $campo_movimiento El nombre del campo que representa el movimiento del pago. No debe estar vacío.
     * @param string $campo_total El nombre del campo que representa el total del pago. No debe estar vacío.
     * @param string $entidad_empleado El nombre de la entidad o tabla relacionada con empleados. No debe estar vacío.
     * @param string $fecha_fin La fecha de fin del rango de cálculo. No debe estar vacía.
     * @param string $fecha_inicio La fecha de inicio del rango de cálculo. No debe estar vacía.
     * @param PDO $link Una conexión PDO válida para ejecutar la consulta.
     * @param int $meta_gestor_id El identificador de la meta del gestor. Debe ser mayor a 0.
     *
     * @return array Devuelve los resultados de la ejecución de la consulta SQL. En caso de error, devuelve un array
     *               con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Recalcular metas del gestor
     * $campo_fecha_pago = 'fecha_pago';
     * $campo_movimiento = 'movimiento';
     * $campo_total = 'total';
     * $entidad_empleado = 'empleado';
     * $fecha_inicio = '2023-01-01';
     * $fecha_fin = '2023-12-31';
     * $meta_gestor_id = 123;
     * $resultado = $this->recalcula_meta_gestor_detalle(
     *     $campo_fecha_pago, $campo_movimiento, $campo_total, $entidad_empleado, $fecha_fin, $fecha_inicio, $link, $meta_gestor_id
     * );
     * print_r($resultado);
     * // Resultado:
     * // ['registros' => [...], 'n_registros' => ..., 'sql' => ...]
     *
     * // Ejemplo 2: Error por fecha vacía
     * $fecha_inicio = '';
     * $resultado = $this->recalcula_meta_gestor_detalle(
     *     $campo_fecha_pago, $campo_movimiento, $campo_total, $entidad_empleado, $fecha_fin, $fecha_inicio, $link, $meta_gestor_id
     * );
     * // Resultado:
     * // ['error' => 'Error $fecha_inicio esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por meta_gestor_id inválido
     * $meta_gestor_id = 0;
     * $resultado = $this->recalcula_meta_gestor_detalle(
     *     $campo_fecha_pago, $campo_movimiento, $campo_total, $entidad_empleado, $fecha_fin, $fecha_inicio, $link, $meta_gestor_id
     * );
     * // Resultado:
     * // ['error' => 'Error $meta_gestor_id debe ser mayor a 0', 'data' => 0]
     * ```
     */
    final public function recalcula_meta_gestor_detalle(
        string $campo_fecha_pago,
        string $campo_movimiento,
        string $campo_total,
        string $entidad_empleado,
        string $fecha_fin,
        string $fecha_inicio,
        PDO $link,
        int $meta_gestor_id
    ): array {
        // Validar los datos de entrada
        $valida = (new \desarrollo_em3\manejo_datos\sql\meta_gestor_detalle())->valida_datos(
            $campo_fecha_pago, $campo_movimiento, $campo_total, $entidad_empleado
        );
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Validar fechas y meta_gestor_id
        $fecha_inicio = trim($fecha_inicio);
        if ($fecha_inicio === '') {
            return (new error())->error('Error $fecha_inicio esta vacio', $fecha_inicio);
        }
        $fecha_fin = trim($fecha_fin);
        if ($fecha_fin === '') {
            return (new error())->error('Error $fecha_fin esta vacio', $fecha_fin);
        }
        if ($meta_gestor_id <= 0) {
            return (new error())->error('Error $meta_gestor_id debe ser mayor a 0', $meta_gestor_id);
        }

        // Generar consulta SQL
        $sql = (new \desarrollo_em3\manejo_datos\sql\meta_gestor_detalle())->recalcula_meta_gestor_detalle(
            $campo_fecha_pago, $campo_movimiento, $campo_total, $entidad_empleado
        );
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $sql);
        }

        // Parámetros para la consulta
        $parametros = [
            "fecha_inicio" => $fecha_inicio,
            "fecha_fin" => $fecha_fin,
            "meta_gestor_id" => $meta_gestor_id
        ];

        // Ejecutar la consulta
        $r_meta = (new transacciones($link))->ejecuta_consulta_segura($sql, $parametros);
        if (error::$en_error) {
            return (new error())->error('No se pudo calcular los abonos extra del contrato', $r_meta);
        }

        // Retornar los resultados
        return $r_meta;
    }



}
