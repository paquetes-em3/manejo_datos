<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use PDO;

class empresa
{

    /**
     * EM3
     * Obtiene un listado de empresas con la condición de que estén marcadas como `es_324 = 'activo'`.
     *
     * Esta función consulta la base de datos para obtener todas las empresas que tienen el campo `es_324`
     * con el valor `'activo'`. Para ello:
     * - Genera la consulta SQL llamando al método `empresa_324()` de la clase `empresa`.
     * - Ejecuta la consulta de forma segura a través del método `exe_objs()` de la clase `consultas`.
     * - Retorna un array con las empresas encontradas o un error en caso de fallo.
     *
     * @param PDO $link Conexión activa a la base de datos.
     *
     * @return array Retorna un array de objetos `stdClass`, donde cada objeto representa una empresa con sus respectivos datos.
     *               Si ocurre un error, retorna un array con la clave `'error'` y un mensaje detallando el fallo.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener empresas con `es_324 = 'activo'`
     * $link = new PDO('mysql:host=localhost;dbname=mi_base', 'usuario', 'password');
     * $resultado = (new empresa())->empresas_324($link);
     *
     * if (isset($resultado['error'])) {
     *     echo "Error: " . $resultado['mensaje'];
     * } else {
     *     foreach ($resultado as $empresa) {
     *         echo "Empresa: " . $empresa->nombre . " - ID: " . $empresa->id . PHP_EOL;
     *     }
     * }
     * ```
     *
     * @example
     * ```php
     * // Ejemplo 2: Sin empresas activas
     * // Si ninguna empresa tiene `es_324 = 'activo'`, el resultado será un array vacío:
     * []
     * ```
     *
     * @example
     * ```php
     * // Ejemplo 3: Error en la consulta SQL
     * $link = null; // Conexión inválida
     * $resultado = (new empresa())->empresas_324($link);
     * print_r($resultado);
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error al ejecutar sql',
     * //     'mensaje' => 'Detalles del error en la consulta'
     * // ]
     * ```
     *
     * ### Proceso de la función:
     * 1. Se obtiene la consulta SQL con la función `empresa_324()`.
     * 2. Se ejecuta la consulta SQL con `exe_objs()`.
     * 3. Se devuelve el array con los resultados obtenidos o un error si ocurre algún fallo.
     */
    final public function empresas_324(PDO $link): array
    {
        // Genera la consulta SQL para obtener empresas con es_324 = 'activo'
        $sql = (new \desarrollo_em3\manejo_datos\sql\empresa())->empresa_324();
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $sql);
        }

        // Ejecuta la consulta y obtiene los resultados como objetos
        $exe = (new consultas())->exe_objs($link, $sql);
        if (error::$en_error) {
            return (new error())->error('Error al obtener empresas', $exe);
        }

        // Retorna el array con las empresas activas
        return $exe;
    }



}
