<?php
namespace desarrollo_em3\manejo_datos\data\entidades;



use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use PDO;

class periodo_pago
{

    private string $key_base_id = 'periodo_pago_id';

    final public function periodos(PDO $link, int $ejercicio_id = -1, bool $en_bruto = false,
                                   string $fecha_final= '9999-01-01', string $fecha_inicial = '1900-01-01',
                                   int $mes_id = -1, string $order_by = '', int $periodicidad_pago_id = -1,
                                   string $status = 'activo'): array
    {
        $sql = (new \desarrollo_em3\manejo_datos\sql\periodo_pago())->periodos($link,$en_bruto, $ejercicio_id,
            $fecha_final,$fecha_inicial,$mes_id, $order_by,$periodicidad_pago_id,$status);

        if(error::$en_error){
            return (new error())->error('Error al generar sql', $sql);
        }

        $exe = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener periodos', $exe);
        }

        return $exe;

    }


}
