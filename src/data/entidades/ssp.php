<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\_valida;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;
use PDO;
use stdClass;

class ssp {
    
    /**
     * Obtiene datos para ser utilizados en un DataTable.
     *
     * Esta función construye y ejecuta una consulta SQL basada en los parámetros proporcionados
     * para obtener datos de una entidad específica. La consulta puede incluir condiciones,
     * filtros, joins, límites y ordenamientos.
     *
     * @param array $campos_base Campos base que se seleccionarán en la consulta SQL.
     * @param array $campos_like Campos que se utilizarán para búsquedas con LIKE.
     * @param string $entidad Nombre de la entidad (tabla) de la cual se obtendrán los datos.
     * @param PDO $link Conexión PDO a la base de datos.
     * @param array $campos_extra Campos adicionales que se seleccionarán en la consulta SQL (opcional).
     * @param array $condicion Condiciones adicionales para la consulta SQL (opcional).
     * @param string $condicion_like Condición LIKE adicional para la consulta SQL (opcional).
     * @param array $filtro_rango Filtros de rango para la consulta SQL (opcional).
     * @param array $join Joins adicionales para la consulta SQL (opcional).
     * @param int $limit Límite de registros a devolver (opcional).
     * @param int $offset Desplazamiento para la paginación (opcional).
     * @param string $order_by Ordenamiento de los registros (opcional).
     *
     * @return array Retorna un array con los resultados de la consulta. En caso de error, retorna un objeto de error.
     *
     * @throws Error Si $campos_base está vacío.
     * @throws Error Si $campos_like está vacío.
     * @throws Error Si $entidad está vacío.
     * @throws Error Si ocurre un error al construir la consulta SQL.
     * @throws Error Si ocurre un error al ejecutar la consulta SQL.
     */
    private function get_data_datatable(array $campos_base, array $campos_like,string $entidad,
    PDO $link,array $campos_extra = array(),array $condicion = array(),string $condicion_like = '',
    array $filtro_rango = array(),array $join = array(),int $limit = 0,int $offset = 0, string $order_by = ''): array {
        if(empty($campos_base)) {
            return (new error())->error('Error $campos_base no puede ser vacio', $campos_base);
        }
        if(empty($campos_like)) {
            return (new error())->error('Error $campos_like no puede ser vacio', $campos_like);
        }
        if(trim($entidad) === '') {
            return (new error())->error('Error $entidad_empleado no puede ser vacio', $entidad);
        }
        $sql = (new sql())->get_data_datatable($campos_base,$campos_like,$entidad,
        $campos_extra, $condicion,$condicion_like,$filtro_rango,$join,$limit,$offset,$order_by);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener exe',$exe);
        }

        return $exe;
    }

    private function sentencia_order_datatable(array $columns, bool $genera_sentencia, array $order): string
    {
        $sentencia = '';
        if($genera_sentencia) {
            unset($columns[count($columns) - 1]);
            $key = $order[0]['column'];

            if (array_key_exists($key, $columns)) {
                $sentencia = $columns[$key]['data'] . " " . $order[0]['dir'];
            }
        }

        return $sentencia;

    }

    private function genera_sentencia(array $columns, array $order): bool
    {
        $genera_sentencia = true;
        if(empty($order)) {
            $genera_sentencia = false;
        }
        if(empty($columns)) {
            $genera_sentencia = false;
        }
        return $genera_sentencia;

    }

    private function genera_sentencia_order(array $order,array $columns)
    {

        $genera_sentencia = $this->genera_sentencia($columns,$order);
        if(error::$en_error){
            return (new error())->error('Error al validar generacion de sentencia', $genera_sentencia);
        }

        $sentencia = $this->sentencia_order_datatable($columns,$genera_sentencia,$order);
        if(error::$en_error){
            return (new error())->error('Error al generar sentencia', $sentencia);
        }

        return $sentencia;
    }

    /**
     * Procesa datos para ser utilizados en un DataTable con procesamiento del lado del servidor.
     *
     * Esta función genera una consulta SQL basada en los parámetros proporcionados y obtiene los datos
     * de una entidad específica. La consulta puede incluir condiciones, filtros, joins, límites y
     * ordenamientos. Además, maneja la paginación y el ordenamiento basado en los parámetros de entrada.
     *
     * @param array $campos_base Campos base que se seleccionarán en la consulta SQL.
     * @param array $campos_like Campos que se utilizarán para búsquedas con LIKE.
     * @param string $entidad Nombre de la entidad (tabla) de la cual se obtendrán los datos.
     * @param array $filtros Array con los filtros y parámetros de ordenamiento proporcionados por DataTables.
     * @param PDO $link Conexión PDO a la base de datos.
     * @param array $campos_extra Campos adicionales que se seleccionarán en la consulta SQL (opcional).
     * @param array $condicion Condiciones adicionales para la consulta SQL (opcional).
     * @param array $filtro_rango Filtros de rango para la consulta SQL (opcional).
     * @param array $join Joins adicionales para la consulta SQL (opcional).
     * @param int $length Número de registros a devolver (opcional).
     * @param int $start Desplazamiento para la paginación (opcional).
     *
     * @return array Retorna un array con los resultados de la consulta. En caso de error, retorna un objeto de error.
     *
     * @throws Error Si $campos_base está vacío.
     * @throws Error Si $campos_like está vacío.
     * @throws Error Si $entidad está vacío.
     * @throws Error Si $filtros está vacío.
     * @throws Error Si ocurre un error al generar la sentencia de ordenamiento.
     * @throws Error Si ocurre un error al obtener los datos.
     */
    public function service_side_processing(array $campos_base,array $campos_like,string $entidad,array $filtros,
    PDO $link,array $campos_extra = array(),array $condicion = array(), array $filtro_rango = array(),array $join = array(), 
    int $length = 0, int $start = 0): array {
        if(empty($campos_base)) {
            return (new error())->error('Error $campos_base no puede ser vacio', $campos_base);
        }
        if(empty($campos_like)) {
            return (new error())->error('Error $campos_like no puede ser vacio', $campos_like);
        }
        if(trim($entidad) === '') {
            return (new error())->error('Error $entidad_empleado no puede ser vacio', $entidad);
        }
        if(empty($filtros)) {
            return (new error())->error('Error $post no puede ser vacio', $filtros);
        }

        $sentencia = $this->genera_sentencia_order($filtros['order_by'], $filtros['columns']);
        if(\desarrollo_em3\error\error::$en_error){
            return (new \desarrollo_em3\error\error())->error('Error al generar $sentencia',
                $sentencia);
        }
        $r_datos = $this->get_data_datatable($campos_base, $campos_like,$entidad,
        $link,$campos_extra,$condicion,$filtros['wh_like'],$filtro_rango, $join,$length,$start,$sentencia);
        if(\desarrollo_em3\error\error::$en_error){
            return (new \desarrollo_em3\error\error())->error('Error al obtener $r_datos',
                $r_datos);
        }

        return $r_datos;
    }

}