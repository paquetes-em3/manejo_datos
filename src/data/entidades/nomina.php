<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


class nomina
{
    final public function monto_calcula_subsidio(\stdClass $montos, bool $usa_sueldos_salarios_subsidio)
    {
        $monto_calcula_subsidio = $montos->monto_sueldos_salarios;
        if(!$usa_sueldos_salarios_subsidio) {
            $monto_calcula_subsidio = $montos->monto_total_gravado;
        }

        return $monto_calcula_subsidio;

    }


}
