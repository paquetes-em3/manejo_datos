<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\_valida;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\data\_datos;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use stdClass;

class pago
{

    private string $key_base_id = 'pago_id';

    /**
     * TRASLADADO
     * Procesa y organiza los datos de una transferencia.
     *
     * Esta función toma un array de registro, separa los campos 'referencia' y 'rel_serie_id' si existen,
     * y los almacena por separado en un objeto `stdClass`, junto con el registro restante.
     *
     * @param array $registro El array de datos que representa la transferencia, que puede contener los campos 'referencia' y 'rel_serie_id'.
     *
     * @return stdClass Un objeto que contiene:
     * - `$registro`: El array con los datos restantes después de eliminar 'referencia' y 'rel_serie_id'.
     * - `$referencia`: La referencia de la transferencia si estaba presente en el array, o una cadena vacía si no.
     * - `$rel_serie_id`: El identificador de la serie relacionada si estaba presente en el array, o -1 si no.
     */
    final public function datos_transferencia(array $registro): stdClass
    {
        $referencia = '';
        $rel_serie_id = -1;
        if(isset($registro['referencia'])) {
            $referencia = $registro['referencia'];
            unset($registro['referencia']);
        }
        if(isset($registro['rel_serie_id'])) {
            $rel_serie_id = $registro['rel_serie_id'];
            unset($registro['rel_serie_id']);
        }

        $data = new stdClass();
        $data->registro = $registro;
        $data->referencia = $referencia;
        $data->rel_serie_id = $rel_serie_id;

        return $data;

    }
    private function estructura(string $tipo): stdClass
    {
        $tipo = trim($tipo);
        $tipo = strtoupper($tipo);
        $estructura = new stdClass();
        if ($tipo === 'SAP'){
            $estructura->campo_comentarios = 'Comments';
            $estructura->campo_fecha = 'DocDate';
            $estructura->campo_fecha_cliente = 'DocDueDate';
            $estructura->campo_folio = 'U_FolioCont';
            $estructura->campo_monto = 'DocTotal';
            $estructura->campo_nombre_cliente = 'CardName';
            $estructura->campo_serie = 'U_SeCont';
            $estructura->campo_tipo_contrato = 'U_TipoContrato';
            $estructura->entidad_empleado = 'ohem';
            $estructura->entidad_cliente = '';
        }

        if ($tipo === 'EM3'){
            $estructura->campo_comentarios = 'comentario';
            $estructura->campo_fecha = 'fecha_corte';
            $estructura->campo_fecha_cliente = 'fecha_cliente';
            $estructura->campo_folio = 'folio';
            $estructura->campo_monto = 'monto';
            $estructura->campo_nombre_cliente = 'nombre_completo';
            $estructura->campo_serie = 'serie';
            $estructura->campo_tipo_contrato = 'tipo_contrato';
            $estructura->entidad_empleado = 'empleado';
            $estructura->entidad_cliente = 'cliente';
        }
        return $estructura;


    }

    final public function pago_corte_entrega_faltante(string $campo_monto, string $campo_serie, int $pago_corte_id,
                                                      PDO $link) {
        if($pago_corte_id <= 0){
            return (new error())->error('Error $pago_id es menor a 0', $pago_corte_id);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\pago())->obten_serie_faltante_entrega(
            $campo_monto, $campo_serie, $pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener sql',$sql);
        }
        $r_pago = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener pagos',$r_pago);
        }
        if((int)$r_pago['n_registros'] === 0){
            return (new error())->error('Error al obtener pago no existe',$r_pago);
        }
        return $r_pago['registros'];
    }

    /**
     * TRASLADADO
     * Obtiene los datos de un pago específico desde la base de datos.
     *
     * Este método consulta la base de datos para recuperar un registro de la tabla `pago`
     * basado en su ID. El resultado se devuelve como un objeto.
     *
     * @param PDO $link Conexión activa a la base de datos.
     * @param int $pago_id ID del pago que se desea consultar. Debe ser mayor a 0.
     *
     * @return stdClass|array Devuelve el registro del pago como un objeto si la operación es exitosa.
     *                        En caso de error, devuelve un objeto de error.
     *
     * @throws error Retorna un error si:
     *         - `$pago_id` es menor o igual a 0.
     *         - Ocurre un problema al ejecutar la consulta.
     *
     * Ejemplo de uso:
     * ```php
     * $link = new PDO(...); // Conexión a la base de datos
     * $pago_id = 123;
     *
     * $pago = $this->pago($link, $pago_id);
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al obtener el pago.";
     * } else {
     *     print_r($pago); // Muestra los datos del pago como un objeto.
     * }
     * ```
     *
     * Salida esperada (ejemplo):
     * ```php
     * stdClass Object
     * (
     *     [id] => 123
     *     [contrato_id] => 456
     *     [monto] => 1000.00
     *     [fecha] => 2023-11-23
     * )
     * ```
     *
     * Proceso:
     * 1. Valida que `$pago_id` sea mayor a 0.
     * 2. Realiza una consulta utilizando el método `registro_bruto` para recuperar el registro del pago.
     * 3. Devuelve el registro como un objeto, o un error si no se encuentra o si ocurre un fallo.
     *
     * Consideraciones:
     * - Asegúrate de que la conexión a la base de datos sea válida antes de llamar al método.
     * - Maneja los errores devueltos para evitar fallos no controlados en el programa.
     *
     * Errores posibles:
     * - `['error' => 'Error $pago_id 0', 'detalle' => $pago_id]` si `$pago_id` es menor o igual a 0.
     * - `['error' => 'Error al obtener pago', 'detalle' => $rs]` si ocurre un fallo al recuperar los datos.
     */
    final public function pago(PDO $link, int $pago_id)
    {
        // Valida que $pago_id sea mayor a 0
        if ($pago_id <= 0) {
            return (new error())->error('Error $pago_id 0', $pago_id);
        }

        // Realiza la consulta para obtener los datos del pago
        $rs = (new consultas())->registro_bruto(new stdClass(), 'pago', $pago_id, $link, false);
        if (error::$en_error) {
            return (new error())->error('Error al obtener pago', $rs);
        }

        // Devuelve el registro del pago como un objeto
        return $rs;
    }


    final public function pago_corte_id(PDO $link, int $pago_id)
    {
        if($pago_id <= 0){
            return (new error())->error('Error $pago_id es menor a 0', $pago_id);
        }
        $columnas = new stdClass();
        $columnas->pago_id = new stdClass();
        $columnas->pago_id->atributo = 'id';

        $columnas->pago_corte_id = new stdClass();
        $columnas->pago_corte_id->atributo = 'pago_corte_id';
        $rs =(new consultas())->registro_bruto($columnas,'pago',$pago_id,$link,false);
        if(error::$en_error){
            return (new error())->error('Error al obtener sql', $rs);
        }

        return $rs;

    }

    /**
     * TRASLADADO
     * Recupera el monto de un pago a partir de su ID.
     *
     * Esta función ejecuta una consulta segura en la base de datos para obtener
     * la información del pago correspondiente al ID proporcionado. Si ocurre
     * algún error en el proceso, se devuelve un objeto de error.
     *
     * @param PDO $link Conexión a la base de datos.
     * @param int $pago_id El ID del pago. Debe ser un valor entero mayor a 0.
     * @return object|array Devuelve un objeto con la información del pago o un objeto de error en caso de fallo.
     */
    final public function pago_monto(PDO $link, int $pago_id)
    {
        if($pago_id <= 0){
            return (new error())->error('Error $pago_id debe ser mayor a 0',$pago_id);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\pago())->pago_monto($pago_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener sql',$sql);
        }

        $r_pago = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener pagos',$r_pago);
        }
        if((int)$r_pago['n_registros'] === 0){
            return (new error())->error('Error al obtener pago no existe',$r_pago);
        }
        return (object)$r_pago['registros'][0];

    }

    final public function pagos_por_contrato(int $contrato_id, PDO $link, bool $puros_id = false, bool $simplificados = false): array
    {
        $sql = (new \desarrollo_em3\manejo_datos\sql\pago())->pagos_simplificados($contrato_id,$puros_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $sql',$sql);
        }
        if(!$simplificados){
            $sql = (new \desarrollo_em3\manejo_datos\sql\pago())->pagos($contrato_id,$link);
            if(error::$en_error){
                return (new error())->error('Error al obtener $sql',$sql);
            }
        }

        $exe = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al ejecutar $sql',$sql);
        }
        return $exe;

    }

    /**
     * EM3
     * Obtiene y filtra registros de pagos basados en un ID, consolidando resultados únicos.
     *
     * Esta función valida los parámetros de entrada, obtiene registros exactos y parciales basados en un ID
     * (`rows_by_id` y `rows_by_id_like`), los combina (`result_by_id`) y filtra duplicados para obtener registros únicos.
     *
     * @param string $key_empleado_id El nombre del campo asociado al identificador del empleado. No puede estar vacío.
     * @param int $limit El número máximo de resultados a devolver. Debe ser mayor a 0.
     * @param PDO $link Una instancia de la conexión PDO para ejecutar las consultas.
     * @param int $pago_id El identificador del pago a buscar. Debe ser mayor a 0.
     *
     * @return array Devuelve un arreglo con registros únicos basados en el ID de pago.
     *               En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Obtener registros únicos por ID de pago
     * $key_empleado_id = 'empleado_id';
     * $limit = 10;
     * $pago_id = 12345;
     * $link = new PDO(...); // Instancia PDO válida
     * $resultado = $this->pagos_por_id($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     ['id' => 1, 'empleado_id' => '12345', 'nombre' => 'Juan', ...],
     * //     ['id' => 2, 'empleado_id' => '12345', 'nombre' => 'Pedro', ...],
     * // ]
     *
     * // Ejemplo 2: Error por clave vacía
     * $key_empleado_id = '';
     * $resultado = $this->pagos_por_id($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $key_empleado_id esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por ID de pago inválido
     * $key_empleado_id = 'empleado_id';
     * $limit = 10;
     * $pago_id = -1;
     * $resultado = $this->pagos_por_id($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error al validar entrada', 'data' => ...]
     * ```
     */
    final public function pagos_por_id(string $key_empleado_id, int $limit, PDO $link, int $pago_id): array
    {
        // Validar los parámetros básicos de entrada
        $valida = (new \desarrollo_em3\manejo_datos\sql\pago())->valida_entrada_base($limit, $pago_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar entrada', $valida);
        }

        // Validar que el identificador del empleado no esté vacío
        $key_empleado_id = trim($key_empleado_id);
        if ($key_empleado_id === '') {
            return (new error())->error('Error $key_empleado_id esta vacio', $key_empleado_id);
        }

        // Obtener registros exactos y parciales, y combinarlos
        $result = $this->result_by_id($key_empleado_id, $limit, $link, $pago_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $result', $result);
        }

        // Filtrar registros duplicados para obtener registros únicos
        $result_new = (new _datos())->rows_news($this->key_base_id, $result);
        if (error::$en_error) {
            return (new error())->error('Error integrar row new', $result_new);
        }

        // Retornar los registros únicos
        return $result_new;
    }



    /**
     * EM3
     * Obtiene los registros de pagos por corte basados en filtros específicos, campos adicionales y uniones SQL.
     *
     * Esta función valida los datos de entrada, genera dinámicamente una consulta SQL `SELECT` para obtener los pagos
     * relacionados con un corte específico, y retorna los registros resultantes como un array de objetos.
     *
     * @param string $campo_canceled El nombre del campo que indica si un pago está cancelado. Puede estar vacío.
     * @param string $campo_monto_pago El nombre del campo que representa el monto del pago. No debe estar vacío.
     * @param string $campo_movto El nombre del campo que representa el movimiento del pago. No debe estar vacío.
     * @param PDO $link Una conexión PDO válida para ejecutar la consulta.
     * @param int $pago_corte_id El identificador del pago por corte. Debe ser mayor a 0.
     * @param array $campos_extra Un array de campos adicionales que deben incluirse en la consulta.
     * @param array $joins Un array de configuraciones para generar las uniones SQL (`LEFT JOIN`).
     *
     * @return array Devuelve un array de registros obtenidos como objetos. En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener registros de pagos por corte con uniones y campos extra
     * $campo_canceled = 'Canceled';
     * $campo_monto_pago = 'DocTotal';
     * $campo_movto = 'Movto';
     * $pago_corte_id = 123;
     * $campos_extra = ['pago.id', 'contrato.id AS contrato_id'];
     * $joins = [
     *     ['entidad_left' => 'pago', 'entidad_right' => 'contrato', 'rename_entidad' => 'p']
     * ];
     * $resultado = $this->pagos_por_corte($campo_canceled, $campo_monto_pago, $campo_movto, $link, $pago_corte_id, $campos_extra, $joins);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     (object) ['id' => 1, 'contrato_id' => 10],
     * //     (object) ['id' => 2, 'contrato_id' => 11],
     * // ]
     *
     * // Ejemplo 2: Error por validación fallida
     * $campo_monto_pago = '';
     * $resultado = $this->pagos_por_corte($campo_canceled, $campo_monto_pago, $campo_movto, $link, $pago_corte_id, $campos_extra, $joins);
     * // Resultado:
     * // ['error' => 'Error al validar pago corte', 'data' => [...]]
     * ```
     */
    final public function pagos_por_corte(
        string $campo_canceled,
        string $campo_monto_pago,
        string $campo_movto,
        PDO $link,
        int $pago_corte_id,
        array $campos_extra,
        array $joins
    ): array {
        // Validar los datos de entrada
        $valida = (new _valida())->valida_pago_corte($campo_monto_pago, $campo_movto, $pago_corte_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar pago corte', $valida);
        }

        // Generar el filtro SQL para los pagos por corte
        $filtro_sql = (new \desarrollo_em3\manejo_datos\sql\pago())->pagos_por_corte_filtro(
            $campo_canceled,
            $campo_monto_pago,
            $campo_movto,
            $pago_corte_id
        );
        if (error::$en_error) {
            return (new error())->error('Error al obtener $filtro_sql', $filtro_sql);
        }

        // Obtener los registros de pagos por corte
        $rows = (new consultas())->rows_by_filter('pago', $filtro_sql, $link, $campos_extra, $joins);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $rows', $rows);
        }

        // Retornar los registros obtenidos
        return $rows;
    }


    final public function pagos_por_corte_con_deposito(PDO $link, int $pago_corte_id, string $tipo_estructura)
    {

        $estructura = $this->estructura($tipo_estructura);
        if(error::$en_error){
            return (new error())->error('Error al obtener estructura',$estructura);
        }

        $sql = (new \desarrollo_em3\manejo_datos\sql\pago())->pagos_por_corte_con_deposito(
            $estructura->campo_comentarios,$estructura->campo_fecha,$estructura->campo_fecha_cliente,
            $estructura->campo_folio,$estructura->campo_monto,$estructura->campo_nombre_cliente,
            $estructura->campo_serie,$estructura->campo_tipo_contrato,$estructura->entidad_cliente,
            $estructura->entidad_empleado,$pago_corte_id);

        $rs = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener resultado',$rs);
        }
        return $rs['registros'];

    }

    /**
     * EM3
     * Combina resultados únicos y parciales en un solo arreglo.
     *
     * Esta función toma un objeto con resultados únicos (`rows_unicos`) y resultados parciales (`rows_like`),
     * valida su estructura y los combina en un único arreglo.
     *
     * @param stdClass $rows Un objeto con las siguientes propiedades:
     *                       - `rows_unicos`: Un arreglo de resultados únicos.
     *                       - `rows_like`: Un arreglo de resultados parciales.
     *
     * @return array Devuelve un arreglo combinado con los resultados únicos y parciales.
     *               En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Combinar resultados únicos y parciales
     * $rows = new stdClass();
     * $rows->rows_unicos = [
     *     ['id' => 1, 'nombre' => 'Juan'],
     *     ['id' => 2, 'nombre' => 'Pedro'],
     * ];
     * $rows->rows_like = [
     *     ['id' => 3, 'nombre' => 'Juanita'],
     *     ['id' => 4, 'nombre' => 'Juana'],
     * ];
     * $resultado = $this->result_base($rows);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     ['id' => 1, 'nombre' => 'Juan'],
     * //     ['id' => 2, 'nombre' => 'Pedro'],
     * //     ['id' => 3, 'nombre' => 'Juanita'],
     * //     ['id' => 4, 'nombre' => 'Juana'],
     * // ]
     *
     * // Ejemplo 2: Error por falta de `rows_unicos`
     * $rows = new stdClass();
     * $rows->rows_like = [];
     * $resultado = $this->result_base($rows);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $rows->rows_unicos no existe', 'data' => $rows]
     *
     * // Ejemplo 3: Error por falta de `rows_like`
     * $rows = new stdClass();
     * $rows->rows_unicos = [];
     * $resultado = $this->result_base($rows);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $rows->rows_like no existe', 'data' => $rows]
     * ```
     */
    private function result_base(stdClass $rows): array
    {
        // Validar que `rows_unicos` exista
        if (!isset($rows->rows_unicos)) {
            return (new error())->error('Error $rows->rows_unicos no existe', $rows);
        }

        // Validar que `rows_unicos` sea contable
        if (!is_countable($rows->rows_unicos)) {
            return (new error())->error('Error $rows->rows_unicos no son contables', $rows);
        }

        // Validar que `rows_like` exista
        if (!isset($rows->rows_like)) {
            return (new error())->error('Error $rows->rows_like no existe', $rows);
        }

        // Validar que `rows_like` sea contable
        if (!is_countable($rows->rows_like)) {
            return (new error())->error('Error $rows->rows_like no son contables', $rows);
        }

        // Combinar los resultados únicos y parciales
        // Retornar el arreglo combinado
        return array_merge($rows->rows_unicos, $rows->rows_like);
    }


    /**
     * EM3
     * Combina registros únicos y parciales obtenidos por ID en un solo arreglo.
     *
     * Esta función valida los parámetros de entrada, recupera registros únicos (`=`) y parciales (`LIKE`)
     * de una entidad, y combina ambos conjuntos en un solo arreglo.
     *
     * @param string $key_empleado_id El nombre del campo asociado al identificador del empleado. No puede estar vacío.
     * @param int $limit El número máximo de resultados a devolver. Debe ser mayor a 0.
     * @param PDO $link Una instancia de la conexión PDO para ejecutar las consultas.
     * @param int $pago_id El identificador del pago a buscar. Debe ser mayor a 0.
     *
     * @return array Devuelve un arreglo combinado con los resultados únicos y parciales.
     *               En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Obtener registros combinados por ID de pago
     * $key_empleado_id = 'empleado_id';
     * $limit = 5;
     * $pago_id = 12345;
     * $link = new PDO(...); // Instancia PDO válida
     * $resultado = $this->result_by_id($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     ['id' => 1, 'empleado_id' => '12345', 'nombre' => 'Juan', ...],
     * //     ['id' => 2, 'empleado_id' => '12345', 'nombre' => 'Pedro', ...],
     * //     ['id' => 3, 'empleado_id' => '12345', 'nombre' => 'Juanita', ...],
     * //     ['id' => 4, 'empleado_id' => '12345', 'nombre' => 'Juana', ...],
     * // ]
     *
     * // Ejemplo 2: Error por campo empleado vacío
     * $key_empleado_id = '';
     * $limit = 5;
     * $pago_id = 12345;
     * $resultado = $this->result_by_id($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $key_empleado_id esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por ID de pago inválido
     * $key_empleado_id = 'empleado_id';
     * $limit = 5;
     * $pago_id = -1;
     * $resultado = $this->result_by_id($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error al validar entrada', 'data' => ...]
     * ```
     */
    private function result_by_id(string $key_empleado_id, int $limit, PDO $link, int $pago_id): array
    {
        // Validar los parámetros básicos de entrada
        $valida = (new \desarrollo_em3\manejo_datos\sql\pago())->valida_entrada_base($limit, $pago_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar entrada', $valida);
        }

        // Validar que el identificador del empleado no esté vacío
        $key_empleado_id = trim($key_empleado_id);
        if ($key_empleado_id === '') {
            return (new error())->error('Error $key_empleado_id esta vacio', $key_empleado_id);
        }

        // Obtener registros únicos y parciales por ID
        $rows = $this->rows_id($key_empleado_id, $limit, $link, $pago_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $rows', $rows);
        }

        // Combinar los resultados en un solo arreglo
        $result = $this->result_base($rows);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $result', $result);
        }

        // Retornar el arreglo combinado
        return $result;
    }


    /**
     * EM3
     * Obtiene registros de una entidad basados en un ID específico mediante una consulta SQL segura.
     *
     * Esta función valida los parámetros de entrada, genera una consulta SQL utilizando el operador `IGUAL`,
     * y ejecuta la consulta en una conexión PDO, devolviendo los registros obtenidos.
     *
     * @param string $key_empleado_id El nombre del campo asociado al identificador del empleado. No puede estar vacío.
     * @param int $limit El número máximo de resultados a devolver. Debe ser mayor a 0.
     * @param PDO $link Una instancia de la conexión PDO para ejecutar la consulta.
     * @param int $pago_id El identificador del pago a buscar. Debe ser mayor a 0.
     *
     * @return array Devuelve un arreglo de registros obtenidos de la consulta SQL.
     *               En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Obtener registros por ID de pago
     * $key_empleado_id = 'empleado_id';
     * $limit = 5;
     * $pago_id = 12345;
     * $link = new PDO(...); // Instancia PDO válida
     * $resultado = $this->rows_by_id($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     ['id' => 1, 'empleado_id' => '12345', 'nombre' => 'Juan', ...],
     * //     ...
     * // ]
     *
     * // Ejemplo 2: Error por campo empleado vacío
     * $key_empleado_id = '';
     * $limit = 5;
     * $pago_id = 12345;
     * $resultado = $this->rows_by_id($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $key_empleado_id esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por ID de pago inválido
     * $key_empleado_id = 'empleado_id';
     * $limit = 5;
     * $pago_id = -1;
     * $resultado = $this->rows_by_id($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error al validar entrada', 'data' => ...]
     * ```
     */
    private function rows_by_id(string $key_empleado_id, int $limit, PDO $link, int $pago_id): array
    {
        // Validar los parámetros básicos de entrada
        $valida = (new \desarrollo_em3\manejo_datos\sql\pago())->valida_entrada_base($limit, $pago_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar entrada', $valida);
        }

        // Validar que el identificador del empleado no esté vacío
        $key_empleado_id = trim($key_empleado_id);
        if ($key_empleado_id === '') {
            return (new error())->error('Error $key_empleado_id esta vacio', $key_empleado_id);
        }

        // Generar la consulta SQL utilizando el operador IGUAL
        $sql = (new \desarrollo_em3\manejo_datos\sql\pago())->get_by_id_equals($pago_id, $key_empleado_id, $limit);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $sql);
        }

        // Ejecutar la consulta SQL de forma segura utilizando PDO
        $r_pago = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if (error::$en_error) {
            return (new error())->error('Error al obtener pagos', $r_pago);
        }

        // Retornar los registros obtenidos
        return $r_pago['registros'];
    }


    /**
     * EM3
     * Obtiene registros de una entidad basados en un ID específico utilizando el operador `LIKE`.
     *
     * Esta función valida los parámetros de entrada, genera una consulta SQL con el operador `LIKE`,
     * y ejecuta la consulta de manera segura utilizando una conexión PDO.
     *
     * @param string $key_empleado_id El nombre del campo asociado al identificador del empleado. No puede estar vacío.
     * @param int $limit El número máximo de resultados a devolver. Debe ser mayor a 0.
     * @param PDO $link Una instancia de la conexión PDO para ejecutar la consulta.
     * @param int $pago_id El identificador del pago a buscar. Debe ser mayor a 0.
     *
     * @return array Devuelve un arreglo de registros obtenidos de la consulta SQL.
     *               En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Obtener registros por ID de pago con operador LIKE
     * $key_empleado_id = 'empleado_id';
     * $limit = 5;
     * $pago_id = 12345;
     * $link = new PDO(...); // Instancia PDO válida
     * $resultado = $this->rows_by_id_like($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     ['id' => 1, 'empleado_id' => '12345', 'nombre' => 'Juan', ...],
     * //     ...
     * // ]
     *
     * // Ejemplo 2: Error por campo empleado vacío
     * $key_empleado_id = '';
     * $limit = 5;
     * $pago_id = 12345;
     * $resultado = $this->rows_by_id_like($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $key_empleado_id esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por ID de pago inválido
     * $key_empleado_id = 'empleado_id';
     * $limit = 5;
     * $pago_id = -1;
     * $resultado = $this->rows_by_id_like($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error al validar entrada', 'data' => ...]
     * ```
     */
    private function rows_by_id_like(string $key_empleado_id, int $limit, PDO $link, int $pago_id): array
    {
        // Validar los parámetros básicos de entrada
        $valida = (new \desarrollo_em3\manejo_datos\sql\pago())->valida_entrada_base($limit, $pago_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar entrada', $valida);
        }

        // Validar que el identificador del empleado no esté vacío
        $key_empleado_id = trim($key_empleado_id);
        if ($key_empleado_id === '') {
            return (new error())->error('Error $key_empleado_id esta vacio', $key_empleado_id);
        }

        // Generar la consulta SQL utilizando el operador LIKE
        $sql = (new \desarrollo_em3\manejo_datos\sql\pago())->get_by_id($pago_id, $key_empleado_id, $limit);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $sql);
        }

        // Ejecutar la consulta SQL de manera segura utilizando PDO
        $r_pago = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if (error::$en_error) {
            return (new error())->error('Error al obtener pagos', $r_pago);
        }

        // Retornar los registros obtenidos
        return $r_pago['registros'];
    }


    /**
     * EM3
     * Obtiene registros de una entidad basados en un ID específico, combinando resultados exactos (`=`) y parciales (`LIKE`).
     *
     * Esta función valida los parámetros de entrada, ejecuta dos consultas SQL (una con operador `=` y otra con `LIKE`),
     * y devuelve los resultados combinados en un objeto.
     *
     * @param string $key_empleado_id El nombre del campo asociado al identificador del empleado. No puede estar vacío.
     * @param int $limit El número máximo de resultados a devolver por cada consulta. Debe ser mayor a 0.
     * @param PDO $link Una instancia de la conexión PDO para ejecutar las consultas.
     * @param int $pago_id El identificador del pago a buscar. Debe ser mayor a 0.
     *
     * @return stdClass|array Devuelve un objeto con dos propiedades:
     *                        - `rows_unicos`: Registros obtenidos con el operador `=`.
     *                        - `rows_like`: Registros obtenidos con el operador `LIKE`.
     *                        En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Obtener registros combinados por ID de pago
     * $key_empleado_id = 'empleado_id';
     * $limit = 5;
     * $pago_id = 12345;
     * $link = new PDO(...); // Instancia PDO válida
     * $resultado = $this->rows_id($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object
     * // (
     * //     [rows_unicos] => Array
     * //         (
     * //             [0] => Array
     * //                 (
     * //                     [id] => 1
     * //                     [empleado_id] => '12345'
     * //                     [nombre] => 'Juan'
     * //                     ...
     * //                 )
     * //             ...
     * //         )
     * //     [rows_like] => Array
     * //         (
     * //             [0] => Array
     * //                 (
     * //                     [id] => 2
     * //                     [empleado_id] => '12345'
     * //                     [nombre] => 'Juanita'
     * //                     ...
     * //                 )
     * //             ...
     * //         )
     * // )
     *
     * // Ejemplo 2: Error por campo empleado vacío
     * $key_empleado_id = '';
     * $limit = 5;
     * $pago_id = 12345;
     * $resultado = $this->rows_id($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $key_empleado_id esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por ID de pago inválido
     * $key_empleado_id = 'empleado_id';
     * $limit = 5;
     * $pago_id = -1;
     * $resultado = $this->rows_id($key_empleado_id, $limit, $link, $pago_id);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error al validar entrada', 'data' => ...]
     * ```
     */
    private function rows_id(string $key_empleado_id, int $limit, PDO $link, int $pago_id)
    {
        // Validar los parámetros básicos de entrada
        $valida = (new \desarrollo_em3\manejo_datos\sql\pago())->valida_entrada_base($limit, $pago_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar entrada', $valida);
        }

        // Validar que el identificador del empleado no esté vacío
        $key_empleado_id = trim($key_empleado_id);
        if ($key_empleado_id === '') {
            return (new error())->error('Error $key_empleado_id esta vacio', $key_empleado_id);
        }

        // Obtener registros exactos (operador `=`)
        $rows_unicos = $this->rows_by_id($key_empleado_id, $limit, $link, $pago_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener pagos', $rows_unicos);
        }

        // Obtener registros parciales (operador `LIKE`)
        $rows_like = $this->rows_by_id_like($key_empleado_id, $limit, $link, $pago_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $rows_like', $rows_like);
        }

        // Combinar los resultados en un objeto
        $data = new stdClass();
        $data->rows_unicos = $rows_unicos;
        $data->rows_like = $rows_like;

        // Retornar el objeto con los resultados
        return $data;
    }


    /**
     * TRASLADADO
     * Calcula la suma de los montos de pagos asociados a un corte específico.
     *
     * Esta función realiza los siguientes pasos:
     * 1. Valida los parámetros necesarios para identificar el corte de pagos.
     * 2. Genera un filtro SQL basado en las condiciones proporcionadas.
     * 3. Ejecuta una consulta para sumar los valores del campo de monto (`$campo_monto_pago`) en la tabla `pago`.
     * 4. Devuelve el total calculado o un error si ocurre un fallo.
     *
     * @param string $campo_canceled Nombre del campo que indica si un pago está cancelado.
     * @param string $campo_monto_pago Nombre del campo que contiene el monto del pago.
     * @param string $campo_movto Nombre del campo que representa el movimiento en la tabla `pago`.
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $pago_corte_id ID del corte de pagos. Debe ser mayor a 0.
     *
     * @return float|array Devuelve el total de los montos como un número decimal, o un arreglo con información de error en caso de fallo.
     *
     * Errores posibles:
     * - `['error' => 'Error al validar pago corte', 'detalle' => $valida]` si falla la validación de los parámetros.
     * - `['error' => 'Error al obtener $filtro_sql', 'detalle' => $filtro_sql]` si ocurre un fallo al generar el filtro SQL.
     * - `['error' => 'Error al obtener $total', 'detalle' => $total]` si ocurre un fallo al calcular la suma de los montos.
     */

    final public function sum_pagos_por_corte(string $campo_canceled, string $campo_monto_pago, string $campo_movto,
                                              PDO $link, int $pago_corte_id)
    {
        $valida = (new _valida())->valida_pago_corte($campo_monto_pago,$campo_movto,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al validar pago corte',$valida);
        }
        $filtro_sql = (new \desarrollo_em3\manejo_datos\sql\pago())->pagos_por_corte_filtro($campo_canceled,
            $campo_monto_pago, $campo_movto, $pago_corte_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $filtro_sql',$filtro_sql);
        }
        $total = (new consultas())->sum_by_filter($campo_monto_pago,'pago',$filtro_sql,$link);
        if(error::$en_error){
            return(new error())->error('Error al obtener $total',$total);
        }

        return $total;

    }


}
