<?php
namespace desarrollo_em3\manejo_datos\data\entidades\pago_corte;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\formato;
use desarrollo_em3\manejo_datos\sql\pago_corte;
use PDO;
use stdClass;

class _reporte_fecha{


    /**
     * EM3
     * Acumula los montos de las cuentas para todos los empleados.
     *
     * Este método recorre el array de empleados, valida que cada empleado y sus depósitos sean correctos,
     * y acumula los montos de las cuentas especificadas en el array `$cuentas` dentro del registro global de empleados.
     *
     * ### Validaciones
     * - Verifica que cada clave en `$empleados` (`$ohem_id`) sea numérica.
     * - Valida que cada valor de `$empleados` sea un objeto.
     * - Asegura que `$empleado->depositos` exista y sea un array.
     *
     * ### Proceso
     * - Para cada empleado, llama al método `acumula_monto_cuentas` para acumular los montos correspondientes a las cuentas.
     *
     * @param array $cuentas Array asociativo donde las claves son alias de cuentas y los valores son descripciones de cuentas.
     * @param array $empleados Array asociativo de empleados, donde cada clave (`$ohem_id`) es el ID del empleado y el valor es un objeto con los datos del empleado.
     *
     * @return array Devuelve el array `$empleados` actualizado con los montos acumulados para todas las cuentas.
     *               En caso de error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Empleados con depósitos válidos
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => 'Cuenta de reporte financiero',
     *     'reporte_operativo' => 'Cuenta de reporte operativo'
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'depositos' => [
     *             (object)[
     *                 'cuenta_empresa_alias' => 'reporte_financiero',
     *                 'deposito_aportaciones_monto_depositado' => 200.0
     *             ],
     *             (object)[
     *                 'cuenta_empresa_alias' => 'reporte_operativo',
     *                 'deposito_aportaciones_monto_depositado' => 150.0
     *             ]
     *         ]
     *     ],
     *     456 => (object)[
     *         'depositos' => [
     *             (object)[
     *                 'cuenta_empresa_alias' => 'reporte_financiero',
     *                 'deposito_aportaciones_monto_depositado' => 100.0
     *             ]
     *         ]
     *     ]
     * ];
     *
     * $resultado = $this->acumula_cuentas_totales($cuentas, $empleados);
     *
     * // Resultado esperado:
     * // $resultado[123]->reporte_financiero->deposito_aportaciones_monto_depositado === 200.0
     * // $resultado[123]->reporte_operativo->deposito_aportaciones_monto_depositado === 150.0
     * // $resultado[456]->reporte_financiero->deposito_aportaciones_monto_depositado === 100.0
     * ```
     *
     * @example Caso: Error en $ohem_id no numérico
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => 'Cuenta de reporte financiero'
     * ];
     * $empleados = [
     *     'id_invalido' => (object)[
     *         'depositos' => []
     *     ]
     * ];
     *
     * $resultado = $this->acumula_cuentas_totales($cuentas, $empleados);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $ohem_id debe ser numérico',
     * //     'data' => 'id_invalido'
     * // ]
     * ```
     *
     * @example Caso: Error en $empleado no es un objeto
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => 'Cuenta de reporte financiero'
     * ];
     * $empleados = [
     *     123 => 'no_es_un_objeto'
     * ];
     *
     * $resultado = $this->acumula_cuentas_totales($cuentas, $empleados);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $empleado debe ser un objeto',
     * //     'data' => 'no_es_un_objeto'
     * // ]
     * ```
     *
     * @example Caso: Error en $empleado->depositos no es un array
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => 'Cuenta de reporte financiero'
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'depositos' => 'no_es_un_array'
     *     ]
     * ];
     *
     * $resultado = $this->acumula_cuentas_totales($cuentas, $empleados);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $empleado->depositos debe ser un array',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function acumula_cuentas_totales(array $cuentas, array $empleados): array
    {
        // Validar que $empleados sea un array asociativo con objetos
        foreach ($empleados as $ohem_id => $empleado) {
            if (!is_numeric($ohem_id)) {
                return (new error())->error('Error $ohem_id debe ser numérico', $ohem_id);
            }
            if (!is_object($empleado)) {
                return (new error())->error('Error $empleado debe ser un objeto', $empleado);
            }
            if(!isset($empleado->depositos)){
                return  (new error())->error('Error $empleado->depositos debe existir',$empleado);
            }
            if(!is_array($empleado->depositos)){
                return  (new error())->error('Error $empleado->depositos debe ser un array',$empleado);
            }

            // Acumular montos para las cuentas del empleado
            $empleados = $this->acumula_monto_cuentas($cuentas, $empleado, $empleados, $ohem_id);
            if (error::$en_error) {
                return (new error())->error('Error al acumular montos para las cuentas', $empleados);
            }
        }

        return $empleados;
    }

    /**
     * EM3
     * Acumula el monto de un depósito en una cuenta específica del reporte de empleados.
     *
     * Este método valida los datos del depósito y verifica si el alias de la cuenta en el depósito coincide con
     * el alias proporcionado. Si coincide, acumula el monto del depósito en el reporte correspondiente del empleado.
     *
     * @param string $alias El alias de la cuenta en el reporte de empleados donde se realizará la acumulación.
     *                      Debe ser una cadena no vacía.
     * @param stdClass $deposito Objeto que contiene los datos del depósito. Debe incluir las propiedades:
     *                           - `cuenta_empresa_alias` (cadena no vacía).
     *                           - `deposito_aportaciones_monto_depositado` (número).
     * @param array $empleados Array asociativo de empleados, donde la clave es el ID del empleado (`ohem_id`)
     *                         y el valor es un objeto que contiene los datos del reporte.
     * @param int $ohem_id ID del empleado cuyo reporte será actualizado. Debe ser mayor o igual a 0.
     *
     * @return array Devuelve el array `$empleados` actualizado si todo es correcto.
     *                     Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Acumulación de monto en la cuenta
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[
     *     'cuenta_empresa_alias' => 'reporte_financiero',
     *     'deposito_aportaciones_monto_depositado' => 500.25
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_cuenta($alias, $deposito, $empleados, $ohem_id);
     * // Resultado esperado:
     * // [
     * //     123 => (object)[
     * //         'reporte_financiero' => (object)[
     * //             'deposito_aportaciones_monto_depositado' => 1500.25
     * //         ]
     * //     ]
     * // ]
     * ```
     *
     * @example Caso: Alias del depósito no coincide
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[
     *     'cuenta_empresa_alias' => 'otra_cuenta',
     *     'deposito_aportaciones_monto_depositado' => 500.25
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_cuenta($alias, $deposito, $empleados, $ohem_id);
     * // Resultado esperado:
     * // [
     * //     123 => (object)[
     * //         'reporte_financiero' => (object)[
     * //             'deposito_aportaciones_monto_depositado' => 1000.00
     * //         ]
     * //     ]
     * // ]
     * ```
     *
     * @example Caso de error: Datos del depósito no válidos
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[
     *     'cuenta_empresa_alias' => '',
     *     'deposito_aportaciones_monto_depositado' => 500.25
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_cuenta($alias, $deposito, $empleados, $ohem_id);
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $deposito->cuenta_empresa_alias esta vacio',
     * //     'data' => $deposito
     * // ]
     * ```
     */

    private function acumula_monto_cuenta(string $alias, stdClass $deposito, array $empleados, int $ohem_id): array
    {
        $valida = $this->valida_sum_deposito($alias,$deposito,$empleados, $ohem_id);
        if(error::$en_error){
            return  (new error())->error('Error al validar datos',$valida);
        }

        if($deposito->cuenta_empresa_alias === $alias) {
            $empleados = $this->acumula_monto_dep($alias,$deposito,$empleados,$ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al integrar cuenta',$empleados);
            }
        }
        return $empleados;
    }

    /**
     * EM3
     * Acumula el monto depositado en el reporte de un empleado.
     *
     * Este método valida la existencia y consistencia de los datos necesarios y, si son correctos,
     * acumula el monto del depósito (`deposito_aportaciones_monto_depositado`) en el alias correspondiente
     * dentro del objeto del empleado especificado.
     *
     * @param string $alias El alias dentro del reporte de empleados donde se acumulará el monto depositado.
     *                      Debe ser una cadena no vacía.
     * @param stdClass $deposito Objeto que contiene los datos del depósito. Debe incluir la propiedad:
     *                           - `deposito_aportaciones_monto_depositado` (numérico).
     * @param array $empleados Array asociativo de empleados, donde la clave es el ID del empleado (`ohem_id`)
     *                         y el valor es un objeto que contiene los datos del reporte.
     * @param int $ohem_id ID del empleado al que se acumulará el monto. Debe ser mayor o igual a 0.
     *
     * @return array Devuelve el array `$empleados` actualizado con el monto acumulado. Si ocurre un error,
     *                     devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Monto acumulado correctamente
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[
     *     'deposito_aportaciones_monto_depositado' => 500.25
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_dep($alias, $deposito, $empleados, $ohem_id);
     * // Resultado:
     * // [
     * //     123 => (object)[
     * //         'reporte_financiero' => (object)[
     * //             'deposito_aportaciones_monto_depositado' => 1500.25
     * //         ]
     * //     ]
     * // ]
     * ```
     *
     * @example Caso de error: Alias vacío
     * ```php
     * $alias = '';
     * $deposito = (object)[
     *     'deposito_aportaciones_monto_depositado' => 500.25
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_dep($alias, $deposito, $empleados, $ohem_id);
     * // Resultado:
     * // [
     * //     'error' => 'Error $alias esta vacio',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Caso de error: `ohem_id` no existe en `$empleados`
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[
     *     'deposito_aportaciones_monto_depositado' => 500.25
     * ];
     * $empleados = [
     *     456 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_dep($alias, $deposito, $empleados, $ohem_id);
     * // Resultado:
     * // [
     * //     'error' => '$empleados[123] no existe',
     * //     'data' => [...]
     * // ]
     * ```
     *
     * @example Caso de error: Depósito no contiene la propiedad requerida
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_dep($alias, $deposito, $empleados, $ohem_id);
     * // Resultado:
     * // [
     * //     'error' => '$deposito->deposito_aportaciones_monto_depositado no existe',
     * //     'data' => $deposito
     * // ]
     * ```
     */

    private function acumula_monto_dep(string $alias, stdClass $deposito, array $empleados, int $ohem_id): array
    {
        $valida = $this->valida_dato_reporte($alias,$deposito,$empleados,$ohem_id);
        if(error::$en_error){
            return  (new error())->error('Error al validar datos',$valida);
        }
        $empleados[$ohem_id]->$alias->deposito_aportaciones_monto_depositado
            += $deposito->deposito_aportaciones_monto_depositado;
        return $empleados;

    }

    /**
     * EM3
     * Acumula los montos de los depósitos en una cuenta específica del reporte de empleados.
     *
     * Este método recorre los depósitos asociados a un empleado y valida cada depósito.
     * Si el alias de la cuenta en el depósito coincide con el alias especificado, acumula su monto
     * en el reporte correspondiente del empleado.
     *
     * @param string $alias El alias de la cuenta en el reporte de empleados donde se realizará la acumulación.
     *                      Debe ser una cadena no vacía.
     * @param stdClass $empleado Objeto que contiene los datos del empleado. Debe incluir:
     *                           - `depositos` (array de objetos): Lista de depósitos asociados al empleado.
     * @param array $empleados Array asociativo de empleados, donde la clave es el ID del empleado (`ohem_id`)
     *                         y el valor es un objeto que contiene los datos del reporte.
     * @param int $ohem_id ID del empleado cuyo reporte será actualizado. Debe ser mayor o igual a 0.
     *
     * @return array Devuelve el array `$empleados` actualizado si todo es correcto.
     *                     Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Acumulación de montos de depósitos
     * ```php
     * $alias = 'reporte_financiero';
     * $empleado = (object)[
     *     'depositos' => [
     *         (object)[
     *             'cuenta_empresa_alias' => 'reporte_financiero',
     *             'deposito_aportaciones_monto_depositado' => 500.25
     *         ],
     *         (object)[
     *             'cuenta_empresa_alias' => 'otra_cuenta',
     *             'deposito_aportaciones_monto_depositado' => 200.00
     *         ]
     *     ]
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_cuenta_deposito($alias, $empleado, $empleados, $ohem_id);
     * // Resultado esperado:
     * // [
     * //     123 => (object)[
     * //         'reporte_financiero' => (object)[
     * //             'deposito_aportaciones_monto_depositado' => 1500.25
     * //         ]
     * //     ]
     * // ]
     * ```
     *
     * @example Caso: Sin depósitos en el empleado
     * ```php
     * $alias = 'reporte_financiero';
     * $empleado = (object)[
     *     'depositos' => []
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_cuenta_deposito($alias, $empleado, $empleados, $ohem_id);
     * // Resultado esperado:
     * // [
     * //     123 => (object)[
     * //         'reporte_financiero' => (object)[
     * //             'deposito_aportaciones_monto_depositado' => 1000.00
     * //         ]
     * //     ]
     * // ]
     * ```
     *
     * @example Caso de error: `$empleado->depositos` no existe
     * ```php
     * $alias = 'reporte_financiero';
     * $empleado = (object)[];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_cuenta_deposito($alias, $empleado, $empleados, $ohem_id);
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $empleado->depositos debe existir',
     * //     'data' => $empleado
     * // ]
     * ```
     *
     * @example Caso de error: `$deposito` no es un objeto
     * ```php
     * $alias = 'reporte_financiero';
     * $empleado = (object)[
     *     'depositos' => [
     *         'No es un objeto'
     *     ]
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_cuenta_deposito($alias, $empleado, $empleados, $ohem_id);
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $deposito debe ser un objeto',
     * //     'data' => 'No es un objeto'
     * // ]
     * ```
     */

    private function acumula_monto_cuenta_deposito(
        string $alias, stdClass $empleado, array $empleados, int $ohem_id): array
    {
        if(!isset($empleado->depositos)){
            return  (new error())->error('Error $empleado->depositos debe existir',$empleado);
        }
        if(!is_array($empleado->depositos)){
            return  (new error())->error('Error $empleado->depositos debe ser un array',$empleado);
        }
        foreach ($empleado->depositos as $deposito){
            if(!is_object($deposito)){
                return  (new error())->error('Error $deposito debe se run objeto',$deposito);
            }


            $valida = $this->valida_sum_deposito($alias,$deposito,$empleados, $ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al validar datos',$valida);
            }

            $empleados = $this->acumula_monto_cuenta($alias,$deposito,$empleados, $ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al integrar cuenta',$empleados);
            }
        }
        return $empleados;

    }


    /**
     * EM3
     * Acumula los montos asociados a las cuentas de un empleado en el registro general de empleados.
     *
     * Este método toma un conjunto de cuentas y acumula los valores de los depósitos asociados a un empleado
     * específico, identificado por `$ohem_id`, en la estructura global de `$empleados`.
     *
     * ### Validaciones
     * - Verifica que `$empleado->depositos` exista y sea un array.
     * - Asegura que las propiedades necesarias dentro de `$empleados[$ohem_id]` estén inicializadas.
     *
     * ### Proceso
     * - Itera sobre las cuentas proporcionadas y crea/inicializa las propiedades necesarias en `$empleados`.
     * - Llama al método `acumula_monto_cuenta_deposito` para procesar los depósitos asociados a cada cuenta.
     *
     * @param array $cuentas Array asociativo donde las claves representan los alias de las cuentas y los valores son las descripciones de las cuentas.
     * @param stdClass $empleado Objeto que contiene los datos del empleado y sus depósitos.
     * @param array $empleados Array global que contiene todos los empleados. Este será actualizado con los acumulados.
     * @param int $ohem_id Identificador único del empleado en `$empleados`.
     *
     * @return array Devuelve el array `$empleados` actualizado con los montos acumulados para las cuentas del empleado.
     *               Si ocurre un error, devuelve un array con detalles del error.
     *
     * @example Caso exitoso: Empleado con depósitos válidos
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => 'Cuenta de reporte financiero',
     *     'reporte_operativo' => 'Cuenta de reporte operativo'
     * ];
     * $empleado = (object)[
     *     'depositos' => [
     *         (object)[
     *             'cuenta_empresa_alias' => 'reporte_financiero',
     *             'deposito_aportaciones_monto_depositado' => 150.0
     *         ],
     *         (object)[
     *             'cuenta_empresa_alias' => 'reporte_operativo',
     *             'deposito_aportaciones_monto_depositado' => 300.0
     *         ]
     *     ]
     * ];
     * $empleados = [
     *     123 => (object)[]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_cuentas($cuentas, $empleado, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // $resultado[123]->reporte_financiero->deposito_aportaciones_monto_depositado === 150.0
     * // $resultado[123]->reporte_operativo->deposito_aportaciones_monto_depositado === 300.0
     * ```
     *
     * @example Caso: Error en `$empleado->depositos` no existe
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => 'Cuenta de reporte financiero'
     * ];
     * $empleado = (object)[];
     * $empleados = [
     *     123 => (object)[]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_cuentas($cuentas, $empleado, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $empleado->depositos debe existir',
     * //     'data' => $empleado
     * // ]
     * ```
     *
     * @example Caso: Error en `$empleado->depositos` no es un array
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => 'Cuenta de reporte financiero'
     * ];
     * $empleado = (object)[
     *     'depositos' => 'string_invalido'
     * ];
     * $empleados = [
     *     123 => (object)[]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->acumula_monto_cuentas($cuentas, $empleado, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $empleado->depositos debe ser un array',
     * //     'data' => $empleado
     * // ]
     * ```
     */

    private function acumula_monto_cuentas(array $cuentas, stdClass $empleado, array $empleados, int $ohem_id): array
    {
        if(!isset($empleado->depositos)){
            return  (new error())->error('Error $empleado->depositos debe existir',$empleado);
        }
        if(!is_array($empleado->depositos)){
            return  (new error())->error('Error $empleado->depositos debe ser un array',$empleado);
        }
        foreach ($cuentas as $alias=>$cuenta){

            if(!isset($empleados[$ohem_id]->$alias)){
                $empleados[$ohem_id]->$alias = new stdClass();
            }
            if(!isset($empleados[$ohem_id]->$alias->deposito_aportaciones_monto_depositado)){
                $empleados[$ohem_id]->$alias->deposito_aportaciones_monto_depositado = 0.0;
            }

            $empleados = $this->acumula_monto_cuenta_deposito($alias,$empleado,$empleados,$ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al integrar cuenta',$empleados);
            }
        }
        return $empleados;
    }

    private function acumula_totales(stdClass $corte, stdClass $datos, int $ohem_id): stdClass
    {
        if(!isset($corte->pago_corte_monto_total)){
            $corte->pago_corte_monto_total = 0.0;
        }
        if(!isset($corte->pago_corte_monto_por_depositar)){
            $corte->pago_corte_monto_por_depositar = 0.0;
        }
        if(!isset($corte->pago_corte_monto_depositado)){
            $corte->pago_corte_monto_depositado = 0.0;
        }
        if(!isset($datos->empleados)){
            $datos->empleados = array();
        }
        if(!isset($datos->empleados)){
            $datos->empleados = array();
        }
        if(!isset($datos->empleados[$ohem_id])){
            $datos->empleados[$ohem_id] = new stdClass();
        }
        if(!isset($datos->empleados[$ohem_id]->pago_corte_monto_total)){
            $datos->empleados[$ohem_id]->pago_corte_monto_total = 0.0;
        }
        if(!isset($datos->empleados[$ohem_id]->pago_corte_monto_por_depositar)){
            $datos->empleados[$ohem_id]->pago_corte_monto_por_depositar = 0.0;
        }
        if(!isset($datos->empleados[$ohem_id]->pago_corte_monto_depositado)){
            $datos->empleados[$ohem_id]->pago_corte_monto_depositado = 0.0;
        }
        $datos->empleados[$ohem_id]->pago_corte_monto_total += $corte->pago_corte_monto_total;
        $datos->empleados[$ohem_id]->pago_corte_monto_por_depositar += $corte->pago_corte_monto_por_depositar;
        $datos->empleados[$ohem_id]->pago_corte_monto_depositado += $corte->pago_corte_monto_depositado;

        return $datos;

    }

    private function acumula_totales_corte(stdClass $empleado, stdClass $datos, int $ohem_id)
    {
        if(!isset($empleado->cortes)){
            $empleado->cortes = array();
        }
        if(!is_array($empleado->cortes)){
            return  (new error())->error('Error $empleado->cortes debe ser un array',$empleado);
        }
        foreach ($empleado->cortes as $corte){
            if(!is_object($corte)){
                return  (new error())->error('Error corte debe ser un objeto',$corte);
            }
            $datos = $this->acumula_totales($corte,$datos,$ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al acumular totales',$datos);
            }
        }
        return $datos;

    }

    private function ajusta_dato(array $campos_moneda, stdClass $dato): array
    {
        $dato = (array)$dato;
        foreach ($dato as $campo=>$val){

            $dato = $this->integra_value_moneda($campo, $campos_moneda, $dato, $val);
            if (error::$en_error) {
                return (new error())->error('Error al integrar formato moneda', $val);
            }

        }
        return $dato;

    }

    final public function campos_moneda(array $datos): array
    {
        $campos_moneda = array('pago_corte_monto_total','pago_corte_monto_por_depositar','pago_corte_monto_depositado');

        foreach ($datos as $ind=>$dato){
            if(!isset($dato->cuentas)){
                $dato->cuentas = array();
            }
            $cuentas = $dato->cuentas;
            foreach ($cuentas as $campo=>$val){
                $campos_moneda[] = $campo;
            }

        }
        return $campos_moneda;

    }

    final public function cols_rpt(array $datos): array
    {
        $cols = array();
        if(isset($datos[0])){
            $cols = array_keys((array)$datos[0]);
        }
        return $cols;

    }

    /**
     * TRASLADADO
     * Obtiene los cortes de pago para empleados en plazas dentro de un rango de fechas.
     *
     * La función valida que los parámetros proporcionados (fechas y entidad de empleado) sean correctos,
     * luego genera una consulta SQL para obtener los cortes de pago utilizando la entidad `pago_corte`.
     * Si la validación o la generación de la consulta SQL falla, se devuelve un error.
     * Si la consulta se ejecuta correctamente, devuelve un array con los cortes de pago.
     *
     * @param array $empleados_id Un array de IDs de los empleados para los que se obtendrán los cortes de pago.
     * @param string $entidad_empleado El nombre de la entidad de empleado.
     * @param string $fecha_final La fecha final del rango de búsqueda.
     * @param string $fecha_inicial La fecha inicial del rango de búsqueda.
     * @param PDO $link La conexión PDO a la base de datos.
     * @param array $plazas_id Un array de IDs de las plazas para filtrar los cortes.
     *
     * @return array Devuelve un array con los cortes de pago. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function cortes(
        array $empleados_id, string $entidad_empleado, string $fecha_final, string $fecha_inicial, PDO $link,
        array $plazas_id): array
    {

        $valida = $this->valida_fechas($entidad_empleado,$fecha_final,$fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al validar datos', $valida);
        }

        $sql = (new pago_corte())->cortes($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial,$plazas_id);
        if(error::$en_error){
            return (new error())->error('Error al generar sql',$sql);
        }

        $cortes = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener cortes',$cortes);
        }
        return $cortes;
    }


    /**
     * EM3
     * Inicializa una nueva cuenta en un array de cuentas.
     *
     * Esta función recibe un alias de cuenta y un array de cuentas ya existente.
     * Si el alias de la cuenta es válido, se agrega al array con un nuevo objeto `stdClass()`.
     *
     * ### Validaciones
     * - Verifica que `$cuenta_empresa_alias` no esté vacío después de ser limpiado con `trim()`.
     *
     * ### Proceso
     * - Se recorta cualquier espacio en blanco del alias de la cuenta.
     * - Se valida que el alias no sea una cadena vacía.
     * - Se agrega la nueva cuenta al array de cuentas con una instancia de `stdClass()`.
     *
     * @param string $cuenta_empresa_alias El alias de la cuenta a inicializar.
     *                                     Debe ser una cadena no vacía.
     *
     * @param array $cuentas Un array asociativo donde las claves son alias de cuentas
     *                       y los valores son objetos `stdClass` que representan las cuentas.
     *
     * @return array Devuelve el array `$cuentas` actualizado con la nueva cuenta agregada.
     *               Si `$cuenta_empresa_alias` está vacío, devuelve un array con un mensaje de error.
     *
     * @throws error Si `$cuenta_empresa_alias` está vacío, se genera un error con un mensaje detallado.
     *
     * @example Caso exitoso: Agregar una nueva cuenta
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => new stdClass(),
     *     'reporte_operativo' => new stdClass()
     * ];
     *
     * $cuenta_empresa_alias = 'cuenta_inversion';
     *
     * $resultado = $this->cuenta_nueva($cuenta_empresa_alias, $cuentas);
     *
     * // Resultado esperado:
     * // $resultado['cuenta_inversion'] === stdClass()
     * ```
     *
     * @example Caso de error: `$cuenta_empresa_alias` está vacío
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => new stdClass()
     * ];
     *
     * $cuenta_empresa_alias = '';
     *
     * $resultado = $this->cuenta_nueva($cuenta_empresa_alias, $cuentas);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $cuenta_empresa_alias esta vacia',
     * //     'data' => ''
     * // ]
     * ```
     */
    private function cuenta_nueva(string $cuenta_empresa_alias, array $cuentas): array
    {
        $cuenta_empresa_alias = trim($cuenta_empresa_alias);
        if ($cuenta_empresa_alias === '') {
            return (new error())->error('Error $cuenta_empresa_alias esta vacia', $cuenta_empresa_alias);
        }
        $cuentas[$cuenta_empresa_alias] = new stdClass();
        return $cuentas;
    }



    /**
     * EM3
     * Procesa los depósitos de un empleado y registra las cuentas asociadas en el array `$cuentas`.
     *
     * Esta función recorre los depósitos de un empleado, verifica la existencia de los alias de las cuentas,
     * y las integra en la estructura de `$cuentas`. Si una cuenta aún no está registrada, se inicializa utilizando
     * el método `init_cuenta_nueva()`.
     *
     * ### Validaciones
     * - Verifica que `$empleado->depositos` exista.
     * - Asegura que `$empleado->depositos` sea un array.
     * - Confirma que `$ohem_id` sea un número mayor a 0.
     * - Valida que cada elemento de `$empleado->depositos` sea un objeto.
     * - Verifica que cada depósito contenga la propiedad `cuenta_empresa_alias` y que esta no esté vacía.
     *
     * ### Proceso
     * - Recorre los depósitos del empleado y valida su estructura.
     * - Extrae el alias de la cuenta (`cuenta_empresa_alias`) de cada depósito.
     * - Si la cuenta aún no está registrada, la inicializa con `init_cuenta_nueva()`.
     *
     * @param array $cuentas Un array asociativo donde las claves son los alias de cuentas y los valores son `stdClass` que representan las cuentas.
     *
     * @param stdClass $empleado Un objeto que contiene la información del empleado. Debe incluir la propiedad:
     *                           - `depositos` (array de objetos): Lista de depósitos del empleado.
     *
     * @param int $ohem_id El identificador único del empleado. Debe ser un número mayor a 0.
     *
     * @return array Devuelve el array `$cuentas` actualizado con las cuentas extraídas de los depósitos del empleado.
     *               Si ocurre un error, devuelve un array con un mensaje de error.
     *
     * @throws error Si `$empleado->depositos` no existe o no es un array, si `$ohem_id` es menor o igual a 0,
     *               si algún depósito no es un objeto o si el alias de la cuenta está vacío.
     *
     * @example Caso exitoso: Registrar cuentas a partir de depósitos del empleado
     * ```php
     * $cuentas = [];
     *
     * $empleado = (object)[
     *     'depositos' => [
     *         (object)[
     *             'cuenta_empresa_alias' => 'reporte_financiero'
     *         ],
     *         (object)[
     *             'cuenta_empresa_alias' => 'reporte_operativo'
     *         ]
     *     ]
     * ];
     *
     * $ohem_id = 123;
     *
     * $resultado = $this->cuenta_by_empleado($cuentas, $empleado, $ohem_id);
     *
     * // Resultado esperado:
     * // $resultado['reporte_financiero'] === stdClass()
     * // $resultado['reporte_operativo'] === stdClass()
     * ```
     *
     * @example Caso de error: `$empleado->depositos` no existe
     * ```php
     * $cuentas = [];
     *
     * $empleado = (object)[];
     * $ohem_id = 123;
     *
     * $resultado = $this->cuenta_by_empleado($cuentas, $empleado, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $empleado->depositos no existe',
     * //     'data' => $empleado
     * // ]
     * ```
     *
     * @example Caso de error: `$empleado->depositos` no es un array
     * ```php
     * $cuentas = [];
     *
     * $empleado = (object)[
     *     'depositos' => 'no_es_un_array'
     * ];
     *
     * $ohem_id = 123;
     *
     * $resultado = $this->cuenta_by_empleado($cuentas, $empleado, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $empleado->depositos debe ser un array',
     * //     'data' => $empleado
     * // ]
     * ```
     *
     * @example Caso de error: `$ohem_id` es menor o igual a 0
     * ```php
     * $cuentas = [];
     *
     * $empleado = (object)[
     *     'depositos' => []
     * ];
     *
     * $ohem_id = 0;
     *
     * $resultado = $this->cuenta_by_empleado($cuentas, $empleado, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $ohem_id es menor a 0',
     * //     'data' => 0
     * // ]
     * ```
     *
     * @example Caso de error: `cuenta_empresa_alias` está vacío en un depósito
     * ```php
     * $cuentas = [];
     *
     * $empleado = (object)[
     *     'depositos' => [
     *         (object)[
     *             'cuenta_empresa_alias' => ''
     *         ]
     *     ]
     * ];
     *
     * $ohem_id = 123;
     *
     * $resultado = $this->cuenta_by_empleado($cuentas, $empleado, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $deposito->cuenta_empresa_alias esta vacia',
     * //     'data' => $empleado->depositos[0]
     * // ]
     * ```
     */
    private function cuenta_by_empleado(array $cuentas, stdClass $empleado, int $ohem_id): array
    {
        if (!isset($empleado->depositos)) {
            return (new error())->error('Error $empleado->depositos no existe', $empleado);
        }
        if (!is_array($empleado->depositos)) {
            return (new error())->error('Error $empleado->depositos debe ser un array', $empleado);
        }
        if ($ohem_id <= 0) {
            return (new error())->error('Error $ohem_id es menor a 0', $ohem_id);
        }
        foreach ($empleado->depositos as $deposito) {
            if (!is_object($deposito)) {
                return (new error())->error('Error $deposito debe ser un objeto', $deposito);
            }
            if (!isset($deposito->cuenta_empresa_alias)) {
                return (new error())->error('Error $deposito->cuenta_empresa_alias no existe', $deposito);
            }

            $deposito->cuenta_empresa_alias = trim($deposito->cuenta_empresa_alias);
            if ($deposito->cuenta_empresa_alias === '') {
                return (new error())->error('Error $deposito->cuenta_empresa_alias esta vacia', $deposito);
            }

            $cuentas = $this->init_cuenta_nueva($deposito->cuenta_empresa_alias, $cuentas, $ohem_id);
            if (error::$en_error) {
                return (new error())->error('Error al integrar cuenta', $cuentas);
            }
        }
        return $cuentas;
    }


    private function cuentas(array $empleados): array
    {
        $cuentas = array();
        foreach ($empleados as $ohem_id=>$empleado){
            $cuentas = $this->cuenta_by_empleado($cuentas,$empleado,$ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al integrar cuenta',$cuentas);
            }

        }
        return $cuentas;

    }

    final public function cuentas_totales(array $datos): array
    {
        $cuentas_totales = array();

        foreach ($datos as $ind=>$dato){
            if(!isset($dato->cuentas)){
                $dato->cuentas = array();
            }
            $cuentas = $dato->cuentas;
            foreach ($cuentas as $campo=>$val){
                if(!isset($cuentas_totales[$campo])){
                    $cuentas_totales[$campo] = 0;
                }
                $cuentas_totales[$campo] += (float)$dato->$campo;
            }

        }

        foreach ($cuentas_totales as $campo=>$value){
            $cuentas_totales[$campo] = (new formato())->moneda($value);
            if(error::$en_error){
                return  (new error())->error('Error al integrar $cuentas_totales',$cuentas_totales);
            }
        }

        return $cuentas_totales;

    }

    private function dato_rpt(array $campos_moneda, stdClass $dato): array
    {
        $dato = $this->init_dato($dato);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $dato', $dato);
        }

        $dato = $this->ajusta_dato($campos_moneda,$dato);
        if (error::$en_error) {
            return (new error())->error('Error al integrar formato moneda', $dato);
        }

        return $dato;

    }

    /**
     * TRASLADADO
     * Obtiene los datos base del reporte, incluyendo cortes y depósitos.
     *
     * Esta función valida las fechas proporcionadas, luego obtiene los registros de cortes y depósitos correspondientes
     * a los empleados y plazas en el rango de fechas especificado. Utiliza una conexión PDO para ejecutar las consultas.
     *
     * @param array $empleados_id       Lista de IDs de empleados para filtrar el reporte.
     * @param string $entidad_empleado  Nombre de la entidad de empleado.
     * @param string $fecha_final       Fecha final del rango en formato 'Y-m-d'.
     * @param string $fecha_inicial     Fecha inicial del rango en formato 'Y-m-d'.
     * @param PDO $link                 Conexión PDO a la base de datos.
     * @param array $plazas_id          Lista de IDs de plazas para filtrar el reporte.
     *
     * @return stdClass|array Devuelve un objeto con las propiedades `cortes` y `depositos`, que contienen los registros obtenidos.
     *                  Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function datos_base_rpt(
        array $empleados_id, string $entidad_empleado, string $fecha_final, string $fecha_inicial, PDO $link,
        array $plazas_id)
    {
        $valida = $this->valida_fechas($entidad_empleado,$fecha_final,$fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al validar datos', $valida);
        }
        $cortes = $this->cortes($empleados_id, $entidad_empleado,$fecha_final,$fecha_inicial,$link,$plazas_id);
        if(error::$en_error){
            return  (new error())->error('Error al obtener cortes',$cortes);
        }


        $depositos = $this->depositos(
            $empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial, $link, $plazas_id);

        if(error::$en_error){
            return  (new error())->error('Error al obtener depositos',$depositos);
        }

        $datos = new stdClass();
        $datos->cortes = $cortes;
        $datos->depositos = $depositos;
        return $datos;

    }

    final public  function datos_completos_reporte(array $empleados_id, string $entidad_empleado, string $fecha_final,
                                             string $fecha_inicial, PDO $link, array $plazas_id)
    {
        $empleados = $this->empleados_base(
            $empleados_id,$entidad_empleado,$fecha_final, $fecha_inicial,$link,$plazas_id);
        if(error::$en_error){
            return  (new error())->error('Error al integrar row',$empleados);
        }

        $datos = $this->datos_reporte_base($empleados);
        if(error::$en_error){
            return  (new error())->error('Error al acumular totales',$datos);
        }
        return $datos;

    }

    private function datos_reporte_base(array $empleados)
    {
        $datos = $this->integra_cuentas_rpt($empleados);
        if(error::$en_error){
            return  (new error())->error('Error al integrar cuenta',$empleados);
        }

        $datos = $this->datos_reporte_corte($datos);
        if(error::$en_error){
            return  (new error())->error('Error al acumular totales',$datos);
        }
        return $datos;

    }


    private function datos_reporte_corte(stdClass $datos)
    {
        if(!isset($datos->empleados)){
            $datos->empleados = array();
        }
        foreach ($datos->empleados as $ohem_id=>$empleado){
            if(!is_object($empleado)){
                return  (new error())->error('Error $empleado debe ser un objeto',$datos);
            }
            $datos = $this->acumula_totales_corte($empleado,$datos,$ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al acumular totales',$datos);
            }
        }
        return $datos;

    }

    final public function datos_rpt(array $campos_moneda, array $datos): array
    {
        foreach ($datos as $ind=>$dato){
            $dato = $this->dato_rpt($campos_moneda, $dato);
            if (error::$en_error) {
                return (new error())->error('Error al integrar $dato', $dato);
            }

            $datos[$ind] = $dato;
        }
        return $datos;

    }

    /**
     * TRASLADADO
     * Obtiene los registros de depósitos para un conjunto de empleados en un rango de fechas.
     *
     * Esta función valida las fechas, genera la consulta SQL necesaria y ejecuta dicha consulta para obtener
     * los datos de los depósitos, filtrados por empleados y plazas. Se espera que las fechas y los IDs de empleados
     * y plazas sean válidos.
     *
     * @param array $empleados_id       Lista de IDs de empleados para filtrar el reporte.
     * @param string $entidad_empleado  Nombre de la entidad de empleado.
     * @param string $fecha_final       Fecha final del rango en formato 'Y-m-d'.
     * @param string $fecha_inicial     Fecha inicial del rango en formato 'Y-m-d'.
     * @param PDO $link                 Conexión PDO a la base de datos.
     * @param array $plazas_id          Lista de IDs de plazas para filtrar el reporte.
     *
     * @return array Devuelve un array con los registros de depósitos. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function depositos(
        array $empleados_id, string $entidad_empleado, string $fecha_final, string $fecha_inicial, PDO $link,
        array $plazas_id): array
    {

        $valida = (new _reporte_fecha())->valida_fechas($entidad_empleado,$fecha_final,$fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al validar datos', $valida);
        }

        $sql = (new pago_corte())->depositos($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial,$plazas_id);
        if(error::$en_error){
            return (new error())->error('Error al generar sql',$sql);
        }

        $depositos = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener $depositos',$depositos);
        }
        return $depositos;

    }

    private function empleado_rpt(stdClass $datos, stdClass $empleado, string $format): array
    {
        $keys_rpt = $this->key_rpt();
        if(error::$en_error){
            return  (new error())->error('Error al integrar $keys_rpt',$keys_rpt);
        }

        $empleado_rpt = $this->init_rpt_by_empleado($empleado,$keys_rpt);
        if (error::$en_error) {
            return (new error())->error('Error al integrar key', $empleado_rpt);
        }

        $empleado_rpt = $this->integra_total_cuentas($datos, $empleado,$empleado_rpt, $format);
        if(error::$en_error){
            return  (new error())->error('Error al integrar cuenta',$empleado_rpt);
        }

        return $empleado_rpt;

    }

    private function empleados_base(
        array $empleados_id, string $entidad_empleado, string $fecha_final, string $fecha_inicial, PDO $link,
        array $plazas_id): array
    {
        $datos = $this->datos_base_rpt($empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial, $link,$plazas_id);
        if(error::$en_error){
            return  (new error())->error('Error al obtener $datos',$datos);
        }

        $empleados = $this->empleados_con_detalle($datos);
        if(error::$en_error){
            return  (new error())->error('Error al integrar row',$empleados);
        }

        $empleados = $this->integra_contadores($empleados);
        if(error::$en_error){
            return  (new error())->error('Error al integrar row',$empleados);
        }
        return $empleados;

    }

    private function empleados_con_detalle(stdClass $datos): array
    {
        $empleados = $this->inicializa_empleados_rpt($datos->cortes, $datos->depositos);
        if(error::$en_error){
            return  (new error())->error('Error al inicializar rows',$empleados);
        }

        $empleados = $this->integra_rows_empleados_rpt($datos,$empleados);
        if(error::$en_error){
            return  (new error())->error('Error al integrar row',$empleados);
        }
        return $empleados;


    }

    final public function empleados_rpt(stdClass $datos, string $format): array
    {
        $empleados_rpt = array();
        foreach ($datos->empleados as $empleado){

            $empleado_rpt = $this->empleado_rpt($datos,$empleado, $format);
            if (error::$en_error) {
                return (new error())->error('Error al integrar $empleado_rpt', $empleado_rpt);
            }

            $empleados_rpt[] = $empleado_rpt;
        }
        return $empleados_rpt;

    }

    /**
     * TRASLADADO
     * Formatea los datos de una cuenta en el reporte de un empleado.
     *
     * Esta función se encarga de validar el alias de la cuenta, asegurarse de que exista
     * dentro del array `$empleado_rpt`, y formatearlo en un array dentro de la clave `cuentas`.
     * Si el alias de la cuenta no existe, se inicializa con un valor predeterminado de `0.0`.
     * Finalmente, el alias de la cuenta se agrega al array de `cuentas` en el reporte del empleado.
     *
     * @param string $cuenta_alias El alias de la cuenta que se desea formatear. Este valor debe ser una cadena no vacía.
     * @param array $empleado_rpt El array que contiene los datos del empleado. Este array se utilizará
     * para agregar o actualizar las cuentas asociadas al empleado.
     *
     * @return array Retorna el array `$empleado_rpt` actualizado con el alias de la cuenta y su valor.
     * Si ocurre algún error (por ejemplo, si el alias está vacío), se retorna un array de error con los detalles.
     *
     * @throws error Si el alias de la cuenta está vacío, se lanza una excepción con detalles del error.
     */
    private function format_json(string $cuenta_alias, array $empleado_rpt): array
    {
        $cuenta_alias = trim($cuenta_alias);
        if($cuenta_alias === ''){
            return (new error())->error('Error $cuenta_alias esta vacia', $cuenta_alias);
        }
        if(is_numeric($cuenta_alias)){
            $cuenta_alias = "$cuenta_alias";
        }
        if(!isset($empleado_rpt['cuentas'])){
            $empleado_rpt['cuentas'] = array();
        }
        if(!isset($empleado_rpt['cuentas_data'])){
            $empleado_rpt['cuentas_data'] = '';
        }
        if(!isset($empleado_rpt[$cuenta_alias])){
            $empleado_rpt[$cuenta_alias] = 0.0;
        }
        $empleado_rpt['cuentas'][$cuenta_alias] = $empleado_rpt[$cuenta_alias];
        if((float)$empleado_rpt[$cuenta_alias] > 0.0) {
            $empleado_rpt['cuentas_data'] .= " $cuenta_alias: $empleado_rpt[$cuenta_alias] ";
        }

        return $empleado_rpt;

    }

    /**
     * TRASLADADO
     * Inicializa el reporte de empleados a partir de cortes y depósitos.
     *
     * Esta función procesa dos arrays de datos: `$cortes` y `$depositos`, ambos contienen registros relacionados
     * con empleados. La función integra estos registros en un array de empleados, donde cada empleado es identificado
     * por su `ohem_id`. Los datos de cortes y depósitos se combinan para generar un reporte completo de empleados.
     *
     * La función valida y organiza los registros de cortes y depósitos mediante la función `integra_empleados_rpt_news()`.
     * Si ocurre algún error durante la inicialización de los empleados, la función retorna un array con los detalles
     * del error en lugar de los empleados procesados.
     *
     * @param array $cortes Array que contiene los datos de cortes de los empleados. Cada corte es un objeto con datos
     * asociados a un empleado, como `ohem_id`, `ohem_nombre_completo`, `plaza_descripcion`, etc.
     * @param array $depositos Array que contiene los datos de depósitos de los empleados. Cada depósito es un objeto con datos
     * relacionados con un empleado, como `ohem_id`, `ohem_nombre_completo`, `plaza_descripcion`, etc.
     *
     * @return array Retorna un array `$empleados` donde cada clave es el `ohem_id` del empleado, y el valor es un objeto
     * que contiene los datos del empleado procesados a partir de los cortes y depósitos. Si ocurre un error, retorna
     * un array con los detalles del error.
     *
     * @see error Para más detalles sobre el formato del array de error retornado.
     */
    private function inicializa_empleados_rpt(array $cortes, array $depositos): array
    {
        $empleados = array();

        $empleados = $this->integra_empleados_rpt_news($empleados, $cortes);
        if(error::$en_error){
            return  (new error())->error('Error al inicializar rows',$empleados);
        }
        $empleados = $this->integra_empleados_rpt_news($empleados, $depositos);
        if(error::$en_error){
            return  (new error())->error('Error al inicializar rows',$empleados);
        }

        return $empleados;

    }

    /**
     * EM3
     * Inicializa una cuenta asociada a un empleado en el array de empleados.
     *
     * Este método verifica y asegura que una cuenta con el alias proporcionado (`$alias`) exista
     * para el empleado identificado por `$ohem_id` dentro del array `$empleados`.
     * Si la cuenta no existe, se inicializa con valores predeterminados.
     *
     * ### Validaciones
     * - Verifica que `$alias` no esté vacío y sea un texto.
     * - Asegura que `$ohem_id` sea un número mayor a 0.
     * - Garantiza que `$empleados[$ohem_id]` sea un objeto.
     *
     * ### Proceso
     * - Si `$ohem_id` no existe en `$empleados`, se inicializa como un nuevo objeto `stdClass`.
     * - Se agrega al objeto del empleado una nueva propiedad con el nombre `$alias`.
     * - Se inicializa la propiedad `deposito_aportaciones_monto_depositado` de `$alias` en 0.
     *
     * @param string $alias Alias de la cuenta a inicializar. Debe ser un texto no vacío.
     * @param array $empleados Array asociativo de empleados, donde cada clave (`$ohem_id`) es el ID del empleado y el valor es un objeto con los datos del empleado.
     * @param int $ohem_id ID del empleado para quien se inicializa la cuenta. Debe ser un número mayor a 0.
     *
     * @return array Devuelve el array `$empleados` actualizado con la cuenta inicializada.
     *               En caso de error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Inicializar cuenta para un empleado existente
     * ```php
     * $empleados = [
     *     123 => (object)[
     *         'nombre' => 'Juan Pérez'
     *     ]
     * ];
     * $alias = 'reporte_financiero';
     * $ohem_id = 123;
     *
     * $resultado = $this->init_cuenta_empleado($alias, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // $resultado[123]->reporte_financiero->deposito_aportaciones_monto_depositado === 0
     * ```
     *
     * @example Caso exitoso: Inicializar cuenta para un empleado nuevo
     * ```php
     * $empleados = [];
     * $alias = 'reporte_financiero';
     * $ohem_id = 456;
     *
     * $resultado = $this->init_cuenta_empleado($alias, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // $resultado[456]->reporte_financiero->deposito_aportaciones_monto_depositado === 0
     * ```
     *
     * @example Error: `$alias` vacío
     * ```php
     * $empleados = [];
     * $alias = '';
     * $ohem_id = 456;
     *
     * $resultado = $this->init_cuenta_empleado($alias, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $alias esta vacio',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: `$alias` es numérico
     * ```php
     * $empleados = [];
     * $alias = '123';
     * $ohem_id = 456;
     *
     * $resultado = $this->init_cuenta_empleado($alias, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $alias debe ser un texto',
     * //     'data' => '123'
     * // ]
     * ```
     *
     * @example Error: `$ohem_id` menor o igual a 0
     * ```php
     * $empleados = [];
     * $alias = 'reporte_financiero';
     * $ohem_id = -1;
     *
     * $resultado = $this->init_cuenta_empleado($alias, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $ohem_id debe ser mayor a 0',
     * //     'data' => -1
     * // ]
     * ```
     *
     * @example Error: `$empleados[$ohem_id]` no es un objeto
     * ```php
     * $empleados = [
     *     123 => 'No es un objeto'
     * ];
     * $alias = 'reporte_financiero';
     * $ohem_id = 123;
     *
     * $resultado = $this->init_cuenta_empleado($alias, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $empleados[$ohem_id] debe ser un objeto',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function init_cuenta_empleado(string $alias, array $empleados, int $ohem_id): array
    {
        $alias = trim($alias);
        if($alias === ''){
            return  (new error())->error('Error $alias esta vacio',$alias);
        }
        if(is_numeric($alias)){
            return  (new error())->error('Error $alias de ser un texto',$alias);
        }
        if($ohem_id <= 0){
            return  (new error())->error('Error $ohem_id dbe ser mayor a 0',$ohem_id);
        }
        if(!isset($empleados[$ohem_id])){
            $empleados[$ohem_id] = new stdClass();
        }
        if(!is_object($empleados[$ohem_id])){
            return  (new error())->error('Error $empleados[$ohem_id] debe ser un objeto',$empleados);
        }
        $empleados[$ohem_id]->$alias = new stdClass();
        $empleados[$ohem_id]->$alias->deposito_aportaciones_monto_depositado = 0;
        return $empleados;

    }


    /**
     * EM3
     * Inicializa una nueva cuenta para un empleado en un array de cuentas.
     *
     * Esta función verifica si el alias de la cuenta y el ID del empleado (`ohem_id`) son válidos,
     * y luego inicializa la cuenta dentro del array `$cuentas`. Si el empleado aún no tiene cuentas
     * registradas, se invoca la función `cuenta_nueva` para agregar la cuenta al conjunto de cuentas.
     *
     * ### Validaciones
     * - Verifica que `$cuenta_empresa_alias` no esté vacío después de ser limpiado con `trim()`.
     * - Verifica que `$ohem_id` sea mayor a 0.
     *
     * ### Proceso
     * - Se recorta cualquier espacio en blanco del alias de la cuenta.
     * - Se valida que el alias de la cuenta no sea una cadena vacía.
     * - Se verifica que `$ohem_id` sea mayor a 0.
     * - Si el empleado no tiene cuentas registradas (`!isset($cuentas[$ohem_id])`), se inicializa con `cuenta_nueva()`.
     * - Si ocurre un error en `cuenta_nueva()`, se captura y se devuelve un mensaje de error.
     *
     * @param string $cuenta_empresa_alias El alias de la cuenta que se desea inicializar.
     *                                     Debe ser una cadena no vacía.
     *
     * @param array $cuentas Un array asociativo donde las claves representan los IDs de empleados (`ohem_id`)
     *                       y los valores son arrays de cuentas asociadas a cada empleado.
     *
     * @param int $ohem_id El identificador único del empleado. Debe ser un número mayor a 0.
     *
     * @return array Devuelve el array `$cuentas` actualizado con la cuenta inicializada.
     *               Si `$cuenta_empresa_alias` está vacío o `$ohem_id` no es válido, devuelve un array con un mensaje de error.
     *
     * @throws error Si `$cuenta_empresa_alias` está vacío o `$ohem_id` es menor o igual a 0, se genera un error con un mensaje detallado.
     *
     * @example Caso exitoso: Inicializar una cuenta para un empleado
     * ```php
     * $cuentas = [
     *     123 => [
     *         'reporte_financiero' => new stdClass()
     *     ]
     * ];
     *
     * $cuenta_empresa_alias = 'cuenta_inversion';
     * $ohem_id = 123;
     *
     * $resultado = $this->init_cuenta_nueva($cuenta_empresa_alias, $cuentas, $ohem_id);
     *
     * // Resultado esperado:
     * // $resultado[123]['cuenta_inversion'] === stdClass()
     * ```
     *
     * @example Caso de error: `$cuenta_empresa_alias` está vacío
     * ```php
     * $cuentas = [
     *     123 => []
     * ];
     *
     * $cuenta_empresa_alias = '';
     * $ohem_id = 123;
     *
     * $resultado = $this->init_cuenta_nueva($cuenta_empresa_alias, $cuentas, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $cuenta_empresa_alias esta vacia',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Caso de error: `$ohem_id` es menor o igual a 0
     * ```php
     * $cuentas = [
     *     123 => []
     * ];
     *
     * $cuenta_empresa_alias = 'cuenta_inversion';
     * $ohem_id = 0;
     *
     * $resultado = $this->init_cuenta_nueva($cuenta_empresa_alias, $cuentas, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $ohem_id es menor a 0',
     * //     'data' => 0
     * // ]
     * ```
     */
    private function init_cuenta_nueva(string $cuenta_empresa_alias, array $cuentas, int $ohem_id): array
    {
        $cuenta_empresa_alias = trim($cuenta_empresa_alias);
        if ($cuenta_empresa_alias === '') {
            return (new error())->error('Error $cuenta_empresa_alias esta vacia', $cuenta_empresa_alias);
        }
        if ($ohem_id <= 0) {
            return (new error())->error('Error $ohem_id es menor a 0', $ohem_id);
        }

        if (!isset($cuentas[$ohem_id])) {
            $cuentas = $this->cuenta_nueva($cuenta_empresa_alias, $cuentas);
            if (error::$en_error) {
                return (new error())->error('Error al integrar cuenta', $cuentas);
            }
        }
        return $cuentas;
    }


    private function init_dato(stdClass $dato): stdClass
    {

        unset($dato->cuentas);
        unset($dato->cuentas_data);
        unset($dato->fecha_final);
        unset($dato->fecha_inicial);

        return $dato;

    }

    /**
     * TRASLADADO
     * Inicializa un objeto de empleado para el reporte basado en los datos proporcionados.
     *
     * Esta función verifica que los datos base proporcionados contengan las propiedades necesarias para
     * la creación de un objeto empleado, como el ID del empleado, el nombre completo, la descripción de la plaza y el ID de la plaza.
     * También inicializa propiedades como el número de cortes, depósitos, y montos relacionados.
     *
     * @param stdClass $base  Objeto que contiene los datos base del empleado (como `ohem_id`, `ohem_nombre_completo`, `plaza_descripcion`, y `plaza_id`).
     *
     * @return stdClass|array Devuelve un objeto empleado con los datos inicializados si la validación es exitosa. Si hay algún error, devuelve un array con los detalles del error.
     */
    private function init_empleado_rpt(stdClass $base)
    {
        $valida = $this->valida_rpt_base($base);
        if(error::$en_error){
            return  (new error())->error('Error al validar base',$valida);
        }
        $empleado = new stdClass();
        $empleado->ohem_id = $base->ohem_id;
        $empleado->ohem_nombre_completo = $base->ohem_nombre_completo;
        $empleado->plaza_descripcion = $base->plaza_descripcion;
        $empleado->plaza_id = $base->plaza_id;
        $empleado->n_cortes = 0;
        $empleado->n_depositos = 0;
        $empleado->pago_corte_monto_total = 0;
        $empleado->pago_corte_monto_depositado = 0;
        $empleado->pago_corte_monto_por_depositar = 0;

        $empleado->cortes = array();
        $empleado->depositos = array();

        return $empleado;

    }

    private function init_rpt_by_empleado(stdClass $empleado, array $keys_rpt): array
    {
        $empleado_rpt = array();
        foreach ($keys_rpt as $key_rpt) {
            $empleado_rpt = $this->integra_key_rpt($empleado, $empleado_rpt, $key_rpt);
            if (error::$en_error) {
                return (new error())->error('Error al integrar key', $empleado_rpt);
            }
        }
        return $empleado_rpt;

    }

    private function integra_contadores(array $empleados): array
    {
        foreach ($empleados as $ohem_id=>$empleado){

            foreach ($empleado->cortes as $corte){
                $empleados[$ohem_id]->n_cortes ++;
            }
            foreach ($empleado->depositos as $deposito){
                $empleados[$ohem_id]->n_depositos ++;
            }
        }
        return $empleados;

    }

    /**
     * TRASLADADO
     * Integra la información de una cuenta en el reporte del empleado.
     *
     * Esta función valida el alias de la cuenta y verifica si existe en el objeto `$empleado`.
     * Si no existe, inicializa la cuenta con un valor predeterminado de `0.0` y luego agrega
     * o actualiza esta información en el array `$empleado_rpt`.
     * Si el formato especificado es 'JSON', la función también formatea el resultado en formato JSON.
     *
     * @param string $cuenta_alias El alias de la cuenta que se va a integrar en el reporte del empleado.
     *                             Debe ser una cadena no vacía.
     * @param stdClass $empleado El objeto que representa al empleado y contiene información de las cuentas.
     * @param array $empleado_rpt El array donde se almacenan los datos del empleado para el reporte.
     * @param string $format El formato deseado para los datos del reporte. Si es 'JSON',
     *                       se formatea el reporte en formato JSON.
     *
     * @return array Retorna el array `$empleado_rpt` actualizado con los datos de la cuenta integrada.
     *               Si ocurre algún error, se retorna un array de error con los detalles.
     *
     * @throws error Si el alias de la cuenta está vacío o si hay problemas al integrar el formato.
     */
    private function integra_cuenta(
        string $cuenta_alias, stdClass $empleado, array $empleado_rpt, string $format): array
    {
        $cuenta_alias = trim($cuenta_alias);
        if($cuenta_alias === ''){
            return  (new error())->error('Error $cuenta_alias esta vacia',$cuenta_alias);
        }
        if(!isset($empleado->$cuenta_alias)){
            $empleado->$cuenta_alias = new stdClass();
        }
        if(!is_object($empleado->$cuenta_alias)){
            return  (new error())->error('Error $empleado->$cuenta_alias debe ser un objeto',$cuenta_alias);
        }
        if(!isset($empleado->$cuenta_alias->deposito_aportaciones_monto_depositado)){
            $empleado->$cuenta_alias->deposito_aportaciones_monto_depositado = 0.0;
        }
        $empleado_rpt[$cuenta_alias] = $empleado->$cuenta_alias->deposito_aportaciones_monto_depositado;

        if($format === 'JSON'){
            $empleado_rpt = $this->format_json($cuenta_alias,$empleado_rpt);
            if(error::$en_error){
                return  (new error())->error('Error ajustar array',$empleado_rpt);
            }
        }

        return $empleado_rpt;

    }

    /**
     * EM3
     * Integra una cuenta en el registro de un empleado dentro del array de empleados.
     *
     * Este método verifica si una cuenta identificada por `$alias` existe para el empleado especificado por `$ohem_id`.
     * Si la cuenta no existe, utiliza el método `init_cuenta_empleado` para inicializarla. Realiza validaciones
     * para garantizar la consistencia de los datos antes de proceder.
     *
     * ### Validaciones
     * - Verifica que `$alias` no esté vacío, sea un texto y no un número.
     * - Asegura que `$ohem_id` sea mayor a 0.
     * - Comprueba que `$empleados[$ohem_id]` sea un objeto válido.
     * - Si `$alias` no está definido dentro del registro del empleado, inicializa la cuenta con valores predeterminados.
     *
     * ### Proceso
     * - Si `$alias` no está definido para el empleado, se llama a `init_cuenta_empleado` para inicializarlo.
     * - Realiza validaciones adicionales para confirmar que `$empleados[$ohem_id]` es un objeto.
     *
     * @param string $alias Alias de la cuenta a integrar. Debe ser un texto no vacío.
     * @param array $empleados Array asociativo de empleados, donde cada clave (`$ohem_id`) es el ID del empleado y el valor es un objeto con los datos del empleado.
     * @param int $ohem_id ID del empleado para quien se integra la cuenta. Debe ser un número mayor a 0.
     *
     * @return array Devuelve el array `$empleados` actualizado con la cuenta integrada.
     *               En caso de error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Integrar cuenta para un empleado existente
     * ```php
     * $empleados = [
     *     123 => (object)[
     *         'nombre' => 'Juan Pérez'
     *     ]
     * ];
     * $alias = 'reporte_financiero';
     * $ohem_id = 123;
     *
     * $resultado = $this->integra_cuenta_empleado($alias, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // $resultado[123]->reporte_financiero->deposito_aportaciones_monto_depositado === 0
     * ```
     *
     * @example Caso exitoso: Inicializar e integrar cuenta para un empleado nuevo
     * ```php
     * $empleados = [];
     * $alias = 'reporte_financiero';
     * $ohem_id = 456;
     *
     * $resultado = $this->integra_cuenta_empleado($alias, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // $resultado[456]->reporte_financiero->deposito_aportaciones_monto_depositado === 0
     * ```
     *
     * @example Error: `$alias` vacío
     * ```php
     * $empleados = [];
     * $alias = '';
     * $ohem_id = 456;
     *
     * $resultado = $this->integra_cuenta_empleado($alias, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $alias esta vacio',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: `$alias` es numérico
     * ```php
     * $empleados = [];
     * $alias = '123';
     * $ohem_id = 456;
     *
     * $resultado = $this->integra_cuenta_empleado($alias, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $alias debe ser un texto',
     * //     'data' => '123'
     * // ]
     * ```
     *
     * @example Error: `$ohem_id` menor o igual a 0
     * ```php
     * $empleados = [];
     * $alias = 'reporte_financiero';
     * $ohem_id = -1;
     *
     * $resultado = $this->integra_cuenta_empleado($alias, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $ohem_id debe ser mayor a 0',
     * //     'data' => -1
     * // ]
     * ```
     *
     * @example Error: `$empleados[$ohem_id]` no es un objeto
     * ```php
     * $empleados = [
     *     123 => 'No es un objeto'
     * ];
     * $alias = 'reporte_financiero';
     * $ohem_id = 123;
     *
     * $resultado = $this->integra_cuenta_empleado($alias, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $empleados[$ohem_id] debe ser un objeto',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function integra_cuenta_empleado(string $alias, array $empleados, int $ohem_id): array
    {
        $alias = trim($alias);
        if($alias === ''){
            return  (new error())->error('Error $alias esta vacio',$alias);
        }
        if(is_numeric($alias)){
            return  (new error())->error('Error $alias de ser un texto',$alias);
        }
        if($ohem_id <= 0){
            return  (new error())->error('Error $ohem_id dbe ser mayor a 0',$ohem_id);
        }
        if(!isset($empleados[$ohem_id]->$alias)){
            $empleados = $this->init_cuenta_empleado($alias,$empleados,$ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al integrar cuenta',$empleados);
            }
        }
        if(!is_object($empleados[$ohem_id])){
            return  (new error())->error('Error $empleados[$ohem_id] debe ser un objeto',$empleados);
        }
        return $empleados;

    }

    /**
     * EM3
     * Integra un conjunto de cuentas en el registro de un empleado específico.
     *
     * Este método verifica y asegura que cada cuenta identificada en el array `$cuentas` esté correctamente configurada
     * para el empleado identificado por `$ohem_id`. Si alguna cuenta no está definida, utiliza el método
     * `integra_cuenta_empleado` para inicializarla.
     *
     * ### Validaciones
     * - Verifica que `$ohem_id` sea un número mayor a 0.
     * - Valida que cada alias en `$cuentas` sea un texto no vacío y no numérico.
     * - Asegura que `$empleados[$ohem_id]` sea un objeto. Si no está definido, lo inicializa como un objeto vacío.
     *
     * ### Proceso
     * - Recorre cada alias del array `$cuentas`.
     * - Integra la cuenta en el registro del empleado especificado por `$ohem_id`.
     * - Realiza validaciones para asegurar que `$empleados[$ohem_id]` sea un objeto válido.
     *
     * @param array $cuentas Array asociativo de cuentas, donde cada clave (`$alias`) es el identificador de la cuenta.
     * @param array $empleados Array asociativo de empleados, donde cada clave (`$ohem_id`) es el ID del empleado y el valor es un objeto con los datos del empleado.
     * @param int $ohem_id ID del empleado para quien se integran las cuentas. Debe ser un número mayor a 0.
     *
     * @return array Devuelve el array `$empleados` actualizado con las cuentas integradas para el empleado especificado.
     *               En caso de error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Integrar cuentas para un empleado existente
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => [],
     *     'estado_cuenta' => []
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'nombre' => 'Juan Pérez'
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->integra_cuenta_por_empleado($cuentas, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // $resultado[123]->reporte_financiero->deposito_aportaciones_monto_depositado === 0
     * // $resultado[123]->estado_cuenta->deposito_aportaciones_monto_depositado === 0
     * ```
     *
     * @example Caso exitoso: Inicializar cuentas para un empleado nuevo
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => [],
     *     'estado_cuenta' => []
     * ];
     * $empleados = [];
     * $ohem_id = 456;
     *
     * $resultado = $this->integra_cuenta_por_empleado($cuentas, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // $resultado[456]->reporte_financiero->deposito_aportaciones_monto_depositado === 0
     * // $resultado[456]->estado_cuenta->deposito_aportaciones_monto_depositado === 0
     * ```
     *
     * @example Error: `$ohem_id` menor o igual a 0
     * ```php
     * $cuentas = ['reporte_financiero' => []];
     * $empleados = [];
     * $ohem_id = -1;
     *
     * $resultado = $this->integra_cuenta_por_empleado($cuentas, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $ohem_id debe ser mayor a 0',
     * //     'data' => -1
     * // ]
     * ```
     *
     * @example Error: `$alias` vacío
     * ```php
     * $cuentas = ['' => []];
     * $empleados = [];
     * $ohem_id = 456;
     *
     * $resultado = $this->integra_cuenta_por_empleado($cuentas, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $alias esta vacio',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: `$alias` numérico
     * ```php
     * $cuentas = ['123' => []];
     * $empleados = [];
     * $ohem_id = 456;
     *
     * $resultado = $this->integra_cuenta_por_empleado($cuentas, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $alias debe ser un texto',
     * //     'data' => '123'
     * // ]
     * ```
     *
     * @example Error: `$empleados[$ohem_id]` no es un objeto
     * ```php
     * $cuentas = ['reporte_financiero' => []];
     * $empleados = [
     *     123 => 'No es un objeto'
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->integra_cuenta_por_empleado($cuentas, $empleados, $ohem_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $empleados[$ohem_id] debe ser un objeto',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function integra_cuenta_por_empleado(array $cuentas, array $empleados, int $ohem_id): array
    {
        if($ohem_id <= 0){
            return  (new error())->error('Error $ohem_id dbe ser mayor a 0',$ohem_id);
        }
        foreach ($cuentas as $alias=>$cuenta){
            $alias = trim($alias);
            if($alias === ''){
                return  (new error())->error('Error $alias esta vacio',$alias);
            }
            if(is_numeric($alias)){
                return  (new error())->error('Error $alias de ser un texto',$alias);
            }
            if(!isset($empleados[$ohem_id])){
                $empleados[$ohem_id] = new stdClass();
            }
            if(!is_object($empleados[$ohem_id])){
                return  (new error())->error('Error $empleados[$ohem_id] debe ser un objeto',$empleados);
            }

            $empleados = $this->integra_cuenta_empleado($alias,$empleados,$ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al integrar cuenta',$empleados);
            }
        }
        return $empleados;


    }

    /**
     * EM3
     * Integra un conjunto de cuentas en los registros de múltiples empleados.
     *
     * Este método recorre el array de empleados, validando cada uno de ellos y asegurándose de que las cuentas
     * definidas en `$cuentas` estén correctamente configuradas en cada registro. Si una cuenta no está configurada
     * para un empleado, utiliza el método `integra_cuenta_por_empleado` para inicializarla.
     *
     * ### Validaciones
     * - Verifica que el identificador del empleado (`$ohem_id`) sea mayor a 0.
     * - Asegura que cada empleado esté correctamente definido y que las cuentas sean integradas exitosamente.
     *
     * ### Proceso
     * - Recorre el array de empleados.
     * - Valida cada `$ohem_id` y configura las cuentas correspondientes utilizando el método `integra_cuenta_por_empleado`.
     * - Maneja errores en caso de datos inválidos o inconsistentes.
     *
     * @param array $cuentas Array asociativo de cuentas donde cada clave es un alias de cuenta.
     * @param array $empleados Array asociativo de empleados donde cada clave es el ID del empleado (`$ohem_id`).
     *                         Cada empleado debe ser un objeto que contendrá las cuentas integradas.
     *
     * @return array Devuelve el array `$empleados` actualizado con las cuentas integradas para cada empleado.
     *               En caso de error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Integrar cuentas en múltiples empleados
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => [],
     *     'estado_cuenta' => []
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'nombre' => 'Juan Pérez'
     *     ],
     *     456 => (object)[
     *         'nombre' => 'Ana López'
     *     ]
     * ];
     *
     * $resultado = $this->integra_cuentas($cuentas, $empleados);
     *
     * // Resultado esperado:
     * // $resultado[123]->reporte_financiero->deposito_aportaciones_monto_depositado === 0
     * // $resultado[123]->estado_cuenta->deposito_aportaciones_monto_depositado === 0
     * // $resultado[456]->reporte_financiero->deposito_aportaciones_monto_depositado === 0
     * // $resultado[456]->estado_cuenta->deposito_aportaciones_monto_depositado === 0
     * ```
     *
     * @example Error: `$ohem_id` no válido
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => []
     * ];
     * $empleados = [
     *     -1 => (object)[
     *         'nombre' => 'Empleado Inválido'
     *     ]
     * ];
     *
     * $resultado = $this->integra_cuentas($cuentas, $empleados);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $ohem_id debe ser mayor a 0',
     * //     'data' => -1
     * // ]
     * ```
     *
     * @example Error: Integración fallida en un empleado
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => []
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'nombre' => 'Empleado Inválido'
     *     ]
     * ];
     *
     * // Simulación: `integra_cuenta_por_empleado` retorna un error
     * $resultado = $this->integra_cuentas($cuentas, $empleados);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error al integrar cuenta',
     * //     'data' => [...]
     * // ]
     * ```
     *
     * @example Caso exitoso: Empleado con cuentas previamente integradas
     * ```php
     * $cuentas = [
     *     'reporte_financiero' => [],
     *     'estado_cuenta' => []
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'nombre' => 'Juan Pérez',
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000
     *         ]
     *     ]
     * ];
     *
     * $resultado = $this->integra_cuentas($cuentas, $empleados);
     *
     * // Resultado esperado:
     * // $resultado[123]->reporte_financiero->deposito_aportaciones_monto_depositado === 1000
     * // $resultado[123]->estado_cuenta->deposito_aportaciones_monto_depositado === 0
     * ```
     */
    private function integra_cuentas(array $cuentas, array $empleados): array
    {
        foreach ($empleados as $ohem_id=>$empleado){
            if($ohem_id <= 0){
                return  (new error())->error('Error $ohem_id dbe ser mayor a 0',$ohem_id);
            }
            $empleados = $this->integra_cuenta_por_empleado($cuentas,$empleados,$ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al integrar cuenta',$empleados);
            }
        }
        return $empleados;

    }


    /**
     * TRASLADADO
     * Integra la información de cuentas en el reporte del empleado.
     *
     * Esta función toma el alias de una cuenta y, si existe en el objeto `$empleado`,
     * agrega la información de la cuenta al array `$empleado_rpt`. También puede formatear
     * los datos en JSON si se especifica el formato adecuado.
     *
     * @param string $cuenta_alias El alias de la cuenta a integrar. Debe ser una cadena no vacía.
     * @param stdClass $empleado El objeto que representa al empleado, que contiene las cuentas.
     * @param array $empleado_rpt El array que almacena la información del reporte del empleado.
     * @param string $format El formato en el que se quiere devolver el resultado. Si es 'JSON',
     *                       se aplicará formato JSON a los datos de la cuenta.
     *
     * @return array Retorna el array `$empleado_rpt` actualizado con los datos de la cuenta integrada.
     *               En caso de error, devuelve un array con los detalles del error.
     *
     * @throws error Si el alias de la cuenta está vacío o si ocurre algún
     *                                     problema al integrar los datos de la cuenta.
     */
    private function integra_cuentas_empleado(
        string $cuenta_alias, stdClass $empleado, array $empleado_rpt, string $format): array
    {
        $cuenta_alias = trim($cuenta_alias);
        if($cuenta_alias === ''){
            return  (new error())->error('Error $cuenta_alias esta vacia',$cuenta_alias);
        }
        if(!isset($empleado_rpt[$cuenta_alias])) {
            $empleado_rpt[$cuenta_alias] = 0.0;
        }
        if(isset($empleado->$cuenta_alias)){
            if(!is_object($empleado->$cuenta_alias)){
                return  (new error())->error('Error $empleado->$cuenta_alias debe ser un objeto',$cuenta_alias);
            }
            $empleado_rpt = $this->integra_cuenta($cuenta_alias,$empleado,$empleado_rpt, $format);
            if(error::$en_error){
                return  (new error())->error('Error al integrar cuenta',$empleado_rpt);
            }
        }
        return $empleado_rpt;

    }

    private function integra_cuentas_reporte(array $cuentas, array $empleados): array
    {
        $empleados = $this->integra_cuentas($cuentas,$empleados);
        if(error::$en_error){
            return  (new error())->error('Error al integrar cuenta',$empleados);
        }

        $empleados = $this->acumula_cuentas_totales($cuentas,$empleados);
        if(error::$en_error){
            return  (new error())->error('Error al integrar cuenta',$empleados);
        }
        return $empleados;

    }

    private function integra_cuentas_rpt(array $empleados)
    {
        $cuentas = $this->cuentas($empleados);
        if(error::$en_error){
            return  (new error())->error('Error al integrar cuenta',$cuentas);
        }

        $empleados = $this->integra_cuentas_reporte($cuentas,$empleados);
        if(error::$en_error){
            return  (new error())->error('Error al integrar cuenta',$empleados);
        }

        $data = new stdClass();
        $data->cuentas = $cuentas;
        $data->empleados = $empleados;



        return $data;


    }

    /**
     * TRASLADADO
     * Integra un empleado en el reporte de empleados, validando los datos base y
     * creando la estructura del empleado antes de agregarlo al array de empleados.
     *
     * @param stdClass $base       El objeto que contiene la información base del empleado.
     * @param array    $empleados  El array de empleados donde se integrará el nuevo empleado.
     *
     * @return array   Devuelve el array actualizado de empleados con el nuevo empleado integrado.
     *                 Si ocurre un error, devuelve un array con los detalles del error.
     */
    private function integra_empleado_rpt(stdClass $base, array $empleados): array
    {
        $valida = $this->valida_rpt_base($base);
        if(error::$en_error){
            return  (new error())->error('Error al validar base',$valida);
        }

        $empleado = $this->init_empleado_rpt($base);
        if(error::$en_error){
            return  (new error())->error('Error al inicializar $empleado',$empleado);
        }
        $empleados[$base->ohem_id] = $empleado;

        return $empleados;

    }

    /**
     * TRASLADADO
     * Integra un empleado en el array de empleados si aún no existe, validando los datos base
     * e inicializando la estructura del empleado si es necesario.
     *
     * @param stdClass $base       El objeto que contiene la información base del empleado.
     * @param array    $empleados  El array de empleados donde se integrará el nuevo empleado si no existe.
     *
     * @return array   Devuelve el array actualizado de empleados con el nuevo empleado integrado si es necesario.
     *                 Si ocurre un error, devuelve un array con los detalles del error.
     */
    private function integra_empleado_rpt_new(stdClass $base, array $empleados): array
    {
        $valida = $this->valida_rpt_base($base);
        if(error::$en_error){
            return  (new error())->error('Error al validar base',$valida);
        }
        if(!isset($empleados[$base->ohem_id])) {
            $empleados = $this->integra_empleado_rpt($base, $empleados);
            if(error::$en_error){
                return  (new error())->error('Error al inicializar row',$empleados);
            }
        }
        return $empleados;

    }

    /**
     * TRASLADADO
     * Integra los registros de empleados dentro de un array de reporte.
     *
     * Esta función procesa una lista de registros (`$rows`), donde cada registro representa datos de un empleado.
     * Cada registro es validado y luego integrado dentro del array `$empleados`, donde la clave es el `ohem_id`
     * del empleado y el valor es un objeto con los detalles de dicho empleado.
     * Si algún registro no es un objeto o falla la validación de los datos, se retorna un array con los detalles del error.
     *
     * @param array $empleados Array que contiene los empleados ya procesados. La clave es el `ohem_id` del empleado
     * y el valor es un objeto con los datos del empleado, incluyendo sus cortes y depósitos.
     * @param array $rows Array de registros que deben ser procesados. Cada registro es un objeto que contiene
     * los datos de un empleado, como `ohem_id`, `ohem_nombre_completo`, `plaza_descripcion`, etc.
     *
     * @return array Retorna el array `$empleados` actualizado con los empleados procesados de `$rows`. Si ocurre un error,
     * retorna un array con los detalles del error (incluyendo el mensaje y el contexto).
     *
     * @see error Para más detalles sobre el formato del array de error retornado.
     */
    private function integra_empleados_rpt_news(array $empleados, array $rows): array
    {
        foreach ($rows as $base) {
            if(!is_object($base)){
                return  (new error())->error('Error $base no es un objeto',$base);
            }
            $valida = $this->valida_rpt_base($base);
            if(error::$en_error){
                return  (new error())->error('Error al validar base',$valida);
            }
            $empleados = $this->integra_empleado_rpt_new($base, $empleados);
            if(error::$en_error){
                return  (new error())->error('Error al inicializar row',$empleados);
            }
        }
        return $empleados;

    }

    /**
     * TRASLADADO
     * Integra una clave específica del empleado en el reporte de empleado.
     *
     * Esta función toma una clave específica de un objeto empleado y la agrega al array del reporte de empleado.
     * Si la clave o el valor no existen, se devuelve un error. La clave se utiliza para identificar qué dato del empleado
     * debe ser integrado en el reporte.
     *
     * @param stdClass $empleado El objeto empleado del cual se extrae la clave.
     * @param array $empleado_rpt El array del reporte de empleado donde se integrará la clave.
     * @param string $key_integra La clave específica que se integrará al reporte.
     *
     * @return array Devuelve el array del reporte de empleado con la clave integrada.
     * @throws error Si la clave está vacía o no existe en el objeto empleado.
     */
    private function integra_key_rpt(stdClass $empleado, array $empleado_rpt, string $key_integra): array
    {
        $key_integra = trim($key_integra);
        if($key_integra === ''){
            return  (new error())->error('Error $key_integra esta vacio',$key_integra);
        }
        if(!isset($empleado->$key_integra)){
            return  (new error())->error('Error $empleado->$key_integra no existe',$empleado);
        }
        $empleado_rpt[$key_integra] = $empleado->$key_integra;
        return $empleado_rpt;

    }

    /**
     * TRASLADADO
     * Integra una fila de datos en el reporte de un empleado.
     *
     * Esta función toma un array de empleados, busca un empleado específico basado en su `ohem_id`,
     * y agrega una fila de datos representada por `$row` en la propiedad correspondiente al nombre de la variable
     * (`$name_var_row`). La propiedad `$name_var_row` es tratada como un array en el objeto del empleado, donde
     * se agregan nuevas filas de datos.
     *
     * Si el `ohem_id` es inválido o si el nombre de la variable de la fila es inválido, o si el empleado no existe en
     * el array, la función devuelve un array con los detalles del error.
     *
     * @param array $empleados Array de empleados donde la clave es el `ohem_id` del empleado, y el valor es un objeto
     * con los datos del empleado. Este array será modificado para integrar la nueva fila de datos.
     * @param string $name_var_row El nombre de la variable que representa la propiedad en el objeto del empleado donde
     * se almacenará la nueva fila de datos. Debe ser una cadena no vacía.
     * @param int $ohem_id El ID del empleado. Debe ser mayor a 0.
     * @param stdClass $row La fila de datos que se va a agregar al reporte del empleado. Esta fila se integra en la propiedad
     * especificada por `$name_var_row`.
     *
     * @return array Retorna el array `$empleados` actualizado con la nueva fila de datos en el empleado correspondiente.
     * Si ocurre un error (por ejemplo, si el `ohem_id` es inválido, o el empleado no existe), retorna un array con los
     * detalles del error.
     *
     * @throws error Si algún dato es inválido, lanza un error detallando la causa.
     */
    private function integra_row_rpt(array $empleados, string $name_var_row, int $ohem_id, stdClass $row): array
    {
        if($ohem_id <= 0){
            return  (new error())->error('Error $ohem_id debe ser mayor a 0',$ohem_id);
        }
        $name_var_row = trim($name_var_row);
        if($name_var_row === ''){
            return  (new error())->error('Error $name_var_row esta vacio',$name_var_row);
        }
        if(!isset($empleados[$ohem_id])){
            return  (new error())->error('$empleados[$ohem_id] no existe',$empleados);
        }
        if(!is_object($empleados[$ohem_id])){
            return  (new error())->error('$empleados[$ohem_id] debe ser un objeto',$empleados);
        }
        $empleados[$ohem_id]->$name_var_row[] = $row;
        return $empleados;

    }

    /**
     * TRASLADADO
     * Integra una fila de datos en el reporte de empleados, verificando que cumpla con las condiciones necesarias.
     *
     * La función valida la existencia y el tipo de los datos relacionados con un empleado específico (`ohem_id`),
     * y los integra en el arreglo de empleados bajo la variable indicada.
     *
     * @param array $empleados Arreglo de empleados donde cada índice es el `ohem_id` y el valor es un objeto con datos del empleado.
     * @param string $name_var_row Nombre de la variable donde se almacenará la fila dentro del objeto del empleado.
     * @param int $ohem_id ID del empleado, debe ser mayor a 0.
     * @param stdClass $row Objeto que contiene los datos de la fila a integrar. Debe contener el campo `ohem_id`.
     *
     * @return array Devuelve el arreglo `$empleados` actualizado con la fila integrada.
     *               En caso de error, devuelve un arreglo con información detallada del fallo.
     *
     * Ejemplo de uso:
     * ```php
     * $empleados = [
     *     123 => (object)[
     *         'nombre' => 'Juan Pérez',
     *         'rows' => []
     *     ]
     * ];
     *
     * $name_var_row = 'rows';
     * $ohem_id = 123;
     * $row = (object)[
     *     'ohem_id' => 123,
     *     'detalle' => 'Pago de nómina'
     * ];
     *
     * $empleados_actualizado = $this->integra_row_rpt_new($empleados, $name_var_row, $ohem_id, $row);
     *
     * print_r($empleados_actualizado);
     * ```
     *
     * Salida esperada:
     * ```php
     * Array
     * (
     *     [123] => stdClass Object
     *         (
     *             [nombre] => Juan Pérez
     *             [rows] => Array
     *                 (
     *                     [0] => stdClass Object
     *                         (
     *                             [ohem_id] => 123
     *                             [detalle] => Pago de nómina
     *                         )
     *
     *                 )
     *         )
     * )
     * ```
     *
     * Proceso:
     * 1. Valida que `$ohem_id` sea mayor a 0.
     * 2. Valida que `$name_var_row` no esté vacío.
     * 3. Asegura que el objeto `$row` contenga el campo `ohem_id` y que coincida con `$ohem_id`.
     * 4. Verifica que el índice `$ohem_id` exista en `$empleados` y que su valor sea un objeto.
     * 5. Llama a la función `integra_row_rpt` para integrar la fila al reporte del empleado.
     * 6. Devuelve el arreglo `$empleados` actualizado o un error si ocurre algún fallo.
     *
     * Errores posibles:
     * - `['error' => 'Error $ohem_id debe ser mayor a 0', 'detalle' => $ohem_id]` si `$ohem_id` es inválido.
     * - `['error' => 'Error $name_var_row esta vacio', 'detalle' => $name_var_row]` si `$name_var_row` está vacío.
     * - `['error' => '$empleados[$ohem_id] no existe', 'detalle' => $empleados]` si no existe el índice `$ohem_id` en `$empleados`.
     * - `['error' => '$empleados[$ohem_id] debe ser un objeto', 'detalle' => $empleados]` si el valor asociado a `$ohem_id` no es un objeto.
     * - `['error' => 'Error al integrar row', 'detalle' => $empleados]` si falla la integración de la fila.
     */
    private function integra_row_rpt_new(array $empleados, string $name_var_row, int $ohem_id, stdClass $row): array
    {
        if($ohem_id <= 0){
            return  (new error())->error('Error $ohem_id debe ser mayor a 0',$ohem_id);
        }
        $name_var_row = trim($name_var_row);
        if($name_var_row === ''){
            return  (new error())->error('Error $name_var_row esta vacio',$name_var_row);
        }

        if(!isset($row->ohem_id)){
            $row->ohem_id = -1;
        }
        if((int)$row->ohem_id === $ohem_id){
            if(!isset($empleados[$ohem_id])){
                return  (new error())->error('$empleados[$ohem_id] no existe',$empleados);
            }
            if(!is_object($empleados[$ohem_id])){
                return  (new error())->error('$empleados[$ohem_id] debe ser un objeto',$empleados);
            }

            $empleados = $this->integra_row_rpt($empleados,$name_var_row,$ohem_id,$row);
            if(error::$en_error){
                return  (new error())->error('Error al integrar row',$empleados);
            }
        }
        return $empleados;

    }

    private function integra_rows_empleados_rpt(stdClass $datos, array $empleados): array
    {
        foreach ($empleados as $ohem_id=>$empleado){

            $empleados = $this->integra_rows_new($datos,$empleados,'cortes',$ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al integrar row',$empleados);
            }
            $empleados = $this->integra_rows_new($datos,$empleados,'depositos',$ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al integrar row',$empleados);
            }

        }
        return $empleados;

    }

    /**
     * TRASLADADO
     * Integra múltiples filas de datos en un arreglo de empleados.
     *
     * La función valida y procesa todas las filas de una propiedad específica dentro de `$datos`, integrándolas en
     * el arreglo `$empleados` para el empleado identificado por `$ohem_id`.
     *
     * @param stdClass $datos Objeto que contiene los datos a procesar, incluyendo la propiedad especificada por `$name_var_row`.
     * @param array $empleados Arreglo de empleados donde cada índice es el `ohem_id` y el valor es un objeto con los datos del empleado.
     * @param string $name_var_row Nombre de la propiedad en `$datos` que contiene las filas a integrar.
     * @param int $ohem_id ID del empleado al que se deben asociar las filas, debe ser mayor a 0.
     *
     * @return array Devuelve el arreglo `$empleados` actualizado con las filas integradas.
     *               En caso de error, devuelve un arreglo con información detallada del fallo.
     *
     * Ejemplo de uso:
     * ```php
     * $datos = (object)[
     *     'rows' => [
     *         (object)['ohem_id' => 123, 'detalle' => 'Pago de nómina 1'],
     *         (object)['ohem_id' => 123, 'detalle' => 'Pago de nómina 2']
     *     ]
     * ];
     *
     * $empleados = [
     *     123 => (object)[
     *         'nombre' => 'Juan Pérez',
     *         'rows' => []
     *     ]
     * ];
     *
     * $name_var_row = 'rows';
     * $ohem_id = 123;
     *
     * $empleados_actualizado = $this->integra_rows_new($datos, $empleados, $name_var_row, $ohem_id);
     *
     * print_r($empleados_actualizado);
     * ```
     *
     * Salida esperada:
     * ```php
     * Array
     * (
     *     [123] => stdClass Object
     *         (
     *             [nombre] => Juan Pérez
     *             [rows] => Array
     *                 (
     *                     [0] => stdClass Object
     *                         (
     *                             [ohem_id] => 123
     *                             [detalle] => Pago de nómina 1
     *                         )
     *                     [1] => stdClass Object
     *                         (
     *                             [ohem_id] => 123
     *                             [detalle] => Pago de nómina 2
     *                         )
     *                 )
     *         )
     * )
     * ```
     *
     * Proceso:
     * 1. Valida que `$name_var_row` no esté vacío.
     * 2. Verifica que `$datos` contenga la propiedad `$name_var_row`.
     * 3. Itera sobre las filas en `$datos->$name_var_row` y valida que cada fila sea un objeto.
     * 4. Para cada fila, llama a `integra_row_rpt_new` para integrarla en el arreglo de empleados.
     * 5. Devuelve el arreglo `$empleados` actualizado o un error si ocurre algún fallo.
     *
     * Errores posibles:
     * - `['error' => 'Error $name_var_row esta vacio', 'detalle' => $name_var_row]` si `$name_var_row` está vacío.
     * - `['error' => 'Error $datos->name_var_row no existe', 'detalle' => $datos]` si `$name_var_row` no existe en `$datos`.
     * - `['error' => 'Error $row debe ser un objeto', 'detalle' => $empleados]` si alguna fila no es un objeto.
     * - `['error' => 'Error $ohem_id debe ser mayor a 0', 'detalle' => $ohem_id]` si `$ohem_id` es inválido.
     * - `['error' => 'Error al integrar row', 'detalle' => $empleados]` si falla la integración de una fila.
     */
    private function integra_rows_new(stdClass $datos, array $empleados, string $name_var_row, int $ohem_id): array
    {
        $name_var_row = trim($name_var_row);
        if($name_var_row === ''){
            return  (new error())->error('Error $name_var_row esta vacio ',$name_var_row);
        }
        if(!isset($datos->$name_var_row)){
            return  (new error())->error('Error $datos->name_var_row no existe '.$name_var_row,$datos);
        }

        foreach ($datos->$name_var_row as $row) {
            if(!is_object($row)){
                return  (new error())->error('Error $row debe ser un objeto',$empleados);
            }
            if($ohem_id <= 0){
                return  (new error())->error('Error $ohem_id debe ser mayor a 0',$ohem_id);
            }
            $empleados = $this->integra_row_rpt_new($empleados,$name_var_row,$ohem_id,$row);
            if(error::$en_error){
                return  (new error())->error('Error al integrar row',$empleados);
            }
        }
        return $empleados;

    }

    /**
     * TRASLADADO
     * Integra todas las cuentas del empleado en el reporte.
     *
     * Esta función toma las cuentas contenidas en el objeto `$datos`, recorre cada una de ellas
     * e integra la información correspondiente en el array `$empleado_rpt`. Se encarga de validar
     * y agregar cada cuenta, aplicando un formato específico si es necesario.
     *
     * @param stdClass $datos El objeto que contiene las cuentas a integrar. Si no existe la propiedad `cuentas`,
     *                        se inicializa como un array vacío.
     * @param stdClass $empleado El objeto que representa al empleado, que contiene la información de las cuentas.
     * @param array $empleado_rpt El array que almacena el reporte del empleado, al cual se integrarán las cuentas.
     * @param string $format El formato en el que se quiere devolver el resultado. Si es 'JSON',
     *                       las cuentas se integrarán en ese formato.
     *
     * @return array Devuelve el array `$empleado_rpt` actualizado con la información de todas las cuentas integradas.
     *               En caso de error, devuelve un array con los detalles del error.
     *
     * @throws error Si ocurre algún problema durante la integración de las cuentas.
     */
    private function integra_total_cuentas(
        stdClass $datos, stdClass $empleado, array $empleado_rpt, string $format): array
    {
        if(!isset($datos->cuentas)){
            $datos->cuentas = array();
        }
        foreach ($datos->cuentas as $cuenta_alias=>$cuenta){
            $empleado_rpt = $this->integra_cuentas_empleado($cuenta_alias,$empleado,$empleado_rpt, $format);
            if(error::$en_error){
                return  (new error())->error('Error al integrar cuenta',$empleado_rpt);
            }
        }
        return $empleado_rpt;

    }

    private function integra_value_moneda(string $campo, array $campos_moneda, array $dato,  $val): array
    {
        if(in_array($campo,$campos_moneda)){
            $dato = $this->val_moneda($campo,$dato,$val);
            if (error::$en_error) {
                return (new error())->error('Error al integrar formato moneda', $val);
            }
        }
        return $dato;

    }

    /**
     * TRASLADADO
     * Genera y devuelve las claves principales para el reporte de empleados.
     *
     * Esta función define un array con las claves esenciales que se utilizarán en el reporte de empleados.
     * Las claves incluyen información sobre el empleado (ID, nombre), la plaza, el número de cortes,
     * número de depósitos y montos relacionados con los pagos y depósitos.
     *
     * @return array Devuelve un array con las claves principales que serán utilizadas en el reporte de empleados.
     */
    private function key_rpt(): array
    {
        $keys_rpt[] = 'ohem_id';
        $keys_rpt[] = 'ohem_nombre_completo';
        $keys_rpt[] = 'plaza_id';
        $keys_rpt[] = 'plaza_descripcion';
        $keys_rpt[] = 'n_cortes';
        $keys_rpt[] = 'n_depositos';
        $keys_rpt[] = 'pago_corte_monto_total';
        $keys_rpt[] = 'pago_corte_monto_por_depositar';
        $keys_rpt[] = 'pago_corte_monto_depositado';

        return $keys_rpt;

    }

    private function plazas_rpt(array $datos): array
    {
        $plazas = array();
        foreach ($datos as $dato){
            $dato = (array)$dato;
            if(!isset($plazas[$dato['plaza_descripcion']])){
                $plazas[$dato['plaza_descripcion']] = $dato['plaza_descripcion'];
            }
        }
        return $plazas;
    }

    final public function plazas_rpt_out(array $datos)
    {
        $plazas = $this->plazas_rpt($datos);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $plazas', $plazas);
        }

        $plazas_string = $this->plazas_rpt_string($plazas);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $plazas_string', $plazas_string);
        }
        return $plazas_string;

    }

    private function plazas_rpt_string(array $plazas): string
    {
        $plazas_string = '';
        foreach ($plazas as $plaza){
            $plazas_string.= "$plaza ";
        }

        return $plazas_string;

    }

    final public function totales(array $datos): array
    {
        $totales = array();
        $n_cortes = 0;
        $n_depositos = 0;
        $pago_corte_monto_total = 0;
        $pago_corte_monto_por_depositar = 0;
        $pago_corte_monto_depositado	 = 0;

        foreach ($datos as $ind=>$dato){
            $n_cortes += $dato->n_cortes;
            $n_depositos += $dato->n_depositos;
            $pago_corte_monto_total += $dato->pago_corte_monto_total;
            $pago_corte_monto_por_depositar += $dato->pago_corte_monto_por_depositar;
            $pago_corte_monto_depositado += $dato->pago_corte_monto_depositado;

            $totales['n_cortes'] = $n_cortes;
            $totales['n_depositos'] = $n_depositos;
            $totales['pago_corte_monto_total'] = (new formato())->moneda((float)$pago_corte_monto_total);
            $totales['pago_corte_monto_por_depositar'] = (new formato())->moneda((float)$pago_corte_monto_por_depositar);
            $totales['pago_corte_monto_depositado'] = (new formato())->moneda((float)$pago_corte_monto_depositado);

        }

        return $totales;

    }

    private function val_moneda(string $campo, array $dato,  $val): array
    {
        $val = (new formato())->moneda((float)$val);
        if (error::$en_error) {
            return (new error())->error('Error al integrar formato moneda', $val);
        }
        $dato[$campo] = $val;
        return $dato;

    }



    /**
     * EM3
     * Valida la existencia e integridad de un dato específico en un reporte de empleados.
     *
     * Este método verifica que un alias de reporte, un objeto de depósito, y un listado de empleados sean válidos
     * y contengan los datos necesarios para procesar la lógica requerida. También se asegura de que los valores sean numéricos
     * cuando corresponda.
     *
     * @param string $alias El alias del dato dentro del reporte de empleados. No debe estar vacío.
     * @param stdClass $deposito Objeto que contiene los datos del depósito a validar.
     *                           Debe incluir la propiedad `deposito_aportaciones_monto_depositado`.
     * @param array $empleados Array asociativo de empleados, donde la clave es el ID del empleado (`ohem_id`)
     *                         y el valor es un objeto que contiene los datos del reporte.
     * @param int $ohem_id ID del empleado. Debe ser mayor o igual a 0.
     *
     * @return bool|array Devuelve `true` si todas las validaciones son exitosas. En caso de error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Dato válido
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[
     *     'deposito_aportaciones_monto_depositado' => 1000.50
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.50
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->valida_dato_reporte($alias, $deposito, $empleados, $ohem_id);
     * // Resultado: true
     * ```
     *
     * @example Caso de error: Alias vacío
     * ```php
     * $alias = '';
     * $deposito = (object)[
     *     'deposito_aportaciones_monto_depositado' => 1000.50
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.50
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->valida_dato_reporte($alias, $deposito, $empleados, $ohem_id);
     * // Resultado:
     * // [
     * //     'error' => 'Error $alias esta vacio',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Caso de error: `ohem_id` menor a 0
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[
     *     'deposito_aportaciones_monto_depositado' => 1000.50
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.50
     *         ]
     *     ]
     * ];
     * $ohem_id = -1;
     *
     * $resultado = $this->valida_dato_reporte($alias, $deposito, $empleados, $ohem_id);
     * // Resultado:
     * // [
     * //     'error' => 'Error $ohem_id es menor a 0',
     * //     'data' => -1
     * // ]
     * ```
     *
     * @example Caso de error: Propiedad faltante en el objeto de empleado
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[
     *     'deposito_aportaciones_monto_depositado' => 1000.50
     * ];
     * $empleados = [
     *     123 => (object)[]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->valida_dato_reporte($alias, $deposito, $empleados, $ohem_id);
     * // Resultado:
     * // [
     * //     'error' => '$empleados[123]->reporte_financiero no existe',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function valida_dato_reporte(string $alias, stdClass $deposito, array $empleados, int $ohem_id)
    {
        $alias = trim($alias);
        if($alias === ''){
            return  (new error())->error('Error $alias esta vacio',$alias);
        }
        if($ohem_id < 0){
            return  (new error())->error('Error $ohem_id es menor a 0',$ohem_id);
        }
        if(!isset($empleados[$ohem_id])){
            return  (new error())->error('$empleados['.$ohem_id.'] no existe',$empleados);
        }
        if(!is_object($empleados[$ohem_id])){
            return  (new error())->error('$empleados['.$ohem_id.'] debe ser un objeto',$empleados);
        }
        if(!isset($empleados[$ohem_id]->$alias)){
            return  (new error())->error('$empleados['.$ohem_id.']->'.$alias.' no existe',$empleados);
        }
        if(!isset($empleados[$ohem_id]->$alias->deposito_aportaciones_monto_depositado)){
            return  (new error())->error(
                '$empleados['.$ohem_id.']->'.$alias.'->deposito_aportaciones_monto_depositado no existe',
                $empleados);
        }
        if(!isset($deposito->deposito_aportaciones_monto_depositado)){
            return  (new error())->error(
                '$deposito->deposito_aportaciones_monto_depositado no existe',$deposito);
        }
        if(!is_numeric($empleados[$ohem_id]->$alias->deposito_aportaciones_monto_depositado)){
            return  (new error())->error(
                '$empleados['.$ohem_id.']->'.$alias.
                '->deposito_aportaciones_monto_depositado debe se run numero',$empleados);
        }

        return true;

    }

    /**
     * TRASLADADO
     * Valida que el nombre de la entidad de empleado y las fechas proporcionadas no estén vacíos.
     *
     * La función verifica que el nombre de la entidad de empleado, así como las fechas inicial y final,
     * no estén vacíos. Si alguno de estos valores está vacío, se devuelve un error con los detalles correspondientes.
     * Si todos los valores son válidos, la función devuelve `true`.
     *
     * @param string $entidad_empleado El nombre de la entidad de empleado.
     * @param string $fecha_final La fecha final del rango de búsqueda.
     * @param string $fecha_inicial La fecha inicial del rango de búsqueda.
     *
     * @return bool|array Devuelve `true` si todos los valores son válidos.
     *                    Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    final public function valida_fechas(string $entidad_empleado, string $fecha_final, string $fecha_inicial)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }
        $fecha_final = trim($fecha_final);
        if($fecha_final === ''){
            return (new error())->error('Error $fecha_final esta vacia', $fecha_final);
        }
        $fecha_inicial = trim($fecha_inicial);
        if($fecha_inicial === ''){
            return (new error())->error('Error $fecha_inicial esta vacia', $fecha_inicial);
        }

        return true;

    }

    /**
     * TRASLADADO
     * Valida los datos base de un reporte para asegurarse de que contengan los campos esenciales
     * y que estos campos tengan valores válidos (no vacíos y numéricos donde se requiere).
     *
     * @param stdClass $base  Objeto que contiene los datos base del reporte, incluyendo propiedades como
     *                        `ohem_id`, `ohem_nombre_completo`, `plaza_descripcion`, y `plaza_id`.
     *
     * @return bool|array Devuelve `true` si todos los campos son válidos. Si alguna validación falla,
     *                    devuelve un array con los detalles del error.
     */
    private function valida_rpt_base(stdClass $base)
    {
        if(!isset($base->ohem_id)){
            return  (new error())->error('$base->ohem_id no existe',$base);
        }
        if(!isset($base->ohem_nombre_completo)){
            return  (new error())->error('$base->ohem_nombre_completo no existe',$base);
        }
        if(!isset($base->plaza_descripcion)){
            return  (new error())->error('$base->plaza_descripcion no existe',$base);
        }
        if(!isset($base->plaza_id)){
            return  (new error())->error('$base->plaza_id no existe',$base);
        }

        if(trim($base->ohem_id) === ''){
            return  (new error())->error('$base->ohem_id esta vacia',$base);
        }
        if(trim($base->ohem_nombre_completo) === ''){
            return  (new error())->error('$base->ohem_nombre_completo esta vacia',$base);
        }
        if(trim($base->plaza_descripcion) === ''){
            return  (new error())->error('$base->plaza_descripcion esta vacia',$base);
        }
        if(trim($base->plaza_id) === ''){
            return  (new error())->error('$base->plaza_id esta vacia',$base);
        }
        if(!is_numeric($base->ohem_id)){
            return  (new error())->error('$base->ohem_id debe ser un numero',$base);
        }
        if(!is_numeric($base->plaza_id)){
            return  (new error())->error('$base->plaza_id debe ser un numero',$base);
        }
        if((int)$base->ohem_id <= 0){
            return  (new error())->error('$base->ohem_id debe ser mayor s 0',$base);
        }
        if((int)$base->plaza_id <= 0){
            return  (new error())->error('$base->plaza_id debe ser mayor s 0',$base);
        }

        return true;

    }

    /**
     * EM3
     * Valida los datos necesarios para realizar la suma de un depósito en un reporte de empleados.
     *
     * Este método verifica que el objeto `$deposito` contenga las propiedades requeridas y que estas sean válidas.
     * También valida los datos asociados al reporte del empleado (`$empleados`) utilizando el método `valida_dato_reporte`.
     *
     * @param string $alias El alias dentro del reporte de empleados donde se realizará la validación.
     *                      Debe ser una cadena no vacía.
     * @param stdClass $deposito Objeto que contiene los datos del depósito. Debe incluir las propiedades:
     *                           - `cuenta_empresa_alias` (cadena no vacía).
     * @param array $empleados Array asociativo de empleados, donde la clave es el ID del empleado (`ohem_id`)
     *                         y el valor es un objeto que contiene los datos del reporte.
     * @param int $ohem_id ID del empleado cuyo reporte será validado. Debe ser mayor o igual a 0.
     *
     * @return bool|array Devuelve `true` si los datos son válidos. Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Validación correcta
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[
     *     'cuenta_empresa_alias' => 'alias_empresa',
     *     'deposito_aportaciones_monto_depositado' => 500.25
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->valida_sum_deposito($alias, $deposito, $empleados, $ohem_id);
     * // Resultado esperado:
     * // true
     * ```
     *
     * @example Caso de error: Propiedad `cuenta_empresa_alias` no existe
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[
     *     'deposito_aportaciones_monto_depositado' => 500.25
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->valida_sum_deposito($alias, $deposito, $empleados, $ohem_id);
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $deposito->cuenta_empresa_alias no existe',
     * //     'data' => $deposito
     * // ]
     * ```
     *
     * @example Caso de error: `cuenta_empresa_alias` está vacía
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[
     *     'cuenta_empresa_alias' => '',
     *     'deposito_aportaciones_monto_depositado' => 500.25
     * ];
     * $empleados = [
     *     123 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->valida_sum_deposito($alias, $deposito, $empleados, $ohem_id);
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $deposito->cuenta_empresa_alias esta vacio',
     * //     'data' => $deposito
     * // ]
     * ```
     *
     * @example Caso de error: Datos del reporte inválidos
     * ```php
     * $alias = 'reporte_financiero';
     * $deposito = (object)[
     *     'cuenta_empresa_alias' => 'alias_empresa',
     *     'deposito_aportaciones_monto_depositado' => 500.25
     * ];
     * $empleados = [
     *     456 => (object)[
     *         'reporte_financiero' => (object)[
     *             'deposito_aportaciones_monto_depositado' => 1000.00
     *         ]
     *     ]
     * ];
     * $ohem_id = 123;
     *
     * $resultado = $this->valida_sum_deposito($alias, $deposito, $empleados, $ohem_id);
     * // Resultado esperado:
     * // [
     * //     'error' => '$empleados[123] no existe',
     * //     'data' => $empleados
     * // ]
     * ```
     */

    private function valida_sum_deposito(string $alias, stdClass $deposito, array $empleados, int $ohem_id)
    {
        if(!isset($deposito->cuenta_empresa_alias)){
            return  (new error())->error('Error $deposito->cuenta_empresa_alias no existe',$deposito);
        }
        $deposito->cuenta_empresa_alias = trim($deposito->cuenta_empresa_alias);
        if($deposito->cuenta_empresa_alias === ''){
            return  (new error())->error('Error $deposito->cuenta_empresa_alias esta vacio',$deposito);
        }
        $valida = $this->valida_dato_reporte($alias,$deposito,$empleados,$ohem_id);
        if(error::$en_error){
            return  (new error())->error('Error al validar datos',$valida);
        }
        return true;

    }

}
