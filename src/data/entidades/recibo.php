<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;

class recibo
{
    final public function filtro_recibo(int $empleado_id, string $key_empleado_id, int $periodo_pago_id,
                                        int $tipo_recibo_id): array
    {
        $key_empleado_id = trim($key_empleado_id);
        if($key_empleado_id === ''){
            return (new error())->error('Error $key_empleado_id esta vacio',$key_empleado_id);
        }
        if($periodo_pago_id <= 0){
            return (new error())->error('Error $periodo_pago_id debe ser mayor a 0',$periodo_pago_id);
        }
        if($tipo_recibo_id <= 0){
            return (new error())->error('Error $tipo_recibo_id debe ser mayor a 0',$tipo_recibo_id);
        }
        if($empleado_id <= 0){
            return (new error())->error('Error $empleado_id debe ser mayor a 0',$empleado_id);
        }
        return array($key_empleado_id=>$empleado_id, 'periodo_pago.id'=>$periodo_pago_id,
            'tipo_recibo.id'=>$tipo_recibo_id);
    }


}
