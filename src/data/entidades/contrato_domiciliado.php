<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use DateInterval;
use DateTime;
use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use PDO;
use stdClass;
use Throwable;

class contrato_domiciliado
{

    final public function contrato_domiciliado(int $contrato_id, PDO $link)
    {
        $sql_con_dom = "SELECT *FROM contrato_domiciliado WHERE contrato_domiciliado.contrato_id= $contrato_id AND status = 'activo' AND contrato_domiciliado.number_token <>''";

        $rs_cont_doms = (new consultas())->exe_objs($link,$sql_con_dom);
        if(error::$en_error){
            return (new error())->error('Error al obtener $rs_cont_doms.',$rs_cont_doms);
        }

        if(count($rs_cont_doms) === 0){
            return (new error())->error('Error no existe contrato en domiciliazacion',$contrato_id);
        }

        if(count($rs_cont_doms) > 1){
            return (new error())->error('Error existe mas de un  contrato en domiciliazacion',$contrato_id);
        }


        $cont_dom = $rs_cont_doms[0];
        if(!isset($cont_dom->number_token)){
            $cont_dom->number_token = '';
        }
        $cont_dom->number_token = trim($cont_dom->number_token);

        return $cont_dom;

    }


    final public function contrato_por_domiciliar(int $contrato_id, PDO $link)
    {
        $contrato = (new consultas())->registro_bruto(new stdClass(), 'contrato',$contrato_id,$link,false);
        if(error::$en_error){
            return (new error())->error('Error al obtener $contrato.',$contrato);
        }

        $contrato->DocTotal = round($contrato->DocTotal,2);
        $contrato->PaidToDate = round($contrato->PaidToDate,2);

        if($contrato->DocTotal - $contrato->PaidToDate <= 0.00){
            return (new error())->error('Error el contrato no tiene saldo.',$contrato);
        }

        $es_domiciliado = (new \desarrollo_em3\manejo_datos\data\entidades\contrato_domiciliado())->es_domiciliado(
            $contrato_id, $link);
        if(error::$en_error){
            return (new error())->error('Error al obtener $es_domiciliado.',$es_domiciliado);
        }
        if(!$es_domiciliado){
            return (new error())->error('Error este contrato no aplica para domicilizacion',$contrato);
        }

        return $contrato;

    }

    final public function dia_semana_es($fecha): stdClass
    {
        $domingo = new stdClass();
        $domingo->number_mex = 7;
        $domingo->number = 0;
        $domingo->esp = 'Domingo';
        $domingo->cobrable = false;

        $lunes = new stdClass();
        $lunes->number_mex = 1;
        $lunes->number = 1;
        $lunes->esp = 'Lunes';
        $lunes->cobrable = true;

        $martes = new stdClass();
        $martes->number_mex = 2;
        $martes->number = 2;
        $martes->esp = 'Martes';
        $martes->cobrable = true;

        $miercoles = new stdClass();
        $miercoles->number_mex = 3;
        $miercoles->number = 3;
        $miercoles->esp = 'Miercoles';
        $miercoles->cobrable = true;

        $jueves = new stdClass();
        $jueves->number_mex = 4;
        $jueves->number = 4;
        $jueves->esp = 'Jueves';
        $jueves->cobrable = true;

        $viernes = new stdClass();
        $viernes->number_mex = 5;
        $viernes->number = 5;
        $viernes->esp = 'Viernes';
        $viernes->cobrable = true;

        $sabado = new stdClass();
        $sabado->number_mex = 6;
        $sabado->number = 6;
        $sabado->esp = 'Sabado';
        $sabado->cobrable = false;

        $dias = [
            'Sunday' => $domingo,
            'Monday' => $lunes,
            'Tuesday' => $martes,
            'Wednesday' => $miercoles,
            'Thursday' => $jueves,
            'Friday' => $viernes,
            'Saturday' => $sabado
        ];

        $nombre_dia = date("l", strtotime($fecha));
        return $dias[$nombre_dia];
    }

    final public function dias_pago(stdClass $contrato): stdClass
    {
        $dia_pago = '';
        if(isset($contrato->dia_pago)){
            $dia_pago = trim($contrato->dia_pago);
        }
        $dia_pago_1 = '';
        if(isset($contrato->dia_pago_1)){
            $dia_pago_1 = trim($contrato->dia_pago_1);
        }

        $dia_pago_2 = '';
        if(isset($contrato->dia_pago_2)){
            $dia_pago_2 = trim($contrato->dia_pago_2);
        }

        $dias = new stdClass();
        $dias->dia_pago = $dia_pago;
        $dias->dia_pago_1 = $dia_pago_1;
        $dias->dia_pago_2 = $dia_pago_2;

        return $dias;

    }

    private function es_domiciliado(int $contrato_id, PDO $link)
    {
        $sql = (new \desarrollo_em3\manejo_datos\sql\contrato_domiciliado())->es_domiciliado($contrato_id,'n_rows');
        if(error::$en_error){
            return (new error())->error('Error al obtener sql',$sql);
        }
        $rs = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al ejecutar sql',$rs);
        }

        $es_dom = false;
        if((int)$rs[0]->n_rows > 0){
            $es_dom = true;
        }
        return $es_dom;

    }

    final public function number_token(stdClass $cont_dom)
    {
        $number_token = '';
        if(isset($cont_dom->number_token)){
            $number_token = trim($cont_dom->number_token);
        }


        if($number_token === ''){
            return  (new error())->error('Error hace falta el token',$cont_dom->contrato_id);
        }

        return $number_token;

    }

    final public function tiene_dia_pago_asignado(stdClass $dias): bool
    {
        $tiene_dia_pago_asignado = false;
        if($dias->dia_pago !== ''){
            $tiene_dia_pago_asignado = true;
        }
        if($dias->dia_pago_1 !== ''){
            $tiene_dia_pago_asignado = true;
        }

        if($dias->dia_pago_2 !== ''){
            $tiene_dia_pago_asignado = true;
        }

        return $tiene_dia_pago_asignado;
    }

    final public function ultimo_dia_mes()
    {
        $hoy = date('Y-m-d');
        try {
            $date = new DateTime($hoy);
            $ultimo_dia_mes = $date->format('Y-m-t');
        }
        catch (Throwable $e){
            return  (new error())->error('Error al asignar fecha',$e);
        }

        $dia_semana_ultimo_dia_mes = $this->dia_semana_es($ultimo_dia_mes);

        if(!$dia_semana_ultimo_dia_mes->cobrable){

            try {
                $date = new DateTime($ultimo_dia_mes);
                $date->sub(new DateInterval('P1D'));
            }
            catch (Throwable $e){
                return  (new error())->error('Error al asignar fecha',$e);
            }
            $ultimo_dia_mes = $date->format('Y-m-d');

            $dia_semana_ultimo_dia_mes = $this->dia_semana_es($ultimo_dia_mes);
            if(!$dia_semana_ultimo_dia_mes->cobrable){
                try {
                    $date = new DateTime($ultimo_dia_mes);
                    $date->sub(new DateInterval('P1D'));
                    $ultimo_dia_mes = $date->format('Y-m-d');
                }
                catch (Throwable $e){
                    return  (new error())->error('Error al asignar fecha',$e);
                }
            }

        }
        return $ultimo_dia_mes;

    }


}
