<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use stdClass;

class oficina_trabajo
{

    private string $key_base_id = 'oficina_trabajo_id';

    /**
     * TRASLADADO
     * Obtiene los datos de una oficina de trabajo por su ID.
     *
     * Esta función genera una consulta SQL para obtener los detalles de una oficina de trabajo basada en su ID,
     * ejecuta la consulta utilizando una conexión PDO y devuelve el resultado en formato de array. Si el ID
     * proporcionado es menor o igual a 0, o si ocurre algún error durante la ejecución de la consulta, devuelve
     * un array con el error correspondiente.
     *
     * @param PDO $link La conexión PDO a la base de datos.
     * @param int $oficina_trabajo_id El ID de la oficina de trabajo que se desea consultar.
     *
     * @return array Un array con los resultados de la consulta o un array con el error en caso de fallo.
     *
     * @throws error Si ocurre un error en la validación del ID o durante la ejecución de la consulta.
     */
    final public function get_oficina_trabajo_by_id(PDO $link, int $oficina_trabajo_id): array {
        if($oficina_trabajo_id <= 0) {
            return (new error())->error('Error $oficina_trabajo_id es menor a 0',$oficina_trabajo_id);
        }

        $sql = (new sql\oficina_trabajo())->get_oficina_trabajo_by_id($oficina_trabajo_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }

        return $exe;
    }
}
