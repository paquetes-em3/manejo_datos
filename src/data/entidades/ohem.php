<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\data\_datos;
use PDO;


class ohem
{
    private string $name_entidad = 'ohem';

    final public function campos(PDO $link)
    {
        $campos = (new _datos())->campos_entidad($this->name_entidad, $link);
        if(error::$en_error){
            return (new error())->error('Error al obtener campos',$campos);
        }
        return $campos;
    }

    final public function row_estado_civil(array $registro): array
    {
        if(isset($registro['estado_civil'])){
            if($registro['estado_civil'] === 'soltero'){
                $registro['martStatus'] = 'S';
            }
            if($registro['estado_civil'] === 'casado'){
                $registro['martStatus'] = 'M';
            }
            if($registro['estado_civil'] === 'divorciado'){
                $registro['martStatus'] = 'D';
            }
            if($registro['estado_civil'] === 'viudo'){
                $registro['martStatus'] = 'W';
            }
        }
        return $registro;

    }

    final public function row_estatus(array $registro): array
    {
        if(isset($registro['status'])){
            if($registro['status'] === 'activo'){
                $registro['Active'] = 'Y';
            }
            if($registro['status'] === 'inactivo'){
                $registro['Active'] = 'N';
            }
        }
        return $registro;

    }

    final public function row_sexo(array $registro): array
    {
        if(isset($registro['sexo'])){ // SEXO
            if($registro['sexo'] === 'H'){
                $registro['sex'] = 'M';
            }
            if($registro['sexo'] === 'M'){
                $registro['sex'] = 'F';
            }
        }
        return $registro;
    }


}
