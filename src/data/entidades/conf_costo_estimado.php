<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use DateTime;
use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use PDO;
use stdClass;
use Throwable;

/**
 * Clase conf_costo_estimado
 *
 * Esta clase se encarga de manejar la configuración de costos estimados para contratos y pagos,
 * realizando cálculos avanzados como la integración del costo estimado sin IVA y generando registros
 * con datos relevantes para reportes y análisis.
 *
 * @package desarrollo_em3\manejo_datos\data\entidades
 */
class conf_costo_estimado
{


    /**
     * EM3
     * Calcula el costo estimado sin IVA basado en la configuración del costo y los datos del contrato y pago.
     *
     * Esta función toma como entrada la configuración de costo estimado, los datos del contrato y pago,
     * y el porcentaje de IVA aplicado. Luego, realiza los cálculos necesarios para determinar el costo
     * sin IVA, asegurando que los valores sean válidos antes de procesarlos.
     *
     * @param stdClass $conf_costo_estimado Objeto con la configuración del costo estimado.
     *        - Propiedad esperada: `costo` (float) Monto estimado con IVA.
     *        - Ejemplo válido: `$conf_costo_estimado->costo = 1000.00`
     *        - Ejemplo inválido: `$conf_costo_estimado->costo = null` (se inicializa en 0)
     * @param stdClass $datos Objeto que contiene los detalles del contrato y pago.
     *        - `contrato` (stdClass) Detalles del contrato.
     *            - Propiedad esperada: `DocTotal` (float) Precio total del contrato con IVA.
     *            - Ejemplo válido: `$datos->contrato->DocTotal = 1200.00`
     *            - Ejemplo inválido: `$datos->contrato = null` (se inicializa como objeto vacío)
     *        - `pago` (stdClass) Detalles del pago.
     *            - Propiedad esperada: `DocTotal` (float) Monto total del pago.
     *            - Ejemplo válido: `$datos->pago->DocTotal = 600.00`
     *            - Ejemplo inválido: `$datos->pago = null` (se inicializa como objeto vacío)
     * @param float $porcentaje_iva Porcentaje de IVA aplicado (ejemplo: 0.16 para 16%). Debe ser mayor o igual a 0.
     *        - Ejemplo válido: `0.16` (16%)
     *        - Ejemplo inválido: `-0.05` (retornará un error)
     *
     * @return float|array Retorna el costo estimado sin IVA como un valor flotante. En caso de error, devuelve un objeto de error.
     *
     * @throws error Lanza un error si:
     *         - `$porcentaje_iva` es menor a 0.
     *         - Si la función `costo_sin_iva` retorna un error.
     *
     * Ejemplo de uso:
     * ```php
     * $conf_costo_estimado = new stdClass();
     * $conf_costo_estimado->costo = 1000.00;
     *
     * $datos = new stdClass();
     * $datos->contrato = new stdClass();
     * $datos->contrato->DocTotal = 1200.00;
     * $datos->pago = new stdClass();
     * $datos->pago->DocTotal = 600.00;
     *
     * $porcentaje_iva = 0.16;
     *
     * $resultado = $this->costo_estimado($conf_costo_estimado, $datos, $porcentaje_iva);
     *
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al calcular el costo estimado.";
     * } else {
     *     echo "Costo estimado sin IVA: " . $resultado;
     * }
     * ```
     */
    private function costo_estimado(stdClass $conf_costo_estimado, stdClass $datos, float $porcentaje_iva)
    {
        // Inicializa valores por defecto si no están definidos
        if (!isset($conf_costo_estimado->costo)) {
            $conf_costo_estimado->costo = 0;
        }
        if (!isset($datos->contrato)) {
            $datos->contrato = new stdClass();
        }
        if (!isset($datos->pago)) {
            $datos->pago = new stdClass();
        }
        if (!isset($datos->pago->DocTotal)) {
            $datos->pago->DocTotal = 0;
        }
        if (!isset($datos->contrato->DocTotal)) {
            $datos->contrato->DocTotal = 0;
        }

        // Validación del porcentaje de IVA
        if ($porcentaje_iva < 0.0) {
            return (new error())->error('Error: $porcentaje_iva es menor a 0', $porcentaje_iva);
        }

        // Obtiene el precio total del contrato con IVA
        $precio = round($datos->contrato->DocTotal, 2);

        // Inicializa el costo sin IVA
        $costo_sin_iva = 0.0;

        // Calcula el costo sin IVA si las condiciones son válidas
        if ($conf_costo_estimado->costo > 0.0 && $precio > 0.0) {
            $costo_sin_iva = $this->costo_sin_iva(
                $conf_costo_estimado->costo,
                $porcentaje_iva,
                $precio,
                $datos->pago->DocTotal
            );

            // Verifica si ocurrió un error durante el cálculo
            if (error::$en_error) {
                return (new error())->error('Error al obtener costo estimado', $costo_sin_iva);
            }
        }

        return $costo_sin_iva;
    }



    /**
     * EM3
     * Calcula el costo estimado sin IVA basado en diversos parámetros financieros.
     *
     * Esta función realiza el cálculo del costo estimado sin IVA a partir del costo estimado con IVA,
     * el porcentaje de IVA, el precio del contrato y el monto total del pago. Se encarga de validar los valores de entrada
     * y realizar los cálculos necesarios para obtener un costo sin IVA preciso.
     *
     * @param float $costo_estimado Monto estimado del costo con IVA. Debe ser mayor o igual a 0.
     *        - Ejemplo válido: `1000.00`
     *        - Ejemplo inválido: `-500.00` (retornará un error)
     * @param float $porcentaje_iva Porcentaje de IVA aplicado (expresado como decimal, por ejemplo, 0.16 para 16%). Debe ser mayor o igual a 0.
     *        - Ejemplo válido: `0.16` (para 16%)
     *        - Ejemplo inválido: `-0.05` (retornará un error)
     * @param float $precio Precio total del contrato con IVA incluido. Debe ser mayor que 0.
     *        - Ejemplo válido: `1200.00`
     *        - Ejemplo inválido: `0.00` (retornará un error)
     * @param float $total_pago Monto total del pago realizado. Debe ser mayor o igual a 0.
     *        - Ejemplo válido: `600.00`
     *        - Ejemplo inválido: `-100.00` (retornará un error)
     *
     * @return float|array Retorna el costo estimado sin IVA como un valor flotante. En caso de error, devuelve un objeto de error.
     *
     * @throws error Lanza un error si:
     *         - `$porcentaje_iva` es menor a 0.
     *         - `$precio` es menor o igual a 0.
     *         - `$total_pago` es menor a 0.
     *         - `$costo_estimado` es menor a 0.
     *
     * Ejemplo de uso:
     * ```php
     * $costo_estimado = 1000.00;
     * $porcentaje_iva = 0.16;
     * $precio = 1200.00;
     * $total_pago = 600.00;
     *
     * $resultado = $this->costo_sin_iva($costo_estimado, $porcentaje_iva, $precio, $total_pago);
     *
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al calcular el costo sin IVA.";
     * } else {
     *     echo "Costo estimado sin IVA: " . $resultado;
     * }
     * ```
     */
    private function costo_sin_iva(
        float $costo_estimado, float $porcentaje_iva, float $precio, float $total_pago
    ) {
        // Validaciones de entrada
        if ($porcentaje_iva < 0.0) {
            return (new error())->error('Error: $porcentaje_iva es menor a 0', $porcentaje_iva);
        }
        if ($precio <= 0.0) {
            return (new error())->error('Error: $precio es menor o igual a 0', $precio);
        }
        if ($total_pago < 0.0) {
            return (new error())->error('Error: $total_pago es menor a 0', $total_pago);
        }
        if ($costo_estimado < 0.0) {
            return (new error())->error('Error: $costo_estimado es menor a 0', $costo_estimado);
        }

        // Cálculo del factor de IVA
        $factor_iva = 1 + $porcentaje_iva;

        // Se obtiene el precio sin IVA
        $precio_sin_iva = round($precio / $factor_iva, 4);

        // Se calcula la relación entre el costo estimado y el precio sin IVA
        $precio_entre_costo = round($costo_estimado / $precio_sin_iva, 4);

        // Se obtiene el costo con IVA ajustado según el pago total
        $costo_con_iva = round($precio_entre_costo * $total_pago, 2);

        // Se devuelve el costo estimado sin IVA
        return round($costo_con_iva / $factor_iva, 2);
    }

    /**
     * EM3
     * Obtiene los datos de un pago y su contrato asociado desde la base de datos.
     *
     * Esta función consulta la base de datos para recuperar la información de un pago específico y
     * el contrato al que está asociado. Devuelve un objeto con ambos registros si la consulta es exitosa.
     *
     * @param PDO $link Conexión activa a la base de datos.
     *        - Debe ser una instancia de `PDO` válida.
     * @param int $pago_id ID del pago a consultar.
     *        - Debe ser mayor a 0.
     *        - Ejemplo válido: `123`
     *        - Ejemplo inválido: `-5` (retorna un error)
     *
     * @return stdClass|array Retorna un objeto con la información del pago y su contrato asociado.
     *                        En caso de error, devuelve un objeto de error.
     *
     * @throws error Retorna un error si:
     *         - `$pago_id` es menor o igual a 0.
     *         - No se encuentra el pago en la base de datos.
     *         - No se encuentra el contrato asociado al pago.
     *
     * Ejemplo de uso:
     * ```php
     * $pago_id = 123;
     * $resultado = $this->datos($link, $pago_id);
     *
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al obtener los datos del pago.";
     * } else {
     *     echo "Pago ID: " . $resultado->pago->id;
     *     echo "Contrato ID: " . $resultado->contrato->id;
     * }
     * ```
     */
    private function datos(PDO $link, int $pago_id)
    {
        // Valida que el ID del pago sea mayor a 0
        if ($pago_id <= 0) {
            return (new error())->error('Error: $pago_id es menor a 0', $pago_id);
        }

        // Obtiene el registro del pago desde la base de datos
        $pago = (new consultas())->registro_bruto(new stdClass(), 'pago', $pago_id, $link, false);

        // Verifica si ocurrió un error al obtener el pago
        if (error::$en_error) {
            return (new error())->error('Error al obtener el pago', $pago);
        }

        // Obtiene el contrato asociado al pago
        $contrato = (new consultas())->registro_bruto(new stdClass(), 'contrato', $pago->contrato_id,
            $link, false);

        // Verifica si ocurrió un error al obtener el contrato
        if (error::$en_error) {
            return (new error())->error('Error al obtener el contrato', $contrato);
        }

        // Crea un objeto con los datos obtenidos
        $data = new stdClass();
        $data->pago = $pago;
        $data->contrato = $contrato;

        return $data;
    }

    /**
     * EM3
     * Desglosa una fecha en componentes de año y mes.
     *
     * Esta función toma una fecha en formato de cadena (`yyyy-mm-dd`) y devuelve un objeto que contiene
     * la fecha original, el año y el mes extraídos. Si la fecha está vacía o el formato no es válido,
     * retorna un objeto de error.
     *
     * @param string $pago_fecha Fecha en formato `yyyy-mm-dd`. No debe estar vacía ni en un formato incorrecto.
     *        - Ejemplo válido: `"2024-03-05"`
     *        - Ejemplo inválido: `""` (retornará un error)
     *        - Ejemplo inválido: `"05/03/2024"` (retornará un error)
     *
     * @return stdClass|array Devuelve un objeto con los siguientes atributos:
     *                  - `pago_fecha` (string): La fecha original.
     *                  - `year` (string): El año extraído de la fecha.
     *                  - `mes` (string): El mes extraído de la fecha.
     *                  En caso de error, devuelve un objeto `error` con información del fallo.
     *
     * @throws error Retorna un error si:
     *         - `$pago_fecha` está vacía.
     *         - No se puede convertir `$pago_fecha` en un objeto `DateTime`.
     *
     * Ejemplo de uso:
     * ```php
     * $pago_fecha = '2024-03-05';
     * $resultado = $this->fecha_desglosada($pago_fecha);
     *
     * if (isset($resultado->error)) {
     *     echo $resultado->error; // Muestra el mensaje de error si ocurre un fallo.
     * } else {
     *     echo "Año: " . $resultado->year . ", Mes: " . $resultado->mes"; // Año: 2024, Mes: 03
 * }
     * ```
     */
    private function fecha_desglosada(string $pago_fecha)
    {
        // Elimina espacios en blanco alrededor de la fecha
        $pago_fecha = trim($pago_fecha);

        // Verifica si la fecha está vacía
        if ($pago_fecha === '') {
            return (new error())->error('Error: $pago_fecha está vacía', $pago_fecha);
        }

        try {
            // Convierte la fecha en un objeto DateTime
            $fecha_obj = new DateTime($pago_fecha);
            $year = $fecha_obj->format('Y'); // Extrae el año
            $mes = $fecha_obj->format('m');  // Extrae el mes
        } catch (Throwable $e) {
            // Captura errores en la conversión de fecha
            return (new error())->error('Error al procesar la fecha del pago', $e);
        }

        // Crea un objeto para almacenar los datos desglosados
        $data = new stdClass();
        $data->pago_fecha = $pago_fecha;
        $data->year = $year;
        $data->mes = $mes;

        return $data;
    }

    /**
     * EM3
     * Obtiene el ID de la configuración de costo estimado asociada a un pago.
     *
     * Esta función valida que el `pago_id` sea mayor a 0 y luego obtiene los datos del pago y su contrato asociado.
     * Si el pago está activo y no ha sido cancelado, se procede a buscar la configuración de costo estimado
     * en función de los datos del contrato y la fecha del pago.
     *
     * @param PDO $link Conexión activa a la base de datos.
     *        - Debe ser una instancia de `PDO` válida.
     * @param int $pago_id ID del pago para buscar la configuración de costo estimado.
     *        - Debe ser un número entero mayor a 0.
     *        - Ejemplo válido: `123`
     *        - Ejemplo inválido: `-5` (retornará un error)
     *
     * @return int|array Retorna el ID de la configuración de costo estimado si se encuentra,
     *                   de lo contrario, devuelve `-1`. En caso de error, devuelve un objeto `error`.
     *
     * @throws error Retorna un error si:
     *         - `$pago_id` es menor o igual a 0.
     *         - No se pueden obtener los datos del pago.
     *         - Ocurre un error al obtener el ID de configuración.
     *
     * Ejemplo de uso:
     * ```php
     * $pago_id = 123;
     * $conf_id = $this->get_conf_id($link, $pago_id);
     *
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al obtener el ID de la configuración de costo estimado.";
     * } else {
     *     echo "ID de configuración obtenida: " . $conf_id;
     * }
     * ```
     */
    final public function get_conf_id(PDO $link, int $pago_id)
    {
        // Valida que el ID del pago sea mayor a 0
        if ($pago_id <= 0) {
            return (new error())->error('Error: $pago_id es menor a 0', $pago_id);
        }

        // Obtiene los datos del pago y su contrato asociado
        $datos = $this->datos($link, $pago_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener datos del pago', $datos);
        }

        $conf_id = -1;

        // Verifica que el pago está activo y no ha sido cancelado
        if ($datos->pago->status === 'activo' && $datos->pago->Canceled === 'N') {
            // Verifica que el contrato tenga un ID de empresa válido
            if (isset($datos->contrato->empresa_id) && $datos->contrato->empresa_id > 0) {
                // Obtiene el ID de la configuración de costo estimado
                $conf_id = $this->id($datos->contrato, $link, $datos->pago);
                if (error::$en_error) {
                    return (new error())->error('Error al obtener el ID de configuración', $conf_id);
                }
            }
        }

        return (int)$conf_id;
    }

    /**
     * EM3
     * Obtiene el ID de la configuración de costo estimado basada en los datos del contrato y la fecha de pago.
     *
     * Este método verifica la existencia de `empresa_id`, `producto_id` y `plaza_id` en el contrato, así como
     * la fecha de pago (`DocDate`). Luego, genera una consulta SQL para buscar la configuración de costo estimado
     * correspondiente y retorna su ID si se encuentra.
     *
     * @param stdClass $contrato Objeto que contiene la información del contrato, incluyendo:
     *        - `empresa_id` (int): ID de la empresa asociada al contrato.
     *        - `producto_id` (int): ID del producto relacionado al contrato.
     *        - `plaza_id` (int): ID de la plaza asociada al contrato.
     *        - Ejemplo válido:
     *          ```php
     *          $contrato = new stdClass();
     *          $contrato->empresa_id = 5;
     *          $contrato->producto_id = 10;
     *          $contrato->plaza_id = 3;
     *          ```
     *
     * @param PDO $link Conexión activa a la base de datos.
     *        - Debe ser una instancia de `PDO` válida.
     *
     * @param stdClass $pago Objeto que contiene la información del pago, incluyendo:
     *        - `DocDate` (string): Fecha del pago en formato `YYYY-MM-DD`.
     *        - Ejemplo válido:
     *          ```php
     *          $pago = new stdClass();
     *          $pago->DocDate = '2024-03-15';
     *          ```
     *
     * @return int|array Retorna el ID de la configuración de costo estimado si se encuentra,
     *                   de lo contrario, devuelve `-1`. En caso de error, retorna un objeto `error`.
     *
     * @throws error Retorna un error si:
     *         - `producto_id` no está definido en el contrato.
     *         - `DocDate` no está definido o está vacío en el pago.
     *         - Ocurre un error al ejecutar la consulta SQL.
     *
     * Ejemplo de uso:
     * ```php
     * $contrato = new stdClass();
     * $contrato->empresa_id = 5;
     * $contrato->producto_id = 10;
     * $contrato->plaza_id = 3;
     *
     * $pago = new stdClass();
     * $pago->DocDate = '2024-03-15';
     *
     * $conf_id = $this->id($contrato, $link, $pago);
     *
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al obtener el ID de la configuración de costo estimado.";
     * } else {
     *     echo "ID de configuración obtenida: " . $conf_id;
     * }
     * ```
     */
    private function id(stdClass $contrato, PDO $link, stdClass $pago)
    {
        $conf_id = -1;

        // Verifica que el contrato tenga un ID de empresa definido
        if (isset($contrato->empresa_id)) {

            // Verifica que el contrato tenga un ID de producto definido
            if (!isset($contrato->producto_id)) {
                return $conf_id;
            }

            // Verifica que el pago tenga una fecha definida
            if (!isset($pago->DocDate)) {
                return $conf_id;
            }
            if (trim($pago->DocDate) === '') {
                return $conf_id;
            }

            // Genera la consulta SQL para obtener la configuración de costo estimado
            $sql = (new \desarrollo_em3\manejo_datos\sql\conf_costo_estimado())->get_conf(
                $contrato->empresa_id, $pago->DocDate, $contrato->plaza_id, $contrato->producto_id
            );

            // Verifica si ocurrió un error en la generación de la consulta SQL
            if (error::$en_error) {
                return (new error())->error('Error al obtener conf sql', $sql);
            }

            // Ejecuta la consulta y obtiene el resultado
            $rs = (new consultas())->exe_objs($link, $sql);

            // Verifica si ocurrió un error al ejecutar la consulta
            if (error::$en_error) {
                return (new error())->error('Error al obtener conf', $rs);
            }

            // Si se encontraron registros, obtiene el ID de la configuración
            if (count($rs) > 0) {
                $conf_id = (int)$rs[0]->id;
            }
        }

        return $conf_id;
    }

    /**
     * EM3
     * Integra el costo estimado en un registro.
     *
     * Esta función obtiene la configuración del costo estimado desde la base de datos
     * si el `conf_costo_estimado_id` es válido, calcula el costo estimado sin IVA
     * y lo integra en el registro proporcionado.
     *
     * @param int $conf_costo_estimado_id ID de la configuración de costo estimado.
     *        - Si es mayor a 0, se buscará en la base de datos.
     *        - Ejemplo válido: `5`
     *        - Ejemplo inválido: `-1` (se omite la consulta)
     * @param stdClass $datos Objeto que contiene información del contrato y pago.
     *        - `contrato` (stdClass) Información del contrato.
     *        - `pago` (stdClass) Información del pago.
     *        - Ejemplo válido:
     *          ```php
     *          $datos = new stdClass();
     *          $datos->contrato = new stdClass();
     *          $datos->contrato->DocTotal = 1200.00;
     *          $datos->pago = new stdClass();
     *          $datos->pago->DocTotal = 600.00;
     *          ```
     * @param PDO $link Conexión a la base de datos.
     *        - Debe ser una conexión PDO activa.
     * @param float $porcentaje_iva Porcentaje del IVA a aplicar (ejemplo: 0.16 para 16%).
     *        - Debe ser mayor o igual a 0.
     *        - Ejemplo válido: `0.16`
     *        - Ejemplo inválido: `-0.05` (retornará un error)
     * @param array $registro Registro donde se integrará el costo estimado.
     *        - Debe ser un array asociativo con los datos del registro.
     *        - Ejemplo válido:
     *          ```php
     *          $registro = ['id' => 1];
     *          ```
     *
     * @return array Devuelve el registro actualizado con el costo estimado integrado.
     *               En caso de error, devuelve un objeto `error`.
     *
     * @throws error Retorna un error si:
     *         - `$porcentaje_iva` es menor a 0.
     *         - No se puede obtener la configuración del costo estimado.
     *         - Falla el cálculo del costo estimado.
     *
     * Ejemplo de uso:
     * ```php
     * $conf_costo_estimado_id = 5;
     * $datos = new stdClass();
     * $datos->contrato = new stdClass();
     * $datos->contrato->DocTotal = 1200.00;
     * $datos->pago = new stdClass();
     * $datos->pago->DocTotal = 600.00;
     *
     * $registro = ['id' => 1];
     * $porcentaje_iva = 0.16;
     *
     * $resultado = $this->integra_costo($conf_costo_estimado_id, $datos, $link, $porcentaje_iva, $registro);
     *
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al integrar el costo.";
     * } else {
     *     print_r($resultado); // Registro actualizado con 'costo' integrado.
     * }
     * ```
     */
    private function integra_costo(
        int $conf_costo_estimado_id,
        stdClass $datos,
        PDO $link,
        float $porcentaje_iva,
        array $registro
    ): array {

        // Validación del porcentaje de IVA
        if ($porcentaje_iva < 0.0) {
            return (new error())->error('Error: $porcentaje_iva es menor a 0', $porcentaje_iva);
        }

        // Inicializa el costo en 0
        $costo = 0.0;

        // Verifica si existe una configuración válida de costo estimado
        if ($conf_costo_estimado_id > 0) {
            // Obtiene la configuración del costo estimado desde la base de datos
            $conf_costo_estimado = (new consultas())->registro_bruto(
                new stdClass(),
                'conf_costo_estimado',
                $conf_costo_estimado_id,
                $link,
                false
            );

            // Verifica si ocurrió un error al obtener la configuración
            if (error::$en_error) {
                return (new error())->error('Error al obtener configuración de costo estimado',
                    $conf_costo_estimado);
            }

            // Calcula el costo estimado sin IVA
            $costo = $this->costo_estimado($conf_costo_estimado, $datos, $porcentaje_iva);
            if (error::$en_error) {
                return (new error())->error('Error al calcular costo estimado', $costo);
            }
        }

        // Integra el costo estimado en el registro
        $registro['costo'] = $costo;

        return $registro;
    }

    /**
     * EM3
     * Integra los datos desglosados de una fecha en un registro.
     *
     * Esta función toma un objeto de datos que incluye un pago con una fecha de vencimiento (`DocDueDate`),
     * desglosa la fecha en año, mes y la fecha original, y los añade a un registro.
     *
     * @param stdClass $datos Objeto que contiene los datos del pago, incluyendo:
     *                        - `pago` (stdClass): Objeto del pago con el campo `DocDueDate` (string, fecha en formato `yyyy-mm-dd`).
     *        - Ejemplo válido:
     *          ```php
     *          $datos = new stdClass();
     *          $datos->pago = new stdClass();
     *          $datos->pago->DocDueDate = '2024-03-10';
     *          ```
     *        - Ejemplo inválido (falta el objeto `pago`):
     *          ```php
     *          $datos = new stdClass();
     *          ```
     *        - Ejemplo inválido (falta `DocDueDate` en `pago`):
     *          ```php
     *          $datos = new stdClass();
     *          $datos->pago = new stdClass();
     *          ```
     *
     * @param array $registro Registro que se actualizará con los datos desglosados de la fecha.
     *        - Debe ser un array asociativo donde se añadirán los datos `year`, `mes` y `fecha`.
     *        - Ejemplo inicial:
     *          ```php
     *          $registro = ['descripcion' => 'Registro inicial'];
     *          ```
     *
     * @return array Devuelve el registro actualizado con los datos de la fecha desglosada:
     *               - `year`: Año extraído de la fecha.
     *               - `mes`: Mes extraído de la fecha.
     *               - `fecha`: Fecha original (`DocDueDate`).
     *               En caso de error, devuelve un arreglo con información detallada del fallo.
     *
     * @throws error Retorna un error si:
     *         - `$datos->pago` no está definido.
     *         - `$datos->pago->DocDueDate` no está definido o no es válido.
     *         - No se puede obtener la fecha desglosada correctamente.
     *
     * Ejemplo de uso:
     * ```php
     * $datos = new stdClass();
     * $datos->pago = new stdClass();
     * $datos->pago->DocDueDate = '2024-03-10';
     *
     * $registro = ['descripcion' => 'Registro inicial'];
     * $registro_actualizado = $this->integra_datos_fecha($datos, $registro);
     *
     * print_r($registro_actualizado);
     * ```
     *
     * Salida esperada (ejemplo):
     * ```php
     * Array
     * (
     *     [descripcion] => Registro inicial
     *     [year] => 2024
     *     [mes] => 03
     *     [fecha] => 2024-03-10
     * )
     * ```
     */
    private function integra_datos_fecha(stdClass $datos, array $registro): array
    {
        // Verifica que el objeto 'pago' existe en $datos
        if (!isset($datos->pago)) {
            return (new error())->error('Error: $datos->pago debe ser un objeto', $datos);
        }

        // Verifica que 'DocDueDate' existe dentro del objeto 'pago'
        if (!isset($datos->pago->DocDueDate)) {
            return (new error())->error('Error: $datos->pago->DocDueDate debe ser una fecha', $datos);
        }

        // Desglosa la fecha en año y mes
        $data_fecha = $this->fecha_desglosada($datos->pago->DocDueDate);

        // Verifica si hubo un error al desglosar la fecha
        if (error::$en_error) {
            return (new error())->error('Error al obtener la fecha desglosada', $data_fecha);
        }

        // Integra los datos desglosados en el registro
        $registro['year'] = $data_fecha->year;
        $registro['mes'] = $data_fecha->mes;
        $registro['fecha'] = $data_fecha->pago_fecha;

        return $registro;
    }


    /**
     * EM3
     * Integra datos generales en un registro basado en la configuración de costos estimados y los datos del contrato y pago.
     *
     * Esta función asegura la existencia de los campos clave (`contrato_id`, `empresa_id`, `conf_costo_estimado_id`) en el
     * registro, obteniendo los valores de los datos proporcionados o inicializándolos si no existen.
     *
     * @param int $conf_costo_estimado_id ID de la configuración de costos estimados. Si es mayor a 0, se buscará en la base de datos.
     *        - Ejemplo válido: `123`
     *        - Ejemplo inválido: `-1` (se inicializa con valores por defecto)
     * @param stdClass $datos Objeto que contiene información del contrato y pago, incluyendo:
     *        - `pago` (stdClass): Objeto con el campo `contrato_id` (int).
     *        - `contrato` (stdClass): Objeto con el campo `empresa_id` (int).
     *        - Ejemplo válido:
     *          ```php
     *          $datos = new stdClass();
     *          $datos->pago = new stdClass();
     *          $datos->pago->contrato_id = 456;
     *          $datos->contrato = new stdClass();
     *          $datos->contrato->empresa_id = 789;
     *          ```
     * @param PDO $link Conexión a la base de datos.
     *        - Debe ser una instancia de `PDO` válida.
     * @param array $registro Registro que se actualizará con los datos generales integrados.
     *        - Ejemplo inicial:
     *          ```php
     *          $registro = ['descripcion' => 'Registro inicial'];
     *          ```
     *
     * @return array Devuelve el registro actualizado con los siguientes campos:
     *               - `contrato_id`: ID del contrato asociado al pago.
     *               - `empresa_id`: ID de la empresa asociada al contrato.
     *               - `conf_costo_estimado_id`: ID de la configuración de costos estimados (si existe).
     *               En caso de error, devuelve un objeto `error`.
     *
     * @throws error Retorna un error si:
     *         - No se puede obtener la configuración de costos estimados desde la base de datos.
     *
     * Ejemplo de uso:
     * ```php
     * $conf_costo_estimado_id = 123;
     *
     * $datos = new stdClass();
     * $datos->pago = new stdClass();
     * $datos->pago->contrato_id = 456;
     * $datos->contrato = new stdClass();
     * $datos->contrato->empresa_id = 789;
     *
     * $link = new PDO(...); // Conexión a la base de datos
     *
     * $registro = ['descripcion' => 'Registro inicial'];
     * $registro_actualizado = $this->integra_datos_generales($conf_costo_estimado_id, $datos, $link, $registro);
     *
     * print_r($registro_actualizado);
     * ```
     *
     * Salida esperada (ejemplo):
     * ```php
     * Array
     * (
     *     [descripcion] => Registro inicial
     *     [contrato_id] => 456
     *     [empresa_id] => 789
     *     [conf_costo_estimado_id] => 123
     * )
     * ```
     */
    private function integra_datos_generales(
        int $conf_costo_estimado_id, stdClass $datos, PDO $link, array $registro
    ): array {
        // Inicializa los objetos si no están definidos
        if (!isset($datos->pago)) {
            $datos->pago = new stdClass();
        }
        if (!isset($datos->pago->contrato_id)) {
            $datos->pago->contrato_id = -1;
        }
        if (!isset($datos->contrato)) {
            $datos->contrato = new stdClass();
        }
        if (!isset($datos->contrato->empresa_id)) {
            $datos->contrato->empresa_id = -1;
        }

        // Inicializa la configuración de costo estimado con valores por defecto
        $conf_costo_estimado = new stdClass();
        $conf_costo_estimado->id = -1;

        // Si existe un ID de configuración de costos estimados, obtenerlo desde la base de datos
        if ($conf_costo_estimado_id > 0) {
            $conf_costo_estimado = (new consultas())->registro_bruto(
                new stdClass(), 'conf_costo_estimado', $conf_costo_estimado_id, $link, false
            );

            // Verifica si hubo un error al obtener la configuración de costos estimados
            if (error::$en_error) {
                return (new error())->error('Error al obtener la configuración de costos estimados',
                    $conf_costo_estimado);
            }
        }

        // Asigna los valores al registro
        $registro['contrato_id'] = $datos->pago->contrato_id;
        $registro['empresa_id'] = $datos->contrato->empresa_id;
        $registro['conf_costo_estimado_id'] = $conf_costo_estimado->id;

        return $registro;
    }

    /**
     * EM3
     * Genera un registro con los datos del pago, contrato y configuración de costos estimados.
     *
     * Esta función valida que el `pago_id` sea válido, obtiene los datos del pago y su contrato,
     * y luego integra información adicional como la fecha desglosada, datos generales y el costo estimado.
     *
     * @param int $conf_costo_estimado_id ID de la configuración de costos estimados.
     *        - Si es mayor a 0, se busca en la base de datos.
     *        - Ejemplo válido: `123`
     *        - Ejemplo inválido: `-1` (se inicializa con valores por defecto)
     * @param PDO $link Conexión activa a la base de datos.
     *        - Debe ser una instancia de `PDO` válida.
     * @param float $porcentaje_iva Porcentaje del IVA aplicado.
     *        - Debe ser mayor o igual a 0.
     *        - Ejemplo válido: `0.16` (para 16%)
     *        - Ejemplo inválido: `-0.05` (retornará un error)
     * @param array $registro Registro base que se actualizará con los datos obtenidos.
     *        - Debe incluir `pago_id` para poder obtener los datos del pago.
     *        - Ejemplo válido:
     *          ```php
     *          $registro = ['pago_id' => 456];
     *          ```
     *
     * @return array Devuelve el registro actualizado con los siguientes campos:
     *               - `year`: Año extraído de la fecha del pago.
     *               - `mes`: Mes extraído de la fecha del pago.
     *               - `fecha`: Fecha original del pago (`DocDueDate`).
     *               - `contrato_id`: ID del contrato asociado al pago.
     *               - `empresa_id`: ID de la empresa asociada al contrato.
     *               - `conf_costo_estimado_id`: ID de la configuración de costos estimados.
     *               - `costo`: Costo estimado calculado sin IVA.
     *               En caso de error, devuelve un objeto `error`.
     *
     * @throws error Retorna un error si:
     *         - `$porcentaje_iva` es menor a 0.
     *         - `$registro['pago_id']` no está definido o es menor a 0.
     *         - No se pueden obtener los datos del pago.
     *         - No se pueden integrar los datos de la fecha, contrato o costos estimados.
     *
     * Ejemplo de uso:
     * ```php
     * $conf_costo_estimado_id = 123;
     * $porcentaje_iva = 0.16;
     * $registro = ['pago_id' => 456];
     *
     * $registro_actualizado = $this->row($conf_costo_estimado_id, $link, $porcentaje_iva, $registro);
     *
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al generar el registro.";
     * } else {
     *     print_r($registro_actualizado);
     * }
     * ```
     *
     * Salida esperada (ejemplo):
     * ```php
     * Array
     * (
     *     [pago_id] => 456
     *     [year] => 2024
     *     [mes] => 03
     *     [fecha] => 2024-03-10
     *     [contrato_id] => 789
     *     [empresa_id] => 5
     *     [conf_costo_estimado_id] => 123
     *     [costo] => 1000.00
     * )
     * ```
     */
    final public function row(
        int $conf_costo_estimado_id,
        PDO $link,
        float $porcentaje_iva,
        array $registro
    ): array {
        // Valida que el porcentaje de IVA no sea negativo
        if ($porcentaje_iva < 0.0) {
            return (new error())->error('Error: $porcentaje_iva es menor a 0', $porcentaje_iva);
        }

        // Verifica que el registro contenga 'pago_id'
        if (!isset($registro['pago_id'])) {
            return (new error())->error('Error: $registro[pago_id] no existe', $registro);
        }
        if ((int)$registro['pago_id'] <= 0) {
            return (new error())->error('Error: $pago_id es menor a 0', $registro);
        }

        // Obtiene los datos del pago y contrato asociados al pago_id
        $datos = $this->datos($link, $registro['pago_id']);
        if (error::$en_error) {
            return (new error())->error('Error al obtener los datos del pago', $registro);
        }

        // Integra la fecha desglosada en el registro
        $registro = $this->integra_datos_fecha($datos, $registro);
        if (error::$en_error) {
            return (new error())->error('Error al integrar la fecha', $registro);
        }

        // Integra datos generales como el contrato y la empresa en el registro
        $registro = $this->integra_datos_generales($conf_costo_estimado_id, $datos, $link, $registro);
        if (error::$en_error) {
            return (new error())->error('Error al integrar los datos generales', $registro);
        }

        // Integra el cálculo del costo estimado en el registro
        $registro = $this->integra_costo($conf_costo_estimado_id, $datos, $link, $porcentaje_iva, $registro);
        if (error::$en_error) {
            return (new error())->error('Error al integrar el costo estimado', $registro);
        }

        return $registro;
    }


}
