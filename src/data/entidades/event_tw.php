<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use function MongoDB\BSON\toRelaxedExtendedJSON;

class event_tw
{
    private string $name_entidad = 'event_tw';


    /**
     * TRASLADADO
     * Obtiene el número de eventos asociados a un empleado en una fecha específica desde la base de datos.
     *
     * Esta función valida que el nombre de la entidad del empleado no esté vacío. Luego, genera una consulta SQL
     * para contar los eventos del empleado (`$ohem_id`) en la fecha proporcionada (`$fecha`) utilizando la clase
     * `event_tw` y su método `n_eventos`. La consulta se ejecuta de manera segura utilizando transacciones.
     * Si la validación, generación de la consulta o su ejecución falla, se devuelve un array con los detalles del error.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param string $fecha Fecha del evento (en formato 'YYYY-MM-DD').
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $ohem_id ID del empleado para el cual se están contando los eventos.
     *
     * @return int|array Devuelve el número de eventos encontrados o un array de error si ocurre algún problema.
     */
    final public function n_eventos(string $entidad_empleado, string $fecha,PDO $link , int $ohem_id)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error entidad empleado esta vacia', $entidad_empleado);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\event_tw())->n_eventos($entidad_empleado, $fecha,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener sql',$sql);
        }

        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al ejecutar sql',$exe);
        }

        return (int)$exe['registros'][0]['n_eventos'];

    }

    /**
     * TRASLADADO
     * Verifica si un empleado tiene eventos en una fecha específica.
     *
     * Esta función valida que el nombre de la entidad del empleado no esté vacío. Luego, llama a la función `n_eventos`
     * para obtener el número de eventos asociados al empleado (`$ohem_id`) en la fecha proporcionada. Si el número de
     * eventos es mayor a 0, la función devuelve `true`, indicando que el empleado tiene eventos; de lo contrario,
     * devuelve `false`. Si ocurre algún error durante el proceso de validación o consulta, se devuelve un array con
     * los detalles del error.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param string $fecha Fecha del evento (en formato 'YYYY-MM-DD').
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $ohem_id ID del empleado para el cual se están verificando los eventos.
     *
     * @return bool|array Devuelve `true` si el empleado tiene eventos, `false` si no los tiene,
     *                    o un array de error si ocurre algún problema durante el proceso.
     */
    final public function tiene_eventos(string $entidad_empleado, string $fecha,PDO $link , int $ohem_id)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error entidad empleado esta vacia', $entidad_empleado);
        }
        $n_eventos = $this->n_eventos($entidad_empleado, $fecha,$link,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener eventos',$n_eventos);
        }

        $tiene = false;
        if($n_eventos > 0 ){
            $tiene = true;
        }
        return $tiene;
    }


}
