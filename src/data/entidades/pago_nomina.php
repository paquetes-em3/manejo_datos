<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use stdClass;

class pago_nomina
{

    /**
     * TRASLADADO
     * Genera un objeto que mapea los campos SQL con sus respectivas entidades.
     *
     * Esta función crea un objeto de tipo `stdClass` donde cada propiedad representa un campo SQL específico.
     * Cada campo contiene un objeto que define la entidad a la que pertenece y el nombre del campo dentro de dicha entidad.
     * Estos mapeos son útiles para construir consultas SQL con nombres de campos personalizados.
     *
     * @return stdClass Un objeto que contiene los mapeos de campos y entidades.
     */
    final public function campos_replace(): stdClass
    {
        $campos = new stdClass();
        $campos->banco_empresa_id = new stdClass();
        $campos->banco_empresa_id->entidad = 'pago_nomina';
        $campos->banco_empresa_id->name_campo = 'banco_empresa_id';

        $campos->banco_empleado_id = new stdClass();
        $campos->banco_empleado_id->entidad = 'pago_nomina';
        $campos->banco_empleado_id->name_campo = 'banco_empleado_id';

        $campos->banco_empresa_descripcion = new stdClass();
        $campos->banco_empresa_descripcion->entidad = 'banco_empresa';
        $campos->banco_empresa_descripcion->name_campo = 'descripcion';

        $campos->banco_empleado_descripcion = new stdClass();
        $campos->banco_empleado_descripcion->entidad = 'banco_empleado';
        $campos->banco_empleado_descripcion->name_campo = 'descripcion';

        return $campos;

    }

    /**
     * TRASLADADO
     * Genera un array de objetos stdClass con información para reemplazar entidades en una operación.
     *
     * Cada objeto dentro del array contiene información sobre una entidad y su relación con otra entidad,
     * así como un posible nombre de entidad renombrada. Esta estructura podría ser usada para mapear o
     * modificar entidades en una operación, como una consulta a la base de datos o una transformación de datos.
     *
     * @return array Un array de objetos stdClass, cada uno con las propiedades:
     *               - `entidad`: El nombre de la entidad principal.
     *               - `entidad_right`: El nombre de la entidad relacionada.
     *               - `rename_entidad`: (opcional) Un nombre alternativo para la entidad.
     */
    final public function params_replaces(): array
    {
        $params = array();
        $params[0] = new stdClass();
        $params[0]->entidad = 'cuenta_empleado';
        $params[0]->entidad_right = 'pago_nomina';

        $params[1] = new stdClass();
        $params[1]->entidad = 'banco';
        $params[1]->entidad_right = 'pago_nomina';
        $params[1]->rename_entidad = 'banco_empresa';

        $params[2] = new stdClass();
        $params[2]->entidad = 'banco';
        $params[2]->entidad_right = 'pago_nomina';
        $params[2]->rename_entidad = 'banco_empleado';

        return $params;

    }


}
