<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;


class configuracion_script
{


    private function esta_en_ejecucion(string $numero_empresa, PDO $link, string $script)
    {

        $sql = (new \desarrollo_em3\manejo_datos\sql\configuracion_script())->get_conf($numero_empresa,$script);
        if(error::$en_error){
            return (new error())->error('Error al obtener sql',$sql);
        }


        $confs = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener datos de script',$confs);
        }
        if(count($confs) === 0){
            return (new error())->error('Error no existe bandera configuracion script',$confs);
        }
        if(count($confs) > 1){
            return (new error())->error('Error existe mas de una bandera configuracion script',$confs);
        }
        $conf = $confs[0];
        if($conf['configuracion_script_en_ejecucion'] === 'activo'){
            return (new error())->error('Error el script ya esta en ejecucion',$conf);
        }

        return $conf;

    }

    final public function inicia(string $numero_empresa, PDO $link, string $script)
    {
        $script_en_exe = $this->esta_en_ejecucion($numero_empresa,$link,$script);
        if(error::$en_error){
            return (new error())->error('Error el verificar script', $script_en_exe);
        }

        $sqls_conf = (new \desarrollo_em3\manejo_datos\sql\configuracion_script())->sqls($numero_empresa,$script);
        if(error::$en_error){
            return (new error())->error('Error el obtener sql de script', $sqls_conf);
        }

        $inicia = (new transacciones($link))->ejecuta_consulta_segura($sqls_conf->en_ejecucion);
        if(error::$en_error){
            return (new error())->error('Error el inicial script', $inicia);
        }

        return $sqls_conf;

    }

}
