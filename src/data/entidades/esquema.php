<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\data\_datos;
use PDO;
use stdClass;


class esquema
{
    private string $name_entidad = 'esquema';

    /**
     * EM3
     * Obtiene los campos de la entidad asociada a la instancia actual.
     *
     * Esta función consulta la estructura de la entidad en la base de datos y devuelve
     * un objeto que contiene los nombres de los campos y sus alias.
     *
     * @param PDO $link Conexión PDO a la base de datos.
     *
     * @return stdClass|array Devuelve un objeto con los campos de la entidad en caso de éxito.
     *                        En caso de error, devuelve un array con detalles del error.
     *
     * @throws error Retorna un error si la consulta falla o si la entidad no tiene campos.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener los campos de una entidad válida
     * $link = new PDO('mysql:host=localhost;dbname=mi_base_datos', 'usuario', 'contraseña');
     * $esquema = new esquema();
     * $campos = $esquema->campos($link);
     * if (isset($campos->error)) {
     *     echo "Error: " . $campos->mensaje;
     * } else {
     *     print_r($campos);
     * }
     *
     * // Ejemplo 2: Error al obtener los campos (entidad inexistente)
     * $link = new PDO('mysql:host=localhost;dbname=mi_base_datos', 'usuario', 'contraseña');
     * $esquema = new esquema();
     * $campos = $esquema->campos($link);
     * // Resultado esperado:
     * // ['error' => 'Error al obtener campos', 'data' => [...]]
     * ```
     */
    final public function campos(PDO $link)
    {
        // Obtiene la estructura de la entidad desde la base de datos
        $campos = (new _datos())->campos_entidad($this->name_entidad, $link);

        // Verifica si ocurrió un error durante la obtención de los campos
        if (error::$en_error) {
            return (new error())->error('Error al obtener campos', $campos);
        }

        // Retorna el objeto con los campos de la entidad
        return $campos;
    }



}
