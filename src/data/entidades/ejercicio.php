<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use PDO;

class ejercicio
{

    final public function obten_ejercicio_anio(bool $get_anio_pasado, PDO $link, int $year)
    {

        if($get_anio_pasado) {
            $year = $year-1;
        }

        $sql = "SELECT *FROM ejercicio WHERE year = '$year'";

        $exe = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener ejercicio',$exe);
        }

        if((count($exe)) === 0){
            return (new error())->error('Error no existe ejercicio registrado del año', $exe);
        }

        return $exe[0];

    }



}
