<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use stdClass;

class reporte_query {

    final public function genera_sentencia_departamento(PDO $link, int $usuario_id) {
        if($usuario_id <= 0){
            return (new error())->error('Error $usuario_id es menor a 0',$usuario_id);
        }

        $sql = (new sql\reporte_query())->obten_id_permisos_departamento($usuario_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }

        $sentencia = '';
        $data = $exe[0]->departamentos;
        if($data ==! '0') {
            $sentencia = "reporte_query.departamento_id IN($data)";
        }

        return $sentencia;
    }

    final public function genera_sentencia_plaza(PDO $link, int $usuario_id) {
        if($usuario_id <= 0){
            return (new error())->error('Error $usuario_id es menor a 0',$usuario_id);
        }

        $sql = (new sql\reporte_query())->obten_id_permisos_plaza($usuario_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }


        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }

        $sentencia = '';
        $data = $exe[0]->plazas;
        if($data ==! '0') {
            $sentencia = "reporte_query.plaza_id IN($data)";
        }
        return $sentencia;
    }

}