<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;

class empleado_dias_pagados
{

    /**
     * TRASLADADO
     * Obtiene los nombres de las columnas relacionadas con los días pagados para una entidad de empleado.
     *
     * Esta función genera un arreglo con los nombres de las columnas correspondientes a los días pagados de un empleado,
     * basándose en el nombre de la entidad del empleado proporcionada. Las columnas incluyen el ID de días pagados y el número de días pagados.
     *
     * @param string $entidad_empleado Nombre de la entidad de empleado utilizado para generar los nombres de las columnas.
     * @return array|string[]|array[] Retorna un arreglo con los nombres de las columnas. En caso de error, retorna un arreglo con información del error.
     */
    final public function columnas_get_dias_pagados(string $entidad_empleado): array
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacio',$entidad_empleado);
        }
        return array($entidad_empleado.'_dias_pagados_id',
            $entidad_empleado.'_dias_pagados_n_dias_pagados');

    }
   final public function filtro_dias_pagados(int $empleado_id, string $key_empleado_id, int $periodo_pago_id): array
   {
       $key_empleado_id = trim($key_empleado_id);
       if($key_empleado_id === ''){
           return (new error())->error('Error $key_empleado_id esta vacio',$key_empleado_id);
       }
       if($periodo_pago_id <= 0){
           return (new error())->error('Error $periodo_pago_id debe ser mayor a 0',$periodo_pago_id);
       }
       if($empleado_id <= 0){
           return (new error())->error('Error $empleado_id debe ser mayor a 0',$empleado_id);
       }
       return array($key_empleado_id=>$empleado_id, 'ohem_dias_pagados.status'=>'activo',
           'periodo_pago.id'=>$periodo_pago_id);

   }


}
