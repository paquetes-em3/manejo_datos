<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use stdClass;

class deposito_aportaciones
{

    private string $key_base_id = 'deposito_aportaciones_id';


    /**
     * TRASLADADO
     * Actualiza la serie de un registro en la entidad `deposito_aportaciones`.
     *
     * Valida que los parámetros `$deposito_aportaciones_id` y `$serie_id` sean mayores a 0. Si las validaciones son exitosas,
     * genera una consulta SQL para actualizar el campo `serie_id` y la ejecuta en la conexión PDO proporcionada.
     *
     * Si ocurre un error en la generación de la consulta SQL o durante su ejecución, la función devuelve un array de error.
     *
     * @param int $deposito_aportaciones_id ID del depósito de aportaciones, debe ser mayor a 0.
     * @param PDO $link Conexión PDO para ejecutar la consulta.
     * @param int $serie_id ID de la serie a actualizar, debe ser mayor a 0.
     *
     * @return array Devuelve el resultado de la ejecución de la consulta o un array de error en caso de falla.
     */
    private function actualiza_serie(int $deposito_aportaciones_id, PDO $link, int $serie_id): array
    {
        if($deposito_aportaciones_id <= 0){
            return(new error())->error('Error $deposito_aportaciones_id debe ser mayor a 0',
                $deposito_aportaciones_id);
        }
        if($serie_id <= 0){
            return(new error())->error('Error $serie_id debe ser mayor a 0',$serie_id);
        }
        $sql = (new sql\deposito_aportaciones())->actualiza_serie($deposito_aportaciones_id, $serie_id);

        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al actualizar serie',$exe);
        }
        return $exe;
    }

    private function actualiza_serie_plaza(stdClass $deposito_aportacion, $link)
    {
        $serie_id = -1;
        $exe = $this->get_series_cuenta_plaza($deposito_aportacion,$link);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }

        if(count($exe) === 1){
            $serie_id = (int)$exe[0]->serie_id;
            $exe = $this->actualiza_serie($deposito_aportacion->id, $link,$serie_id);
            if(error::$en_error){
                return (new error())->error('Error al actualizar serie',$exe);
            }
        }
        return $serie_id;

    }

    private function datos_deposito(int $cuenta_empresa_id, PDO $link, int $pago_corte_id)
    {
        $pago_corte = (new consultas())->registro_bruto(new stdClass(),'pago_corte',$pago_corte_id,$link,
            false);
        if(error::$en_error){
            return (new error())->error('Error al obtener $pago_corte',$pago_corte);
        }
        $cuenta_empresa = (new consultas())->registro_bruto(new stdClass(),'cuenta_empresa',$cuenta_empresa_id,
            $link,false);
        if(error::$en_error){
            return (new error())->error('Error al obtener $cuenta_empresa',$cuenta_empresa);
        }

        $datos = new stdClass();
        $datos->pago_corte = $pago_corte;
        $datos->cuenta_empresa = $cuenta_empresa;

        return $datos;

    }

    /**
     * TRASLADADO
     * Obtiene una fila específica de la tabla `deposito_aportaciones` ejecutando una consulta SQL en la base de datos.
     *
     * Esta función valida que el ID del depósito de aportaciones sea mayor a 0 y que el nombre de la entidad del empleado no esté vacío.
     * Luego, genera la consulta SQL correspondiente mediante la función `deposito_aportaciones_row` de la clase `sql\deposito_aportaciones`.
     * La consulta se ejecuta utilizando la función `exe_objs` de la clase `consultas` en la conexión PDO proporcionada.
     *
     * Si la consulta no devuelve ningún resultado o si ocurre un error en cualquiera de los pasos, la función retorna un array
     * de error con los detalles del fallo.
     *
     * @param int $deposito_aportaciones_id El ID del depósito de aportaciones. Debe ser mayor a 0.
     * @param string $entidad_empleado El nombre de la entidad del empleado. No debe estar vacío.
     * @param PDO $link La conexión PDO a la base de datos.
     *
     * @return stdClass|array Retorna un objeto `stdClass` con los datos del depósito de aportaciones si el proceso es exitoso.
     *                        Si ocurre un error, retorna un array con los detalles del fallo.
     */
    final public function deposito_aportaciones_row(int $deposito_aportaciones_id, string $entidad_empleado, PDO $link)
    {
        if($deposito_aportaciones_id <= 0){
            return(new error())->error('Error $deposito_aportaciones_id es menor a 0',
                $deposito_aportaciones_id);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return(new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }

       $sql = (new sql\deposito_aportaciones())->deposito_aportaciones_row($deposito_aportaciones_id,
           $entidad_empleado);
       if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
       }

       $exe = (new consultas())->exe_objs($link,$sql);
       if(error::$en_error){
           return(new error())->error('Error al ejecutar $sql',$exe);
       }
       if(count($exe) === 0){
           return(new error())->error('Error no existe registro ',$exe);
       }
       return $exe[0];

    }

    private function get_series_cuenta_plaza(stdClass $deposito_aportaciones, PDO $link): array
    {
        $datos = $this->datos_deposito($deposito_aportaciones->cuenta_empresa_id, $link, $deposito_aportaciones->pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $datos',$datos);
        }

        $exe = $this->series_cuenta_plaza($datos->cuenta_empresa->id,$link,$datos->pago_corte->plaza_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }
        return $exe;

    }

    final public function obten_depositos_por_corte(PDO $link,int $pago_corte_id) {
        if($pago_corte_id <= 0){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0',$pago_corte_id);
        }
        
        $sql = (new sql\deposito_aportaciones())->obten_depositos_por_corte($pago_corte_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }

        return $exe;
    }

    final public function regenera_series(int $deposito_aportaciones_id, PDO $link)
    {
        if($deposito_aportaciones_id <= 0){
            return (new error())->error('Error $deposito_aportaciones_id debe ser mayor a 0',
                $deposito_aportaciones_id);
        }
        $deposito_aportacion  = (new consultas())->registro_bruto(new stdClass(),'deposito_aportaciones',
            $deposito_aportaciones_id,$link,false);
        if(error::$en_error){
            return (new error())->error('Error al obtener deposito',$deposito_aportacion);
        }
        $serie_id = -1;
        if((int)$deposito_aportacion->serie_id === 1000){
            $serie_id = $this->actualiza_serie_plaza($deposito_aportacion,$link);
            if(error::$en_error){
                return (new error())->error('Error al actualizar serie',$serie_id);
            }
        }
        return $serie_id;

    }

    private function series_cuenta_plaza(int $cuenta_empresa_id, PDO $link, int $plaza_id): array
    {
        $sql = (new sql\deposito_aportaciones())->series_cuenta_plaza($cuenta_empresa_id, $plaza_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }
        return $exe;

    }

    /**
     * TRASLADADO
     * Calcula la suma de los depósitos asociados a un corte específico.
     *
     * Esta función valida el ID del corte de pago, construye un filtro SQL para
     * seleccionar los depósitos asociados a ese corte, y luego calcula la suma
     * de los depósitos utilizando el campo 'monto_depositado'. Si hay algún error
     * durante el proceso, se devuelve un array con el error correspondiente.
     *
     * @param PDO $link Conexión a la base de datos.
     * @param int $pago_corte_id El ID del corte de pago.
     *
     * @return float|array La suma de los depósitos o un array de error.
     */
    final public function sum_depositos_por_corte(PDO $link, int $pago_corte_id)
    {
        if($pago_corte_id <= 0){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0',$pago_corte_id);
        }
        $filtro_sql = (new \desarrollo_em3\manejo_datos\sql\deposito_aportaciones())->depositos_por_corte_filtro(
            $pago_corte_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $filtro_sql',$filtro_sql);
        }
        $total = (new consultas())->sum_by_filter('monto_depositado','deposito_aportaciones',
            $filtro_sql,$link);
        if(error::$en_error){
            return(new error())->error('Error al obtener $total',$total);
        }

        return $total;

    }

    /**
     * Suma de depósitos por corte de pago para una entidad de empleado específica.
     *
     * Este método recupera los montos totales de depósito asociados con un determinado
     *ID de corte de pago y entidad del empleado. Si el ID de corte de pago es
     *inválido (menor o igual a 0), devuelve error. También maneja
     * posibles errores durante la preparación y ejecución de consultas SQL.
     *
     * @param PDO $link El objeto de conexión PDO para la interacción con la base de datos.
     * @param string $entidad_empleado El identificador de la entidad del empleado.
     * @param int $pago_corte_id El ID del límite de pago para sumar los depósitos.
     *
     * @return array Una serie de depósitos sumados si se realiza correctamente;
     * un objeto de error si ocurre un error.
     */
    final public function sum_deposito_por_pago_corte(PDO $link, string $entidad_empleado, int $pago_corte_id)
    {
        if($pago_corte_id <= 0){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0',$pago_corte_id);
        }
        $sql = (new sql\deposito_aportaciones())->sum_deposito_por_pago_corte($entidad_empleado, $pago_corte_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }

        return $exe;
    }


    /**
     * TRASLADADO
     * Verifica si un depósito de aportaciones es válido y si el pago de corte asociado no ha sido marcado como "validado".
     *
     * Esta función valida que el ID del depósito de aportaciones sea mayor a 0. Luego, obtiene el registro correspondiente
     * de la tabla `deposito_aportaciones` utilizando la función `registro_bruto` de la clase `consultas`. Posteriormente,
     * verifica si el pago de corte asociado al depósito no está marcado como "validado" llamando a la función
     * `verifica_pago_corte`.
     *
     * Si ocurre algún error en cualquiera de estos pasos, la función retorna un array de error con los detalles.
     *
     * @param int $deposito_aportaciones_id El ID del depósito de aportaciones. Debe ser mayor a 0.
     * @param PDO $link La conexión PDO a la base de datos.
     *
     * @return bool|array Retorna `true` si el depósito y el pago de corte asociado son válidos.
     *                    Si ocurre un error, retorna un array con los detalles del fallo.
     */
    final public function verifica_corte_deposito(int $deposito_aportaciones_id, PDO $link)
    {
        if($deposito_aportaciones_id <= 0){
            return (new error())->error('Error $deposito_aportaciones_id debe ser mayor a 0',
                $deposito_aportaciones_id);
        }
        $deposito_aportaciones = (new consultas())->registro_bruto(new stdClass(),'deposito_aportaciones',
            $deposito_aportaciones_id, $link,false);
        if(error::$en_error) {
            return (new error())->error('Error al obtener $deposito_aportaciones', $deposito_aportaciones);
        }

        $verifica = $this->verifica_pago_corte($link,$deposito_aportaciones->pago_corte_id);
        if(error::$en_error) {
            return (new error())->error('Error al verificar $pago_corte', $verifica);
        }

        return true;
    }


    /**
     * TRASLADADO
     * Verifica si un pago de corte es válido y no ha sido marcado como "validado".
     *
     * Esta función valida que el ID del pago de corte sea mayor a 0. Luego, obtiene el registro correspondiente
     * de la tabla `pago_corte` utilizando la función `registro_bruto` de la clase `consultas`. Si el pago de corte
     * está marcado como "validado", la función retorna un error. Si el proceso es exitoso y el pago no está validado,
     * retorna `true`.
     *
     * Si ocurre algún error en cualquier paso, la función retorna un array de error con los detalles.
     *
     * @param PDO $link La conexión PDO a la base de datos.
     * @param int $pago_corte_id El ID del pago de corte. Debe ser mayor a 0.
     *
     * @return bool|array Retorna `true` si el pago de corte no está validado y es válido.
     *                    Si ocurre un error, retorna un array con los detalles del fallo.
     */
    final public function verifica_pago_corte(PDO $link, int $pago_corte_id)
    {
        if($pago_corte_id <= 0){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0',
                $pago_corte_id);
        }
        $pago_corte = (new consultas())->registro_bruto(new stdClass(), 'pago_corte', $pago_corte_id,
            $link, false);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $pago_corte', $pago_corte);
        }
        if ($pago_corte->validado === 'activo') {
            return (new error())->error('Error el corte esta validado', $pago_corte);
        }
        return true;
    }


}
