<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\data\_datos;
use PDO;
use stdClass;


class gasto
{
    private string $name_entidad = 'gasto';


    /**
     * TRASLADADO
     * Genera un array de columnas básicas basado en una columna proporcionada.
     *
     * Esta función valida que la columna proporcionada no esté vacía. Si está vacía,
     * devuelve un error. Si la columna es válida, genera un array con las columnas
     * 'gasto_id', 'gasto_' concatenado con la columna proporcionada, 'unidad_negocio_id',
     * y 'forma_pago_id'.
     *
     * @param string $columna La columna base para generar el array.
     *
     * @return array Retorna un array con las columnas básicas. En caso de error,
     * retorna un array con los detalles del error.
     */
    private function columnas_basicas(string $columna): array
    {
        $columna = trim($columna);
        if($columna === ''){
            return (new error())->error('Error columna esta vacia',$columna);
        }
        return array('gasto_id', 'gasto_'.$columna, 'unidad_negocio_id', 'forma_pago_id');

    }

    private function columnas_cascada(string $columna, array $validacion_cascada)
    {
        $columnas_cascada = array();
        if(isset($validacion_cascada[$columna])){
            $columnas_cascada = $validacion_cascada[$columna];
        }

        return $columnas_cascada;

    }

    private function gasto_valida(string $columna, array $columnas_cascada, int $gasto_id, object $gasto_modelo,
                                       array $validacion_cascada)
    {
        $columnas_basicas = $this->get_columnas_basicas($columna,$columnas_cascada,$validacion_cascada);
        if(error::$en_error){
            return (new error())->error('Error al obtener $columnas_basicas',$columnas_basicas);
        }

        $gasto_modelo->registro_id = $gasto_id;
        $gasto = $gasto_modelo->obten_data(array(), $columnas_basicas);
        if(error::$en_error){
            return (new error())->error('Error al obtener gasto',$gasto);
        }
        return $gasto;

    }

    final public function gasto_validado(string $columna, int $gasto_id, object $gasto_modelo)
    {
        $validacion_cascada = $this->validacion_cascada();
        if(error::$en_error){
            return (new error())->error('Error al obtener $validacion_cascada',$validacion_cascada);
        }

        $columnas_cascada = $this->columnas_cascada($columna,$validacion_cascada);
        if(error::$en_error){
            return (new error())->error('Error al obtener $columnas_cascada',$columnas_cascada);
        }

        $gasto = $this->gasto_valida($columna, $columnas_cascada,$gasto_id,$gasto_modelo,$validacion_cascada);
        if(error::$en_error){
            return (new error())->error('Error al obtener gasto',$gasto);
        }

        $valida = $this->verifica_gasto($columna,$columnas_cascada,$gasto);
        if(error::$en_error){
            return (new error())->error('Error al $validar',$valida);
        }
        return $gasto;

    }

    /**
     * TRASLADADO
     * Obtiene las columnas básicas y las combina con columnas en cascada si es necesario.
     *
     * Esta función valida que la columna proporcionada no esté vacía. Luego, obtiene
     * las columnas básicas utilizando la función `columnas_basicas`. Si la columna
     * está presente en el array de validación en cascada, se combinan las columnas
     * básicas con las columnas en cascada. En caso de error, se devuelve un array con
     * detalles del error.
     *
     * @param string $columna La columna base para generar el array de columnas básicas.
     * @param array $columnas_cascada Un array de columnas adicionales que se añadirán si es necesario.
     * @param array $validacion_cascada Un array de validaciones que define cuándo añadir las columnas en cascada.
     *
     * @return array Retorna un array con las columnas básicas y, si es necesario, con las columnas en cascada.
     * En caso de error, retorna un array con los detalles del error.
     */
    private function get_columnas_basicas(string $columna, array $columnas_cascada, array $validacion_cascada): array
    {
        $columna = trim($columna);
        if($columna === ''){
            return (new error())->error('Error columna esta vacia',$columna);
        }
        $columnas_basicas = $this->columnas_basicas($columna);
        if(error::$en_error){
            return (new error())->error('Error al obtener $columnas_basicas', $columnas_basicas);
        }
        if(isset($validacion_cascada[$columna])){
            $columnas_basicas = array_merge($columnas_basicas, $columnas_cascada);
        }
        return $columnas_basicas;

    }

    final public function upd_gasto(string $columna, array $gasto): array
    {
        $upd_gasto = array('gasto.'.$columna=>'activo', 'gasto.fecha_'.$columna=>date("Y-m-d H:i:s"));
        if($columna === 'tesoreria_valida' ||
            ((int)$gasto['forma_pago_id'] === 1 && ($columna === 'director_finanzas_valida' || $columna === 'auxiliar_admin_valida'))){
            $upd_gasto['gasto.fecha_pago'] = date('Y-m-d');
        }
        return $upd_gasto;

    }
    private function valida_cascada(array $columnas_cascada, array $gasto)
    {
        $validacion = $this->validacion($columnas_cascada, $gasto);
        if(error::$en_error){
            return (new error())->error('Error al obtener $validacion',$validacion);
        }

        if($validacion === 'inactivo'){
            return (new error())->error('Error no puedes validar, hay validaciones pendientes',
                array($gasto, $columnas_cascada));
        }
        return true;

    }

    private function validacion(array $columnas_cascada, array $gasto): string
    {
        $validacion = 'inactivo';
        foreach($columnas_cascada as $key){
            if(trim($gasto[$key]) === 'activo'){
                $validacion = 'activo';
                break;
            }
        }
        return $validacion;

    }
    private function validacion_cascada(): array
    {
        $validacion_cascada['director_admin_valida'] = array('gasto_usuario_valida');
        $validacion_cascada['director_finanzas_valida'] = array( 'gasto_director_admin_valida');
        $validacion_cascada['director_general_valida'] = array('gasto_director_admin_valida');
        $validacion_cascada['auxiliar_admin_valida'] = array('gasto_director_finanzas_valida',
            'gasto_director_general_valida');
        $validacion_cascada['tesoreria_valida'] = array('gasto_director_finanzas_valida',
            'gasto_director_general_valida');

        return $validacion_cascada;

    }

    private function verifica_gasto(string $columna, array $columnas_cascada, array $gasto)
    {
        if(count($columnas_cascada) > 0){
            $valida = $this->valida_cascada($columnas_cascada,$gasto);
            if(error::$en_error){
                return (new error())->error('Error al $validar',$valida);
            }
        }

        if(trim($gasto['gasto_'.$columna]) === 'activo'){
            return (new error())->error('Error la validacion se realizo previamente',$gasto);
        }

        return true;

    }


}
