<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\data\_limpia;
use desarrollo_em3\manejo_datos\sql;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use stdClass;

class contrato
{

    final public function ajusta_monto_pago(string $entidad, int $id, PDO $link)
    {
        $row = (new consultas())->registro_bruto(new stdClass(),$entidad,$id,$link,false);
        if(error::$en_error){
            return (new error())->error('Error al obtener row',$row);
        }


        $montos = $this->montos_pago($row);
        if(error::$en_error){
            return (new error())->error('Error al obtener montos',$montos);
        }

        $exe =$this->exe_montos_pago($entidad,$id,$link,$montos->monto_pago_default,$montos->monto_pago_min,$row);
        if(error::$en_error){
            return (new error())->error('Error al ajusta monto de pago',$exe);
        }
        return $exe;

    }



    /**
     * TRASLADADO
     * Obtiene el ID de la empresa desde un arreglo de resultados.
     *
     * Esta función analiza un arreglo de objetos para buscar el campo `empresa_id` en el primer elemento.
     * Si el campo existe, es válido y no está vacío, se convierte en un entero y se devuelve.
     * Si no se encuentra, se retorna `-1` como valor predeterminado.
     *
     * @param array $rs_plaza_l Arreglo de objetos que contienen información de plazas.
     *                          Ejemplo de estructura:
     *                          ```php
     *                          [
     *                              (object) ['empresa_id' => '123', 'nombre' => 'Plaza A'],
     *                              (object) ['empresa_id' => '456', 'nombre' => 'Plaza B']
     *                          ]
     *                          ```
     *
     * @return int Devuelve el `empresa_id` como entero si existe y es válido, de lo contrario devuelve `-1`.
     *
     * Ejemplo de uso:
     * ```php
     * $rs_plaza_l = [
     *     (object) ['empresa_id' => '123', 'nombre' => 'Plaza A'],
     *     (object) ['empresa_id' => '456', 'nombre' => 'Plaza B']
     * ];
     * $empresa_id = $this->empresa_id($rs_plaza_l);
     * echo $empresa_id; // Salida: 123
     * ```
     *
     * Casos posibles:
     * - Si `$rs_plaza_l` está vacío, retorna `-1`.
     * - Si `$rs_plaza_l[0]` no contiene el campo `empresa_id`, retorna `-1`.
     * - Si `$rs_plaza_l[0]->empresa_id` está vacío o es inválido, retorna `-1`.
     */
    private function empresa_id(array $rs_plaza_l): int
    {
        $empresa_id = -1;
        if(count($rs_plaza_l) > 0){
            if(isset($rs_plaza_l[0]->empresa_id)) {
                if(trim($rs_plaza_l[0]->empresa_id) !== '') {
                    $empresa_id = (int)$rs_plaza_l[0]->empresa_id;
                }
            }
        }

        return $empresa_id;

    }


    private function empresa_id_by_serie(PDO $link, int $plaza_id, string $serie)
    {
        if($plaza_id <= ''){
            return (new error())->error('Error plaza_id debe ser mayor a 0', $plaza_id);
        }
        $con_serie = true;
        $serie = trim($serie);
        if($serie === ''){
            $con_serie = false;
        }
        $empresa_id = -1;
        if($con_serie) {
            $sql = (new sql\plaza_literal())->get_plaza($plaza_id, $serie);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $sql', $sql);
            }

            $rs_plaza_l = (new consultas())->exe_objs($link, $sql);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $rs_plaza_l', $rs_plaza_l);
            }

            $empresa_id = $this->empresa_id($rs_plaza_l);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $empresa_id', $empresa_id);
            }
        }

        return $empresa_id;

    }

    private function exe_montos_pago(string $entidad, int $id, PDO $link, float $monto_pago_default, float $monto_pago_min, stdClass $row_contrato)
    {
        $sql_upd_monto_pago = (new sql\contrato())->sql_upd_monto_pago($entidad,$id,$monto_pago_default, $monto_pago_min,$row_contrato);
        if(error::$en_error){
            return (new error())->error('Error al obtener sql',$sql_upd_monto_pago);
        }

        if($sql_upd_monto_pago !== ''){
            $exe = (new transacciones($link))->ejecuta_consulta_segura($sql_upd_monto_pago);
            if(error::$en_error){
                return (new error())->error('Error al ajusta monto de pago',$exe);
            }
        }
        return $sql_upd_monto_pago;

    }


    final public function integra_empresa(int $contrato_id, PDO $link)
    {
        if($contrato_id <= 0){
            return (new error())->error('Error $contrato_id debe ser mayor a 0',$contrato_id);
        }
        $contrato = (new consultas())->registro_bruto(
            new stdClass(),'contrato',$contrato_id,$link,false);
        if(error::$en_error) {
            return (new error())->error('Error al obtener $contrato',$contrato);
        }

        $plaza_id = $contrato->plaza_id;
        $serie =  $contrato->U_SeCont;

        $empresa_id = $this->empresa_id_by_serie($link,$plaza_id,$serie);
        if(error::$en_error) {
            return (new error())->error('Error al obtener empresa del contrato_id '.$contrato_id,$empresa_id);
        }
        if($empresa_id > 0){

            $sql = (new sql\contrato())->update_empresa($contrato_id,$empresa_id);
            if(error::$en_error) {
                return (new error())->error('Error al obtener sql',$sql);
            }
            $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
            if(error::$en_error) {
                return (new error())->error('Error al actualiza contrato',$exe);
            }
        }
        return $empresa_id;

    }

    /**
     * TRASLADADO
     * Limpia y formatea los datos de ubicación en un registro.
     *
     * Esta función verifica si las claves 'latitud' y 'longitud' existen en el registro. Si existen, se limpia y formatea sus valores
     * utilizando la función `limpia_double`. En caso de error durante el proceso de limpieza, se retorna un arreglo con información del error.
     *
     * @param array $registro Arreglo que contiene los datos de ubicación, incluyendo 'latitud' y 'longitud'.
     * @return array Retorna el registro con los valores de 'latitud' y 'longitud' limpiados y formateados, o un arreglo con información del error en caso de falla.
     */
    final public function limpia_datos_ubicacion(array $registro): array
    {
        if(isset($registro['latitud'])){
            $registro = $this->limpia_double('latitud',$registro);
            if(error::$en_error){
                return (new error())->error('Error al limpiar latitud',$registro);
            }

        }
        if(isset($registro['longitud'])){
            $registro = (new contrato())->limpia_double('longitud',$registro);
            if(error::$en_error){
                return (new error())->error('Error al limpiar longitud',$registro);
            }
        }

        return $registro;

    }

    /**
     * TRASLADADO
     * Limpia y formatea un valor en un registro utilizando una clave específica.
     *
     * Esta función verifica que la clave proporcionada no esté vacía y exista en el registro. Si no existe, se asigna un valor por defecto de 0.
     * Luego, utiliza una función exestructuraterna para limpiar el valor asociado a la clave y lo actualiza en el registro.
     *
     * @param string $key_limpiar Clave en el registro cuyo valor será limpiado y formateado.
     * @param array $registro Arreglo que contiene los datos donde se encuentra el valor a limpiar.
     * @return array Retorna el registro con el valor limpiado y formateado, o un arreglo con información del error en caso de falla.
     */
    private function limpia_double(string $key_limpiar, array $registro): array
    {
        $key_limpiar = trim($key_limpiar);
        if($key_limpiar === ''){
            return (new error())->error('Error $key_limpiar esta vacio', $key_limpiar);
        }
        if(!isset($registro[$key_limpiar])){
            $registro[$key_limpiar ] = 0;
        }
        $var = $registro[$key_limpiar];
        $var = (new _limpia())->limpia_double($var);
        if(error::$en_error){
            return (new error())->error('Error al limpiar el var', $var);
        }

        $registro[$key_limpiar] = $var;

        return $registro;

    }

    private function montos_pago(stdClass $row): stdClass
    {
        $monto_pago_min = -1;
        $monto_pago_default = -1;
        if($row->U_CondPago === 'S'){
            $monto_pago_min = 75;
            $monto_pago_default = 100;
        }
        if($row->U_CondPago === 'Q'){
            $monto_pago_min = 150;
            $monto_pago_default = 200;
        }
        if($row->U_CondPago === 'M'){
            $monto_pago_min = 300;
            $monto_pago_default = 400;
        }

        $montos = new stdClass();
        $montos->monto_pago_min = $monto_pago_min;
        $montos->monto_pago_default = $monto_pago_default;

        return $montos;

    }

    final public function notas_credito_conciliado(int $contrato_id, PDO $link)
    {
        $contrato = (new consultas())->registro_bruto(new stdClass(),'contrato',$contrato_id,$link,false);
        if(error::$en_error){
            return (new error())->error('Error al obtener contrato',$contrato);
        }

        $conciliado =true;

        if($contrato->ultima_rev_sap === '1900-01-01 00:00:00'){
            $conciliado = false;
        }

        if($conciliado && $contrato->tiene_nc_sap === 'inactivo'){
            if(round($contrato->monto_nc_sap,2) > 0.0){
                $conciliado = false;
            }
        }

        if($conciliado && $contrato->tiene_nc_sap === 'inactivo'){
            if(round($contrato->monto_nc_em3,2) > 0.0){
                $conciliado = false;
            }
        }

        if($conciliado && $contrato->tiene_nc_sap === 'activo'){
            if(round($contrato->monto_nc_sap,2) !== round($contrato->monto_nc_em3,2)){
                $conciliado = false;
            }
        }

        return $conciliado;

    }

    final public function obten_contratos_gestor(string $estructura, PDO $link, int $ohem_id, string $status = '') {
        if($ohem_id <= 0) {
            return (new error())->error('Error $ohem_id debe ser mayor a 0',$ohem_id);
        }
        $estructura = strtoupper(trim($estructura));
        if($estructura === ''){
            return  (new error())->error('Error $estructura esta vacia',$estructura);
        }
        $estructuras_valida = array('SAP','EM3');
        if(!in_array($estructura,$estructuras_valida)){
            return  (new error())->error('Error $estructura invalida',$estructuras_valida);
        }

        $sql = (new sql\contrato())->obten_contratos_gestor($estructura,$ohem_id, $status);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener registros',$exe);
        }

        return $exe;
    }

    final public function obten_datos_para_bitacora(int $contrato_id, PDO $link)
    {
        $sql_contrato = (new sql\contrato())->obten_datos_para_bitacora($contrato_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $sql_contrato',$sql_contrato);
        }
        $rows = (new consultas())->exe_objs($link,$sql_contrato);
        if(error::$en_error){
            return (new error())->error('Error al obtener $rows',$rows);
        }
        if(count($rows) === 0){
            return (new error())->error('Error no existe contrato',$rows);
        }
        return $rows[0];


    }

    public function obten_contrato_excel(array $campos_base, string $campo_folio,string $campo_serie,
    PDO $link,string $contrato = '', int $contrato_id = 0, int $limit = 0, $offset = -1,int $plaza_id = 0) {
        if(empty($campos_base)) {
            return (new error())->error('Error campos vacios',$campos_base);
        }

        $sql = (new sql\contrato())->obten_contrato_excel($campos_base,$campo_folio,$campo_serie,
        $contrato,$contrato_id,$limit,$offset,$plaza_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener sql',$sql);
        }

        if($sql !== ''){
            $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
            if(error::$en_error){
                return (new error())->error('Error al validar contrato',$exe);
            }
        }
        return $exe;
    }
}
