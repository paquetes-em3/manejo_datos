<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;
use PDO;

class acl_usuario {

    /**
     * TOTAL
     * Genera una sentencia de seguridad para filtrar registros basados en una entidad específica y un usuario.
     *
     * Esta función valida los datos de permiso, obtiene los IDs relacionados a la entidad para el usuario especificado,
     * y genera una sentencia SQL que puede ser utilizada como filtro para asegurar que solo se acceda a los registros
     * correspondientes. Si no se encuentran IDs, la sentencia resultante será vacía.
     *
     * @param string $entidad La entidad para la cual se desean generar las restricciones de seguridad. No puede ser una cadena vacía.
     * @param PDO $link Conexión PDO utilizada para la ejecución de las consultas.
     * @param int $usuario_id El ID del usuario para quien se generan las restricciones. Debe ser un valor mayor a 0.
     * @param string $status (Opcional) Estado para filtrar los IDs obtenidos. Si se proporciona, se utiliza en la consulta.
     *
     * @return array Retorna un array con la sentencia de seguridad generada. Contiene las claves:
     *               - 'ids': Los IDs obtenidos para la entidad.
     *               - 'sql': La sentencia SQL generada (ejemplo: "entidad_id IN('1,2,3')").
     *               Si ocurre algún error durante el proceso, retorna un array de error con los detalles.
     *
     * @throws error Si la entidad está vacía, si la validación de los datos de permiso falla, o si ocurre un error al obtener los IDs.
     *
     * @access public
     * @final
     */
    final public function genera_sentencia_seguridad(
        string $entidad,PDO $link, int $usuario_id, string $status = ''): array
    {
        if(trim($entidad) === '') {
            return (new error())->error('Error no puede ser vacio',$entidad);
        }
        $valida = (new sql\acl_usuario())->valida_datos_permiso($entidad,$usuario_id);
        if(error::$en_error){
            return  (new error())->error('Error al validar datos',$valida);
        }
        
        $ids = $this->obten_ids_permiso($entidad,$link,$usuario_id, $status);
        if(error::$en_error){
            return(new error())->error('Error al obtener $ids',$ids);
        }
        $sentencia = ["ids" => "","sql" => ""];
        if(!empty($ids)) {
            $cadena = $ids[0]->$entidad;
            $sentencia = [
                "ids" => $ids[0]->$entidad,
                "sql" => $entidad . "_id IN('$cadena')"
            ];
        }        

        return $sentencia;
    }

    /**
     * TOTAL
     * Obtiene los IDs relacionados con los permisos de una entidad específica para un usuario dado mediante una consulta a la base de datos.
     *
     * Esta función valida los datos de permiso, genera una consulta SQL para obtener los IDs relacionados con la entidad especificada
     * y ejecuta dicha consulta usando la conexión proporcionada. Si alguna validación falla o si ocurre un error al ejecutar la consulta,
     * devuelve un array de error.
     *
     * @param string $entidad La entidad para la cual se desean obtener los IDs. No puede ser una cadena vacía.
     * @param PDO $link Conexión PDO para ejecutar la consulta a la base de datos.
     * @param int $usuario_id El ID del usuario para quien se están obteniendo los permisos. Debe ser un valor mayor a 0.
     * @param string $status (Opcional) Estado para filtrar la consulta. Si se proporciona, se agrega un filtro adicional
     *                       en la consulta SQL generada.
     *
     * @return array Retorna un array con los resultados de la consulta si la validación es exitosa,
     *               o un array de error si alguna de las validaciones falla o si hay errores al ejecutar la consulta.
     *
     * @throws error Si la entidad está vacía, si la validación de los datos de permiso falla, o si ocurre un error al ejecutar la consulta.
     *
     * @access private
     */
    private function obten_ids_permiso(string $entidad,PDO $link,int $usuario_id, string $status = ''): array {
        if(trim($entidad) === '') {
            return (new error())->error('Error no puede ser vacio',$entidad);
        }
        $valida = (new sql\acl_usuario())->valida_datos_permiso($entidad,$usuario_id);
        if(error::$en_error){
            return  (new error())->error('Error al validar datos',$valida);
        }
        $sql = (new sql\acl_usuario())->obten_ids_permiso($entidad,$usuario_id,$status);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }

        return $exe;
    }
}