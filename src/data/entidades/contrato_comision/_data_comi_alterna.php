<?php
namespace desarrollo_em3\manejo_datos\data\entidades\contrato_comision;

use desarrollo_em3\error\error;
use stdClass;

/**
 * Clase `_data_comi_alterna`
 *
 * Esta clase forma parte del módulo `contrato_comision` y está diseñada para gestionar la lógica relacionada con
 * comisiones alternativas en esquemas de empleados y coordinadores. Permite inicializar, validar y configurar
 * información de comisiones basándose en registros y esquemas específicos.
 *
 * ### Propósito
 * La clase tiene como objetivo principal facilitar la configuración y evaluación de esquemas de comisiones,
 * proporcionando métodos para:
 * - Verificar si las comisiones aplican a coordinadores o adjuntos.
 * - Gestionar datos relacionados con empleados y jefes inmediatos.
 * - Validar la integridad y consistencia de registros de comisiones.
 *
 * ### Características principales
 * - **Inicialización:** Métodos para inicializar datos de comisiones con valores predeterminados.
 * - **Configuración:** Métodos para determinar si un esquema aplica a un empleado, coordinador o adjunto.
 * - **Validación:** Validaciones extensivas para asegurar que los registros sean consistentes y completos.
 * - **Extensibilidad:** Diseño modular que permite integrar nuevos esquemas o reglas de negocio fácilmente.
 *
 * ### Uso
 * Esta clase está pensada para ser utilizada dentro del sistema de gestión de contratos y comisiones,
 * especialmente en flujos donde se requiere determinar la aplicación de comisiones específicas para empleados
 * basándose en registros y relaciones jerárquicas.
 *
 * ### Ejemplo de uso
 *
 * #### Caso de inicialización de datos
 * ```php
 * use desarrollo_em3\manejo_datos\data\entidades\contrato_comision\_data_comi_alterna;
 *
 * // Crear instancia de la clase
 * $data_comi = new _data_comi_alterna();
 *
 * // Inicializar datos de comisión alternativa
 * $data_comi_alt = $data_comi->data_comi_alterna_init();
 *
 * // Resultado esperado:
 * // (object) [
 * //     'aplica_comision_adjunto' => false,
 * //     'empleado_jefe_inmediato_id' => -1,
 * //     'aplica_coordinador' => false
 * // ]
 * ```
 *
 * #### Caso de verificación de comisiones para un jefe inmediato
 * ```php
 * use desarrollo_em3\manejo_datos\data\entidades\contrato_comision\_data_comi_alterna;
 *
 * // Crear instancia de la clase
 * $data_comi = new _data_comi_alterna();
 *
 * // Configurar datos iniciales
 * $data_comi_alt = (object)[
 *     'empleado_jefe_inmediato_id' => 123,
 *     'aplica_comision_adjunto' => true,
 *     'aplica_coordinador' => false
 * ];
 * $key_empleado_id = 'jefe_id';
 * $registro_comision = [
 *     'jefe_id' => 123,
 *     'esquema_aplica_coordinador' => 'activo'
 * ];
 *
 * // Evaluar si aplica la comisión al jefe inmediato
 * $resultado = $data_comi->data_comi_alterna_encontro_jefe($data_comi_alt, $key_empleado_id, $registro_comision);
 *
 * // Resultado esperado:
 * // (object) [
 * //     'empleado_jefe_inmediato_id' => 123,
 * //     'aplica_coordinador' => true,
 * //     'encontrado' => true
 * // ]
 * ```
 *
 * @package desarrollo_em3\manejo_datos\data\entidades\contrato_comision
 * @since 2025
 */
class _data_comi_alterna{

    /**
     * EM3
     * Configura la aplicación de la comisión alterna para el coordinador.
     *
     * Este método modifica el objeto `$data_comi_alt` para establecer si la comisión alterna aplica al coordinador,
     * dependiendo del valor de `$esquema_aplica_coordinador`.
     *
     * @param stdClass $data_comi_alt Objeto que contiene los datos de la comisión alterna.
     *                                Se agregará o actualizará la propiedad `aplica_coordinador` como un booleano.
     * @param string $esquema_aplica_coordinador Valor que indica si el esquema aplica al coordinador.
     *                                           Debe ser `'activo'` para activar la propiedad `aplica_coordinador`.
     *
     * @return stdClass Devuelve el objeto `$data_comi_alt` con la propiedad `aplica_coordinador` actualizada.
     *
     * @example Caso exitoso: Esquema aplica al coordinador
     * ```php
     * $data_comi_alt = new stdClass();
     * $esquema_aplica_coordinador = 'activo';
     *
     * $resultado = $obj->data_comi_alterna_aplica_cord($data_comi_alt, $esquema_aplica_coordinador);
     * // Resultado:
     * // $resultado->aplica_coordinador === true
     * ```
     *
     * @example Caso: Esquema no aplica al coordinador
     * ```php
     * $data_comi_alt = new stdClass();
     * $esquema_aplica_coordinador = 'inactivo';
     *
     * $resultado = $obj->data_comi_alterna_aplica_cord($data_comi_alt, $esquema_aplica_coordinador);
     * // Resultado:
     * // $resultado->aplica_coordinador === false
     * ```
     *
     * @example Caso: Valor vacío o no válido para `$esquema_aplica_coordinador`
     * ```php
     * $data_comi_alt = new stdClass();
     * $esquema_aplica_coordinador = '';
     *
     * $resultado = $obj->data_comi_alterna_aplica_cord($data_comi_alt, $esquema_aplica_coordinador);
     * // Resultado:
     * // $resultado->aplica_coordinador === false
     * ```
     */
    private function data_comi_alterna_aplica_cord(
        stdClass $data_comi_alt, string $esquema_aplica_coordinador): stdClass
    {
        $data_comi_alt->aplica_coordinador = false;
        if($esquema_aplica_coordinador === 'activo'){
            $data_comi_alt->aplica_coordinador = true;
        }
        return $data_comi_alt;
    }

    /**
     * EM3
     * Genera un objeto con información de comisión alternativa y valida si aplica al adjunto.
     *
     * Este método inicializa un objeto con datos de comisión alternativa basados en el identificador
     * del jefe inmediato. Además, verifica si el empleado tiene un esquema que aplica al coordinador
     * en la cadena de registros de comisión.
     *
     * @param array $cadena Datos relacionados con la comisión, que deben incluir:
     *                      - `'empleado_jefe_inmediato_id'` (opcional): ID del jefe inmediato.
     *                        Si no está definido o es nulo, se inicializa como una cadena vacía.
     * @param string $key_empleado_id Clave para identificar al empleado dentro de los registros de comisión.
     *                                Debe ser una cadena no vacía.
     * @param array $registros_cadena_comision Lista de registros de comisión donde se buscarán coincidencias.
     *                                         Cada registro debe ser un array que incluya:
     *                                         - `$key_empleado_id`: Identificador del empleado.
     *                                         - `'esquema_aplica_coordinador'`: Indica si aplica al coordinador.
     *
     * @return stdClass|array Devuelve un objeto `stdClass` con las siguientes propiedades:
     *                        - `aplica_comision_adjunto`: Indica si se aplica la comisión al adjunto (`true` por defecto).
     *                        - `empleado_jefe_inmediato_id`: ID del jefe inmediato.
     *                        - `aplica_coordinador`: Indica si aplica al coordinador (`false` por defecto).
     *                        - `encontrado`: Indica si se encontró al jefe inmediato en los registros de comisión.
     *                        En caso de error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Esquema aplica al coordinador
     * ```php
     * $cadena = ['empleado_jefe_inmediato_id' => 123];
     * $key_empleado_id = 'empleado_id';
     * $registros_cadena_comision = [
     *     [
     *         'empleado_id' => 123,
     *         'esquema_aplica_coordinador' => 'activo',
     *     ],
     *     [
     *         'empleado_id' => 456,
     *         'esquema_aplica_coordinador' => 'inactivo',
     *     ],
     * ];
     *
     * $resultado = $this->data_comi_alt_es_adjunto($cadena, $key_empleado_id, $registros_cadena_comision);
     * // Resultado:
     * // (object) [
     * //     'aplica_comision_adjunto' => true,
     * //     'empleado_jefe_inmediato_id' => 123,
     * //     'aplica_coordinador' => true,
     * //     'encontrado' => true
     * // ]
     * ```
     *
     * @example Caso: No se encuentra el jefe inmediato en los registros
     * ```php
     * $cadena = ['empleado_jefe_inmediato_id' => 999];
     * $key_empleado_id = 'empleado_id';
     * $registros_cadena_comision = [
     *     [
     *         'empleado_id' => 123,
     *         'esquema_aplica_coordinador' => 'activo',
     *     ],
     *     [
     *         'empleado_id' => 456,
     *         'esquema_aplica_coordinador' => 'inactivo',
     *     ],
     * ];
     *
     * $resultado = $this->data_comi_alt_es_adjunto($cadena, $key_empleado_id, $registros_cadena_comision);
     * // Resultado:
     * // (object) [
     * //     'aplica_comision_adjunto' => true,
     * //     'empleado_jefe_inmediato_id' => 999,
     * //     'aplica_coordinador' => false,
     * //     'encontrado' => false
     * // ]
     * ```
     *
     * @example Caso de error: Clave de empleado vacía
     * ```php
     * $cadena = ['empleado_jefe_inmediato_id' => 123];
     * $key_empleado_id = '';
     * $registros_cadena_comision = [
     *     [
     *         'empleado_id' => 123,
     *         'esquema_aplica_coordinador' => 'activo',
     *     ]
     * ];
     *
     * $resultado = $this->data_comi_alt_es_adjunto($cadena, $key_empleado_id, $registros_cadena_comision);
     * // Resultado:
     * // [
     * //     'error' => 'Error $key_empleado_id esta vacia',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Caso de error: Registro de comisión no es un array
     * ```php
     * $cadena = ['empleado_jefe_inmediato_id' => 123];
     * $key_empleado_id = 'empleado_id';
     * $registros_cadena_comision = [
     *     'empleado_id' => 123,
     *     'esquema_aplica_coordinador' => 'activo',
     * ];
     *
     * $resultado = $this->data_comi_alt_es_adjunto($cadena, $key_empleado_id, $registros_cadena_comision);
     * // Resultado:
     * // [
     * //     'error' => 'Error $cadena debe ser un array',
     * //     'data' => [...]
     * // ]
     * ```
     */

    final public function data_comi_alt_es_adjunto(
        array $cadena, string $key_empleado_id, array $registros_cadena_comision)
    {
        if(!isset($cadena['empleado_jefe_inmediato_id'])){
            $cadena['empleado_jefe_inmediato_id'] = '';
        }
        if(is_null($cadena['empleado_jefe_inmediato_id'])){
            $cadena['empleado_jefe_inmediato_id'] = '';
        }
        $key_empleado_id = trim($key_empleado_id);
        if($key_empleado_id === ''){
            return (new error())->error('Error $key_empleado_id esta vacia',$key_empleado_id);
        }
        $data_comi_alt = $this->data_comi_alterna_jefe($cadena['empleado_jefe_inmediato_id']);
        if(error::$en_error){
            return (new error())->error('Error al inicializar datos de comsion',$data_comi_alt);
        }
        $data_comi_alt = $this->data_comi_alterna_es_coord($data_comi_alt,$key_empleado_id,$registros_cadena_comision);
        if(error::$en_error){
            return (new error())->error('Error al inicializar datos de comsion',$data_comi_alt);
        }
        return $data_comi_alt;

    }


    /**
     * EM3
     * Verifica si un jefe inmediato se encuentra en una cadena de registros de comisión.
     *
     * Este método realiza las siguientes operaciones:
     * - Valida que el identificador del empleado jefe (`key_empleado_id`) no esté vacío.
     * - Verifica que el objeto `$data_comi_alt` incluya la propiedad `empleado_jefe_inmediato_id`.
     * - Recorre los registros de la cadena de comisión para buscar una coincidencia con el jefe inmediato.
     * - Si encuentra una coincidencia, actualiza los datos de la comisión (`aplica_coordinador`).
     *
     * @param stdClass $data_comi_alt Objeto que contiene información sobre la comisión alternativa.
     *                                Debe incluir la propiedad `empleado_jefe_inmediato_id`.
     * @param string $key_empleado_id Identificador clave del empleado jefe en el registro de la cadena.
     *                                No debe estar vacío.
     * @param array $registros_cadena_comision Lista de registros de la cadena de comisión.
     *                                         Cada registro debe ser un array con las claves `$key_empleado_id`
     *                                         y `esquema_aplica_coordinador`.
     *
     * @return stdClass|array Devuelve el objeto `$data_comi_alt` actualizado si todo es correcto.
     *                        Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Jefe encontrado en la cadena
     * ```php
     * $data_comi_alt = (object)[
     *     'empleado_jefe_inmediato_id' => '123',
     *     'aplica_coordinador' => false
     * ];
     * $key_empleado_id = 'jefe_id';
     * $registros_cadena_comision = [
     *     [
     *         'jefe_id' => '123',
     *         'esquema_aplica_coordinador' => 'activo'
     *     ],
     *     [
     *         'jefe_id' => '456',
     *         'esquema_aplica_coordinador' => 'inactivo'
     *     ]
     * ];
     *
     * $resultado = $this->data_comi_alterna_es_coord($data_comi_alt, $key_empleado_id, $registros_cadena_comision);
     * // Resultado:
     * // (object) [
     * //     'empleado_jefe_inmediato_id' => '123',
     * //     'aplica_coordinador' => true,
     * //     'encontrado' => true
     * // ]
     * ```
     *
     * @example Caso: Identificador del jefe vacío
     * ```php
     * $data_comi_alt = (object)[
     *     'empleado_jefe_inmediato_id' => '123',
     *     'aplica_coordinador' => false
     * ];
     * $key_empleado_id = '';
     * $registros_cadena_comision = [
     *     [
     *         'jefe_id' => '123',
     *         'esquema_aplica_coordinador' => 'activo'
     *     ]
     * ];
     *
     * $resultado = $this->data_comi_alterna_es_coord($data_comi_alt, $key_empleado_id, $registros_cadena_comision);
     * // Resultado:
     * // [
     * //     'error' => 'Error $key_empleado_id esta vacia',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Caso: Registro de cadena no es un array
     * ```php
     * $data_comi_alt = (object)[
     *     'empleado_jefe_inmediato_id' => '123',
     *     'aplica_coordinador' => false
     * ];
     * $key_empleado_id = 'jefe_id';
     * $registros_cadena_comision = [
     *     'No es un array'
     * ];
     *
     * $resultado = $this->data_comi_alterna_es_coord($data_comi_alt, $key_empleado_id, $registros_cadena_comision);
     * // Resultado:
     * // [
     * //     'error' => 'Error $cadena debe ser un array',
     * //     'data' => [...]
     * // ]
     * ```
     *
     * @example Caso: Sin coincidencia en la cadena
     * ```php
     * $data_comi_alt = (object)[
     *     'empleado_jefe_inmediato_id' => '999',
     *     'aplica_coordinador' => false
     * ];
     * $key_empleado_id = 'jefe_id';
     * $registros_cadena_comision = [
     *     [
     *         'jefe_id' => '123',
     *         'esquema_aplica_coordinador' => 'activo'
     *     ],
     *     [
     *         'jefe_id' => '456',
     *         'esquema_aplica_coordinador' => 'inactivo'
     *     ]
     * ];
     *
     * $resultado = $this->data_comi_alterna_es_coord($data_comi_alt, $key_empleado_id, $registros_cadena_comision);
     * // Resultado:
     * // (object) [
     * //     'empleado_jefe_inmediato_id' => '999',
     * //     'aplica_coordinador' => false,
     * //     'encontrado' => false
     * // ]
     * ```
     */

    private function data_comi_alterna_es_coord(
        stdClass $data_comi_alt, string $key_empleado_id, array $registros_cadena_comision)
    {
        $key_empleado_id = trim($key_empleado_id);
        if($key_empleado_id === ''){
            return (new error())->error('Error $key_empleado_id esta vacia',$key_empleado_id);
        }
        if(!isset($data_comi_alt->empleado_jefe_inmediato_id)){
            return (new error())->error('Error $data_comi_alt->empleado_jefe_inmediato_id No existe'
                ,$data_comi_alt);
        }
        foreach ($registros_cadena_comision as $cadena){
            if(!is_array($cadena)){
                return (new error())->error('Error $cadena debe ser un array',$registros_cadena_comision);
            }
            if(!isset($cadena[$key_empleado_id])){
                return (new error())->error(
                    'Error $cadena en la posicion '.$key_empleado_id.' No existe',$cadena);
            }
            if(!isset($cadena['esquema_aplica_coordinador'])){
                return (new error())->error(
                    'Error $cadena en la posicion esquema_aplica_coordinador No existe', $cadena);
            }
            $data_comi_alt = $this->data_comi_alterna_encontro_jefe($data_comi_alt,$key_empleado_id,$cadena);
            if(error::$en_error){
                return (new error())->error('Error al inicializar datos de comsion',$data_comi_alt);
            }
            if($data_comi_alt->encontrado){
                break;
            }
        }
        return $data_comi_alt;

    }

    /**
     * EM3
     * Inicializa un objeto con información de la comisión alternativa para un jefe inmediato.
     *
     * Este método crea un objeto `stdClass` que contiene datos relacionados con la comisión alternativa.
     * Si el identificador del jefe inmediato (`$empleado_jefe_inmediato_id`) es `null` o está vacío,
     * se asigna el valor predeterminado `-1`. El objeto resultante incluye propiedades para determinar
     * si se aplica la comisión adjunta y si se aplica al coordinador.
     *
     * @param mixed $empleado_jefe_inmediato_id Identificador del jefe inmediato. Puede ser un número o una cadena.
     *                                          Si es `null` o una cadena vacía, se asignará el valor `-1`.
     *
     * @return stdClass Objeto con las propiedades inicializadas:
     *                  - `aplica_comision_adjunto`: Indica si se aplica la comisión adjunta (por defecto, `true`).
     *                  - `empleado_jefe_inmediato_id`: ID del jefe inmediato (predeterminado `-1` si es nulo o vacío).
     *                  - `aplica_coordinador`: Indica si se aplica la comisión al coordinador (por defecto, `false`).
     *
     * @example Caso exitoso: ID del jefe es un número
     * ```php
     * $empleado_jefe_inmediato_id = 123;
     * $resultado = $this->data_comi_alterna_jefe($empleado_jefe_inmediato_id);
     * // Resultado:
     * // (object) [
     * //     'aplica_comision_adjunto' => true,
     * //     'empleado_jefe_inmediato_id' => 123,
     * //     'aplica_coordinador' => false
     * // ]
     * ```
     *
     * @example Caso: ID del jefe es nulo
     * ```php
     * $empleado_jefe_inmediato_id = null;
     * $resultado = $this->data_comi_alterna_jefe($empleado_jefe_inmediato_id);
     * // Resultado:
     * // (object) [
     * //     'aplica_comision_adjunto' => true,
     * //     'empleado_jefe_inmediato_id' => -1,
     * //     'aplica_coordinador' => false
     * // ]
     * ```
     *
     * @example Caso: ID del jefe es una cadena vacía
     * ```php
     * $empleado_jefe_inmediato_id = '';
     * $resultado = $this->data_comi_alterna_jefe($empleado_jefe_inmediato_id);
     * // Resultado:
     * // (object) [
     * //     'aplica_comision_adjunto' => true,
     * //     'empleado_jefe_inmediato_id' => -1,
     * //     'aplica_coordinador' => false
     * // ]
     * ```
     *
     * @example Caso: ID del jefe es una cadena con espacios
     * ```php
     * $empleado_jefe_inmediato_id = '   ';
     * $resultado = $this->data_comi_alterna_jefe($empleado_jefe_inmediato_id);
     * // Resultado:
     * // (object) [
     * //     'aplica_comision_adjunto' => true,
     * //     'empleado_jefe_inmediato_id' => -1,
     * //     'aplica_coordinador' => false
     * // ]
     * ```
     */
    private function data_comi_alterna_jefe($empleado_jefe_inmediato_id): stdClass
    {
        if(is_null($empleado_jefe_inmediato_id)){
            $empleado_jefe_inmediato_id = -1;
        }
        if(trim($empleado_jefe_inmediato_id) === ''){
            $empleado_jefe_inmediato_id = -1;
        }

        $data_comi_alt = new stdClass();
        $data_comi_alt->aplica_comision_adjunto = true;
        $data_comi_alt->empleado_jefe_inmediato_id = $empleado_jefe_inmediato_id;
        $data_comi_alt->aplica_coordinador = false;

        return $data_comi_alt;

    }


    /**
     * EM3
     * Verifica si un jefe inmediato coincide con los datos de un registro de comisión y actualiza los datos de la comisión.
     *
     * Este método realiza las siguientes operaciones:
     * - Valida que el identificador del empleado jefe (`key_empleado_id`) no esté vacío.
     * - Verifica si el `registro_comision` contiene las claves necesarias (`key_empleado_id` y `esquema_aplica_coordinador`).
     * - Comprueba si el identificador del jefe inmediato en `data_comi_alt` coincide con el valor correspondiente en `registro_comision`.
     * - Si hay coincidencia, actualiza los datos de la comisión (`aplica_coordinador`) según el esquema del registro.
     *
     * @param stdClass $data_comi_alt Objeto que contiene información sobre la comisión alternativa.
     *                                Debe incluir la propiedad `empleado_jefe_inmediato_id`.
     * @param string $key_empleado_id Identificador clave del empleado jefe en el registro de comisión.
     *                                No debe estar vacío.
     * @param array $registro_comision Array que contiene la información del registro de comisión.
     *                                 Debe incluir las claves `$key_empleado_id` y `esquema_aplica_coordinador`.
     *
     * @return stdClass|array Devuelve el objeto `$data_comi_alt` actualizado si todo es correcto.
     *                        Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Jefe encontrado
     * ```php
     * $data_comi_alt = (object)[
     *     'empleado_jefe_inmediato_id' => '123',
     *     'aplica_coordinador' => false
     * ];
     * $key_empleado_id = 'jefe_id';
     * $registro_comision = [
     *     'jefe_id' => '123',
     *     'esquema_aplica_coordinador' => 'activo'
     * ];
     *
     * $resultado = $this->data_comi_alterna_encontro_jefe($data_comi_alt, $key_empleado_id, $registro_comision);
     * // Resultado:
     * // (object) [
     * //     'empleado_jefe_inmediato_id' => '123',
     * //     'aplica_coordinador' => true,
     * //     'encontrado' => true
     * // ]
     * ```
     *
     * @example Caso: Identificador del jefe vacío
     * ```php
     * $data_comi_alt = (object)[
     *     'empleado_jefe_inmediato_id' => '123'
     * ];
     * $key_empleado_id = '';
     * $registro_comision = [
     *     'jefe_id' => '123',
     *     'esquema_aplica_coordinador' => 'activo'
     * ];
     *
     * $resultado = $this->data_comi_alterna_encontro_jefe($data_comi_alt, $key_empleado_id, $registro_comision);
     * // Resultado:
     * // [
     * //     'error' => 'Error $key_empleado_id esta vacia',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Caso: Clave no existente en el registro de comisión
     * ```php
     * $data_comi_alt = (object)[
     *     'empleado_jefe_inmediato_id' => '123'
     * ];
     * $key_empleado_id = 'jefe_id';
     * $registro_comision = [
     *     'esquema_aplica_coordinador' => 'activo'
     * ];
     *
     * $resultado = $this->data_comi_alterna_encontro_jefe($data_comi_alt, $key_empleado_id, $registro_comision);
     * // Resultado:
     * // [
     * //     'error' => 'Error $registro_comision en la posicion jefe_id No existe',
     * //     'data' => [...]
     * // ]
     * ```
     *
     * @example Caso: Sin coincidencia del jefe
     * ```php
     * $data_comi_alt = (object)[
     *     'empleado_jefe_inmediato_id' => '456',
     *     'aplica_coordinador' => false
     * ];
     * $key_empleado_id = 'jefe_id';
     * $registro_comision = [
     *     'jefe_id' => '123',
     *     'esquema_aplica_coordinador' => 'activo'
     * ];
     *
     * $resultado = $this->data_comi_alterna_encontro_jefe($data_comi_alt, $key_empleado_id, $registro_comision);
     * // Resultado:
     * // (object) [
     * //     'empleado_jefe_inmediato_id' => '456',
     * //     'aplica_coordinador' => false,
     * //     'encontrado' => false
     * // ]
     * ```
     */
    private function data_comi_alterna_encontro_jefe(
        stdClass $data_comi_alt, string $key_empleado_id, array $registro_comision)
    {
        $key_empleado_id = trim($key_empleado_id);
        if($key_empleado_id === ''){
            return (new error())->error('Error $key_empleado_id esta vacia',$key_empleado_id);
        }
        if(!isset($registro_comision[$key_empleado_id])){
            return (new error())->error('Error $registro_comision en la posicion '.$key_empleado_id.' No existe'
                ,$registro_comision);
        }
        if(!isset($registro_comision['esquema_aplica_coordinador'])){
            return (new error())->error(
                'Error $registro_comision en la posicion esquema_aplica_coordinador No existe',
                $registro_comision);
        }
        if(!isset($data_comi_alt->empleado_jefe_inmediato_id)){
            return (new error())->error('Error $data_comi_alt->empleado_jefe_inmediato_id No existe'
                ,$data_comi_alt);
        }
        $encontrado = false;
        if($registro_comision[$key_empleado_id] === $data_comi_alt->empleado_jefe_inmediato_id){
            $data_comi_alt = $this->data_comi_alterna_aplica_cord(
                $data_comi_alt,$registro_comision['esquema_aplica_coordinador']);
            if(error::$en_error){
                return (new error())->error('Error al inicializar datos de comsion',$data_comi_alt);
            }
            $encontrado = true;
        }
        $data_comi_alt->encontrado =$encontrado;
        return $data_comi_alt;

    }

    /**
     * EM3
     * Inicializa un objeto para gestionar la comisión alternativa.
     *
     * Este método crea e inicializa un objeto `stdClass` con las propiedades necesarias para
     * manejar la lógica de comisiones alternativas. Las propiedades se inicializan con valores predeterminados.
     *
     * @return stdClass Devuelve un objeto con las siguientes propiedades:
     *                  - `aplica_comision_adjunto` (bool): Indica si se aplica la comisión al adjunto (por defecto `false`).
     *                  - `empleado_jefe_inmediato_id` (int): ID del jefe inmediato (por defecto `-1`).
     *                  - `aplica_coordinador` (bool): Indica si se aplica al coordinador (por defecto `false`).
     *
     * @example Caso exitoso: Inicialización del objeto
     * ```php
     * $resultado = $this->data_comi_alterna_init();
     * // Resultado:
     * // (object) [
     * //     'aplica_comision_adjunto' => false,
     * //     'empleado_jefe_inmediato_id' => -1,
     * //     'aplica_coordinador' => false
     * // ]
     * ```
     *
     * @example Uso en conjunto con otros métodos
     * ```php
     * // Inicializar el objeto
     * $data_comi_alt = $this->data_comi_alterna_init();
     *
     * // Actualizar propiedades del objeto
     * $data_comi_alt->empleado_jefe_inmediato_id = 123;
     * $data_comi_alt->aplica_comision_adjunto = true;
     * $data_comi_alt->aplica_coordinador = true;
     *
     * // Resultado esperado:
     * // (object) [
     * //     'aplica_comision_adjunto' => true,
     * //     'empleado_jefe_inmediato_id' => 123,
     * //     'aplica_coordinador' => true
     * // ]
     * ```
     *
     * @note Este método es útil para inicializar un objeto con valores seguros antes de aplicar lógica adicional.
     */

    final public function data_comi_alterna_init(): stdClass
    {
        $data_comi_alt = new stdClass();
        $data_comi_alt->aplica_comision_adjunto = false;
        $data_comi_alt->empleado_jefe_inmediato_id = -1;
        $data_comi_alt->aplica_coordinador = false;
        return $data_comi_alt;

    }
}
