<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;

class meta_gestor
{


    /**
     * EM3
     * Ajusta el valor de 'COBRADO TOTAL' en 'COBRADO CONTRATOS EXTRA' si 'META' es igual a 0.
     *
     * Esta función valida el índice y verifica si el campo 'META' está definido y es igual a 0.
     * Si es así, llama a la función `integra_cobrado_total` para integrar el valor de 'COBRADO TOTAL'
     * en 'COBRADO CONTRATOS EXTRA'. Devuelve el array `$rows_out` actualizado.
     *
     * @param int $indice Índice de la fila en `$rows_out` que se actualizará. Debe ser mayor o igual a 0.
     * @param array $row Fila de datos que contiene la clave 'META' y 'COBRADO TOTAL'.
     * @param array $rows_out Array de filas donde se ajustará el valor de 'COBRADO TOTAL' en la posición `$indice`.
     *
     * @return array Devuelve el array `$rows_out` actualizado si 'META' es igual a 0.
     *               Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Caso Exitoso: 'META' igual a 0
     * ```php
     * $indice = 0;
     * $row = ['META' => 0.0, 'COBRADO TOTAL' => 500.0];
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     *     ['GESTOR ID' => '456', 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->ajusta_cobrado_total($indice, $row, $rows_out);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 500.0],
     * //     ['GESTOR ID' => '456', 'COBRADO CONTRATOS EXTRA' => 0],
     * // ]
     * ```
     *
     * @example Caso: 'META' diferente de 0
     * ```php
     * $indice = 1;
     * $row = ['META' => 100.0, 'COBRADO TOTAL' => 500.0];
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     *     ['GESTOR ID' => '456', 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->ajusta_cobrado_total($indice, $row, $rows_out);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     * //     ['GESTOR ID' => '456', 'COBRADO CONTRATOS EXTRA' => 0],
     * // ]
     * ```
     *
     * @example Caso: 'META' no definido
     * ```php
     * $indice = 0;
     * $row = ['COBRADO TOTAL' => 500.0];
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->ajusta_cobrado_total($indice, $row, $rows_out);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 500.0],
     * // ]
     * ```
     *
     * @example Error: Índice negativo
     * ```php
     * $indice = -1;
     * $row = ['META' => 0.0, 'COBRADO TOTAL' => 500.0];
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->ajusta_cobrado_total($indice, $row, $rows_out);
     * // Resultado:
     * // [
     * //     'error' => 'Error indice debe ser mayor o igual a 0',
     * //     'data' => -1
     * // ]
     * ```
     */
    private function ajusta_cobrado_total(int $indice, array $row, array $rows_out): array
    {
        // Validar que el índice sea mayor o igual a 0
        if ($indice < 0) {
            return (new error())->error('Error indice debe ser mayor o igual a 0', $indice);
        }

        // Inicializar 'META' si no está definida
        if (!isset($row['META'])) {
            $row['META'] = 0.0;
        }

        // Si 'META' es igual a 0, integrar el valor de 'COBRADO TOTAL'
        if ((float)$row['META'] === 0.0) {
            $rows_out = $this->integra_cobrado_total($indice, $row, $rows_out);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $rows_out', $rows_out);
            }
        }

        return $rows_out;
    }

    /**
     * EM3
     * Ajusta los valores totales acumulando los valores de un array de filas basado en claves específicas.
     *
     * Esta función recorre un conjunto predefinido de claves (`keys_val`) y acumula los valores correspondientes
     * del array `$row` en el array `$totales`. Si alguna clave no existe en `$row` o `$totales`, se inicializa con `0.0`.
     * Los valores se redondean a 2 decimales antes de acumularlos.
     *
     * @param array $row Array que contiene los datos de la fila actual. Debe incluir las claves que se desean acumular.
     * @param array $totales Array acumulador que contiene los valores totales. Las claves deben coincidir con `$row`.
     *
     * @return array Devuelve el array `$totales` actualizado con los valores acumulados de `$row`.
     *               Si alguna clave no existe en `$row` o `$totales`, se inicializa en `0.0` antes de acumular.
     *
     * @example Uso Exitoso
     * ```php
     * $row = [
     *     'CONTRATOS CONGELADOS' => 5,
     *     'CARTERA ACTUAL' => 10,
     *     'CONTRATOS NUEVOS' => 3,
     *     'META' => 100,
     *     'COBRADO TOTAL' => 50,
     *     'COBRADO META' => 30,
     *     'COBRADO META DE MAS' => 20,
     *     'COBRADO CONTRATOS NUEVOS' => 15,
     *     'COBRADO CONTRATOS EXTRA' => 10
     * ];
     * $totales = [
     *     'CONTRATOS CONGELADOS' => 0,
     *     'CARTERA ACTUAL' => 0,
     *     'CONTRATOS NUEVOS' => 0,
     *     'META' => 0,
     *     'COBRADO TOTAL' => 0,
     *     'COBRADO META' => 0,
     *     'COBRADO META DE MAS' => 0,
     *     'COBRADO CONTRATOS NUEVOS' => 0,
     *     'COBRADO CONTRATOS EXTRA' => 0
     * ];
     * $resultado = $obj->ajusta_totales($row, $totales);
     * // Resultado:
     * // [
     * //     'CONTRATOS CONGELADOS' => 5,
     * //     'CARTERA ACTUAL' => 10,
     * //     'CONTRATOS NUEVOS' => 3,
     * //     'META' => 100,
     * //     'COBRADO TOTAL' => 50,
     * //     'COBRADO META' => 30,
     * //     'COBRADO META DE MAS' => 20,
     * //     'COBRADO CONTRATOS NUEVOS' => 15,
     * //     'COBRADO CONTRATOS EXTRA' => 10
     * // ]
     * ```
     *
     * @example Caso: Claves Faltantes en `$row`
     * ```php
     * $row = [
     *     'CONTRATOS CONGELADOS' => 5,
     *     'CARTERA ACTUAL' => 10
     * ];
     * $totales = [
     *     'CONTRATOS CONGELADOS' => 2,
     *     'CARTERA ACTUAL' => 8,
     *     'CONTRATOS NUEVOS' => 0,
     *     'META' => 50
     * ];
     * $resultado = $obj->ajusta_totales($row, $totales);
     * // Resultado:
     * // [
     * //     'CONTRATOS CONGELADOS' => 7,
     * //     'CARTERA ACTUAL' => 18,
     * //     'CONTRATOS NUEVOS' => 0,
     * //     'META' => 50,
     * //     'COBRADO TOTAL' => 0,
     * //     'COBRADO META' => 0,
     * //     'COBRADO META DE MAS' => 0,
     * //     'COBRADO CONTRATOS NUEVOS' => 0,
     * //     'COBRADO CONTRATOS EXTRA' => 0
     * // ]
     * ```
     *
     * @example Error: Claves Faltantes en `$totales`
     * ```php
     * $row = [
     *     'CONTRATOS CONGELADOS' => 5,
     *     'CARTERA ACTUAL' => 10,
     *     'META' => 100
     * ];
     * $totales = [];
     * $resultado = $obj->ajusta_totales($row, $totales);
     * // Resultado:
     * // [
     * //     'CONTRATOS CONGELADOS' => 5,
     * //     'CARTERA ACTUAL' => 10,
     * //     'CONTRATOS NUEVOS' => 0,
     * //     'META' => 100,
     * //     'COBRADO TOTAL' => 0,
     * //     'COBRADO META' => 0,
     * //     'COBRADO META DE MAS' => 0,
     * //     'COBRADO CONTRATOS NUEVOS' => 0,
     * //     'COBRADO CONTRATOS EXTRA' => 0
     * // ]
     * ```
     */
    private function ajusta_totales(array $row, array $totales): array
    {
        $keys_val = [
            'CONTRATOS CONGELADOS',
            'CARTERA ACTUAL',
            'CONTRATOS NUEVOS',
            'META',
            'COBRADO TOTAL',
            'COBRADO META',
            'COBRADO META DE MAS',
            'COBRADO CONTRATOS NUEVOS',
            'COBRADO CONTRATOS EXTRA'
        ];

        foreach ($keys_val as $key) {
            if (!isset($row[$key])) {
                $row[$key] = 0.0;
            }
            if (!isset($totales[$key])) {
                $totales[$key] = 0.0;
            }
            $totales[$key] += round($row[$key], 2);
        }

        return $totales;
    }


    /**
     * EM3
     * Asigna el valor de 'COBRADO TOTAL' a 'COBRADO CONTRATOS EXTRA' en una fila específica.
     *
     * Esta función toma una fila de datos y un índice correspondiente dentro de `$rows_out`.
     * Si 'COBRADO TOTAL' no está definido en `$row`, se inicializa a 0. Luego, se asigna
     * su valor a la clave 'COBRADO CONTRATOS EXTRA' en la fila correspondiente dentro de `$rows_out`.
     *
     * @param int $indice Índice de la fila en `$rows_out` que se actualizará. Debe ser mayor o igual a 0.
     * @param array $row Fila de datos que contiene la clave 'COBRADO TOTAL'.
     * @param array $rows_out Array de filas donde se asignará el valor de 'COBRADO TOTAL' en la posición `$indice`.
     *
     * @return array Devuelve el array `$rows_out` actualizado con el valor de 'COBRADO CONTRATOS EXTRA'.
     *               Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Caso Exitoso: Asignación de valor
     * ```php
     * $indice = 0;
     * $row = ['COBRADO TOTAL' => 1000];
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     *     ['GESTOR ID' => '456', 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->asigna_cobrado_total($indice, $row, $rows_out);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 1000],
     * //     ['GESTOR ID' => '456', 'COBRADO CONTRATOS EXTRA' => 0],
     * // ]
     * ```
     *
     * @example Caso: 'COBRADO TOTAL' no definido
     * ```php
     * $indice = 1;
     * $row = [];
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     *     ['GESTOR ID' => '456', 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->asigna_cobrado_total($indice, $row, $rows_out);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     * //     ['GESTOR ID' => '456', 'COBRADO CONTRATOS EXTRA' => 0],
     * // ]
     * ```
     *
     * @example Error: Índice negativo
     * ```php
     * $indice = -1;
     * $row = ['COBRADO TOTAL' => 1000];
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->asigna_cobrado_total($indice, $row, $rows_out);
     * // Resultado:
     * // [
     * //     'error' => 'Error indice debe ser mayor o igual a 0',
     * //     'data' => -1
     * // ]
     * ```
     */
    private function asigna_cobrado_total(int $indice, array $row, array $rows_out): array
    {
        // Validar que el índice sea mayor o igual a 0
        if ($indice < 0) {
            return (new error())->error('Error indice debe ser mayor o igual a 0', $indice);
        }

        // Inicializar 'COBRADO TOTAL' si no está definido
        if (!isset($row['COBRADO TOTAL'])) {
            $row['COBRADO TOTAL'] = 0;
        }

        // Asignar 'COBRADO TOTAL' a 'COBRADO CONTRATOS EXTRA' en la fila correspondiente
        $rows_out[$indice]['COBRADO CONTRATOS EXTRA'] = $row['COBRADO TOTAL'];

        return $rows_out;
    }


    /**
     * EM3
     * Agrega los datos del gestor a una fila.
     *
     * Esta función valida que los datos del gestor y su ID no estén vacíos y los agrega al array `$row_new`.
     * Si alguno de los valores está vacío, devuelve un error.
     *
     * @param string $gestor Nombre del gestor.
     * @param string $gestor_id Identificador único del gestor.
     * @param array $row_new Fila de datos donde se agregarán los valores del gestor.
     *
     * @return array Devuelve el array `$row_new` actualizado con los datos del gestor.
     *               Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Uso exitoso
     * ```php
     * $gestor = 'Juan Pérez';
     * $gestor_id = '123';
     * $row_new = ['PLAZA' => 'Zona Norte'];
     *
     * $resultado = $this->data_gestor($gestor, $gestor_id, $row_new);
     * // Resultado:
     * // [
     * //     'PLAZA' => 'Zona Norte',
     * //     'GESTOR ID' => '123',
     * //     'GESTOR' => 'Juan Pérez'
     * // ]
     * ```
     *
     * @example Error: Gestor vacío
     * ```php
     * $gestor = '';
     * $gestor_id = '123';
     * $row_new = ['PLAZA' => 'Zona Norte'];
     *
     * $resultado = $this->data_gestor($gestor, $gestor_id, $row_new);
     * // Resultado:
     * // [
     * //     'error' => 'Error gestor esta vacía',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Gestor ID vacío
     * ```php
     * $gestor = 'Juan Pérez';
     * $gestor_id = '';
     * $row_new = ['PLAZA' => 'Zona Norte'];
     *
     * $resultado = $this->data_gestor($gestor, $gestor_id, $row_new);
     * // Resultado:
     * // [
     * //     'error' => 'Error $gestor_id esta vacía',
     * //     'data' => ''
     * // ]
     * ```
     */
    private function data_gestor(string $gestor, string $gestor_id, array $row_new): array
    {
        $gestor = trim($gestor);
        $gestor_id = trim($gestor_id);

        // Validación de datos del gestor
        if ($gestor === '') {
            return (new error())->error('Error gestor está vacía', $gestor);
        }

        if ($gestor_id === '') {
            return (new error())->error('Error $gestor_id está vacía', $gestor_id);
        }

        // Agregar los datos al array
        $row_new['GESTOR ID'] = $gestor_id;
        $row_new['GESTOR'] = $gestor;

        return $row_new;
    }

    /**
     * EM3
     * Agrega los datos de la meta a una fila.
     *
     * Esta función valida la existencia de las claves requeridas en `$r_meta` y asigna valores predeterminados
     * si no están definidas. Luego, agrega los valores de meta al array `$row_new`.
     *
     * @param array $r_meta Datos relacionados con la meta. Debe incluir las claves:
     *                      - `'plaza_descripcion'`: Descripción de la plaza.
     *                      - `'meta_gestor_fecha_inicio'`: Fecha de inicio de la meta.
     *                      - `'meta_gestor_fecha_fin'`: Fecha de fin de la meta.
     * @param array $row_new Fila de datos donde se agregarán los valores de la meta.
     *
     * @return array Devuelve el array `$row_new` actualizado con los datos de la meta.
     *
     * @example Caso exitoso: Datos completos de meta
     * ```php
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     * $row_new = ['GESTOR ID' => '123'];
     *
     * $resultado = $this->data_meta($r_meta, $row_new);
     * // Resultado:
     * // [
     * //     'GESTOR ID' => '123',
     * //     'PLAZA' => 'Zona Norte',
     * //     'FECHA INICIO' => '2025-01-01',
     * //     'FECHA FIN' => '2025-01-31'
     * // ]
     * ```
     *
     * @example Caso: Datos incompletos de meta
     * ```php
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte'
     * ];
     * $row_new = ['GESTOR ID' => '123'];
     *
     * $resultado = $this->data_meta($r_meta, $row_new);
     * // Resultado:
     * // [
     * //     'GESTOR ID' => '123',
     * //     'PLAZA' => 'Zona Norte',
     * //     'FECHA INICIO' => 'SIN FECHA INICIAL',
     * //     'FECHA FIN' => 'SIN FECHA FINAL'
     * // ]
     * ```
     */
    private function data_meta(array $r_meta, array $row_new): array
    {
        // Validación y asignación de valores predeterminados
        $plaza_descripcion = isset($r_meta['plaza_descripcion'])
            ? trim($r_meta['plaza_descripcion']) : 'SIN PLAZA';
        $meta_gestor_fecha_inicio = isset($r_meta['meta_gestor_fecha_inicio'])
            ? trim($r_meta['meta_gestor_fecha_inicio']) : 'SIN FECHA INICIAL';
        $meta_gestor_fecha_fin = isset($r_meta['meta_gestor_fecha_fin'])
            ? trim($r_meta['meta_gestor_fecha_fin']) : 'SIN FECHA FINAL';

        // Asignación de datos al array
        $row_new['PLAZA'] = $plaza_descripcion;
        $row_new['FECHA INICIO'] = $meta_gestor_fecha_inicio;
        $row_new['FECHA FIN'] = $meta_gestor_fecha_fin;

        return $row_new;
    }

    /**
     * EM3
     * Ejecuta una consulta SQL de forma segura y gestiona posibles errores.
     *
     * Valida que la consulta SQL no esté vacía antes de ejecutarla. Si ocurre un error durante
     * la ejecución, retorna un mensaje con el contexto proporcionado para facilitar el diagnóstico.
     *
     * @param PDO $link Conexión activa a la base de datos.
     * @param string $sql La consulta SQL que se ejecutará.
     * @param string $contexto Descripción del contexto donde se ejecuta la consulta (para identificar errores).
     *
     * @return array Devuelve el resultado de la consulta en caso de éxito. Si ocurre un error, retorna
     *               un array con los detalles del mismo.
     *
     * @example Uso exitoso
     * ```php
     * $link = new PDO('mysql:host=localhost;dbname=mi_base', 'usuario', 'contraseña');
     * $sql = "SELECT * FROM tabla";
     * $contexto = 'Obtener datos de tabla';
     *
     * $resultado = $this->ejecuta_consulta_segura($link, $sql, $contexto);
     * // Resultado esperado: ['registros' => [...]]
     * ```
     *
     * @example Error: SQL vacío
     * ```php
     * $link = new PDO('mysql:host=localhost;dbname=mi_base', 'usuario', 'contraseña');
     * $sql = '';
     * $contexto = 'Intento de consulta con SQL vacío';
     *
     * $resultado = $this->ejecuta_consulta_segura($link, $sql, $contexto);
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error sql está vacío',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Fallo en la ejecución de la consulta
     * ```php
     * $link = new PDO('mysql:host=localhost;dbname=mi_base', 'usuario', 'contraseña');
     * $sql = "SELECT * FROM tabla_inexistente";
     * $contexto = 'Consulta con tabla inexistente';
     *
     * $resultado = $this->ejecuta_consulta_segura($link, $sql, $contexto);
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error al ejecutar consulta en Consulta con tabla inexistente',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function ejecuta_consulta_segura(PDO $link, string $sql, string $contexto): array {
        // Validar que la consulta SQL no esté vacía
        if (trim($sql) === '') {
            return (new error())->error('Error sql está vacío', $sql);
        }

        // Ejecutar la consulta de forma segura
        $resultado = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if (error::$en_error) {
            return (new error())->error("Error al ejecutar consulta en $contexto", $resultado);
        }

        return $resultado;
    }

    /**
     * EM3
     * Verifica si un cliente existe en una lista de filas.
     *
     * Esta función compara el valor del campo `<entidad_empleado>_id` en `$cliente` con el campo `'GESTOR ID'`
     * en cada fila de `$rows`. Si encuentra una coincidencia, devuelve `true`. Si no encuentra ninguna coincidencia, devuelve `false`.
     *
     * @param array $cliente Array que contiene los datos del cliente. Debe incluir la clave `<entidad_empleado>_id`.
     * @param string $entidad_empleado Nombre de la entidad del empleado (por ejemplo, `'ohem'` o `'empleado'`). Por defecto, `'ohem'`.
     * @param array $rows Array de filas donde se buscará el cliente. Cada fila debe contener la clave `'GESTOR ID'`.
     *
     * @return bool Devuelve `true` si se encuentra una coincidencia entre `<entidad_empleado>_id` en `$cliente` y `'GESTOR ID'` en `$rows`.
     *              Devuelve `false` si no se encuentra ninguna coincidencia.
     *
     * @example Caso exitoso: Cliente existe
     * ```php
     * $cliente = ['ohem_id' => '123'];
     * $entidad_empleado = 'ohem';
     * $rows = [
     *     ['GESTOR ID' => '123'],
     *     ['GESTOR ID' => '456']
     * ];
     * $resultado = $this->existe_cliente($cliente, $entidad_empleado, $rows);
     * // Resultado: true
     * ```
     *
     * @example Caso exitoso: Cliente no existe
     * ```php
     * $cliente = ['ohem_id' => '789'];
     * $entidad_empleado = 'ohem';
     * $rows = [
     *     ['GESTOR ID' => '123'],
     *     ['GESTOR ID' => '456']
     * ];
     * $resultado = $this->existe_cliente($cliente, $entidad_empleado, $rows);
     * // Resultado: false
     * ```
     *
     * @example Caso exitoso: Entidad personalizada
     * ```php
     * $cliente = ['empleado_id' => '456'];
     * $entidad_empleado = 'empleado';
     * $rows = [
     *     ['GESTOR ID' => '123'],
     *     ['GESTOR ID' => '456']
     * ];
     * $resultado = $this->existe_cliente($cliente, $entidad_empleado, $rows);
     * // Resultado: true
     * ```
     *
     * @example Caso: Clave no presente en `$cliente`
     * ```php
     * $cliente = ['nombre' => 'Juan'];
     * $entidad_empleado = 'empleado';
     * $rows = [
     *     ['GESTOR ID' => '123'],
     *     ['GESTOR ID' => '456']
     * ];
     * $resultado = $this->existe_cliente($cliente, $entidad_empleado, $rows);
     * // Resultado: false
     * ```
     */
    private function existe_cliente(array $cliente, string $entidad_empleado, array $rows): bool
    {
        $entidad_empleado = trim($entidad_empleado);
        if ($entidad_empleado === '') {
            $entidad_empleado = 'ohem';
        }
        $key_empleado_id = $entidad_empleado . '_id';
        $existe_cliente = false;
        foreach ($rows as $row) {
            if (!isset($cliente[$key_empleado_id])) {
                continue;
            }
            if (!isset($row['GESTOR ID'])) {
                continue;
            }
            if ((string)$cliente[$key_empleado_id] === (string)$row['GESTOR ID']) {
                $existe_cliente = true;
                break;
            }
        }
        return $existe_cliente;
    }

    /**
     * EM3
     * Obtiene el identificador del gestor asociado a un cliente.
     *
     * Esta función devuelve el valor de `<entidad_empleado>_id` dentro del array `$cliente`.
     * Si no existe la clave o el valor está vacío, devuelve `'S/A'` como valor predeterminado.
     *
     * @param array $cliente Array que contiene los datos del cliente. Puede incluir la clave `<entidad_empleado>_id`.
     * @param string $entidad_empleado (Opcional) Nombre de la entidad del empleado. Por defecto, es `'ohem'`.
     *                                  El campo buscado será `<entidad_empleado>_id`.
     *
     * @return string|int Devuelve el valor de `<entidad_empleado>_id` si existe y no está vacío.
     *                Si no existe la clave o el valor está vacío, devuelve `'S/A'`.
     *
     * @example Caso Exitoso: Cliente con Gestor ID
     * ```php
     * $cliente = ['ohem_id' => '123'];
     * $resultado = $obj->gestor_id($cliente);
     * // Resultado: '123'
     * ```
     *
     * @example Caso: Cliente sin Gestor ID
     * ```php
     * $cliente = ['ohem_name' => 'Juan'];
     * $resultado = $obj->gestor_id($cliente);
     * // Resultado: 'S/A'
     * ```
     *
     * @example Caso Exitoso: Entidad Empleado Personalizada
     * ```php
     * $cliente = ['empleado_id' => '456'];
     * $resultado = $obj->gestor_id($cliente, 'empleado');
     * // Resultado: '456'
     * ```
     *
     * @example Caso: Cliente con Clave Vacía
     * ```php
     * $cliente = ['ohem_id' => ''];
     * $resultado = $obj->gestor_id($cliente);
     * // Resultado: 'S/A'
     * ```
     */
    private function gestor_id(array $cliente, string $entidad_empleado = 'ohem')
    {
        $entidad_empleado = trim($entidad_empleado);
        if ($entidad_empleado === '') {
            $entidad_empleado = 'ohem';
        }
        $key_empleado_id = $entidad_empleado . '_id';
        $gestor_id = 'S/A';
        if (isset($cliente[$key_empleado_id]) && $cliente[$key_empleado_id] !== '') {
            $gestor_id = $cliente[$key_empleado_id];
        }
        return $gestor_id;
    }

    /**
     * EM3
     * Obtiene el valor correspondiente a una clave específica dentro de una fila de datos.
     *
     * Esta función realiza los siguientes pasos:
     * - Valida que la clave no esté vacía.
     * - Obtiene la lista de claves que deben inicializarse en cero (`keys_cero`).
     * - Calcula o asigna el valor correspondiente utilizando la función `value`.
     *
     * @param string $key Clave cuyo valor se desea obtener.
     * @param array $row Fila de datos que contiene los valores necesarios.
     *
     * @return array|float|string Devuelve el valor correspondiente a la clave proporcionada. Si ocurre un error,
     *               devuelve un array con detalles del error.
     *
     * @example Caso exitoso: Valor asociado a una clave
     * ```php
     * $key = 'COBRADO TOTAL';
     * $row = ['COBRADO TOTAL' => 100];
     * $resultado = $this->get_value($key, $row);
     * // Resultado: 100
     * ```
     *
     * @example Caso exitoso: Clave con valor inicializado en 0
     * ```php
     * $key = 'CONTRATOS CONGELADOS';
     * $row = [];
     * $resultado = $this->get_value($key, $row);
     * // Resultado: 0
     * ```
     *
     * @example Error: Clave vacía
     * ```php
     * $key = '';
     * $row = [];
     * $resultado = $this->get_value($key, $row);
     * // Resultado:
     * // [
     * //     'error' => 'Error $key está vacío',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Fallo al obtener `keys_cero`
     * ```php
     * // Suponiendo que keys_cero tiene un error
     * $key = 'CONTRATOS CONGELADOS';
     * $row = [];
     * $resultado = $this->get_value($key, $row);
     * // Resultado:
     * // [
     * //     'error' => 'Error al obtener $keys_cero',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function get_value(string $key, array $row)
    {
        // Validación de clave vacía
        $key = trim($key);
        if ($key === '') {
            return (new error())->error('Error $key está vacío', $key);
        }

        // Obtener claves que se inicializan en cero
        $keys_cero = $this->keys_cero();
        if (error::$en_error) {
            return (new error())->error('Error al obtener $keys_cero', $keys_cero);
        }

        // Calcular o asignar el valor correspondiente
        $value = $this->value($key, $keys_cero, $row);
        if (error::$en_error) {
            return (new error())->error('Error al obtener valor para $key', $value);
        }

        return $value;
    }


    /**
     * EM3
     * Obtiene las claves que deben inicializarse en cero.
     *
     * Esta función devuelve un array con los nombres de las claves que se inicializan
     * en cero en las estructuras de datos procesadas.
     *
     * @return array Devuelve un array con las claves inicializadas en cero.
     *
     * @example Uso
     * ```php
     * $keys = $this->keys_cero();
     * // Resultado:
     * // [
     * //     'CONTRATOS CONGELADOS',
     * //     'CARTERA ACTUAL',
     * //     'CONTRATOS NUEVOS',
     * //     'META',
     * //     'COBRADO TOTAL',
     * //     'COBRADO META',
     * //     'COBRADO META DE MAS',
     * //     'COBRADO CONTRATOS NUEVOS',
     * //     'COBRADO CONTRATOS EXTRA',
     * //     'PORCENTAJE META GENERAL',
     * //     'PORCENTAJE META REAL'
     * // ]
     * ```
     */
    private function keys_cero(): array
    {
        return [
            'CONTRATOS CONGELADOS',
            'CARTERA ACTUAL',
            'CONTRATOS NUEVOS',
            'META',
            'COBRADO TOTAL',
            'COBRADO META',
            'COBRADO META DE MAS',
            'COBRADO CONTRATOS NUEVOS',
            'COBRADO CONTRATOS EXTRA',
            'PORCENTAJE META GENERAL',
            'PORCENTAJE META REAL',
        ];
    }

    /**
     * EM3
     * Inicializa un array de totales con valores predeterminados y claves específicas.
     *
     * Esta función combina las operaciones de inicialización básica y asignación de valores iniciales
     * para un array de totales. Asegura que el array esté estructurado correctamente y que todas las claves
     * necesarias estén presentes con valores iniciales.
     *
     * @return array Devuelve el array de totales inicializado con todas las claves necesarias y valores predeterminados.
     *               Si ocurre un error durante la inicialización, devuelve un array con detalles del error.
     *
     * @example Uso Exitoso
     * ```php
     * $resultado = $obj->inicializa_totales();
     * // Resultado:
     * // [
     * //     'CONTRATOS CONGELADOS' => 0,
     * //     'CARTERA ACTUAL' => 0,
     * //     'CONTRATOS NUEVOS' => 0,
     * //     'META' => 0,
     * //     'COBRADO TOTAL' => 0,
     * //     'COBRADO META' => 0,
     * //     'COBRADO META DE MAS' => 0,
     * //     'COBRADO CONTRATOS NUEVOS' => 0,
     * //     'COBRADO CONTRATOS EXTRA' => 0,
     * // ]
     * ```
     *
     * @example Error en Inicialización Básica
     * ```php
     * // Supongamos que `init_totales()` retorna un error.
     * $resultado = $obj->inicializa_totales();
     * // Resultado:
     * // [
     * //     'error' => 'Error al obtener $totales',
     * //     'data' => [...] // Detalles del error generado en init_totales()
     * // ]
     * ```
     *
     * @example Error en Asignación de Valores Iniciales
     * ```php
     * // Supongamos que `init_totales_0()` retorna un error.
     * $resultado = $obj->inicializa_totales();
     * // Resultado:
     * // [
     * //     'error' => 'Error al obtener $totales',
     * //     'data' => [...] // Detalles del error generado en init_totales_0()
     * // ]
     * ```
     */
    private function inicializa_totales(): array
    {
        $totales = $this->init_totales();
        if (error::$en_error) {
            return (new error())->error('Error al obtener $totales', $totales);
        }

        $totales = $this->init_totales_0($totales);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $totales', $totales);
        }

        return $totales;
    }

    /**
     * EM3
     * Integra una nueva fila basada en los datos de un cliente y su meta asociada.
     *
     * Esta función valida los datos proporcionados en `$cliente` y `$r_meta`, obtiene el ID del gestor asociado al cliente,
     * y genera una nueva fila basada en los datos validados. Si ocurre algún error durante el proceso, devuelve un array
     * con detalles del error.
     *
     * @param array $cliente Array que contiene los datos del cliente. Debe incluir:
     *                       - `'GESTOR'` (opcional): Nombre del gestor. Si no está definido, se asigna `'S/A'`.
     *                       - `<entidad_empleado>_id`: ID del gestor asociado.
     *                       - `'n_contratos'`: Número total de contratos del cliente.
     *                       - `'n_contratos_nuevos'`: Número de contratos nuevos del cliente.
     * @param string $entidad_empleado Nombre de la entidad del empleado (por ejemplo, `'ohem'` o `'empleado'`).
     *                                  Este valor se utiliza para construir la clave `<entidad_empleado>_id` en `$cliente`.
     * @param array $r_meta Array que contiene los datos de la meta asociada. Debe incluir:
     *                      - `'plaza_descripcion'`: Descripción de la plaza.
     *                      - `'meta_gestor_fecha_inicio'`: Fecha de inicio de la meta.
     *                      - `'meta_gestor_fecha_fin'`: Fecha de fin de la meta.
     *
     * @return array Devuelve un array que representa la nueva fila integrada con los datos del cliente y la meta.
     *               Si ocurre un error, devuelve un array con detalles del mismo.
     *
     * @example Caso exitoso: Cliente con todos los datos
     * ```php
     * $cliente = [
     *     'GESTOR' => 'Juan Pérez',
     *     'ohem_id' => '123',
     *     'n_contratos' => 10,
     *     'n_contratos_nuevos' => 3
     * ];
     * $entidad_empleado = 'ohem';
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     *
     * $resultado = $this->integra_row_new($cliente, $entidad_empleado, $r_meta);
     * // Resultado:
     * // [
     * //     'PLAZA' => 'Zona Norte',
     * //     'FECHA INICIO' => '2025-01-01',
     * //     'FECHA FIN' => '2025-01-31',
     * //     'GESTOR ID' => '123',
     * //     'GESTOR' => 'Juan Pérez',
     * //     'CARTERA ACTUAL' => 10,
     * //     'CONTRATOS NUEVOS' => 3,
     * //     ...
     * // ]
     * ```
     *
     * @example Error: Falta un campo en `$r_meta`
     * ```php
     * $cliente = [
     *     'GESTOR' => 'Juan Pérez',
     *     'ohem_id' => '123',
     *     'n_contratos' => 10,
     *     'n_contratos_nuevos' => 3
     * ];
     * $entidad_empleado = 'ohem';
     * $r_meta = [
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     *
     * $resultado = $this->integra_row_new($cliente, $entidad_empleado, $r_meta);
     * // Resultado:
     * // [
     * //     'error' => 'Error $r_meta[plaza_descripcion] no existe',
     * //     'data' => [...]
     * // ]
     * ```
     *
     * @example Error: Falta el ID del gestor en `$cliente`
     * ```php
     * $cliente = [
     *     'GESTOR' => 'Juan Pérez',
     *     'n_contratos' => 10,
     *     'n_contratos_nuevos' => 3
     * ];
     * $entidad_empleado = 'ohem';
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     *
     * $resultado = $this->integra_row_new($cliente, $entidad_empleado, $r_meta);
     * // Resultado:
     * // [
     * //     'error' => 'Error al obtener $gestor_id',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function integra_row_new(array $cliente, string $entidad_empleado, array $r_meta): array
    {
        // Validar los datos del cliente y la meta
        $valida = $this->valida_row($cliente, $r_meta);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Obtener el ID del gestor asociado al cliente
        $gestor_id = $this->gestor_id($cliente, $entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $gestor_id', $gestor_id);
        }

        // Generar la nueva fila basada en los datos validados
        $row_new = $this->row_new($cliente, $gestor_id, $r_meta);
        if (error::$en_error) {
            return (new error())->error('Error al generar row new', $row_new);
        }

        return $row_new;
    }

    /**
     * EM3
     * Integra información de pagos en las filas existentes o genera nuevas filas si no existen coincidencias.
     *
     * Este método actualiza las filas de datos existentes con la información de los pagos proporcionados.
     * Si un gestor no existe en las filas, se crea una nueva fila basada en la información del pago y los datos
     * de la meta.
     *
     * @param string $entidad_empleado Nombre de la entidad del empleado, utilizado para construir el identificador
     *                                 del gestor en los pagos. Si está vacío, se asigna el valor predeterminado `'ohem'`.
     * @param array $pagos_rows Arreglo de pagos a integrar. Cada elemento debe ser un array que contenga:
     *                          - `'GESTOR'`: Nombre del gestor (opcional, valor predeterminado `'S/A'`).
     *                          - `<entidad_empleado>_id`: Identificador del gestor (opcional, valor predeterminado `'S/A'`).
     *                          - `'COBRADO_GENERAL'`: Monto cobrado total (opcional, valor predeterminado `0`).
     * @param array $r_meta Arreglo con los datos de la meta. Debe incluir:
     *                      - `'plaza_descripcion'`: Descripción de la plaza.
     *                      - `'meta_gestor_fecha_inicio'`: Fecha de inicio de la meta.
     *                      - `'meta_gestor_fecha_fin'`: Fecha de fin de la meta.
     * @param array $rows Arreglo de filas existentes para integrar la información de pagos.
     *
     * @return array Devuelve las filas actualizadas o ampliadas con los datos de los pagos. Si ocurre un error,
     *               devuelve un array con detalles del error.
     *
     * @example Caso exitoso: Integración de pagos
     * ```php
     * $entidad_empleado = 'ohem';
     * $pagos_rows = [
     *     [
     *         'GESTOR' => 'Juan Pérez',
     *         'ohem_id' => '123',
     *         'COBRADO_GENERAL' => 1500
     *     ],
     *     [
     *         'GESTOR' => 'Ana López',
     *         'ohem_id' => '456',
     *         'COBRADO_GENERAL' => 2000
     *     ]
     * ];
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     * $rows = [
     *     [
     *         'GESTOR ID' => '123',
     *         'GESTOR' => 'Juan Pérez',
     *         'COBRADO TOTAL' => 1000
     *     ]
     * ];
     *
     * $resultado = $this->integra_row_pagos($entidad_empleado, $pagos_rows, $r_meta, $rows);
     * // Resultado:
     * // [
     * //     [
     * //         'GESTOR ID' => '123',
     * //         'GESTOR' => 'Juan Pérez',
     * //         'COBRADO TOTAL' => 1500
     * //     ],
     * //     [
     * //         'PLAZA' => 'Zona Norte',
     * //         'FECHA INICIO' => '2025-01-01',
     * //         'FECHA FIN' => '2025-01-31',
     * //         'GESTOR ID' => '456',
     * //         'GESTOR' => 'Ana López',
     * //         'CARTERA ACTUAL' => 0,
     * //         'CONTRATOS NUEVOS' => 0,
     * //         'COBRADO TOTAL' => 2000
     * //     ]
     * // ]
     * ```
     *
     * @example Caso: Datos de pago incompletos
     * ```php
     * $entidad_empleado = 'empleado';
     * $pagos_rows = [
     *     [
     *         'GESTOR' => 'Carlos Ruiz',
     *         'empleado_id' => ''
     *     ]
     * ];
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Centro',
     *     'meta_gestor_fecha_inicio' => '2025-02-01',
     *     'meta_gestor_fecha_fin' => '2025-02-28'
     * ];
     * $rows = [];
     *
     * $resultado = $this->integra_row_pagos($entidad_empleado, $pagos_rows, $r_meta, $rows);
     * // Resultado:
     * // [
     * //     'error' => 'Error al integrar $row_new',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function integra_row_pagos(
        string $entidad_empleado,
        array $pagos_rows,
        array $r_meta,
        array $rows
    ): array {
        $entidad_empleado = trim($entidad_empleado) ?: 'ohem';
        $key_empleado_id = "{$entidad_empleado}_id";

        foreach ($pagos_rows as $pago) {
            // Validación del formato del pago
            if (!is_array($pago)) {
                return (new error())->error('Error $pago debe ser un array', $pago);
            }

            // Valores predeterminados para claves opcionales
            $pago['GESTOR'] = $pago['GESTOR'] ?? 'S/A';
            $pago[$key_empleado_id] = $pago[$key_empleado_id] ?? 'S/A';
            $pago['COBRADO_GENERAL'] = $pago['COBRADO_GENERAL'] ?? 0;

            // Verificar si el gestor ya existe en las filas
            $existe_pago = false;
            foreach ($rows as &$row) {
                if ((string)$pago[$key_empleado_id] === (string)$row['GESTOR ID']) {
                    $row['COBRADO TOTAL'] = $pago['COBRADO_GENERAL'];
                    $existe_pago = true;
                    break;
                }
            }

            // Si no existe, se crea una nueva fila
            if (!$existe_pago) {
                $row_new = $this->row_new_base($pago['GESTOR'], $pago[$key_empleado_id], $r_meta);
                if (error::$en_error) {
                    return (new error())->error('Error al integrar $row_new', $row_new);
                }

                $row_new['CARTERA ACTUAL'] = 0;
                $row_new['CONTRATOS NUEVOS'] = 0;
                $row_new['COBRADO TOTAL'] = $pago['COBRADO_GENERAL'];
                $rows[] = $row_new;
            }
        }

        return $rows;
    }



    /**
     * EM3
     * Inicializa los datos de las filas de gestores con información de un cliente.
     *
     * Esta función busca en las filas de gestores (`$rows`) una coincidencia con el gestor asociado a un cliente
     * utilizando el campo `<entidad_empleado>_id`. Si encuentra una coincidencia, actualiza las filas con
     * los valores de `n_contratos` y `n_contratos_nuevos` provenientes del cliente.
     *
     * @param array $cliente Array que contiene los datos del cliente, incluyendo `<entidad_empleado>_id`,
     *                       `n_contratos` y `n_contratos_nuevos`.
     * @param string $entidad_empleado Nombre de la entidad del empleado (por defecto, `'ohem'`).
     *                                  El campo buscado será `<entidad_empleado>_id`.
     * @param array $rows Array que contiene las filas de datos de gestores, cada fila con un campo `'GESTOR ID'`.
     *
     * @return array Devuelve el array `$rows` actualizado con los datos de `n_contratos` y `n_contratos_nuevos`
     *               si hay coincidencias. Si no hay coincidencias, las filas originales se devuelven sin cambios.
     *
     * @example Caso Exitoso: Cliente con datos para actualizar
     * ```php
     * $cliente = [
     *     'ohem_id' => '123',
     *     'n_contratos' => 5,
     *     'n_contratos_nuevos' => 2
     * ];
     * $entidad_empleado = 'ohem';
     * $rows = [
     *     ['GESTOR ID' => '123', 'CARTERA ACTUAL' => 0, 'CONTRATOS NUEVOS' => 0],
     *     ['GESTOR ID' => '456', 'CARTERA ACTUAL' => 0, 'CONTRATOS NUEVOS' => 0]
     * ];
     *
     * $resultado = $this->init_data_row_gestor($cliente, $entidad_empleado, $rows);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'CARTERA ACTUAL' => 5, 'CONTRATOS NUEVOS' => 2],
     * //     ['GESTOR ID' => '456', 'CARTERA ACTUAL' => 0, 'CONTRATOS NUEVOS' => 0]
     * // ]
     * ```
     *
     * @example Caso: Cliente sin coincidencias en las filas
     * ```php
     * $cliente = [
     *     'ohem_id' => '789',
     *     'n_contratos' => 5,
     *     'n_contratos_nuevos' => 2
     * ];
     * $entidad_empleado = 'ohem';
     * $rows = [
     *     ['GESTOR ID' => '123', 'CARTERA ACTUAL' => 0, 'CONTRATOS NUEVOS' => 0],
     *     ['GESTOR ID' => '456', 'CARTERA ACTUAL' => 0, 'CONTRATOS NUEVOS' => 0]
     * ];
     *
     * $resultado = $this->init_data_row_gestor($cliente, $entidad_empleado, $rows);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'CARTERA ACTUAL' => 0, 'CONTRATOS NUEVOS' => 0],
     * //     ['GESTOR ID' => '456', 'CARTERA ACTUAL' => 0, 'CONTRATOS NUEVOS' => 0]
     * // ]
     * ```
     *
     * @example Caso: Cliente sin claves requeridas
     * ```php
     * $cliente = ['n_contratos' => 5];
     * $entidad_empleado = 'ohem';
     * $rows = [
     *     ['GESTOR ID' => '123', 'CARTERA ACTUAL' => 0, 'CONTRATOS NUEVOS' => 0]
     * ];
     *
     * $resultado = $this->init_data_row_gestor($cliente, $entidad_empleado, $rows);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'CARTERA ACTUAL' => 0, 'CONTRATOS NUEVOS' => 0]
     * // ]
     * ```
     */
    private function init_data_row_gestor(array $cliente, string $entidad_empleado, array $rows): array
    {
        $entidad_empleado = trim($entidad_empleado);
        if ($entidad_empleado === '') {
            $entidad_empleado = 'ohem';
        }
        $key_empleado_id = $entidad_empleado . '_id';
        foreach ($rows as $indice => $row) {

            if (!isset($cliente[$key_empleado_id])) {
                continue;
            }
            if (!isset($row['GESTOR ID'])) {
                continue;
            }
            if ((string)$cliente[$key_empleado_id] === (string)$row['GESTOR ID']) {
                if (!isset($cliente['n_contratos'])) {
                    $cliente['n_contratos'] = 0;
                }
                if (!isset($cliente['n_contratos_nuevos'])) {
                    $cliente['n_contratos_nuevos'] = 0;
                }
                $rows[$indice]['CARTERA ACTUAL'] = $cliente['n_contratos'];
                $rows[$indice]['CONTRATOS NUEVOS'] = $cliente['n_contratos_nuevos'];
                break;
            }
        }
        return $rows;
    }

    /**
     * EM3
     * Inicializa las filas con datos de metas y clientes.
     *
     * Esta función ejecuta consultas SQL para obtener datos de metas y clientes, valida los datos de la meta,
     * y combina ambas fuentes en una estructura de filas integrada.
     *
     * @param string $entidad_empleado Nombre de la entidad del empleado (por ejemplo, `'ohem'` o `'empleado'`).
     * @param PDO $link Conexión activa a la base de datos mediante PDO.
     * @param array $r_meta Datos relacionados con la meta que se procesará. Debe incluir:
     *                      - `'plaza_descripcion'`: Descripción de la plaza.
     *                      - `'meta_gestor_fecha_inicio'`: Fecha de inicio de la meta.
     *                      - `'meta_gestor_fecha_fin'`: Fecha de fin de la meta.
     * @param string $sql_clientes Consulta SQL para obtener los datos de clientes.
     * @param string $sql_metas Consulta SQL para obtener los datos de metas.
     *
     * @return array Devuelve un array con las filas integradas de metas y clientes. Si ocurre un error,
     *               devuelve un array con detalles del error.
     *
     * @example Uso exitoso
     * ```php
     * $entidad_empleado = 'ohem';
     * $link = new PDO('mysql:host=localhost;dbname=mi_base', 'usuario', 'contraseña');
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     * $sql_clientes = "SELECT * FROM clientes WHERE plaza_id = 1";
     * $sql_metas = "SELECT * FROM metas WHERE plaza_id = 1";
     *
     * $resultado = $this->init_rows($entidad_empleado, $link, $r_meta, $sql_clientes, $sql_metas);
     * // Resultado:
     * // [
     * //     [
     * //         'PLAZA' => 'Zona Norte',
     * //         'FECHA INICIO' => '2025-01-01',
     * //         'FECHA FIN' => '2025-01-31',
     * //         'GESTOR ID' => '123',
     * //         'GESTOR' => 'Juan Pérez',
     * //         'CARTERA ACTUAL' => 5,
     * //         ...
     * //     ]
     * // ]
     * ```
     *
     * @example Error: Consulta SQL de clientes vacía
     * ```php
     * $entidad_empleado = 'ohem';
     * $link = new PDO('mysql:host=localhost;dbname=mi_base', 'usuario', 'contraseña');
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     * $sql_clientes = '';
     * $sql_metas = "SELECT * FROM metas WHERE plaza_id = 1";
     *
     * $resultado = $this->init_rows($entidad_empleado, $link, $r_meta, $sql_clientes, $sql_metas);
     * // Resultado:
     * // [
     * //     'error' => 'Error: $sql_clientes está vacío',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Datos de meta incompletos
     * ```php
     * $entidad_empleado = 'ohem';
     * $link = new PDO('mysql:host=localhost;dbname=mi_base', 'usuario', 'contraseña');
     * $r_meta = [
     *     'meta_gestor_fecha_inicio' => '2025-01-01'
     * ];
     * $sql_clientes = "SELECT * FROM clientes WHERE plaza_id = 1";
     * $sql_metas = "SELECT * FROM metas WHERE plaza_id = 1";
     *
     * $resultado = $this->init_rows($entidad_empleado, $link, $r_meta, $sql_clientes, $sql_metas);
     * // Resultado:
     * // [
     * //     'error' => 'Error al validar datos de la meta',
     * //     'data' => [...]
     * // ]
     * ```
     *
     * @example Error: Consulta SQL de metas vacía
     * ```php
     * $entidad_empleado = 'ohem';
     * $link = new PDO('mysql:host=localhost;dbname=mi_base', 'usuario', 'contraseña');
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     * $sql_clientes = "SELECT * FROM clientes WHERE plaza_id = 1";
     * $sql_metas = '';
     *
     * $resultado = $this->init_rows($entidad_empleado, $link, $r_meta, $sql_clientes, $sql_metas);
     * // Resultado:
     * // [
     * //     'error' => 'Error: $sql_metas está vacío',
     * //     'data' => ''
     * // ]
     * ```
     */
    private function init_rows(
        string $entidad_empleado,
        PDO $link,
        array $r_meta,
        string $sql_clientes,
        string $sql_metas
    ): array {
        // Validación de las consultas SQL
        $sql_clientes = trim($sql_clientes);
        if (empty($sql_clientes)) {
            return (new error())->error('Error: $sql_clientes está vacío', $sql_clientes);
        }

        $sql_metas = trim($sql_metas);
        if (empty($sql_metas)) {
            return (new error())->error('Error: $sql_metas está vacío', $sql_metas);
        }

        // Validación de los datos de la meta
        $validacion_meta = $this->valida_meta($r_meta);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos de la meta', $validacion_meta);
        }

        // Ejecución de las consultas SQL
        $metas = $this->ejecuta_consulta_segura($link, $sql_metas, 'metas');
        if (error::$en_error) {
            return (new error())->error('Error al ejecutar consulta de metas', $metas);
        }

        $clientes = $this->ejecuta_consulta_segura($link, $sql_clientes, 'clientes');
        if (error::$en_error) {
            return (new error())->error('Error al ejecutar consulta de clientes', $clientes);
        }

        // Construcción inicial de las filas con datos de metas
        $rows = $metas['registros'];

        // Integración de datos de clientes en las filas
        $rows = $this->rows_cliente($clientes['registros'], $entidad_empleado, $r_meta, $rows);
        if (error::$en_error) {
            return (new error())->error('Error al integrar datos de clientes', $rows);
        }

        return $rows;
    }


    /**
     * EM3
     * Procesa y organiza un conjunto de filas basado en un orden específico de claves.
     *
     * Esta función toma un conjunto de filas, valida que cada una sea un array y las reorganiza
     * según las claves especificadas en `$order_keys`. Si una fila no es válida o ocurre un error,
     * devuelve un detalle del error. En caso de éxito, devuelve un array con las filas procesadas.
     *
     * @param array $rows Conjunto de filas a procesar. Cada fila debe ser un array asociativo.
     *
     * @return array Devuelve un array de filas reorganizadas. Si ocurre un error, devuelve un array con detalles del error.
     *
     * @example Caso Exitoso: Reorganización de filas
     * ```php
     * $rows = [
     *     [
     *         'GESTOR ID' => '123',
     *         'GESTOR' => 'Juan Pérez',
     *         'FECHA INICIO' => '2025-01-01',
     *         'META' => 500,
     *         'COBRADO TOTAL' => 250,
     *     ],
     *     [
     *         'GESTOR ID' => '456',
     *         'GESTOR' => 'Ana López',
     *         'FECHA INICIO' => '2025-02-01',
     *         'META' => 300,
     *         'COBRADO TOTAL' => 150,
     *     ],
     * ];
     *
     * $resultado = $obj->init_rows_out($rows);
     * // Resultado:
     * // [
     * //     [
     * //         'GESTOR ID' => '123',
     * //         'GESTOR' => 'Juan Pérez',
     * //         'FECHA INICIO' => '2025-01-01',
     * //         'FECHA FIN' => '',
     * //         'PLAZA' => '',
     * //         'CONTRATOS CONGELADOS' => 0,
     * //         'CARTERA ACTUAL' => 0,
     * //         ...
     * //     ],
     * //     [
     * //         'GESTOR ID' => '456',
     * //         'GESTOR' => 'Ana López',
     * //         'FECHA INICIO' => '2025-02-01',
     * //         'FECHA FIN' => '',
     * //         'PLAZA' => '',
     * //         'CONTRATOS CONGELADOS' => 0,
     * //         'CARTERA ACTUAL' => 0,
     * //         ...
     * //     ]
     * // ]
     * ```
     *
     * @example Caso Error: Fila no válida
     * ```php
     * $rows = [
     *     'GESTOR ID' => '123',
     *     'GESTOR' => 'Juan Pérez',
     *     'META' => 500,
     * ];
     *
     * $resultado = $obj->init_rows_out($rows);
     * // Resultado:
     * // [
     * //     'error' => 'Error $row debe ser un array',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function init_rows_out(array $rows): array
    {
        // Claves ordenadas para la salida
        $order_keys = [
            'GESTOR ID', 'GESTOR', 'FECHA INICIO', 'FECHA FIN', 'PLAZA',
            'CONTRATOS CONGELADOS', 'CARTERA ACTUAL', 'CONTRATOS NUEVOS', 'META',
            'COBRADO TOTAL', 'COBRADO META', 'COBRADO META DE MAS',
            'COBRADO CONTRATOS NUEVOS', 'COBRADO CONTRATOS EXTRA',
            'PORCENTAJE META GENERAL', 'PORCENTAJE META REAL'
        ];

        $rows_out = [];

        foreach ($rows as $row) {
            // Validar que cada fila sea un array
            if (!is_array($row)) {
                return (new error())->error('Error: $row debe ser un array', $rows);
            }

            // Procesar la fila y reorganizar según las claves ordenadas
            $row_new = $this->integra_keys($order_keys, $row, []);
            if (error::$en_error) {
                return (new error())->error('Error al integrar valores en $row_new', $row_new);
            }

            // Agregar la fila procesada al resultado
            $rows_out[] = $row_new;
        }

        return $rows_out;
    }


    /**
     * EM3
     * Inicializa un array de totales con valores vacíos.
     *
     * Esta función genera un array que contiene valores vacíos o predeterminados
     * según la estructura deseada. Se utiliza típicamente como base para cálculos
     * o para inicializar valores que serán completados posteriormente.
     *
     * @return array Devuelve un array inicializado con valores vacíos.
     *
     * @example Uso Exitoso
     * ```php
     * $resultado = $obj->init_totales();
     * // Resultado:
     * // [
     * //     '',
     * //     '',
     * //     '',
     * //     '',
     * //     ''
     * // ]
     * ```
     *
     * @example Caso: Utilizar los totales inicializados para cálculos
     * ```php
     * $totales = $obj->init_totales();
     * $totales[0] = 100;
     * $totales[1] = 200;
     * $totales[2] = 300;
     *
     * // Resultado:
     * // [
     * //     100,
     * //     200,
     * //     300,
     * //     '',
     * //     ''
     * // ]
     * ```
     */
    private function init_totales(): array
    {
        $totales = array();
        $totales[] = '';
        $totales[] = '';
        $totales[] = '';
        $totales[] = '';
        $totales[] = '';

        return $totales;
    }


    /**
     * EM3
     * Inicializa un array de totales con claves específicas asignadas a valores cero.
     *
     * Esta función toma un array existente y asigna valores iniciales de `0` a las claves
     * relacionadas con totales predefinidos, asegurando consistencia en la estructura de datos.
     *
     * @param array $totales Array en el que se inicializarán las claves con valores de `0`.
     *
     * @return array Devuelve el array con las claves de totales inicializadas a `0`.
     *
     * @example Uso Exitoso
     * ```php
     * $totales = [];
     * $resultado = $obj->init_totales_0($totales);
     * // Resultado:
     * // [
     * //     'CONTRATOS CONGELADOS' => 0,
     * //     'CARTERA ACTUAL' => 0,
     * //     'CONTRATOS NUEVOS' => 0,
     * //     'META' => 0,
     * //     'COBRADO TOTAL' => 0,
     * //     'COBRADO META' => 0,
     * //     'COBRADO META DE MAS' => 0,
     * //     'COBRADO CONTRATOS NUEVOS' => 0,
     * //     'COBRADO CONTRATOS EXTRA' => 0,
     * // ]
     * ```
     *
     * @example Caso: Modificar valores inicializados
     * ```php
     * $totales = [];
     * $totales = $obj->init_totales_0($totales);
     * $totales['CARTERA ACTUAL'] = 150;
     * $totales['META'] = 500;
     * // Resultado:
     * // [
     * //     'CONTRATOS CONGELADOS' => 0,
     * //     'CARTERA ACTUAL' => 150,
     * //     'CONTRATOS NUEVOS' => 0,
     * //     'META' => 500,
     * //     'COBRADO TOTAL' => 0,
     * //     'COBRADO META' => 0,
     * //     'COBRADO META DE MAS' => 0,
     * //     'COBRADO CONTRATOS NUEVOS' => 0,
     * //     'COBRADO CONTRATOS EXTRA' => 0,
     * // ]
     * ```
     *
     * @example Error: Array no inicializado
     * ```php
     * $totales = null; // Incorrecto
     * $resultado = $obj->init_totales_0($totales);
     * // Resultado:
     * // [
     * //     'CONTRATOS CONGELADOS' => 0,
     * //     'CARTERA ACTUAL' => 0,
     * //     ...
     * // ]
     * ```
     */
    private function init_totales_0(array $totales): array
    {
        $totales['CONTRATOS CONGELADOS'] = 0;
        $totales['CARTERA ACTUAL'] = 0;
        $totales['CONTRATOS NUEVOS'] = 0;
        $totales['META'] = 0;
        $totales['COBRADO TOTAL'] = 0;
        $totales['COBRADO META'] = 0;
        $totales['COBRADO META DE MAS'] = 0;
        $totales['COBRADO CONTRATOS NUEVOS'] = 0;
        $totales['COBRADO CONTRATOS EXTRA'] = 0;

        return $totales;
    }


    /**
     * EM3
     * Integra el valor de 'COBRADO TOTAL' en 'COBRADO CONTRATOS EXTRA' para una fila específica si es mayor a 0.
     *
     * Esta función valida que el índice proporcionado sea válido, inicializa 'COBRADO TOTAL' si no está definido
     * en `$row`, y si su valor es mayor a 0, utiliza la función `asigna_cobrado_total` para integrarlo en
     * 'COBRADO CONTRATOS EXTRA' dentro de `$rows_out`.
     *
     * @param int $indice Índice de la fila en `$rows_out` que se actualizará. Debe ser mayor o igual a 0.
     * @param array $row Fila de datos que contiene la clave 'COBRADO TOTAL'.
     * @param array $rows_out Array de filas donde se integrará el valor de 'COBRADO TOTAL' en la posición `$indice`.
     *
     * @return array Devuelve el array `$rows_out` actualizado si 'COBRADO TOTAL' es mayor a 0.
     *               Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Caso Exitoso: 'COBRADO TOTAL' mayor a 0
     * ```php
     * $indice = 0;
     * $row = ['COBRADO TOTAL' => 500.0];
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     *     ['GESTOR ID' => '456', 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->integra_cobrado_total($indice, $row, $rows_out);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 500.0],
     * //     ['GESTOR ID' => '456', 'COBRADO CONTRATOS EXTRA' => 0],
     * // ]
     * ```
     *
     * @example Caso: 'COBRADO TOTAL' no definido
     * ```php
     * $indice = 1;
     * $row = [];
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     *     ['GESTOR ID' => '456', 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->integra_cobrado_total($indice, $row, $rows_out);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     * //     ['GESTOR ID' => '456', 'COBRADO CONTRATOS EXTRA' => 0],
     * // ]
     * ```
     *
     * @example Caso Error: Índice negativo
     * ```php
     * $indice = -1;
     * $row = ['COBRADO TOTAL' => 1000.0];
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->integra_cobrado_total($indice, $row, $rows_out);
     * // Resultado:
     * // [
     * //     'error' => 'Error indice debe ser mayor o igual a 0',
     * //     'data' => -1
     * // ]
     * ```
     */
    private function integra_cobrado_total(int $indice, array $row, array $rows_out): array
    {
        // Validar que el índice sea mayor o igual a 0
        if ($indice < 0) {
            return (new error())->error('Error indice debe ser mayor o igual a 0', $indice);
        }

        // Inicializar 'COBRADO TOTAL' si no está definido
        if (!isset($row['COBRADO TOTAL'])) {
            $row['COBRADO TOTAL'] = 0.0;
        }

        // Integrar el valor si 'COBRADO TOTAL' es mayor a 0
        if ((float)$row['COBRADO TOTAL'] > 0.0) {
            $rows_out = $this->asigna_cobrado_total($indice, $row, $rows_out);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $rows_out', $rows_out);
            }
        }

        return $rows_out;
    }


    /**
     * EM3
     * Integra un conjunto de claves y sus valores desde una fila (`$row`) en otra fila (`$row_new`).
     *
     * Esta función recorre un conjunto de claves definidas en `$order_keys`, valida cada clave y
     * utiliza la función `integra_value_key` para integrar su valor desde `$row` en `$row_new`.
     * Si ocurre algún error durante el proceso, se devuelve un array con los detalles del error.
     *
     * @param array $order_keys Lista de claves a integrar en `$row_new`.
     * @param array $row Fila de datos de origen desde donde se obtendrán los valores.
     * @param array $row_new Fila de datos de destino donde se integrarán los valores.
     *
     * @return array Devuelve `$row_new` actualizado con los valores integrados. Si ocurre un error,
     *               devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Integración de múltiples claves
     * ```php
     * $order_keys = ['COBRADO TOTAL', 'CONTRATOS NUEVOS'];
     * $row = [
     *     'COBRADO TOTAL' => 100,
     *     'CONTRATOS NUEVOS' => 5
     * ];
     * $row_new = [];
     *
     * $resultado = $this->integra_keys($order_keys, $row, $row_new);
     * // Resultado:
     * // [
     * //     'COBRADO TOTAL' => 100,
     * //     'CONTRATOS NUEVOS' => 5
     * // ]
     * ```
     *
     * @example Error: Clave vacía en `$order_keys`
     * ```php
     * $order_keys = ['COBRADO TOTAL', ''];
     * $row = [
     *     'COBRADO TOTAL' => 100,
     *     'CONTRATOS NUEVOS' => 5
     * ];
     * $row_new = [];
     *
     * $resultado = $this->integra_keys($order_keys, $row, $row_new);
     * // Resultado:
     * // [
     * //     'error' => 'Error $key está vacío',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Fallo al integrar un valor
     * ```php
     * $order_keys = ['COBRADO TOTAL'];
     * $row = [];
     * $row_new = [];
     *
     * $resultado = $this->integra_keys($order_keys, $row, $row_new);
     * // Resultado:
     * // [
     * //     'error' => 'Error al integrar value $row_new',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function integra_keys(array $order_keys, array $row, array $row_new): array
    {
        foreach ($order_keys as $key) {
            // Validación de clave vacía
            $key = trim($key);
            if ($key === '') {
                return (new error())->error('Error $key está vacío', $key);
            }

            // Integración de la clave en la fila nueva
            $row_new = $this->integra_value_key($key, $row, $row_new);
            if (error::$en_error) {
                return (new error())->error('Error al integrar value $row_new', $row_new);
            }
        }

        return $row_new;
    }

    /**
     * EM3
     * Calcula e integra los totales en un conjunto de filas de salida.
     *
     * Esta función recorre las filas proporcionadas en `$rows_out`, ajusta los totales de las columnas relevantes
     * y los agrega al final del conjunto. Si ocurre un error en cualquier etapa del proceso, devuelve un array con
     * los detalles del error.
     *
     * @param array $rows_out Array de filas procesadas, donde cada fila representa un conjunto de datos.
     *
     * @return array Devuelve las filas procesadas con los totales integrados al final. Si ocurre un error,
     *               devuelve un array con los detalles del mismo.
     *
     * @example Uso exitoso:
     * ```php
     * $rows_out = [
     *     [
     *         'CONTRATOS CONGELADOS' => 2,
     *         'CARTERA ACTUAL' => 50,
     *         'CONTRATOS NUEVOS' => 5,
     *         'META' => 100,
     *         'COBRADO TOTAL' => 75,
     *         'COBRADO META' => 60,
     *         'COBRADO META DE MAS' => 15,
     *         'COBRADO CONTRATOS NUEVOS' => 10,
     *         'COBRADO CONTRATOS EXTRA' => 5,
     *     ],
     *     [
     *         'CONTRATOS CONGELADOS' => 3,
     *         'CARTERA ACTUAL' => 40,
     *         'CONTRATOS NUEVOS' => 4,
     *         'META' => 90,
     *         'COBRADO TOTAL' => 80,
     *         'COBRADO META' => 50,
     *         'COBRADO META DE MAS' => 30,
     *         'COBRADO CONTRATOS NUEVOS' => 15,
     *         'COBRADO CONTRATOS EXTRA' => 10,
     *     ]
     * ];
     *
     * $resultado = $this->integra_totales($rows_out);
     * // Resultado:
     * // [
     * //     [...], // Primera fila
     * //     [...], // Segunda fila
     * //     [], // Separador vacío
     * //     [
     * //         'CONTRATOS CONGELADOS' => 5,
     * //         'CARTERA ACTUAL' => 90,
     * //         'CONTRATOS NUEVOS' => 9,
     * //         'META' => 190,
     * //         'COBRADO TOTAL' => 155,
     * //         'COBRADO META' => 110,
     * //         'COBRADO META DE MAS' => 45,
     * //         'COBRADO CONTRATOS NUEVOS' => 25,
     * //         'COBRADO CONTRATOS EXTRA' => 15,
     * //     ]
     * // ]
     * ```
     *
     * @example Error: Entrada inválida
     * ```php
     * $rows_out = 'cadena_inválida';
     * $resultado = $this->integra_totales($rows_out);
     * // Resultado:
     * // [
     * //     'error' => 'Error al obtener $totales',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function integra_totales(array $rows_out): array
    {
        $indice = 0;

        // Inicializa los totales
        $totales = $this->inicializa_totales();
        if (error::$en_error) {
            return (new error())->error('Error al obtener $totales', $totales);
        }

        // Ajusta los totales para cada fila
        foreach ($rows_out as $indice => $row) {
            $totales = $this->ajusta_totales($row, $totales);
            if (error::$en_error) {
                return (new error())->error('Error al ajustar totales', $totales);
            }
        }

        // Agrega una fila vacía y los totales al final
        $rows_out[$indice + 1] = array();
        $rows_out[$indice + 2] = $totales;

        return $rows_out;
    }

    /**
     * EM3
     * Integra un valor calculado o predeterminado en una fila de datos para una clave específica.
     *
     * Esta función realiza los siguientes pasos:
     * - Valida que la clave no esté vacía.
     * - Obtiene el valor correspondiente a la clave utilizando `get_value`.
     * - Integra el valor obtenido en la fila de datos bajo la clave especificada.
     *
     * @param string $key Clave para la cual se desea integrar un valor en la fila.
     * @param array $row Fila de datos en la que se integrará el valor.
     *
     * @return array Devuelve la fila actualizada con el valor integrado. Si ocurre un error,
     *               devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Valor integrado en la fila
     * ```php
     * $key = 'COBRADO TOTAL';
     * $row = ['COBRADO META' => 100, 'META' => 200];
     * $resultado = $this->integra_value($key, $row);
     * // Resultado:
     * // [
     * //     'COBRADO META' => 100,
     * //     'META' => 200,
     * //     'COBRADO TOTAL' => 0 // Calculado por get_value
     * // ]
     * ```
     *
     * @example Error: Clave vacía
     * ```php
     * $key = '';
     * $row = ['COBRADO META' => 100, 'META' => 200];
     * $resultado = $this->integra_value($key, $row);
     * // Resultado:
     * // [
     * //     'error' => 'Error $key está vacío',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Fallo al obtener valor
     * ```php
     * $key = 'COBRADO TOTAL';
     * $row = [];
     * // Suponiendo que get_value genera un error
     * $resultado = $this->integra_value($key, $row);
     * // Resultado:
     * // [
     * //     'error' => 'Error al obtener porcentaje',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function integra_value(string $key, array $row): array
    {
        // Validación de clave vacía
        $key = trim($key);
        if ($key === '') {
            return (new error())->error('Error $key está vacío', $key);
        }

        // Obtener el valor correspondiente a la clave
        $value = $this->get_value($key, $row);
        if (error::$en_error) {
            return (new error())->error('Error al obtener valor para $key', $value);
        }

        // Integrar el valor en la fila
        $row[$key] = $value;

        return $row;
    }


    /**
     * EM3
     * Integra un valor de una clave específica desde una fila (`$row`) en otra fila (`$row_new`).
     *
     * Esta función valida que la clave no esté vacía, verifica e integra el valor correspondiente
     * desde `$row` en `$row_new` utilizando `integra_value_nuevo`. Si ocurre algún error durante
     * el proceso, lo devuelve con los detalles.
     *
     * @param string $key Clave que será utilizada para integrar el valor desde `$row` a `$row_new`.
     * @param array $row Fila de datos de origen desde donde se obtiene el valor.
     * @param array $row_new Fila de datos de destino donde se integrará el valor.
     *
     * @return array Devuelve la fila `$row_new` actualizada con el valor integrado. Si ocurre un error,
     *               devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Valor integrado en la fila de destino
     * ```php
     * $key = 'COBRADO TOTAL';
     * $row = ['COBRADO TOTAL' => 100];
     * $row_new = [];
     *
     * $resultado = $this->integra_value_key($key, $row, $row_new);
     * // Resultado:
     * // [
     * //     'COBRADO TOTAL' => 100
     * // ]
     * ```
     *
     * @example Error: Clave vacía
     * ```php
     * $key = '';
     * $row = ['COBRADO TOTAL' => 100];
     * $row_new = [];
     *
     * $resultado = $this->integra_value_key($key, $row, $row_new);
     * // Resultado:
     * // [
     * //     'error' => 'Error $key está vacío',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Fallo al integrar el valor
     * ```php
     * $key = 'COBRADO TOTAL';
     * $row = [];
     * $row_new = [];
     *
     * $resultado = $this->integra_value_key($key, $row, $row_new);
     * // Resultado:
     * // [
     * //     'error' => 'Error al integrar value',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function integra_value_key(string $key, array $row, array $row_new): array
    {
        // Validación de clave vacía
        $key = trim($key);
        if ($key === '') {
            return (new error())->error('Error $key está vacío', $key);
        }

        // Integración del valor en la fila de origen
        $row = $this->integra_value_nuevo($key, $row);
        if (error::$en_error) {
            return (new error())->error('Error al integrar value', $row);
        }

        // Asignación del valor al nuevo array
        $row_new[$key] = $row[$key];

        return $row_new;
    }


    /**
     * EM3
     * Integra un valor calculado en una fila de datos si no está definido o está vacío.
     *
     * Esta función verifica si una clave específica en la fila de datos no está definida o está vacía.
     * Si se cumple esta condición, integra el valor correspondiente utilizando `integra_value`.
     *
     * @param string $key Clave para verificar y, en su caso, integrar el valor en la fila.
     * @param array $row Fila de datos en la que se verificará e integrará el valor.
     *
     * @return array Devuelve la fila actualizada con el valor integrado si la clave estaba indefinida o vacía.
     *               Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Valor integrado en la fila
     * ```php
     * $key = 'COBRADO TOTAL';
     * $row = ['COBRADO META' => 100, 'META' => 200];
     * $resultado = $this->integra_value_nuevo($key, $row);
     * // Resultado:
     * // [
     * //     'COBRADO META' => 100,
     * //     'META' => 200,
     * //     'COBRADO TOTAL' => 0 // Calculado por integra_value
     * // ]
     * ```
     *
     * @example Caso exitoso: Clave ya definida
     * ```php
     * $key = 'COBRADO TOTAL';
     * $row = ['COBRADO META' => 100, 'META' => 200, 'COBRADO TOTAL' => 50];
     * $resultado = $this->integra_value_nuevo($key, $row);
     * // Resultado:
     * // [
     * //     'COBRADO META' => 100,
     * //     'META' => 200,
     * //     'COBRADO TOTAL' => 50 // Sin cambios
     * // ]
     * ```
     *
     * @example Error: Clave vacía
     * ```php
     * $key = '';
     * $row = ['COBRADO META' => 100, 'META' => 200];
     * $resultado = $this->integra_value_nuevo($key, $row);
     * // Resultado:
     * // [
     * //     'error' => 'Error $key está vacío',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Fallo en la integración del valor
     * ```php
     * $key = 'COBRADO TOTAL';
     * $row = ['COBRADO META' => 100, 'META' => 200];
     * // Suponiendo que integra_value genera un error
     * $resultado = $this->integra_value_nuevo($key, $row);
     * // Resultado:
     * // [
     * //     'error' => 'Error al integrar value',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function integra_value_nuevo(string $key, array $row): array
    {
        // Validación de clave vacía
        $key = trim($key);
        if ($key === '') {
            return (new error())->error('Error $key está vacío', $key);
        }

        // Verificar si la clave no está definida o está vacía
        if (!isset($row[$key]) || $row[$key] === '') {
            $row = $this->integra_value($key, $row);
            if (error::$en_error) {
                return (new error())->error('Error al integrar value', $row);
            }
        }

        return $row;
    }

    /**
     * EM3
     * Calcula el porcentaje de cumplimiento de una meta basado en una clave específica y un valor de meta.
     *
     * Esta función toma un valor asociado a `$key_base` dentro de `$row` y lo divide entre el valor de `'META'`.
     * Si `'META'` no existe o es igual a 0, retorna 0.0 para evitar divisiones por cero. Además, valida que
     * `$key_base` no esté vacío y maneja valores no definidos en `$row` asignando valores predeterminados.
     *
     * @param string $key_base La clave del array `$row` que contiene el valor a usar en el cálculo del porcentaje.
     * @param array $row El array que contiene los datos necesarios para calcular el porcentaje.
     *                   Debe incluir la clave `'META'` para realizar el cálculo.
     *
     * @return float|array Devuelve el porcentaje redondeado a dos decimales si la clave y los valores son válidos.
     *                     Si ocurre un error (como `$key_base` vacío), devuelve un array con detalles del error.
     *
     * @example Uso exitoso: Cálculo del porcentaje
     * ```php
     * $key_base = 'COBRADO META';
     * $row = [
     *     'COBRADO META' => 50,
     *     'META' => 100
     * ];
     *
     * $resultado = $obj->porcentaje_meta($key_base, $row);
     * // Resultado: 50.0
     * ```
     *
     * @example Caso: Clave no definida en `$row`
     * ```php
     * $key_base = 'COBRADO META';
     * $row = [
     *     'META' => 100
     * ];
     *
     * $resultado = $obj->porcentaje_meta($key_base, $row);
     * // Resultado: 0.0
     * ```
     *
     * @example Caso: `'META'` no definido o igual a 0
     * ```php
     * $key_base = 'COBRADO META';
     * $row = [
     *     'COBRADO META' => 50
     * ];
     *
     * $resultado = $obj->porcentaje_meta($key_base, $row);
     * // Resultado: 0.0
     *
     * $row = [
     *     'COBRADO META' => 50,
     *     'META' => 0
     * ];
     *
     * $resultado = $obj->porcentaje_meta($key_base, $row);
     * // Resultado: 0.0
     * ```
     *
     * @example Error: `$key_base` vacío
     * ```php
     * $key_base = '';
     * $row = [
     *     'COBRADO META' => 50,
     *     'META' => 100
     * ];
     *
     * $resultado = $obj->porcentaje_meta($key_base, $row);
     * // Resultado:
     * // [
     * //     'error' => 'Error $key_base esta vacio',
     * //     'data' => ''
     * // ]
     * ```
     */
    private function porcentaje_meta(string $key_base, array $row)
    {
        $key_base = trim($key_base);
        if ($key_base === '') {
            return (new error())->error('Error $key_base esta vacio', $key_base);
        }
        if (!isset($row[$key_base])) {
            $row[$key_base] = 0;
        }
        if (!isset($row['META']) || $row['META'] == 0) {
            $rs = 0.0;
        } else {
            $rs = round($row[$key_base] / $row['META'] * 100, 2);
        }
        return $rs;
    }

    /**
     * EM3
     * Genera el resultado final de la meta integrando datos de clientes, metas y pagos.
     *
     * Este método procesa la información de metas y clientes desde consultas SQL, integra datos de pagos,
     * y genera un conjunto de filas con ajustes y totales calculados.
     *
     * @param string $entidad_empleado Nombre de la entidad del empleado, como `'ohem'` o `'empleado'`.
     *                                 Si está vacío, se asigna `'ohem'` como valor predeterminado.
     * @param PDO $link Conexión activa a la base de datos mediante PDO.
     * @param array $pagos_rows Arreglo de pagos. Cada elemento debe incluir:
     *                          - `'GESTOR'`: Nombre del gestor (opcional).
     *                          - `<entidad_empleado>_id`: Identificador del gestor.
     *                          - `'COBRADO_GENERAL'`: Monto cobrado total (opcional, valor predeterminado `0`).
     * @param array $r_meta Datos de la meta. Debe incluir:
     *                      - `'plaza_descripcion'`: Descripción de la plaza.
     *                      - `'meta_gestor_fecha_inicio'`: Fecha de inicio de la meta.
     *                      - `'meta_gestor_fecha_fin'`: Fecha de fin de la meta.
     * @param string $sql_clientes Consulta SQL para obtener los datos de clientes.
     * @param string $sql_metas Consulta SQL para obtener los datos de metas.
     *
     * @return array Devuelve las filas procesadas con datos integrados de clientes, metas y pagos,
     *               incluyendo ajustes y totales. Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Generación de resultados
     * ```php
     * $entidad_empleado = 'ohem';
     * $link = new PDO('mysql:host=localhost;dbname=mi_base', 'usuario', 'contraseña');
     * $pagos_rows = [
     *     [
     *         'GESTOR' => 'Juan Pérez',
     *         'ohem_id' => '123',
     *         'COBRADO_GENERAL' => 1500
     *     ]
     * ];
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     * $sql_clientes = "SELECT * FROM clientes WHERE plaza_id = 1";
     * $sql_metas = "SELECT * FROM metas WHERE plaza_id = 1";
     *
     * $resultado = $obj->result_meta($entidad_empleado, $link, $pagos_rows, $r_meta, $sql_clientes, $sql_metas);
     * // Resultado:
     * // [
     * //     [
     * //         'PLAZA' => 'Zona Norte',
     * //         'FECHA INICIO' => '2025-01-01',
     * //         'FECHA FIN' => '2025-01-31',
     * //         'GESTOR ID' => '123',
     * //         'GESTOR' => 'Juan Pérez',
     * //         'CARTERA ACTUAL' => 0,
     * //         'CONTRATOS NUEVOS' => 0,
     * //         'COBRADO TOTAL' => 1500,
     * //         'CONTRATOS CONGELADOS' => 0,
     * //         ...
     * //     ],
     * //     ...
     * // ]
     * ```
     *
     * @example Error: Consulta SQL de clientes vacía
     * ```php
     * $entidad_empleado = 'ohem';
     * $link = new PDO('mysql:host=localhost;dbname=mi_base', 'usuario', 'contraseña');
     * $pagos_rows = [];
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     * $sql_clientes = '';
     * $sql_metas = "SELECT * FROM metas WHERE plaza_id = 1";
     *
     * $resultado = $obj->result_meta($entidad_empleado, $link, $pagos_rows, $r_meta, $sql_clientes, $sql_metas);
     * // Resultado:
     * // [
     * //     'error' => 'Error: $sql_clientes está vacío',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Validación de la meta falla
     * ```php
     * $entidad_empleado = 'ohem';
     * $link = new PDO('mysql:host=localhost;dbname=mi_base', 'usuario', 'contraseña');
     * $pagos_rows = [];
     * $r_meta = [
     *     'meta_gestor_fecha_inicio' => '2025-01-01'
     * ];
     * $sql_clientes = "SELECT * FROM clientes WHERE plaza_id = 1";
     * $sql_metas = "SELECT * FROM metas WHERE plaza_id = 1";
     *
     * $resultado = $obj->result_meta($entidad_empleado, $link, $pagos_rows, $r_meta, $sql_clientes, $sql_metas);
     * // Resultado:
     * // [
     * //     'error' => 'Error al validar datos de la meta',
     * //     'data' => [...]
     * // ]
     * ```
     */
    final public function result_meta(
        string $entidad_empleado,
        PDO $link,
        array $pagos_rows,
        array $r_meta,
        string $sql_clientes,
        string $sql_metas
    ): array {
        // Validación de las consultas SQL
        $sql_clientes = trim($sql_clientes);
        if (empty($sql_clientes)) {
            return (new error())->error('Error: $sql_clientes está vacío', $sql_clientes);
        }

        $sql_metas = trim($sql_metas);
        if (empty($sql_metas)) {
            return (new error())->error('Error: $sql_metas está vacío', $sql_metas);
        }

        // Validación de los datos de la meta
        $validacion_meta = $this->valida_meta($r_meta);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos de la meta', $validacion_meta);
        }

        $entidad_empleado = trim($entidad_empleado) ?: 'ohem';

        // Inicializa las filas con datos de clientes y metas
        $rows = $this->init_rows($entidad_empleado, $link, $r_meta, $sql_clientes, $sql_metas);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $rows', $rows);
        }

        // Integra la información de pagos en las filas
        $rows = $this->integra_row_pagos($entidad_empleado, $pagos_rows, $r_meta, $rows);
        if (error::$en_error) {
            return (new error())->error('Error al integrar pagos en $rows', $rows);
        }

        // Procesa las filas para ajustes y totales
        $rows = $this->rows_out($rows);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $rows_out', $rows);
        }

        return $rows;
    }


    /**
     * EM3
     * Genera una nueva fila basada en los datos del cliente y la meta proporcionados.
     *
     * Esta función valida los datos del cliente y de la meta antes de generar una nueva fila.
     * Utiliza `row_new_base` para inicializar los valores básicos de la fila y luego
     * integra los datos específicos del cliente, como la cartera actual y los contratos nuevos.
     *
     * @param array $cliente Array con los datos del cliente. Debe incluir:
     *                       - `'GESTOR'`: Nombre del gestor.
     *                       - `'n_contratos'`: Número total de contratos del cliente.
     *                       - `'n_contratos_nuevos'`: Número de contratos nuevos del cliente.
     * @param string|int $gestor_id Identificador del gestor asociado al cliente.
     * @param array $r_meta Array con los datos de la meta asociada. Debe incluir:
     *                      - `'plaza_descripcion'`: Descripción de la plaza.
     *                      - `'meta_gestor_fecha_inicio'`: Fecha de inicio de la meta.
     *                      - `'meta_gestor_fecha_fin'`: Fecha de fin de la meta.
     *
     * @return array Devuelve un array que representa la nueva fila generada.
     *               Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso
     * ```php
     * $cliente = [
     *     'GESTOR' => 'Juan Pérez',
     *     'n_contratos' => 5,
     *     'n_contratos_nuevos' => 2
     * ];
     * $gestor_id = '123';
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     *
     * $resultado = $obj->row_new($cliente, $gestor_id, $r_meta);
     * // Resultado:
     * // [
     * //     'PLAZA' => 'Zona Norte',
     * //     'FECHA INICIO' => '2025-01-01',
     * //     'FECHA FIN' => '2025-01-31',
     * //     'GESTOR ID' => '123',
     * //     'GESTOR' => 'Juan Pérez',
     * //     'CONTRATOS CONGELADOS' => 0,
     * //     'META' => 0,
     * //     'COBRADO META' => 0,
     * //     'COBRADO META DE MAS' => 0,
     * //     'COBRADO CONTRATOS NUEVOS' => 0,
     * //     'COBRADO CONTRATOS EXTRA' => 0,
     * //     'CARTERA ACTUAL' => 5,
     * //     'CONTRATOS NUEVOS' => 2
     * // ]
     * ```
     *
     * @example Error: Validación de datos falla
     * ```php
     * $cliente = ['GESTOR' => ''];
     * $gestor_id = '';
     * $r_meta = [];
     *
     * $resultado = $obj->row_new($cliente, $gestor_id, $r_meta);
     * // Resultado:
     * // [
     * //     'error' => 'Error al validar datos',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function row_new(array $cliente, $gestor_id, array $r_meta): array
    {
        // Validación de datos del cliente y la meta
        $valida = $this->valida_row($cliente, $r_meta);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Obtener y limpiar el nombre del gestor
        $GESTOR = trim($cliente['GESTOR']);

        // Generar la base de la nueva fila
        $row_new = $this->row_new_base($GESTOR, $gestor_id, $r_meta);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $row_new', $row_new);
        }

        // Agregar datos específicos del cliente
        $row_new['CARTERA ACTUAL'] = $cliente['n_contratos'];
        $row_new['CONTRATOS NUEVOS'] = $cliente['n_contratos_nuevos'];

        return $row_new;
    }

    /**
     * EM3
     * Procesa e integra filas de clientes en las filas existentes.
     *
     * Esta función recorre un conjunto de clientes, valida sus datos junto con los de la meta asociada, verifica si ya
     * existen en las filas actuales y actualiza o integra nuevas filas según sea necesario.
     *
     * @param array $clientes_rows Lista de arrays que contienen los datos de los clientes. Cada cliente debe incluir:
     *                             - `'GESTOR'`: Nombre del gestor.
     *                             - `<entidad_empleado>_id`: Identificador del gestor asociado.
     *                             - `'n_contratos'`: Número total de contratos.
     *                             - `'n_contratos_nuevos'`: Número de contratos nuevos.
     * @param string $entidad_empleado Nombre de la entidad del empleado (por ejemplo, `'ohem'` o `'empleado'`).
     *                                  Este valor se utiliza para construir la clave `<entidad_empleado>_id` en los clientes.
     * @param array $r_meta Array con los datos de la meta asociada. Debe incluir:
     *                      - `'plaza_descripcion'`: Descripción de la plaza.
     *                      - `'meta_gestor_fecha_inicio'`: Fecha de inicio de la meta.
     *                      - `'meta_gestor_fecha_fin'`: Fecha de fin de la meta.
     * @param array $rows Array con las filas existentes de datos. Cada fila debe incluir:
     *                    - `'GESTOR ID'`: Identificador del gestor.
     *
     * @return array Devuelve las filas actualizadas o integradas con los datos de los clientes. Si ocurre un error,
     *               devuelve un array con detalles del mismo.
     *
     * @example Caso exitoso: Cliente existente actualizado
     * ```php
     * $clientes_rows = [
     *     [
     *         'GESTOR' => 'Juan Pérez',
     *         'ohem_id' => '123',
     *         'n_contratos' => 10,
     *         'n_contratos_nuevos' => 3
     *     ]
     * ];
     * $entidad_empleado = 'ohem';
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     * $rows = [
     *     ['GESTOR ID' => '123', 'CARTERA ACTUAL' => 5, 'CONTRATOS NUEVOS' => 2]
     * ];
     *
     * $resultado = $this->rows_cliente($clientes_rows, $entidad_empleado, $r_meta, $rows);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'CARTERA ACTUAL' => 10, 'CONTRATOS NUEVOS' => 3]
     * // ]
     * ```
     *
     * @example Caso exitoso: Cliente nuevo integrado
     * ```php
     * $clientes_rows = [
     *     [
     *         'GESTOR' => 'Juan Pérez',
     *         'ohem_id' => '456',
     *         'n_contratos' => 8,
     *         'n_contratos_nuevos' => 4
     *     ]
     * ];
     * $entidad_empleado = 'ohem';
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     * $rows = [
     *     ['GESTOR ID' => '123', 'CARTERA ACTUAL' => 5, 'CONTRATOS NUEVOS' => 2]
     * ];
     *
     * $resultado = $this->rows_cliente($clientes_rows, $entidad_empleado, $r_meta, $rows);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'CARTERA ACTUAL' => 5, 'CONTRATOS NUEVOS' => 2],
     * //     [
     * //         'PLAZA' => 'Zona Norte',
     * //         'FECHA INICIO' => '2025-01-01',
     * //         'FECHA FIN' => '2025-01-31',
     * //         'GESTOR ID' => '456',
     * //         'GESTOR' => 'Juan Pérez',
     * //         'CARTERA ACTUAL' => 8,
     * //         'CONTRATOS NUEVOS' => 4,
     * //         ...
     * //     ]
     * // ]
     * ```
     *
     * @example Error: Cliente no es un array
     * ```php
     * $clientes_rows = 'GESTOR';
     * $entidad_empleado = 'ohem';
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     * $rows = [];
     *
     * $resultado = $this->rows_cliente($clientes_rows, $entidad_empleado, $r_meta, $rows);
     * // Resultado:
     * // [
     * //     'error' => 'Error $clientes_rows[] debe ser un array',
     * //     'data' => $clientes_rows
     * // ]
     * ```
     */
    private function rows_cliente(
        array $clientes_rows,
        string $entidad_empleado,
        array $r_meta,
        array $rows
    ): array {
        $entidad_empleado = trim($entidad_empleado) ?: 'ohem';

        foreach ($clientes_rows as $cliente) {
            if (!is_array($cliente)) {
                return (new error())->error('Error $clientes_rows[] debe ser un array', $clientes_rows);
            }

            // Validar datos del cliente y la meta
            $valida = $this->valida_row($cliente, $r_meta);
            if (error::$en_error) {
                return (new error())->error('Error al validar datos', $valida);
            }

            // Verificar si el cliente ya existe en las filas
            $existe_cliente = $this->existe_cliente($cliente, $entidad_empleado, $rows);
            if (error::$en_error) {
                return (new error())->error('Error al verificar si existe cliente', $existe_cliente);
            }

            // Actualizar filas existentes con datos del cliente
            $rows = $this->init_data_row_gestor($cliente, $entidad_empleado, $rows);
            if (error::$en_error) {
                return (new error())->error('Error al inicializar filas', $rows);
            }

            // Si el cliente no existe, generar una nueva fila e integrarla
            if (!$existe_cliente) {
                $row_new = $this->integra_row_new($cliente, $entidad_empleado, $r_meta);
                if (error::$en_error) {
                    return (new error())->error('Error al generar nueva fila', $row_new);
                }
                $rows[] = $row_new;
            }
        }

        return $rows;
    }


    /**
     * EM3
     * Inicializa los valores predeterminados de una fila con ceros.
     *
     * Esta función asigna valores predeterminados (0) a las claves relacionadas con los contratos,
     * metas y cobros dentro del array `$row_new`.
     *
     * @param array $row_new Fila de datos donde se inicializarán los valores predeterminados.
     *
     * @return array Devuelve el array `$row_new` actualizado con las claves inicializadas.
     *
     * @example Uso exitoso
     * ```php
     * $row_new = ['GESTOR ID' => '123', 'GESTOR' => 'Juan Pérez'];
     *
     * $resultado = $this->row_new_0($row_new);
     * // Resultado:
     * // [
     * //     'GESTOR ID' => '123',
     * //     'GESTOR' => 'Juan Pérez',
     * //     'CONTRATOS CONGELADOS' => 0,
     * //     'META' => 0,
     * //     'COBRADO META' => 0,
     * //     'COBRADO META DE MAS' => 0,
     * //     'COBRADO CONTRATOS NUEVOS' => 0,
     * //     'COBRADO CONTRATOS EXTRA' => 0
     * // ]
     * ```
     */
    private function row_new_0(array $row_new): array
    {
        $defaults = [
            'CONTRATOS CONGELADOS' => 0,
            'META' => 0,
            'COBRADO META' => 0,
            'COBRADO META DE MAS' => 0,
            'COBRADO CONTRATOS NUEVOS' => 0,
            'COBRADO CONTRATOS EXTRA' => 0,
        ];

        // Combina los valores predeterminados con los existentes
        return array_merge($row_new, $defaults);
    }



    /**
     * EM3
     * Genera una fila base inicializada con datos de meta, gestor y valores predeterminados.
     *
     * Este método crea una estructura de fila inicial basada en la información del gestor y los datos
     * de la meta proporcionados. Integra datos validados y completa valores predeterminados para las claves
     * necesarias.
     *
     * @param string $gestor Nombre del gestor. Si está vacío, se asignará `'S/A'`.
     * @param string $gestor_id Identificador del gestor. Si está vacío, se asignará `'S/A'`.
     * @param array $r_meta Arreglo con los datos de la meta. Debe incluir:
     *                      - `'plaza_descripcion'`: Descripción de la plaza.
     *                      - `'meta_gestor_fecha_inicio'`: Fecha de inicio de la meta.
     *                      - `'meta_gestor_fecha_fin'`: Fecha de fin de la meta.
     *
     * @return array Devuelve un array inicializado con los datos del gestor, la meta y valores predeterminados.
     *               Si ocurre un error, devuelve un array con detalles del error.
     *
     * @example Caso exitoso: Generación de fila base
     * ```php
     * $gestor = 'Juan Pérez';
     * $gestor_id = '123';
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     *
     * $fila_base = $this->row_new_base($gestor, $gestor_id, $r_meta);
     * // Resultado:
     * // [
     * //     'PLAZA' => 'Zona Norte',
     * //     'FECHA INICIO' => '2025-01-01',
     * //     'FECHA FIN' => '2025-01-31',
     * //     'GESTOR ID' => '123',
     * //     'GESTOR' => 'Juan Pérez',
     * //     'CONTRATOS CONGELADOS' => 0,
     * //     'META' => 0,
     * //     'COBRADO META' => 0,
     * //     'COBRADO META DE MAS' => 0,
     * //     'COBRADO CONTRATOS NUEVOS' => 0,
     * //     'COBRADO CONTRATOS EXTRA' => 0,
     * // ]
     * ```
     *
     * @example Caso: Gestor con datos faltantes
     * ```php
     * $gestor = '';
     * $gestor_id = '';
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     *
     * $fila_base = $this->row_new_base($gestor, $gestor_id, $r_meta);
     * // Resultado:
     * // [
     * //     'PLAZA' => 'Zona Norte',
     * //     'FECHA INICIO' => '2025-01-01',
     * //     'FECHA FIN' => '2025-01-31',
     * //     'GESTOR ID' => 'S/A',
     * //     'GESTOR' => 'S/A',
     * //     'CONTRATOS CONGELADOS' => 0,
     * //     'META' => 0,
     * //     'COBRADO META' => 0,
     * //     'COBRADO META DE MAS' => 0,
     * //     'COBRADO CONTRATOS NUEVOS' => 0,
     * //     'COBRADO CONTRATOS EXTRA' => 0,
     * // ]
     * ```
     *
     * @example Error: Faltan datos de la meta
     * ```php
     * $gestor = 'Juan Pérez';
     * $gestor_id = '123';
     * $r_meta = [];
     *
     * $fila_base = $this->row_new_base($gestor, $gestor_id, $r_meta);
     * // Resultado:
     * // [
     * //     'error' => 'Error al integrar datos de meta en $row_new',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function row_new_base(string $gestor, string $gestor_id, array $r_meta): array
    {
        $gestor = trim($gestor);
        $gestor_id = trim($gestor_id);

        // Validación de datos del gestor
        if ($gestor === '') {
            $gestor = 'S/A';
        }

        if ($gestor_id === '') {
            $gestor_id = 'S/A';
        }

        // Inicialización de la fila base
        $row_new = [];

        // Integración de datos de meta
        $row_new = $this->data_meta($r_meta, $row_new);
        if (error::$en_error) {
            return (new error())->error('Error al integrar datos de meta en $row_new', $row_new);
        }

        // Integración de datos del gestor
        $row_new = $this->data_gestor($gestor, $gestor_id, $row_new);
        if (error::$en_error) {
            return (new error())->error('Error al integrar datos del gestor en $row_new', $row_new);
        }

        // Inicialización de valores predeterminados
        $row_new = $this->row_new_0($row_new);
        if (error::$en_error) {
            return (new error())->error('Error al inicializar valores predeterminados en $row_new', $row_new);
        }

        return $row_new;
    }


    /**
     * EM3
     * Ajusta el valor de 'COBRADO TOTAL' para cada fila en `$rows_out` si corresponde.
     *
     * Esta función recorre todas las filas en `$rows_out`, valida los índices y los datos de cada fila,
     * y ajusta el valor de 'COBRADO TOTAL' en 'COBRADO CONTRATOS EXTRA' para las filas donde 'META' es igual a 0,
     * utilizando la función `ajusta_cobrado_total`.
     *
     * @param array $rows_out Array de filas con datos procesados, donde cada fila debe ser un array asociativo.
     *
     * @return array Devuelve el array `$rows_out` actualizado con los ajustes realizados.
     *               Si ocurre un error, devuelve un array con detalles del error.
     *
     * @example Caso Exitoso: Ajuste de 'COBRADO TOTAL'
     * ```php
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'META' => 0.0, 'COBRADO TOTAL' => 500.0, 'COBRADO CONTRATOS EXTRA' => 0],
     *     ['GESTOR ID' => '456', 'META' => 100.0, 'COBRADO TOTAL' => 200.0, 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->rows_cobrado_total($rows_out);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'META' => 0.0, 'COBRADO TOTAL' => 500.0, 'COBRADO CONTRATOS EXTRA' => 500.0],
     * //     ['GESTOR ID' => '456', 'META' => 100.0, 'COBRADO TOTAL' => 200.0, 'COBRADO CONTRATOS EXTRA' => 0],
     * // ]
     * ```
     *
     * @example Caso: Sin ajustes necesarios
     * ```php
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'META' => 100.0, 'COBRADO TOTAL' => 500.0, 'COBRADO CONTRATOS EXTRA' => 0],
     *     ['GESTOR ID' => '456', 'META' => 200.0, 'COBRADO TOTAL' => 200.0, 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->rows_cobrado_total($rows_out);
     * // Resultado:
     * // [
     * //     ['GESTOR ID' => '123', 'META' => 100.0, 'COBRADO TOTAL' => 500.0, 'COBRADO CONTRATOS EXTRA' => 0],
     * //     ['GESTOR ID' => '456', 'META' => 200.0, 'COBRADO TOTAL' => 200.0, 'COBRADO CONTRATOS EXTRA' => 0],
     * // ]
     * ```
     *
     * @example Error: Índice no numérico
     * ```php
     * $rows_out = [
     *     'key' => ['GESTOR ID' => '123', 'META' => 0.0, 'COBRADO TOTAL' => 500.0, 'COBRADO CONTRATOS EXTRA' => 0],
     * ];
     *
     * $resultado = $obj->rows_cobrado_total($rows_out);
     * // Resultado:
     * // [
     * //     'error' => 'Error el indice debe ser un numero',
     * //     'data' => $rows_out
     * // ]
     * ```
     *
     * @example Error: Fila no es un array
     * ```php
     * $rows_out = [
     *     ['GESTOR ID' => '123', 'META' => 0.0, 'COBRADO TOTAL' => 500.0],
     *     'InvalidRow'
     * ];
     *
     * $resultado = $obj->rows_cobrado_total($rows_out);
     * // Resultado:
     * // [
     * //     'error' => 'Error $row debe ser un array',
     * //     'data' => $rows_out
     * // ]
     * ```
     */
    private function rows_cobrado_total(array $rows_out): array
    {
        foreach ($rows_out as $indice => $row) {
            // Validar que el índice sea numérico
            if (!is_numeric($indice)) {
                return (new error())->error('Error el indice debe ser un numero', $rows_out);
            }

            // Validar que la fila sea un array
            if (!is_array($row)) {
                return (new error())->error('Error $row debe ser un array', $rows_out);
            }

            // Ajustar el valor de 'COBRADO TOTAL' si aplica
            $rows_out = $this->ajusta_cobrado_total($indice, $row, $rows_out);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $rows_out', $rows_out);
            }
        }

        return $rows_out;
    }


    /**
     * EM3
     * Procesa y ajusta las filas de datos, incluyendo ajustes en los totales y campos específicos.
     *
     * Esta función toma un conjunto de filas de entrada (`$rows`), las inicializa con claves y valores
     * predeterminados, ajusta el campo `'COBRADO TOTAL'` según las reglas definidas, y finalmente
     * integra los totales en las filas de salida.
     *
     * @param array $rows Array de filas de entrada. Cada fila debe ser un array asociativo con claves
     *                    representando los nombres de los campos.
     *
     * @return array Devuelve las filas ajustadas y con los totales integrados. Si ocurre un error,
     *               devuelve un array con los detalles del mismo.
     *
     * @example Uso exitoso: Procesar filas
     * ```php
     * $rows = [
     *     [
     *         'GESTOR ID' => '123',
     *         'GESTOR' => 'Juan Pérez',
     *         'COBRADO TOTAL' => 5000.00,
     *         'META' => 10000.00
     *     ],
     *     [
     *         'GESTOR ID' => '456',
     *         'GESTOR' => 'Luis García',
     *         'COBRADO TOTAL' => 7000.00,
     *         'META' => 15000.00
     *     ]
     * ];
     *
     * $resultado = $this->rows_out($rows);
     * // Resultado:
     * // [
     * //     [
     * //         'GESTOR ID' => '123',
     * //         'GESTOR' => 'Juan Pérez',
     * //         'COBRADO TOTAL' => 5000.00,
     * //         'META' => 10000.00,
     * //         'PORCENTAJE META GENERAL' => 50.00,
     * //         ...
     * //     ],
     * //     [
     * //         'GESTOR ID' => '456',
     * //         'GESTOR' => 'Luis García',
     * //         'COBRADO TOTAL' => 7000.00,
     * //         'META' => 15000.00,
     * //         'PORCENTAJE META GENERAL' => 46.67,
     * //         ...
     * //     ],
     * //     [], // Línea en blanco
     * //     [
     * //         'CONTRATOS CONGELADOS' => 0,
     * //         'CARTERA ACTUAL' => 0,
     * //         'CONTRATOS NUEVOS' => 0,
     * //         'META' => 25000.00,
     * //         'COBRADO TOTAL' => 12000.00,
     * //         ...
     * //     ]
     * // ]
     * ```
     *
     * @example Error: `$rows` no es un array válido
     * ```php
     * $rows = 'datos_invalidos';
     *
     * $resultado = $this->rows_out($rows);
     * // Resultado:
     * // [
     * //     'error' => 'Error al obtener $rows_out',
     * //     'data' => 'datos_invalidos'
     * // ]
     * ```
     */
    private function rows_out(array $rows): array
    {
        // Inicializa las filas de salida con claves y valores predeterminados
        $rows_out = $this->init_rows_out($rows);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $rows_out', $rows_out);
        }

        // Ajusta las filas para manejar el campo COBRADO TOTAL
        $rows_out = $this->rows_cobrado_total($rows_out);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $rows_out', $rows_out);
        }

        // Integra los totales al final del conjunto de datos
        $rows_out = $this->integra_totales($rows_out);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $rows_out', $rows_out);
        }

        return $rows_out;
    }


    /**
     * EM3
     * Valida la existencia de claves necesarias en los datos de la meta.
     *
     * Verifica que las claves requeridas existan en el array `$r_meta`. Si falta alguna clave,
     * devuelve un array de error con detalles específicos. Si todas las claves están presentes,
     * retorna `true`.
     *
     * @param array $r_meta Array con los datos de la meta a validar.
     *
     * @return bool|array Devuelve `true` si todas las claves necesarias están presentes.
     *                    Si falta alguna clave, devuelve un array con los detalles del error.
     *
     * @example Uso exitoso
     * ```php
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     * $resultado = $this->valida_meta($r_meta);
     * // Resultado: true
     * ```
     *
     * @example Error: Falta una clave
     * ```php
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte'
     * ];
     * $resultado = $this->valida_meta($r_meta);
     * // Resultado:
     * // [
     * //     'error' => 'Error $r_meta[meta_gestor_fecha_inicio] no existe',
     * //     'data' => [...]
     * // ]
     * ```
     */
    private function valida_meta(array $r_meta) {
        // Claves requeridas
        $required_keys = [
            'plaza_descripcion',
            'meta_gestor_fecha_inicio',
            'meta_gestor_fecha_fin'
        ];

        // Validación de claves
        foreach ($required_keys as $key) {
            if (!isset($r_meta[$key])) {
                return (new error())->error("Error \$r_meta[$key] no existe", $r_meta);
            }
        }

        return true;
    }

    /**
     * EM3
     * Valida la existencia de claves esenciales en los datos del cliente y la meta.
     *
     * Esta función verifica que los datos requeridos existan tanto en el array `$cliente` como en `$r_meta`.
     * Si algún campo clave no está presente o está vacío, devuelve un error con detalles.
     * Si todos los campos están presentes, devuelve `true`.
     *
     * @param array $cliente Array que contiene los datos del cliente. Debe incluir:
     *                       - `'GESTOR'` (opcional): Nombre del gestor. Si no está definido, se asigna `'S/A'`.
     *                       - `'n_contratos'`: Número total de contratos del cliente.
     *                       - `'n_contratos_nuevos'`: Número de contratos nuevos del cliente.
     * @param array $r_meta Array que contiene los datos meta asociados al cliente. Debe incluir:
     *                      - `'plaza_descripcion'`: Descripción de la plaza.
     *                      - `'meta_gestor_fecha_inicio'`: Fecha de inicio de la meta del gestor.
     *                      - `'meta_gestor_fecha_fin'`: Fecha de fin de la meta del gestor.
     *
     * @return bool|array Devuelve `true` si todos los campos requeridos están presentes y válidos.
     *                    Si falta algún campo, devuelve un array con detalles del error.
     *
     * @example Caso exitoso: Todos los campos presentes
     * ```php
     * $cliente = [
     *     'GESTOR' => 'Juan Pérez',
     *     'n_contratos' => 10,
     *     'n_contratos_nuevos' => 3
     * ];
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     *
     * $resultado = $this->valida_row($cliente, $r_meta);
     * // Resultado: true
     * ```
     *
     * @example Error: Falta un campo en `$r_meta`
     * ```php
     * $cliente = [
     *     'GESTOR' => 'Juan Pérez',
     *     'n_contratos' => 10,
     *     'n_contratos_nuevos' => 3
     * ];
     * $r_meta = [
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     *
     * $resultado = $this->valida_row($cliente, $r_meta);
     * // Resultado:
     * // [
     * //     'error' => 'Error $r_meta[plaza_descripcion] no existe',
     * //     'data' => [...]
     * // ]
     * ```
     *
     * @example Error: Falta un campo en `$cliente`
     * ```php
     * $cliente = [
     *     'n_contratos' => 10
     * ];
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     *
     * $resultado = $this->valida_row($cliente, $r_meta);
     * // Resultado:
     * // [
     * //     'error' => 'Error $cliente[n_contratos_nuevos] no existe',
     * //     'data' => [...]
     * // ]
     * ```
     *
     * @example Caso: Campo opcional `'GESTOR'` no definido
     * ```php
     * $cliente = [
     *     'n_contratos' => 10,
     *     'n_contratos_nuevos' => 5
     * ];
     * $r_meta = [
     *     'plaza_descripcion' => 'Zona Norte',
     *     'meta_gestor_fecha_inicio' => '2025-01-01',
     *     'meta_gestor_fecha_fin' => '2025-01-31'
     * ];
     *
     * $resultado = $this->valida_row($cliente, $r_meta);
     * // Resultado: true
     * // Nota: Se asigna 'S/A' a $cliente['GESTOR'].
     * ```
     */
    private function valida_row(array $cliente, array $r_meta)
    {
        if (!isset($r_meta['plaza_descripcion'])) {
            return (new error())->error('Error $r_meta[plaza_descripcion] no existe', $r_meta);
        }
        if (!isset($r_meta['meta_gestor_fecha_inicio'])) {
            return (new error())->error('Error $r_meta[meta_gestor_fecha_inicio] no existe', $r_meta);
        }
        if (!isset($r_meta['meta_gestor_fecha_fin'])) {
            return (new error())->error('Error $r_meta[meta_gestor_fecha_fin] no existe', $r_meta);
        }
        if (!isset($cliente['GESTOR'])) {
            $cliente['GESTOR'] = 'S/A';
        }
        if (!isset($cliente['n_contratos'])) {
            return (new error())->error('Error $cliente[n_contratos] no existe', $cliente);
        }
        if (!isset($cliente['n_contratos_nuevos'])) {
            return (new error())->error('Error $cliente[n_contratos_nuevos] no existe', $cliente);
        }

        return true;
    }


    /**
     * EM3
     * Obtiene un valor basado en la clave proporcionada, la lista de claves con valor cero y los datos de una fila.
     *
     * Esta función determina el valor de una clave específica (`$key`) dentro de un contexto dado:
     * - Si la clave está vacía, retorna un error.
     * - Si la clave pertenece a `$keys_cero`, el valor será 0.
     * - Para claves específicas como `'PORCENTAJE META GENERAL'` y `'PORCENTAJE META REAL'`,
     *   calcula el porcentaje correspondiente utilizando valores de la fila.
     * - Si no se encuentra un caso definido, retorna `'POR ASIGNAR'`.
     *
     * @param string $key Clave cuyo valor se desea obtener.
     * @param array $keys_cero Lista de claves que deben asignarse con el valor 0.
     * @param array $row Fila de datos que contiene los valores necesarios para las claves dinámicas.
     *
     * @return array|float|string Devuelve el valor calculado o asignado para la clave proporcionada.
     *               Si ocurre un error, retorna un array con los detalles del error.
     *
     * @example Uso exitoso: Clave con valor cero
     * ```php
     * $key = 'CONTRATOS NUEVOS';
     * $keys_cero = ['CONTRATOS NUEVOS', 'META'];
     * $row = [];
     * $resultado = $this->value($key, $keys_cero, $row);
     * // Resultado: 0
     * ```
     *
     * @example Uso exitoso: Porcentaje meta general
     * ```php
     * $key = 'PORCENTAJE META GENERAL';
     * $keys_cero = ['CONTRATOS NUEVOS', 'META'];
     * $row = ['COBRADO TOTAL' => 50, 'META' => 100];
     * $resultado = $this->value($key, $keys_cero, $row);
     * // Resultado: 50.0
     * ```
     *
     * @example Error: Clave vacía
     * ```php
     * $key = '';
     * $keys_cero = ['CONTRATOS NUEVOS', 'META'];
     * $row = [];
     * $resultado = $this->value($key, $keys_cero, $row);
     * // Resultado:
     * // [
     * //     'error' => 'Error $key está vacío',
     * //     'data' => ''
     * // ]
     * ```
     */
    private function value(string $key, array $keys_cero, array $row)
    {
        $key = trim($key);

        // Validación de clave vacía
        if ($key === '') {
            return (new error())->error('Error $key está vacío', $key);
        }

        // Valor predeterminado para claves desconocidas
        $value = 'POR ASIGNAR';

        // Asignar valor 0 para claves definidas en $keys_cero
        if (in_array($key, $keys_cero)) {
            $value = 0;
        }

        // Calcular porcentajes para claves específicas
        if ($key === 'PORCENTAJE META GENERAL') {
            $value = $this->value_porcentaje_meta('COBRADO TOTAL', $row);
            if (error::$en_error) {
                return (new error())->error('Error al obtener porcentaje meta general', $value);
            }
        }

        if ($key === 'PORCENTAJE META REAL') {
            $value = $this->value_porcentaje_meta('COBRADO META', $row);
            if (error::$en_error) {
                return (new error())->error('Error al obtener porcentaje meta real', $value);
            }
        }

        return $value;
    }


    /**
     * EM3
     * Calcula el porcentaje de cumplimiento de una meta basado en una clave específica.
     *
     * Esta función toma un valor asociado a `$key_base` dentro del array `$row` y calcula
     * su porcentaje respecto al valor de `'META'`. Si `'META'` no existe o es menor o igual a 0,
     * retorna 0.0. Si ocurre un error en la clave `$key_base`, devuelve un mensaje detallado.
     *
     * @param string $key_base Clave del array `$row` que contiene el valor a usar para el cálculo del porcentaje.
     * @param array $row Array que contiene los datos necesarios para calcular el porcentaje.
     *                   Debe incluir la clave `'META'` con el valor de la meta.
     *
     * @return float|array Devuelve el porcentaje calculado como un número flotante redondeado a dos decimales.
     *                     Si ocurre un error, devuelve un array con detalles del mismo.
     *
     * @example Uso exitoso
     * ```php
     * $key_base = 'COBRADO META';
     * $row = [
     *     'COBRADO META' => 50,
     *     'META' => 100
     * ];
     * $resultado = $this->value_porcentaje_meta($key_base, $row);
     * // Resultado: 50.0
     * ```
     *
     * @example Caso: `'META'` es 0
     * ```php
     * $key_base = 'COBRADO META';
     * $row = [
     *     'COBRADO META' => 50,
     *     'META' => 0
     * ];
     * $resultado = $this->value_porcentaje_meta($key_base, $row);
     * // Resultado: 0.0
     * ```
     *
     * @example Error: `$key_base` está vacío
     * ```php
     * $key_base = '';
     * $row = [
     *     'COBRADO META' => 50,
     *     'META' => 100
     * ];
     * $resultado = $this->value_porcentaje_meta($key_base, $row);
     * // Resultado:
     * // [
     * //     'error' => 'Error $key_base está vacío',
     * //     'data' => ''
     * // ]
     * ```
     */
    private function value_porcentaje_meta(string $key_base, array $row)
    {
        $key_base = trim($key_base);

        // Validación de la clave base
        if ($key_base === '') {
            return (new error())->error('Error $key_base está vacío', $key_base);
        }

        // Inicialización de la clave 'META' si no existe
        $meta_value = $row['META'] ?? 0.0;

        // Retorna 0 si la meta es 0 o no válida
        if ((float)$meta_value <= 0.0) {
            return 0.0;
        }

        // Cálculo del porcentaje si 'META' es válido
        $value = $this->porcentaje_meta($key_base, $row);
        if (error::$en_error) {
            return (new error())->error('Error al obtener porcentaje', $value);
        }

        return $value;
    }




}
