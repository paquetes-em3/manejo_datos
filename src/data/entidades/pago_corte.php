<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\_valida;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\data\entidades\pago_corte\_reporte_fecha;
use desarrollo_em3\manejo_datos\formato;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use stdClass;

class pago_corte
{


    /**
     * TRASLADADO
     * Aplica la validación para un pago de corte en la base de datos.
     *
     * La función valida que el ID de pago de corte sea mayor a 0, luego obtiene el registro del corte de pago correspondiente
     * desde la base de datos. Posteriormente, aplica la validación del pago de corte mediante la función `aplica_validado_transaccion`.
     * Si ocurre algún error en el proceso, se devuelve un array con los detalles del error.
     *
     * @param PDO $link La conexión PDO utilizada para ejecutar las consultas.
     * @param int $pago_corte_id El ID del pago de corte que se está verificando. Debe ser mayor a 0.
     *
     * @return bool|array Devuelve `true` si la validación se aplica correctamente.
     *                    Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function aplica_validacion(PDO $link, int $pago_corte_id)
    {
        if($pago_corte_id <=0 ){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0', $pago_corte_id);
        }

        $pago_corte = (new consultas())->registro_bruto(
            new stdClass(),'pago_corte', $pago_corte_id,$link, false);
        if(error::$en_error){
            return (new error())->error('Error al obtener pago_corte',$pago_corte);
        }
        $aplica_validado = $this->aplica_validado_transaccion($link, $pago_corte,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $aplica_validado', $aplica_validado);
        }

        return $aplica_validado;

    }

    /**
     * TRASLADADO
     * Verifica si todos los depósitos han sido validados y aplica el estado de validación de corte de pago.
     *
     * La función verifica si el ID de pago de corte es mayor a 0. Si la opción para validar el pago de corte está activada,
     * verifica si todos los depósitos asociados al pago de corte están validados. Si todos están validados,
     * establece el estado de validación como verdadero. Si ocurre algún error durante el proceso,
     * devuelve un array con los detalles del error.
     *
     * @param bool $aplica_validar_pago_corte Indica si se debe validar el corte de pago.
     * @param PDO $link Conexión PDO utilizada para ejecutar las consultas relacionadas con los depósitos.
     * @param int $pago_corte_id El ID del corte de pago que se está validando. Debe ser mayor a 0.
     *
     * @return bool|array Devuelve `true` si todos los depósitos han sido validados y se aplica la validación de pago de corte.
     *                    Devuelve `false` si no se aplica. Si ocurre un error, devuelve un array con los detalles del error.
     */
    private function aplica_validado(bool $aplica_validar_pago_corte, PDO $link, int $pago_corte_id)
    {
        if($pago_corte_id <=0 ){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0', $pago_corte_id);
        }
        $aplica_validado = false;
        if($aplica_validar_pago_corte) {
            $todos_validados = $this->depositos_validados($link, $pago_corte_id);
            if (error::$en_error) {
                return (new error())->error('Error al ver depositos validados', $todos_validados);
            }
            if($todos_validados){
                $aplica_validado = true;
            }
        }
        return $aplica_validado;

    }

    /**
     * TRASLADADO
     * Verifica y aplica la validación de un pago de corte dentro de una transacción.
     *
     * La función valida que el ID de pago de corte sea mayor a 0 y determina si se debe aplicar la validación del pago de corte
     * utilizando la función `aplica_validar_pago_corte`. Posteriormente, se valida el corte de pago si corresponde, y se verifica
     * si todos los depósitos han sido validados. Si ocurre algún error en el proceso, se devuelve un array con los detalles del error.
     *
     * @param PDO $link La conexión PDO utilizada para ejecutar las consultas en la transacción.
     * @param stdClass $pago_corte Un objeto que contiene los datos del pago de corte.
     * @param int $pago_corte_id El ID del pago de corte que se está verificando. Debe ser mayor a 0.
     *
     * @return bool|array Devuelve `true` si se aplica la validación de los depósitos en el pago de corte.
     *                    Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function aplica_validado_transaccion(PDO $link, stdClass $pago_corte, int $pago_corte_id)
    {
        if($pago_corte_id <=0 ){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0', $pago_corte_id);
        }
        $aplica_validar_pago_corte = $this->aplica_validar_pago_corte($pago_corte);
        if(error::$en_error){
            return (new error())->error('Error al obtener $aplica_validar_pago_corte',
                $aplica_validar_pago_corte);
        }

        $aplica_validado = $this->aplica_validado($aplica_validar_pago_corte, $link,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $aplica_validado', $aplica_validado);
        }

        return $aplica_validado;

    }

    /**
     * TRASLADADO
     * Valida los montos de un objeto de pago de corte y determina si aplica la validación.
     *
     * Esta función verifica que los montos en el objeto `$pago_corte` estén definidos y sean numéricos.
     * Luego, redondea estos montos a dos decimales y comprueba si el monto por depositar es igual a 0.0
     * y si el monto depositado coincide con el monto total. Si ambas condiciones se cumplen, se establece
     * que la validación de pago de corte aplica.
     *
     * @param stdClass $pago_corte Objeto que contiene los montos del pago de corte a validar.
     *
     * @return bool|array Devuelve `true` si la validación de pago de corte aplica, `false` en caso contrario,
     *                    o un array de error si alguno de los valores en `$pago_corte` no está definido o no es numérico.
     */
    private function aplica_validar_pago_corte(stdClass $pago_corte)
    {

        if(!isset($pago_corte->monto_por_depositar)){
            return (new error())->error('Error al generar $pago_corte->monto_por_depositar no existe',
                $pago_corte);
        }
        if(!is_numeric($pago_corte->monto_por_depositar)){
            return (new error())->error('Error al generar $pago_corte->monto_por_depositar debe ser un numero',
                $pago_corte);
        }
        if(!isset($pago_corte->monto_depositado)){
            return (new error())->error('Error al generar $pago_corte->monto_depositado no existe',
                $pago_corte);
        }
        if(!is_numeric($pago_corte->monto_depositado)){
            return (new error())->error('Error al generar $pago_corte->monto_depositado debe ser un numero',
                $pago_corte);
        }
        if(!isset($pago_corte->monto_total)){
            return (new error())->error('Error al generar $pago_corte->monto_total no existe',
                $pago_corte);
        }
        if(!is_numeric($pago_corte->monto_total)){
            return (new error())->error('Error al generar $pago_corte->monto_depositado debe ser un numero',
                $pago_corte);
        }

        $aplica_validar_pago_corte = false;
        $pago_corte->monto_por_depositar = round($pago_corte->monto_por_depositar,2);
        $pago_corte->monto_depositado = round($pago_corte->monto_depositado,2);
        $pago_corte->monto_total = round($pago_corte->monto_total,2);

        if($pago_corte->monto_por_depositar === 0.0){
            if($pago_corte->monto_depositado === $pago_corte->monto_total){
                $aplica_validar_pago_corte = true;
            }
        }
        return $aplica_validar_pago_corte;

    }

    /**
     * TRASLADADO
     * Obtiene una lista de cortes de pago que no han sido validados desde la base de datos.
     *
     * Esta función genera y ejecuta una consulta SQL que selecciona los cortes de pago en los cuales
     * los montos total, depositado, y por depositar son 0, y el estado de validación es "inactivo".
     * La consulta está limitada por el parámetro `$limit`, que define el número máximo de registros
     * a devolver. Si el límite es menor que 0, se devuelve un array con detalles del error.
     *
     * @param PDO $link Objeto PDO para la conexión a la base de datos.
     * @param int $limit [opcional] Número máximo de registros a devolver. El valor predeterminado es 10.
     *
     * @return array Devuelve un array de objetos que representan los cortes de pago obtenidos,
     *               o un array de error si alguna validación o operación falla.
     */
    private function cortes_sin_validar(PDO $link, int $limit = 10): array
    {
        if($limit < 0){
            return (new error())->error('Error limit debe ser mayor a 0', $limit);
        }

        $sql = (new \desarrollo_em3\manejo_datos\sql\pago_corte())->cortes_sin_validar($limit);
        if(error::$en_error){
            return (new error())->error('Error al generar sql',$sql);
        }
        $cortes = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener cortes',$cortes);
        }
        return $cortes;

    }

    /**
     * TRASLADADO
     * Verifica si todos los depósitos de un corte de pago específico están validados.
     *
     * Esta función valida que el ID del corte de pago (`pago_corte_id`) sea mayor que 0,
     * luego obtiene los depósitos asociados a ese corte de pago desde la base de datos
     * utilizando un filtro SQL. Después, verifica si todos los depósitos están validados.
     * Si algún depósito no está validado, o si ocurre algún error durante la validación o la
     * obtención de los datos, se devuelve un array con los detalles del error.
     *
     * @param PDO $link Objeto PDO para la conexión a la base de datos.
     * @param int $pago_corte_id ID del corte de pago cuyo estado de validación de depósitos se va a verificar.
     *
     * @return bool|array Devuelve `true` si todos los depósitos están validados, `false` si alguno no lo está,
     *                    o un array de error si alguna validación o operación falla.
     */
    private function depositos_validados(PDO $link, int $pago_corte_id)
    {
        if($pago_corte_id <=0 ){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0', $pago_corte_id);
        }
        $filtro_sql = "deposito_aportaciones.pago_corte_id = $pago_corte_id";
        $deposito_aportaciones = (new consultas())->rows_by_filter('deposito_aportaciones', $filtro_sql, $link);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $deposito_aportaciones', $deposito_aportaciones);
        }

        $todos_validados = $this->todos_validados($deposito_aportaciones);
        if (error::$en_error) {
            return (new error())->error('Error al ver depositos validados',
                $todos_validados);
        }

        return $todos_validados;

    }

    private function get_empleados_rpt(
        array $empleados_id, string $entidad_empleado, string $fecha_final, string $fecha_inicial, string $format, PDO $link,
        array $plazas_id): array
    {
        $datos = (new _reporte_fecha())->datos_completos_reporte(
            $empleados_id,$entidad_empleado,$fecha_final, $fecha_inicial,$link,$plazas_id);
        if(error::$en_error){
            return  (new error())->error('Error al integrar cuenta',$datos);
        }

        $empleados_rpt = (new _reporte_fecha())->empleados_rpt($datos, $format);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $empleados_rpt', $empleados_rpt);
        }
        return$empleados_rpt;

    }




    /**
     * TRASLADADO
     * Inicializa un proceso de corte de pagos, recalculando los montos y validando el corte.
     *
     * Esta función realiza los siguientes pasos:
     * 1. Valida los parámetros necesarios para identificar el corte de pagos.
     * 2. Recalcula los montos totales, depositados y pendientes de depósito asociados al corte.
     * 3. Valida la consistencia y estado del corte de pagos después de recalcular.
     * 4. Devuelve un objeto con los resultados del recálculo y la validación.
     *
     * @param string $campo_canceled Nombre del campo que indica si un pago está cancelado.
     * @param string $campo_monto_pago Nombre del campo que contiene el monto del pago.
     * @param string $campo_movto Nombre del campo que representa el movimiento en la tabla `pago`.
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $pago_corte_id ID del corte de pagos. Debe ser mayor a 0.
     *
     * @return stdClass|array Devuelve un objeto con los resultados del recálculo y la validación, o un arreglo con información de error en caso de fallo.
     *
     * El objeto devuelto tiene la siguiente estructura:
     * - `recalcula`: Resultado del proceso de recálculo de montos.
     * - `valida`: Resultado del proceso de validación del corte.
     *
     * Errores posibles:
     * - `['error' => 'Error al validar pago corte', 'detalle' => $valida]` si falla la validación inicial de los parámetros.
     * - `['error' => 'Error al recalcular', 'detalle' => $recalcula]` si ocurre un fallo durante el proceso de recálculo.
     * - `['error' => 'Error al $valida', 'detalle' => $valida]` si ocurre un fallo durante el proceso de validación del corte.
     */
    final public function init_corte(string $campo_canceled, string $campo_monto_pago, string $campo_movto,
                                     PDO $link, int $pago_corte_id)
    {
        $valida = (new _valida())->valida_pago_corte($campo_monto_pago,$campo_movto,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al validar pago corte',$valida);
        }

        $recalcula = $this->recalcula_montos($campo_canceled,$campo_monto_pago,$campo_movto,$link,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al recalcular',$recalcula);
        }

        $valida = $this->validar_pago_corte($link,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al $valida',$valida);
        }

        $data = new stdClass();
        $data->recalcula = $recalcula;
        $data->valida = $valida;

        return $data;

    }




    final public function integra_link_deposito_aportacion(
        string $campo_serie, array $r_deposito_faltante, int $registro_id, array $serie_duplicada,
        string $session_id): array
    {

        $link = $this->link_deposito_aportacion($registro_id,$session_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener link',$link);
        }


        foreach($r_deposito_faltante AS $index => $faltante) {

            $r_deposito_faltante = $this->integra_serie_faltante(
                $campo_serie,$faltante,$index,$link,$r_deposito_faltante,$serie_duplicada);
            if(error::$en_error) {
                return (new error())->error('Error al integrar serie faltante',$r_deposito_faltante);
            }
        }
        return $r_deposito_faltante;

    }



    /**
     * TRASLADADO
     * Integra la serie faltante en un array de depósitos, activando o desactivando la inserción de depósitos según la duplicación de la serie.
     *
     * La función valida los parámetros de entrada, activando la inserción de depósitos y asignando un enlace en el índice correspondiente
     * si no hay errores. También recorre las series duplicadas y desactiva la inserción si el código de la serie ya está presente.
     * Si ocurre algún error durante el proceso, devuelve un array con los detalles del fallo.
     *
     * @param string $campo_serie El nombre del campo de la serie que se está verificando.
     * @param array $faltante El array que contiene los datos del contrato faltante.
     * @param int $index El índice dentro del array `$r_deposito_faltante` que será modificado.
     * @param string $link El enlace que se asignará en el campo `href` del depósito.
     * @param array $r_deposito_faltante El array que contiene los datos de los depósitos faltantes.
     * @param array $serie_duplicada Un array que contiene las series duplicadas a verificar.
     *
     * @return array Devuelve el array de depósitos faltantes modificado. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function integra_serie_faltante(string $campo_serie, array $faltante, int $index, string $link,
                                                 array $r_deposito_faltante, array $serie_duplicada): array
    {

        if($index < 0){
            return (new error())->error('Error index debe ser mayor o igual a 0',$index);
        }
        $link = trim($link);
        if($link === ''){
            return (new error())->error('Error $link esta vacio',$link);
        }
        $campo_serie = trim($campo_serie);
        if($campo_serie === ''){
            return (new error())->error('Error $campo_serie esta vacia',$campo_serie);
        }
        if(!isset($faltante["contrato_$campo_serie"])){
            return (new error())->error('Error $faltante["contrato_'.$campo_serie.'"]',$faltante);
        }

        $r_deposito_faltante[$index]['inserta_deposito'] = 'activo';
        $r_deposito_faltante[$index]['href'] = $link;

        foreach($serie_duplicada AS $serie) {
            if(!is_array($serie)){
                return (new error())->error('Error $serie debe ser un array',$serie_duplicada);
            }
            if(!isset($serie['serie_codigo'])){
                return (new error())->error('Error $serie[serie_codigo] no existe',$serie);
            }
            $serie_codigo = trim($serie['serie_codigo']);
            if($serie_codigo === ''){
                return (new error())->error('Error $serie[serie_codigo] esta vacia',$serie);
            }

            $r_deposito_faltante  =$this->row_deposito_literal(
                $campo_serie,$faltante,$index,$r_deposito_faltante,$serie);
            if(error::$en_error) {
                return (new error())->error('Error al obtener row_deposito_literal',$r_deposito_faltante);
            }
        }
        return $r_deposito_faltante;

    }

    /**
     * TRASLADADO
     * Genera un enlace para el depósito de aportación, validando el ID de registro y la sesión.
     *
     * La función valida que el ID del registro sea mayor a 0 y que el `session_id` no esté vacío. Luego, genera y devuelve un enlace
     * para el depósito de aportación utilizando los parámetros proporcionados.
     *
     * @param int $registro_id El ID del registro, que debe ser mayor a 0.
     * @param string $session_id El ID de la sesión, que no debe estar vacío.
     *
     * @return string|array Devuelve una cadena con el enlace generado. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function link_deposito_aportacion(int $registro_id, string $session_id)
    {
        if($registro_id <= 0){
            return (new error())->error('Error $registro_id debe ser mayor a 0',$registro_id);
        }
        $session_id = trim($session_id);
        if($session_id ===''){
            return (new error())->error('Error $session_id esta vacia',$session_id);
        }
        $link = "index.php?seccion=pago_corte&accion=deposito_aportacion_por_literal&";
        $link .= "session_id=".$session_id."&registro_id=$registro_id";
        return $link;

    }


    /**
     * TRASLADADO
     * Calcula los montos totales, depositados y pendientes de depósito para un corte de pagos.
     *
     * Esta función realiza los siguientes pasos:
     * 1. Valida los parámetros necesarios para identificar el corte de pagos.
     * 2. Calcula el monto total de los pagos asociados al corte.
     * 3. Calcula el monto ya depositado en el corte.
     * 4. Calcula el monto pendiente por depositar como la diferencia entre el monto total y el monto depositado.
     * 5. Devuelve un objeto con los tres montos calculados.
     *
     * @param string $campo_canceled Nombre del campo que indica si un pago está cancelado.
     * @param string $campo_monto_pago Nombre del campo que contiene el monto del pago.
     * @param string $campo_movto Nombre del campo que representa el movimiento en la tabla `pago`.
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $pago_corte_id ID del corte de pagos. Debe ser mayor a 0.
     *
     * @return stdClass|array Devuelve un objeto con los montos calculados (`monto_total`, `monto_depositado`, `monto_por_depositar`)
     *                        o un arreglo con información de error en caso de fallo.
     *
     * El objeto de montos tiene la siguiente estructura:
     * - `monto_total`: Monto total de los pagos asociados al corte.
     * - `monto_depositado`: Monto ya depositado en el corte.
     * - `monto_por_depositar`: Monto pendiente por depositar.
     *
     * Errores posibles:
     * - `['error' => 'Error al validar pago corte', 'detalle' => $valida]` si falla la validación de los parámetros.
     * - `['error' => 'Error al obtener monto', 'detalle' => $monto_total]` si ocurre un fallo al calcular el monto total.
     * - `['error' => 'Error al obtener monto', 'detalle' => $monto_depositado]` si ocurre un fallo al calcular el monto depositado.
     */
    private function montos(string $campo_canceled, string $campo_monto_pago, string $campo_movto,PDO $link,
                                 int $pago_corte_id)
    {

        $valida = (new _valida())->valida_pago_corte($campo_monto_pago,$campo_movto,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al validar pago corte',$valida);
        }

        $monto_total = (new pago())->sum_pagos_por_corte($campo_canceled,
            $campo_monto_pago,$campo_movto, $link,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener monto',$monto_total);
        }

        $deposito_aportaciones_dat = new deposito_aportaciones();
        $monto_depositado = $deposito_aportaciones_dat->sum_depositos_por_corte(
            $link,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener monto',$monto_depositado);
        }

        $monto_total = round($monto_total,2);
        $monto_depositado = round($monto_depositado,2);
        $monto_por_depositar = round($monto_total - $monto_depositado,2);

        $montos = new stdClass();
        $montos->monto_depositado = $monto_depositado;
        $montos->monto_total = $monto_total;
        $montos->monto_por_depositar = $monto_por_depositar;

        return $montos;

    }



    /**
     * TRASLADADO
     * Obtiene la información base de un pago de corte a partir de un depósito de aportaciones y un pago de depósito.
     *
     * La función valida que el ID del depósito de aportaciones sea mayor a 0, luego obtiene los registros del depósito de
     * aportaciones y del pago de corte asociados. Finalmente, agrega el ID del pago de depósito y el monto depositado
     * redondeado a dos decimales al registro de `pago_corte` y lo devuelve. Si ocurre un error durante el proceso, devuelve
     * un array con los detalles del fallo.
     *
     * @param int $deposito_aportaciones_id El ID del depósito de aportaciones, que debe ser mayor a 0.
     * @param int $deposito_pago_id El ID del pago de depósito.
     * @param PDO $link La conexión PDO utilizada para ejecutar las consultas.
     *
     * @return array Devuelve el array con la información del pago de corte, incluyendo el ID del pago de depósito y el monto depositado.
     *               Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    final public function pago_corte_base(int $deposito_aportaciones_id, int $deposito_pago_id, PDO $link): array
    {

        if($deposito_aportaciones_id <= 0){
            return (new error())->error('Error al $deposito_aportaciones_id debe ser mayor a 0',
                $deposito_aportaciones_id);
        }

        $deposito_aportaciones = (new consultas())->registro_bruto(new stdClass(), 'deposito_aportaciones',
            $deposito_aportaciones_id,$link,false);
        if(error::$en_error){
            return (new error())->error('Error al obtener $deposito_aportaciones', $deposito_aportaciones);
        }

        $pago_corte = (new consultas())->registro_bruto(new stdClass(),'pago_corte',
            $deposito_aportaciones->pago_corte_id,$link);
        if(error::$en_error){
           return (new error())->error('Error al obtener $pago_corte',$pago_corte);
        }

        $pago_corte['deposito_pago_id'] = $deposito_pago_id;
        $pago_corte['deposito_aportaciones_monto_depositado'] =
            round($deposito_aportaciones->monto_depositado,2);

        return $pago_corte;

    }

    /**
     * EM3
     * Obtiene registros de pagos por corte incluyendo información de serie mediante uniones SQL.
     *
     * Esta función valida los datos de entrada, construye dinámicamente uniones SQL y campos adicionales
     * relacionados con la serie, y retorna los pagos por corte con la información de la serie asociada.
     *
     * @param string $campo_canceled El nombre del campo que indica si un pago está cancelado. Puede estar vacío.
     * @param string $campo_monto_pago El nombre del campo que representa el monto del pago. No debe estar vacío.
     * @param string $campo_movto El nombre del campo que representa el movimiento del pago. No debe estar vacío.
     * @param string $campo_serie El nombre del campo que representa la serie en la entidad "contrato". No debe estar vacío.
     * @param PDO $link Una conexión PDO válida para ejecutar la consulta.
     * @param int $pago_corte_id El identificador del pago por corte. Debe ser mayor a 0.
     *
     * @return array Devuelve un array de registros obtenidos como objetos. En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener registros de pagos por corte con serie
     * $campo_canceled = 'Canceled';
     * $campo_monto_pago = 'DocTotal';
     * $campo_movto = 'Movto';
     * $campo_serie = 'serie';
     * $pago_corte_id = 123;
     * $resultado = $this->pagos_por_corte_con_serie($campo_canceled, $campo_monto_pago, $campo_movto, $campo_serie, $link, $pago_corte_id);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     (object) ['DocTotal' => 100.0, 'serie' => 'S001'],
     * //     (object) ['DocTotal' => 200.0, 'serie' => 'S002'],
     * // ]
     *
     * // Ejemplo 2: Error por validación fallida
     * $campo_serie = '';
     * $resultado = $this->pagos_por_corte_con_serie($campo_canceled, $campo_monto_pago, $campo_movto, $campo_serie, $link, $pago_corte_id);
     * // Resultado:
     * // ['error' => 'Error $campo_serie esta vacia', 'data' => '']
     * ```
     */
    final public function pagos_por_corte_con_serie(
        string $campo_canceled,
        string $campo_monto_pago,
        string $campo_movto,
        string $campo_serie,
        PDO $link,
        int $pago_corte_id
    ): array {
        // Validar los datos de entrada
        $valida = (new _valida())->valida_pago_corte($campo_monto_pago, $campo_movto, $pago_corte_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar pago corte', $valida);
        }

        // Validar el campo de serie
        $campo_serie = trim($campo_serie);
        if ($campo_serie === '') {
            return (new error())->error('Error $campo_serie esta vacia', $campo_serie);
        }

        // Construir campos adicionales y configuraciones de uniones
        $campos_extra[] = 'contrato.' . $campo_serie;
        $joins[0]['entidad_left'] = 'contrato';
        $joins[0]['entidad_right'] = 'pago';

        // Obtener los pagos por corte
        $pagos = (new pago())->pagos_por_corte(
            $campo_canceled,
            $campo_monto_pago,
            $campo_movto,
            $link,
            $pago_corte_id,
            $campos_extra,
            $joins
        );
        if (error::$en_error) {
            return (new error())->error('Error al obtener $pagos', $pagos);
        }

        // Retornar los pagos obtenidos
        return $pagos;
    }


    /**
     * TRASLADADO
     * Obtiene los registros de pagos por corte.
     *
     * Este método construye una consulta SQL basada en los parámetros y el modelo de datos proporcionados,
     * ejecuta la consulta y retorna los resultados en forma de un arreglo.
     *
     * @param PDO $link Conexión a la base de datos.
     * @param string $order Orden en que se presentarán los resultados de la consulta.
     *                      Ejemplo: 'pago.fecha DESC'.
     * @param int $pago_corte_id ID del corte de pago para filtrar los resultados.
     * @param string $modelo_datos Nombre del modelo de datos que se utilizará para obtener los parámetros.
     *
     * @return array Devuelve un arreglo con los registros de pagos obtenidos.
     *               En caso de error, devuelve un objeto `error`.
     *
     * @throws error Si:
     *               - Fallan las validaciones de los datos de entrada.
     *               - Ocurre un error al generar la consulta SQL.
     *               - Falla la ejecución de la consulta SQL.
     *
     * @example
     * ```php
     * $link = new PDO(...); // Conexión a la base de datos
     * $order = 'pago.fecha DESC';
     * $pago_corte_id = 123;
     * $modelo_datos = 'modelo_pago_corte';
     *
     * $pagos = $this->pagos_por_corte($link, $order, $pago_corte_id, $modelo_datos);
     * if (\desarrollo_em3\error\error::$en_error) {
     *     echo "Error al obtener los pagos por corte.";
     * } else {
     *     print_r($pagos);
     *     // Salida esperada:
     *     // Array con los registros de los pagos por corte.
     * }
     * ```
     */
    final public function pagos_por_corte(PDO $link, string $order, int $pago_corte_id, string $modelo_datos): array
    {
        // Obtiene los parámetros del modelo de datos
        $params = $this->params_modelo($modelo_datos);
        if (error::$en_error) {
            return (new error())->error('Error al obtener params', $params);
        }

        // Valida el corte de pago
        $valida = (new _valida())->valida_pago_corte($params->campo_monto_pago, $params->campo_movto, $pago_corte_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar pago corte', $valida);
        }

        // Valida los datos relacionados con los campos
        $valida = (new \desarrollo_em3\manejo_datos\sql\pago_corte())->valida_data_campos(
            $params->campo_folio,
            $params->campo_monto_pago,
            $params->campo_serie,
            $params->entidad_empleado
        );
        if (error::$en_error) {
            return (new error())->error('Error al validar datos', $valida);
        }

        // Genera la consulta SQL para obtener los pagos por corte
        $sql = (new \desarrollo_em3\manejo_datos\sql\pago_corte())->pagos_por_corte(
            $params->campo_canceled,
            $params->campo_folio,
            $params->campo_monto_pago,
            $params->campo_movto,
            $params->campo_serie,
            $params->entidad_empleado,
            $order,
            $pago_corte_id
        );
        if (error::$en_error) {
            return (new error())->error('Error al obtener $sql', $sql);
        }

        // Ejecuta la consulta y obtiene los resultados
        $rows = (new consultas())->exe_objs($link, $sql);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $rows', $rows);
        }

        return $rows;
    }


    /**
     * EM3
     * Obtiene un conjunto de parámetros estándar según el modelo de datos especificado.
     *
     * Esta función valida el nombre del modelo de datos (`SAP` o `EM3`) y retorna el conjunto de parámetros
     * correspondientes utilizando las funciones `params_sap` o `params_em3`.
     *
     * @param string $modelo_datos El nombre del modelo de datos. Debe ser `SAP` o `EM3`. No debe estar vacío.
     *
     * @return stdClass|array Devuelve un objeto con las configuraciones estándar del modelo de datos. En caso de error,
     *                        devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener parámetros para el modelo SAP
     * $modelo_datos = 'SAP';
     * $resultado = $this->params_modelo($modelo_datos);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [campo_canceled] => 'Canceled'
     * //     [campo_folio] => 'U_FolioCont'
     * //     [campo_monto_pago] => 'DocTotal'
     * //     [campo_movto] => 'U_Movto'
     * //     [campo_serie] => 'U_SeCont'
     * //     [entidad_empleado] => 'ohem'
     * // )
     *
     * // Ejemplo 2: Obtener parámetros para el modelo EM3
     * $modelo_datos = 'EM3';
     * $resultado = $this->params_modelo($modelo_datos);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [campo_canceled] => ''
     * //     [campo_folio] => 'folio'
     * //     [campo_monto_pago] => 'monto'
     * //     [campo_movto] => 'movimiento'
     * //     [campo_serie] => 'serie'
     * //     [entidad_empleado] => 'empleado'
     * // )
     *
     * // Ejemplo 3: Modelo de datos vacío (error)
     * $modelo_datos = '';
     * $resultado = $this->params_modelo($modelo_datos);
     * // Resultado:
     * // ['error' => 'Error $modelo_datos esta vacio', 'data' => '']
     *
     * // Ejemplo 4: Modelo de datos no soportado
     * $modelo_datos = 'OTRO';
     * $resultado = $this->params_modelo($modelo_datos);
     * // Resultado:
     * // ['error' => 'Error al obtener params', 'data' => null]
     * ```
     */
    private function params_modelo(string $modelo_datos)
    {
        // Limpiar y validar el nombre del modelo de datos
        $modelo_datos = trim($modelo_datos);
        if ($modelo_datos === '') {
            return (new error())->error('Error $modelo_datos esta vacio', $modelo_datos);
        }
        $modelo_datos = strtoupper($modelo_datos);

        // Crear el objeto de parámetros
        $params = new stdClass();

        // Obtener los parámetros según el modelo de datos
        if ($modelo_datos === 'SAP') {
            $params = $this->params_sap();
            if (error::$en_error) {
                return (new error())->error('Error al obtener params', $params);
            }
        } elseif ($modelo_datos === 'EM3') {
            $params = $this->params_em3();
            if (error::$en_error) {
                return (new error())->error('Error al obtener params', $params);
            }
        }

        // Retornar los parámetros obtenidos
        return $params;
    }

    /**
     * EM3
     * Genera un conjunto de parámetros estándar para configuraciones relacionadas con EM3.
     *
     * Esta función crea y retorna un objeto (`stdClass`) con las configuraciones predeterminadas para campos
     * y entidades utilizadas en integraciones o consultas relacionadas con el sistema EM3.
     *
     * @return stdClass Devuelve un objeto con las siguientes propiedades:
     *                  - `campo_canceled` (string): Nombre del campo que indica si un registro está cancelado
     *                    (vacío por defecto en esta configuración).
     *                  - `campo_folio` (string): Nombre del campo que representa el folio del registro.
     *                  - `campo_monto_pago` (string): Nombre del campo que representa el monto del pago.
     *                  - `campo_movto` (string): Nombre del campo que representa el movimiento asociado.
     *                  - `campo_serie` (string): Nombre del campo que representa la serie asociada.
     *                  - `entidad_empleado` (string): Nombre de la entidad o tabla relacionada con empleados.
     *
     * @example
     * ```php
     * // Uso de la función params_em3 para obtener parámetros estándar
     * $params = $this->params_em3();
     * print_r($params);
     * // Resultado:
     * // stdClass Object (
     * //     [campo_canceled] => ''
     * //     [campo_folio] => 'folio'
     * //     [campo_monto_pago] => 'monto'
     * //     [campo_movto] => 'movimiento'
     * //     [campo_serie] => 'serie'
     * //     [entidad_empleado] => 'empleado'
     * // )
     * ```
     */
    private function params_em3(): stdClass
    {
        // Crear el objeto de parámetros
        $params = new stdClass();

        // Asignar las propiedades estándar
        $params->campo_canceled = '';
        $params->campo_folio = 'folio';
        $params->campo_monto_pago = 'monto';
        $params->campo_movto = 'movimiento';
        $params->campo_serie = 'serie';
        $params->entidad_empleado = 'empleado';

        // Retornar el objeto de parámetros
        return $params;
    }



    final public function params_rpt(array $datos)
    {
        $params = new stdClass();
        $campos_moneda = (new _reporte_fecha())->campos_moneda($datos);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $campos_moneda', $campos_moneda);
        }
        $params->campos_moneda = $campos_moneda;
        $cuentas_totales = (new _reporte_fecha())->cuentas_totales($datos);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $cuentas_totales', $cuentas_totales);
        }
        $params->cuentas_totales = $cuentas_totales;

        $plazas_string = (new _reporte_fecha())->plazas_rpt_out($datos);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $plazas_string', $plazas_string);
        }
        $params->plazas_string = $plazas_string;

        $totales = (new _reporte_fecha())->totales($datos);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $totales', $totales);
        }
        $params->totales = $totales;

        $datos = (new _reporte_fecha())->datos_rpt($campos_moneda,$datos);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $dato', $datos);
        }
        $params->datos = $datos;

        $cols = (new _reporte_fecha())->cols_rpt($datos);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $cols', $cols);
        }
        $params->cols = $cols;

        return $params;

    }

    /**
     * EM3
     * Genera un conjunto de parámetros estándar para configuraciones relacionadas con SAP.
     *
     * Esta función crea y retorna un objeto (`stdClass`) con las configuraciones predeterminadas para campos
     * y entidades utilizadas en integraciones o consultas relacionadas con SAP.
     *
     * @return stdClass Devuelve un objeto con las siguientes propiedades:
     *                  - `campo_canceled` (string): Nombre del campo que indica si un registro está cancelado.
     *                  - `campo_folio` (string): Nombre del campo que representa el folio del registro.
     *                  - `campo_monto_pago` (string): Nombre del campo que representa el monto del pago.
     *                  - `campo_movto` (string): Nombre del campo que representa el movimiento asociado.
     *                  - `campo_serie` (string): Nombre del campo que representa la serie asociada.
     *                  - `entidad_empleado` (string): Nombre de la entidad o tabla relacionada con empleados.
     *
     * @example
     * ```php
     * // Uso de la función params_sap para obtener parámetros estándar
     * $params = $this->params_sap();
     * print_r($params);
     * // Resultado:
     * // stdClass Object (
     * //     [campo_canceled] => 'Canceled'
     * //     [campo_folio] => 'U_FolioCont'
     * //     [campo_monto_pago] => 'DocTotal'
     * //     [campo_movto] => 'U_Movto'
     * //     [campo_serie] => 'U_SeCont'
     * //     [entidad_empleado] => 'ohem'
     * // )
     * ```
     */
    private function params_sap(): stdClass
    {
        // Crear el objeto de parámetros
        $params = new stdClass();

        // Asignar las propiedades estándar
        $params->campo_canceled = 'Canceled';
        $params->campo_folio = 'U_FolioCont';
        $params->campo_monto_pago = 'DocTotal';
        $params->campo_movto = 'U_Movto';
        $params->campo_serie = 'U_SeCont';
        $params->entidad_empleado = 'ohem';

        // Retornar el objeto de parámetros
        return $params;
    }


    /**
     * TRASLADADO
     * Recalcula y actualiza los montos de cortes de pago que no han sido validados.
     *
     * Esta función obtiene una lista de cortes de pago que no han sido validados, basada en un límite especificado,
     * y luego recalcula y actualiza los montos para cada uno de esos cortes. Para cada corte, se validan los datos
     * antes de realizar el recálculo. Si alguna validación, obtención de cortes, recálculo o actualización falla,
     * se devuelve un array con los detalles del error.
     *
     * @param string $campo_canceled Nombre del campo que indica si el pago ha sido cancelado.
     * @param string $campo_monto_pago Nombre del campo que representa el monto del pago.
     * @param string $campo_movto Nombre del campo que representa el movimiento.
     * @param PDO $link Objeto PDO para la conexión a la base de datos.
     * @param int $limit [opcional] Número máximo de cortes sin validar a procesar. El valor predeterminado es 10.
     *
     * @return array Devuelve un array con los cortes procesados si el recálculo y actualización son exitosos,
     *               o un array de error si alguna validación, obtención de datos, recálculo o actualización falla.
     */
    final public function recalcula_cortes_sin_validar(string $campo_canceled, string $campo_monto_pago,
                                                       string $campo_movto, PDO $link, int $limit = 10): array
    {
        if($limit < 0){
            return (new error())->error('Error limit debe ser mayor a 0', $limit);
        }
        $cortes = $this->cortes_sin_validar($link,$limit);
        if(error::$en_error){
            return(new error())->error('Error al obtener cortes',$cortes);
        }

        foreach ($cortes as $corte){
            $valida = (new _valida())->valida_pago_corte($campo_monto_pago,$campo_movto,$corte->id);
            if(error::$en_error){
                return (new error())->error('Error al validar pago corte',$valida);
            }

            $recalcula = $this->recalcula_montos($campo_canceled,$campo_monto_pago,$campo_movto,$link,$corte->id);
            if(error::$en_error){
                return (new error())->error('Error al recalcular',$recalcula);
            }
        }

        return $cortes;

    }

    /**
     * TRASLADADO
     * Recalcula los montos totales, depositados y pendientes de depósito para un corte de pagos y actualiza los valores en la base de datos.
     *
     * Esta función realiza los siguientes pasos:
     * 1. Valida los parámetros necesarios para identificar el corte de pagos.
     * 2. Calcula los montos totales, depositados y pendientes de depósito utilizando la función `montos`.
     * 3. Actualiza los montos calculados en la base de datos para el corte de pagos especificado.
     * 4. Devuelve el resultado de la actualización.
     *
     * @param string $campo_canceled Nombre del campo que indica si un pago está cancelado.
     * @param string $campo_monto_pago Nombre del campo que contiene el monto del pago.
     * @param string $campo_movto Nombre del campo que representa el movimiento en la tabla `pago`.
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $pago_corte_id ID del corte de pagos. Debe ser mayor a 0.
     *
     * @return array Devuelve un arreglo con el resultado de la actualización o un arreglo con información de error en caso de fallo.
     *
     * Errores posibles:
     * - `['error' => 'Error al validar pago corte', 'detalle' => $valida]` si falla la validación de los parámetros.
     * - `['error' => 'Error al obtener montos', 'detalle' => $montos]` si ocurre un fallo al calcular los montos.
     * - `['error' => 'Error al actualizar montos', 'detalle' => $exe]` si ocurre un fallo al actualizar los montos en la base de datos.
     */
    final public function recalcula_montos(string $campo_canceled, string $campo_monto_pago, string $campo_movto,
                                           PDO $link, int $pago_corte_id): array
    {


        $valida = (new _valida())->valida_pago_corte($campo_monto_pago,$campo_movto,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al validar pago corte',$valida);
        }

        $montos = $this->montos($campo_canceled, $campo_monto_pago,$campo_movto,$link,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener montos',$montos);
        }

        $exe = $this->update_montos($link,
            $montos->monto_depositado,$montos->monto_por_depositar,$montos->monto_total,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al actualizar montos',$exe);
        }

        return $exe;

    }

    final public function row_alta(string $hoy, string $movimiento, int $ohem_id, int $plaza_id): array
    {
        $registro['ohem_id'] = $ohem_id;
        $registro['fecha'] = $hoy;
        $registro['plaza_id'] = $plaza_id;
        $registro['status'] = 'activo';
        $registro['movimiento'] = $movimiento;
        return $registro;

    }

    /**
     * TRASLADADO
     * Modifica un array de depósitos faltantes desactivando la inserción si la serie coincide con el contrato.
     *
     * La función valida que los parámetros de entrada estén correctamente definidos y que no estén vacíos.
     * Si el código de la serie coincide con el campo del contrato correspondiente, desactiva la inserción de depósitos
     * para el índice especificado dentro del array de depósitos faltantes.
     *
     * @param string $campo_serie El nombre del campo de la serie que se está verificando.
     * @param array $faltante El array que contiene los datos del contrato faltante.
     * @param int $index El índice dentro del array `$r_deposito_faltante` que será modificado si la condición se cumple.
     * @param array $r_deposito_faltante El array que contiene los datos de los depósitos faltantes.
     * @param array $serie El array que contiene la información de la serie, incluyendo la clave `serie_codigo`.
     *
     * @return array Devuelve el array de depósitos faltantes modificado. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function row_deposito_literal(
        string $campo_serie, array $faltante, int $index, array $r_deposito_faltante, array $serie): array
    {
        if(!isset($serie['serie_codigo'])){
            return (new error())->error('Error $serie[serie_codigo] no existe',$serie);
        }
        $serie_codigo = trim($serie['serie_codigo']);
        if($serie_codigo === ''){
            return (new error())->error('Error $serie[serie_codigo] esta vacia',$serie);
        }
        $campo_serie = trim($campo_serie);
        if($campo_serie === ''){
            return (new error())->error('Error $campo_serie esta vacia',$campo_serie);
        }

        if(!isset($faltante["contrato_$campo_serie"])){
            return (new error())->error('Error $faltante["contrato_'.$campo_serie.'"]',$faltante);
        }
        if($index < 0){
            return (new error())->error('Error index debe ser mayor o igual a 0',$index);
        }

        if($serie_codigo === $faltante["contrato_$campo_serie"]){
            $r_deposito_faltante[$index]['inserta_deposito'] = 'inactivo';
        }
        return $r_deposito_faltante;

    }

    final public function rpt_corte_por_dia(bool $download, array $empleados_id, string $entidad_empleado,$fecha_final,
                                            $fecha_inicial, string $format,PDO $link, string $name_out, array $plazas_id, string $url_file)
    {
        $empleados_rpt = $this->get_empleados_rpt(
            $empleados_id,$entidad_empleado,$fecha_final,$fecha_inicial, $format, $link, $plazas_id);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $empleados_rpt', $empleados_rpt);
        }
        $out = $empleados_rpt;
        if($format === 'CSV') {
            $out = (new formato())->out_csv($download, $empleados_rpt, $name_out, $url_file);
            if (error::$en_error) {
                return (new error())->error('Error al dat salida de csv', $out);
            }
        }
        if($format === 'JSON'){

            foreach ($empleados_rpt as $indice=>$empleado) {
                $empleado['fecha_inicial'] = $fecha_inicial;
                $empleado['fecha_final'] = $fecha_final;
                $empleados_rpt[$indice] = $empleado;
            }

            $out = (new formato())->out_json($download, $empleados_rpt);
            if (error::$en_error) {
                return (new error())->error('Error al dat salida de csv', $out);
            }
        }
        return $out;

    }

    /**
     * TRASLADADO
     * Verifica si todos los depósitos en una lista están validados.
     *
     * Esta función recorre un array de depósitos de aportaciones y verifica si todos están validados.
     * Si alguno de los depósitos no está validado (es decir, su campo `validado` es 'inactivo'),
     * la función devuelve `false`. Si no existe el campo `validado` en un depósito, se devuelve
     * un array con los detalles del error. Si todos los depósitos están validados, devuelve `true`.
     *
     * @param array $deposito_aportaciones Array de depósitos de aportaciones, cada uno puede ser un objeto o un array.
     *
     * @return bool|array Devuelve `true` si todos los depósitos están validados, `false` si alguno no lo está,
     *                    o un array de error si falta el campo `validado` en alguno de los depósitos.
     */
    private function todos_validados(array $deposito_aportaciones)
    {
        $todos_validados = true;
        if(count($deposito_aportaciones) === 0){
            $todos_validados = false;
        }
        foreach ($deposito_aportaciones as $deposito_aportacion){
            if(is_array($deposito_aportacion)){
                $deposito_aportacion = (object)$deposito_aportacion;
            }
            if(!isset($deposito_aportacion->validado)){
                return (new error())->error('Error no existe $deposito_aportacion->validado',
                    $deposito_aportacion);
            }

            if($deposito_aportacion->validado === 'inactivo'){
                $todos_validados = false;
                break;
            }

        }

        return $todos_validados;

    }

    /**
     * TRASLADADO
     * Actualiza los montos de un corte de pago en la base de datos.
     *
     * Esta función valida que el ID del pago de corte sea mayor que 0, luego construye y ejecuta
     * una consulta SQL `UPDATE` para actualizar los campos `monto_depositado`, `monto_total`, y
     * `monto_por_depositar` en la tabla `pago_corte` para el registro especificado por el ID del
     * corte de pago. Si alguna validación, construcción de la consulta o ejecución de la transacción
     * falla, se devuelve un array con los detalles del error.
     *
     * @param PDO $link Objeto PDO para la conexión a la base de datos.
     * @param float $monto_depositado El nuevo valor para el campo `monto_depositado`.
     * @param float $monto_por_depositar El nuevo valor para el campo `monto_por_depositar`.
     * @param float $monto_total El nuevo valor para el campo `monto_total`.
     * @param int $pago_corte_id ID del pago de corte que se está actualizando.
     *
     * @return array Devuelve un array con los resultados de la ejecución de la consulta SQL,
     *               o un array de error si alguna validación, construcción de la consulta o
     *               ejecución de la transacción falla.
     */
    private function update_montos(PDO $link, float $monto_depositado, float $monto_por_depositar,
                                        float $monto_total, int $pago_corte_id): array
    {
        if($pago_corte_id <= 0){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0',$pago_corte_id);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\pago_corte())->update_montos($monto_depositado,
            $monto_por_depositar,$monto_total,$pago_corte_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }
        $exe = (new transacciones($link))->ejecuta_sql($sql);
        if(error::$en_error){
            return (new error())->error('Error al actualizar monto',$exe);
        }
        return $exe;

    }


    /**
     * TRASLADADO
     * Valida el corte de pago y actualiza su estado si se ha aplicado la validación correctamente.
     *
     * La función verifica que el ID del corte de pago sea mayor a 0 y luego aplica la validación del corte de pago.
     * Si la validación es exitosa, se genera una consulta SQL para actualizar el estado del corte de pago a validado y
     * ejecuta dicha consulta. Si ocurre algún error en el proceso, se devuelve un array con los detalles del error.
     *
     * @param PDO $link La conexión PDO utilizada para ejecutar las consultas.
     * @param int $pago_corte_id El ID del corte de pago que se está validando. Debe ser mayor a 0.
     *
     * @return bool|array Devuelve `true` si el corte de pago se valida y actualiza correctamente.
     *                    Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function valida_corte(PDO $link, int $pago_corte_id)
    {
        if($pago_corte_id <=0 ){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0', $pago_corte_id);
        }
        $aplica_validado = $this->aplica_validacion($link,$pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $aplica_validado', $aplica_validado);
        }

        if($aplica_validado){
            $sql = (new \desarrollo_em3\manejo_datos\sql\pago_corte())->update_validado($pago_corte_id);
            if(error::$en_error){
                return (new error())->error('Error al obtener $sql', $sql);
            }
            $exe = (new transacciones($link))->ejecuta_sql($sql);
            if(error::$en_error){
                return (new error())->error('Error al ejecutar $sql', $exe);
            }
        }

        return $aplica_validado;

    }





    /**
     * TRASLADADO
     * Valida el corte de pago y devuelve el registro actualizado del mismo.
     *
     * La función valida que el ID del corte de pago sea mayor a 0 y luego procede a validar el corte de pago
     * llamando a la función `valida_corte`. Si la validación es exitosa, obtiene el registro actualizado del
     * corte de pago desde la base de datos. Si ocurre algún error durante el proceso, se devuelve un array con
     * los detalles del error.
     *
     * @param PDO $link La conexión PDO utilizada para ejecutar las consultas.
     * @param int $pago_corte_id El ID del corte de pago que se está validando. Debe ser mayor a 0.
     *
     * @return stdClass|array Devuelve el registro actualizado del corte de pago como un objeto `stdClass`.
     *                        Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    final public function validar_pago_corte(PDO $link, int $pago_corte_id)
    {
        if($pago_corte_id <= 0){
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0', $pago_corte_id);
        }
        $rs = $this->valida_corte($link, $pago_corte_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $rs', $rs);
        }

        $pago_corte = (new consultas())->registro_bruto(new stdClass(),'pago_corte', $pago_corte_id,$link,
            false);
        if(error::$en_error){
            return (new error())->error('Error al obtener pago_corte',$pago_corte);
        }

        return $pago_corte;

    }




}
