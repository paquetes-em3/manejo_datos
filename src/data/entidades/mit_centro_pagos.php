<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use PDO;
use stdClass;

class mit_centro_pagos
{

    final public function existen_pagos_aprobados(int $contrato_id, int $ejercicio, PDO $link, int $numero_semana)
    {
        $rs = $this->obten_mit_pagos_referencia($contrato_id, $ejercicio, $link, $numero_semana,'approved');
        if(error::$en_error){
            return (new error())->error('Error al obtener pagos aprovados',$rs);
        }
        $rs_array = (array)$rs;
        $existe = false;
        if(count($rs_array) > 0){
            $existe = true;
        }
        return $existe;

    }

    private function obten_mit_pagos_referencia(int $contrato_id, int $ejercicio, PDO $link, int $numero_semana,
                                                string $state)
    {
        if($contrato_id <= 0){
            return (new error())->error('Error contrato_id debe ser mayor a 0',$contrato_id);
        }

        if($ejercicio <= 0){
            return (new error())->error('Error $ejercicio debe ser mayor a 0',$ejercicio);
        }

        if($numero_semana <= 0){
            return (new error())->error('Error $numero_semana debe ser mayor a 0',$numero_semana);
        }
        $state = trim($state);

        $sql = (new \desarrollo_em3\manejo_datos\sql\mit_centro_pagos())->obten_mit_pagos_referencia($contrato_id,
            $ejercicio, $numero_semana, $state);
        if(error::$en_error){
            return (new error())->error('Error al obtener sql', $sql);
        }
        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al ejecutar sql', $exe);
        }
        $rs = new stdClass();
        if(count($exe) > 0){
            $rs = $exe[0];
        }
        return $rs;
    }



}
