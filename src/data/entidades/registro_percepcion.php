<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;
use PDO;

class registro_percepcion {

    /**
     * TRASLADADO
     * Obtiene información detallada de un registro de percepción específico.
     *
     * Esta función genera una consulta SQL para buscar un registro de percepción por su ID y entidad asociada,
     * ejecuta la consulta en la base de datos y retorna el resultado.
     *
     * @param string $entidad_empleado Nombre de la entidad (tabla) asociada al empleado.
     *                                 Por ejemplo: `'empleados'`.
     * @param PDO $link Conexión a la base de datos.
     * @param int $registro_percepcion_id ID del registro de percepción que se desea obtener.
     *
     * @return array Devuelve un arreglo con los resultados obtenidos del registro de percepción.
     *               En caso de error, devuelve un arreglo con información detallada del fallo.
     *
     * Ejemplo de uso:
     * ```php
     * $link = new PDO(...); // Conexión a la base de datos
     * $entidad_empleado = 'empleados';
     * $registro_percepcion_id = 123;
     * $result = $this->obten_registro_percepcion($entidad_empleado, $link, $registro_percepcion_id);
     * if (isset($result['error'])) {
     *     echo $result['error'];
     * } else {
     *     print_r($result); // Datos del registro de percepción
     * }
     * ```
     *
     * Salida esperada (ejemplo):
     * ```php
     * [
     *     'id' => 123,
     *     'empleado_id' => 456,
     *     'percepcion' => 'Bonificación',
     *     'monto' => 1000.00,
     *     ...
     * ]
     * ```
     *
     * Proceso:
     * 1. Valida que `$registro_percepcion_id` sea mayor o igual a 0.
     * 2. Genera una consulta SQL para buscar el registro de percepción en la entidad especificada.
     * 3. Ejecuta la consulta en la base de datos.
     * 4. Retorna los resultados de la consulta.
     *
     * Errores posibles:
     * - `['error' => 'Error $registro_percepcion_id debe ser mayor a 0', 'detalle' => $registro_percepcion_id]`
     *   si `$registro_percepcion_id` es menor que 0.
     * - `['error' => 'Error al obtener $sql', 'detalle' => $sql]` si ocurre un error al generar la consulta SQL.
     * - `['error' => 'Error al obtener relaciones', 'detalle' => $exe]` si ocurre un error al ejecutar la consulta SQL.
     */
    final public function obten_registro_percepcion(
        string $entidad_empleado, PDO $link, int  $registro_percepcion_id): array
    {

        if($registro_percepcion_id < 0) {
            return (new error())->error(
                'Error $registro_percepcion_id debe ser mayor a 0',$registro_percepcion_id);
        }

        $sql = (new sql\registro_percepcion())->obten_registro_percepcion($entidad_empleado, $registro_percepcion_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }


        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }

        return $exe;
    }
}