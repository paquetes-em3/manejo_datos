<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\error\valida;
use desarrollo_em3\manejo_datos\consultas;
use PDO;
use stdClass;

class deposito_pago
{

    private string $key_base_id = 'deposito_pago_id';



    /**
     * TRASLADADO
     * Prepara un array con los datos necesarios para insertar un depósito de pago.
     *
     * Esta función valida los datos en el arreglo `$_POST` para asegurarse de que los campos `pago_id` y
     * `deposito_aportaciones_id` sean números positivos. Luego, valida que el campo de monto proporcionado
     * en el array `$pago` sea un número válido. Si alguna validación falla, devuelve un array con los detalles
     * del error. Si todo es correcto, devuelve un array con los datos preparados para la inserción del depósito de pago.
     *
     * @param string $campo_monto Nombre del campo que representa el monto depositado.
     * @param array $pago Array con los datos del pago, que incluye el monto.
     *
     * @return array Devuelve un array con los datos `pago_id`, `deposito_aportaciones_id` y `monto_depositado`
     *               listos para la inserción, o un array de error si ocurre algún problema durante la validación.
     */
    private function deposito_pago_ins(string $campo_monto, array $pago): array
    {
        $keys = array('pago_id','deposito_aportaciones_id');
        $valida = (new valida())->valida_numbers_positivos($keys,$_POST);
        if(error::$en_error){
            return (new error())->error('Error al validar POST',$valida);
        }
        $campo_monto = trim($campo_monto);
        if($campo_monto === ''){
            return (new error())->error('Error $campo_monto esta vacio',$campo_monto);
        }

        $keys = array($campo_monto);
        $valida = (new valida())->valida_numbers($keys,$pago);
        if(error::$en_error){
            return (new error())->error('Error al validar $pago',$valida);
        }
        $deposito_pago_ins['pago_id'] = $_POST['pago_id'];
        $deposito_pago_ins['deposito_aportaciones_id'] = $_POST['deposito_aportaciones_id'];
        $deposito_pago_ins['monto_depositado'] = $pago[$campo_monto];

        return $deposito_pago_ins;

    }


    final public function deposito_pago_ins_base(
        string $campo_monto, int $deposito_aportaciones_id, stdClass $pago): array
    {
        $campo_monto = trim($campo_monto);
        if($campo_monto === ''){
            return (new error())->error('Error $campo_monto esta vacio',$campo_monto);
        }
        $key_monto_pago = 'pago_'.$campo_monto;

        if(!isset($pago->pago_id)){
            return (new error())->error('Error $pago->pago_id no existe',$pago);
        }
        if(!isset($pago->$key_monto_pago)){
            return (new error())->error('Error $pago->$key_monto_pago no existe',$pago);
        }
        if(!is_numeric($pago->pago_id)){
            return (new error())->error('Error $pago->pago_id debe ser un numero',$pago);
        }
        if(!is_numeric($pago->$key_monto_pago)){
            return (new error())->error('Error $pago->$key_monto_pago debe ser un numero',$pago);
        }
        if($pago->pago_id <= 0){
            return (new error())->error('Error $pago->pago_id debe ser mayor a 0',$pago);
        }
        if($deposito_aportaciones_id <= 0){
            return (new error())->error('Error $deposito_aportaciones_id debe ser mayor a 0',
                $deposito_aportaciones_id);
        }
        $deposito_pago_ins['pago_id'] = $pago->pago_id;
        $deposito_pago_ins['deposito_aportaciones_id'] = $deposito_aportaciones_id;
        $deposito_pago_ins['monto_depositado'] = $pago->$key_monto_pago;

        return $deposito_pago_ins;

    }


    final public function existe_deposito_pago(int $pago_id, PDO $link)
    {
        if($pago_id <= 0){
            return (new error())->error('Error $pago_id es menor a 0',$pago_id);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\deposito_pago())->existe_deposito_pago($pago_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al ejecutar $sql',$exe);
        }

        $existe = false;
        if((int)$exe[0]->n_registros > 0){
            $existe = true;
        }
        return $existe;

    }




    final public function genera_deposito_pago_ins(string $campo_monto, PDO $link): array
    {
        $keys = array('pago_id','deposito_aportaciones_id');
        $valida = (new valida())->valida_numbers_positivos($keys,$_POST);
        if(error::$en_error){
            return (new error())->error('Error al validar POST',$valida);
        }
        $campo_monto = trim($campo_monto);
        if($campo_monto === ''){
            return (new error())->error('Error $campo_monto esta vacio',$campo_monto);
        }

        $pago = (new consultas())->registro_bruto(new stdClass(),'pago', $_POST['pago_id'],$link);
        if(error::$en_error){
            return (new error())->error('Error al obtener',$pago);
        }
        $deposito_pago_ins = $this->deposito_pago_ins($campo_monto,$pago);
        if(error::$en_error){
            return (new error())->error('Error al integrar $deposito_pago_ins',$deposito_pago_ins);
        }
        return $deposito_pago_ins;


    }

    /**
     * TRASLADADO
     * Calcula la suma de los montos depositados para una aportación específica.
     *
     * Este método calcula la suma de todos los depósitos asociados a un
     * `deposito_aportacion_id` específico, filtrando aquellos que estén activos.
     * La suma se obtiene de la columna `monto_depositado` de la tabla `deposito_pago`.
     * Si el ID es menor o igual a 0, o si ocurre un error durante el proceso, se
     * devuelve un error.
     *
     * @param int $deposito_aportacion_id El ID del depósito de aportación a sumar.
     * @param PDO $link La conexión PDO a la base de datos.
     *
     * @return float|array El total sumado de los montos depositados, redondeado a
     *                     dos decimales. En caso de error, devuelve un arreglo con
     *                     la información del error.
     */
    final public function sum_depositos_por_aportacion(int $deposito_aportacion_id, PDO $link)
    {
        if($deposito_aportacion_id <= 0){
            return (new error())->error('Error $deposito_aportacion_id debe ser mayor a 0',$deposito_aportacion_id);
        }
        $filtro_sql = (new \desarrollo_em3\manejo_datos\sql\deposito_pago())->depositos_por_aport_filtro(
            $deposito_aportacion_id);
        if(error::$en_error){
            return(new error())->error('Error al obtener $filtro_sql',$filtro_sql);
        }
        $total = (new consultas())->sum_by_filter('monto_depositado','deposito_pago',
            $filtro_sql,$link);
        if(error::$en_error){
            return(new error())->error('Error al obtener $total',$total);
        }

        return round($total,2);

    }




}
