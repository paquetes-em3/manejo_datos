<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use PDO;
use stdClass;


class costo_estimado_det
{

    /**
     * REG
     * Genera un concepto de factura basado en los datos de entrada y los montos proporcionados.
     *
     * Esta función valida los datos de la factura y los montos antes de generar un array con la información estructurada.
     * Se realizan verificaciones para asegurar que los valores esenciales estén definidos y sean numéricos.
     *
     * @param stdClass $datos_fc Objeto con la información de la factura.
     *                           - Debe contener `descripcion` como una cadena no vacía.
     *                           - Ejemplo:
     *                             ```php
     *                             $datos_fc = new stdClass();
     *                             $datos_fc->descripcion = "Servicio de mantenimiento";
     *                             ```
     * @param int $factura_id ID de la factura a la que se asociará el concepto.
     *                        - Debe ser un número entero mayor a 0.
     *                        - Ejemplo: `1001`
     * @param stdClass $montos Objeto con los valores monetarios del concepto.
     *                         - Debe contener:
     *                           - `precio_unitario`: Número mayor a 0.
     *                           - `iva`: Número válido.
     *                         - Ejemplo:
     *                           ```php
     *                           $montos = new stdClass();
     *                           $montos->precio_unitario = 500.00;
     *                           $montos->iva = 80.00;
     *                           ```
     *
     * @return array Devuelve un array con la información estructurada del concepto de factura si todo es válido.
     *                     En caso de error, retorna un array con un mensaje de error.
     *
     * @example Uso correcto
     * ```php
     * $datos_fc = new stdClass();
     * $datos_fc->descripcion = "Servicio de consultoría";
     *
     * $factura_id = 2001;
     *
     * $montos = new stdClass();
     * $montos->precio_unitario = 1500.75;
     * $montos->iva = 240.12;
     *
     * $resultado = (new costo_estimado_det())->concepto_ins($datos_fc, $factura_id, $montos);
     * print_r($resultado);
     * // Output esperado:
     * // Array (
     * //     [factura_id] => 2001
     * //     [concepto_id] => 1
     * //     [descripcion] => "Servicio de consultoría"
     * //     [cantidad] => 1
     * //     [precio_unitario] => 1500.75
     * //     [importe] => 1500.75
     * //     [descuento] => 0
     * //     [iva_trasladado] => 240.12
     * //     [iva_retenido] => 0
     * //     [isr_retenido] => 0
     * //     [ieps_trasladado] => 0
     * //     [subtotal] => 1500.75
     * //     [status] => "activo"
     * // )
     * ```
     *
     * @example Caso de error: `descripcion` no existe
     * ```php
     * $datos_fc = new stdClass(); // Falta la propiedad `descripcion`
     *
     * $factura_id = 2001;
     *
     * $montos = new stdClass();
     * $montos->precio_unitario = 1500.75;
     * $montos->iva = 240.12;
     *
     * $resultado = (new costo_estimado_det())->concepto_ins($datos_fc, $factura_id, $montos);
     * print_r($resultado);
     * // Output esperado:
     * // Array (
     * //     [error] => 'Error $datos_fc->descripcion no existe'
     * //     [data] => stdClass Object ()
     * // )
     * ```
     *
     * @example Caso de error: `precio_unitario` no es un número
     * ```php
     * $datos_fc = new stdClass();
     * $datos_fc->descripcion = "Servicio de consultoría";
     *
     * $factura_id = 2001;
     *
     * $montos = new stdClass();
     * $montos->precio_unitario = "texto"; // Valor inválido
     * $montos->iva = 240.12;
     *
     * $resultado = (new costo_estimado_det())->concepto_ins($datos_fc, $factura_id, $montos);
     * print_r($resultado);
     * // Output esperado:
     * // Array (
     * //     [error] => 'Error $montos->precio_unitario debe ser un numero'
     * //     [data] => stdClass Object ()
     * // )
     * ```
     */
    final public function concepto_ins(stdClass $datos_fc, int $factura_id, stdClass $montos): array
    {
        // Validaciones de la descripción
        if (!isset($datos_fc->descripcion)) {
            return (new error())->error('Error $datos_fc->descripcion no existe', $datos_fc);
        }
        $datos_fc->descripcion = trim($datos_fc->descripcion);
        if ($datos_fc->descripcion === '') {
            return (new error())->error('Error $datos_fc->descripcion está vacía', $datos_fc);
        }

        // Validaciones de montos
        if (!isset($montos->precio_unitario)) {
            return (new error())->error('Error $montos->precio_unitario no existe', $montos);
        }
        if (!isset($montos->iva)) {
            return (new error())->error('Error $montos->iva no existe', $montos);
        }
        if (!is_numeric($montos->precio_unitario)) {
            return (new error())->error('Error $montos->precio_unitario debe ser un número', $montos);
        }
        if (!is_numeric($montos->iva)) {
            return (new error())->error('Error $montos->iva debe ser un número', $montos);
        }

        // Redondeo y validación final de precio unitario
        $montos->precio_unitario = round($montos->precio_unitario, 2);
        if ($montos->precio_unitario <= 0) {
            return (new error())->error('Error $montos->precio_unitario debe ser mayor a 0', $montos);
        }

        // Construcción del array con la información del concepto
        return [
            'factura_id' => $factura_id,
            'concepto_id' => 1,
            'descripcion' => $datos_fc->descripcion,
            'cantidad' => 1,
            'precio_unitario' => $montos->precio_unitario,
            'importe' => $montos->precio_unitario,
            'descuento' => 0,
            'iva_trasladado' => $montos->iva,
            'iva_retenido' => 0,
            'isr_retenido' => 0,
            'ieps_trasladado' => 0,
            'subtotal' => $montos->precio_unitario,
            'status' => 'activo',
        ];
    }


    final public function contratos_evento()
    {

        $contratos_genera = $this->contratos_genera_init();
        if(error::$en_error){
            return (new error())->error('Error al obtener $contratos_genera', $contratos_genera);
        }

        $contratos_timbra = $this->contratos_timbra_init();
        if(error::$en_error){
            return (new error())->error('Error al obtener $contratos_timbra', $contratos_timbra);
        }

        $contratos = $this->contratos_unicos($contratos_genera,$contratos_timbra);
        if(error::$en_error){
            return (new error())->error('Error al obtener $contratos', $contratos);
        }

        $data = new stdClass();
        $data->contratos_genera = $contratos->contratos_genera;
        $data->contratos_timbra = $contratos->contratos_timbra;

        return $data;

    }

    /**
     * REG
     * Filtra o reemplaza el array de contratos a generar en función del evento recibido.
     *
     * Si el evento es `"genera"` y `$_GET['contrato_id']` está definido, se crea un nuevo array
     * con ese contrato, reemplazando los valores previos de `$contratos_genera`.
     * En caso contrario, se devuelve el array original sin modificaciones.
     *
     * @param array $contratos_genera Array con los contratos a generar.
     * @param string $evento Cadena que indica el tipo de evento. Si es `"genera"` y `$_GET['contrato_id']` está presente,
     *                       se reinicializa el array con dicho contrato.
     *
     * @return array Retorna un array actualizado con los contratos a generar según el evento.
     *
     * @example (Caso donde `$_GET['contrato_id']` redefine los contratos)
     * ```php
     * $_GET['contrato_id'] = 1234;
     * $contratos_genera = [2001, 2002];
     *
     * $resultado = (new costo_estimado_det())->contratos_genera($contratos_genera, 'genera');
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [0] => 1234
     * // )
     * ```
     *
     * @example (Caso donde el evento no es `"genera"`, se conserva el array original)
     * ```php
     * $_GET['contrato_id'] = 1234;
     * $contratos_genera = [2001, 2002];
     *
     * $resultado = (new costo_estimado_det())->contratos_genera($contratos_genera, 'timbra');
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [0] => 2001
     * //     [1] => 2002
     * // )
     * ```
     *
     * @example (Caso sin `$_GET['contrato_id']`, no se modifica el array)
     * ```php
     * $_GET = []; // No se define 'contrato_id'
     * $contratos_genera = [2001, 2002];
     *
     * $resultado = (new costo_estimado_det())->contratos_genera($contratos_genera, 'genera');
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [0] => 2001
     * //     [1] => 2002
     * // )
     * ```
     */
    private function contratos_genera(array $contratos_genera, string $evento): array
    {
        $evento = trim($evento);

        if (isset($_GET['contrato_id']) && $evento === 'genera') {
            $contratos_genera = [];
            $contratos_genera[] = $_GET['contrato_id'];
        }

        return $contratos_genera;
    }


    /**
     * REG
     * Inicializa el array de contratos para generación de documentos.
     *
     * Esta función verifica si existe el parámetro `contratos_genera` en `$_POST`. Si existe,
     * asigna su valor al array `$contratos_genera`. En caso contrario, retorna un array vacío.
     *
     * @return array Retorna un array con los contratos a generar si el parámetro está presente en `$_POST`,
     *               de lo contrario, retorna un array vacío.
     *
     * @example (Caso con `$_POST['contratos_genera']` definido)
     * ```php
     * $_POST['contratos_genera'] = [1001, 1002, 1003];
     *
     * $resultado = (new costo_estimado_det())->contratos_genera_init();
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [0] => 1001
     * //     [1] => 1002
     * //     [2] => 1003
     * // )
     * ```
     *
     * @example (Caso sin `$_POST['contratos_genera']`)
     * ```php
     * $_POST = []; // No se define 'contratos_genera'
     *
     * $resultado = (new costo_estimado_det())->contratos_genera_init();
     * print_r($resultado);
     * // Output:
     * // Array ()
     * ```
     */
    private function contratos_genera_init(): array
    {
        $contratos_genera = [];

        if (isset($_POST['contratos_genera'])) {
            $contratos_genera = $_POST['contratos_genera'];
        }

        return $contratos_genera;
    }


    /**
     * REG
     * Filtra o reemplaza el array de contratos a timbrar en función del evento recibido.
     *
     * Si el evento es `"timbra"` y `$_GET['contrato_id']` está definido, se crea un nuevo array
     * con ese contrato, reemplazando los valores previos de `$contratos_timbra`.
     * En caso contrario, se devuelve el array original sin modificaciones.
     *
     * @param array $contratos_timbra Array con los contratos a timbrar.
     * @param string $evento Cadena que indica el tipo de evento. Si es `"timbra"` y `$_GET['contrato_id']` está presente,
     *                       se reinicializa el array con dicho contrato.
     *
     * @return array Retorna un array actualizado con los contratos a timbrar según el evento.
     *
     * @example (Caso donde `$_GET['contrato_id']` redefine los contratos a timbrar)
     * ```php
     * $_GET['contrato_id'] = 5678;
     * $contratos_timbra = [3001, 3002];
     *
     * $resultado = (new costo_estimado_det())->contratos_timbra($contratos_timbra, 'timbra');
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [0] => 5678
     * // )
     * ```
     *
     * @example (Caso donde el evento no es `"timbra"`, se conserva el array original)
     * ```php
     * $_GET['contrato_id'] = 5678;
     * $contratos_timbra = [3001, 3002];
     *
     * $resultado = (new costo_estimado_det())->contratos_timbra($contratos_timbra, 'genera');
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [0] => 3001
     * //     [1] => 3002
     * // )
     * ```
     *
     * @example (Caso sin `$_GET['contrato_id']`, no se modifica el array)
     * ```php
     * $_GET = []; // No se define 'contrato_id'
     * $contratos_timbra = [3001, 3002];
     *
     * $resultado = (new costo_estimado_det())->contratos_timbra($contratos_timbra, 'timbra');
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [0] => 3001
     * //     [1] => 3002
     * // )
     * ```
     */
    private function contratos_timbra(array $contratos_timbra, string $evento): array
    {
        if (isset($_GET['contrato_id']) && $evento === 'timbra') {
            $contratos_timbra = [];
            $contratos_timbra[] = $_GET['contrato_id'];
        }

        return $contratos_timbra;
    }


    /**
     * REG
     * Inicializa el array de contratos para timbrado de documentos.
     *
     * Esta función verifica si existe el parámetro `contratos_timbra` en `$_POST`. Si existe,
     * asigna su valor al array `$contratos_timbra`. En caso contrario, retorna un array vacío.
     *
     * @return array Retorna un array con los contratos a timbrar si el parámetro está presente en `$_POST`,
     *               de lo contrario, retorna un array vacío.
     *
     * @example (Caso con `$_POST['contratos_timbra']` definido)
     * ```php
     * $_POST['contratos_timbra'] = [2001, 2002, 2003];
     *
     * $resultado = (new costo_estimado_det())->contratos_timbra_init();
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [0] => 2001
     * //     [1] => 2002
     * //     [2] => 2003
     * // )
     * ```
     *
     * @example (Caso sin `$_POST['contratos_timbra']`)
     * ```php
     * $_POST = []; // No se define 'contratos_timbra'
     *
     * $resultado = (new costo_estimado_det())->contratos_timbra_init();
     * print_r($resultado);
     * // Output:
     * // Array ()
     * ```
     */
    private function contratos_timbra_init(): array
    {
        $contratos_timbra = [];

        if (isset($_POST['contratos_timbra'])) {
            $contratos_timbra = $_POST['contratos_timbra'];
        }

        return $contratos_timbra;
    }



    /**
     * REG
     * Procesa los contratos a generar y timbrar basándose en el evento actual.
     *
     * Esta función obtiene el evento actual (`genera` o `timbra`), filtra los contratos a procesar
     * y devuelve un objeto con los resultados organizados.
     *
     * La función valida la existencia del evento y filtra los contratos dependiendo de su tipo (`genera` o `timbra`).
     * En caso de error en cualquier paso del proceso, se devuelve un mensaje de error estructurado.
     *
     * @param array $contratos_genera Lista inicial de contratos a generar.
     *                                Ejemplo de entrada:
     *                                ```php
     *                                $contratos_genera = [1001, 1002, 1003];
     *                                ```
     * @param array $contratos_timbra Lista inicial de contratos a timbrar.
     *                                Ejemplo de entrada:
     *                                ```php
     *                                $contratos_timbra = [2001, 2002];
     *                                ```
     *
     * @return stdClass|array Devuelve un objeto con los contratos procesados y el evento si todo es correcto.
     *                        En caso de error, devuelve un array con un mensaje detallado.
     *
     * @example Uso correcto (Evento "genera")
     * ```php
     * $_GET['evento'] = 'genera';
     * $_GET['contrato_id'] = 3001;
     *
     * $contratos_genera = [1001, 1002, 1003];
     * $contratos_timbra = [2001, 2002];
     *
     * $resultado = (new costo_estimado_det())->contratos_transaccion($contratos_genera, $contratos_timbra);
     * print_r($resultado);
     * // Output esperado:
     * // stdClass Object (
     * //     [contratos_genera] => Array ( [0] => 3001 )
     * //     [contratos_timbra] => Array ( [0] => 2001, [1] => 2002 )
     * //     [evento] => genera
     * // )
     * ```
     *
     * @example Uso correcto (Evento "timbra")
     * ```php
     * $_GET['evento'] = 'timbra';
     * $_GET['contrato_id'] = 4001;
     *
     * $contratos_genera = [1001, 1002, 1003];
     * $contratos_timbra = [2001, 2002];
     *
     * $resultado = (new costo_estimado_det())->contratos_transaccion($contratos_genera, $contratos_timbra);
     * print_r($resultado);
     * // Output esperado:
     * // stdClass Object (
     * //     [contratos_genera] => Array ( [0] => 1001, [1] => 1002, [2] => 1003 )
     * //     [contratos_timbra] => Array ( [0] => 4001 )
     * //     [evento] => timbra
     * // )
     * ```
     *
     * @example Caso de error en la obtención del evento
     * ```php
     * $_GET = []; // No se define 'evento'
     *
     * $contratos_genera = [1001, 1002, 1003];
     * $contratos_timbra = [2001, 2002];
     *
     * $resultado = (new costo_estimado_det())->contratos_transaccion($contratos_genera, $contratos_timbra);
     * print_r($resultado);
     * // Output esperado:
     * // Array (
     * //     [error] => 'Error al obtener evento'
     * //     [data] => ''
     * // )
     * ```
     *
     * @example Caso de error en la obtención de contratos
     * ```php
     * $_GET['evento'] = 'genera';
     *
     * $contratos_genera = 'texto_invalido'; // Esto generará un error
     * $contratos_timbra = [2001, 2002];
     *
     * $resultado = (new costo_estimado_det())->contratos_transaccion($contratos_genera, $contratos_timbra);
     * print_r($resultado);
     * // Output esperado:
     * // Array (
     * //     [error] => 'Error en la transacción de contratos'
     * //     [data] => [
     * //         'evento' => 'genera',
     * //         'contratos_genera' => 'texto_invalido',
     * //         'contratos_timbra' => [2001, 2002]
     * //     ]
     * // )
     * ```
     */
    private function contratos_transaccion(array $contratos_genera, array $contratos_timbra)
    {
        // Obtener el evento actual desde la variable global $_GET
        $evento = $this->evento();
        if (error::$en_error) {
            return (new error())->error('Error al obtener evento', $evento);
        }

        // Filtrar contratos según el evento
        $contratos_genera = $this->contratos_genera($contratos_genera, $evento);
        $contratos_timbra = $this->contratos_timbra($contratos_timbra, $evento);

        // Verificar si ocurrió un error en el proceso
        if (error::$en_error) {
            return (new error())->error('Error en la transacción de contratos', [
                'evento' => $evento,
                'contratos_genera' => $contratos_genera,
                'contratos_timbra' => $contratos_timbra
            ]);
        }

        // Retornar los datos estructurados
        return (object)[
            'contratos_genera' => $contratos_genera,
            'contratos_timbra' => $contratos_timbra,
            'evento' => $evento
        ];
    }


    /**
     * REG
     * Procesa los contratos únicos, unificando la información de generación y timbrado.
     *
     * Esta función recibe dos arrays de contratos (`contratos_genera` y `contratos_timbra`) y
     * devuelve un objeto con los contratos organizados. Si no hay datos en `$_POST`,
     * ejecuta `contratos_transaccion` para determinar los contratos actualizados según el evento.
     *
     * @param array $contratos_genera Lista inicial de contratos a generar.
     *                                Ejemplo de entrada:
     *                                ```php
     *                                $contratos_genera = [101, 102, 103];
     *                                ```
     * @param array $contratos_timbra Lista inicial de contratos a timbrar.
     *                                Ejemplo de entrada:
     *                                ```php
     *                                $contratos_timbra = [201, 202];
     *                                ```
     *
     * @return stdClass|array Devuelve un objeto con los contratos procesados.
     *                        En caso de error, devuelve un array con un mensaje detallado.
     *
     * @example Uso correcto sin datos en $_POST (se ejecuta `contratos_transaccion`)
     * ```php
     * $_POST = []; // No hay datos en POST
     * $_GET['evento'] = 'genera';
     * $_GET['contrato_id'] = 301;
     *
     * $contratos_genera = [101, 102, 103];
     * $contratos_timbra = [201, 202];
     *
     * $resultado = (new costo_estimado_det())->contratos_unicos($contratos_genera, $contratos_timbra);
     * print_r($resultado);
     * // Output esperado:
     * // stdClass Object (
     * //     [contratos_genera] => Array ( [0] => 301 )
     * //     [contratos_timbra] => Array ( [0] => 201, [1] => 202 )
     * //     [evento] => genera
     * // )
     * ```
     *
     * @example Uso correcto con datos en $_POST (se conserva la entrada original)
     * ```php
     * $_POST['some_key'] = 'valor'; // Hay datos en POST
     * $_GET['evento'] = 'genera';
     *
     * $contratos_genera = [101, 102, 103];
     * $contratos_timbra = [201, 202];
     *
     * $resultado = (new costo_estimado_det())->contratos_unicos($contratos_genera, $contratos_timbra);
     * print_r($resultado);
     * // Output esperado:
     * // stdClass Object (
     * //     [contratos_genera] => Array ( [0] => 101, [1] => 102, [2] => 103 )
     * //     [contratos_timbra] => Array ( [0] => 201, [1] => 202 )
     * // )
     * ```
     *
     * @example Caso de error en la ejecución de `contratos_transaccion`
     * ```php
     * $_POST = []; // No hay datos en POST
     * $_GET = []; // No se define 'evento' en GET (esto generará un error en `contratos_transaccion`)
     *
     * $contratos_genera = [101, 102, 103];
     * $contratos_timbra = [201, 202];
     *
     * $resultado = (new costo_estimado_det())->contratos_unicos($contratos_genera, $contratos_timbra);
     * print_r($resultado);
     * // Output esperado:
     * // Array (
     * //     [error] => 'Error al obtener contratos'
     * //     [data] => ''
     * // )
     * ```
     */
    private function contratos_unicos(array $contratos_genera, array $contratos_timbra)
    {
        // Crear un objeto para almacenar los contratos
        $contratos = new stdClass();
        $contratos->contratos_genera = $contratos_genera;
        $contratos->contratos_timbra = $contratos_timbra;

        // Si $_POST está vacío, procesar los contratos con contratos_transaccion
        if (count($_POST) === 0) {
            $contratos = $this->contratos_transaccion($contratos_genera, $contratos_timbra);

            // Verificar si ocurrió un error durante la transacción de contratos
            if (error::$en_error) {
                return (new error())->error('Error al obtener contratos', $contratos);
            }
        }

        // Devolver los contratos procesados
        return $contratos;
    }


    /**
     * REG
     * Obtiene el código postal de una empresa asociada a una plaza específica en la base de datos.
     *
     * Esta función valida los datos de entrada, genera una consulta SQL para obtener el código postal (`cp.codigo_postal`)
     * desde la tabla `empresa_plaza` y la ejecuta utilizando la conexión PDO.
     *
     * @param stdClass $contrato Objeto con información del contrato. Debe contener la propiedad `plaza_id` con un ID mayor a 0.
     * @param stdClass $empresa  Objeto con información de la empresa. Puede contener `id` o `empresa_id` como identificador válido.
     * @param PDO $link Conexión activa a la base de datos.
     *
     * @return stdClass|array Retorna un objeto con la información del código postal si se encuentra una única coincidencia.
     *                        En caso de error, devuelve un array con el mensaje de error y los datos asociados.
     *
     * @example
     * ```php
     * $contrato = new stdClass();
     * $contrato->plaza_id = 5;
     *
     * $empresa = new stdClass();
     * $empresa->id = 10;
     *
     * $pdo = new PDO("mysql:host=localhost;dbname=testdb", "user", "password");
     *
     * $resultado = (new costo_estimado_det())->cp_empresa($contrato, $empresa, $pdo);
     * print_r($resultado);
     * // Output: stdClass Object ( [codigo_postal] => "12345" )
     * ```
     *
     * @example (Caso de error: Empresa sin ID válido)
     * ```php
     * $contrato = new stdClass();
     * $contrato->plaza_id = 5;
     *
     * $empresa = new stdClass();
     *
     * $pdo = new PDO("mysql:host=localhost;dbname=testdb", "user", "password");
     *
     * $resultado = (new costo_estimado_det())->cp_empresa($contrato, $empresa, $pdo);
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [error] => Error is no existe $empresa->id debe existir $empresa->empresa_id
     * //     [data] => stdClass Object ()
     * // )
     * ```
     *
     * @example (Caso de error: Más de una coincidencia encontrada)
     * ```php
     * $contrato = new stdClass();
     * $contrato->plaza_id = 5;
     *
     * $empresa = new stdClass();
     * $empresa->id = 10;
     *
     * $pdo = new PDO("mysql:host=localhost;dbname=testdb", "user", "password");
     *
     * $resultado = (new costo_estimado_det())->cp_empresa($contrato, $empresa, $pdo);
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [error] => Error hay más de una conf: 5 empresa: 10
     * //     [data] => Array ( [0] => stdClass Object ( [codigo_postal] => "12345" ), [1] => stdClass Object ( [codigo_postal] => "67890" ) )
     * // )
     * ```
     */
    private function cp_empresa(stdClass $contrato, stdClass $empresa, PDO $link)
    {
        // Validación de la existencia de plaza_id en el contrato
        if (!isset($contrato->plaza_id)) {
            return (new error())->error('Error $contrato->plaza_id debe existir', $contrato);
        }

        // Validación de que plaza_id sea mayor a 0
        if ((int)$contrato->plaza_id <= 0) {
            return (new error())->error('Error is no existe $contrato->plaza_id debe ser mayor a 0', $contrato);
        }

        // Generar la consulta SQL para obtener el código postal
        $sql = (new \desarrollo_em3\manejo_datos\sql\costo_estimado_det())->cp_empresa($contrato, $empresa);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $sql', $sql);
        }

        // Ejecutar la consulta y obtener los resultados
        $empresas_plaza_rs = (new consultas())->exe_objs($link, $sql);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $empresas_plaza_rs', $empresas_plaza_rs);
        }

        // Validar si no se encontró ninguna coincidencia
        if (count($empresas_plaza_rs) === 0) {
            return (new error())->error(
                "Error no hay empresa plaza conf plaza: $contrato->plaza_id empresa: $empresa->empresa_id",
                $empresas_plaza_rs
            );
        }

        // Validar si se encontraron múltiples coincidencias
        if (count($empresas_plaza_rs) > 1) {
            return (new error())->error(
                "Error hay mas de una conf: $contrato->plaza_id empresa: $empresa->empresa_id",
                $empresas_plaza_rs
            );
        }

        // Retornar el único resultado encontrado
        return $empresas_plaza_rs[0];
    }



    /**
     * REG
     * Obtiene los datos de un contrato y la empresa asociada desde la base de datos.
     *
     * Esta función valida el `contrato_id`, consulta la base de datos para obtener la información del contrato
     * y, posteriormente, recupera los datos de la empresa asociada al contrato.
     *
     * @param int $contrato_id ID del contrato a consultar. Debe ser un valor mayor a 0.
     * @param PDO $link Conexión activa a la base de datos.
     *
     * @return stdClass|array Retorna un objeto con los datos del contrato y la empresa asociada.
     *                        En caso de error, devuelve un array con el mensaje de error y los datos asociados.
     *
     * @example
     * ```php
     * $pdo = new PDO("mysql:host=localhost;dbname=testdb", "user", "password");
     *
     * $contrato_id = 123;
     * $resultado = (new costo_estimado_det())->datos_contrato($contrato_id, $pdo);
     * print_r($resultado);
     * // Output: stdClass Object (
     * //     [contrato] => stdClass Object (
     * //         [id] => 123
     * //         [empresa_id] => 10
     * //         [fecha] => "2024-02-25"
     * //     )
     * //     [empresa] => stdClass Object (
     * //         [id] => 10
     * //         [nombre] => "Empresa XYZ"
     * //     )
     * // )
     * ```
     *
     * @example (Caso de error: `contrato_id` inválido)
     * ```php
     * $pdo = new PDO("mysql:host=localhost;dbname=testdb", "user", "password");
     *
     * $contrato_id = 0;
     * $resultado = (new costo_estimado_det())->datos_contrato($contrato_id, $pdo);
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [error] => Error $contrato_id debe ser mayor a 0
     * //     [data] => 0
     * // )
     * ```
     *
     * @example (Caso de error: No se encuentra el contrato en la base de datos)
     * ```php
     * $pdo = new PDO("mysql:host=localhost;dbname=testdb", "user", "password");
     *
     * $contrato_id = 9999;
     * $resultado = (new costo_estimado_det())->datos_contrato($contrato_id, $pdo);
     * print_r($resultado);
     * // Output:
     * // Array (
     * //     [error] => Error al obtener $contrato
     * //     [data] => null
     * // )
     * ```
     */
    private function datos_contrato(int $contrato_id, PDO $link)
    {
        // Validar que el contrato_id sea mayor a 0
        if ($contrato_id <= 0) {
            return (new error())->error('Error $contrato_id debe ser mayor a 0', $contrato_id);
        }

        // Obtener los datos del contrato desde la base de datos
        $contrato = (new consultas())->registro_bruto(new stdClass(), 'contrato', $contrato_id, $link,
            false);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $contrato', $contrato);
        }

        // Obtener los datos de la empresa asociada al contrato
        $empresa = (new consultas())->registro_bruto(new stdClass(), 'empresa', $contrato->empresa_id, $link,
            false);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $empresa', $empresa);
        }

        // Estructurar los datos en un objeto de salida
        $data = new stdClass();
        $data->contrato = $contrato;
        $data->empresa = $empresa;

        return $data;
    }

    private function datos_factura(int $contrato_id, PDO $link, int $tipo_operacion_factura_id)
    {
        $datos = $this->datos_contrato($contrato_id,$link);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $datos', $datos);
        }

        $empresa_324 = $datos->empresa;
        $contrato = $datos->contrato;

        $datos_fc = $this->datos_fc($contrato, $contrato_id,$datos,$empresa_324,$link,$tipo_operacion_factura_id);

        if (error::$en_error) {
            return (new error())->error('Error al obtener $datos_fc', $datos_fc);
        }

        $datos_fc->empresa_324 = $empresa_324;
        $datos_fc->contrato = $contrato;

        return $datos_fc;

    }

    final public function datos_factura_full(int $contrato_id, PDO $link, int $tipo_operacion_factura_id)
    {
        $datos_fc = $this->datos_factura($contrato_id,$link,$tipo_operacion_factura_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $datos_fc', $datos_fc);
        }

        $montos = $this->montos($datos_fc);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $montos', $montos);
        }

        $datos_fc->montos = $montos;

        return $datos_fc;

    }

    private function datos_fc(stdClass $contrato, int $contrato_id, stdClass $datos, stdClass $empresa_324,
                                   PDO $link, int $tipo_operacion_factura_id)
    {

        $rs = new stdClass();

        $ssa = $this->solicitud_servicio($contrato_id,$link);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $ssa', $ssa);
        }
        $rs->ssa = $ssa;

        $descripcion = $this->descripcion_partida($contrato,$ssa);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $descripcion', $descripcion);
        }
        $rs->descripcion = $descripcion;

        $empresa_plaza_324 = $this->cp_empresa($datos->contrato, $datos->empresa,$link);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $empresa_plaza_324', $empresa_plaza_324);
        }
        $rs->empresa_plaza_324 = $empresa_plaza_324;

        $regimen_fiscal_324  = (new consultas())->registro_bruto(new stdClass(), 'regimen_fiscal',
            $empresa_324->regimen_fiscal_id, $link, false);

        if (error::$en_error) {
            return (new error())->error('Error al obtener $regimen_fiscal_324', $regimen_fiscal_324);
        }
        $rs->regimen_fiscal_324 = $regimen_fiscal_324;

        $empresa_funeraria_actual = $this->empresa_funeraria_actual($contrato, $empresa_324,$link);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $empresa_funeraria_actual', $empresa_funeraria_actual);
        }
        $rs->empresa_funeraria_actual = $empresa_funeraria_actual;


        $serie_act_row = $this->get_serie($contrato,$empresa_funeraria_actual,$link,$tipo_operacion_factura_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $serie_act_row', $serie_act_row);
        }
        $rs->serie_act_row = $serie_act_row;

        $year = date('Y');
        $ejercicio = (new ejercicio())->obten_ejercicio_anio(false, $link,$year);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $ejercicio', $ejercicio);
        }
        $rs->ejercicio = $ejercicio;

        return $rs;


    }

    private function descripcion_partida(stdClass $contrato, stdClass $ssa): string
    {
        date_default_timezone_set("America/Mexico_City");
        setlocale(LC_TIME, 'es_MX.UTF-8');
        $fecha_expedicion = $ssa->fecha_expedicion;
        $data_exp = strtotime($fecha_expedicion);
        $mes_expedicion = strftime('%B', $data_exp);
        $year = strftime('%Y', $data_exp);
        $fol = $contrato->U_SeCont.$contrato->U_FolioCont;
        return "SERVICIOS PRESTADOS EL MES DE $mes_expedicion $year $fol";

    }


    private function empresa_funeraria_actual(stdClass $contrato, stdClass $empresa_324, PDO $link)
    {

        $sql = (new \desarrollo_em3\manejo_datos\sql\conf_costo_estimado())
            ->costo_estimado_actual($contrato,$empresa_324);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $sql', $sql);
        }


        $empresa_funeraria_actual_rs = (new consultas())->exe_objs($link, $sql);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $empresa_funeraria_actual_rs',
                $empresa_funeraria_actual_rs);
        }

        if(count($empresa_funeraria_actual_rs) === 0){
            return (new error())->error('Error al obtener $empresa_funeraria_actual_rs no hay registros',
                $empresa_funeraria_actual_rs);
        }

        if(count($empresa_funeraria_actual_rs) > 1){
            return (new error())->error(
                'Error al obtener $empresa_funeraria_actual_rs existe mas de un registro',
                $empresa_funeraria_actual_rs);
        }

        return $empresa_funeraria_actual_rs[0];

    }

    /**
     * REG
     * Obtiene el valor del evento desde la variable `$_GET`.
     *
     * Esta función revisa si existe el parámetro `evento` en `$_GET`. Si está definido,
     * devuelve su valor limpio de espacios en blanco al inicio y al final.
     * Si no está presente, retorna una cadena vacía (`''`).
     *
     * @return string Retorna el valor del evento si está definido en `$_GET`, de lo contrario, retorna una cadena vacía.
     *
     * @example (Caso con `$_GET['evento']` definido)
     * ```php
     * $_GET['evento'] = 'genera';
     *
     * $resultado = (new costo_estimado_det())->evento();
     * echo $resultado;
     * // Output: "genera"
     * ```
     *
     * @example (Caso sin `$_GET['evento']`)
     * ```php
     * $_GET = []; // No se define 'evento'
     *
     * $resultado = (new costo_estimado_det())->evento();
     * echo $resultado;
     * // Output: ""
     * ```
     *
     * @example (Caso con espacios en el valor de `$_GET['evento']`)
     * ```php
     * $_GET['evento'] = '  timbra  ';
     *
     * $resultado = (new costo_estimado_det())->evento();
     * echo $resultado;
     * // Output: "timbra"
     * ```
     */
    private function evento(): string
    {
        $evento = '';

        if (isset($_GET['evento'])) {
            $evento = trim($_GET['evento']);
        }

        return $evento;
    }

    private function factura_fune_get(int $contrato_id, PDO $link)
    {
        $sql = (new \desarrollo_em3\manejo_datos\sql\costo_estimado_det())->factura_fune($contrato_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $sql', $sql);
        }

        $exe = (new consultas())->exe_objs($link,$sql);
        if (error::$en_error) {
            return (new error())->error('Error al obtener facturas', $exe);
        }

        if(count($exe) === 0){
            return (new error())->error('Error no hay factura generada', $exe);
        }

        if(count($exe) > 1){
            return (new error())->error('Error de integridad hay mas de una factura', $exe);
        }

        return $exe[0];

    }

    final public function factura_fune_ins(int $contrato_id, stdClass $datos_fc, int $factura_id): array
    {
        $fc_fun_ins['factura_id'] = $factura_id;
        $fc_fun_ins['status'] = 'activo';
        $fc_fun_ins['contrato_id'] = $contrato_id;
        $fc_fun_ins['empresa_funeraria_id'] = $datos_fc->serie_act_row->empresa_id;
        $fc_fun_ins['empresa_324_id'] = $datos_fc->empresa_324->id;

        return $fc_fun_ins;

    }


    final public function factura_ins(stdClass $datos_fc, stdClass $montos): array
    {

        if(!isset($datos_fc->serie_act_row)){
            return (new error())->error('Error $datos_fc->serie_act_row no existe', $datos_fc);
        }
        if(!isset($datos_fc->regimen_fiscal_324)){
            return (new error())->error('Error $datos_fc->regimen_fiscal_324 no existe', $datos_fc);
        }
        if(!isset($datos_fc->empresa_324)){
            return (new error())->error('Error $datos_fc->empresa_324 no existe', $datos_fc);
        }
        if(!isset($datos_fc->empresa_plaza_324)){
            return (new error())->error('Error $datos_fc->empresa_plaza_324 no existe', $datos_fc);
        }
        if(!isset($datos_fc->empresa_funeraria_actual)){
            return (new error())->error('Error $datos_fc->empresa_funeraria_actual no existe', $datos_fc);
        }
        if(!isset($datos_fc->ejercicio)){
            return (new error())->error('Error $datos_fc->ejercicio no existe', $datos_fc);
        }


        if(!isset($datos_fc->serie_act_row->empresa_id)){
            return (new error())->error('Error $datos_fc->serie_act_row->empresa_id no existe', $datos_fc);
        }
        if(!isset($datos_fc->serie_act_row->plaza_id)){
            return (new error())->error('Error $datos_fc->serie_act_row->plaza_id no existe', $datos_fc);
        }
        if(!isset($datos_fc->serie_act_row->serie)){
            return (new error())->error('Error $datos_fc->serie_act_row->serie no existe', $datos_fc);
        }


        if(!isset($datos_fc->regimen_fiscal_324->id)){
            return (new error())->error('Error $datos_fc->regimen_fiscal_324->id no existe', $datos_fc);
        }
        if(!isset($datos_fc->empresa_324->razon_social)){
            return (new error())->error('Error $datos_fc->empresa_324->razon_social no existe', $datos_fc);
        }
        if(!isset($datos_fc->empresa_324->rfc)){
            return (new error())->error('Error $datos_fc->empresa_324->rfc no existe', $datos_fc);
        }


        if(!isset($datos_fc->empresa_plaza_324->codigo_postal)){
            return (new error())->error('Error $datos_fc->empresa_plaza_324->codigo_postal no existe', $datos_fc);
        }


        if(!isset($datos_fc->empresa_funeraria_actual->costo)){
            return (new error())->error('Error $datos_fc->empresa_funeraria_actual->costo no existe', $datos_fc);
        }


        if(!isset($datos_fc->ejercicio->id)){
            return (new error())->error('Error $datos_fc->ejercicio->id no existe', $datos_fc);
        }


        $factura_ins['empresa_id'] = $datos_fc->serie_act_row->empresa_id;
        $factura_ins['plaza_id'] = $datos_fc->serie_act_row->plaza_id;
        $factura_ins['metodo_pago_id'] = 2;
        $factura_ins['forma_pago_id'] = 22;
        $factura_ins['moneda_id'] = 99;
        $factura_ins['tipo_cambio_id'] = 396;
        $factura_ins['tipo_comprobante_id'] = 1;
        $factura_ins['uso_cfdi_id'] = 3;
        $factura_ins['tipo_operacion_factura_id'] = 8;
        $factura_ins['regimen_fiscal_receptor_id'] = $datos_fc->regimen_fiscal_324->id;
        $factura_ins['receptor_razon_social'] = $datos_fc->empresa_324->razon_social;
        $factura_ins['receptor_rfc'] = $datos_fc->empresa_324->rfc;
        $factura_ins['receptor_codigo_postal'] = $datos_fc->empresa_plaza_324->codigo_postal;
        $factura_ins['serie'] = $datos_fc->serie_act_row->serie;
        $factura_ins['fecha'] = date('Y-m-d');
        $factura_ins['hora'] = date('H:i:s');
        $factura_ins['saldo'] = $datos_fc->empresa_funeraria_actual->costo;
        $factura_ins['subtotal'] = $datos_fc->empresa_funeraria_actual->costo;
        $factura_ins['status'] = 'activo';
        $factura_ins['observaciones'] = '';
        $factura_ins['ejercicio_id'] = $datos_fc->ejercicio->id;
        $factura_ins['meses'] = date('m');
        $factura_ins['total'] = $montos->total;

        return $factura_ins;

    }

    final public function factura_fune_por_timbrar(int $contrato_id, PDO $link)
    {
        $factura_fune = $this->factura_fune_get($contrato_id,$link);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $factura_fune', $factura_fune);
        }

        $factura_id = $factura_fune->factura_id;

        $factura = (new consultas())->registro_bruto(new stdClass(), 'factura',$factura_id,$link,false);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $factura', $factura);
        }

        if(!isset($factura->uuid)){
            $factura->uuid = '';
        }

        if($factura->uuid !== ''){
            return (new error())->error('Error la factura esta timbrada', $factura);
        }

        return $factura_id;


    }

    private function get_serie(stdClass $contrato,stdClass $empresa, PDO $link, int $tipo_operacion_factura_id)
    {
        $sql_serie = (new \desarrollo_em3\manejo_datos\sql\costo_estimado_det())->serie($contrato,$empresa,
            $tipo_operacion_factura_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $sql_serie', $sql_serie);
        }

        $series_act = (new consultas())->exe_objs($link, $sql_serie);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $series_act', $series_act);
        }
        if (count($series_act) === 0) {
            return (new error())->error(
                "Error no hay serie conf plaza: $contrato->plaza_id empresa: $empresa->empresa_id",
                $series_act);
        }

        return $series_act[0];

    }

    private function montos(stdClass $datos_fc): stdClass
    {
        $iva = round($datos_fc->empresa_funeraria_actual->costo * .16,2);
        $total = round($datos_fc->empresa_funeraria_actual->costo + $iva,2);
        $precio_unitario = round($total - $iva,2);

        $montos = new stdClass();
        $montos->iva = $iva;
        $montos->total = $total;
        $montos->precio_unitario = $precio_unitario;

        return $montos;

    }

    final public function registro_new_genera(
        int $conf_costo_estimado_id,
        PDO $link,
        float $porcentaje_iva,
        array $row_en_proceso
    ) {
        // Valida que el porcentaje de IVA sea mayor o igual a 0
        if ($porcentaje_iva < 0.0) {
            return (new error())->error('Error $porcentaje_iva es menor a 0', $porcentaje_iva);
        }

        // Valida la existencia y validez del pago_id
        if (!isset($row_en_proceso['pago_id'])) {
            return (new error())->error('Error al $row_en_proceso[pago_id] no existe', $row_en_proceso);
        }
        if ((int)$row_en_proceso['pago_id'] <= 0) {
            return (new error())->error('Error $row_en_proceso[pago_id] es menor a 0', $row_en_proceso);
        }

        // Genera un nuevo registro con los datos integrados
        $registro_new = (new conf_costo_estimado())->row($conf_costo_estimado_id, $link, $porcentaje_iva, $row_en_proceso);
        if (error::$en_error) {
            return (new error())->error('Error al obtener registro en proceso', $registro_new);
        }

        // Verifica si existe un registro en la tabla 'costo_estimado_det' con el mismo pago_id
        $existe = (new consultas())->existe('pago_id', 'costo_estimado_det', $link, $row_en_proceso['pago_id']);
        if (error::$en_error) {
            return (new error())->error('Error al obtener si existe row', $existe);
        }

        // Devuelve el registro generado y el estado de existencia
        $data = new stdClass();
        $data->registro_new = $registro_new;
        $data->existe = $existe;

        return $data;
    }

    private function solicitud_servicio(int $contrato_id, PDO $link)
    {


        $sql = (new \desarrollo_em3\manejo_datos\sql\costo_estimado_det())->solicitud_servicio($contrato_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $sql', $sql);
        }

        $r_ssa = (new consultas())->exe_objs($link,$sql);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $r_ssa', $r_ssa);
        }

        if(count($r_ssa) === 0){
            return (new error())->error('Error no existe solicitud', $r_ssa);
        }

        if(count($r_ssa) > 1){
            return (new error())->error('Error existe mas de una solicitud', $r_ssa);
        }
        return $r_ssa[0];

    }



}
