<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\data\_datos;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;

class esquema_guardadito
{
    private string $name_entidad = 'esquema_guardadito';

    final public function campos(PDO $link)
    {
        $campos = (new _datos())->campos_entidad($this->name_entidad, $link);
        if(error::$en_error){
            return (new error())->error('Error al obtener campos',$campos);
        }
        return $campos;
    }

    final public function esquemas_guardadito(int $esquema_id, string $fecha, PDO $link)
    {
        $sql = (new \desarrollo_em3\manejo_datos\sql\esquema_guardadito())->sql_por_fecha_id($esquema_id,$fecha,$link);
        if(error::$en_error) {
            return (new error())->error('Error al obtener $sql', $sql);
        }

        $r_esquema_guardadito = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error) {
            return (new error())->error('Error al obtener $r_esquema_guardadito', $r_esquema_guardadito);
        }

        if($r_esquema_guardadito['n_registros']>1){
            return (new error())->error('Error solo puede haber un esquema para este tipo',
                $r_esquema_guardadito);
        }

        return $r_esquema_guardadito['registros'];

    }


}
