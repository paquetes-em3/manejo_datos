<?php
namespace desarrollo_em3\manejo_datos\data\entidades;


use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\data\entidades\contrato_comision\_data_comi_alterna;
use stdClass;

class contrato_comision
{

    /**
     * TRASLADADO
     * Determina si se aplica una comisión alterna para un empleado específico basado en los datos proporcionados.
     *
     * Esta función verifica si se debe aplicar una comisión alterna para un empleado, basándose en la existencia de un jefe inmediato
     * y en las condiciones de la comisión alterna definidas en los datos de comisión alterna.
     *
     * @param array $cadena Arreglo que contiene los datos de empleados, donde la clave es el ID del empleado.
     * @param stdClass $data_comi_alt Objeto que contiene los datos alternos de comisión, incluyendo las propiedades relacionadas con la comisión y coordinación.
     * @param string $key_empleado_id Clave que se usa para obtener el ID del empleado desde el arreglo `$cadena`.
     * @return bool|array Retorna `true` si se aplica la comisión alterna, de lo contrario, retorna `false`. En caso de error, se retorna un arreglo con información del error.
     */
    final public function aplica_comision_alterna(array $cadena, stdClass $data_comi_alt, string $key_empleado_id)
    {
        $key_empleado_id = trim($key_empleado_id);
        if($key_empleado_id === ''){
            return (new error())->error('Error $key_empleado_id esta vacia',$key_empleado_id);
        }
        if(!isset($cadena[$key_empleado_id])){
            return (new error())->error('Error $cadena[$key_empleado_id] no existe',$cadena);
        }

        $tiene_jefe_inmediato = $this->tiene_jefe_inmediato($data_comi_alt);
        if(error::$en_error){
            return (new error())->error('Error al validar jefe',$tiene_jefe_inmediato);
        }


        $aplica_comision_alterna = $this->es_comision_alterna($cadena,$data_comi_alt,$key_empleado_id,
            $tiene_jefe_inmediato);
        if(error::$en_error){
            return (new error())->error('Error al validar comision alterna',$aplica_comision_alterna);
        }

        return $aplica_comision_alterna;

    }

    /**
     * EM3
     * Procesa y determina la configuración de la comisión alternativa basada en un empleado y una cadena de registros.
     *
     * Este método evalúa si un empleado aplica para una comisión alternativa, revisando una cadena de registros
     * de comisiones. Si el esquema indica que el empleado es adjunto, se inicializan y configuran las propiedades
     * correspondientes en el objeto de datos de comisión alternativa.
     *
     * @param string $key_empleado_id Clave del empleado que se está evaluando.
     * @param array $registros_cadena_comision Arreglo que contiene la cadena de registros de comisiones,
     *                                         cada registro debe incluir:
     *                                         - `'esquema_es_adjunto'`: Indica si el esquema aplica para adjuntos.
     *
     * @return stdClass|array Devuelve un objeto `stdClass` con las siguientes propiedades:
     *                        - `aplica_comision_adjunto` (bool): Indica si aplica la comisión como adjunto.
     *                        - `empleado_jefe_inmediato_id` (int): ID del jefe inmediato asociado.
     *                        - `aplica_coordinador` (bool): Indica si aplica como coordinador.
     *                        - `encontrado` (bool): Indica si se encontró el jefe en los registros.
     *
     *                        Si ocurre un error, devuelve un array con los detalles del error.
     *
     * @example Caso exitoso: Comisión alternativa para un adjunto
     * ```php
     * $key_empleado_id = 'empleado_123';
     * $registros_cadena_comision = [
     *     [
     *         'esquema_es_adjunto' => 'activo',
     *         'empleado_jefe_inmediato_id' => 456,
     *         'esquema_aplica_coordinador' => 'activo'
     *     ]
     * ];
     *
     * $resultado = $this->data_comi_alt($key_empleado_id, $registros_cadena_comision);
     * // Resultado:
     * // (object) [
     * //     'aplica_comision_adjunto' => true,
     * //     'empleado_jefe_inmediato_id' => 456,
     * //     'aplica_coordinador' => true,
     * //     'encontrado' => true
     * // ]
     * ```
     *
     * @example Error: Clave del empleado vacía
     * ```php
     * $key_empleado_id = '';
     * $registros_cadena_comision = [
     *     ['esquema_es_adjunto' => 'activo']
     * ];
     *
     * $resultado = $this->data_comi_alt($key_empleado_id, $registros_cadena_comision);
     * // Resultado:
     * // [
     * //     'error' => 'Error $key_empleado_id esta vacia',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Esquema no contiene `'esquema_es_adjunto'`
     * ```php
     * $key_empleado_id = 'empleado_123';
     * $registros_cadena_comision = [
     *     ['empleado_jefe_inmediato_id' => 456]
     * ];
     *
     * $resultado = $this->data_comi_alt($key_empleado_id, $registros_cadena_comision);
     * // Resultado:
     * // [
     * //     'error' => 'Error $cadena[esquema_es_adjunto] no existe',
     * //     'data' => [...]
     * // ]
     * ```
     *
     * @note Este método depende de la clase `_data_comi_alterna` para inicializar y procesar la lógica de comisiones alternativas.
     *       Se asegura de manejar todos los escenarios posibles de datos ausentes o valores predeterminados.
     */
    final public function data_comi_alt(string $key_empleado_id, array $registros_cadena_comision)
    {
        $key_empleado_id = trim($key_empleado_id);
        if($key_empleado_id === ''){
            return (new error())->error('Error $key_empleado_id esta vacia',$key_empleado_id);
        }

        $data_comi_alt = (new _data_comi_alterna())->data_comi_alterna_init();
        if(error::$en_error){
            return (new error())->error('Error al inicializar datos de comsion',$data_comi_alt);
        }

        foreach ($registros_cadena_comision as  $cadena){
            if(!isset($cadena['esquema_es_adjunto'])){
                return (new error())->error('Error $cadena[esquema_es_adjunto] no existe',$cadena);
            }
            $esquema_es_adjunto = $cadena['esquema_es_adjunto'];
            if($esquema_es_adjunto === 'activo'){
                $data_comi_alt = (new _data_comi_alterna())->data_comi_alt_es_adjunto($cadena,$key_empleado_id,
                    $registros_cadena_comision);
                if(error::$en_error){
                    return (new error())->error('Error al inicializar datos de comsion',$data_comi_alt);
                }
                break;
            }
        }
        return $data_comi_alt;
    }

    /**
     * TRASLADADO
     * Determina si se aplica una comisión alterna basada en los datos del empleado y las condiciones de comisión.
     *
     * Esta función verifica si la comisión alterna aplica para un empleado específico en base a si tiene un jefe inmediato,
     * si el empleado es el jefe según los datos proporcionados, y si se aplica la comisión adjunta y coordinación.
     *
     * @param array $cadena Arreglo que contiene los datos de empleados, donde la clave es el ID del empleado.
     * @param stdClass $data_comi_alt Objeto que contiene los datos alternos de comisión, incluyendo las propiedades relacionadas con la comisión adjunta y la coordinación.
     * @param string $key_empleado_id Clave que se usa para obtener el ID del empleado desde el arreglo `$cadena`.
     * @param bool $tiene_jefe_inmediato Indica si el empleado tiene un jefe inmediato definido.
     * @return bool|array Retorna `true` si se aplica la comisión alterna, de lo contrario, retorna `false`. En caso de error, se retorna un arreglo con información del error.
     */
    private function es_comision_alterna(array $cadena, stdClass $data_comi_alt, string $key_empleado_id,
                                         bool $tiene_jefe_inmediato)
    {
        $key_empleado_id = trim($key_empleado_id);
        if($key_empleado_id === ''){
            return (new error())->error('Error $key_empleado_id esta vacio',$key_empleado_id);
        }
        if(!isset($cadena[$key_empleado_id])){
            return (new error())->error('Error no existe $cadena[$key_empleado_id]',$cadena);
        }
        $aplica_comision_alterna = false;
        if($tiene_jefe_inmediato) {
            $soy_jefe = $this->soy_jefe($cadena,$data_comi_alt,$key_empleado_id);
            if(error::$en_error){
                return (new error())->error('Error al validar si soy jefe',$soy_jefe);
            }

            if ($soy_jefe) {
                $soy_coord_adj = $this->soy_coord_ajunto($data_comi_alt);
                if(error::$en_error){
                    return (new error())->error('Error al validar si $soy_coord_adj',$soy_coord_adj);
                }
                if ($soy_coord_adj) {
                    $aplica_comision_alterna = true;
                }
            }
        }

        return $aplica_comision_alterna;

    }

    /**
     * TRASLADADO
     * Verifica si el objeto de datos alternos de comisión aplica tanto comisión adjunta como coordinación.
     *
     * Esta función verifica si el objeto `$data_comi_alt` tiene establecidas las propiedades `aplica_comision_adjunto` y `aplica_coordinador`.
     * Retorna `true` si ambas propiedades están establecidas y son verdaderas, de lo contrario, retorna `false`.
     *
     * @param stdClass $data_comi_alt Objeto que contiene los datos alternos de comisión, incluyendo las propiedades `aplica_comision_adjunto` y `aplica_coordinador`.
     * @return bool Retorna `true` si ambas propiedades `aplica_comision_adjunto` y `aplica_coordinador` son verdaderas, de lo contrario, retorna `false`.
     */
    private function soy_coord_ajunto(stdClass $data_comi_alt): bool
    {
        $out = false;
        if(isset($data_comi_alt->aplica_comision_adjunto)){
            if(isset($data_comi_alt->aplica_coordinador)){
                $out = $data_comi_alt->aplica_comision_adjunto &&  $data_comi_alt->aplica_coordinador;
            }
        }
        return $out;

    }

    /**
     * TRASLADADO
     * Verifica si el empleado en la cadena es el jefe inmediato según los datos alternos de comisión.
     *
     * Esta función compara el ID del empleado en la cadena con el ID del jefe inmediato almacenado en los datos alternos de comisión.
     * Retorna `true` si el ID del empleado en la cadena coincide con el ID del jefe inmediato, de lo contrario, retorna `false`.
     *
     * @param array $cadena Arreglo que contiene los datos de empleados, donde la clave es el ID del empleado.
     * @param stdClass $data_comi_alt Objeto que contiene los datos alternos de comisión, incluyendo la propiedad `empleado_jefe_inmediato_id`.
     * @param string $key_empleado_id Clave que se usa para obtener el ID del empleado desde el arreglo `$cadena`.
     * @return bool|array Retorna `true` si el empleado en la cadena es el jefe inmediato según los datos alternos, de lo contrario, retorna `false`.
     */
    private function soy_jefe(array $cadena, stdClass $data_comi_alt, string $key_empleado_id)
    {
        $key_empleado_id = trim($key_empleado_id);
        if($key_empleado_id === ''){
            return (new error())->error('Error $key_empleado_id esta vacio',$key_empleado_id);
        }
        if(!isset($cadena[$key_empleado_id])){
            return (new error())->error('Error no existe $cadena[$key_empleado_id]',$cadena);
        }
        $out = false;
        if(isset($data_comi_alt->empleado_jefe_inmediato_id)){
            $out = (int)$cadena[$key_empleado_id] === (int)$data_comi_alt->empleado_jefe_inmediato_id;
        }
        return $out;

    }

    /**
     * TRASLADADO
     * Verifica si el objeto de datos alternos de comisión tiene un jefe inmediato definido.
     *
     * Esta función revisa si la propiedad `empleado_jefe_inmediato_id` está definida en el objeto `$data_comi_alt`.
     * Si no está definida, retorna `false`; de lo contrario, retorna `true`.
     *
     * @param stdClass $data_comi_alt Objeto que contiene los datos alternos de comisión, incluyendo la propiedad `empleado_jefe_inmediato_id`.
     * @return bool Retorna `true` si la propiedad `empleado_jefe_inmediato_id` está definida, de lo contrario, retorna `false`.
     */
    private function tiene_jefe_inmediato(stdClass $data_comi_alt): bool
    {
        $tiene_jefe_inmediato = true;
        if(!isset($data_comi_alt->empleado_jefe_inmediato_id)){
            $tiene_jefe_inmediato = false;
        }

        return $tiene_jefe_inmediato;

    }

}
