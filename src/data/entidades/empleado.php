<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\_valida;
use desarrollo_em3\manejo_datos\consultas;
use PDO;
use stdClass;

class empleado
{
    /**
     * EM3
     * Genera un array de nombres de columnas relacionadas con un filtro de días de prueba.
     *
     * Esta función valida los parámetros proporcionados y construye un array de nombres de columnas
     * relacionadas con la entidad y un campo de fecha de inicio específico.
     *
     * @param string $campo_fecha_inicio El nombre del campo de fecha de inicio. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla). No debe estar vacío.
     *
     * @return array|array Devuelve un array con los nombres de columnas generados. En caso de error,
     *                     devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar columnas válidas
     * $campo_fecha_inicio = 'fecha_ingreso';
     * $entidad = 'empleados';
     * $resultado = $this->columnas_filtro_dias_prueba($campo_fecha_inicio, $entidad);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     'empleados_id',
     * //     'empleados_nombre_completo',
     * //     'empleados_fecha_ingreso',
     * //     'departamento_id',
     * //     'puesto_id'
     * // ]
     *
     * // Ejemplo 2: Entidad vacía (error)
     * $campo_fecha_inicio = 'fecha_ingreso';
     * $entidad = '';
     * $resultado = $this->columnas_filtro_dias_prueba($campo_fecha_inicio, $entidad);
     * // Resultado:
     * // ['error' => 'Error entidad esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Campo de fecha vacío (error)
     * $campo_fecha_inicio = '';
     * $entidad = 'empleados';
     * $resultado = $this->columnas_filtro_dias_prueba($campo_fecha_inicio, $entidad);
     * // Resultado:
     * // ['error' => 'Error $campo_fecha_inicio esta vacia', 'data' => '']
     * ```
     */
    private function columnas_filtro_dias_prueba(string $campo_fecha_inicio, string $entidad): array
    {
        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error entidad esta vacia', $entidad);
        }

        // Validar que el campo de fecha de inicio no esté vacío
        $campo_fecha_inicio = trim($campo_fecha_inicio);
        if ($campo_fecha_inicio === '') {
            return (new error())->error('Error $campo_fecha_inicio esta vacia', $campo_fecha_inicio);
        }

        // Construir y retornar el array de columnas
        return [
            $entidad . '_id',
            $entidad . '_nombre_completo',
            $entidad . '_' . $campo_fecha_inicio,
            'departamento_id',
            'puesto_id'
        ];
    }


    /**
     * TRASLADADO
     * Genera una lista de nombres de columnas para filtrar empleados administrativos.
     *
     * @param string $entidad Nombre de la entidad base (e.g., 'empleado', 'admin').
     * @param array $campos_add Campos adicionales que se deben agregar a la lista de columnas.
     * @return array La lista de nombres de columnas, incluyendo la entidad base y los campos adicionales.
     */
    private function columnas_filtro_empleado_admin(string $entidad, array $campos_add = array()): array
    {

        $entidad = trim($entidad);
        if($entidad === ''){
            return (new error())->error('Error entidad esta vacia', $entidad);
        }

        $campos_base = array($entidad.'_id', $entidad.'_salario', $entidad.'_bono_gasolina',
            $entidad.'_pago_semanal_infonavit', $entidad.'_aplica_garantia_gestor',
            $entidad.'_salario_diario', $entidad.'_nombre_completo', 'puesto_id', 'puesto_descripcion',
            'plaza_id', 'plaza_descripcion', 'departamento_id', 'departamento_descripcion',
            'puesto_dias_descanso');

        foreach ($campos_add as $campo){
            $campo = trim($campo);
            $campo = trim($campo);
            if($campo === ''){
                return (new error())->error('Error $campo esta vacio', $campo);
            }
            $campos_base[] = $campo;
        }

        return $campos_base;

    }

    /**
     * TRASLADADO
     * Genera una cláusula SQL para verificar que una fecha en una tabla es menor o igual a una fecha específica.
     *
     * @param int $departamento_id El identificador del departamento puede ser < a 0.
     * @param int $plaza_id Identificador de la plaza debe ser mayor a 0.
     * @param string $entidad El nombre de la tabla que contiene el campo de fecha.
     *
     * @return array Un conjunto de elementos para enviar a filtro and.
     */
    private function filtro_empleado_admin(int $departamento_id, int $plaza_id, string $entidad): array
    {
        $entidad = trim($entidad);
        if($entidad === ''){
            return (new error())->error('Error entidad esta vacia', $entidad);
        }
        if($plaza_id <= 0){
            return (new error())->error('Error $plaza_id debe ser mayor a 0', $plaza_id);
        }
        $filtro = array('plaza.id'=>$plaza_id, "$entidad.status"=>'activo',
            "$entidad.es_administrativo"=>'activo');
        if($departamento_id > 0){
            $filtro['departamento.id'] = $departamento_id;
        }

        return $filtro;

    }

    /**
     * EM3
     * Genera un filtro especial para días de prueba basado en fechas, entidades y una cláusula `IN`.
     *
     * Esta función valida los parámetros de entrada relacionados con fechas y una cláusula SQL `IN`,
     * y construye un array de filtros dinámicos para usar en consultas SQL.
     *
     * @param string $campo_fecha_inicio El nombre del campo de fecha de inicio en la entidad. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla) a la que pertenece el campo. No debe estar vacío.
     * @param string $fecha La fecha actual o de referencia para calcular la diferencia en días. No debe estar vacía.
     * @param int $n_dias_prueba El número de días de prueba que se desea validar. Debe ser mayor o igual a 0.
     * @param string $sql_plazas_in La cláusula SQL `IN` para filtrar por plazas. No debe estar vacía.
     *
     * @return array Devuelve un array con los filtros generados. En caso de error, devuelve un array
     *                     con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Filtro especial válido
     * $campo_fecha_inicio = 'fecha_inicio';
     * $entidad = 'empleados';
     * $fecha = '2023-01-01';
     * $n_dias_prueba = 30;
     * $sql_plazas_in = 'IN( 1,2,3 )';
     * $resultado = $this->filtro_especial_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $sql_plazas_in);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     "DATEDIFF('2023-01-01', empleados.fecha_inicio)" => [
     * //         'operador' => '=',
     * //         'valor' => 30
     * //     ],
     * //     'plaza.id' => [
     * //         'operador' => 'IN( 1,2,3 )',
     * //         'valor' => ''
     * //     ]
     * // ]
     *
     * // Ejemplo 2: Cláusula IN vacía (error)
     * $sql_plazas_in = '';
     * $resultado = $this->filtro_especial_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $sql_plazas_in);
     * // Resultado:
     * // ['error' => 'Error $sql_plazas_in esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Número de días de prueba negativo (error)
     * $n_dias_prueba = -10;
     * $resultado = $this->filtro_especial_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $sql_plazas_in);
     * // Resultado:
     * // ['error' => 'Error $n_dias_prueba debe ser mayor igual a 0', 'data' => -10]
     * ```
     */
    private function filtro_especial_dias_prueba(string $campo_fecha_inicio, string $entidad, string $fecha,
                                                 int $n_dias_prueba, string $sql_plazas_in): array
    {
        // Validar los campos base relacionados con fechas
        $valida = (new _valida())->valida_base_campos_fecha($campo_fecha_inicio, $entidad, $fecha);
        if (error::$en_error) {
            return (new error())->error('Error al validar campos base', $valida);
        }

        // Validar que $sql_plazas_in no esté vacío
        $sql_plazas_in = trim($sql_plazas_in);
        if ($sql_plazas_in === '') {
            return (new error())->error('Error $sql_plazas_in esta vacia', $sql_plazas_in);
        }

        // Validar que el número de días de prueba sea mayor o igual a 0
        if ($n_dias_prueba < 0) {
            return (new error())->error('Error $n_dias_prueba debe ser mayor igual a 0', $n_dias_prueba);
        }

        // Construir y retornar los filtros dinámicos
        return [
            "DATEDIFF('$fecha', $entidad.$campo_fecha_inicio)" => [
                'operador' => '=',
                'valor' => $n_dias_prueba
            ],
            'plaza.id' => [
                'operador' => $sql_plazas_in,
                'valor' => ''
            ]
        ];
    }


    private function genera_sentencia(array $columns, array $order): bool
    {
        $genera_sentencia = true;
        if(empty($order)) {
            $genera_sentencia = false;
        }
        if(empty($columns)) {
            $genera_sentencia = false;
        }
        return $genera_sentencia;

    }

    private function genera_sentencia_order(array $order,array $columns)
    {

        $genera_sentencia = $this->genera_sentencia($columns,$order);
        if(error::$en_error){
            return (new error())->error('Error al validar generacion de sentencia', $genera_sentencia);
        }

        $sentencia = $this->sentencia_order_datatable($columns,$genera_sentencia,$order);
        if(error::$en_error){
            return (new error())->error('Error al generar sentencia', $sentencia);
        }

        return $sentencia;
    }

    /**
     * Recupera los datos de empleados en formato de datatable.
     *
     * @param string $entidad_empleado  Nombre de la entidad que representa el empleado. No puede estar vacío.
     * @param PDO $link  Conexión PDO a la base de datos.
     * @param string $plaza_id  ID de la plaza para filtrar empleados. No puede estar vacío.
     * @param int $limit  Límite de resultados a obtener. Por defecto es 0, lo que indica sin límite.
     * @param int $offset  Desplazamiento desde donde iniciar la consulta. Por defecto es 0.
     * @param string $order_by  Columna por la cual ordenar los resultados. Puede estar vacío.
     * @param string $status  Estado del empleado a filtrar.
     * @param string $wh_like  Cadena de texto para aplicar filtro "like" en la consulta. Puede estar vacío.
     *
     * @return array  Devuelve los resultados de la consulta en formato de array de objetos si se ejecuta correctamente.
     *                       En caso de error, devuelve el mensaje de error correspondiente.
     */
    private function get_empleado_datatable(string $entidad_empleado,PDO $link,string $plaza_id,int $limit = 0,
                                            int $offset = 0, string $order_by = '',string $status='activo',string $wh_like = ''): array {
        if(trim($entidad_empleado) === '') {
            return (new error())->error('Error $entidad_empleado no puede ser vacio', $entidad_empleado);
        }
        if(trim($plaza_id) === '') {
            return (new error())->error('Error $plaza_id no puede ser vacio', $plaza_id);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\empleado())->get_empleado_datatable($entidad_empleado,$plaza_id,$status,$limit,
            $offset,$order_by,$wh_like);
        if(error::$en_error){
            return(new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link, $sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener relaciones',$exe);
        }

        return $exe;
    }


    /**
     * EM3
     * Genera un filtro especial para días de prueba basado en validaciones y una lista de plazas.
     *
     * Esta función valida los parámetros de entrada, genera una cláusula SQL `IN` para las plazas,
     * y construye un filtro especial utilizando las funciones auxiliares.
     *
     * @param string $campo_fecha_inicio El nombre del campo de fecha de inicio en la entidad. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla) a la que pertenece el campo. No debe estar vacío.
     * @param string $fecha La fecha actual o de referencia para calcular la diferencia en días. No debe estar vacía.
     * @param int $n_dias_prueba El número de días de prueba que se desea validar. Debe ser mayor o igual a 0.
     * @param array $plazas_id Un array de identificadores de plazas. No debe estar vacío.
     *
     * @return array Devuelve un array con los filtros generados. En caso de error, devuelve un array
     *               con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar filtro especial válido
     * $campo_fecha_inicio = 'fecha_inicio';
     * $entidad = 'empleados';
     * $fecha = '2023-01-01';
     * $n_dias_prueba = 30;
     * $plazas_id = [1, 2, 3];
     * $resultado = $this->get_filtro_especial_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $plazas_id);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     "DATEDIFF('2023-01-01', empleados.fecha_inicio)" => [
     * //         'operador' => '=',
     * //         'valor' => 30
     * //     ],
     * //     'plaza.id' => [
     * //         'operador' => 'IN( 1,2,3 )',
     * //         'valor' => ''
     * //     ]
     * // ]
     *
     * // Ejemplo 2: Lista de plazas vacía (error)
     * $plazas_id = [];
     * $resultado = $this->get_filtro_especial_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $plazas_id);
     * // Resultado:
     * // ['error' => 'Error al validar campos base', 'data' => [...]]
     *
     * // Ejemplo 3: Número de días de prueba negativo (error)
     * $n_dias_prueba = -10;
     * $resultado = $this->get_filtro_especial_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $plazas_id);
     * // Resultado:
     * // ['error' => 'Error al validar campos base', 'data' => [...]]
     * ```
     */
    private function get_filtro_especial_dias_prueba(string $campo_fecha_inicio, string $entidad, string $fecha,
                                                     int $n_dias_prueba, array $plazas_id): array
    {
        // Validar los parámetros de entrada
        $valida = (new _valida())->valida_entrada_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $plazas_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar campos base', $valida);
        }

        // Generar la cláusula SQL IN para las plazas
        $sql_plazas_in = (new \desarrollo_em3\manejo_datos\sql\empleado())->sql_plazas_in($plazas_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $sql_plazas_in);
        }

        // Generar el filtro especial basado en días de prueba
        $filtro_especial = $this->filtro_especial_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba,
            $sql_plazas_in);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $filtro_especial', $filtro_especial);
        }

        // Retornar el filtro especial generado
        return $filtro_especial;
    }


    /**
     * EM3
     * Genera los parámetros necesarios para un filtro de días de prueba, incluyendo filtros básicos,
     * filtros especiales y columnas relacionadas.
     *
     * Esta función valida los parámetros de entrada, genera filtros básicos, filtros especiales, y
     * una lista de columnas relacionadas con la entidad y los días de prueba.
     *
     * @param string $campo_fecha_inicio El nombre del campo de fecha de inicio en la entidad. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla). No debe estar vacío.
     * @param string $fecha La fecha actual o de referencia para calcular la diferencia en días. No debe estar vacía.
     * @param int $n_dias_prueba El número de días de prueba que se desea validar. Debe ser mayor o igual a 0.
     * @param array $plazas_id Un array de identificadores de plazas. No debe estar vacío.
     *
     * @return stdClass|array Devuelve un objeto con las claves:
     *                        - `filtro`: Filtros básicos aplicados a la entidad.
     *                        - `filtro_especial`: Filtros dinámicos generados para días de prueba.
     *                        - `columnas_basicas`: Columnas relacionadas con la entidad.
     *                        En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar parámetros válidos
     * $campo_fecha_inicio = 'fecha_ingreso';
     * $entidad = 'empleados';
     * $fecha = '2023-01-01';
     * $n_dias_prueba = 30;
     * $plazas_id = [1, 2, 3];
     * $resultado = $this->params_filtro_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $plazas_id);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [filtro] => Array (
     * //         [empleados.status] => 'activo'
     * //     )
     * //     [filtro_especial] => Array (
     * //         "DATEDIFF('2023-01-01', empleados.fecha_ingreso)" => Array (
     * //             [operador] => '=',
     * //             [valor] => 30
     * //         ),
     * //         [plaza.id] => Array (
     * //             [operador] => 'IN( 1,2,3 )',
     * //             [valor] => ''
     * //         )
     * //     )
     * //     [columnas_basicas] => Array (
     * //         [0] => 'empleados_id',
     * //         [1] => 'empleados_nombre_completo',
     * //         [2] => 'empleados_fecha_ingreso',
     * //         [3] => 'departamento_id',
     * //         [4] => 'puesto_id'
     * //     )
     * // )
     *
     * // Ejemplo 2: Error en validación de entrada
     * $plazas_id = [];
     * $resultado = $this->params_filtro_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $plazas_id);
     * // Resultado:
     * // ['error' => 'Error al validar campos base', 'data' => [...]]
     * ```
     */
    final public function params_filtro_dias_prueba(string $campo_fecha_inicio, string $entidad, string $fecha,
                                                    int $n_dias_prueba, array $plazas_id)
    {
        // Validar los parámetros de entrada
        $valida = (new _valida())->valida_entrada_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $plazas_id);
        if (error::$en_error) {
            return (new error())->error('Error al validar campos base', $valida);
        }

        // Generar filtros básicos
        $filtro = [$entidad . '.status' => 'activo'];

        // Generar filtros especiales
        $filtro_especial = $this->get_filtro_especial_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $plazas_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $filtro_especial', $filtro_especial);
        }

        // Generar columnas básicas
        $columnas_basicas = $this->columnas_filtro_dias_prueba($campo_fecha_inicio, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $columnas_basicas', $columnas_basicas);
        }

        // Construir y retornar el objeto de parámetros
        $params = new stdClass();
        $params->filtro = $filtro;
        $params->filtro_especial = $filtro_especial;
        $params->columnas_basicas = $columnas_basicas;

        return $params;
    }


    /**
     * TRASLADADO
     * Genera los parámetros necesarios para filtrar empleados administrativos.
     *
     * @param string $campo_fecha Campo de la fecha a utilizar en el filtro.
     * @param array $campos_add Campos adicionales a incluir en el filtro.
     * @param int $departamento_id ID del departamento para filtrar.
     * @param string $entidad Nombre de la entidad base.
     * @param string $fecha_inicio_menor_a Fecha límite inferior para el campo de la fecha.
     * @param int $plaza_id ID de la plaza para filtrar.
     * @return stdClass|array Objeto con los parámetros generados: sql_where_string, filtro, columnas_basicas.
     */
    final public function params_filtro_empleado_adm(string $campo_fecha, array $campos_add, int $departamento_id,
                                                     string $entidad, string $fecha_inicio_menor_a, int $plaza_id)
    {
        $campo_fecha = trim($campo_fecha);
        if($campo_fecha === ''){
            return (new error())->error('Error $campo_fecha debe tener info',$campo_fecha);
        }
        $entidad = trim($entidad);
        if($entidad === ''){
            return (new error())->error('Error $entidad debe tener info',$entidad);
        }
        if($plaza_id <= 0){
            return (new error())->error('Error $plaza_id debe ser mayor a 0', $plaza_id);
        }

        $sql_where_string = (new \desarrollo_em3\manejo_datos\sql\empleado())->sql_extra_fecha_inicio($campo_fecha,
            $fecha_inicio_menor_a,$entidad);
        if(error::$en_error){
            return (new error())->error('Error al generar sql de empleado',$sql_where_string);
        }

        $filtro = $this->filtro_empleado_admin($departamento_id,$plaza_id,$entidad);
        if(error::$en_error){
            return (new error())->error('Error al generar filtro',$filtro);
        }

        $columnas_basicas = $this->columnas_filtro_empleado_admin($entidad, $campos_add);
        if(error::$en_error){
            return (new error())->error('Error al generar $columnas_basicas',$columnas_basicas);
        }

        $params = new stdClass();
        $params->sql_where_string = $sql_where_string;
        $params->filtro = $filtro;
        $params->columnas_basicas = $columnas_basicas;

        return $params;

    }

    private function sentencia_order_datatable(array $columns, bool $genera_sentencia, array $order): string
    {
        $sentencia = '';
        if($genera_sentencia) {
            unset($columns[count($columns) - 1]);
            $key = $order[0]['column'];

            if (array_key_exists($key, $columns)) {
                $sentencia = $columns[$key]['data'] . " " . $order[0]['dir'];
            }
        }

        return $sentencia;

    }

    /**
     * Obtiene datos de empleados en una tabla según los filtros y parámetros especificados.
     *
     * @param string $entidad_empleado Identificador de la entidad de empleado. No puede estar vacío.
     * @param array $filtros Filtros aplicados a la consulta, incluyendo 'order_by' y 'wh_like'. No puede estar vacío.
     * @param PDO $link Conexión a la base de datos. Debe ser un objeto PDO válido.
     * @param string $plaza_id Identificador de la plaza. No puede estar vacío.
     * @param int $length Longitud de la consulta (número de registros a devolver). Por defecto es 0.
     * @param int $start Posición de inicio para la consulta (offset). Por defecto es 0.
     * @param string $status Estado de los empleados a filtrar. Por defecto es una cadena vacía.
     *
     * @return array Devuelve un array con los datos de empleados o un objeto de error en caso de fallo.
     *
     * @throws Exception Lanza una excepción si se producen errores durante la ejecución.
     */
    public function ssp(string $entidad_empleado,array $filtros,PDO $link,string $plaza_id,
                        int $length = 0, int $start = 0,string $status = 'activo'): array {
        if(trim($entidad_empleado) === '') {
            return (new error())->error('Error $entidad_empleado no puede ser vacio', $entidad_empleado);
        }
        if(trim($plaza_id) === '') {
            return (new error())->error('Error $plaza_id no puede ser vacio', $plaza_id);
        }
        if(empty($filtros)) {
            return (new error())->error('Error $post no puede ser vacio', $filtros);
        }

        $sentencia = $this->genera_sentencia_order($filtros['order_by'], $filtros['columns']);
        if(\desarrollo_em3\error\error::$en_error){
            return (new \desarrollo_em3\error\error())->error('Error al generar $sentencia',
                $sentencia);
        }
        $r_empleado = $this->get_empleado_datatable($entidad_empleado,$link,$plaza_id,
            $length,$start,$sentencia,$status,$filtros['wh_like']);
        if(\desarrollo_em3\error\error::$en_error){
            return (new \desarrollo_em3\error\error())->error('Error al obtener $r_empleado',
                $r_empleado);
        }

        return $r_empleado;
    }


}
