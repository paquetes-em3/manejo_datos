<?php
namespace desarrollo_em3\manejo_datos\data\entidades;

use DateTime;
use desarrollo_em3\calculo\calculo;
use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use stdClass;
use Throwable;

class asistencia_app
{

    private string $key_base_id = 'asistencia_app_id';

    /**
     * TRASLADADO
     * Actualiza la distancia del jefe en un registro de asistencia en la base de datos.
     *
     * Esta función genera y ejecuta una sentencia SQL para actualizar la columna `distancia_jefe` de un registro específico
     * en la tabla `asistencia_app`, basándose en el ID de la asistencia proporcionado. Utiliza una conexión PDO para realizar
     * la transacción de forma segura.
     *
     * @param int $asistencia_app_id ID del registro de asistencia en la tabla `asistencia_app`. Debe ser mayor a 0.
     * @param float $distancia_jefe La distancia calculada entre el empleado y su jefe inmediato.
     * @param PDO $link Conexión PDO a la base de datos para ejecutar la consulta.
     *
     * @return array Resultado de la ejecución de la consulta de actualización. En caso de error, devuelve un array con los detalles del error.
     */
    private function actualiza_distancia(int $asistencia_app_id, float $distancia_jefe, PDO $link): array
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id es menor a 0',$asistencia_app_id);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->actualiza_distancia(
            $asistencia_app_id,$distancia_jefe);
        if(error::$en_error){
            return (new error())->error('Error al obtener sql',$sql);
        }

        $upd = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al actualizar distancia',$upd);
        }
        return $upd;

    }

    /**
     * TRASLADADO
     * Actualiza la distancia entre un empleado y su jefe inmediato en un registro de asistencia.
     *
     * Esta función obtiene los parámetros necesarios para calcular la distancia entre el empleado y su jefe
     * inmediato basándose en el ID de asistencia proporcionado, y luego actualiza la columna `distancia_jefe`
     * en la tabla `asistencia_app`. Utiliza una conexión PDO para realizar la transacción.
     *
     * @param int $asistencia_app_id ID del registro de asistencia en la tabla `asistencia_app`. Debe ser mayor a 0.
     * @param string $entidad_empleado Nombre de la entidad del empleado. No debe estar vacía.
     * @param PDO $link Conexión PDO a la base de datos para ejecutar las consultas.
     *
     * @return stdClass|array Devuelve un objeto con los parámetros actualizados y la distancia modificada en caso de éxito,
     *                        o un array con detalles del error en caso de fallo.
     */
    private function actualiza_distancia_jefe(int $asistencia_app_id, string $entidad_empleado, PDO $link)
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id dbe ser mayor a 0',$asistencia_app_id);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $params = $this->params_distancia($asistencia_app_id,$entidad_empleado,$link);
        if(error::$en_error){
            return (new error())->error('Error al obtener $params',$params);
        }
        $upd = $this->actualiza_distancia($asistencia_app_id,$params->distancia_jefe, $link);
        if(error::$en_error){
            return (new error())->error('Error al modificar distancia',$upd);
        }

        $params->upd = $upd;
        return $params;

    }

    /**
     * TRASLADADO
     * Actualiza el estado de "checada_en_sitio" a 'activo' en un registro de asistencia basado en un empleado y una fecha.
     *
     * Esta función valida los parámetros de asistencia (`$entidad_empleado`, `$fecha` y `$ohem_id`) mediante la función
     * `valida_asistencia` de la clase `asistencia_app`. Luego, obtiene el ID del registro de asistencia utilizando
     * `asistencia_app_id`. Si el ID de asistencia es válido, actualiza el estado de "checada_en_sitio" a 'activo'
     * mediante la función `upd_checada_en_sitio_activa`. Si ocurre algún error durante la validación, obtención del
     * ID de asistencia o actualización del estado, devuelve un array con los detalles del error.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param string $fecha Fecha de la asistencia (en formato 'YYYY-MM-DD').
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $ohem_id ID del empleado (referenciado en la tabla `asistencia_app`).
     *
     * @return array Devuelve el resultado de la actualización si es exitosa, o un array de error si ocurre algún problema.
     */
    private function actualiza_en_sitio(string $entidad_empleado, string $fecha, PDO $link, int $ohem_id): array
    {
        $valida = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->valida_asistencia(
            $entidad_empleado,$fecha,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al validar asistencia',$valida);
        }
        $asistencia_app_id = $this->asistencia_app_id($entidad_empleado,$fecha,$link,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $asistencia_app_id',$asistencia_app_id);
        }

        $tiene_checada_app_upd = $this->upd_checada_en_sitio_activa($asistencia_app_id,$link);
        if(error::$en_error){
            return (new error())->error('Error al actualizar tiene checada',$tiene_checada_app_upd);
        }
        return $tiene_checada_app_upd;

    }

    /**
     * TRASLADADO
     * Ajusta el estado de la asistencia si es válida, actualizando el registro en la tabla `asistencia_app`.
     *
     * La función primero valida si la asistencia es válida utilizando el método `asistencia_valida`. Si la asistencia es válida
     * y el ID del registro está presente y es mayor a 0, actualiza el estado de asistencia a 'activo' en la base de datos.
     * En caso de errores, devuelve un array con los detalles del error.
     *
     * @param stdClass $asistencia_app Un objeto que contiene los datos de la asistencia, incluido el ID del registro.
     * @param PDO $link La conexión PDO utilizada para ejecutar las consultas de actualización.
     *
     * @return bool|array Devuelve `true` si la asistencia es válida y se ha actualizado correctamente. Devuelve un array de error si se produce algún fallo.
     */
    private function ajusta_asistencia(stdClass $asistencia_app, PDO $link)
    {
        $asistencia_valida = $this->asistencia_valida($asistencia_app);
        if(error::$en_error){
            return (new error())->error('Error al actualizar obtener asistencia valida',$asistencia_valida);
        }

        if($asistencia_valida){
            if(!isset($asistencia_app->id) ){
                return (new error())->error('Error $asistencia_app->id debe ser mayor a 0',$asistencia_app);
            }
            if(trim($asistencia_app->id)===''){
                return (new error())->error('Error $asistencia_app->id debe esta vacia',$asistencia_app);
            }
            if((int)($asistencia_app->id) <= 0){
                return (new error())->error('Error $asistencia_app->id debe ser mayor a 0',$asistencia_app);
            }
            $upd = $this->upd_asistencia($asistencia_app->id, $link);
            if(error::$en_error){
                return (new error())->error('Error al actualizar asistencia',$upd);
            }
        }
        return $asistencia_valida;

    }


    private function ajusta_asistencias(array $asistencias, string $entidad_empleado, PDO $link): array
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $datas = array();
        foreach ($asistencias as $row) {
            if(!is_object($row)){
                return  (new error())->error('Error $row debe ser un objeto', $row);
            }
            if(!isset($row->id)){
                return  (new error())->error('Error $row->id debe exitir', $row);
            }
            if(!is_numeric($row->id)){
                return  (new error())->error('Error $row->id debe ser un numero', $row);
            }
            if((int)$row->id <= 0){
                return  (new error())->error('Error $row->id debe ser mayor a 0', $row);
            }
            $data = $this->data_dia_pago($row->id,$entidad_empleado,$link);
            if (error::$en_error) {
                return  (new error())->error('Error al modificar datos de asistencia', $data);
            }
            $datas[] = $data;
        }
        return $datas;

    }


    final public function ajusta_asistencias_por_dia(string $entidad_empleado, string $fecha, PDO $link): array
    {
        $fecha = trim($fecha);
        if($fecha === ''){
            return (new error())->error('Error fecha esta vacia',$fecha);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $asistencias = $this->asistencias_por_dia($fecha, $link);
        if (error::$en_error) {
            return  (new error())->error('Error al obtener asistencia', $asistencias);
        }

        $regenera = (new asistencia_app())->ajusta_asistencias($asistencias,$entidad_empleado,$link);
        if (error::$en_error) {
            return  (new error())->error('Error al modificar datos de asistencia', $regenera);
        }
        return $regenera;

    }

    /**
     * TRASLADADO
     * Ajusta la distancia entre un empleado y su jefe inmediato, y luego ajusta la distancia de manera recursiva para sus subordinados.
     *
     * Esta función actualiza la distancia entre un empleado y su jefe inmediato en la tabla `asistencia_app` y,
     * luego, de manera recursiva ajusta la distancia de todos los subordinados del empleado si los hubiera.
     *
     * @param int $asistencia_app_id ID del registro de asistencia en la tabla `asistencia_app`. Debe ser mayor a 0.
     * @param string $entidad_empleado Nombre de la entidad del empleado. No debe estar vacía.
     * @param PDO $link Conexión PDO a la base de datos para ejecutar las consultas.
     *
     * @return stdClass|array Devuelve un objeto con los datos actualizados y las distancias ajustadas en caso de éxito,
     *                        o un array con detalles del error en caso de fallo.
     */
    private function ajusta_distancia(int $asistencia_app_id, string $entidad_empleado, PDO $link)
    {

        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id dbe ser mayor a 0',$asistencia_app_id);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }

        $data = $this->actualiza_distancia_jefe($asistencia_app_id,$entidad_empleado,$link);
        if(error::$en_error){
            return (new error())->error('Error al modificar distancia',$data);
        }

        $exe = $this->ajusta_distancia_recursive($data, $entidad_empleado,$link);
        if(error::$en_error){
            return (new error())->error('Error al ajustar $distancia_upd',$exe);
        }


        return $data;

    }

    /**
     * TRASLADADO
     * Ajusta de manera recursiva la distancia entre un empleado y su jefe inmediato para todos sus subordinados.
     *
     * Esta función toma los datos de asistencia y ajusta recursivamente la distancia de los empleados subordinados
     * al empleado indicado. Cada vez que encuentra subordinados, ajusta la distancia para cada uno de ellos.
     *
     * @param stdClass $data Objeto que contiene la fila de asistencia del empleado principal, incluyendo sus datos.
     * @param string $entidad_empleado Nombre de la entidad del empleado. No debe estar vacía.
     * @param PDO $link Conexión PDO a la base de datos para ejecutar las consultas.
     *
     * @return array Devuelve un array con los resultados del ajuste de distancia de los subordinados.
     *               Si ocurre un error, devuelve un array con detalles del error.
     */
    final public function ajusta_distancia_recursive(stdClass $data, string $entidad_empleado, PDO $link): array
    {
        if(!isset($data->asistencia_row)){
            return (new error())->error('Error no existe $data->asistencia_row',$data);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $rows = $this->subordinados($data->asistencia_row,$entidad_empleado, $link);
        if(error::$en_error){
            return (new error())->error('Error al obtener $rows',$rows);
        }

        $exe['a_su_cargo'] = array();
        foreach ($rows as $row){
            $distancia_upd = $this->ajusta_distancia($row->id,$entidad_empleado, $link);
            if(error::$en_error){
                return (new error())->error('Error al ajustar $distancia_upd',$distancia_upd);
            }
            $exe['a_su_cargo'][] = $distancia_upd;
        }

        return $exe;

    }

    final public function asistencia(string $entidad_empleado, PDO $link, int $registro_percepcion_id) {

        $r_registro_percepcion =
            (new registro_percepcion())->obten_registro_percepcion($entidad_empleado,$link, $registro_percepcion_id);
        if(error::$en_error) {
            return (new error())->error('Error al asignar formato', $r_registro_percepcion);
        }

        if(count($r_registro_percepcion) <= 0) {
            return (new error())->error('Error al obtener registro $r_registro_percepcion_modelo',
                $r_registro_percepcion);
        }

        $periodo_pago_fecha_inicial = $r_registro_percepcion[0]->periodo_pago_fecha_inicial;
        $periodo_pago_fecha_final = $r_registro_percepcion[0]->periodo_pago_fecha_final;
        $ohem_id = $r_registro_percepcion[0]->ohem_id;

        $dias_laborados = $this->asistencia_conteo_dias(
            $entidad_empleado, $periodo_pago_fecha_inicial, $periodo_pago_fecha_final, $link, $ohem_id,
            'activo');

        if(error::$en_error) {
            return (new error())->error('Error al obtener dias laborados', $dias_laborados);
        }

        $dias_no_laborados = $this->asistencia_conteo_dias(
            $entidad_empleado, $periodo_pago_fecha_inicial, $periodo_pago_fecha_final, $link, $ohem_id,
            'inactivo');
        if(error::$en_error) {
            return (new error())->error('Error al obtener $dias_no_laborados', $dias_no_laborados);
        }

        $dias_no_registrados = $this->dias_no_registrados(
            $dias_laborados,$dias_no_laborados, $r_registro_percepcion[0]->periodicidad_pago_n_elementos);
        if(error::$en_error) {
            return (new error())->error('Error al obtener $dias_no_registrados', $dias_no_registrados);
        }


        $data = $r_registro_percepcion[0];
        $data->dias_laborados = $dias_laborados;
        $data->dias_no_laborados = $dias_no_laborados;
        $data->dias_no_registrados = $dias_no_registrados;


        return $data;
    }

    /**
     * TRASLADADO
     * Obtiene el ID de un registro de asistencia en la aplicación basado en un empleado y una fecha.
     *
     * Esta función valida los parámetros de asistencia (`$entidad_empleado`, `$fecha` y `$ohem_id`) mediante la función
     * `valida_asistencia` de la clase `asistencia_app`. Luego, genera una consulta SQL para obtener el ID del registro de
     * asistencia correspondiente utilizando `asistencia_app_id` y ejecuta la consulta en la base de datos con `exe_objs`.
     * Si la asistencia es encontrada, devuelve el ID de la asistencia; en caso contrario, devuelve -1. Si ocurre algún error
     * durante la validación, obtención de la consulta o ejecución, se devuelve un array con los detalles del error.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param string $fecha Fecha de la asistencia (en formato 'YYYY-MM-DD').
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $ohem_id ID del empleado (referenciado en la tabla `asistencia_app`).
     *
     * @return int|array Devuelve el ID de asistencia si existe, -1 si no se encuentra,
     *                   o un array de error si ocurre algún problema durante el proceso.
     */
    private function asistencia_app_id(string $entidad_empleado,string $fecha, PDO $link,int $ohem_id)
    {
        $valida = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->valida_asistencia(
            $entidad_empleado,$fecha,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al validar asistencia',$valida);
        }

        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->asistencia_app_id(
            $entidad_empleado,$fecha,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al ejecutar $sql',$exe);
        }
        $asistencia_app_id = -1;
        if(count($exe)>0) {
            (int)$asistencia_app_id = $exe[0]->asistencia_app_id;
        }
        return $asistencia_app_id;

    }

    /**
     * TRASLADADO
     * Valida los datos de asistencia y asigna el día si aún no ha sido asignado.
     *
     * La función valida los datos de la asistencia proporcionada y, si el campo `dia` tiene el valor 'POR ASIGNAR',
     * procede a ejecutar la asignación del día correspondiente mediante la función `exe_dia`. Si ocurre algún error,
     * se devuelve un array con los detalles del fallo.
     *
     * @param stdClass $asistencia_app Un objeto que contiene los datos de la asistencia, incluyendo `fecha`, `id` y `dia`.
     * @param PDO $link La conexión PDO utilizada para ejecutar las consultas.
     *
     * @return stdClass|array Devuelve un objeto con el resultado de la asignación del día si fue exitoso, o un array
     *                        con los detalles del error si ocurre algún fallo.
     */
    private function asistencia_transacciona_dia(stdClass $asistencia_app, PDO $link)
    {
        $valida = $this->valida_asistencia($asistencia_app);
        if(error::$en_error){
            return (new error())->error('Error al validar asistencia',$valida);
        }
        if(!isset($asistencia_app->dia)){
            return (new error())->error('Error $asistencia_app->dia',$asistencia_app);
        }

        $exe = new stdClass();
        if($asistencia_app->dia === 'POR ASIGNAR') {

            $exe = $this->exe_dia($asistencia_app, $link);
            if(error::$en_error){
                return (new error())->error('Error al actualizar ajustar asistencia',$exe);
            }
        }
        return $exe;

    }


    private function asistencia_jefe_upd(int $asistencia_app_id, string $entidad_empleado, PDO $link)
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id debe ser mayor a 0',$asistencia_app_id);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $asistencia_row = (new consultas())->registro_bruto(new stdClass(),'asistencia_app',
            $asistencia_app_id,$link,false);
        if(error::$en_error){
            return (new error())->error('Error al obtener $asistencia_row',$asistencia_row);
        }

        $upd = $this->upd_checada_con_jefe($asistencia_row,$entidad_empleado,$link);
        if (error::$en_error) {
            return (new error())->error('Error al actualizar $asistencia_row', $upd);
        }
        $upd->asistencia_row = $asistencia_row;
        return $upd;

    }

    /**
     * TRASLADADO
     * Valida la asistencia de un empleado en función de la distancia, la checada en sitio, y si el jefe ha checado.
     *
     * Esta función primero valida la distancia entre el empleado y su jefe utilizando la función `distancia_valida`.
     * Luego, verifica los campos `checada_en_sitio` y `jefe_checo`, asignando valores predeterminados de 'inactivo'
     * si no están definidos. La asistencia se considera válida si:
     * - La checada en sitio es 'activo'.
     * - El jefe también ha checado ('activo').
     * - La distancia entre el empleado y el jefe es válida.
     *
     * Si todas estas condiciones se cumplen, la función retorna `true`. De lo contrario, retorna `false`.
     *
     * @param stdClass $asistencia_app Un objeto que contiene los datos de asistencia, incluyendo la distancia,
     *                                 el estado de checada en sitio y si el jefe ha checado.
     *
     * @return bool|array Retorna `true` si la asistencia es válida. Si ocurre un error al validar la distancia,
     *                    retorna un array de error con los detalles del fallo.
     */
    private function asistencia_valida(stdClass $asistencia_app)
    {
        $distancia_valida = $this->distancia_valida($asistencia_app);
        if(error::$en_error){
            return (new error())->error('Error al actualizar obtener distancia valida',$distancia_valida);
        }
        if(!isset($asistencia_app->checada_en_sitio)){
            $asistencia_app->checada_en_sitio = 'inactivo';
        }

        if(!isset($asistencia_app->jefe_checo)){
            $asistencia_app->jefe_checo = 'inactivo';
        }

        $asistencia_valida = false;
        if($asistencia_app->checada_en_sitio === 'activo'){
            if($asistencia_app->jefe_checo === 'activo'){
                if($distancia_valida){
                    $asistencia_valida = true;
                }
            }
        }

        return $asistencia_valida;

    }

    /**
     * TRASLADADO
     * Valida y ajusta la asistencia de un empleado en caso de que esté inactiva.
     *
     * La función verifica si el campo `asistencia` del objeto `asistencia_app` existe. Si el estado de asistencia es 'inactivo',
     * se ajusta la asistencia a través de la función `ajusta_asistencia`. Si ocurre algún error durante el ajuste de la asistencia,
     * se devuelve un array con los detalles del error.
     *
     * @param stdClass $asistencia_app Un objeto que contiene los datos de la asistencia, incluyendo el campo `asistencia`.
     * @param PDO $link La conexión PDO utilizada para ejecutar las consultas y transacciones relacionadas con la asistencia.
     *
     * @return bool|array Devuelve `true` si la asistencia fue ajustada correctamente. Si no se requiere ajuste, devuelve `false`.
     *                    Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function asistencia_valida_transacciona(stdClass $asistencia_app, PDO $link)
    {
        if(!isset($asistencia_app->asistencia)){
            return (new error())->error('Error $asistencia_app->asistencia no existe',$asistencia_app);
        }
        $asistencia_valida = false;
        if($asistencia_app->asistencia === 'inactivo'){
            $asistencia_valida = $this->ajusta_asistencia($asistencia_app,$link);
            if(error::$en_error){
                return (new error())->error('Error al actualizar ajustar asistencia',$asistencia_valida);
            }

        }
        return $asistencia_valida;

    }

    /**
     * TRASLADADO
     * Obtiene las asistencias de empleados dentro de un período específico, validando los datos de entrada y ejecutando la consulta SQL.
     *
     * La función primero valida los parámetros necesarios como el campo de fecha de ingreso, la entidad del empleado,
     * y las fechas del período. Luego genera una consulta SQL para obtener las asistencias correspondientes al período especificado.
     * Si ocurre algún error durante el proceso, devuelve un array con los detalles del fallo.
     *
     * @param string $campo_fecha_ingreso El nombre del campo de la fecha de ingreso del empleado.
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     * @param string $fecha_final La fecha final del período (en formato `YYYY-MM-DD`).
     * @param string $fecha_inicial La fecha inicial del período (en formato `YYYY-MM-DD`).
     * @param PDO $link La conexión PDO utilizada para ejecutar la consulta SQL.
     *
     * @return array Devuelve un array con los registros de asistencias obtenidos. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function asistencias_periodo(string $campo_fecha_ingreso, string $entidad_empleado,
                                              string $fecha_final, string $fecha_inicial, PDO $link): array
    {
        $valida = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->valida_data_asistencia(
            $campo_fecha_ingreso, $entidad_empleado,$fecha_final,$fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al validar datos', $valida);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->asistencias_periodo(
            $campo_fecha_ingreso,$entidad_empleado,$fecha_final,$fecha_inicial);
        if (error::$en_error) {
            return  (new error())->error('Error al obtener $sql', $sql);
        }

        $asistencias = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener asistencias',$asistencias);
        }
        return $asistencias;

    }

    /**
     * TRASLADADO
     * Obtiene todas las asistencias correspondientes a una fecha específica desde la base de datos.
     *
     * La función valida que la fecha no esté vacía, genera una consulta SQL para obtener las asistencias del día especificado,
     * y ejecuta dicha consulta en la base de datos utilizando una conexión PDO. Si ocurre algún error durante el proceso,
     * se devuelve un array con los detalles del fallo.
     *
     * @param string $fecha La fecha en formato `YYYY-MM-DD` para la cual se quieren obtener las asistencias.
     * @param PDO $link La conexión PDO utilizada para ejecutar la consulta SQL.
     *
     * @return array Devuelve un array con los registros de asistencia obtenidos. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function asistencias_por_dia(string $fecha, PDO $link): array
    {
        $fecha = trim($fecha);
        if($fecha === ''){
            return (new error())->error('Error fecha esta vacia',$fecha);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->asistencias_por_dias($fecha);
        if (error::$en_error) {
            return  (new error())->error('Error al obtener asistencia sql', $sql);
        }

        $rows = (new consultas())->exe_objs($link,$sql);
        if (error::$en_error) {
            return  (new error())->error('Error al obtener asistencia', $rows);
        }
        return $rows;

    }

    final public function asistencia_conteo_dias(string $entidad_empleado, string $fecha_inicial,
                                                 string $fecha_final, PDO $link, int $ohem_id, string $status)
    {
        if($ohem_id <= 0){
            return (new error())->error('Error $ohem_id es menor a 0',$ohem_id);
        }

        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }

        $f_inicio = trim($fecha_inicial);
        if($f_inicio === ''){
            return (new error())->error('Error fecha esta vacia',$f_inicio);
        }
        $f_fin = trim($fecha_final);
        if($f_fin === ''){
            return (new error())->error('Error fecha esta vacia',$f_fin);
        }

        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->asistencia_conteo_dias($entidad_empleado,
            $fecha_inicial, $fecha_final, $ohem_id, $status);
        if (error::$en_error) {
            return  (new error())->error('Error al obtener asistencia sql', $sql);
        }

        $exe = (new consultas())->exe_objs($link,$sql);
        if (error::$en_error) {
            return  (new error())->error('Error al obtener asistencia', $exe);
        }
        return (int)$exe[0]->dias;
    }

    /**
     * TRASLADADO
     * Obtiene las asistencias recientes inactivas desde la base de datos, aplicando un límite y un desplazamiento (offset).
     *
     * La función valida que el valor de offset sea mayor o igual a 0. Luego genera una consulta SQL para obtener las
     * asistencias recientes inactivas y ejecuta esa consulta. Si ocurre algún error durante la generación de la consulta
     * o la ejecución de la misma, devuelve un array con los detalles del fallo.
     *
     * @param PDO $link La conexión PDO utilizada para ejecutar la consulta.
     * @param int $offset El número de registros a omitir (desplazamiento) en la consulta SQL. Debe ser mayor o igual a 0.
     *
     * @return array Devuelve un array con las asistencias recientes obtenidas. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function asistencias_recientes(PDO $link, int $offset): array
    {
        if($offset < 0){
            return (new error())->error('Error $offset debe ser mayor a 0',$offset);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->asistencias_recientes($offset);
        if(error::$en_error){
            return  (new error())->error('Error al obtener $sql',$sql);
        }
        $rows = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return  (new error())->error('Error al obtener $rows',$rows);
        }
        return $rows;

    }

    final public function asistencias_recientes_ajusta(string $entidad_empleado, PDO $link)
    {

        $itera = true;
        $offset = 0;
        $n_upds = 0;
        while($itera){

            $asistencias = $this->asistencias_recientes($link, $offset);
            if(error::$en_error){
                return  (new error())->error('Error al obtener $asistencias',$asistencias);
            }
            if(count($asistencias) === 0){
                $itera = false;
            }
            else{
                foreach ($asistencias as $asistencia_app) {
                    $data = $this->data_dia_pago($asistencia_app->id, $entidad_empleado, $link);
                    if (error::$en_error) {
                        return (new error())->error('Error al modificar datos de asistencia', $data);
                    }
                    if ($data->asistencia_valida) {
                        $n_upds++;
                    }
                }
                if($n_upds > 0){
                    $itera = false;
                }
            }
            $offset = $offset + 10;

        }

        return $n_upds;


    }

    final public function checada_app(string $entidad_empleado,int $event_tw_id, PDO $link )
    {
        $row = (new consultas())->registro_bruto(new stdClass(), 'event_tw',$event_tw_id,$link,false);
        if(error::$en_error) {
            return (new error())->error('Error al obtener event tw',$row);
        }

        $fecha_pura = explode(' ',$row->fecha);
        $fecha_pura_hoy = $fecha_pura[0];
        $key_empleado_id = $entidad_empleado.'_id';
        $tiene_checada_app = $this->integra_checada_sitio($entidad_empleado,$fecha_pura_hoy,$link,
            $row->$key_empleado_id);
        if(error::$en_error){
            return (new error())->error('Error al validar si tiene checada',$tiene_checada_app);
        }
        return $tiene_checada_app;

    }

    /**
     * TRASLADADO
     * Actualiza un array de empleado con los datos de asistencia de un día específico.
     *
     * Esta función toma un objeto de asistencia por día, valida que el día no esté vacío, y extrae varios datos relacionados con
     * la asistencia, como la hora de checada, si el jefe ha checado, el estado del checador, la distancia con el jefe, y si la
     * asistencia aplica para el pago. Estos datos se agregan al array del empleado utilizando como claves el nombre del día
     * seguido del sufijo correspondiente.
     *
     * Si alguno de los datos no está definido en el objeto de asistencia, se asignan valores predeterminados:
     * - La hora de entrada (`hora_entrada`) es una cadena vacía.
     * - El jefe checó (`jefe_checo`) es 'inactivo'.
     * - El checador (`checador`) es 'inactivo'.
     * - La distancia al jefe (`distancia_jefe`) es -1.
     * - La asistencia (`asistencia`) es 'inactivo'.
     *
     * @param stdClass $asistencia_dia Objeto que contiene los datos de asistencia para el día.
     * @param string $dia El día de la asistencia que se va a procesar. No debe estar vacío.
     * @param array $empleado El array del empleado que se actualizará con los datos de asistencia.
     *
     * @return array Retorna el array del empleado actualizado con los datos de asistencia para el día específico.
     *                     Si ocurre un error, retorna un array de error con los detalles del fallo.
     */
    private function checada_con_dato(stdClass $asistencia_dia, string $dia, array $empleado): array
    {
        $dia = trim($dia);
        if($dia === ''){
            return (new error())->error('Error $dia esta vacio',$dia);
        }
        $hora_entrada = '';
        if(isset($asistencia_dia->hora_entrada)){
            $hora_entrada = trim($asistencia_dia->hora_entrada);
        }
        $jefe_checo = 'inactivo';
        if(isset($asistencia_dia->jefe_checo)){
            $jefe_checo = trim($asistencia_dia->jefe_checo);
        }
        $checador = 'inactivo';
        if(isset($asistencia_dia->checador)){
            $checador = trim($asistencia_dia->checador);
        }
        $distancia_jefe = -1;
        if(isset($asistencia_dia->distancia_jefe)){
            $distancia_jefe = trim($asistencia_dia->distancia_jefe);
        }
        $asistencia = 'inactivo';
        if(isset($asistencia_dia->asistencia)){
            $asistencia = trim($asistencia_dia->asistencia);
        }

        $empleado["$dia"."_APP_AS"] = $hora_entrada;
        $empleado["$dia"."_JEFE_CHECO"] = $jefe_checo;
        $empleado["$dia"."_checador"] = $checador;
        $empleado["$dia"."_distancia_jefe"] = $distancia_jefe;
        $empleado["$dia"."_aplica_pago"] = $asistencia;

        return $empleado;

    }

    private function checada_empleado(stdClass $empleado_actual, array $empleados_full, string $estructura,
                                      string $fecha_final, string $fecha_inicial, PDO $link, int $ohem_id): array
    {
        $empleado = $this->row_checada_rpt(
            $empleado_actual,$empleados_full, $estructura, $link,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener datos de asistencia',$empleado);
        }
        $empleado = $this->row_checada_rpt_detalle($empleado,$empleados_full,$fecha_final,$fecha_inicial,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener datos de asistencia',$empleado);
        }
        return $empleado;

    }


    final public function data_dia_pago(int $asistencia_app_id, string $entidad_empleado, PDO $link)
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id dbe ser mayor a 0',$asistencia_app_id);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }

        $rs = new stdClass();
        $distancia_jefe = $this->ajusta_distancia($asistencia_app_id,$entidad_empleado,$link);
        if(error::$en_error){
            return (new error())->error('Error al modificar $distancia_jefe',$distancia_jefe);
        }
        $rs->distacia_jefe = $distancia_jefe;

        $upd = $this->upd_checada_desde_asis($asistencia_app_id,$entidad_empleado,$link);

        if(error::$en_error){
            return (new error())->error('Error al actualizar tiene checada',$upd);
        }
        $rs->desde_biometrico = $upd;
        $upd = $this->asistencia_jefe_upd($asistencia_app_id,$entidad_empleado,$link);
        if (error::$en_error) {
            return (new error())->error('Error al actualizar $asistencia_row', $upd);
        }
        $rs->checada_jefe = $upd;
        $asistencia_app = (new consultas())->registro_bruto(new stdClass(),'asistencia_app',
            $asistencia_app_id,$link,false);
        if(error::$en_error){
            return (new error())->error('Error al obtener datos de asistencia',$asistencia_app);
        }
        $asistencia_valida = $this->asistencia_valida_transacciona($asistencia_app,$link);
        if(error::$en_error){
            return (new error())->error('Error al actualizar ajustar asistencia',$asistencia_valida);
        }

        $rs->asistencia_valida = $asistencia_valida;
        $tr_dia = $this->asistencia_transacciona_dia($asistencia_app,$link);
        if(error::$en_error){
            return (new error())->error('Error al actualizar ajustar $tr_dia',$tr_dia);
        }

        $rs->dia_transaccion = $tr_dia;

        return $rs;

    }

    /**
     * TRASLADADO
     * Obtiene el nombre del día de la semana en español a partir de una fecha proporcionada.
     *
     * Esta función toma una cadena de texto que representa una fecha, valida que no esté vacía,
     * y luego obtiene el número del día de la semana (0 para domingo, 6 para sábado) utilizando
     * la función `date` de PHP. Posteriormente, retorna el nombre del día en español en mayúsculas.
     *
     * @param string $fecha La fecha en formato 'Y-m-d' u otro formato reconocible por strtotime.
     *
     * @return string|array Retorna el nombre del día de la semana en español si la fecha es válida.
     *                      Si la fecha está vacía, retorna un array de error con los detalles del fallo.
     */
    private function dia_espaniol(string $fecha)
    {
        $fecha = trim($fecha);
        if($fecha === ''){
            return (new error())->error('Error $fecha esta vacia',$fecha);
        }
        $n_dia = (int)date('w', strtotime($fecha));

        $dias_espaniol[0] = 'DOMINGO';
        $dias_espaniol[1] = 'LUNES';
        $dias_espaniol[2] = 'MARTES';
        $dias_espaniol[3] = 'MIERCOLES';
        $dias_espaniol[4] = 'JUEVES';
        $dias_espaniol[5] = 'VIERNES';
        $dias_espaniol[6] = 'SABADO';

        return $dias_espaniol[$n_dia];

    }

    private function dias_no_registrados(int $dias_laborados, int $dias_no_laborados, int $periodicidad_pago_n_elementos): int
    {
        return $periodicidad_pago_n_elementos - ($dias_laborados + $dias_no_laborados);

    }

    /**
     * TRASLADADO
     * Calcula el número de días en un periodo dado por dos fechas.
     *
     * Esta función recibe dos fechas como cadenas, las valida, y calcula la diferencia en días entre ellas,
     * incluyendo el día final en el cálculo. Si las fechas están vacías o no tienen un formato válido,
     * se retorna un array con los detalles del error.
     *
     * @param string $fecha_final La fecha final del periodo. No debe estar vacía y debe ser una fecha válida.
     * @param string $fecha_inicial La fecha inicial del periodo. No debe estar vacía y debe ser una fecha válida.
     *
     * @return int|array Retorna un entero que representa el número de días en el periodo, incluyendo el día final.
     *                   Si ocurre un error, retorna un array con los detalles del error.
     *
     *
     * @access private
     */
    private function dias_periodo(string $fecha_final, string $fecha_inicial)
    {
        $fecha_final = trim($fecha_final);
        if($fecha_final === ''){
            return (new error())->error('Error $fecha_final esta vacia',$fecha_final);
        }
        $fecha_inicial = trim($fecha_inicial);
        if($fecha_inicial === ''){
            return (new error())->error('Error $fecha_inicial esta vacia',$fecha_inicial);
        }
        try {
            $fecha_inicial_obj = new DateTime($fecha_inicial);
        }
        catch (Throwable $e){
            return (new error())->error('Error al ajustar $fecha_inicial',$e);
        }
        try {
            $fecha_final_obj = new DateTime($fecha_final);
        }
        catch (Throwable $e){
            return (new error())->error('Error al ajustar $fecha_final',$e);
        }

        $diferencia = $fecha_inicial_obj->diff($fecha_final_obj);

        return $diferencia->days + 1;

    }

    /**
     * TRASLADADO
     * Calcula la distancia entre un empleado y su jefe inmediato basado en las coordenadas geográficas.
     *
     * Esta función obtiene las coordenadas (latitud y longitud) de la checada del jefe inmediato de un empleado en una fecha específica
     * y calcula la distancia entre ambos. Si no se encuentra una checada válida del jefe, la distancia por defecto es `-1`.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param string $fecha Fecha en la que se busca la checada del jefe inmediato.
     * @param PDO $link Conexión PDO a la base de datos.
     * @param float $latitud_empleado Latitud del empleado en el momento de la asistencia.
     * @param float $longitud_empleado Longitud del empleado en el momento de la asistencia.
     * @param int $ohem_jefe_inmediato_id ID del jefe inmediato del empleado.
     *
     * @return float|array Devuelve la distancia en metros entre el empleado y su jefe inmediato. Si no se puede calcular la distancia,
     * devuelve un array de error con los detalles.
     */
    private function distancia_jefe(string $entidad_empleado, string $fecha, PDO $link, float $latitud_empleado,
                                    float $longitud_empleado,  int $ohem_jefe_inmediato_id)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }

        $param = $this->params_checada_jefe($entidad_empleado, $fecha,$link,$ohem_jefe_inmediato_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener params',$param);
        }
        $distancia_jefe = -1;
        if($param->tiene_checada){
            $fecha = trim($fecha);
            if($fecha === ''){
                return (new error())->error('Error $fecha esta vacia',$fecha);
            }
            $latitud_jefe = $param->checada->latitud;
            $longitud_jefe = $param->checada->longitud;

            $distancia_jefe = (new calculo())->calcula_distancia_con_coordenadas(
                $latitud_empleado,$longitud_empleado,$latitud_jefe,$longitud_jefe);
            if(error::$en_error){
                return (new error())->error('Error al obtener distancia',$distancia_jefe);
            }
        }

        return $distancia_jefe;

    }

    /**
     * TRASLADADO
     * Valida la distancia entre un empleado y su jefe en el registro de asistencia.
     *
     * Esta función verifica si el campo `distancia_jefe` está definido en el objeto `asistencia_app`.
     * Si no está definido, le asigna un valor por defecto de -1. Luego, valida que la distancia sea un
     * valor entre 0.0 y 200.0, ambos inclusive. Si cumple con este criterio, retorna `true`, de lo contrario,
     * retorna `false`.
     *
     * @param stdClass $asistencia_app Objeto que contiene los datos de asistencia, incluyendo la distancia entre el empleado y su jefe.
     *
     * @return bool Retorna `true` si la distancia está en el rango válido (0.0 a 200.0), de lo contrario, retorna `false`.
     */
    private function distancia_valida(stdClass $asistencia_app): bool
    {
        if(!isset($asistencia_app->distancia_jefe)){
            $asistencia_app->distancia_jefe = -1;
        }
        $distancia_valida = false;
        if((float)$asistencia_app->distancia_jefe <= 200.0){
            if((float)$asistencia_app->distancia_jefe >= 0.0){
                $distancia_valida = true;
            }
        }
        return $distancia_valida;

    }

    /**
     * TRASLADADO
     * Obtiene la información actual de los empleados a partir de un conjunto completo de asistencias.
     *
     * La función valida que el nombre de la entidad del empleado no esté vacío y recorre el array de empleados completos,
     * verificando que el ID de cada empleado sea válido y obteniendo la información actual de cada empleado desde la base de datos.
     * Si ocurre algún error durante el proceso, devuelve un array con los detalles del fallo.
     *
     * @param array $empleados_full Un array que contiene las asistencias agrupadas por empleado.
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     * @param PDO $link La conexión PDO utilizada para ejecutar las consultas.
     *
     * @return array Devuelve un array con la información actualizada de los empleados. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function empleados_actuales_asistencia(array $empleados_full, string $entidad_empleado, PDO $link): array
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }
        $empleados_actual = array();

        foreach ($empleados_full as $ohem_id=>$empleado){
            if((int)$ohem_id <= 0){
                return (new error())->error('Error $ohem_id debe ser mayor a 0',$empleados_full);
            }
            if(isset($empleados_actual[$ohem_id])){
                continue;
            }
            $empleado_database = (new consultas())->registro_bruto(
                new stdClass(),$entidad_empleado,$ohem_id,$link,false);
            if(error::$en_error){
                return (new error())->error('Error al obtener $empleado_database',$empleado_database);
            }
            $empleados_actual[$ohem_id] = $empleado_database;

        }
        return $empleados_actual;

    }

    /**
     * TRASLADADO
     * Agrupa las asistencias de un período por empleado, validando los datos y estructurando las asistencias por ID de empleado.
     *
     * La función valida que el nombre de la entidad del empleado no esté vacío, y recorre la lista de asistencias
     * para asegurarse de que cada asistencia tenga un ID de empleado válido. Las asistencias se agrupan por el ID
     * del empleado en un array asociativo. Si ocurre algún error durante el proceso, devuelve un array con los detalles del fallo.
     *
     * @param array $asistencias Un array de asistencias (pueden ser objetos o arrays) que deben ser agrupadas por empleado.
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     *
     * @return array Devuelve un array asociativo donde las claves son los IDs de los empleados y los valores son sus asistencias.
     *               Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function empleados_full_periodo_asistencia(array $asistencias, string $entidad_empleado): array
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacio',$entidad_empleado);
        }
        $empleados_full = array();
        foreach ($asistencias as $asistencia){
            if(is_array($asistencia)){
                $asistencia = (object)$asistencia;
            }
            $key_empleado_id = $entidad_empleado.'_id';

            if(!isset($asistencia->$key_empleado_id)){
                return (new error())->error('Error $asistencia->'.$key_empleado_id.' no existe',$asistencia);
            }
            if(trim($asistencia->$key_empleado_id) === ''){
                return (new error())->error('Error $asistencia->'.$key_empleado_id .'esta vacia',$asistencia);
            }
            if(!is_numeric($asistencia->$key_empleado_id)){
                return (new error())->error(
                    'Error $asistencia->'.$key_empleado_id.' debe ser un numero',$asistencia);
            }
            if((int)$asistencia->$key_empleado_id <= 0){
                return (new error())->error(
                    'Error $asistencia->'.$$key_empleado_id.'  debe ser mayor a 0',$asistencia);
            }

            $empleados_full[$asistencia->$key_empleado_id][] = $asistencia;
        }
        return $empleados_full;

    }

    private function empleados_rpt(string $estructura, PDO $link, stdClass $params): array
    {
        $empleados = array();
        foreach ($params->empleados_actual as $ohem_id=>$empleado_actual){

            $empleado = $this->checada_empleado(
                $empleado_actual,$params->empleados_full, $estructura, $params->periodo_pago->fecha_final,
                $params->periodo_pago->fecha_inicial,$link,$ohem_id);
            if(error::$en_error){
                return  (new error())->error('Error al asignar checada',$empleado);
            }

            $empleados[] = $empleado;
        }
        return $empleados;


    }

    /**
     * TRASLADADO
     * Obtiene el esquema más reciente de un empleado antes de una fecha límite específica, validando los datos y ejecutando una consulta SQL.
     *
     * La función primero valida los datos de entrada, incluidos el nombre del campo del esquema, la entidad del empleado,
     * la entidad relacionada, la fecha límite y el ID del empleado. Luego genera y ejecuta una consulta SQL para obtener
     * el esquema más reciente del empleado antes de la fecha límite. Si el esquema no existe o hay errores, devuelve un
     * array con los detalles del fallo.
     *
     * @param string $campo_esquema_name El nombre del campo del esquema que se desea obtener.
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     * @param string $entidad_rel_esquema El nombre de la entidad relacionada con el esquema.
     * @param string $fecha_limite La fecha límite hasta la cual se desea obtener el esquema del empleado (en formato `YYYY-MM-DD`).
     * @param PDO $link La conexión PDO utilizada para ejecutar la consulta.
     * @param int $ohem_id El ID del empleado, que debe ser mayor a 0.
     *
     * @return array Devuelve un array con el esquema más reciente del empleado. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function esquema_empleado_fecha(
        string $campo_esquema_name, string $entidad_empleado, string $entidad_rel_esquema, string $fecha_limite,
        PDO $link, int $ohem_id)
    {
        $valida = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->valida_data_asistencia_reg(
            $campo_esquema_name,$entidad_empleado,$entidad_rel_esquema ,$fecha_limite,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error validar datos', $valida);
        }

        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->empleado_esquema_fecha(
            $campo_esquema_name,$entidad_empleado, $entidad_rel_esquema,$fecha_limite, $ohem_id);
        if(error::$en_error){
            return  (new error())->error('Error al obtener $sql',$sql);
        }
        $esquema_rs = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return  (new error())->error('Error al obtener $esquema',$esquema_rs);
        }

        if((int)$esquema_rs['n_registros'] === 0){
            return  (new error())->error('Error no existe esquema',$esquema_rs);
        }

        return $esquema_rs['registros'][0];

    }

    /**
     * TRASLADADO
     * Obtiene el historial de esquemas de un empleado en base a las fechas de las primeras y últimas checadas.
     *
     * Esta función valida los datos proporcionados, como la entidad del empleado, la entidad de relación del esquema,
     * y las fechas de la primera y última checada. Luego obtiene el esquema correspondiente al empleado para cada fecha
     * relevante, incluyendo el esquema actual.
     *
     * @param string $campo_esquema_name El nombre del campo del esquema que se va a consultar.
     * @param string $entidad_empleado El nombre de la entidad del empleado.
     * @param string $entidad_rel_esquema El nombre de la entidad de relación del esquema.
     * @param PDO $link La conexión PDO a la base de datos.
     * @param stdClass $primer_checada Un objeto que contiene los datos de la primera checada, incluyendo la fecha.
     * @param stdClass $ultima_checada Un objeto que contiene los datos de la última checada, incluyendo la fecha.
     *
     * @return stdClass|array Un objeto que contiene los esquemas para la primera checada, la última checada, y el esquema actual.
     *
     * @throws error Si ocurre un error en la validación de datos o durante la obtención de los esquemas.
     */
    private function esquemas_historia(
        string $campo_esquema_name, string $entidad_empleado, string $entidad_rel_esquema, PDO $link,
        stdClass $primer_checada, stdClass $ultima_checada)
    {

        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacio', $entidad_empleado);
        }
        $entidad_rel_esquema = trim($entidad_rel_esquema);
        if($entidad_rel_esquema === ''){
            return (new error())->error('Error $entidad_rel_esquema esta vacio', $entidad_rel_esquema);
        }

        if(!isset($primer_checada->fecha)){
            return  (new error())->error('Error $primer_checada->fecha no existe',$primer_checada);
        }
        $primer_checada->fecha = trim($primer_checada->fecha);
        if($primer_checada->fecha === ''){
            return (new error())->error('Error $primer_checada->fecha esta vacio', $primer_checada->fecha);
        }

        if(!isset($ultima_checada->fecha)){
            return  (new error())->error('Error $ultima_checada->fecha no existe',$ultima_checada);
        }
        $ultima_checada->fecha = trim($ultima_checada->fecha);
        if($ultima_checada->fecha === ''){
            return (new error())->error('Error $ultima_checada->fecha esta vacio', $ultima_checada->fecha);
        }

        $key_empleado_id = $entidad_empleado.'_id';
        if(!isset($primer_checada->$key_empleado_id)){
            return  (new error())->error('Error $primer_checada->'.$key_empleado_id.' no existe',
                $primer_checada);
        }
        if(!is_numeric($primer_checada->$key_empleado_id)){
            return  (new error())->error('Error $primer_checada->'.$key_empleado_id.' debe ser un numero',
                $primer_checada);
        }
        if((int)$primer_checada->$key_empleado_id <= 0){
            return  (new error())->error('Error $primer_checada->'.$key_empleado_id.' debe ser mayor a 0',
                $primer_checada);
        }
        if(!isset($ultima_checada->$key_empleado_id)){
            return  (new error())->error('Error $ultima_checada->'.$key_empleado_id.' no existe',
                $ultima_checada);
        }
        if(!is_numeric($ultima_checada->$key_empleado_id)){
            return  (new error())->error('Error $ultima_checada->'.$key_empleado_id.' debe ser un numero',
                $ultima_checada);
        }
        if((int)$ultima_checada->$key_empleado_id <= 0){
            return  (new error())->error('Error $ultima_checada->'.$key_empleado_id.' debe ser mayor a 0',
                $ultima_checada);
        }

        $campo_esquema_name = trim($campo_esquema_name);
        if($campo_esquema_name === ''){
            return (new error())->error('Error $campo_esquema_name esta vacio', $campo_esquema_name);
        }


        $esquema_primer_checada = $this->esquema_empleado_fecha(
            $campo_esquema_name,$entidad_empleado, $entidad_rel_esquema,$primer_checada->fecha,$link,
            $primer_checada->$key_empleado_id);
        if(error::$en_error){
            return  (new error())->error('Error al obtener $esquema',$esquema_primer_checada);
        }

        $esquema_ultima_checada = $this->esquema_empleado_fecha($campo_esquema_name,$entidad_empleado,
            $entidad_rel_esquema,$ultima_checada->fecha,$link, $ultima_checada->$key_empleado_id);
        if(error::$en_error){
            return  (new error())->error('Error al obtener $esquema',$esquema_ultima_checada);
        }

        $esquema_actual = $this->esquema_empleado_fecha($campo_esquema_name,$entidad_empleado,
            $entidad_rel_esquema,'9999-12-31',$link,$primer_checada->$key_empleado_id);
        if(error::$en_error){
            return  (new error())->error('Error al obtener $esquema',$esquema_actual);
        }

        $esquemas = new stdClass();
        $esquemas->esquema_primer_checada = $esquema_primer_checada;
        $esquemas->esquema_ultima_checada = $esquema_ultima_checada;
        $esquemas->esquema_actual = $esquema_actual;

        return $esquemas;

    }

    /**
     * TRASLADADO
     * Actualiza el día en la tabla de asistencia con base en la fecha proporcionada.
     *
     * La función valida la existencia y formato de la fecha y el ID de asistencia. Luego, obtiene el día en español
     * correspondiente a la fecha y actualiza la tabla de asistencia con dicho día. Si ocurre algún error en el proceso,
     * se devuelve un array con los detalles del error.
     *
     * @param stdClass $asistencia_app Un objeto que contiene los datos de la asistencia, incluyendo `fecha` y `id`.
     * @param PDO $link La conexión PDO utilizada para ejecutar las consultas.
     *
     * @return array Devuelve el resultado de la ejecución de la consulta SQL o un array de error si se produce algún fallo.
     */
    private function exe_dia(stdClass $asistencia_app, PDO $link): array
    {
        $valida = $this->valida_asistencia($asistencia_app);
        if(error::$en_error){
            return (new error())->error('Error al validar asistencia',$valida);
        }

        $dia =$this->dia_espaniol($asistencia_app->fecha);
        if(error::$en_error){
            return (new error())->error('Error al actualizar obtener dia',$dia);
        }

        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->upd_dia($asistencia_app->id,$dia);
        if(error::$en_error){
            return (new error())->error('Error al actualizar obtener sql',$sql);
        }

        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al actualizar ajustar asistencia',$exe);
        }

        return $exe;

    }

    /**
     * TRASLADADO
     * Obtiene la checada de asistencia de un empleado en una fecha específica desde la base de datos.
     *
     * Esta función valida los parámetros de asistencia, incluyendo la entidad del empleado, la fecha, y el ID del
     * empleado. Luego, genera una consulta SQL que busca el registro de asistencia en la tabla `asistencia_app`.
     * La consulta se ejecuta utilizando una conexión PDO. Si se encuentra un registro, se convierte en un objeto
     * `stdClass`. Si no se encuentran valores para las coordenadas (`latitud` y `longitud`), se les asigna un valor
     * predeterminado de `-9999`.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param string $fecha Fecha en la que se busca la checada de asistencia.
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $ohem_id ID del empleado en la entidad de empleados.
     *
     * @return stdClass|array Devuelve un objeto `stdClass` con la checada de asistencia o un array de error si ocurre algún problema.
     */
    private function get_checada(string $entidad_empleado, string $fecha, PDO $link, int $ohem_id)
    {

        $valida = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->valida_asistencia(
            $entidad_empleado,$fecha,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al validar asistencia',$valida);
        }

        $checada = new stdClass();
        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->get_checada($entidad_empleado, $fecha,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener sql',$sql);
        }
        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al ejecutar sql',$exe);
        }
        if((int)$exe['n_registros'] > 0){
            $checada = (object)$exe['registros'][0];
        }
        if(!isset($checada->latitud)){
            $checada->latitud = -9999;
        }
        if(!isset($checada->longitud)){
            $checada->longitud = -9999;
        }
        if(trim($checada->longitud) === ''){
            $checada->longitud = -9999;
        }
        if(trim($checada->latitud) === ''){
            $checada->latitud = -9999;
        }
        return $checada;

    }

    /**
     * TRASLADADO
     * Obtiene un array de los días en un periodo especificado por dos fechas.
     *
     * Esta función valida las fechas inicial y final, calcula la cantidad de días en el periodo
     * y genera un array asociativo donde las claves son las fechas en formato `Y-m-d` y los valores
     * son los nombres de los días de la semana en español. Si ocurre un error en cualquier etapa del
     * proceso, se retorna un array con los detalles del error.
     *
     * @param string $fecha_final La fecha final del periodo. No debe estar vacía.
     * @param string $fecha_inicial La fecha inicial del periodo. No debe estar vacía.
     *
     * @return array Retorna un array asociativo con las fechas como claves y los nombres de los días
     *               de la semana en español como valores. Si ocurre un error, retorna un array con
     *               los detalles del error.
     *
     *
     * @access private
     */
    private function get_dias_periodo(string $fecha_final, string $fecha_inicial): array
    {
        $fecha_final = trim($fecha_final);
        if($fecha_final === ''){
            return (new error())->error('Error $fecha_final esta vacia',$fecha_final);
        }
        $fecha_inicial = trim($fecha_inicial);
        if($fecha_inicial === ''){
            return (new error())->error('Error $fecha_inicial esta vacia',$fecha_inicial);
        }
        $dias_periodo = $this->dias_periodo($fecha_final,$fecha_inicial);
        if(error::$en_error){
            return (new error())->error('Error al obtener $dias',$dias_periodo);
        }

        $dia_recorrido = 0;
        $dias = array();
        while($dia_recorrido < $dias_periodo){
            $dias = $this->integra_data_dia($dia_recorrido,$dias,$fecha_inicial);
            if(error::$en_error){
                return (new error())->error('Error al obtener $dias',$dias);
            }
            $dia_recorrido++;
        }
        return $dias;

    }

    /**
     * TRASLADADO
     * Obtiene la distancia entre un empleado y su jefe inmediato basado en los datos de asistencia.
     *
     * Esta función toma los datos de un registro de asistencia de un empleado y calcula la distancia entre el empleado y su jefe inmediato.
     * Utiliza las coordenadas geográficas (latitud y longitud) de ambos para realizar el cálculo.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param PDO $link Conexión PDO a la base de datos.
     * @param array|stdClass $registro Registro de datos del empleado (puede ser un objeto o un array).
     *
     * @return float|array Devuelve la distancia en metros entre el empleado y su jefe inmediato. Si ocurre algún error, devuelve
     * un array de error con los detalles.
     */
    final public function get_distancia_jefe(string $entidad_empleado, PDO $link, $registro)
    {

        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }

        if(is_object($registro)){
            $registro = (array)$registro;
        }
        $init = $this->init_params($entidad_empleado, $registro);
        if(error::$en_error){
            return (new error())->error('Error al $init params',$init);
        }

        $distancia_jefe = $this->distancia_jefe($entidad_empleado, $init->fecha, $link,$init->latitud,$init->longitud,
            $init->ohem_jefe_inmediato_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener distancia',$distancia_jefe);
        }
        return $distancia_jefe;

    }

    /**
     * TRASLADADO
     * Obtiene un reporte completo de los empleados con sus asistencias dentro de un período de pago específico.
     *
     * La función valida que el período de pago (con fechas inicial y final) y otros parámetros requeridos no estén vacíos.
     * Luego, obtiene las asistencias de los empleados en el período de tiempo proporcionado, agrupa las asistencias por empleado,
     * y devuelve un array con los empleados y sus asistencias. Si ocurre algún error durante el proceso, devuelve un array con los detalles del fallo.
     *
     * @param string $campo_fecha_ingreso El nombre del campo de la fecha de ingreso del empleado.
     * @param string $entidad_empleado El nombre de la entidad del empleado (por ejemplo, 'empleado').
     * @param PDO $link La conexión PDO utilizada para ejecutar las consultas.
     * @param stdClass $periodo_pago Un objeto que contiene las propiedades `fecha_inicial` y `fecha_final` para el período de pago.
     *
     * @return array Devuelve un array con los empleados y sus asistencias agrupadas. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function get_empleados_full_rpt(
        string $campo_fecha_ingreso, string $entidad_empleado, PDO $link, stdClass $periodo_pago): array
    {
        if(!isset($periodo_pago->fecha_final)){
            return  (new error())->error('Error $periodo_pago->fecha_final no existe',$periodo_pago);
        }
        if(!isset($periodo_pago->fecha_inicial)){
            return  (new error())->error('Error $periodo_pago->fecha_inicial no existe',$periodo_pago);
        }
        $periodo_pago->fecha_inicial = trim($periodo_pago->fecha_inicial);
        if($periodo_pago->fecha_inicial === ''){
            return (new error())->error('Error $periodo_pago->fecha_inicial esta vacia',$periodo_pago);
        }
        $periodo_pago->fecha_final = trim($periodo_pago->fecha_final);
        if($periodo_pago->fecha_final === ''){
            return (new error())->error('Error $periodo_pago->fecha_final esta vacia',$periodo_pago);
        }

        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }
        $campo_fecha_ingreso = trim($campo_fecha_ingreso);
        if($campo_fecha_ingreso === ''){
            return (new error())->error('Error $campo_fecha_ingreso esta vacia', $campo_fecha_ingreso);
        }
        $asistencias = $this->asistencias_periodo($campo_fecha_ingreso,$entidad_empleado,$periodo_pago->fecha_final,
            $periodo_pago->fecha_inicial,$link);
        if(error::$en_error){
            return  (new error())->error('Error al obtener asistencias',$asistencias);
        }


        $empleados_full = $this->empleados_full_periodo_asistencia($asistencias, $entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al obtener $empleados_full',$empleados_full);
        }
        return $empleados_full;

    }

    /**
     * TRASLADADO
     * Inicializa los datos de checada para un día específico en el array de un empleado.
     *
     * Esta función recibe un día y el array del empleado, y establece valores predeterminados para los datos de checada
     * correspondientes al día. Valida que el día no esté vacío. Luego, inicializa las siguientes claves en el array del
     * empleado:
     * - `{$dia}_APP_AS`: Cadena vacía para la hora de asistencia.
     * - `{$dia}_JEFE_CHECO`: 'inactivo' para indicar si el jefe checó.
     * - `{$dia}_checador`: 'inactivo' para el estado del checador.
     * - `{$dia}_distancia_jefe`: '-1' para la distancia con el jefe.
     * - `{$dia}_aplica_pago`: 'inactivo' para el estado de pago aplicable.
     *
     * @param string $dia El día para el cual se inicializan los datos de checada. No debe estar vacío.
     * @param array $empleado El array del empleado que se actualizará con los datos de checada inicializados.
     *
     * @return array Retorna el array del empleado con los datos de checada inicializados.
     *                     Si ocurre un error, retorna un array de error con los detalles del fallo.
     */
    private function init_data_checada(string $dia, array $empleado): array
    {
        $dia = trim($dia);
        if($dia === ''){
            return  (new error())->error('Error $dia esta vacio',$dia);
        }
        $empleado["$dia"."_APP_AS"] = '';
        $empleado["$dia"."_JEFE_CHECO"] = 'inactivo';
        $empleado["$dia"."_checador"] = 'inactivo';
        $empleado["$dia"."_distancia_jefe"] = '-1';
        $empleado["$dia"."_distancia_jefe"] = '-1';
        $empleado["$dia"."_aplica_pago"] = 'inactivo';

        return $empleado;

    }


    /**
     * TRASLADADO
     * Inicializa los parámetros necesarios a partir del registro de un empleado.
     *
     * Esta función toma el nombre de la entidad del empleado y un registro asociado (puede ser un array o un objeto).
     * Si el registro es un objeto, lo convierte en un array. Luego, extrae los valores de latitud, longitud,
     * ID del jefe inmediato y la fecha del registro, asignando valores predeterminados si alguno de ellos no está presente.
     * Los datos extraídos se agrupan en un objeto `stdClass` que se devuelve como resultado.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param array|stdClass $registro Registro asociado al empleado, puede ser un array o un objeto.
     *
     * @return stdClass|array Devuelve un objeto `stdClass` con los parámetros inicializados o un array de error si ocurre algún problema.
     */
    private function init_params(string $entidad_empleado, $registro)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        if(is_object($registro)){
            $registro = (array)$registro;
        }
        $latitud = -1;
        if(isset($registro['latitud'])){
            $latitud = $registro['latitud'];
        }
        $longitud = -1;
        if(isset($registro['longitud'])){
            $longitud = $registro['longitud'];
        }

        $key_jefe_inmediato_id = $entidad_empleado.'_jefe_inmediato_id';
        $ohem_jefe_inmediato_id = -1;
        if(isset($registro[$key_jefe_inmediato_id])){
            $ohem_jefe_inmediato_id = $registro[$key_jefe_inmediato_id];
        }
        $fecha = '';
        if(isset($registro['fecha'])){
            $fecha = $registro['fecha'];
        }

        $params = new stdClass();
        $params->latitud = $latitud;
        $params->longitud= $longitud;
        $params->ohem_jefe_inmediato_id= $ohem_jefe_inmediato_id;
        $params->fecha= $fecha;

        return $params;

    }

    /**
     * TRASLADADO
     * Integra la checada de un día específico en el array de un empleado.
     *
     * Esta función toma un array de asistencias del empleado, el día a procesar y la fecha correspondiente,
     * y actualiza el array del empleado con los datos de asistencia del día específico. Valida que tanto el día
     * como la fecha no estén vacíos. Recorre el array de asistencias y, si encuentra una asistencia cuyo campo
     * `fecha` coincida con la fecha proporcionada, utiliza la función `checada_con_dato` para agregar los datos
     * de checada al array del empleado.
     *
     * Si los datos de asistencia no son objetos o si hay algún error en el proceso, la función retorna un array
     * de error con los detalles del fallo.
     *
     * @param array $asistencias_empleado Un array que contiene las asistencias del empleado.
     * @param string $dia El día de la asistencia que se va a procesar. No debe estar vacío.
     * @param string $dia_fecha La fecha correspondiente al día de la asistencia. No debe estar vacía.
     * @param array $empleado El array del empleado que se actualizará con los datos de checada.
     *
     * @return array Retorna el array del empleado actualizado con los datos de asistencia para el día específico.
     *                     Si ocurre un error, retorna un array de error con los detalles del fallo.
     */
    private function integra_checada_dia(
        array $asistencias_empleado, string $dia, string $dia_fecha, array $empleado): array
    {
        $dia = trim($dia);
        if($dia === ''){
            return  (new error())->error('Error $dia esta vacio',$dia);
        }
        $dia_fecha = trim($dia_fecha);
        if($dia_fecha === ''){
            return  (new error())->error('Error $dia_fecha esta vacio',$dia_fecha);
        }

        foreach ($asistencias_empleado as $asistencia_dia){
            if(!is_object($asistencia_dia)){
                return  (new error())->error('Error $asistencias_empleado datos debe ser un objeto',
                    $asistencias_empleado);
            }
            if(!isset($asistencia_dia->fecha)){
                $asistencia_dia->fecha = '';
            }
            if($asistencia_dia->fecha === $dia_fecha){
                $empleado = $this->checada_con_dato($asistencia_dia,$dia,$empleado);
                if(error::$en_error){
                    return  (new error())->error('Error al $empleado',$empleado);
                }
                break;
            }
        }
        return $empleado;

    }

    private function integra_checada_dia_completa(array $asistencias_empleado, string $dia, string $dia_fecha, array $empleado): array
    {

        $empleado = $this->integra_checada_dia($asistencias_empleado,$dia,$dia_fecha,$empleado);
        if(error::$en_error){
            return  (new error())->error('Error al integrar checada',$empleado);
        }

        $tiene_checada = $this->tiene_checada($asistencias_empleado,$dia_fecha);
        if(error::$en_error){
            return  (new error())->error('Error al validar si hay checada',$tiene_checada);
        }
        if(!$tiene_checada){
            $empleado = $this->init_data_checada($dia,$empleado);
            if(error::$en_error){
                return  (new error())->error('Error al $empleado',$empleado);
            }

        }
        return $empleado;

    }


    private function integra_checada_sitio(string $entidad_empleado, string $fecha, PDO $link, int $ohem_id)
    {
        $tiene_checada_app = $this->tiene_checada_app($entidad_empleado,$fecha,$link,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al validar si tiene checada',$tiene_checada_app);
        }
        if($tiene_checada_app){
            $actualiza_checada = $this->actualiza_en_sitio($entidad_empleado,$fecha,$link,$ohem_id);
            if(error::$en_error){
                return (new error())->error('Error al actualizar tiene checada',$actualiza_checada);
            }
        }
        return $tiene_checada_app;

    }

    /**
     * TRASLADADO
     * Integra datos de un día específico en un array de días, basándose en una fecha inicial.
     *
     * Esta función calcula una fecha sumando un número de días a una fecha inicial y determina
     * el nombre del día de la semana en español para esa fecha. Luego, integra esta información
     * en el array de días proporcionado. Si ocurre un error durante el proceso, se retorna un
     * array con los detalles del error.
     *
     * @param int $dia_recorrido El número de días que se deben sumar a la fecha inicial.
     * @param array $dias Array asociativo donde la clave es la fecha (formato `Y-m-d`)
     *                    y el valor es el nombre del día de la semana en español.
     * @param string $fecha_inicial La fecha inicial a partir de la cual se suman los días. No debe estar vacía.
     *
     * @return array Retorna el array `$dias` actualizado con la fecha calculada y el nombre del día en español.
     *               Si ocurre un error, retorna un array con los detalles del error.
     *
     *
     * @access private
     */
    private function integra_data_dia(int $dia_recorrido, array $dias, string $fecha_inicial): array
    {
        $fecha_inicial = trim($fecha_inicial);
        if($fecha_inicial === ''){
            return (new error())->error('Error $fecha_inicial esta vacia',$fecha_inicial);
        }
        try {
            $fecha_r = new DateTime($fecha_inicial);
            $fecha_r->modify("+$dia_recorrido days");

            $fecha_r = $fecha_r->format('Y-m-d');
        }
        catch (Throwable $e){
            return (new error())->error('Error al ajustar $fecha_inicial',$e);
        }
        $dia_r = $this->dia_espaniol($fecha_r);
        if(error::$en_error){
            return (new error())->error('Error al obtener $dia_r',$dia_r);
        }

        $dias[$fecha_r] = $dia_r;

        return $dias;

    }

    /**
     * TRASLADADO
     * Integra los días de un periodo en un array de empleado.
     *
     * Esta función toma un array que representa un empleado, calcula los días dentro de un periodo especificado
     * por las fechas inicial y final, y agrega al array del empleado las fechas del periodo asociadas con el
     * nombre del día de la semana en español como clave. Si ocurre un error durante el cálculo de los días
     * o las validaciones, se retorna un array con los detalles del error.
     *
     * @param array $empleado Array que representa al empleado al que se le integrarán los días.
     * @param string $fecha_final Fecha que marca el final del periodo. No debe estar vacía.
     * @param string $fecha_inicial Fecha que marca el inicio del periodo. No debe estar vacía.
     *
     * @return array Retorna el array del empleado actualizado con los días del periodo.
     *               Si ocurre un error, retorna un array con los detalles del error.
     *
     * @access private
     */
    private function integra_dias_checada(array $empleado, string $fecha_final, string $fecha_inicial): array
    {
        $fecha_final = trim($fecha_final);
        if($fecha_final === ''){
            return (new error())->error('Error $fecha_final esta vacia',$fecha_final);
        }
        $fecha_inicial = trim($fecha_inicial);
        if($fecha_inicial === ''){
            return (new error())->error('Error $fecha_inicial esta vacia',$fecha_inicial);
        }
        $dias = $this->get_dias_periodo($fecha_final,$fecha_inicial);
        if(error::$en_error){
            return  (new error())->error('Error al obtener $dias',$dias);
        }

        foreach ($dias as $dia_fecha=>$dia){
            $empleado[$dia] = $dia_fecha;
        }
        return $empleado;

    }

    private function integra_dias_checada_empleado(array $asistencias_empleado, array $empleado, string $fecha_final, string $fecha_inicial): array
    {
        $dias = $this->get_dias_periodo($fecha_final,$fecha_inicial);
        if(error::$en_error){
            return  (new error())->error('Error al obtener $dias',$dias);
        }
        foreach ($dias as $dia_fecha=>$dia){
            $empleado = $this->integra_checada_dia_completa($asistencias_empleado,$dia,$dia_fecha,$empleado);
            if(error::$en_error){
                return  (new error())->error('Error al integrar $empleado',$empleado);
            }
        }
        return $empleado;

    }

    /**
     * TRASLADADO
     * Obtiene y valida los parámetros de una checada de asistencia para un empleado en una fecha específica.
     *
     * Esta función valida los datos de asistencia (entidad del empleado, fecha y ID del empleado) y luego intenta
     * obtener la checada correspondiente desde la base de datos. Si la checada contiene valores válidos para las
     * coordenadas (`latitud` y `longitud`), se considera que existe una checada válida. Si alguna de las coordenadas
     * es `-9999`, se descarta la checada y se considera que no hay una checada válida.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param string $fecha Fecha en la que se busca la checada de asistencia.
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $ohem_id ID del empleado en la entidad de empleados.
     *
     * @return stdClass|array Devuelve un objeto `stdClass` que contiene dos propiedades:
     * - `tiene_checada` (bool): Indica si la checada es válida.
     * - `checada` (stdClass): Objeto que contiene los datos de la checada. Si no hay una checada válida, este objeto está vacío.
     * Devuelve un array de error si ocurre algún problema durante el proceso.
     */
    private function params_checada(string $entidad_empleado, string $fecha, PDO $link, int $ohem_id)
    {
        $valida = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->valida_asistencia(
            $entidad_empleado,$fecha,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al validar asistencia',$valida);
        }

        $tiene_checada = false;
        $checada = $this->get_checada($entidad_empleado, $fecha,$link,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al ejecutar sql',$checada);
        }
        if(!empty($checada)){
            $tiene_checada = true;
            if($checada->latitud === -9999 || $checada->longitud === -9999){
                $tiene_checada = false;
                $checada = new stdClass();
            }
        }

        $data = new stdClass();
        $data->tiene_checada = $tiene_checada;
        $data->checada = $checada;

        return $data;

    }

    /**
     * TRASLADADO
     * Obtiene los parámetros de la checada del jefe inmediato de un empleado en una fecha específica.
     *
     * Esta función valida que el nombre de la entidad del empleado no esté vacío y que tanto la fecha como el
     * ID del jefe inmediato sean válidos. Si estos valores son correctos, se llama a la función `params_checada`
     * para obtener los parámetros de la checada del jefe inmediato. Si no se cumple alguna de las condiciones
     * necesarias, se retorna un objeto vacío indicando que no se encontró una checada válida.
     *
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param string $fecha Fecha en la que se busca la checada del jefe inmediato.
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $ohem_jefe_inmediato_id ID del jefe inmediato del empleado en la entidad de empleados.
     *
     * @return stdClass|array Devuelve un objeto `stdClass` con los siguientes campos:
     * - `tiene_checada` (bool): Indica si se encontró una checada válida.
     * - `checada` (stdClass): Objeto que contiene los datos de la checada del jefe inmediato.
     * En caso de error, devuelve un array de error con los detalles.
     */
    private function params_checada_jefe(string $entidad_empleado, string $fecha, PDO $link,
                                         int $ohem_jefe_inmediato_id)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $param = new stdClass();
        $param->checada = new stdClass();
        $param->tiene_checada = false;
        if($fecha !== '' && $ohem_jefe_inmediato_id > 0){
            $param = $this->params_checada($entidad_empleado, $fecha,$link,$ohem_jefe_inmediato_id);
            if(error::$en_error){
                return (new error())->error('Error al obtener params',$param);
            }
        }
        return $param;

    }

    /**
     * TRASLADADO
     * Obtiene los parámetros de distancia para un registro de asistencia.
     *
     * Esta función toma el ID de una asistencia, la entidad del empleado y realiza el cálculo de la distancia
     * entre el empleado y su jefe inmediato. Devuelve un objeto con el registro de asistencia y la distancia calculada.
     *
     * @param int $asistencia_app_id ID del registro de asistencia en la aplicación.
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param PDO $link Conexión PDO a la base de datos.
     *
     * @return stdClass|array Devuelve un objeto con el registro de asistencia y la distancia calculada entre el empleado y su jefe inmediato.
     * Si ocurre algún error, devuelve un array con los detalles del error.
     */
    private function params_distancia(int $asistencia_app_id, string $entidad_empleado, PDO $link)
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id dbe ser mayor a 0',$asistencia_app_id);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }

        $asistencia_row = (new consultas())->registro_bruto(new stdClass(),'asistencia_app',
            $asistencia_app_id,$link);
        if(error::$en_error){
            return (new error())->error('Error al obtener asistencia_app',$asistencia_row);
        }

        $distancia_jefe = $this->get_distancia_jefe($entidad_empleado, $link, $asistencia_row);
        if(error::$en_error){
            return (new error())->error('Error al obtener distancia',$distancia_jefe);
        }

        $data = new stdClass();
        $data->asistencia_row = $asistencia_row;
        $data->distancia_jefe = $distancia_jefe;

        return $data;

    }


    /**
     * TRASLADADO
     * Genera los parámetros necesarios para un reporte de asistencia.
     *
     * Esta función valida las entradas necesarias, obtiene información relacionada con el periodo de pago,
     * empleados completos y empleados actuales, y construye un objeto con estos datos para ser utilizado en
     * la generación de reportes de asistencia. Si alguna validación o proceso falla, se retorna un array con
     * los detalles del error.
     *
     * @param string $campo_fecha_ingreso El nombre del campo que representa la fecha de ingreso de los empleados.
     * @param string $entidad_empleado El nombre de la entidad asociada a los empleados. No debe estar vacío.
     * @param string $fecha_final La fecha final del periodo de reporte.
     * @param string $fecha_inicial La fecha inicial del periodo de reporte.
     * @param PDO $link Conexión PDO para realizar consultas a la base de datos.
     * @param int $periodo_pago_id (Opcional) Identificador del periodo de pago. Si es mayor a 0, se obtiene el periodo desde la base de datos.
     *
     * @return stdClass|array Retorna un objeto con las propiedades:
     *                        - `periodo_pago`: Información del periodo de pago.
     *                        - `empleados_full`: Lista de empleados completa basada en el periodo.
     *                        - `empleados_actual`: Lista de empleados actuales para el reporte.
     *                        Si ocurre un error, retorna un array con los detalles del error.
     *
     *
     * @access private
     */
    private function parametros_reporte_asistencia(string $campo_fecha_ingreso, string $entidad_empleado,
                                                   string $fecha_final, string $fecha_inicial, PDO $link,
                                                   int $periodo_pago_id)
    {

        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia', $entidad_empleado);
        }
        $campo_fecha_ingreso = trim($campo_fecha_ingreso);
        if($campo_fecha_ingreso === ''){
            return (new error())->error('Error $campo_fecha_ingreso esta vacia', $campo_fecha_ingreso);
        }

        $periodo_pago = $this->periodo_pago($fecha_final,$fecha_inicial,$link,$periodo_pago_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $periodo_pago',$periodo_pago);
        }

        $empleados_full = $this->get_empleados_full_rpt($campo_fecha_ingreso,$entidad_empleado,$link,$periodo_pago);
        if(error::$en_error){
            return (new error())->error('Error al obtener $empleados_full',$empleados_full);
        }

        $empleados_actual = $this->empleados_actuales_asistencia($empleados_full,$entidad_empleado,$link);
        if(error::$en_error){
            return (new error())->error('Error al obtener $empleados_actual',$empleados_actual);
        }
        $params = new stdClass();
        $params->periodo_pago = $periodo_pago;
        $params->empleados_full = $empleados_full;
        $params->empleados_actual = $empleados_actual;

        return $params;

    }

    /**
     * TRASLADADO
     * Genera los parámetros para el reporte, incluyendo las fechas de primera y última checada y los esquemas asociados.
     *
     * Esta función valida los datos de los empleados y las checadas proporcionados. Luego, obtiene el esquema correspondiente
     * para cada checada, generando los parámetros necesarios para el reporte, tales como la primera checada, la última checada
     * y los esquemas históricos asociados.
     *
     * @param string $campo_esquema_name El nombre del campo de esquema a consultar.
     * @param array $empleados_full Un array que contiene la información completa de los empleados, indexado por el ID del empleado.
     * @param string $entidad_empleado El nombre de la entidad del empleado.
     * @param string $entidad_rel_esquema El nombre de la entidad de relación del esquema.
     * @param PDO $link La conexión PDO a la base de datos.
     * @param int $ohem_id El ID del empleado.
     *
     * @return stdClass|array Un objeto que contiene los parámetros del reporte, incluyendo la primera checada,
     * la última checada y los esquemas.
     *
     * @throws error Si ocurre un error en la validación de datos o durante la obtención de los esquemas.
     */
    private function parametros_rpt(
        string $campo_esquema_name, array $empleados_full, string $entidad_empleado, $entidad_rel_esquema, PDO $link,
        int $ohem_id)
    {
        if($ohem_id <= 0){
            return  (new error())->error('Error $ohem_id debe ser mayor a 0',$ohem_id);
        }
        if(!isset($empleados_full[$ohem_id])){
            return  (new error())->error('Error $empleados_full[$ohem_id] no existe',$empleados_full);
        }
        if(!is_array($empleados_full[$ohem_id])){
            return  (new error())->error('Error $empleados_full[$ohem_id] debe ser un array',$empleados_full);
        }
        if(!isset($empleados_full[$ohem_id][0])){
            return  (new error())->error('Error $empleados_full[$ohem_id][0] no existe',$empleados_full);
        }
        if(!is_object($empleados_full[$ohem_id][0])){
            return  (new error())->error('Error $empleados_full[$ohem_id][0] dede ser un objeto',
                $empleados_full);
        }
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacio', $entidad_empleado);
        }
        $entidad_rel_esquema = trim($entidad_rel_esquema);
        if($entidad_rel_esquema === ''){
            return (new error())->error('Error $entidad_rel_esquema esta vacio', $entidad_rel_esquema);
        }

        if(!isset($empleados_full[$ohem_id][0]->fecha)){
            return  (new error())->error('Error $empleados_full[$ohem_id][0]->fecha no existe',$empleados_full);
        }
        $empleados_full[$ohem_id][0]->fecha = trim($empleados_full[$ohem_id][0]->fecha);
        if($empleados_full[$ohem_id][0]->fecha === ''){
            return (new error())->error('Error $empleados_full[$ohem_id][0]->fecha esta vacio', $empleados_full);
        }
        $campo_esquema_name = trim($campo_esquema_name);
        if($campo_esquema_name === ''){
            return (new error())->error('Error $campo_esquema_name esta vacio', $campo_esquema_name);
        }

        $ultima_checada = end($empleados_full[$ohem_id]);
        $primer_checada = $empleados_full[$ohem_id][0];

        $esquemas = $this->esquemas_historia(
            $campo_esquema_name,$entidad_empleado, $entidad_rel_esquema,$link,$primer_checada,$ultima_checada);
        if(error::$en_error){
            return  (new error())->error('Error al obtener $esquemas',$esquemas);
        }

        $params = new stdClass();
        $params->ultima_checada = $ultima_checada;
        $params->primer_checada = $primer_checada;
        $params->esquemas = $esquemas;

        return $params;

    }

    /**
     * TRASLADADO
     * Obtiene y valida el periodo de pago con base en fechas y un identificador.
     *
     * Esta función toma las fechas inicial y final de un periodo de pago, así como un identificador opcional
     * del periodo, y devuelve un objeto con los datos del periodo. Si se proporciona un identificador válido
     * (`$periodo_pago_id` mayor a 0), intenta obtener el registro correspondiente desde la base de datos.
     * Valida que las fechas inicial y final no estén vacías. Si ocurre algún error, retorna un array con
     * los detalles del error.
     *
     * @param string $fecha_final La fecha final del periodo de pago. No debe estar vacía.
     * @param string $fecha_inicial La fecha inicial del periodo de pago. No debe estar vacía.
     * @param PDO $link Conexión PDO para realizar consultas a la base de datos.
     * @param int $periodo_pago_id (Opcional) Identificador del periodo de pago. Si es mayor a 0, se busca el registro correspondiente en la base de datos.
     *
     * @return stdClass|array Retorna un objeto `stdClass` con los datos del periodo de pago si el proceso es exitoso.
     *                        Si ocurre un error, retorna un array con los detalles del error.
     *
     * @throws desarrollo_em3\error\error Si las fechas están vacías o si ocurre un error al obtener el periodo de pago desde la base de datos.
     *
     * @access private
     */
    private function periodo_pago(string $fecha_final, string $fecha_inicial, PDO $link, int $periodo_pago_id)
    {
        $periodo_pago = new stdClass();
        $periodo_pago->fecha_final = trim($fecha_final);
        $periodo_pago->fecha_inicial = trim($fecha_inicial);
        if($periodo_pago_id > 0) {
            $periodo_pago = (new consultas())->registro_bruto(
                new stdClass(), 'periodo_pago', $periodo_pago_id, $link, false);
            if (error::$en_error) {
                return (new error())->error('Error al obtener periodo de pago', $periodo_pago);
            }
        }
        if($periodo_pago->fecha_final === ''){
            return (new error())->error('Error $periodo_pago->fecha_final esta vacia', $periodo_pago);
        }
        if($periodo_pago->fecha_inicial === ''){
            return (new error())->error('Error $periodo_pago->fecha_inicial esta vacia', $periodo_pago);
        }
        return $periodo_pago;

    }

    final public function reporte_asistencia(string $campo_fecha_ingreso, string $entidad_empleado,
                                             string $estructura, string $fecha_inicial, string $fecha_final, PDO $link,
                                             int $periodo_pago_id ): array
    {
        $params = $this->parametros_reporte_asistencia($campo_fecha_ingreso,$entidad_empleado, $fecha_final,
            $fecha_inicial,$link,$periodo_pago_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $params',$params);
        }

        $empleados = $this->empleados_rpt( $estructura, $link,$params);
        if(error::$en_error){
            return  (new error())->error('Error al asignar checada',$empleados);
        }
        return $empleados;

    }

    /**
     * TRASLADADO
     * Genera un array con los datos de checada de un empleado para un reporte, basándose en la estructura proporcionada.
     *
     * Esta función valida el ID del empleado (`ohem_id`) y verifica que exista y sea un array en el conjunto de empleados completos (`empleados_full`).
     * Luego, dependiendo de la estructura especificada (SAP o EM3), extrae los datos correspondientes del empleado actual y utiliza la función
     * `parametros_rpt` para obtener los datos relacionados con la última y primera checada, así como los esquemas de empleo. Si la estructura es SAP,
     * extrae el `empID` y la `startDate`; si es EM3, extrae la `fecha_ingreso`.
     *
     * Además, si el empleado tiene una oficina de trabajo asignada, recupera la descripción de dicha oficina.
     *
     * Si ocurre algún error en el proceso, la función retorna un array de error con los detalles del fallo.
     *
     * @param stdClass $empleado_actual El objeto que contiene los datos del empleado actual.
     * @param array $empleados_full Un array que contiene los datos completos de los empleados.
     * @param string $estructura La estructura de la que se está extrayendo la información (SAP o EM3).
     * @param PDO $link La conexión PDO a la base de datos.
     * @param int $ohem_id El ID del empleado que se está procesando. Debe ser mayor a 0.
     *
     * @return array|array Retorna un array con los datos del empleado y sus checadas si el proceso es exitoso.
     *                     Si ocurre un error, retorna un array con los detalles del fallo.
     */
    private function row_checada_rpt(stdClass $empleado_actual, array $empleados_full, string $estructura, PDO $link,
                                     int $ohem_id): array
    {
        if($ohem_id <= 0){
            return  (new error())->error('Error $ohem_id debe ser mayor a 0',$ohem_id);
        }
        if(!isset($empleados_full[$ohem_id])){
            return  (new error())->error('Error $empleados_full[$ohem_id] no existe',$empleados_full);
        }
        if(!is_array($empleados_full[$ohem_id])){
            return  (new error())->error('Error $empleados_full[$ohem_id] debe ser un array',$empleados_full);
        }
        if(!isset($empleados_full[$ohem_id][0])){
            return  (new error())->error('Error $empleados_full[$ohem_id][0] no existe',$empleados_full);
        }
        if(!is_object($empleados_full[$ohem_id][0])){
            return  (new error())->error('Error $empleados_full[$ohem_id][0] dede ser un objeto',
                $empleados_full);
        }
        $estructura = strtoupper(trim($estructura));
        if($estructura === ''){
            return  (new error())->error('Error $estructura esta vacia',$estructura);
        }
        $estructuras_valida = array('SAP','EM3');
        if(!in_array($estructura,$estructuras_valida)){
            return  (new error())->error('Error $estructura invalida',$estructuras_valida);
        }

        if(!isset($empleados_full[$ohem_id][0]->fecha)){
            return  (new error())->error('Error $empleados_full[$ohem_id][0]->fecha no existe',
                $empleados_full);
        }
        $empleados_full[$ohem_id][0]->fecha = trim($empleados_full[$ohem_id][0]->fecha);
        if($empleados_full[$ohem_id][0]->fecha === ''){
            return (new error())->error('Error $empleados_full[$ohem_id][0]->fecha esta vacio',
                $empleados_full);
        }

        $oficina_trabajo_id = -1;
        if(isset( $empleado_actual->oficina_trabajo_id) && (int)$empleado_actual->oficina_trabajo_id > 0 ){
            $oficina_trabajo_id = $empleado_actual->oficina_trabajo_id;
        }

        $empleado = array();


        if($estructura === 'SAP') {
            if(!isset($empleado_actual->empID)){
                return  (new error())->error('Error $empleado_actual->empID debe existir',$empleado_actual);
            }
            if(!isset($empleado_actual->startDate)){
                return  (new error())->error('Error $empleado_actual->startDate debe existir',$empleado_actual);
            }
            $empleado['empID'] = $empleado_actual->empID;
            $fecha_ingreso = $empleado_actual->startDate;
            $campo_esquema_name = 'Name';
            $entidad_empleado = 'ohem';
            $entidad_rel_esquema = 'empleado_esquema';
        }
        else{
            if(!isset($empleado_actual->fecha_ingreso)){
                return  (new error())->error('Error $empleado_actual->fecha_ingreso debe existir',
                    $empleado_actual);
            }
            $fecha_ingreso = $empleado_actual->fecha_ingreso;
            $campo_esquema_name = 'nombre';
            $entidad_empleado = 'empleado';
            $entidad_rel_esquema = 'esquema_empleado';
        }

        $params = $this->parametros_rpt(
            $campo_esquema_name,$empleados_full,$entidad_empleado, $entidad_rel_esquema,$link,$ohem_id);
        if(error::$en_error){
            return  (new error())->error('Error al obtener $params',$params);
        }
        $oficina_trabajo_descripcion = '';
        if($oficina_trabajo_id > 0) {
            $r_oficina_trabajo = (new oficina_trabajo())->get_oficina_trabajo_by_id($link, $oficina_trabajo_id);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $r_oficina_trabajo', $r_oficina_trabajo);
            }
            $oficina_trabajo_descripcion = $r_oficina_trabajo[0]->oficina_trabajo_descripcion;

        }


        $empleado['ohem_id'] = $ohem_id;
        $empleado['plaza'] = $params->ultima_checada->plaza;
        $empleado['fecha_ingreso'] = $fecha_ingreso;
        $empleado['status_inicio_periodo'] = $params->primer_checada->status;
        $empleado['status_fin_periodo'] = $params->ultima_checada->status;
        $empleado['status_actual'] = $empleado_actual->status;
        $empleado['esquema_inicio_periodo'] = $params->esquemas->esquema_primer_checada['esquema_name'];
        $empleado['esquema_inicio_fecha'] = $params->esquemas->esquema_primer_checada['esquema_fecha'];
        $empleado['esquema_fin_periodo'] = $params->esquemas->esquema_ultima_checada['esquema_name'];
        $empleado['esquema_fin_fecha'] = $params->esquemas->esquema_ultima_checada['esquema_fecha'];
        $empleado['esquema_actual'] = $params->esquemas->esquema_actual['esquema_name'];
        $empleado['esquema_actual_fecha'] = $params->esquemas->esquema_actual['esquema_fecha'];
        $empleado['nombre_completo'] = $empleado_actual->nombre_completo;
        $empleado['oficina_trabajo_id'] = $oficina_trabajo_id;
        $empleado['oficina_trabajo_descripcion'] = $oficina_trabajo_descripcion;

        return $empleado;


    }

    private function row_checada_rpt_detalle(array $empleado, array $empleados_full, string $fecha_final, string $fecha_inicial, int $ohem_id): array
    {
        $empleado = $this->integra_dias_checada($empleado,$fecha_final,$fecha_inicial);
        if(error::$en_error){
            return  (new error())->error('Error al asignar $dias',$empleado);
        }

        $empleado = $this->integra_dias_checada_empleado($empleados_full[$ohem_id],$empleado,$fecha_final,$fecha_inicial);
        if(error::$en_error){
            return  (new error())->error('Error al asignar checada',$empleado);
        }
        return $empleado;

    }

    /**
     * TRASLADADO
     * Obtiene los subordinados de un empleado en un registro de asistencia desde la base de datos.
     *
     * Esta función valida que el nombre de la entidad del empleado no esté vacío. Luego, genera una consulta SQL
     * para obtener los subordinados de un empleado en función de la fila de asistencia proporcionada. La consulta
     * se ejecuta utilizando una conexión PDO y devuelve los resultados como un array. Si ocurre algún error durante
     * la validación, la generación de la consulta SQL o la ejecución de la misma, se devuelve un array con los detalles
     * del error.
     *
     * @param array $asistencia_row Fila de datos del registro de asistencia.
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param PDO $link Conexión PDO a la base de datos.
     *
     * @return array Devuelve un array con los subordinados obtenidos de la base de datos o un array de error si ocurre algún problema.
     */
    private function subordinados(array $asistencia_row, string $entidad_empleado, PDO $link): array
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->subordinados($asistencia_row,$entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al obtener $sql',$sql);
        }
        $rows = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener $rows',$rows);
        }

        return $rows;


    }

    /**
     * TRASLADADO
     * Verifica si existe una checada para una fecha específica dentro de las asistencias del empleado.
     *
     * Esta función recibe un array de asistencias del empleado y una fecha, y verifica si hay alguna checada registrada
     * para esa fecha. Valida que la fecha no esté vacía y recorre el array de asistencias, comprobando si el campo `fecha`
     * de alguna asistencia coincide con la fecha proporcionada. Si encuentra una coincidencia, retorna `true`; de lo
     * contrario, retorna `false`.
     *
     * Si los datos de asistencia no son objetos o si la fecha está vacía, la función retorna un array de error con los detalles.
     *
     * @param array $asistencias_empleado Un array que contiene las asistencias del empleado.
     * @param string $dia_fecha La fecha que se va a verificar. No debe estar vacía.
     *
     * @return bool|array Retorna `true` si existe una checada para la fecha especificada.
     *                    Si no hay coincidencias, retorna `false`. Si ocurre un error, retorna un array de error con los detalles del fallo.
     */
    private function tiene_checada(array $asistencias_empleado, string $dia_fecha)
    {
        $dia_fecha = trim($dia_fecha);
        if($dia_fecha === ''){
            return  (new error())->error('Error $dia_fecha esta vacio',$dia_fecha);
        }

        $tiene_checada = false;
        foreach ($asistencias_empleado as $asistencia_dia){
            if(!is_object($asistencia_dia)){
                return  (new error())->error('Error $asistencias_empleado datos debe ser un objeto',
                    $asistencias_empleado);
            }
            if(!isset($asistencia_dia->fecha)){
                $asistencia_dia->fecha = '';
            }

            if($asistencia_dia->fecha === $dia_fecha){
                $tiene_checada = true;
                break;
            }
        }
        return $tiene_checada;

    }

    /**
     * TRASLADADO
     * Verifica si un empleado tiene una checada registrada en la aplicación de asistencia en una fecha específica.
     *
     * Esta función valida la asistencia del empleado para una fecha determinada y luego consulta la base de datos
     * para verificar si tiene alguna checada registrada en la aplicación de asistencia.
     *
     * @param string $entidad_empleado Nombre de la entidad del empleado (por ejemplo, 'empleado').
     *                                 No debe estar vacío.
     * @param string $fecha Fecha en la que se desea verificar la checada (formato YYYY-MM-DD).
     * @param PDO $link Conexión PDO a la base de datos.
     * @param int $ohem_id ID del empleado en el sistema SAP (o similar). Debe ser mayor a 0.
     *
     * @return bool|array Devuelve true si el empleado tiene una o más checadas registradas en la fecha especificada.
     *              Devuelve false si no tiene checadas. Si ocurre un error, devuelve un array con detalles del error.
     */
    private function tiene_checada_app(string $entidad_empleado,string $fecha, PDO $link,int $ohem_id)
    {
        $valida = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->valida_asistencia(
            $entidad_empleado,$fecha,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al validar asistencia',$valida);
        }

        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->n_checadas($entidad_empleado,$fecha,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $sql',$sql);
        }

        $exe = (new consultas())->exe_objs($link,$sql);
        if(error::$en_error){
            return (new error())->error('Error al ejecutar $sql',$exe);
        }

        $tiene_checada = false;
        if((int)$exe[0]->n_checadas > 0){
            $tiene_checada = true;
        }
        return $tiene_checada;

    }

    /**
     * TRASLADADO
     * Verifica si el empleado tiene asignado un jefe inmediato en un registro de asistencia.
     *
     * Esta función revisa si el campo que indica el jefe inmediato del empleado está presente
     * y si contiene un valor mayor a 0, lo que indica que el empleado tiene un jefe asignado.
     *
     * @param stdClass $asistencia_row Objeto que representa un registro de asistencia del empleado.
     *                                 Se espera que contenga el campo 'jefe_inmediato_id' o equivalente.
     * @param string $entidad_empleado Nombre de la entidad del empleado (por ejemplo, 'empleado').
     *                                 No debe estar vacío.
     *
     * @return bool|array Devuelve true si el empleado tiene un jefe inmediato asignado, de lo contrario, devuelve false.
     *              Si ocurre un error, devuelve un array con detalles del error.
     */
    private function tiene_jefe(stdClass $asistencia_row, string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        $key_jefe_id = $entidad_empleado.'_jefe_inmediato_id';
        $tiene_jefe = false;
        if(isset($asistencia_row->$key_jefe_id)){
            if(trim($asistencia_row->$key_jefe_id) !== ''){
                if((int)$asistencia_row->$key_jefe_id > 0){
                    $tiene_jefe = true;
                }
            }
        }
        return $tiene_jefe;
    }

    /**
     * TRASLADADO
     * Actualiza el estado de asistencia en la tabla `asistencia_app` a 'activo' utilizando una conexión PDO.
     *
     * La función valida que el ID de asistencia sea mayor a 0, genera una consulta SQL para actualizar el registro y
     * ejecuta la consulta de forma segura utilizando una conexión PDO. Si ocurre algún error en el proceso, devuelve
     * un array de error con los detalles del fallo.
     *
     * @param int $asistencia_app_id El ID del registro en `asistencia_app` que se desea actualizar. Debe ser mayor a 0.
     * @param PDO $link La conexión PDO utilizada para ejecutar la consulta.
     *
     * @return array Devuelve un array con el resultado de la ejecución de la consulta o un array de error en caso de fallo.
     */
    private function upd_asistencia(int $asistencia_app_id, PDO $link): array
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id debe ser mayor a 0',$asistencia_app_id);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->upd_asistencia($asistencia_app_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener asistencia sql upd',$sql);
        }

        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al actualizar asistencia',$exe);
        }
        return $exe;

    }


    /**
     * TRASLADADO
     * Actualiza el estado de "checada_en_sitio" a 'activo' en un registro de asistencia si existen eventos asociados.
     *
     * Esta función toma el ID de un registro de asistencia y el nombre de la entidad del empleado. Valida que el
     * ID de la asistencia sea mayor a 0 y luego obtiene el registro de asistencia de la tabla `asistencia_app`.
     * Después, verifica si el empleado tiene eventos en la fecha del registro de asistencia utilizando `tiene_eventos`
     * de la clase `event_tw`. Si tiene eventos, se actualiza el campo `checada_en_sitio` a 'activo'. Si ocurre algún error
     * en la validación, obtención del registro de asistencia o actualización, se devuelve un array con los detalles del error.
     *
     * @param int $asistencia_app_id ID del registro de asistencia.
     * @param string $entidad_empleado Nombre de la entidad que representa al empleado.
     * @param PDO $link Conexión PDO a la base de datos.
     *
     * @return array Devuelve el resultado de la actualización si es exitosa, o un array de error si ocurre algún problema.
     */
    private function upd_checada_desde_asis(int $asistencia_app_id, string $entidad_empleado, PDO $link): array
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error al obtener $asistencia_app_id es menor a 0', $asistencia_app_id);
        }
        $asistencia_app = (new consultas())->registro_bruto(new stdClass(),'asistencia_app',
            $asistencia_app_id,$link,false);
        if(error::$en_error){
            return (new error())->error('Error al obtener asistencia_app',$asistencia_app);
        }
        $key_empleado_id = $entidad_empleado.'_id';

        if(!isset($asistencia_app->$key_empleado_id)){
            $asistencia_app->$key_empleado_id = -1;
        }

        $tiene_checada_tw = (new event_tw())->tiene_eventos($entidad_empleado,
            $asistencia_app->fecha,$link,$asistencia_app->$key_empleado_id);
        if(error::$en_error){
            return (new error())->error('Error al verificar si tiene checada $tiene',$tiene_checada_tw);
        }
        $upd = array();
        if($tiene_checada_tw){
            $upd = $this->upd_checada_en_sitio_activa($asistencia_app->id,$link);
            if(error::$en_error){
                return (new error())->error('Error al actualizar tiene checada',$upd);
            }
        }
        return $upd;

    }


    /**
     * TRASLADADO
     * Actualiza el estado de "checada_en_sitio" a 'activo' en un registro de asistencia en la base de datos.
     *
     * Esta función valida que el ID de la asistencia sea mayor a 0. Luego, genera una consulta SQL para actualizar
     * el campo `checada_en_sitio` a 'activo' en la tabla `asistencia_app` mediante la función `upd_checada_en_sitio_activa`
     * de la clase `asistencia_app`. La consulta se ejecuta de manera segura utilizando transacciones.
     * Si ocurre algún error en el proceso de validación, obtención de la consulta o ejecución de la actualización,
     * se devuelve un array con los detalles del error.
     *
     * @param int $asistencia_app_id ID del registro de asistencia que se va a actualizar.
     * @param PDO $link Conexión PDO a la base de datos.
     *
     * @return array Devuelve el resultado de la actualización si es exitosa o un array de error si ocurre algún problema.
     */
    private function upd_checada_en_sitio_activa(int $asistencia_app_id, PDO $link): array
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id debe ser mayor a 0',$asistencia_app_id);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->upd_checada_en_sitio_activa($asistencia_app_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $sql',$sql);
        }
        $exe = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if(error::$en_error){
            return (new error())->error('Error al ejecutar $sql',$exe);
        }

        return $exe;

    }

    /**
     * TRASLADADO
     * Actualiza el estado de 'jefe_checo' a 'activo' en la tabla `asistencia_app` para un registro específico.
     *
     * Esta función valida el ID de la asistencia y luego ejecuta una consulta SQL que actualiza el campo
     * `jefe_checo` a 'activo' en el registro de la tabla `asistencia_app` correspondiente al ID proporcionado.
     * Si ocurre un error en cualquier parte del proceso, devuelve un array con los detalles del error.
     *
     * @param int $asistencia_app_id ID del registro en la tabla `asistencia_app`. Debe ser mayor a 0.
     * @param PDO $link Conexión PDO para ejecutar la consulta segura.
     *
     * @return array Resultado de la ejecución de la consulta. Si el ID es inválido o ocurre un error, retorna
     *               un array con detalles del error.
     */
    private function upd_checada_jefe(int $asistencia_app_id, PDO $link): array
    {
        if($asistencia_app_id <= 0){
            return (new error())->error('Error $asistencia_app_id debe ser mayor a 0',$asistencia_app_id);
        }
        $sql = (new \desarrollo_em3\manejo_datos\sql\asistencia_app())->upd_jefe_checo_activo($asistencia_app_id);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $sql', $sql);
        }
        $upd = (new transacciones($link))->ejecuta_consulta_segura($sql);
        if (error::$en_error) {
            return (new error())->error('Error al actualizar $asistencia_row', $upd);
        }
        return $upd;

    }

    /**
     * TRASLADADO
     * Actualiza el estado de 'jefe_checo' a 'activo' si el jefe tiene una checada válida en el día especificado.
     *
     * Esta función toma una fila de asistencia y verifica si el jefe inmediato del empleado tiene una checada registrada
     * en la fecha de la asistencia. Si es así, actualiza el campo `jefe_checo` a 'activo' en el registro correspondiente
     * de la tabla `asistencia_app`. Si ocurren errores durante el proceso, devuelve un array con detalles del error.
     *
     * @param stdClass $asistencia_row Objeto que contiene la información de la fila de asistencia. Debe incluir las propiedades:
     *                                  - `fecha` (fecha de la asistencia).
     *                                  - `id` (ID del registro en la tabla `asistencia_app`).
     *                                  - `{entidad_empleado}_jefe_inmediato_id` (ID del jefe inmediato del empleado).
     * @param string $entidad_empleado Nombre de la entidad del empleado. No debe estar vacío.
     * @param PDO $link Conexión PDO para ejecutar consultas.
     *
     * @return stdClass|array Retorna un objeto con las siguientes propiedades:
     *                  - `key_jefe_id`: Nombre de la clave que representa al jefe inmediato en el objeto de asistencia.
     *                  - `tiene_checada`: Valor booleano que indica si el jefe tiene una checada registrada en la fecha.
     *                  - `upd`: Resultado de la actualización si la checada del jefe es válida.
     *                  Si ocurre un error, retorna un array con los detalles del mismo.
     */
    private function upd_checada_jefe_activa(stdClass $asistencia_row, string $entidad_empleado, PDO $link)
    {

        $valida = $this->valida_checada($asistencia_row,$entidad_empleado);
        if (error::$en_error) {
            return (new error())->error('Error al $valida asistencia', $valida);
        }

        $key_jefe_id = $entidad_empleado.'_jefe_inmediato_id';

        $tiene_checada = $this->tiene_checada_app($entidad_empleado,$asistencia_row->fecha,$link,
            $asistencia_row->$key_jefe_id);
        if(error::$en_error){
            return (new error())->error('Error al validar si $tiene_checada',$tiene_checada);
        }
        $data = new stdClass();
        $data->key_jefe_id = $key_jefe_id;
        $data->tiene_checada = $tiene_checada;
        if($tiene_checada) {
            $upd = $this->upd_checada_jefe($asistencia_row->id, $link);
            if (error::$en_error) {
                return (new error())->error('Error al actualizar $asistencia_row', $upd);
            }
            $data->upd = $upd;
        }

        return $data;

    }

    /**
     * TRASLADADO
     * Actualiza el estado de la checada de un empleado con jefe inmediato en el registro de asistencia.
     *
     * La función valida que la entidad del empleado no esté vacía y verifica si el empleado tiene un jefe inmediato asignado.
     * Si tiene jefe, se valida la checada y se actualiza el estado de la asistencia en la base de datos. Si ocurre algún error,
     * se devuelve un array con los detalles del error.
     *
     * @param stdClass $asistencia_row Un objeto que contiene los datos de la asistencia, como el ID de asistencia y la fecha.
     * @param string $entidad_empleado El nombre de la entidad del empleado, utilizado para validar la asistencia y jefe inmediato.
     * @param PDO $link La conexión PDO utilizada para ejecutar las consultas de actualización.
     *
     * @return stdClass|array Devuelve un objeto con los detalles de la actualización, incluido si tiene jefe inmediato o no.
     *                        Devuelve un array de error si se produce algún fallo en el proceso.
     */
    private function upd_checada_con_jefe(stdClass $asistencia_row, string $entidad_empleado, PDO $link)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }

        $upd = new stdClass();
        $tiene_jefe = $this->tiene_jefe($asistencia_row,$entidad_empleado);
        if(error::$en_error){
            return (new error())->error('Error al obtener $tiene_jefe',$tiene_jefe);
        }
        $upd->tiene_jefe = $tiene_jefe;

        if($tiene_jefe){

            $valida = $this->valida_checada($asistencia_row,$entidad_empleado);
            if (error::$en_error) {
                return (new error())->error('Error al $valida asistencia', $valida);
            }

            $upd = $this->upd_checada_jefe_activa($asistencia_row,$entidad_empleado,$link);
            if (error::$en_error) {
                return (new error())->error('Error al actualizar $asistencia_row', $upd);
            }
        }

        return $upd;

    }

    /**
     * TRASLADADO
     * Valida los datos de asistencia asegurando que los campos clave existan y sean correctos.
     *
     * La función verifica la existencia y validez de los campos `fecha` e `id` en el objeto de asistencia.
     * Si alguno de estos campos no cumple con las validaciones, devuelve un array con los detalles del error.
     *
     * @param stdClass $asistencia_app Un objeto que contiene los datos de la asistencia, incluyendo `fecha` e `id`.
     *
     * @return bool|array Devuelve `true` si todos los campos de asistencia son válidos. Si ocurre un error,
     *                    devuelve un array con los detalles del fallo.
     */
    private function valida_asistencia(stdClass $asistencia_app)
    {
        if(!isset($asistencia_app->fecha)){
            return (new error())->error('Error $asistencia_app->fecha no existe',$asistencia_app);
        }
        $asistencia_app->fecha = trim($asistencia_app->fecha);
        if($asistencia_app->fecha === ''){
            return (new error())->error('Error $asistencia_app->fecha esta vacia',$asistencia_app->fecha);
        }
        if(!isset($asistencia_app->id)){
            return (new error())->error('Error $asistencia_app->id no existe',$asistencia_app);
        }
        if(!is_numeric($asistencia_app->id)){
            return (new error())->error('Error $asistencia_app->id debe ser un numero',$asistencia_app);
        }
        if($asistencia_app->id <= 0){
            return (new error())->error('Error $asistencia_app->id debe ser mayor a 0',$asistencia_app->id);
        }
        return true;


    }

    /**
     * TRASLADADO
     * Valida los datos de una asistencia para asegurar que los campos clave estén presentes y correctos.
     *
     * La función valida que el nombre de la entidad del empleado no esté vacío, que los campos `fecha`, el ID del jefe inmediato,
     * y el ID de la asistencia estén presentes en el objeto `asistencia_row` y que contengan valores válidos (no vacíos y mayores a 0).
     * Si ocurre algún error durante la validación, se devuelve un array de error con los detalles.
     *
     * @param stdClass $asistencia_row Un objeto que contiene los datos de la asistencia, como `fecha` y el ID del jefe inmediato.
     * @param string $entidad_empleado El nombre de la entidad del empleado que se utilizará para validar el campo `jefe_inmediato_id`.
     *
     * @return bool|array Devuelve `true` si la validación es exitosa, o un array de error si se produce algún fallo.
     */
    private function valida_checada(stdClass $asistencia_row, string $entidad_empleado)
    {
        $entidad_empleado = trim($entidad_empleado);
        if($entidad_empleado === ''){
            return (new error())->error('Error $entidad_empleado esta vacia',$entidad_empleado);
        }
        if(!isset($asistencia_row->fecha)){
            return (new error())->error('Error $asistencia_row->fecha no existe',$asistencia_row);
        }
        if(trim($asistencia_row->fecha) === ''){
            return (new error())->error('Error $asistencia_row->fecha esta vacia',$asistencia_row);
        }
        $key_jefe_id = $entidad_empleado.'_jefe_inmediato_id';
        if(!isset($asistencia_row->$key_jefe_id)){
            return (new error())->error('Error $asistencia_row->$key_jefe_id no existe',$asistencia_row);
        }
        if($asistencia_row->$key_jefe_id <= 0){
            return (new error())->error('Error $asistencia_row->$key_jefe_id debe ser mayor a 0',
                $asistencia_row);
        }
        if(!isset($asistencia_row->id)){
            return (new error())->error('Error $asistencia_row->id no existe',$asistencia_row);
        }
        if($asistencia_row->id <= 0){
            return (new error())->error('Error $asistencia_row->id debe ser mayor a 0',$asistencia_row);
        }
        return true;

    }





}
