<?php
namespace desarrollo_em3\manejo_datos\data;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\consultas;
use desarrollo_em3\manejo_datos\sql;
use desarrollo_em3\manejo_datos\transacciones;
use PDO;
use stdClass;

class _datos{

    /**
     * EM3
     * Obtiene los campos de una entidad (tabla) en la base de datos y los estructura en un objeto.
     *
     * Esta función ejecuta una consulta `DESCRIBE` sobre la tabla proporcionada para obtener su estructura,
     * y genera un objeto donde cada campo de la tabla es una propiedad con su nombre original y su equivalente renombrado.
     *
     * @param string $entidad Nombre de la entidad (tabla) en la base de datos. No debe estar vacío.
     * @param PDO $link Conexión activa a la base de datos mediante PDO.
     *
     * @return stdClass|array Un objeto donde cada campo de la entidad es una propiedad con su respectivo renombre.
     *                        Si ocurre un error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener los campos de una tabla
     * $entidad = 'usuarios';
     * $campos = $this->campos_entidad($entidad, $pdo);
     * print_r($campos);
     * // Resultado esperado:
     * // stdClass Object
     * // (
     * //     [id] => stdClass Object
     * //         (
     * //             [rename] => usuarios_id
     * //         )
     * //     [nombre] => stdClass Object
     * //         (
     * //             [rename] => usuarios_nombre
     * //         )
     * //     [correo] => stdClass Object
     * //         (
     * //             [rename] => usuarios_correo
     * //         )
     * // )
     *
     * // Ejemplo 2: Error por entidad vacía
     * $entidad = '';
     * $campos = $this->campos_entidad($entidad, $pdo);
     * print_r($campos);
     * // Resultado esperado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Tabla inexistente en la base de datos
     * $entidad = 'tabla_no_existente';
     * $campos = $this->campos_entidad($entidad, $pdo);
     * print_r($campos);
     * // Resultado esperado:
     * // ['error' => 'Error al obtener campos', 'data' => [...]]
     * ```
     *
     * @throws error Si:
     *         - `$entidad` está vacío.
     *         - Ocurre un error al ejecutar la consulta `DESCRIBE`.
     *         - No se pueden obtener los campos de la entidad.
     */
    final public function campos_entidad(string $entidad, PDO $link)
    {
        // Elimina espacios en blanco del nombre de la entidad
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Genera la consulta SQL `DESCRIBE` para obtener la estructura de la tabla
        $describe = (new sql())->describe($entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $describe);
        }

        // Ejecuta la consulta para obtener la estructura de la tabla
        $exe = (new transacciones($link))->ejecuta_consulta_segura($describe);
        if (error::$en_error) {
            return (new error())->error('Error al obtener campos', $exe);
        }

        // Obtiene los registros de la consulta
        $registros = $exe['registros'];

        // Inicializa el objeto que contendrá los campos de la tabla
        $campos = new stdClass();

        // Recorre cada campo de la tabla
        foreach ($registros as $row) {
            // Obtiene el nombre del campo y lo limpia
            $campo = trim($row['Field']);

            // Agrega el campo al objeto con un nuevo nombre
            $campos->$campo = new stdClass();
            $campos->$campo->rename = $entidad . '_' . $campo;
        }

        // Retorna el objeto con los campos renombrados
        return $campos;
    }


    /**
     * EM3
     * Verifica si un valor específico existe en un arreglo de registros basado en una clave dada.
     *
     * Esta función recorre un arreglo de registros, valida que cada registro sea un arreglo y que contenga la clave especificada,
     * y verifica si algún registro tiene un valor que coincida con el valor de comparación.
     *
     * @param string $key_compare El nombre de la clave a buscar dentro de los registros. No puede estar vacío.
     * @param array $rows Un arreglo de registros en el que se buscará el valor. Cada registro debe ser un arreglo asociativo.
     * @param string $val_compare El valor a buscar en la clave especificada.
     *
     * @return bool|array Devuelve `true` si se encuentra el valor en la clave especificada, `false` en caso contrario.
     *                    En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Buscar un valor en registros
     * $key_compare = 'id';
     * $rows = [
     *     ['id' => '123', 'nombre' => 'Juan'],
     *     ['id' => '456', 'nombre' => 'Pedro']
     * ];
     * $val_compare = '123';
     * $resultado = $this->existe_by_campo($key_compare, $rows, $val_compare);
     * echo $resultado; // true
     *
     * // Ejemplo 2: Valor no encontrado
     * $val_compare = '789';
     * $resultado = $this->existe_by_campo($key_compare, $rows, $val_compare);
     * echo $resultado; // false
     *
     * // Ejemplo 3: Error por clave vacía
     * $key_compare = '';
     * $resultado = $this->existe_by_campo($key_compare, $rows, $val_compare);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => '$key_compare esta vacio', 'data' => '']
     * ```
     */
    private function existe_by_campo(string $key_compare, array $rows, string $val_compare)
    {
        // Validar que la clave de comparación no esté vacía
        $key_compare = trim($key_compare);
        if ($key_compare === '') {
            return (new error())->error('$key_compare esta vacio', $key_compare);
        }

        // Inicializar el indicador de existencia
        $existe = false;

        // Recorrer los registros para buscar el valor
        foreach ($rows as $row_cmp) {
            // Validar que cada registro sea un arreglo
            if (!is_array($row_cmp)) {
                return (new error())->error('Error $row_cmp debe ser un array', $rows);
            }

            // Asegurar que la clave existe en el registro, si no, asignar un valor vacío
            if (!isset($row_cmp[$key_compare])) {
                $row_cmp[$key_compare] = '';
            }

            // Comparar el valor en la clave especificada
            if ((string)$row_cmp[$key_compare] === $val_compare) {
                $existe = true;
                break;
            }
        }

        // Retornar el resultado de la búsqueda
        return $existe;
    }

    /**
     * EM3
     * Obtiene el rango de fechas basado en los años y meses proporcionados en `$row_data`.
     *
     * Esta función extrae y valida los valores de años (`years_filter`) y meses (`meses_filter`)
     * a partir de los datos proporcionados. Devuelve un objeto con la información procesada.
     *
     * @param PDO $link Conexión PDO a la base de datos. Se usa para obtener información de los ejercicios almacenados.
     * @param array $row_data Un array de datos que puede contener:
     *                        - `ejercicio_inicial`: (Opcional) ID del año inicial.
     *                        - `ejercicio_final`: (Opcional) ID del año final.
     *                        - `mes_inicial`: (Opcional) Mes inicial (1-12).
     *                        - `mes_final`: (Opcional) Mes final (1-12).
     *
     * @return stdClass|array Devuelve un objeto con la siguiente estructura:
     *                        - `years`: Objeto con los años inicial y final.
     *                        - `meses`: Objeto con los meses inicial y final.
     *                        Si ocurre un error, retorna un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener fechas con datos específicos
     * $row_data = [
     *     'ejercicio_inicial' => 5,  // Año inicial en la base de datos
     *     'ejercicio_final' => 6,    // Año final en la base de datos
     *     'mes_inicial' => 3,        // Marzo
     *     'mes_final' => 9           // Septiembre
     * ];
     * $fechas = $this->filters_fecha($pdo, $row_data);
     * print_r($fechas);
     * // Resultado esperado:
     * // stdClass Object
     * // (
     * //     [years] => stdClass Object
     * //         (
     * //             [inicial] => 2022
     * //             [final] => 2023
     * //         )
     * //
     * //     [meses] => stdClass Object
     * //         (
     * //             [inicial] => 03
     * //             [final] => 09
     * //         )
     * // )
     *
     * // Ejemplo 2: Sin especificar fechas (valores predeterminados)
     * $row_data = [];
     * $fechas = $this->filters_fecha($pdo, $row_data);
     * print_r($fechas);
     * // Resultado esperado (suponiendo que estamos en octubre 2024):
     * // stdClass Object
     * // (
     * //     [years] => stdClass Object
     * //         (
     * //             [inicial] => 1900
     * //             [final] => 2024
     * //         )
     * //
     * //     [meses] => stdClass Object
     * //         (
     * //             [inicial] => 01
     * //             [final] => 10
     * //         )
     * // )
     * ```
     *
     * @throws error Si ocurre un error al obtener los años o los meses.
     */
    private function filters_fecha(PDO $link, array $row_data)
    {
        // Obtener el rango de años
        $years = $this->years_filter($link, $row_data);
        if (error::$en_error) {
            return (new error())->error('Error al obtener ejercicios', $years);
        }

        // Obtener el rango de meses
        $meses = $this->meses_filter($row_data);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $meses', $meses);
        }

        // Crear un objeto con los resultados
        $data = new stdClass();
        $data->years = $years;
        $data->meses = $meses;

        return $data;
    }


    /**
     * EM3
     * Genera un rango de fechas basado en los años y meses proporcionados en `$row_data`.
     *
     * Esta función obtiene el año y mes inicial y final llamando a `filters_fecha()`,
     * y construye dos fechas en formato `YYYY-MM-DD`:
     * - `fecha_inicial`: Primer día del mes inicial del año inicial.
     * - `fecha_final`: Último día del mes final del año final.
     *
     * @param PDO $link Conexión PDO a la base de datos. Se usa para obtener información de los ejercicios almacenados.
     * @param array $row_data Un array de datos que puede contener:
     *                        - `ejercicio_inicial`: (Opcional) ID del año inicial.
     *                        - `ejercicio_final`: (Opcional) ID del año final.
     *                        - `mes_inicial`: (Opcional) Mes inicial (1-12).
     *                        - `mes_final`: (Opcional) Mes final (1-12).
     *
     * @return stdClass|array Devuelve un objeto con la siguiente estructura:
     *                        - `fecha_inicial`: Fecha de inicio en formato `YYYY-MM-DD` (primer día del mes).
     *                        - `fecha_final`: Fecha de fin en formato `YYYY-MM-DD` (último día del mes).
     *                        Si ocurre un error, retorna un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener fechas con datos específicos
     * $row_data = [
     *     'ejercicio_inicial' => 5,  // Año inicial en la base de datos
     *     'ejercicio_final' => 6,    // Año final en la base de datos
     *     'mes_inicial' => 3,        // Marzo
     *     'mes_final' => 9           // Septiembre
     * ];
     * $fechas = $this->filtros_fecha($pdo, $row_data);
     * print_r($fechas);
     * // Resultado esperado:
     * // stdClass Object
     * // (
     * //     [fecha_inicial] => "2022-03-01"
     * //     [fecha_final] => "2023-09-30"
     * // )
     *
     * // Ejemplo 2: Sin especificar fechas (valores predeterminados)
     * $row_data = [];
     * $fechas = $this->filtros_fecha($pdo, $row_data);
     * print_r($fechas);
     * // Resultado esperado (suponiendo que estamos en octubre 2024):
     * // stdClass Object
     * // (
     * //     [fecha_inicial] => "1900-01-01"
     * //     [fecha_final] => "2024-10-31"
     * // )
     *
     * // Ejemplo 3: Error al obtener fechas
     * $row_data = ['ejercicio_inicial' => 'invalid'];
     * $fechas = $this->filtros_fecha($pdo, $row_data);
     * // Resultado esperado:
     * // ['error' => 'Error al obtener $fechas', 'data' => [...]]
     * ```
     *
     * @throws error Si ocurre un error al obtener los años y meses.
     */
    final public function filtros_fecha(PDO $link, array $row_data)
    {
        // Obtener los datos de años y meses
        $fechas = $this->filters_fecha($link, $row_data);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $fechas', $fechas);
        }

        // Construir fecha inicial (primer día del mes inicial del año inicial)
        $fecha_inicial = $fechas->years->inicial . "-" . $fechas->meses->inicial . "-01";

        // Construir fecha final (último día del mes final del año final)
        $fecha_final = $fechas->years->final . "-" . $fechas->meses->final . "-01";
        $fecha_final = date("Y-m-t", strtotime($fecha_final));

        // Crear objeto con las fechas calculadas
        $data = new stdClass();
        $data->fecha_inicial = $fecha_inicial;
        $data->fecha_final = $fecha_final;

        return $data;
    }



    /**
     * EM3
     * Integra un registro en un conjunto de resultados si no existe ya basado en una clave de comparación.
     *
     * Esta función valida los parámetros de entrada, verifica si un registro ya existe en un conjunto de resultados
     * basado en una clave específica, y lo agrega al conjunto si no existe.
     *
     * @param string $key_compare El nombre de la clave a utilizar para comparar registros. No puede estar vacío.
     * @param array $row_compare El registro a verificar e integrar en el resultado. Debe ser un arreglo asociativo.
     * @param array $rows El conjunto de registros existentes donde se realiza la comparación.
     * @param array $rows_new El conjunto de resultados donde se agregará el registro si no existe.
     *
     * @return array Devuelve el conjunto de resultados actualizado (`$rows_new`).
     *               En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Agregar un registro único al conjunto de resultados
     * $key_compare = 'id';
     * $row_compare = ['id' => '123', 'nombre' => 'Juan'];
     * $rows = [
     *     ['id' => '456', 'nombre' => 'Pedro'],
     *     ['id' => '789', 'nombre' => 'Maria'],
     * ];
     * $rows_new = [
     *     ['id' => '456', 'nombre' => 'Pedro'],
     * ];
     * $resultado = $this->integra_a_result($key_compare, $row_compare, $rows, $rows_new);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     ['id' => '456', 'nombre' => 'Pedro'],
     * //     ['id' => '123', 'nombre' => 'Juan'],
     * // ]
     *
     * // Ejemplo 2: Error por clave vacía
     * $key_compare = '';
     * $resultado = $this->integra_a_result($key_compare, $row_compare, $rows, $rows_new);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $key_compare esta vacio', 'data' => '']
     * ```
     */
    private function integra_a_result(string $key_compare, array $row_compare, array $rows, array $rows_new): array
    {
        // Validar que la clave de comparación no esté vacía
        $key_compare = trim($key_compare);
        if ($key_compare === '') {
            return (new error())->error('Error $key_compare esta vacio', $key_compare);
        }

        // Asegurar que la clave existe en el registro a comparar
        if (!isset($row_compare[$key_compare])) {
            $row_compare[$key_compare] = '';
        }

        // Verificar si el registro ya existe en el conjunto de resultados
        $existe = $this->existe_by_campo($key_compare, $rows, $row_compare[$key_compare]);
        if (error::$en_error) {
            return (new error())->error('Error al validar si existe elemento', $existe);
        }

        // Si no existe, agregar el registro al conjunto de resultados
        if (!$existe) {
            $rows_new[] = $row_compare;
        }

        // Retornar el conjunto de resultados actualizado
        return $rows_new;
    }

    /**
     * EM3
     * Obtiene los meses inicial y final a partir de un conjunto de datos.
     *
     * Esta función toma un array de datos (`$row_data`) que puede contener valores de meses (`mes_inicial` y `mes_final`),
     * y devuelve un objeto con los valores obtenidos. Si no se proporcionan valores, se asignan valores por defecto:
     * `1` (enero) como mes inicial y el mes actual (`m`) como mes final.
     *
     * @param array $row_data Un array de datos que puede contener los siguientes valores opcionales:
     *                        - `mes_inicial`: (Opcional) Número del mes inicial (1-12).
     *                        - `mes_final`: (Opcional) Número del mes final (1-12).
     *
     * @return stdClass Devuelve un objeto con las siguientes propiedades:
     *                  - `inicial`: Mes inicial en formato `MM` (dos dígitos).
     *                  - `final`: Mes final en formato `MM` (dos dígitos).
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener meses con datos definidos
     * $row_data = [
     *     'mes_inicial' => 3,  // Marzo
     *     'mes_final' => 9     // Septiembre
     * ];
     * $meses = $this->meses_filter($row_data);
     * print_r($meses);
     * // Resultado esperado:
     * // stdClass Object
     * // (
     * //     [inicial] => 03
     * //     [final] => 09
     * // )
     *
     * // Ejemplo 2: Sin especificar meses (usa valores por defecto)
     * $row_data = [];
     * $meses = $this->meses_filter($row_data);
     * print_r($meses);
     * // Resultado esperado (suponiendo que estamos en octubre):
     * // stdClass Object
     * // (
     * //     [inicial] => 01
     * //     [final] => 10
     * // )
     *
     * // Ejemplo 3: Mes inicial sin formato correcto (se corrige automáticamente)
     * $row_data = ['mes_inicial' => 5];
     * $meses = $this->meses_filter($row_data);
     * print_r($meses);
     * // Resultado esperado:
     * // stdClass Object
     * // (
     * //     [inicial] => 05
     * //     [final] => 10  (Suponiendo que el mes actual es octubre)
     * // )
     * ```
     */
    private function meses_filter(array $row_data): stdClass
    {
        // Valor por defecto del mes inicial (Enero)
        $mes_inicial = 1;

        // Verificar si se proporciona el mes inicial
        if(isset($row_data['mes_inicial']) && $row_data['mes_inicial'] !== ''){
            $mes_inicial = $row_data['mes_inicial'];
        }

        // Convertir a formato MM si es menor o igual a 9
        if($mes_inicial <= 9){
            $mes_inicial = "0".$mes_inicial;
        }

        // Valor por defecto del mes final (Mes actual)
        $mes_final = (int)date('m');

        // Verificar si se proporciona el mes final
        if(isset($row_data['mes_final']) && $row_data['mes_final'] !== ''){
            $mes_final = $row_data['mes_final'];
        }

        // Convertir a formato MM si es menor o igual a 9
        if($mes_final <= 9){
            $mes_final = "0".$mes_final;
        }

        // Retornar los meses en un objeto stdClass
        $meses = new stdClass();
        $meses->inicial = $mes_inicial;
        $meses->final = $mes_final;

        return $meses;
    }



    /**
     * EM3
     * Genera un conjunto único de registros basado en una clave de comparación.
     *
     * Esta función recorre un conjunto de registros, verifica si cada registro ya existe en el resultado final
     * (basado en la clave de comparación), y agrega solo los registros únicos al conjunto de resultados.
     *
     * @param string $key_compare El nombre de la clave a utilizar para comparar registros. No puede estar vacío.
     * @param array $result_reasigna El conjunto de registros a procesar. Cada registro debe ser un arreglo asociativo.
     *
     * @return array Devuelve un arreglo con registros únicos basados en la clave de comparación.
     *               En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Filtrar registros únicos por clave
     * $key_compare = 'id';
     * $result_reasigna = [
     *     ['id' => '123', 'nombre' => 'Juan'],
     *     ['id' => '456', 'nombre' => 'Pedro'],
     *     ['id' => '123', 'nombre' => 'Juan'],
     * ];
     * $resultado = $this->rows_news($key_compare, $result_reasigna);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     ['id' => '123', 'nombre' => 'Juan'],
     * //     ['id' => '456', 'nombre' => 'Pedro'],
     * // ]
     *
     * // Ejemplo 2: Error por clave vacía
     * $key_compare = '';
     * $resultado = $this->rows_news($key_compare, $result_reasigna);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $key_compare esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por registro no válido
     * $result_reasigna = [
     *     ['id' => '123', 'nombre' => 'Juan'],
     *     'invalid_record'
     * ];
     * $resultado = $this->rows_news($key_compare, $result_reasigna);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $row debe ser un array', 'data' => $result_reasigna]
     * ```
     */
    final public function rows_news(string $key_compare, array $result_reasigna): array
    {
        // Validar que la clave de comparación no esté vacía
        $key_compare = trim($key_compare);
        if ($key_compare === '') {
            return (new error())->error('Error $key_compare esta vacio', $key_compare);
        }

        // Inicializar el conjunto de resultados únicos
        $result_new = array();

        // Recorrer los registros a reasignar
        foreach ($result_reasigna as $row) {
            // Validar que el registro sea un arreglo
            if (!is_array($row)) {
                return (new error())->error('Error $row debe ser un array', $result_reasigna);
            }

            // Integrar el registro en el conjunto de resultados únicos
            $result_new = $this->integra_a_result($key_compare, $row, $result_new, $result_new);
            if (error::$en_error) {
                return (new error())->error('Error integrar row new', $result_new);
            }
        }

        // Retornar el conjunto de resultados únicos
        return $result_new;
    }

    /**
     * EM3
     * Obtiene los años inicial y final de un conjunto de datos basado en registros de ejercicios.
     *
     * Esta función toma un array de datos (`$row_data`) que puede contener identificadores de ejercicios (`ejercicio_inicial` y `ejercicio_final`),
     * consulta la base de datos para obtener los años correspondientes a estos ejercicios, y devuelve un objeto con los valores obtenidos.
     * Si no se proporcionan valores, se asignan valores por defecto: `1900` como año inicial y el año actual (`Y`) como año final.
     *
     * @param PDO $link Conexión PDO activa a la base de datos.
     * @param array $row_data Un array de datos que puede contener los identificadores de ejercicios:
     *                        - `ejercicio_inicial`: (Opcional) ID del ejercicio inicial.
     *                        - `ejercicio_final`: (Opcional) ID del ejercicio final.
     *
     * @return stdClass|array Devuelve un objeto con las siguientes propiedades:
     *                        - `inicial`: Año inicial obtenido o valor por defecto (`1900`).
     *                        - `final`: Año final obtenido o el año actual (`Y`).
     *                        En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener años con ejercicios definidos
     * $row_data = [
     *     'ejercicio_inicial' => 1,  // ID de un ejercicio en la base de datos
     *     'ejercicio_final' => 3     // ID de otro ejercicio en la base de datos
     * ];
     * $years = $this->years_filter($pdo, $row_data);
     * print_r($years);
     * // Resultado esperado (si los ejercicios existen en la base de datos):
     * // stdClass Object
     * // (
     * //     [inicial] => 2020
     * //     [final] => 2023
     * // )
     *
     * // Ejemplo 2: Sin especificar ejercicios
     * $row_data = [];
     * $years = $this->years_filter($pdo, $row_data);
     * print_r($years);
     * // Resultado esperado:
     * // stdClass Object
     * // (
     * //     [inicial] => 1900
     * //     [final] => 2024  (Suponiendo que estamos en el año 2024)
     * // )
     *
     * // Ejemplo 3: Error al obtener el ejercicio
     * $row_data = ['ejercicio_inicial' => 99999]; // Un ID inexistente
     * $years = $this->years_filter($pdo, $row_data);
     * print_r($years);
     * // Resultado esperado:
     * // ['error' => 'Error al obtener ejercicio', 'detalle' => ...]
     * ```
     */
    private function years_filter(PDO $link, array $row_data)
    {
        // Asignar valores por defecto
        $year_inicial = '1900';

        // Verificar si se proporciona el ejercicio inicial en los datos
        if(isset($row_data['ejercicio_inicial']) && $row_data['ejercicio_inicial'] !== ''){
            $ejercicio = (new consultas())->registro_bruto(new stdClass(), 'ejercicio',
                $row_data['ejercicio_inicial'], $link, false);

            // Manejo de error en la consulta del ejercicio inicial
            if(error::$en_error){
                return (new error())->error('Error al obtener ejercicio', $ejercicio);
            }

            // Asignar el año obtenido de la base de datos
            $year_inicial = $ejercicio->year;
        }

        // Año final por defecto: Año actual
        $year_final = date('Y');

        // Verificar si se proporciona el ejercicio final en los datos
        if(isset($row_data['ejercicio_final']) && $row_data['ejercicio_final'] !== ''){
            $ejercicio = (new consultas())->registro_bruto(new stdClass(),
                'ejercicio', $row_data['ejercicio_final'], $link, false);

            // Manejo de error en la consulta del ejercicio final
            if(error::$en_error){
                return (new error())->error('Error al obtener ejercicio', $ejercicio);
            }

            // Asignar el año obtenido de la base de datos
            $year_final = $ejercicio->year;
        }

        // Retornar los años en un objeto stdClass
        $years = new stdClass();
        $years->inicial = $year_inicial;
        $years->final = $year_final;

        return $years;
    }


}
