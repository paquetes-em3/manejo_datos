<?php
namespace desarrollo_em3\manejo_datos\data;

class _limpia{

    /**
     * TRASLADADO
     * Limpia una cadena eliminando caracteres específicos para preparar el valor para su uso como número.
     *
     * Esta función elimina espacios en blanco, comas, signos de dólar y otros espacios en la cadena
     * para asegurar que el valor esté en un formato adecuado para ser tratado como un número.
     *
     * @param string $var Cadena que contiene el valor a limpiar.
     * @return string Cadena limpia y preparada para su uso como número.
     */
    final public function limpia_double(string $var): string
    {
        $var = trim($var);
        $var = str_replace(',','',$var);
        $var = str_replace(' ','',$var);
        $var = str_replace('$','',$var);
        return trim($var);

    }
}