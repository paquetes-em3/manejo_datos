<?php
namespace desarrollo_em3\manejo_datos\data;

use desarrollo_em3\error\error;

class _childrens
{
    /**
     * EM3
     * Valida y genera un array de modelos hijos basado en configuraciones específicas.
     *
     * Esta función toma un array de modelos hijos, valida que cumplan con la estructura requerida,
     * y genera un array con los modelos procesados. Cada modelo debe contener las claves `filtros`
     * y `filtros_con_valor`, y estas deben ser arrays.
     *
     * @param array $hijo Un array de modelos hijos, donde cada clave debe ser un string y cada valor
     *                    debe contener las claves:
     *                    - `filtros` (array): Un conjunto de filtros aplicables al modelo.
     *                    - `filtros_con_valor` (array): Filtros con valores definidos.
     *
     * @return array Devuelve un array con los modelos hijos validados y procesados. En caso de error,
     *               devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar modelos hijos válidos
     * $hijo = [
     *     'modelo1' => [
     *         'filtros' => ['campo1', 'campo2'],
     *         'filtros_con_valor' => ['campo1' => 'valor1']
     *     ],
     *     'modelo2' => [
     *         'filtros' => ['campo3'],
     *         'filtros_con_valor' => ['campo3' => 'valor2']
     *     ]
     * ];
     * $resultado = $this->genera_modelos_hijos($hijo);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     'modelo1' => [
     * //         'filtros' => ['campo1', 'campo2'],
     * //         'filtros_con_valor' => ['campo1' => 'valor1']
     * //     ],
     * //     'modelo2' => [
     * //         'filtros' => ['campo3'],
     * //         'filtros_con_valor' => ['campo3' => 'valor2']
     * //     ]
     * // ]
     *
     * // Ejemplo 2: Clave no válida (error)
     * $hijo = [
     *     0 => [
     *         'filtros' => ['campo1'],
     *         'filtros_con_valor' => ['campo1' => 'valor1']
     *     ]
     * ];
     * $resultado = $this->genera_modelos_hijos($hijo);
     * // Resultado:
     * // ['error' => 'Error $key debe ser un string', 'data' => [...]]
     *
     * // Ejemplo 3: Filtros faltantes (error)
     * $hijo = [
     *     'modelo1' => [
     *         'filtros_con_valor' => ['campo1' => 'valor1']
     *     ]
     * ];
     * $resultado = $this->genera_modelos_hijos($hijo);
     * // Resultado:
     * // ['error' => 'Error debe existir filtros ', 'data' => [...]]
     * ```
     */
    final public function genera_modelos_hijos(array $hijo): array
    {
        // Inicializar el array para los modelos hijos procesados
        $modelos_hijos = [];

        // Recorrer cada elemento del array de hijos
        foreach ($hijo as $key => $modelo) {
            // Validar que la clave sea un string
            if (is_numeric($key)) {
                return (new error())->error('Error $key debe ser un string', $hijo);
            }

            // Validar que las claves `filtros` y `filtros_con_valor` existan
            if (!isset($modelo['filtros'])) {
                return (new error())->error('Error debe existir filtros ', $hijo);
            }
            if (!isset($modelo['filtros_con_valor'])) {
                return (new error())->error('Error debe existir filtros_con_valor ', $hijo);
            }

            // Validar que `filtros` y `filtros_con_valor` sean arrays
            if (!is_array($modelo['filtros'])) {
                return (new error())->error('Error debe ser array filtros ', $hijo);
            }
            if (!is_array($modelo['filtros_con_valor'])) {
                return (new error())->error('Error debe ser array filtros_con_valor ', $hijo);
            }

            // Asignar los filtros y filtros con valor al modelo procesado
            $modelos_hijos[$key]['filtros'] = $modelo['filtros'];
            $modelos_hijos[$key]['filtros_con_valor'] = $modelo['filtros_con_valor'];
        }

        // Retornar los modelos hijos procesados
        return $modelos_hijos;
    }


    /**
     * EM3
     * Genera un filtro dinámico para un modelo hijo basado en los datos del modelo y una fila de datos.
     *
     * Esta función valida la estructura del modelo hijo (`data_modelo`), extrae los filtros y sus valores,
     * y construye un array que combina los filtros dinámicos provenientes de `$row` y los filtros con
     * valores definidos en `data_modelo['filtros_con_valor']`.
     *
     * @param array $data_modelo Un array que describe los filtros para el modelo hijo. Debe incluir:
     *                           - `filtros` (array): Un mapa donde la clave es el campo del modelo hijo
     *                             y el valor es el nombre del campo en `$row`.
     *                           - `filtros_con_valor` (array): Un mapa de filtros con valores estáticos.
     * @param array $row Una fila de datos utilizada para generar los filtros dinámicos. Debe contener
     *                   los valores necesarios para los campos definidos en `data_modelo['filtros']`.
     *
     * @return array Devuelve un array que combina los filtros dinámicos y los filtros con valores estáticos.
     *               En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Filtros válidos
     * $data_modelo = [
     *     'filtros' => [
     *         'campo_hijo1' => 'campo_padre1',
     *         'campo_hijo2' => 'campo_padre2'
     *     ],
     *     'filtros_con_valor' => [
     *         'campo_hijo3' => 'valor_estatico'
     *     ]
     * ];
     * $row = [
     *     'campo_padre1' => 'valor1',
     *     'campo_padre2' => 'valor2'
     * ];
     * $resultado = $this->obten_filtro_para_hijo($data_modelo, $row);
     * print_r($resultado);
     * // Resultado:
     * // [
     * //     'campo_hijo1' => 'valor1',
     * //     'campo_hijo2' => 'valor2',
     * //     'campo_hijo3' => 'valor_estatico'
     * // ]
     *
     * // Ejemplo 2: Campo faltante en $row (error)
     * $data_modelo = [
     *     'filtros' => [
     *         'campo_hijo1' => 'campo_padre1'
     *     ],
     *     'filtros_con_valor' => []
     * ];
     * $row = [];
     * $resultado = $this->obten_filtro_para_hijo($data_modelo, $row);
     * // Resultado:
     * // ['error' => 'Error no existe $row[campo_padre1]', 'data' => [...]]
     *
     * // Ejemplo 3: Campo vacío en `filtros` (error)
     * $data_modelo = [
     *     'filtros' => [
     *         'campo_hijo1' => ''
     *     ],
     *     'filtros_con_valor' => []
     * ];
     * $row = [
     *     'campo_padre1' => 'valor1'
     * ];
     * $resultado = $this->obten_filtro_para_hijo($data_modelo, $row);
     * // Resultado:
     * // ['error' => 'Error no $campo_row no puede venir vacio', 'data' => [...]]
     * ```
     */
    final public function obten_filtro_para_hijo(array $data_modelo, array $row): array
    {
        // Validar la estructura del modelo hijo
        $valida = $this->valida_filtros_hijo($data_modelo);
        if (error::$en_error) {
            return (new error())->error('Error al validar data_modelo', $valida);
        }

        // Extraer los filtros y filtros con valores del modelo hijo
        $filtros = $data_modelo['filtros'];
        $filtros_con_valor = $data_modelo['filtros_con_valor'];
        $filtro = [];

        // Procesar filtros dinámicos basados en `$row`
        foreach ($filtros as $campo_filtro => $campo_row) {
            if ($campo_row === '') {
                return (new error())->error('Error no $campo_row no puede venir vacio', $filtros);
            }
            if (!isset($row[$campo_row])) {
                return (new error())->error('Error no existe $row[' . $campo_row . ']', $filtros);
            }
            $filtro[$campo_filtro] = $row[$campo_row];
        }

        // Agregar los filtros con valores estáticos
        foreach ($filtros_con_valor as $campo_filtro => $value) {
            $filtro[$campo_filtro] = $value;
        }

        // Retornar el filtro combinado
        return $filtro;
    }


    /**
     * EM3
     * Valida la estructura de los filtros de un modelo hijo.
     *
     * Esta función verifica que el array `$data_modelo` tenga las claves `filtros` y `filtros_con_valor`,
     * y que ambas sean arrays. Si alguna clave falta o no cumple con el formato esperado, devuelve un error.
     *
     * @param array $data_modelo Un array que contiene los filtros de un modelo hijo. Debe incluir:
     *                           - `filtros` (array): Un conjunto de filtros aplicables al modelo.
     *                           - `filtros_con_valor` (array): Filtros con valores definidos.
     *
     * @return bool|array Devuelve `true` si la validación es exitosa. En caso de error, devuelve un array
     *                    con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Validación exitosa
     * $data_modelo = [
     *     'filtros' => ['campo1', 'campo2'],
     *     'filtros_con_valor' => ['campo1' => 'valor1']
     * ];
     * $resultado = $this->valida_filtros_hijo($data_modelo);
     * echo $resultado ? 'Validación exitosa' : 'Error';
     * // Resultado:
     * // Validación exitosa
     *
     * // Ejemplo 2: Falta la clave `filtros` (error)
     * $data_modelo = [
     *     'filtros_con_valor' => ['campo1' => 'valor1']
     * ];
     * $resultado = $this->valida_filtros_hijo($data_modelo);
     * // Resultado:
     * // ['error' => 'Error data_modelo[filtros] no existe', 'data' => [...]]
     *
     * // Ejemplo 3: `filtros` no es un array (error)
     * $data_modelo = [
     *     'filtros' => 'no_es_array',
     *     'filtros_con_valor' => ['campo1' => 'valor1']
     * ];
     * $resultado = $this->valida_filtros_hijo($data_modelo);
     * // Resultado:
     * // ['error' => 'Error data_modelo[filtros] debe ser array', 'data' => [...]]
     * ```
     */
    final public function valida_filtros_hijo(array $data_modelo)
    {
        // Verificar que la clave `filtros` exista
        if (!isset($data_modelo['filtros'])) {
            return (new error())->error('Error data_modelo[filtros] no existe', $data_modelo);
        }

        // Verificar que la clave `filtros_con_valor` exista
        if (!isset($data_modelo['filtros_con_valor'])) {
            return (new error())->error('Error data_modelo[filtros_con_valor] no existe', $data_modelo);
        }

        // Validar que `filtros` sea un array
        if (!is_array($data_modelo['filtros'])) {
            return (new error())->error('Error data_modelo[filtros] debe ser array', $data_modelo);
        }

        // Validar que `filtros_con_valor` sea un array
        if (!is_array($data_modelo['filtros_con_valor'])) {
            return (new error())->error('Error data_modelo[filtros_con_valor] debe ser array', $data_modelo);
        }

        // Si todas las validaciones pasan, retornar true
        return true;
    }


}
