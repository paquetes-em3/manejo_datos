<?php
namespace desarrollo_em3\manejo_datos;

use desarrollo_em3\error\error;

class _valida
{

    /**
     * EM3
     * Valida los parámetros relacionados con campos de fecha y una entidad.
     *
     * Esta función asegura que los valores de `$campo_fecha_inicio`, `$entidad` y `$fecha` no estén vacíos.
     * Si alguno de estos parámetros no cumple con los criterios, genera un error descriptivo.
     *
     * @param string $campo_fecha_inicio El nombre del campo de fecha de inicio a validar. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla) a validar. No debe estar vacío.
     * @param string $fecha La fecha a validar. No debe estar vacía.
     *
     * @return bool|array Devuelve `true` si todos los parámetros son válidos. En caso de error, devuelve un array
     *                    con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Parámetros válidos
     * $campo_fecha_inicio = 'fecha_inicio';
     * $entidad = 'empleados';
     * $fecha = '2023-01-01';
     * $resultado = $this->valida_base_campos_fecha($campo_fecha_inicio, $entidad, $fecha);
     * echo $resultado ? 'Validación exitosa' : 'Error';
     * // Resultado:
     * // Validación exitosa
     *
     * // Ejemplo 2: Fecha vacía (error)
     * $campo_fecha_inicio = 'fecha_inicio';
     * $entidad = 'empleados';
     * $fecha = '';
     * $resultado = $this->valida_base_campos_fecha($campo_fecha_inicio, $entidad, $fecha);
     * // Resultado:
     * // ['error' => 'Error fecha esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Entidad vacía (error)
     * $campo_fecha_inicio = 'fecha_inicio';
     * $entidad = '';
     * $fecha = '2023-01-01';
     * $resultado = $this->valida_base_campos_fecha($campo_fecha_inicio, $entidad, $fecha);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     *
     * // Ejemplo 4: Campo de fecha vacío (error)
     * $campo_fecha_inicio = '';
     * $entidad = 'empleados';
     * $fecha = '2023-01-01';
     * $resultado = $this->valida_base_campos_fecha($campo_fecha_inicio, $entidad, $fecha);
     * // Resultado:
     * // ['error' => 'Error $campo_fecha_inicio esta vacia', 'data' => '']
     * ```
     */
    final public function valida_base_campos_fecha(string $campo_fecha_inicio, string $entidad, string $fecha)
    {
        // Validar que la fecha no esté vacía
        $fecha = trim($fecha);
        if ($fecha === '') {
            return (new error())->error('Error fecha esta vacia', $fecha);
        }

        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Validar que el campo de fecha de inicio no esté vacío
        $campo_fecha_inicio = trim($campo_fecha_inicio);
        if ($campo_fecha_inicio === '') {
            return (new error())->error('Error $campo_fecha_inicio esta vacia', $campo_fecha_inicio);
        }

        // Si todas las validaciones son exitosas, retornar true
        return true;
    }


    /**
     * EM3
     * Valida el nombre de un modelo para asegurar que cumpla con los criterios requeridos.
     *
     * Esta función verifica que el nombre del modelo no esté vacío, y que sea una cadena de texto no numérica.
     * Si el nombre del modelo es válido, lo retorna. En caso contrario, genera un error descriptivo.
     *
     * @param string $name_modelo El nombre del modelo a validar.
     *
     * @return string|array Devuelve el nombre del modelo si es válido. En caso de error, devuelve un array con
     *                      detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Nombre de modelo válido
     * $name_modelo = 'empleados';
     * $resultado = $this->valida_data_modelo($name_modelo);
     * echo $resultado;
     * // Resultado:
     * // empleados
     *
     * // Ejemplo 2: Nombre de modelo vacío (error)
     * $name_modelo = '';
     * $resultado = $this->valida_data_modelo($name_modelo);
     * // Resultado:
     * // ['error' => 'Error modelo no puede venir vacio', 'data' => '']
     *
     * // Ejemplo 3: Nombre de modelo numérico (error)
     * $name_modelo = '12345';
     * $resultado = $this->valida_data_modelo($name_modelo);
     * // Resultado:
     * // ['error' => 'Error modelo debe ser un txt', 'data' => '12345']
     * ```
     */
    final public function valida_data_modelo(string $name_modelo)
    {
        // Validar que el nombre del modelo no esté vacío
        if ($name_modelo === '') {
            return (new error())->error('Error modelo no puede venir vacio', $name_modelo);
        }

        // Validar que el nombre del modelo no sea numérico
        if (is_numeric($name_modelo)) {
            return (new error())->error('Error modelo debe ser un txt', $name_modelo);
        }

        // Retornar el nombre del modelo si es válido
        return $name_modelo;
    }


    /**
     * ERROR
     * @param string $accion
     * @param string $seccion
     * @return array|true
     */
    final public function valida_datos_lista_entrada(string $accion, string $seccion)
    {
        $seccion = trim($seccion);
        if($seccion === ''){
            return (new error())->error('Error seccion no puede venir vacio',$seccion);
        }
        $accion = trim($accion);
        if($accion === ''){
            return (new error())->error('Error $accion no puede venir vacio',$accion);
        }
        return true;
    }

    /**
     * EM3
     * Valida los parámetros de entrada relacionados con días de prueba.
     *
     * Esta función verifica la validez de los campos base relacionados con fechas, así como los valores adicionales
     * como el número de días de prueba (`n_dias_prueba`) y el array de identificadores de plazas (`plazas_id`).
     *
     * @param string $campo_fecha_inicio El nombre del campo de fecha de inicio a validar. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla) a validar. No debe estar vacío.
     * @param string $fecha La fecha a validar. No debe estar vacía.
     * @param int $n_dias_prueba El número de días de prueba. Debe ser mayor o igual a 0.
     * @param array $plazas_id Un array de identificadores de plazas. No debe estar vacío.
     *
     * @return bool|array Devuelve `true` si todos los parámetros son válidos. En caso de error, devuelve un array
     *                    con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Parámetros válidos
     * $campo_fecha_inicio = 'fecha_inicio';
     * $entidad = 'empleados';
     * $fecha = '2023-01-01';
     * $n_dias_prueba = 15;
     * $plazas_id = [1, 2, 3];
     * $resultado = $this->valida_entrada_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $plazas_id);
     * echo $resultado ? 'Validación exitosa' : 'Error';
     * // Resultado:
     * // Validación exitosa
     *
     * // Ejemplo 2: Array de plazas vacío (error)
     * $campo_fecha_inicio = 'fecha_inicio';
     * $entidad = 'empleados';
     * $fecha = '2023-01-01';
     * $n_dias_prueba = 15;
     * $plazas_id = [];
     * $resultado = $this->valida_entrada_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $plazas_id);
     * // Resultado:
     * // ['error' => 'Error $plazas_id esta vacia', 'data' => []]
     *
     * // Ejemplo 3: Número de días de prueba negativo (error)
     * $campo_fecha_inicio = 'fecha_inicio';
     * $entidad = 'empleados';
     * $fecha = '2023-01-01';
     * $n_dias_prueba = -5;
     * $plazas_id = [1, 2, 3];
     * $resultado = $this->valida_entrada_dias_prueba($campo_fecha_inicio, $entidad, $fecha, $n_dias_prueba, $plazas_id);
     * // Resultado:
     * // ['error' => 'Error $n_dias_prueba debe ser mayor igual a 0', 'data' => -5]
     * ```
     */
    final public function valida_entrada_dias_prueba(string $campo_fecha_inicio, string $entidad, string $fecha,
                                                     int $n_dias_prueba, array $plazas_id)
    {
        // Validar los campos base relacionados con fechas
        $valida = $this->valida_base_campos_fecha($campo_fecha_inicio, $entidad, $fecha);
        if (error::$en_error) {
            return (new error())->error('Error al validar campos base', $valida);
        }

        // Validar que el array de plazas no esté vacío
        if (count($plazas_id) === 0) {
            return (new error())->error('Error $plazas_id esta vacia', $plazas_id);
        }

        // Validar que el número de días de prueba sea mayor o igual a 0
        if ($n_dias_prueba < 0) {
            return (new error())->error('Error $n_dias_prueba debe ser mayor igual a 0', $n_dias_prueba);
        }

        // Si todas las validaciones son exitosas, retornar true
        return true;
    }


    /**
     * EM3
     * Valida los datos relacionados con un pago por corte.
     *
     * Esta función verifica que el identificador del pago por corte, el campo de monto de pago,
     * y el campo de movimiento sean válidos, es decir, que no estén vacíos o sean inválidos.
     *
     * @param string $campo_monto_pago El nombre o valor del campo de monto de pago. No debe estar vacío.
     * @param string $campo_movto El nombre o valor del campo de movimiento. No debe estar vacío.
     * @param int $pago_corte_id El identificador del pago por corte. Debe ser mayor a 0.
     *
     * @return bool|array Devuelve `true` si todos los datos son válidos. En caso de error, devuelve un array
     *                    con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Validación exitosa
     * $campo_monto_pago = 'monto';
     * $campo_movto = 'movimiento';
     * $pago_corte_id = 123;
     * $resultado = $this->valida_pago_corte($campo_monto_pago, $campo_movto, $pago_corte_id);
     * echo $resultado ? 'Validación exitosa' : 'Error';
     * // Resultado:
     * // Validación exitosa
     *
     * // Ejemplo 2: Identificador de pago por corte inválido (error)
     * $campo_monto_pago = 'monto';
     * $campo_movto = 'movimiento';
     * $pago_corte_id = 0;
     * $resultado = $this->valida_pago_corte($campo_monto_pago, $campo_movto, $pago_corte_id);
     * // Resultado:
     * // ['error' => 'Error $pago_corte_id debe ser mayor a 0', 'data' => 0]
     *
     * // Ejemplo 3: Campo de monto de pago vacío (error)
     * $campo_monto_pago = '';
     * $campo_movto = 'movimiento';
     * $pago_corte_id = 123;
     * $resultado = $this->valida_pago_corte($campo_monto_pago, $campo_movto, $pago_corte_id);
     * // Resultado:
     * // ['error' => 'Error $campo_monto_pago no puede venir vacio', 'data' => '']
     * ```
     */
    final public function valida_pago_corte(string $campo_monto_pago, string $campo_movto, int $pago_corte_id)
    {
        // Validar que el identificador del pago por corte sea mayor a 0
        if ($pago_corte_id <= 0) {
            return (new error())->error('Error $pago_corte_id debe ser mayor a 0', $pago_corte_id);
        }

        // Validar que el campo de monto de pago no esté vacío
        $campo_monto_pago = trim($campo_monto_pago);
        if ($campo_monto_pago === '') {
            return (new error())->error('Error $campo_monto_pago no puede venir vacio', $campo_monto_pago);
        }

        // Validar que el campo de movimiento no esté vacío
        $campo_movto = trim($campo_movto);
        if ($campo_movto === '') {
            return (new error())->error('Error $campo_movto no puede venir vacio', $campo_movto);
        }

        // Si todas las validaciones pasan, retornar true
        return true;
    }


}
