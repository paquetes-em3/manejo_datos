<?php
namespace desarrollo_em3\manejo_datos;

use DateTime;
use desarrollo_em3\error\error;
use Throwable;

class fechas{

    final public static function ayer(string $fecha = ''){
        $fecha = trim($fecha);
        if($fecha === ''){
            $fecha = date('Y-m-d');
        }
        $ayer = self::n_days_from_datetime($fecha,1,'-');
        if(error::$en_error){
            return (new error())->error('Error al obtener ayer',$ayer);
        }

        return $ayer;

    }

    private function dias_atras_sem(string $fecha_fin, $number_dia_ini_periodo)
    {
        $dia =  date('w', strtotime($fecha_fin));
        $dias_atras = $dia - $number_dia_ini_periodo;
        if($dias_atras < 0 ){
            $dias_atras = $dias_atras*-1;
            $dias_atras = 7 - $dias_atras;
        }

        return $dias_atras;

    }
    private function es_yyyy_mm_dd(string $fecha_fin)
    {
        $regex_yyyy_mm_dd = "/^20[2-3][0-9]-[0-1][0-9]-[0-3][0-9]$/";
        return preg_match($regex_yyyy_mm_dd, $fecha_fin);

    }
    private function es_yyyymmdd(string $fecha_fin)
    {
        $regex_yyyymmdd = "/^20[2-3][0-9][0-1][0-9][0-3][0-9]$/";
        return preg_match($regex_yyyymmdd, $fecha_fin);

    }

    private function format(string $fecha_fin)
    {
        $format = '';

        $es_yyyy_mm_dd = $this->es_yyyy_mm_dd($fecha_fin);
        if(error::$en_error){
            return (new error())->error('Error al verificar regex',$es_yyyy_mm_dd);
        }

        if($es_yyyy_mm_dd){
            $format = 'Y-m-d';
        }

        $es_yyyymmdd = $this->es_yyyymmdd($fecha_fin);
        if(error::$en_error){
            return (new error())->error('Error al verificar regex',$es_yyyymmdd);
        }

        if($es_yyyymmdd){
            $format = 'Ymd';
        }
        $format = trim($format);
        if($format === ''){
            return (new error())->error('Error la $format esta vacia',$format);
        }
        return $format;

    }
    final public function get_fecha_ini_periodo_sem(string $fecha_fin, int $number_dia_ini_periodo)
    {
        $fecha_fin = trim($fecha_fin);
        if($fecha_fin === ''){
            return (new error())->error('Error la fecha esta vacia',$fecha_fin);
        }
        $format = $this->format($fecha_fin);
        if(error::$en_error){
            return (new error())->error('Error al verificar $format',$format);
        }

        $dias_atras = $this->dias_atras_sem($fecha_fin,$number_dia_ini_periodo);
        if(error::$en_error){
            return (new error())->error('Error al verificar $dias_atras',$dias_atras);
        }

        return  date($format,strtotime($fecha_fin."- $dias_atras days"));
    }

    final public static function n_days_from_datetime(string $fecha, int $n_days = 1, string $operador = '+'){
        $dia = '';
        try {
            $datetime = new DateTime($fecha);
            $date = $datetime->format('Y-m-d');
            $date = date_create($date);
            $str = $operador.$n_days." days";
            date_add($date, date_interval_create_from_date_string($str));
            $dia = date_format($date, "Y-m-d");
        }
        catch (Throwable $e){
            return (new error())->error('Error al obtener fecha',$e);
        }



        return $dia;
    }
}
