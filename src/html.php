<?php
namespace desarrollo_em3\manejo_datos;




use desarrollo_em3\error\error;

class html{

    private function strong(string $etiqueta): string
    {
        return "<strong>$etiqueta : </strong>";
    }
    private function strong_value($etiqueta, string $value)
    {
        $strong = $this->strong($etiqueta);
        if(error::$en_error){
            return (new error())->error('Error al generar strong', $strong);
        }
        return $strong.$value;

    }

    final public function td_strong(int $colspan, string $etiqueta, string $value)
    {
        $strong = $this->strong_value($etiqueta, $value);
        if(error::$en_error){
            return (new error())->error('Error al generar strong', $strong);
        }
        $colspan_html = '';
        if($colspan > 1){
            $colspan_html = "colspan='$colspan'";
        }
        return "<td $colspan_html>$strong</td>";

    }

    
}
