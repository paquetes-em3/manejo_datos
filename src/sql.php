<?php
namespace desarrollo_em3\manejo_datos;


use desarrollo_em3\error\error;
use desarrollo_em3\error\valida;
use desarrollo_em3\manejo_datos\data\entidades\empresa;
use PDO;
use stdClass;

class sql{
    private array $types_sentencias = array('LIKE','IGUAL');

    /**
     * EM3
     * Genera una cadena `AND` condicionalmente si una consulta SQL no está vacía.
     *
     * Esta función verifica si la cadena `$sql` no está vacía y, en ese caso, devuelve `AND` con espacios
     * antes y después. Si la cadena está vacía, devuelve una cadena vacía.
     *
     * @param string $sql La cadena SQL a evaluar.
     *
     * @return string Devuelve `AND` si `$sql` no está vacía; de lo contrario, devuelve una cadena vacía.
     *
     * @example
     * ```php
     * // Ejemplo 1: Cadena no vacía
     * $sql = "status = 'activo'";
     * $and = $this->and($sql);
     * echo $and;
     * // Resultado:
     * // AND
     *
     * // Ejemplo 2: Cadena vacía
     * $sql = "";
     * $and = $this->and($sql);
     * echo $and;
     * // Resultado:
     * // (cadena vacía)
     *
     * // Ejemplo 3: Uso en la construcción de una consulta SQL
     * $sql = "status = 'activo'";
     * $and = $this->and($sql);
     * $final_query = "SELECT * FROM empleados WHERE " . $sql . $and . " edad > 30";
     * echo $final_query;
     * // Resultado:
     * // SELECT * FROM empleados WHERE status = 'activo' AND edad > 30
     * ```
     */
    private function and(string $sql): string
    {
        // Eliminar espacios alrededor de la cadena
        $sql = trim($sql);

        // Determinar si se debe agregar `AND`
        $and = '';
        if ($sql !== '') {
            $and = ' AND ';
        }

        // Retornar `AND` o cadena vacía
        return $and;
    }



    /**
     * REG
     * Genera una representación SQL de un campo con alias.
     *
     * Esta función toma el nombre de una entidad (tabla) y un campo específico dentro de la entidad,
     * y los combina en una sintaxis SQL con un alias que sigue el formato `{entidad}.{campo} AS {entidad}_{campo}`.
     *
     * Se incluyen validaciones para garantizar que los parámetros sean cadenas no vacías y no numéricas en el caso de la entidad.
     *
     * @param string $entidad     Nombre de la entidad (tabla) en la base de datos. Debe ser un texto válido y no numérico.
     * @param string $name_campo  Nombre del campo dentro de la entidad. Debe ser un texto válido y no numérico.
     *
     * @return string|array       Retorna la representación SQL del campo con alias si los parámetros son válidos.
     *                            En caso de error, retorna un array de error generado por la clase `error`.
     *
     * @throws error          Lanza una excepción si los parámetros son inválidos (se manejan mediante la clase `error`).
     *
     * @example
     * // Uso válido:
     * echo campo('usuarios', 'nombre');
     * // Salida: "usuarios.nombre AS usuarios_nombre"
     *
     * @example
     * // Uso con un nombre de campo numérico (incorrecto)
     * echo campo('usuarios', '123');
     * // Retorna un error: "Error $name_campo esta vacio"
     *
     * @example
     * // Uso con una entidad vacía (incorrecto)
     * echo campo('', 'nombre');
     * // Retorna un error: "Error $entidad esta vacio"
     *
     * @example
     * // Uso con un campo vacío (incorrecto)
     * echo campo('usuarios', '');
     * // Retorna un error: "Error $name_campo esta vacio"
     *
     * @example
     * // Uso con una entidad numérica (incorrecto)
     * echo campo('1234', 'nombre');
     * // Retorna un error: "Error $entidad debe ser un texto"
     */
    final public function campo(string $entidad, string $name_campo)
    {
        // Limpia el nombre de la entidad
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        // Limpia el nombre del campo
        $name_campo = trim($name_campo);
        if ($name_campo === '') {
            return (new error())->error('Error $name_campo esta vacio', $name_campo);
        }

        if (is_numeric($entidad)) {
            return (new error())->error('Error $entidad debe ser un texto', $entidad);
        }

        // Retorna la representación SQL del campo con alias
        return "$entidad.$name_campo AS " . $entidad . "_$name_campo";
    }




    /**
     * EM3
     * Agrega un campo base a un objeto estándar (`stdClass`) con información detallada de la entidad y el campo.
     *
     * Esta función toma un objeto de campos (`stdClass`), el nombre de una entidad, y el nombre de un campo. Luego,
     * agrega una nueva propiedad al objeto con una estructura que incluye la entidad y el nombre del campo.
     *
     * @param stdClass $campos Un objeto que contendrá los campos generados, con cada propiedad representando un campo.
     * @param string $entidad El nombre de la entidad (tabla) a la que pertenece el campo. No debe estar vacío.
     * @param string $name_campo El nombre del campo dentro de la entidad. No debe estar vacío.
     *
     * @return stdClass|array Devuelve el objeto `$campos` actualizado con el nuevo campo agregado. En caso de error,
     *                        devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Agregar un campo base al objeto
     * $campos = new stdClass();
     * $entidad = 'empleados';
     * $name_campo = 'nombre';
     * $resultado = $this->campo_base_obj($campos, $entidad, $name_campo);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [empleados_nombre] => stdClass Object (
     * //         [entidad] => 'empleados'
     * //         [name_campo] => 'nombre'
     * //     )
     * // )
     *
     * // Ejemplo 2: Entidad vacía (error)
     * $campos = new stdClass();
     * $entidad = '';
     * $name_campo = 'nombre';
     * $resultado = $this->campo_base_obj($campos, $entidad, $name_campo);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Nombre de campo vacío (error)
     * $campos = new stdClass();
     * $entidad = 'empleados';
     * $name_campo = '';
     * $resultado = $this->campo_base_obj($campos, $entidad, $name_campo);
     * // Resultado:
     * // ['error' => 'Error $name_campo esta vacio', 'data' => '']
     * ```
     */
    final public function campo_base_obj(stdClass $campos, string $entidad, string $name_campo)
    {
        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        // Validar que el nombre del campo no esté vacío
        $name_campo = trim($name_campo);
        if ($name_campo === '') {
            return (new error())->error('Error $name_campo esta vacio', $name_campo);
        }

        // Generar la clave para el nuevo campo
        $key = $entidad . '_' . $name_campo;

        // Agregar el campo al objeto
        $campos->$key = new stdClass();
        $campos->$key->entidad = $entidad;
        $campos->$key->name_campo = $name_campo;

        // Retornar el objeto actualizado
        return $campos;
    }


    /**
     * EM3
     * Genera una expresión SQL para manejar valores nulos en un campo, asignando 0 como valor por defecto.
     *
     * Esta función toma el nombre de un campo y una entidad, valida que no estén vacíos, y genera una
     * expresión SQL utilizando `COALESCE` para manejar valores nulos en el campo, retornando 0 en su lugar.
     *
     * @param string $campo El nombre del campo que se evaluará. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla) que contiene el campo. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la expresión `COALESCE`. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar expresión SQL válida
     * $campo = 'monto';
     * $entidad = 'pagos';
     * $resultado = $this->campo_nulo_0($campo, $entidad);
     * echo $resultado;
     * // Resultado:
     * // COALESCE(pagos.monto, 0)
     *
     * // Ejemplo 2: Entidad vacía (error)
     * $campo = 'monto';
     * $entidad = '';
     * $resultado = $this->campo_nulo_0($campo, $entidad);
     * // Resultado:
     * // ['error' => 'Error $entidad está vacío', 'data' => '']
     *
     * // Ejemplo 3: Campo vacío (error)
     * $campo = '';
     * $entidad = 'pagos';
     * $resultado = $this->campo_nulo_0($campo, $entidad);
     * // Resultado:
     * // ['error' => 'Error $campo está vacío', 'data' => '']
     * ```
     */
    final public function campo_nulo_0(string $campo, string $entidad)
    {
        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad está vacío', $entidad);
        }

        // Validar que el campo no esté vacío
        $campo = trim($campo);
        if ($campo === '') {
            return (new error())->error('Error $campo está vacío', $campo);
        }

        // Retornar la expresión SQL con COALESCE
        return "COALESCE($entidad.$campo, 0)";
    }



    /**
     * EM3
     * Genera una declaración SQL para un campo con un alias basado en su nombre.
     *
     * Esta función valida que el nombre del campo no esté vacío, y luego genera una
     * declaración SQL que incluye un alias. El alias reemplaza los puntos (`.`) en el nombre
     * del campo con guiones bajos (`_`).
     *
     * @param string $campo El nombre del campo a procesar. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena con la declaración SQL del campo con alias.
     *                      Si `$campo` está vacío, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar un campo SQL con alias
     * $campo = "tabla.campo";
     * $resultado = $this->campo_sql($campo);
     * echo $resultado;
     * // Resultado:
     * // tabla.campo AS tabla_campo
     *
     * // Ejemplo 2: Campo vacío (error)
     * $campo = "";
     * $resultado = $this->campo_sql($campo);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Uso en una consulta SQL
     * $campos = [
     *     $this->campo_sql("tabla.campo1"),
     *     $this->campo_sql("tabla.campo2")
     * ];
     * $sql = "SELECT " . implode(", ", $campos) . " FROM tabla";
     * echo $sql;
     * // Resultado:
     * // SELECT tabla.campo1 AS tabla_campo1, tabla.campo2 AS tabla_campo2 FROM tabla
     * ```
     */
    final public function campo_sql(string $campo)
    {
        // Eliminar espacios en blanco alrededor del nombre del campo
        $campo = trim($campo);

        // Validar que el campo no esté vacío
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        // Generar el alias reemplazando los puntos por guiones bajos
        $campo_rename = str_replace('.', '_', $campo);

        // Retornar la declaración SQL con alias
        return trim($campo) . ' AS ' . $campo_rename;
    }


    /**
     * EM3
     * Genera una expresión SQL para calcular la suma de un campo dentro de una entidad.
     *
     * Esta función valida los parámetros de entrada y construye una expresión SQL utilizando la función `SUM`,
     * con el formato `SUM(entidad.campo)`.
     *
     * @param string $campo El nombre del campo que será sumado. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla) que contiene el campo. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la expresión `SUM(entidad.campo)`. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar expresión SUM válida
     * $campo = 'monto';
     * $entidad = 'pagos';
     * $resultado = $this->campo_sum($campo, $entidad);
     * echo $resultado;
     * // Resultado:
     * // SUM(pagos.monto)
     *
     * // Ejemplo 2: Error por campo vacío
     * $campo = '';
     * $entidad = 'pagos';
     * $resultado = $this->campo_sum($campo, $entidad);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por entidad vacía
     * $campo = 'monto';
     * $entidad = '';
     * $resultado = $this->campo_sum($campo, $entidad);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacio', 'data' => '']
     * ```
     */
    private function campo_sum(string $campo, string $entidad)
    {
        // Validar que el campo no esté vacío
        $campo = trim($campo);
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        // Retornar la expresión SQL de suma
        return "SUM($entidad.$campo)";
    }


    /**
     * EM3
     * Genera una expresión SQL para sumar un campo y asignarle un alias (`AS`).
     *
     * Esta función valida que el nombre del campo no esté vacío, genera una expresión SQL con
     * la función `SUM` aplicada al campo, y asigna un alias para el resultado. Si no se especifica
     * un alias (`$rename`), se genera automáticamente como `<campo>_suma`.
     *
     * @param string $campo El nombre del campo a sumar. No debe estar vacío.
     * @param string $rename (Opcional) El alias que se asignará al resultado de la suma. Si está vacío,
     *                       se genera como `<campo>_suma`.
     *
     * @return string|array Devuelve una cadena SQL con la expresión de suma y el alias. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar suma con un alias específico
     * $campo = 'ventas';
     * $rename = 'total_ventas';
     * $resultado = $this->campo_sum_base($campo, $rename);
     * echo $resultado;
     * // Resultado:
     * // SUM(ventas) AS total_ventas
     *
     * // Ejemplo 2: Generar suma con un alias automático
     * $campo = 'ventas';
     * $rename = '';
     * $resultado = $this->campo_sum_base($campo, $rename);
     * echo $resultado;
     * // Resultado:
     * // SUM(ventas) AS ventas_suma
     *
     * // Ejemplo 3: Campo vacío (error)
     * $campo = '';
     * $rename = 'total_ventas';
     * $resultado = $this->campo_sum_base($campo, $rename);
     * // Resultado:
     * // ['error' => 'Error campo esta vacio', 'data' => '']
     * ```
     */
    final public function campo_sum_base(string $campo, string $rename)
    {
        // Validar que el campo no esté vacío
        $campo = trim($campo);
        if ($campo === '') {
            return (new error())->error('Error campo esta vacio', $campo);
        }

        // Validar y asignar un alias predeterminado si $rename está vacío
        $rename = trim($rename);
        if ($rename === '') {
            $rename = $campo . '_suma';
        }

        // Retornar la expresión SQL para la suma
        return "SUM($campo) AS $rename";
    }


    /**
     * EM3
     * Genera una expresión SQL para calcular la suma de un campo dentro de una entidad, manejando valores nulos.
     *
     * Esta función valida los parámetros de entrada y construye una expresión SQL utilizando la función `SUM`
     * combinada con `IFNULL`, devolviendo 0 si el resultado de la suma es nulo.
     *
     * @param string $campo El nombre del campo que será sumado. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla) que contiene el campo. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la expresión `IFNULL(SUM(entidad.campo), 0)`. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar expresión SUM con manejo de nulos
     * $campo = 'monto';
     * $entidad = 'pagos';
     * $resultado = $this->campo_sum_null($campo, $entidad);
     * echo $resultado;
     * // Resultado:
     * // IFNULL(SUM(pagos.monto), 0)
     *
     * // Ejemplo 2: Error por campo vacío
     * $campo = '';
     * $entidad = 'pagos';
     * $resultado = $this->campo_sum_null($campo, $entidad);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por entidad vacía
     * $campo = 'monto';
     * $entidad = '';
     * $resultado = $this->campo_sum_null($campo, $entidad);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacio', 'data' => '']
     * ```
     */
    private function campo_sum_null(string $campo, string $entidad)
    {
        // Validar que el campo no esté vacío
        $campo = trim($campo);
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        // Generar la expresión SUM
        $sum_doc_total = $this->campo_sum($campo, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $sum_doc_total);
        }

        // Retornar la expresión SQL con manejo de nulos
        return "IFNULL($sum_doc_total, 0)";
    }


    /**
     * EM3
     * Genera una expresión SQL para calcular la suma de un campo, manejar valores nulos, y asignar un alias al resultado.
     *
     * Esta función valida los parámetros de entrada, genera una expresión SQL con `SUM` y `IFNULL` para manejar valores nulos,
     * y asigna un alias al resultado utilizando el formato `AS`.
     *
     * @param string $alias El alias que se asignará al resultado de la suma. No debe estar vacío.
     * @param string $campo El nombre del campo que será sumado. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla) que contiene el campo. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la expresión `SUM(entidad.campo) AS alias`. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar expresión SUM con alias
     * $alias = 'suma_monto';
     * $campo = 'monto';
     * $entidad = 'pagos';
     * $resultado = $this->campo_sum_rename($alias, $campo, $entidad);
     * echo $resultado;
     * // Resultado:
     * // IFNULL(SUM(pagos.monto), 0) AS 'suma_monto'
     *
     * // Ejemplo 2: Error por alias vacío
     * $alias = '';
     * $campo = 'monto';
     * $entidad = 'pagos';
     * $resultado = $this->campo_sum_rename($alias, $campo, $entidad);
     * // Resultado:
     * // ['error' => 'Error al validar datos de entrada', 'data' => [...]]
     *
     * // Ejemplo 3: Error por campo vacío
     * $alias = 'suma_monto';
     * $campo = '';
     * $entidad = 'pagos';
     * $resultado = $this->campo_sum_rename($alias, $campo, $entidad);
     * // Resultado:
     * // ['error' => 'Error al validar datos de entrada', 'data' => [...]]
     *
     * // Ejemplo 4: Error por entidad vacía
     * $alias = 'suma_monto';
     * $campo = 'monto';
     * $entidad = '';
     * $resultado = $this->campo_sum_rename($alias, $campo, $entidad);
     * // Resultado:
     * // ['error' => 'Error al validar datos de entrada', 'data' => [...]]
     * ```
     */
    final public function campo_sum_rename(string $alias, string $campo, string $entidad)
    {
        // Validar los datos de entrada
        $valida = (new sql())->valida_entrada_rename($alias, $campo, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al validar datos de entrada', $valida);
        }

        // Generar la expresión SUM con manejo de nulos
        $sum_doc_total = $this->campo_sum_null($campo, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener sql', $sum_doc_total);
        }

        // Retornar la expresión SQL con alias
        return "$sum_doc_total AS '$alias'";
    }

    final public function campos_alias(array $campos)
    {
        $campos_sql = '';
        foreach ($campos as $entidad=>$campo_entidad) {
            $entidad = trim($entidad);
            if($entidad === '') {
                return (new error())->error('Error $entidad esta vacio', $entidad);
            }
            if(is_numeric($entidad)){
                return (new error())->error('Error $entidad debe ser un texto', $entidad);
            }
            if(!is_array($campo_entidad)) {
                return (new error())->error('Error $campo_entidad debe ser un array', $campo_entidad);
            }
            foreach ($campo_entidad as $campo) {

                $campo = trim($campo);
                if($campo === '') {
                    return (new error())->error('Error $campo esta vacio', $campo);
                }

                $campo_sql = (new sql())->campo($entidad, $campo);
                if (error::$en_error) {
                    return (new error())->error('Error al generar campo', $campo_sql);
                }

                $coma = '';
                if ($campos_sql !== '') {
                    $coma = ', ';
                }
                $campos_sql .= trim($coma . $campo_sql);

            }
        }
        return $campos_sql;

    }


    /**
     * EM3
     * Genera una cadena SQL que lista campos adicionales con sus alias y separadores.
     *
     * Esta función utiliza `campos_sql_base` para procesar un array de campos adicionales,
     * valida si se necesita una coma como separador, y devuelve la lista de campos SQL
     * formateados con alias y separadores.
     *
     * @param array $campos_extra Un array de nombres de campos adicionales a incluir en la consulta SQL.
     *
     * @return string|array Devuelve una cadena con los campos formateados para SQL, incluyendo alias
     *                      y separadores (comas). En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar campos SQL válidos
     * $campos_extra = ['tabla.campo1', 'tabla.campo2', 'tabla.campo3'];
     * $resultado = $this->campos_extra_sql($campos_extra);
     * echo $resultado;
     * // Resultado:
     * // , tabla.campo1 AS tabla_campo1, tabla.campo2 AS tabla_campo2, tabla.campo3 AS tabla_campo3
     *
     * // Ejemplo 2: Campo vacío en el array (error)
     * $campos_extra = ['tabla.campo1', '', 'tabla.campo3'];
     * $resultado = $this->campos_extra_sql($campos_extra);
     * // Resultado:
     * // ['error' => 'Error obtener $campos_sql', 'data' => [...]]
     *
     * // Ejemplo 3: Array vacío
     * $campos_extra = [];
     * $resultado = $this->campos_extra_sql($campos_extra);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     * ```
     */
    private function campos_extra_sql(array $campos_extra)
    {
        // Generar la lista base de campos SQL
        $campos_sql = $this->campos_sql_base($campos_extra);
        if (error::$en_error) {
            return (new error())->error('Error obtener $campos_sql', $campos_sql);
        }

        // Determinar si se necesita una coma como separador
        $coma = $this->coma($campos_sql);
        if (error::$en_error) {
            return (new error())->error('Error obtener coma', $coma);
        }

        // Retornar la cadena de campos SQL formateada con alias y separadores
        return trim($coma . $campos_sql);
    }


    /**
     * EM3
     * Genera un conjunto de campos SQL con alias basados en una configuración proporcionada.
     *
     * Esta función toma un objeto que define los campos y genera representaciones SQL con alias
     * utilizando el método `campo`. Si un campo tiene una definición SQL personalizada, se utiliza esa
     * definición en lugar de la generada automáticamente.
     *
     * @param stdClass $campos Un objeto donde cada clave es el nombre de la variable en el resultado, y
     *                         cada valor es otro objeto con los siguientes atributos:
     *                         - `entidad` (string): El nombre de la entidad (tabla) a la que pertenece el campo.
     *                         - `name_campo` (string): El nombre del campo en la entidad.
     *                         - `sql` (string, opcional): Una definición SQL personalizada para el campo.
     *
     * @return stdClass|array Devuelve un objeto donde cada clave es el nombre de la variable en `$campos` y su valor
     *                        es la representación SQL generada o personalizada. En caso de error, devuelve un array
     *                        con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar campos SQL con alias
     * $campos = (object)[
     *     'nombre_completo' => (object)[
     *         'entidad' => 'empleados',
     *         'name_campo' => 'nombre'
     *     ],
     *     'fecha_ingreso' => (object)[
     *         'entidad' => 'empleados',
     *         'name_campo' => 'fecha_ingreso'
     *     ]
     * ];
     * $resultado = $this->campos_sql($campos);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [nombre_completo] => empleados.nombre AS empleados_nombre
     * //     [fecha_ingreso] => empleados.fecha_ingreso AS empleados_fecha_ingreso
     * // )
     *
     * // Ejemplo 2: Campo con definición SQL personalizada
     * $campos = (object)[
     *     'total_sueldo' => (object)[
     *         'entidad' => 'empleados',
     *         'name_campo' => 'sueldo',
     *         'sql' => 'SUM(empleados.sueldo) AS total_sueldo'
     *     ]
     * ];
     * $resultado = $this->campos_sql($campos);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [total_sueldo] => SUM(empleados.sueldo) AS total_sueldo
     * // )
     *
     * // Ejemplo 3: Error en definición de campo
     * $campos = (object)[
     *     'nombre_completo' => (object)[
     *         'entidad' => '',
     *         'name_campo' => 'nombre'
     *     ]
     * ];
     * $resultado = $this->campos_sql($campos);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     * ```
     */
    final public function campos_sql(stdClass $campos)
    {
        // Objeto que contendrá los campos generados en formato SQL
        $campos_sql = new stdClass();

        foreach ($campos as $name_var => $params) {
            // Valida la existencia de 'entidad' y 'name_campo' en los parámetros
            if (!isset($params->entidad)) {
                return (new error())->error('Error $params->entidad no existe', $params);
            }
            if (!isset($params->name_campo)) {
                return (new error())->error('Error $params->name_campo no existe', $params);
            }

            // Limpia y valida los valores de 'entidad' y 'name_campo'
            $entidad = trim($params->entidad);
            if ($entidad === '') {
                return (new error())->error('Error $entidad esta vacia', $entidad);
            }
            $name_campo = trim($params->name_campo);
            if ($name_campo === '') {
                return (new error())->error('Error $name_campo esta vacia', $name_campo);
            }

            // Genera el campo SQL utilizando el método 'campo'
            $var = $this->campo($entidad, $name_campo);
            if (error::$en_error) {
                return (new error())->error('Error al integrar $var', $var);
            }

            // Si se define una cadena SQL personalizada, se utiliza en lugar del campo generado
            if (isset($params->sql) && trim($params->sql) !== '') {
                $var = trim($params->sql);
            }

            // Agrega el campo SQL al objeto de resultado
            $campos_sql->$name_var = $var;
        }

        // Devuelve el conjunto de campos SQL generados
        return $campos_sql;
    }



    /**
     * EM3
     * Genera una cadena SQL que lista campos adicionales con sus alias y separadores.
     *
     * Esta función procesa un array de campos adicionales, valida que cada campo no esté vacío,
     * genera declaraciones SQL para cada campo utilizando alias, y las concatena con comas para
     * formar una lista de campos SQL lista para usar en consultas.
     *
     * @param array $campos_extra Un array de nombres de campos adicionales a incluir en la consulta SQL.
     *
     * @return string|array Devuelve una cadena con los campos formateados para SQL, incluyendo sus alias
     *                      y separadores (comas). En caso de error, devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar campos SQL válidos
     * $campos_extra = ['tabla.campo1', 'tabla.campo2', 'tabla.campo3'];
     * $resultado = $this->campos_sql_base($campos_extra);
     * echo $resultado;
     * // Resultado:
     * // tabla.campo1 AS tabla_campo1, tabla.campo2 AS tabla_campo2, tabla.campo3 AS tabla_campo3
     *
     * // Ejemplo 2: Campo vacío en el array (error)
     * $campos_extra = ['tabla.campo1', '', 'tabla.campo3'];
     * $resultado = $this->campos_sql_base($campos_extra);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Array vacío
     * $campos_extra = [];
     * $resultado = $this->campos_sql_base($campos_extra);
     * // Resultado:
     * // (cadena vacía)
     * ```
     */
    private function campos_sql_base(array $campos_extra)
    {
        // Inicializar la cadena de campos SQL
        $campos_sql = '';

        // Recorrer cada campo adicional
        foreach ($campos_extra as $campo) {
            // Eliminar espacios alrededor del campo
            $campo = trim($campo);

            // Validar que el campo no esté vacío
            if ($campo === '') {
                return (new error())->error('Error $campo esta vacio', $campo);
            }

            // Generar parámetros para el campo SQL
            $param = $this->param_campo_sql($campo, $campos_sql);
            if (error::$en_error) {
                return (new error())->error('Error obtener $param', $param);
            }

            // Concatenar el campo SQL con el separador
            $campos_sql .= $param->coma . $param->campo_sql;
        }

        // Retornar la cadena de campos SQL
        return $campos_sql;
    }


    /**
     * EM3
     * Genera una lista de columnas SQL separadas por comas para una entidad, con alias configurados.
     *
     * Esta función recorre un objeto de campos, valida que cada campo tenga un alias (`rename`),
     * y construye dinámicamente una lista de columnas SQL en formato `entidad.campo AS alias`.
     *
     * @param stdClass $campos Un objeto donde:
     *                         - Las claves son los nombres de los campos.
     *                         - Los valores son objetos que contienen la propiedad `rename`, que representa el alias del campo.
     * @param string $name_entidad El nombre de la entidad (tabla) a la que pertenecen los campos. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con las columnas generadas separadas por comas.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar columnas SQL con alias
     * $campos = (object)[
     *     'id' => (object)['rename' => 'empleado_id'],
     *     'nombre' => (object)['rename' => 'empleado_nombre'],
     *     'apellido' => (object)['rename' => 'empleado_apellido']
     * ];
     * $name_entidad = 'empleados';
     * $resultado = $this->campos_sql_coma($campos, $name_entidad);
     * echo $resultado;
     * // Resultado:
     * // empleados.id AS empleado_id, empleados.nombre AS empleado_nombre, empleados.apellido AS empleado_apellido
     *
     * // Ejemplo 2: Error por entidad vacía
     * $name_entidad = '';
     * $resultado = $this->campos_sql_coma($campos, $name_entidad);
     * // Resultado:
     * // ['error' => 'Error $name_entidad esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por campo sin alias
     * $campos = (object)[
     *     'id' => (object)['rename' => 'empleado_id'],
     *     'nombre' => (object)[] // Falta 'rename'
     * ];
     * $name_entidad = 'empleados';
     * $resultado = $this->campos_sql_coma($campos, $name_entidad);
     * // Resultado:
     * // ['error' => 'Error $data->rename no existe', 'data' => [...]]
     * ```
     */
    final public function campos_sql_coma(stdClass $campos, string $name_entidad)
    {
        // Validar que el nombre de la entidad no esté vacío
        $name_entidad = trim($name_entidad);
        if ($name_entidad === '') {
            return (new error())->error('Error $name_entidad esta vacio', $name_entidad);
        }

        // Inicializar la cadena SQL
        $sql = '';

        // Recorrer los campos y generar las columnas con alias
        foreach ($campos as $campo => $data) {
            // Validar que exista la propiedad 'rename' en el objeto del campo
            if (!isset($data->rename)) {
                return (new error())->error('Error $data->rename no existe', $campos);
            }

            // Validar que el alias no esté vacío
            $rename = trim($data->rename);
            if ($rename === '') {
                return (new error())->error('Error rename esta vacio', $campos);
            }

            // Obtener la coma separadora si es necesario
            $coma = (new sql())->coma($sql);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $coma', $coma);
            }

            // Concatenar el campo con su alias
            $sql .= $coma . $name_entidad . '.' . $campo . ' AS ' . $data->rename;
        }

        // Retornar la cadena SQL generada
        return $sql;
    }


    /**
     * EM3
     * Combina una lista de campos SQL en una sola cadena separada por comas.
     *
     * Esta función recorre un objeto de campos SQL (`stdClass`), valida que cada campo no esté vacío,
     * y los combina en una cadena separada por comas, agregando automáticamente la coma cuando sea necesario.
     *
     * @param stdClass $campos_rs Un objeto que contiene los campos SQL a combinar. Cada propiedad debe ser una cadena válida.
     *
     * @return string|array Devuelve una cadena con los campos SQL combinados y separados por comas. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Combinar campos SQL válidos
     * $campos_rs = (object)[
     *     'campo1' => 'pago.id AS pago_id',
     *     'campo2' => 'contrato.folio AS contrato_folio',
     *     'campo3' => '(SELECT serie.codigo FROM serie WHERE ...) AS serie_codigo'
     * ];
     * $resultado = $this->campos_sql_coma_bruto($campos_rs);
     * echo $resultado;
     * // Resultado:
     * // pago.id AS pago_id, contrato.folio AS contrato_folio, (SELECT serie.codigo FROM serie WHERE ...) AS serie_codigo
     *
     * // Ejemplo 2: Campo vacío (error)
     * $campos_rs = (object)[
     *     'campo1' => 'pago.id AS pago_id',
     *     'campo2' => ''
     * ];
     * $resultado = $this->campos_sql_coma_bruto($campos_rs);
     * // Resultado:
     * // ['error' => 'Error $campo no puede venir vaciado', 'data' => '']
     * ```
     */
    final public function campos_sql_coma_bruto(stdClass $campos_rs)
    {
        // Inicializar la cadena SQL
        $campos_sql = '';

        // Recorrer los campos proporcionados
        foreach ($campos_rs as $campo) {
            // Validar que el campo no esté vacío
            $campo = trim($campo);
            if ($campo === '') {
                return (new error())->error('Error $campo no puede venir vaciado', $campo);
            }

            // Obtener la coma para separar los campos
            $coma = $this->coma($campos_sql);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $coma', $coma);
            }

            // Concatenar el campo con la coma
            $campos_sql .= $coma . $campo;
        }

        // Retornar la cadena SQL generada
        return $campos_sql;
    }


    /**
     * EM3
     * Genera una cadena SQL con una lista de campos separados por comas.
     *
     * Esta función toma un array de nombres de campos, valida que no estén vacíos, y construye
     * una cadena SQL concatenando los campos con comas como separadores.
     *
     * @param array $campos Un array de nombres de campos. Cada elemento debe ser un string no vacío.
     *
     * @return string|array Devuelve una cadena con los nombres de los campos separados por comas.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar una cadena válida
     * $campos = ['campo1', 'campo2', 'campo3'];
     * $resultado = $this->campos_sql_puros($campos);
     * echo $resultado;
     * // Resultado:
     * // campo1, campo2, campo3
     *
     * // Ejemplo 2: Campo vacío en el array (error)
     * $campos = ['campo1', '', 'campo3'];
     * $resultado = $this->campos_sql_puros($campos);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => [...]]
     *
     * // Ejemplo 3: Array vacío
     * $campos = [];
     * $resultado = $this->campos_sql_puros($campos);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     * ```
     */
    final public function campos_sql_puros(array $campos)
    {
        // Inicializar la cadena para los campos SQL
        $campos_sql = '';

        // Recorrer cada campo del array
        foreach ($campos as $campo) {
            // Eliminar espacios alrededor del nombre del campo
            $campo = trim($campo);

            // Validar que el campo no esté vacío
            if ($campo === '') {
                return (new error())->error('Error $campo esta vacio', $campos);
            }

            // Determinar si se debe agregar una coma como separador
            $coma = $this->coma($campos_sql);
            if (error::$en_error) {
                return (new error())->error('Error al integrar coma', $coma);
            }

            // Concatenar el campo a la cadena
            $campos_sql .= $coma . $campo;
        }

        // Retornar la cadena de campos SQL
        return $campos_sql;
    }


    /**
     * EM3
     * Genera una lista de columnas SQL con alias (`AS`), separadas por comas.
     *
     * Esta función recorre un arreglo de columnas, valida que cada columna no esté vacía, y genera
     * una lista de columnas SQL en el formato `campo AS alias`, donde el alias reemplaza los puntos (`.`)
     * en el nombre del campo por guiones bajos (`_`).
     *
     * @param array $columnas Un arreglo de nombres de columnas en formato `tabla.campo`.
     *
     * @return string|array Devuelve una cadena SQL con las columnas generadas separadas por comas.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar columnas SQL con alias
     * $columnas = ['tabla.campo1', 'tabla.campo2', 'otra_tabla.campo3'];
     * $resultado = $this->campos_sql_puros_with_as($columnas);
     * echo $resultado;
     * // Resultado:
     * // tabla.campo1 AS tabla_campo1, tabla.campo2 AS tabla_campo2, otra_tabla.campo3 AS otra_tabla_campo3
     *
     * // Ejemplo 2: Error por columna vacía
     * $columnas = ['tabla.campo1', ''];
     * $resultado = $this->campos_sql_puros_with_as($columnas);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     * ```
     */
    final public function campos_sql_puros_with_as(array $columnas)
    {
        // Inicializar la cadena SQL
        $campos_sql = '';

        // Recorrer las columnas
        foreach ($columnas as $campo) {
            $campo = trim($campo);

            // Validar que la columna no esté vacía
            if ($campo === '') {
                return (new error())->error('Error $campo esta vacio', $campo);
            }

            // Generar el alias para la columna
            $campo_sql = $campo . ' AS ' . str_replace('.', '_', $campo);

            // Obtener la coma separadora si es necesario
            $coma = (new sql())->coma($campos_sql);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $coma', $coma);
            }

            // Concatenar la columna SQL con alias
            $campos_sql .= $coma . $campo_sql;
        }

        // Retornar la cadena SQL generada
        return $campos_sql;
    }


    /**
     * EM3
     * Genera una cadena de campos SQL, asociando cada campo con una entidad y manejando automáticamente las separaciones con comas.
     *
     * Esta función recorre un arreglo de campos, valida que no estén vacíos, y construye una lista de campos SQL en formato `entidad.campo`.
     * También maneja las comas necesarias para separar múltiples campos en la lista.
     *
     * @param array $campos Un arreglo con los nombres de los campos. Ningún campo puede estar vacío.
     * @param string $entidad El nombre de la entidad o tabla asociada a los campos. No puede estar vacío.
     *
     * @return string|array Devuelve una cadena de campos SQL en el formato `entidad.campo, entidad.campo`.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar una lista de campos SQL
     * $campos = ['id', 'nombre', 'email'];
     * $entidad = 'usuario';
     * $resultado = $this->campos_sql_string($campos, $entidad);
     * echo $resultado;
     * // Resultado:
     * // usuario.id, usuario.nombre, usuario.email
     *
     * // Ejemplo 2: Error por entidad vacía
     * $campos = ['id', 'nombre', 'email'];
     * $entidad = '';
     * $resultado = $this->campos_sql_string($campos, $entidad);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por campo vacío
     * $campos = ['id', '', 'email'];
     * $entidad = 'usuario';
     * $resultado = $this->campos_sql_string($campos, $entidad);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     * ```
     */
    final public function campos_sql_string(array $campos, string $entidad)
    {
        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        // Inicializar la lista de campos SQL
        $campos_sql = '';

        // Recorrer cada campo en el arreglo
        foreach ($campos as $campo) {
            $campo = trim($campo);

            // Validar que el campo no esté vacío
            if ($campo === '') {
                return (new error())->error('Error $campo esta vacio', $campo);
            }

            // Obtener los parámetros para el campo
            $params = $this->params_campo($campo, $campos_sql, $entidad);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $params', $params);
            }

            // Agregar el campo a la lista SQL
            $campos_sql .= $params->coma . $params->campo_sql;
        }

        // Retornar la lista de campos SQL generada
        return $campos_sql;
    }


    /**
     * EM3
     * Genera una lista de columnas SQL para múltiples tablas, con alias en formato `entidad.campo`.
     *
     * Esta función recorre un objeto `stdClass` que contiene configuraciones de columnas,
     * valida las propiedades necesarias (`name_entidad` y `name_campo`), y construye dinámicamente
     * una lista de columnas SQL separadas por comas.
     *
     * @param stdClass $columnas Un objeto donde:
     *                           - Cada propiedad es un conjunto de parámetros para una columna.
     *                           - Cada conjunto debe incluir:
     *                             - `name_entidad`: El nombre de la entidad (tabla).
     *                             - `name_campo`: El nombre del campo dentro de la tabla.
     *
     * @return string|array Devuelve una cadena SQL con las columnas generadas separadas por comas.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar columnas SQL para múltiples tablas
     * $columnas = (object)[
     *     'sala_usuario_id' => (object)['name_entidad' => 'sala_usuario', 'name_campo' => 'id'],
     *     'plaza_id' => (object)['name_entidad' => 'plaza', 'name_campo' => 'id']
     * ];
     * $resultado = $this->campos_sql_string_multi_table($columnas);
     * echo $resultado;
     * // Resultado:
     * // sala_usuario.id AS sala_usuario_id, plaza.id AS plaza_id
     *
     * // Ejemplo 2: Error por columna mal configurada
     * $columnas = (object)[
     *     'sala_usuario_id' => (object)['name_entidad' => 'sala_usuario'], // Falta name_campo
     * ];
     * $resultado = $this->campos_sql_string_multi_table($columnas);
     * // Resultado:
     * // ['error' => 'Error $params->name_campo no existe', 'data' => {...}]
     * ```
     */
    final public function campos_sql_string_multi_table(stdClass $columnas)
    {
        // Inicializar la cadena SQL
        $campos_sql = '';

        // Recorrer las configuraciones de columnas
        foreach ($columnas as $params) {
            // Validar que los parámetros sean un objeto
            if (!is_object($params)) {
                return (new error())->error('Error $params debe ser un objeto', $params);
            }

            // Validar que exista `name_entidad` en los parámetros
            if (!isset($params->name_entidad)) {
                return (new error())->error('Error $params->name_entidad no existe', $params);
            }

            // Validar que exista `name_campo` en los parámetros
            if (!isset($params->name_campo)) {
                return (new error())->error('Error $params->name_campo no existe', $params);
            }

            // Limpiar y validar el nombre de la entidad
            $entidad = trim($params->name_entidad);
            if ($entidad === '') {
                return (new error())->error('Error $entidad esta vacio', $params);
            }

            // Limpiar y validar el nombre del campo
            $name_campo = trim($params->name_campo);
            if ($name_campo === '') {
                return (new error())->error('Error $name_campo esta vacio', $params);
            }

            // Construir la representación SQL del campo
            $campo_sql = $this->campo($entidad, $name_campo);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $campo_sql', $campo_sql);
            }

            // Agregar coma si no es la primera columna
            $coma = $this->coma($campos_sql);
            if (error::$en_error) {
                return (new error())->error('Error al obtener coma', $coma);
            }

            // Concatenar el campo a la cadena SQL
            $campos_sql .= $coma . $campo_sql;
        }

        // Retornar la cadena SQL generada
        return $campos_sql;
    }


    /**
     * EM3
     * Obtiene el valor de la propiedad `columna` de un objeto `stdClass` si existe, o devuelve una cadena vacía.
     *
     * Esta función verifica si la propiedad `columna` está definida en el objeto `$replaces_obj`. Si existe,
     * retorna su valor; de lo contrario, devuelve una cadena vacía.
     *
     * @param stdClass $replaces_obj Un objeto que puede contener la propiedad `columna`.
     *
     * @return string Devuelve el valor de la propiedad `columna` si está definida, o una cadena vacía si no.
     *
     * @example
     * ```php
     * // Ejemplo 1: Objeto con la propiedad `columna`
     * $replaces_obj = (object)[
     *     'columna' => 'tabla1.nombre'
     * ];
     * $resultado = $this->columna_compare($replaces_obj);
     * echo $resultado;
     * // Resultado:
     * // tabla1.nombre
     *
     * // Ejemplo 2: Objeto sin la propiedad `columna`
     * $replaces_obj = (object)[
     *     'valor' => 'tabla1.id'
     * ];
     * $resultado = $this->columna_compare($replaces_obj);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     *
     * // Ejemplo 3: Objeto vacío
     * $replaces_obj = new stdClass();
     * $resultado = $this->columna_compare($replaces_obj);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     * ```
     */
    private function columna_compare(stdClass $replaces_obj): string
    {
        // Inicializar la variable de retorno
        $columna_compare = '';

        // Verificar si la propiedad `columna` está definida en el objeto
        if (isset($replaces_obj->columna)) {
            $columna_compare = $replaces_obj->columna;
        }

        // Retornar el valor de `columna` o una cadena vacía
        return $columna_compare;
    }


    /**
     * EM3
     * Obtiene el valor de la propiedad `columna_replace` de un objeto `stdClass` si existe, o devuelve una cadena vacía.
     *
     * Esta función verifica si la propiedad `columna_replace` está definida en el objeto `$replaces_obj`. Si existe,
     * retorna su valor; de lo contrario, devuelve una cadena vacía.
     *
     * @param stdClass $replaces_obj Un objeto que puede contener la propiedad `columna_replace`.
     *
     * @return string Devuelve el valor de la propiedad `columna_replace` si está definida, o una cadena vacía si no.
     *
     * @example
     * ```php
     * // Ejemplo 1: Objeto con la propiedad `columna_replace`
     * $replaces_obj = (object)[
     *     'columna_replace' => 'tabla2.nombre'
     * ];
     * $resultado = $this->columna_rs($replaces_obj);
     * echo $resultado;
     * // Resultado:
     * // tabla2.nombre
     *
     * // Ejemplo 2: Objeto sin la propiedad `columna_replace`
     * $replaces_obj = (object)[
     *     'columna' => 'tabla1.nombre'
     * ];
     * $resultado = $this->columna_rs($replaces_obj);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     *
     * // Ejemplo 3: Objeto vacío
     * $replaces_obj = new stdClass();
     * $resultado = $this->columna_rs($replaces_obj);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     * ```
     */
    private function columna_rs(stdClass $replaces_obj): string
    {
        // Inicializar la variable de retorno
        $columna_rs = '';

        // Verificar si la propiedad `columna_replace` está definida en el objeto
        if (isset($replaces_obj->columna_replace)) {
            $columna_rs = $replaces_obj->columna_replace;
        }

        // Retornar el valor de `columna_replace` o una cadena vacía
        return $columna_rs;
    }


    /**
     * EM3
     * Genera una coma (`,`) condicionalmente si una cadena SQL no está vacía.
     *
     * Esta función verifica si la cadena `$sql` no está vacía y, en ese caso, retorna una coma (`,`).
     * Si la cadena está vacía, retorna una cadena vacía. Es útil para construir dinámicamente
     * consultas SQL con separadores condicionales.
     *
     * @param string $sql La cadena SQL a evaluar.
     *
     * @return string Devuelve una coma (`,`) si `$sql` no está vacía; de lo contrario, devuelve una cadena vacía.
     *
     * @example
     * ```php
     * // Ejemplo 1: Cadena no vacía
     * $sql = "SELECT * FROM empleados";
     * $coma = $this->coma($sql);
     * echo $coma;
     * // Resultado:
     * // ,
     *
     * // Ejemplo 2: Cadena vacía
     * $sql = "";
     * $coma = $this->coma($sql);
     * echo $coma;
     * // Resultado:
     * // (cadena vacía)
     *
     * // Ejemplo 3: Uso en la construcción de una consulta SQL
     * $campos = "nombre, edad";
     * $sql = "SELECT $campos";
     * $coma = $this->coma($campos);
     * $sql .= $coma . " salario";
     * echo $sql;
     * // Resultado:
     * // SELECT nombre, edad, salario
     * ```
     */
    final public function coma(string $sql): string
    {
        // Eliminar espacios en blanco alrededor de la cadena SQL
        $sql = trim($sql);

        // Retornar una coma si la cadena no está vacía; de lo contrario, retornar cadena vacía
        $coma = '';
        if ($sql !== '') {
            $coma = ',';
        }
        return $coma;
    }


    /**
     * EM3
     * Genera la lista de columnas SQL para una consulta `SELECT`.
     *
     * Este método procesa un objeto de columnas (`$columnas`) y genera una cadena con los
     * atributos formateados para una consulta SQL. Si no se definen columnas, la función
     * devuelve `*` como selección predeterminada.
     *
     * ### Validaciones
     * - Verifica que `$entidad` no esté vacío.
     * - Llama a `init_columnas_sql()` para generar la cadena de columnas SQL.
     * - Si no se genera ninguna columna, devuelve `*` para seleccionar todas las columnas.
     *
     * ### Proceso
     * 1. Se valida que `$entidad` sea una cadena no vacía.
     * 2. Se llama a `init_columnas_sql()` para procesar las columnas.
     * 3. Si la cadena de columnas generada está vacía, se asigna `*` por defecto.
     *
     * @param stdClass $columnas Objeto que contiene la lista de columnas a incluir en la consulta SQL.
     *                           Cada propiedad de `$columnas` representa una columna con:
     *                           - `atributo` (string): Nombre del campo a incluir.
     *
     * @param string $entidad Nombre de la tabla o entidad a la que pertenecen las columnas.
     *                        Debe ser una cadena no vacía.
     *                        Ejemplo de entrada:
     *                        ```php
     *                        'producto'
     *                        ```
     *
     * @return string|array Devuelve una cadena con las columnas SQL formateadas correctamente.
     *                Si no hay columnas definidas, devuelve `*`.
     *                En caso de error, devuelve un array con los detalles del fallo.
     *
     * @throws error Si `$entidad` está vacío o si ocurre un error al generar las columnas SQL.
     *
     * @example Entrada válida:
     * ```php
     * $columnas = (object)[
     *     (object)['atributo' => 'id'],
     *     (object)['atributo' => 'nombre'],
     *     (object)['atributo' => 'precio']
     * ];
     * $entidad = 'producto';
     *
     * $resultado = $this->columnas_sql($columnas, $entidad);
     *
     * // Resultado esperado:
     * // 'producto.id AS id, producto.nombre AS nombre, producto.precio AS precio'
     * ```
     *
     * @example Entrada sin columnas (devuelve `*`):
     * ```php
     * $columnas = (object)[];
     * $entidad = 'producto';
     *
     * $resultado = $this->columnas_sql($columnas, $entidad);
     *
     * // Resultado esperado:
     * // '*'
     * ```
     *
     * @example Error: `$entidad` está vacío
     * ```php
     * $columnas = (object)[
     *     (object)['atributo' => 'id']
     * ];
     * $entidad = '';
     *
     * $resultado = $this->columnas_sql($columnas, $entidad);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $entidad esta vacia',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: Fallo en `init_columnas_sql`
     * ```php
     * $columnas = (object)[
     *     (object)['atributo' => '']
     * ];
     * $entidad = 'producto';
     *
     * $resultado = $this->columnas_sql($columnas, $entidad);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error al obtener columna',
     * //     'data' => $columnas
     * // ]
     * ```
     */
    private function columnas_sql(stdClass $columnas, string $entidad)
    {
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        $columnas_sql = $this->init_columnas_sql($columnas, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener columna', $columnas_sql);
        }

        if (trim($columnas_sql) === '') {
            $columnas_sql = '*';
        }

        return $columnas_sql;
    }


    /**
     * EM3
     * Reemplaza una columna en una consulta SQL utilizando los valores proporcionados en un objeto `stdClass`.
     *
     * Esta función toma una consulta SQL y un objeto de reemplazos, valida las condiciones de reemplazo
     * (`columna_compare` y `columna_rs`), y utiliza `str_replace` para sustituir la columna especificada
     * por otra en la consulta.
     *
     * @param string $consulta La consulta SQL original donde se realizará el reemplazo.
     * @param stdClass $replaces_obj Un objeto que contiene las propiedades:
     *                               - `columna`: El valor a buscar en la consulta SQL.
     *                               - `columna_replace`: El valor que reemplazará al original.
     *
     * @return string|array Devuelve la consulta SQL modificada con el reemplazo realizado. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Reemplazo válido en una consulta SQL
     * $consulta = "SELECT tabla1.nombre, tabla1.id FROM tabla1";
     * $replaces_obj = (object)[
     *     'columna' => 'tabla1.nombre',
     *     'columna_replace' => 'tabla2.nombre'
     * ];
     * $resultado = $this->consulta_columna_replace($consulta, $replaces_obj);
     * echo $resultado;
     * // Resultado:
     * // SELECT tabla2.nombre, tabla1.id FROM tabla1
     *
     * // Ejemplo 2: Sin propiedad `columna` (error)
     * $consulta = "SELECT tabla1.nombre FROM tabla1";
     * $replaces_obj = (object)[
     *     'columna_replace' => 'tabla2.nombre'
     * ];
     * $resultado = $this->consulta_columna_replace($consulta, $replaces_obj);
     * // Resultado:
     * // ['error' => 'Error al sobreescribir sql', 'data' => '']
     *
     * // Ejemplo 3: Sin propiedad `columna_replace` (error)
     * $consulta = "SELECT tabla1.nombre FROM tabla1";
     * $replaces_obj = (object)[
     *     'columna' => 'tabla1.nombre'
     * ];
     * $resultado = $this->consulta_columna_replace($consulta, $replaces_obj);
     * // Resultado:
     * // ['error' => 'Error al sobreescribir sql', 'data' => '']
     * ```
     */
    private function consulta_columna_replace(string $consulta, stdClass $replaces_obj)
    {
        // Obtener el valor de la columna a buscar en la consulta
        $columna_compare = $this->columna_compare($replaces_obj);
        if (error::$en_error) {
            return (new error())->error('Error al sobreescribir sql', $columna_compare);
        }

        // Obtener el valor de la columna a reemplazar en la consulta
        $columna_rs = $this->columna_rs($replaces_obj);
        if (error::$en_error) {
            return (new error())->error('Error al sobreescribir sql', $columna_rs);
        }

        // Realizar el reemplazo en la consulta SQL
        return str_replace($columna_compare, $columna_rs, $consulta);
    }


    /**
     * EM3
     * Reemplaza una sección de una consulta SQL basada en los valores proporcionados en un objeto `stdClass`.
     *
     * Esta función toma una consulta SQL y un objeto de reemplazos, valida las condiciones de reemplazo
     * (`replace_join_compare` y `replace_join_rs`), y utiliza `str_replace` para sustituir la sección de la
     * consulta SQL especificada.
     *
     * @param string $consulta La consulta SQL original donde se realizará el reemplazo.
     * @param stdClass $replaces_obj Un objeto que contiene las propiedades:
     *                               - `join`: El valor a buscar en la consulta SQL.
     *                               - `join_replace`: El valor que reemplazará al original.
     *
     * @return string|array Devuelve la consulta SQL modificada con el reemplazo realizado. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Reemplazo válido en una consulta SQL
     * $consulta = "SELECT * FROM tabla1 INNER JOIN tabla2 ON tabla1.id = tabla2.tabla1_id";
     * $replaces_obj = (object)[
     *     'join' => 'INNER JOIN tabla2 ON tabla1.id = tabla2.tabla1_id',
     *     'join_replace' => 'LEFT JOIN tabla2 ON tabla1.id = tabla2.tabla1_id'
     * ];
     * $resultado = $this->consulta_join_replace($consulta, $replaces_obj);
     * echo $resultado;
     * // Resultado:
     * // SELECT * FROM tabla1 LEFT JOIN tabla2 ON tabla1.id = tabla2.tabla1_id
     *
     * // Ejemplo 2: Sin propiedad `join` (error)
     * $consulta = "SELECT * FROM tabla1";
     * $replaces_obj = (object)[
     *     'join_replace' => 'LEFT JOIN tabla2 ON tabla1.id = tabla2.tabla1_id'
     * ];
     * $resultado = $this->consulta_join_replace($consulta, $replaces_obj);
     * // Resultado:
     * // ['error' => 'Error al sobreescribir sql', 'data' => '']
     *
     * // Ejemplo 3: Sin propiedad `join_replace` (error)
     * $consulta = "SELECT * FROM tabla1 INNER JOIN tabla2 ON tabla1.id = tabla2.tabla1_id";
     * $replaces_obj = (object)[
     *     'join' => 'INNER JOIN tabla2 ON tabla1.id = tabla2.tabla1_id'
     * ];
     * $resultado = $this->consulta_join_replace($consulta, $replaces_obj);
     * // Resultado:
     * // ['error' => 'Error al sobreescribir sql', 'data' => '']
     * ```
     */
    private function consulta_join_replace(string $consulta, stdClass $replaces_obj)
    {
        // Obtener el valor de `join` para buscar en la consulta
        $replace_join_compare = $this->replace_join_compare($replaces_obj);
        if (error::$en_error) {
            return (new error())->error('Error al sobreescribir sql', $replace_join_compare);
        }

        // Obtener el valor de `join_replace` para reemplazar en la consulta
        $replace_join_rs = $this->replace_join_rs($replaces_obj);
        if (error::$en_error) {
            return (new error())->error('Error al sobreescribir sql', $replace_join_compare);
        }

        // Realizar el reemplazo en la consulta SQL
        return str_replace($replace_join_compare, $replace_join_rs, $consulta);
    }


    /**
     * EM3
     * Realiza reemplazos dinámicos en una consulta SQL utilizando un objeto o array de configuraciones.
     *
     * Esta función permite sobreescribir partes específicas de una consulta SQL, como `JOIN` y columnas,
     * utilizando un objeto de reemplazos. Los reemplazos son gestionados mediante las funciones
     * `consulta_join_replace` y `consulta_columna_replace`.
     *
     * @param string $consulta La consulta SQL original donde se realizarán los reemplazos.
     * @param array|object $replaces Un array o un objeto que contiene las configuraciones de reemplazo.
     *                               Si es un array, se convierte a un objeto.
     *                               Propiedades esperadas:
     *                               - `join`: Cadena a buscar para reemplazar en la sección `JOIN`.
     *                               - `join_replace`: Cadena para reemplazar en la sección `JOIN`.
     *                               - `columna`: Cadena a buscar para reemplazar en las columnas.
     *                               - `columna_replace`: Cadena para reemplazar en las columnas.
     *
     * @return string|array Devuelve la consulta SQL modificada con los reemplazos realizados.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Reemplazos válidos en una consulta SQL
     * $consulta = "SELECT tabla1.nombre, tabla1.id FROM tabla1 INNER JOIN tabla2 ON tabla1.id = tabla2.tabla1_id";
     * $replaces = [
     *     'join' => 'INNER JOIN tabla2 ON tabla1.id = tabla2.tabla1_id',
     *     'join_replace' => 'LEFT JOIN tabla2 ON tabla1.id = tabla2.tabla1_id',
     *     'columna' => 'tabla1.nombre',
     *     'columna_replace' => 'tabla2.nombre'
     * ];
     * $resultado = $this->consulta_sobreescrita($consulta, $replaces);
     * echo $resultado;
     * // Resultado:
     * // SELECT tabla2.nombre, tabla1.id FROM tabla1 LEFT JOIN tabla2 ON tabla1.id = tabla2.tabla1_id
     *
     * // Ejemplo 2: Reemplazos con formato incorrecto (error)
     * $consulta = "SELECT tabla1.nombre FROM tabla1";
     * $replaces = 'esto_no_es_un_objeto';
     * $resultado = $this->consulta_sobreescrita($consulta, $replaces);
     * // Resultado:
     * // ['error' => 'Error $replaces debe ser un objeto', 'data' => 'esto_no_es_un_objeto']
     * ```
     */
    private function consulta_sobreescrita(string $consulta, $replaces)
    {
        // Convertir a objeto si el parámetro $replaces es un array
        if (is_array($replaces)) {
            $replaces = (object)$replaces;
        }

        // Validar que $replaces sea un objeto
        if (!is_object($replaces)) {
            return (new error())->error('Error $replaces debe ser un objeto', $replaces);
        }

        // Reemplazar secciones de la consulta relacionadas con `JOIN`
        $consulta = $this->consulta_join_replace($consulta, $replaces);
        if (error::$en_error) {
            return (new error())->error('Error al sobreescribir sql', $consulta);
        }

        // Reemplazar secciones de la consulta relacionadas con columnas
        $consulta = $this->consulta_columna_replace($consulta, $replaces);
        if (error::$en_error) {
            return (new error())->error('Error al sobreescribir sql', $consulta);
        }

        // Retornar la consulta modificada
        return $consulta;
    }



    /**
     * EM3
     * Genera una consulta SQL `DESCRIBE` para obtener la estructura de una tabla.
     *
     * Esta función toma el nombre de una tabla como parámetro, valida que no esté vacío,
     * y genera una consulta SQL para describir la estructura de la tabla.
     *
     * @param string $entidad El nombre de la tabla a describir.
     *
     * @return string|array Devuelve la consulta SQL `DESCRIBE` como una cadena. Si el nombre de la tabla
     *                      está vacío, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar una consulta válida
     * $query = $this->describe('empleados');
     * echo $query;
     * // Resultado:
     * // DESCRIBE empleados
     *
     * // Ejemplo 2: Error al proporcionar un nombre vacío
     * $query = $this->describe('');
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Ejecutar la consulta
     * $sql = $this->describe('departamentos');
     * $resultado = $this->ejecuta_consulta_segura($sql);
     * if (isset($resultado['error'])) {
     *     echo "Error: " . $resultado['mensaje'];
     * } else {
     *     foreach ($resultado['registros'] as $columna) {
     *         echo "Columna: " . $columna['Field'];
     *     }
     * }
     * ```
     */
    final public function describe(string $entidad)
    {
        // Eliminar espacios en blanco alrededor del nombre de la entidad
        $entidad = trim($entidad);

        // Validar que la entidad no esté vacía
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Retornar la consulta DESCRIBE
        return "DESCRIBE $entidad";
    }


    /**
     * EM3
     * Genera una declaración SQL para una entidad con un alias (`AS`).
     *
     * Esta función toma el nombre de una entidad y un alias (`rename`), valida que la entidad no esté vacía,
     * y genera una declaración SQL que incluye el alias. Si el alias está vacío, utiliza el nombre de la entidad como alias.
     *
     * @param string $entidad El nombre de la entidad (tabla). No debe estar vacío.
     * @param string $rename (Opcional) El alias que se usará para la entidad. Si está vacío, se usa el nombre de la entidad.
     *
     * @return string|array Devuelve una cadena SQL con la declaración de la entidad y su alias (`AS`).
     *                      En caso de error, devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar una entidad con alias
     * $entidad = 'tabla_principal';
     * $rename = 'tp';
     * $resultado = $this->entidad_base_as($entidad, $rename);
     * echo $resultado;
     * // Resultado:
     * // tabla_principal AS tp
     *
     * // Ejemplo 2: Alias vacío (se usa el nombre de la entidad)
     * $entidad = 'tabla_principal';
     * $rename = '';
     * $resultado = $this->entidad_base_as($entidad, $rename);
     * echo $resultado;
     * // Resultado:
     * // tabla_principal AS tabla_principal
     *
     * // Ejemplo 3: Entidad vacía (error)
     * $entidad = '';
     * $rename = 'tp';
     * $resultado = $this->entidad_base_as($entidad, $rename);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     * ```
     */
    final public function entidad_base_as(string $entidad, string $rename)
    {
        // Eliminar espacios alrededor del nombre de la entidad
        $entidad = trim($entidad);

        // Validar que la entidad no esté vacía
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Eliminar espacios alrededor del alias y usar el nombre de la entidad si está vacío
        $rename = trim($rename);
        if ($rename === '') {
            $rename = $entidad;
        }

        // Retornar la declaración SQL con alias
        return "$entidad AS $rename";
    }


    /**
     * TRASLADADO
     * Verifica la existencia de un registro específico en una entidad de la base de datos.
     *
     * Esta función genera una consulta SQL para contar los registros en una tabla específica (`entidad`)
     * donde el ID del registro coincide con el proporcionado. Si la entidad está vacía o el ID no es válido,
     * retorna un error.
     *
     * @param string $entidad Nombre de la tabla en la base de datos donde se verificará la existencia del registro.
     * @param int $registro_id ID del registro que se desea verificar. Debe ser mayor a 0.
     *
     * @return string|array Devuelve una cadena con la consulta SQL generada para verificar la existencia
     *                      del registro o un arreglo de error en caso de parámetros inválidos.
     *
     * Errores posibles:
     * - `['error' => 'Error $entidad esta vacia', 'detalle' => $entidad]` si el nombre de la entidad está vacío.
     * - `['error' => 'Error $registro_id debe ser mayor a 0', 'detalle' => $registro_id]` si el ID del registro es menor o igual a 0.
     */
    final public function existe_by_id(string $entidad, int $registro_id)
    {
        $entidad = trim($entidad);
        if($entidad === ''){
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }
        if($registro_id <= 0){
            return (new error())->error('Error $registro_id debe ser mayor a 0', $registro_id);
        }
        return /** @lang MYSQL */ "SELECT COUNT(*) AS n_rows FROM $entidad WHERE id = $registro_id";

    }

    /**
     * EM3
     * Genera una cláusula SQL `WHERE` especial basada en un arreglo de filtros personalizados.
     *
     * Esta función valida y construye dinámicamente una cláusula SQL con múltiples condiciones
     * definidas en el arreglo `$filtro_especial`, soportando operadores personalizados y valores
     * que pueden ser campos o literales.
     *
     * @param array $filtro_especial Un arreglo asociativo donde:
     *                               - La clave es el nombre del campo.
     *                               - El valor es otro arreglo con las claves:
     *                                 - `operador`: El operador lógico o de comparación (por ejemplo, `=`, `>`, `<`, `LIKE`).
     *                                 - `valor`: El valor con el cual comparar. Puede ser vacío si el operador no requiere un valor.
     *                                 - `valor_es_campo` (opcional): Indica si el valor es un campo en lugar de un literal.
     *
     * @return string|array Devuelve una cadena SQL con las condiciones `WHERE` generadas. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula SQL válida
     * $filtro_especial = [
     *     'campo1' => ['operador' => '=', 'valor' => 'valor1'],
     *     'campo2' => ['operador' => '>', 'valor' => '10'],
     *     'campo3' => ['operador' => '=', 'valor' => 'campo4', 'valor_es_campo' => true]
     * ];
     * $resultado = $this->filtro_especial_sql($filtro_especial);
     * echo $resultado;
     * // Resultado:
     * // campo1='valor1' AND campo2>'10' AND campo3=campo4
     *
     * // Ejemplo 2: Error por filtro no válido
     * $filtro_especial = [
     *     'campo1' => 'no_es_array'
     * ];
     * $resultado = $this->filtro_especial_sql($filtro_especial);
     * // Resultado:
     * // ['error' => 'Error filtro debe ser un array', 'data' => 'no_es_array']
     * ```
     */
    final public function filtro_especial_sql(array $filtro_especial)
    {
        // Inicializa la cláusula SQL
        $filtro_especial_sql = '';

        // Itera sobre cada filtro en el arreglo
        foreach ($filtro_especial as $campo => $filtro) {
            // Valida que el filtro sea un arreglo
            if (!is_array($filtro)) {
                return (new error())->error('Error filtro debe ser un array', $filtro);
            }

            // Limpia el nombre del campo
            $campo = trim($campo);

            // Asigna valores predeterminados si no están definidos
            if (!isset($filtro['valor'])) {
                $filtro['valor'] = '';
            }
            if (!isset($filtro['operador'])) {
                $filtro['operador'] = '';
            }

            // Construye la cláusula SQL
            if ($filtro_especial_sql === '') {
                // Primera condición
                if (isset($filtro['valor_es_campo']) && $filtro['valor_es_campo']) {
                    $filtro_especial_sql .= "'" . $campo . "'" . $filtro['operador'] . $filtro['valor'];
                } else {
                    if (trim($filtro['valor']) === '') {
                        $filtro_especial_sql .= $campo . $filtro['operador'];
                    } else {
                        $filtro_especial_sql .= $campo . $filtro['operador'] . "'" . $filtro['valor'] . "'";
                    }
                }
            } else {
                // Condiciones subsecuentes
                if (isset($filtro['valor_es_campo']) && $filtro['valor_es_campo']) {
                    $filtro_especial_sql .= ' AND ' . "'" . $campo . "'" . $filtro['operador'] . $filtro['valor'];
                } else {
                    if (trim($filtro['valor']) === '') {
                        $filtro_especial_sql .= ' AND ' . $campo . $filtro['operador'];
                    } else {
                        $filtro_especial_sql .= ' AND ' . $campo . $filtro['operador'] . "'" . $filtro['valor'] . "'";
                    }
                }
            }
        }

        return $filtro_especial_sql;
    }


    /**
     * EM3
     * Genera una cláusula SQL `WHERE` basada en rangos definidos para múltiples campos.
     *
     * Esta función recorre un arreglo de filtros de rango, valida los datos, y construye una condición SQL
     * utilizando el operador `BETWEEN` para cada campo especificado.
     *
     * @param array $filtro_rango Un arreglo asociativo donde:
     *                            - La clave es el nombre del campo.
     *                            - El valor es un arreglo con las claves:
     *                              - `valor1`: El valor inferior del rango.
     *                              - `valor2`: El valor superior del rango.
     *
     * @return string|array Devuelve una cadena SQL con las condiciones de rango generadas. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar cláusula de rango para múltiples campos
     * $filtro_rango = [
     *     'campo1' => ['valor1' => '2023-01-01', 'valor2' => '2023-12-31'],
     *     'campo2' => ['valor1' => 10, 'valor2' => 20]
     * ];
     * $resultado = $this->filtro_rango_sql($filtro_rango);
     * echo $resultado;
     * // Resultado:
     * // campo1 BETWEEN '2023-01-01' AND '2023-12-31' AND campo2 BETWEEN '10' AND '20'
     *
     * // Ejemplo 2: Error por campo vacío
     * $filtro_rango = [
     *     '' => ['valor1' => 10, 'valor2' => 20]
     * ];
     * $resultado = $this->filtro_rango_sql($filtro_rango);
     * // Resultado:
     * // ['error' => 'Error campo está vacío', 'detalle' => '']
     *
     * // Ejemplo 3: Error por filtro mal configurado
     * $filtro_rango = [
     *     'campo1' => 'no_es_array'
     * ];
     * $resultado = $this->filtro_rango_sql($filtro_rango);
     * // Resultado:
     * // ['error' => 'Error filtro debe ser un array', 'detalle' => 'no_es_array']
     * ```
     */
    final public function filtro_rango_sql(array $filtro_rango)
    {
        $filtro_rango_sql = '';

        // Recorre cada filtro para generar las condiciones de rango
        foreach ($filtro_rango as $campo => $filtro) {
            $campo = trim($campo);

            // Valida que el nombre del campo no esté vacío
            if ($campo === '') {
                return ['error' => 'Error campo está vacío', 'detalle' => $campo];
            }

            // Valida que el filtro sea un arreglo
            if (!is_array($filtro)) {
                return ['error' => 'Error filtro debe ser un array', 'detalle' => $filtro];
            }

            // Valida que existan los índices 'valor1' y 'valor2' en el filtro
            if (!isset($filtro['valor1'])) {
                return ['error' => 'Error filtro[valor1] debe existir', 'detalle' => $filtro];
            }
            if (!isset($filtro['valor2'])) {
                return ['error' => 'Error filtro[valor2] debe existir', 'detalle' => $filtro];
            }

            // Construye la condición SQL para el rango
            $condicion = $campo . ' BETWEEN ' . "'" . $filtro['valor1'] . "'" . " AND " . "'" . $filtro['valor2'] . "'";

            // Concatena la condición al resultado
            if ($filtro_rango_sql === '') {
                $filtro_rango_sql .= $condicion;
            } else {
                $filtro_rango_sql .= ' AND ' . $condicion;
            }
        }

        return $filtro_rango_sql;
    }



    /**
     * EM3
     * Genera una cláusula SQL `AND` basada en un array de filtros.
     *
     * Esta función toma un array asociativo donde las claves son nombres de campos y los valores son sus respectivos
     * valores, y genera una cadena SQL para una cláusula `AND`. Si el array está vacío o contiene claves no válidas,
     * se devuelve un error.
     *
     * @param array $filtro Un array asociativo donde:
     *                      - Las claves son nombres de campos en formato `tabla.campo`.
     *                      - Los valores son los valores a comparar en la cláusula SQL.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `AND` generada. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula `AND` válida
     * $filtro = [
     *     'empleados.status' => 'activo',
     *     'empleados.edad' => 30
     * ];
     * $resultado = $this->genera_and($filtro);
     * echo $resultado;
     * // Resultado:
     * // empleados.status = 'activo' AND empleados.edad = '30'
     *
     * // Ejemplo 2: Filtro con claves no válidas (error)
     * $filtro = [
     *     0 => 'activo'
     * ];
     * $resultado = $this->genera_and($filtro);
     * // Resultado:
     * // ['error' => 'Los key deben de ser campos asociativos con referencia a tabla.campo', 'data' => [...]]
     *
     * // Ejemplo 3: Filtro vacío
     * $filtro = [];
     * $resultado = $this->genera_and($filtro);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     * ```
     */
    final public function genera_and(array $filtro = array())
    {
        // Inicializar la sentencia SQL
        $sentencia = '';

        // Recorrer el array de filtros
        foreach ($filtro as $key => $value) {
            // Validar que las claves sean asociativas
            if (is_numeric($key)) {
                return (new error())->error(
                    'Los key deben de ser campos asociativos con referencia a tabla.campo', $filtro
                );
            }

            // Omitir valores que son arrays
            if (is_array($value)) {
                continue;
            }

            // Escapar las claves y valores para evitar inyección SQL
            $key = addslashes($key);
            $value = addslashes($value);

            // Construir la cláusula AND
            $sentencia .= $sentencia === '' ? "$key = '$value'" : " AND $key = '$value'";
        }

        // Retornar la cláusula generada
        return $sentencia;
    }


    /**
     * EM3
     * Genera una cláusula SQL `AND` utilizando comparaciones de texto con `LIKE`.
     *
     * Esta función toma un array asociativo donde las claves son nombres de campos y los valores son cadenas de texto,
     * y genera una cláusula SQL `AND` con comparaciones de texto utilizando `LIKE`.
     *
     * @param array $filtros Un array asociativo donde:
     *                       - Las claves son nombres de campos en formato `tabla.campo`.
     *                       - Los valores son cadenas de texto que se usarán en las comparaciones `LIKE`.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `AND` generada. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula `AND` válida con `LIKE`
     * $filtros = [
     *     'empleados.nombre' => 'Juan',
     *     'empleados.apellido' => 'Pérez'
     * ];
     * $resultado = $this->genera_and_textos($filtros);
     * echo $resultado;
     * // Resultado:
     * // empleados.nombre LIKE '%Juan%' AND empleados.apellido LIKE '%Pérez%'
     *
     * // Ejemplo 2: Filtro con claves no válidas (error)
     * $filtros = [
     *     0 => 'Juan'
     * ];
     * $resultado = $this->genera_and_textos($filtros);
     * // Resultado:
     * // ['error' => 'Los key deben de ser campos asociativos con referencia a tabla.campo', 'data' => [...]]
     *
     * // Ejemplo 3: Filtro vacío
     * $filtros = [];
     * $resultado = $this->genera_and_textos($filtros);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     * ```
     */
    final public function genera_and_textos(array $filtros)
    {
        // Inicializar la sentencia SQL
        $sentencia = '';

        // Recorrer el array de filtros
        foreach ($filtros as $key => $value) {
            // Validar que las claves sean asociativas
            if (is_numeric($key)) {
                return (new error())->error(
                    'Los key deben de ser campos asociativos con referencia a tabla.campo', $filtros
                );
            }

            // Omitir valores que son arrays
            if (is_array($value)) {
                continue;
            }

            // Escapar las claves y valores para evitar inyección SQL
            $key = addslashes($key);
            $value = addslashes($value);

            // Construir la cláusula AND con LIKE
            $sentencia .= $sentencia === "" ? "$key LIKE '%$value%'" : " AND $key LIKE '%$value%'";
        }

        // Retornar la cláusula generada
        return $sentencia;
    }


    /**
     * EM3
     * Genera una cadena SQL de LEFT JOINs a partir de un conjunto de parámetros.
     *
     * Esta función toma un array de parámetros que representan relaciones de JOIN, los valida e
     * integra en una estructura de SQL. Si la integración es exitosa, devuelve una cadena de texto
     * con las cláusulas `LEFT JOIN`. Si ocurre algún error durante el proceso, devuelve un objeto
     * de error con detalles.
     *
     * ---
     * **Parámetros:**
     *
     * @param array $params Un array de parámetros que define los JOINs a incluir en la consulta.
     *                      Cada elemento debe ser un array u objeto que contenga la estructura de un JOIN.
     *
     * ---
     * **Retorno:**
     *
     * @return string|array Devuelve una cadena con todas las cláusulas `LEFT JOIN` concatenadas y
     *                      correctamente formateadas. En caso de error, devuelve un array con detalles.
     *
     * ---
     * **Ejemplo de uso:**
     *
     * ```php
     * // Definir los parámetros de JOINs
     * $params = [
     *     ['entidad_left' => 'empleados', 'entidad_right' => 'departamentos', 'rename_entidad' => 'emp'],
     *     ['entidad_left' => 'ventas', 'entidad_right' => 'clientes', 'rename_entidad' => 'v']
     * ];
     *
     * // Generar la cadena de JOINs
     * $resultado = $this->genera_join_replace($params);
     *
     * // Imprimir el resultado
     * echo $resultado;
     * ```
     *
     * **Salida esperada:**
     * ```sql
     * LEFT JOIN empleados emp ON emp.id = departamentos.empleado_id
     * LEFT JOIN ventas v ON v.id = clientes.venta_id
     * ```
     *
     * ---
     * **Casos de uso y validaciones:**
     *
     * ```php
     * // Caso 1: JOINs válidos
     * $params = [
     *     ['entidad_left' => 'productos', 'entidad_right' => 'categorias', 'rename_entidad' => 'p']
     * ];
     * $resultado = $this->genera_join_replace($params);
     * echo $resultado;
     * // Salida esperada:
     * // "LEFT JOIN productos p ON p.id = categorias.producto_id"
     *
     * // Caso 2: Error por parámetro inválido
     * $params = [
     *     'entidad_left' => 'empleados',
     *     'entidad_right' => 'departamentos'
     * ];
     * $resultado = $this->genera_join_replace($params);
     * print_r($resultado);
     * // Salida esperada:
     * // ['error' => 'Error al integrar joins', 'data' => '...']
     *
     * // Caso 3: Sin JOINs en la entrada
     * $params = [];
     * $resultado = $this->genera_join_replace($params);
     * print_r($resultado);
     * // Salida esperada:
     * // ""
     * ```
     *
     * ---
     * **Notas:**
     * - Si `$params` está vacío, la función devuelve una cadena vacía (`""`).
     * - Cada elemento de `$params` debe ser un array u objeto con información suficiente para construir un JOIN.
     * - Si un JOIN está mal definido, la función devuelve un error con el mensaje `'Error al integrar joins'`.
     * - Se usa `trim()` en la salida final para eliminar espacios innecesarios.
     * - Los JOINs se concatenan con `LEFT JOIN` y se separan por un espacio para mantener la estructura SQL correcta.
     */

    final public function genera_join_replace(array $params)
    {
        $joins = $this->left_joins($params);
        if(error::$en_error){
            return (new error())->error('Error al integrar joins',$joins);
        }

        $join_replace = $this->joins_replace($joins);
        if(error::$en_error){
            return (new error())->error('Error al integrar $join_replace',$join_replace);
        }
        return $join_replace;

    }

    private function genera_sentencia_like(array $campos_like = [], string $condicion_like = ''): string {

        if (empty($campos_like) || $condicion_like === '') {
            return '';
        }

        $condicion_like = addslashes($condicion_like);
    
        $condiciones = array_map(fn($campo) => "$campo LIKE '%$condicion_like%'", $campos_like);
    
        return "(" . implode(" OR ", $condiciones) . ")";
    }

    public function get_data_datatable (array $campos_base,array $campos_like,string $entidad,
    array $campos_extra = array(),
    array $condicion = array(),string $condicion_like = '',array $filtro_rango = array(),
    array $join = array(),int $limit = 0,int $offset = 0, string $order_by = '') 
    {
        if(empty($campos_base)) {
            return (new error())->error('Error $campos_base no puede ser vacio', $campos_base);
        }
        if(trim($entidad) === '') {
            return (new error())->error('Error $entidad_empleado no puede ser vacio', $entidad);
        }

        $campos = $this->init_campos_base($campos_base);
        if(error::$en_error){
            return (new error())->error('Error al generar $campos', $campos);
        }

        if (!empty($campos_extra)) {
            $campos .= ','.implode(',',$campos_extra);
        }
        $sentencia_join = '';
        if(!empty($join)) {
            $sentencia_join = implode(' ', $join);
        }

        $where = '';
        $where_conditions = [];
        if(!empty($condicion)) {
            $where_and = (new sql())->genera_and($condicion);
            if (error::$en_error) {
                return (new error())->error('Error al generar and', $where_and);
            }
            $where_conditions[] = $where_and;
        }
        if(!empty($filtro_rango)) {
            $filtro_rango_sql = (new sql())->filtro_rango_sql($filtro_rango);
            if (error::$en_error) {
                return (new error())->error('Error al generar and', $filtro_rango_sql);
            }
            $where_conditions[] = $filtro_rango_sql;
        }
        
        if(trim($condicion_like) !== '') {
            if (empty($campos_like)) {
                return (new error())->error('Error where_like no puede estar vacio', $campos_like);
            }
            $where_like = $this->genera_sentencia_like($campos_like,$condicion_like);
            $where_conditions[] = $where_like;
        }
        $where = '';
        if (!empty($where_conditions)) {
            $where .= ' WHERE ' . implode(' AND ', $where_conditions);
        }

        $filters = "";
        if(trim($order_by) !== '') {
            $filters .= " ORDER BY $order_by";
        }
        if($limit > 0) {
            $filters .= " LIMIT $limit";
        }
        if($offset > 0) {
            $filters .= " OFFSET $offset";
        }

        $lf_from = $entidad . " " . $sentencia_join;

        $where .= $filters;
        return /**@lang MYSQL */ "SELECT $campos FROM $lf_from $where";
    }

    private function genera_in_empresa(PDO $link)
    {
        $empresas_324 =  (new empresa())->empresas_324($link);
        if(error::$en_error){
            return (new error())->error('Error al obtener empresas', $empresas_324);
        }

        $in_sql = $this->in_empresa($empresas_324);
        if(error::$en_error){
            return (new error())->error('Error al obtener $in_sql', $in_sql);
        }

        return $in_sql;

    }

    private function in_empresa(array $empresas_324)
    {
        $in_sql = '';
        foreach ($empresas_324 as $empresa) {
            $coma = $this->coma($in_sql);
            if(error::$en_error){
                return (new error())->error('Error al obtener $coma', $coma);
            }
            $in_sql.= $coma.$empresa->id;
        }

        return $in_sql;

    }

    /**
     * TRASLADADO
     * Agrega un valor a una cadena de valores `IN` en una consulta SQL, asegurando que esté correctamente formateado.
     *
     * La función valida que el valor proporcionado en `$value_in` no esté vacío, luego determina si es necesario agregar
     * una coma (utilizando la función `coma`) para concatenar el nuevo valor a la cadena `$in_sql`. Devuelve la cadena
     * actualizada. Si ocurre algún error, devuelve un array con los detalles del fallo.
     *
     * @param string $in_sql La cadena que contiene los valores previos para la cláusula `IN`.
     * @param string $value_in El nuevo valor que se agregará a la cadena `IN`.
     *
     * @return string|array Devuelve la cadena actualizada con el nuevo valor agregado. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function in_sql(string $in_sql, string $value_in)
    {
        $value_in = trim($value_in);
        if($value_in === ''){
            return (new error())->error('Error $value_in esta vacio', $value_in);
        }
        $coma = $this->coma($in_sql);
        if(error::$en_error){
            return (new error())->error('Error al obtener coma', $coma);
        }
        $in_sql.=$coma.$value_in;

        return $in_sql;

    }

    /**
     * TRASLADADO
     * Genera una cadena SQL con valores formateados para una cláusula `IN` a partir de un array de valores.
     *
     * La función recorre un array de valores, asegurándose de que cada uno no esté vacío, y luego los agrega a una
     * cadena para su uso en una cláusula `IN`. Utiliza la función `in_sql` para concatenar los valores correctamente.
     * Si ocurre un error en el proceso, devuelve un array con los detalles del fallo.
     *
     * @param array $values Un array de valores que serán concatenados para formar una cláusula `IN`.
     *
     * @return string|array Devuelve una cadena formateada con los valores para la cláusula `IN`. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function in_sql_base(array $values)
    {
        $in_sql = "";
        foreach ($values as $value){
            $value = trim($value);
            if($value === ''){
                return (new error())->error('Error $value esta vacio', $value);
            }
            $in_sql = $this->in_sql($in_sql,$value);
            if(error::$en_error){
                return (new error())->error('Error al obtener $in_sql', $in_sql);
            }
        }
        return trim($in_sql);

    }

    /**
     * TRASLADADO
     * Genera una cláusula SQL `IN` completa utilizando un campo y un conjunto de valores.
     *
     * La función valida que el campo no esté vacío y que el array de valores sea procesado correctamente
     * para crear la parte de la consulta SQL correspondiente a la cláusula `IN`. Primero, genera los valores
     * para la cláusula `IN` utilizando `in_sql_base` y luego integra la cláusula completa mediante
     * `integra_in_sql`, con la opción de agregar la condición `AND` si se especifica.
     * Si ocurre algún error durante el proceso, devuelve un array con los detalles del fallo.
     *
     * @param string $campo El campo que será utilizado en la cláusula `IN`.
     * @param bool $con_and Si es `true`, agrega la condición `AND` antes de la cláusula `IN`.
     * @param array $values El conjunto de valores que serán utilizados en la cláusula `IN`.
     *
     * @return string|array Devuelve la cláusula SQL `IN` completa. Si ocurre un error, devuelve un array con los detalles del fallo.
     */

    final public function in_sql_completo(string $campo, bool $con_and, array $values)
    {
        $campo = trim($campo);
        if($campo === ''){
            return (new error())->error('Error $campo esta vacio',$campo);
        }

        $in_sql = $this->in_sql_base($values);
        if(error::$en_error){
            return (new error())->error('Error al obtener $in_sql', $in_sql);
        }

        $in_sql = $this->integra_in_sql($campo,$con_and,$in_sql);
        if(error::$en_error){
            return (new error())->error('Error al integrar key $in_sql', $in_sql);
        }
        return $in_sql;

    }

    private function init_campos_base(array $campos_base) {

        $campos = (new sql())->campos_sql_puros_with_as($campos_base);
        if(error::$en_error){
            return (new error())->error('Error al generar $campos', $campos);
        }

        return $campos;
    }

    /**
     * EM3
     * Inicializa la cadena de columnas SQL a partir de un objeto de atributos.
     *
     * Este método recorre un objeto `$columnas` que contiene múltiples atributos y construye
     * la cadena de columnas SQL necesarias para una consulta `SELECT`. Cada atributo se valida
     * y se integra con su respectivo alias.
     *
     * ### Validaciones
     * - Verifica que `$entidad` no esté vacío.
     * - Confirma que `$columnas` sea un objeto (`stdClass`).
     * - Asegura que cada elemento de `$columnas` sea un objeto válido.
     * - Valida que cada columna contenga la propiedad `atributo` y que esta no sea una cadena vacía.
     *
     * ### Proceso
     * 1. Se valida que `$entidad` sea una cadena no vacía.
     * 2. Se inicializa la cadena de columnas SQL como una cadena vacía.
     * 3. Se recorre cada columna en `$columnas`:
     *    - Se verifica que sea un objeto.
     *    - Se valida que la propiedad `atributo` exista y no sea vacía.
     *    - Se agrega la columna a la cadena SQL utilizando `integra_columnas_sql()`.
     *
     * @param stdClass $columnas Objeto que contiene la lista de columnas a incluir en la consulta SQL.
     *                           Cada propiedad de `$columnas` representa una columna con:
     *                           - `atributo` (string): Nombre del campo a incluir.
     *
     * @param string $entidad Nombre de la tabla o entidad a la que pertenecen las columnas.
     *                        Debe ser una cadena no vacía.
     *                        Ejemplo de entrada:
     *                        ```php
     *                        'producto'
     *                        ```
     *
     * @return string|array Devuelve una cadena con las columnas SQL formateadas correctamente.
     *                En caso de error, devuelve un array con los detalles del fallo.
     *
     * @throws error Si `$entidad` está vacío, si `$columnas` no es un objeto válido, si alguna columna no es un objeto,
     *               o si `atributo` no existe o está vacío.
     *
     * @example Entrada válida:
     * ```php
     * $columnas = (object)[
     *     (object)['atributo' => 'id'],
     *     (object)['atributo' => 'nombre'],
     *     (object)['atributo' => 'precio']
     * ];
     * $entidad = 'producto';
     *
     * $resultado = $this->init_columnas_sql($columnas, $entidad);
     *
     * // Resultado esperado:
     * // 'producto.id AS id, producto.nombre AS nombre, producto.precio AS precio'
     * ```
     *
     * @example Error: `$entidad` está vacío
     * ```php
     * $columnas = (object)[
     *     (object)['atributo' => 'id'],
     *     (object)['atributo' => 'nombre']
     * ];
     * $entidad = '';
     *
     * $resultado = $this->init_columnas_sql($columnas, $entidad);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $entidad esta vacio',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: `$columnas` contiene un elemento que no es un objeto
     * ```php
     * $columnas = (object)[
     *     'id',
     *     (object)['atributo' => 'nombre']
     * ];
     * $entidad = 'producto';
     *
     * $resultado = $this->init_columnas_sql($columnas, $entidad);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $column debe ser un objeto',
     * //     'data' => $columnas
     * // ]
     * ```
     *
     * @example Error: `$columnas` contiene un elemento sin `atributo`
     * ```php
     * $columnas = (object)[
     *     (object)[],
     *     (object)['atributo' => 'nombre']
     * ];
     * $entidad = 'producto';
     *
     * $resultado = $this->init_columnas_sql($columnas, $entidad);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error atributo no existe en columnas',
     * //     'data' => $columnas[0]
     * // ]
     * ```
     *
     * @example Error: `$column->atributo` está vacío
     * ```php
     * $columnas = (object)[
     *     (object)['atributo' => ''],
     *     (object)['atributo' => 'nombre']
     * ];
     * $entidad = 'producto';
     *
     * $resultado = $this->init_columnas_sql($columnas, $entidad);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $column->atributo esta vacio',
     * //     'data' => $columnas[0]
     * // ]
     * ```
     */
    private function init_columnas_sql(stdClass $columnas, string $entidad)
    {
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }
        $columnas_sql = '';
        foreach ($columnas as $column) {
            if (!is_object($column)) {
                return (new error())->error('Error $column debe ser un objeto', $columnas);
            }
            if (!isset($column->atributo)) {
                return (new error())->error('Error atributo no existe en columnas', $column);
            }
            $column->atributo = trim($column->atributo);
            if ($column->atributo === '') {
                return (new error())->error('Error $column->atributo esta vacio', $column);
            }
            $columnas_sql = $this->integra_columnas_sql($column, $columnas_sql, $entidad);
            if (error::$en_error) {
                return (new error())->error('Error al obtener columna', $columnas_sql);
            }
        }
        return $columnas_sql;
    }


    /**
     * TRASLADADO
     * Inicializa y devuelve un valor de un arreglo asociativo basado en una clave dada.
     *
     * Esta función toma un arreglo asociativo y una clave, y devuelve el valor
     * correspondiente a esa clave después de limpiarlo. Si la clave está vacía,
     * se devuelve un objeto de error. Si la clave no existe en el arreglo, se devuelve
     * una cadena vacía.
     *
     * @param array $data Arreglo asociativo del cual se extraerá el valor.
     * @param string $key_compare Clave con la que se buscará el valor en el arreglo.
     *
     * @return string|array Retorna el valor encontrado y limpiado, una cadena vacía si la clave no existe,
     * o un objeto de error si la clave está vacía.
     */
    private function init_txt_array(array $data, string $key_compare)
    {
        $key_compare = trim($key_compare);
        if($key_compare === ''){
            return (new error())->error('Error key_compare esta vacio',$key_compare);
        }
        $value = '';
        if(isset($data[$key_compare])){
            $value = trim($data[$key_compare]);
        }
        return $value;

    }

    /**
     * TRASLADADO
     * Inicializa y devuelve un objeto con valores de un arreglo asociativo basado en un conjunto de claves.
     *
     * Esta función recorre un conjunto de claves, las limpia y busca sus valores correspondientes
     * en un arreglo asociativo utilizando la función `init_txt_array`. Si alguna clave está vacía
     * o si ocurre un error al obtener un valor, se devuelve un objeto de error.
     *
     * @param array $data Arreglo asociativo del cual se extraerán los valores.
     * @param array $keys_compare Conjunto de claves que se utilizarán para buscar los valores en el arreglo.
     *
     * @return stdClass|array Retorna un objeto stdClass con los valores correspondientes a las claves,
     * o un objeto de error si alguna clave está vacía o si ocurre un error al obtener los valores.
     */
    private function init_values_array(array $data, array $keys_compare)
    {
        $rs = new stdClass();
        foreach ($keys_compare as $key_compare){
            $key_compare = trim($key_compare);
            if($key_compare === ''){
                return (new error())->error('Error key_compare esta vacio',$key_compare);
            }
            $value = $this->init_txt_array($data,$key_compare);
            if(error::$en_error){
                return (new error())->error('Error al obtener $value', $value);
            }
            $rs->$key_compare = $value;
        }
        return $rs;

    }

    /**
     * EM3
     * Integra una nueva columna en una sentencia SQL `SELECT`, asegurando que la estructura sea válida.
     *
     * Esta función toma un objeto de columna (`stdClass`), una cadena de columnas SQL existentes y
     * el nombre de la entidad a la que pertenece la columna. Se encarga de validar la estructura de entrada,
     * generar el alias correspondiente (`AS atributo`) y concatenar la columna a la cadena SQL.
     *
     * ### Validaciones
     * - Verifica que `$entidad` no esté vacío.
     * - Asegura que `$column` contenga la propiedad `atributo` y que esta no sea una cadena vacía.
     * - Obtiene la coma correspondiente para separar columnas en la sentencia SQL.
     * - Genera el alias SQL de la forma `entidad.atributo AS atributo`.
     *
     * ### Proceso
     * 1. Se valida que `$entidad` sea una cadena no vacía.
     * 2. Se obtiene la coma (`,`) si hay más columnas ya definidas.
     * 3. Se valida que `$column` contenga el atributo requerido.
     * 4. Se genera la estructura SQL con alias (`entidad.atributo AS atributo`).
     * 5. Se concatena la nueva columna a `$columnas_sql` con la coma obtenida.
     *
     * @param stdClass $column Objeto que contiene la información de la columna a agregar.
     *                         Debe incluir:
     *                         - `atributo` (string): Nombre del atributo de la entidad.
     *
     * @param string $columnas_sql Cadena que contiene las columnas ya agregadas en la sentencia `SELECT`.
     *                              Ejemplo de entrada:
     *                              ```php
     *                              'producto.id AS id, producto.nombre AS nombre'
     *                              ```
     *
     * @param string $entidad Nombre de la entidad (tabla) a la que pertenece la columna.
     *                        Debe ser una cadena no vacía.
     *                        Ejemplo de entrada:
     *                        ```php
     *                        'producto'
     *                        ```
     *
     * @return string|array Devuelve la cadena `$columnas_sql` actualizada con la nueva columna agregada.
     *                En caso de error, devuelve un array con los detalles del fallo.
     *
     * @throws error Si `$entidad` está vacío, si `$column->atributo` no existe o está vacío,
     *               o si ocurre un error al obtener la coma o el campo SQL.
     *
     * @example Entrada válida:
     * ```php
     * $column = (object)[
     *     'atributo' => 'precio'
     * ];
     * $columnas_sql = 'producto.id AS id, producto.nombre AS nombre';
     * $entidad = 'producto';
     *
     * $resultado = $this->integra_columnas_sql($column, $columnas_sql, $entidad);
     *
     * // Resultado esperado:
     * // 'producto.id AS id, producto.nombre AS nombre, producto.precio AS precio'
     * ```
     *
     * @example Error: `$entidad` está vacío
     * ```php
     * $column = (object)[
     *     'atributo' => 'precio'
     * ];
     * $columnas_sql = 'producto.id AS id, producto.nombre AS nombre';
     * $entidad = '';
     *
     * $resultado = $this->integra_columnas_sql($column, $columnas_sql, $entidad);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $entidad esta vacio',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: `$column->atributo` no existe
     * ```php
     * $column = (object)[];
     * $columnas_sql = 'producto.id AS id, producto.nombre AS nombre';
     * $entidad = 'producto';
     *
     * $resultado = $this->integra_columnas_sql($column, $columnas_sql, $entidad);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error atributo no existe en columnas',
     * //     'data' => $column
     * // ]
     * ```
     *
     * @example Error: `$column->atributo` está vacío
     * ```php
     * $column = (object)[
     *     'atributo' => ''
     * ];
     * $columnas_sql = 'producto.id AS id, producto.nombre AS nombre';
     * $entidad = 'producto';
     *
     * $resultado = $this->integra_columnas_sql($column, $columnas_sql, $entidad);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $column->atributo esta vacio',
     * //     'data' => $column
     * // ]
     * ```
     */
    private function integra_columnas_sql(stdClass $column, string $columnas_sql, string $entidad)
    {
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        $coma = $this->coma($columnas_sql);
        if (error::$en_error) {
            return (new error())->error('Error al obtener coma', $coma);
        }

        if (!isset($column->atributo)) {
            return (new error())->error('Error atributo no existe en columnas', $column);
        }

        $column->atributo = trim($column->atributo);
        if ($column->atributo === '') {
            return (new error())->error('Error $column->atributo esta vacio', $column);
        }

        $campo_sql = $this->campo($entidad, $column->atributo);
        if (error::$en_error) {
            return (new error())->error('Error al obtener campo_sql', $campo_sql);
        }

        $campo_sql = $entidad . '.' . $column->atributo . ' AS ' . $column->atributo;

        $columnas_sql .= $coma . $campo_sql;

        return $columnas_sql;
    }


    /**
     * TRASLADADO
     * Integra una cláusula SQL `IN` si el valor proporcionado no está vacío.
     *
     * La función valida que el campo no esté vacío. Si la cadena `$in_sql` no está vacía,
     * se integra una cláusula `IN` utilizando la función `integra_key_in`, agregando el campo
     * correspondiente y la condición `AND` si se especifica. Si ocurre un error durante el proceso,
     * se devuelve un array con los detalles del fallo.
     *
     * @param string $campo El campo que será utilizado en la cláusula `IN`.
     * @param bool $con_and Si es `true`, agrega la condición `AND` antes de la cláusula `IN`.
     * @param string $in_sql La cadena SQL que contiene los valores para la cláusula `IN`.
     *
     * @return string|array Devuelve la cadena SQL con la cláusula `IN` integrada.
     *                      Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function integra_in_sql(string $campo, bool $con_and,string $in_sql)
    {
        $campo = trim($campo);
        if($campo === ''){
            return (new error())->error('Error $campo esta vacio',$campo);
        }
        $in_sql = trim($in_sql);
        if($in_sql !== ''){
            $in_sql = $this->integra_key_in($campo,$con_and,$in_sql);
            if(error::$en_error){
                return (new error())->error('Error al integrar key $in_sql', $in_sql);
            }
        }
        return $in_sql;

    }

    /**
     * EM3
     * Genera una cláusula SQL `LEFT JOIN` utilizando parámetros proporcionados en un objeto estándar.
     *
     * Esta función valida los parámetros de entrada, opcionalmente utiliza un alias para la entidad base,
     * y construye la cláusula `LEFT JOIN` entre las entidades especificadas.
     *
     * @param stdClass $param Un objeto que contiene los parámetros necesarios para la unión:
     *                        - `entidad` (string): El nombre de la entidad base. No debe estar vacío.
     *                        - `entidad_right` (string): El nombre de la entidad que se unirá. No debe estar vacío.
     *                        - `rename_entidad` (string, opcional): Un alias para la entidad base. Puede estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `LEFT JOIN` generada. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar un LEFT JOIN con alias
     * $param = (object)[
     *     'entidad' => 'empleados',
     *     'entidad_right' => 'departamentos',
     *     'rename_entidad' => 'emp'
     * ];
     * $resultado = $this->integra_join($param);
     * echo $resultado;
     * // Resultado:
     * // empleados AS emp ON emp.departamento_id = departamentos.id
     *
     * // Ejemplo 2: Generar un LEFT JOIN sin alias
     * $param = (object)[
     *     'entidad' => 'empleados',
     *     'entidad_right' => 'departamentos'
     * ];
     * $resultado = $this->integra_join($param);
     * echo $resultado;
     * // Resultado:
     * // empleados ON empleados.departamento_id = departamentos.id
     *
     * // Ejemplo 3: Error por entidad vacía
     * $param = (object)[
     *     'entidad' => '',
     *     'entidad_right' => 'departamentos'
     * ];
     * $resultado = $this->integra_join($param);
     * // Resultado:
     * // ['error' => 'Error al validar $param', 'data' => [...]]
     * ```
     */
    private function integra_join(stdClass $param)
    {
        // Validar los parámetros de entrada
        $valida = $this->valida_param($param);
        if (error::$en_error) {
            return (new error())->error('Error al validar $param', $valida);
        }

        // Obtener el alias para la entidad base, si existe
        $rename_entidad = '';
        if (isset($param->rename_entidad)) {
            $rename_entidad = trim($param->rename_entidad);
        }

        // Generar la cláusula LEFT JOIN
        $lj = $this->left_join($param->entidad, $param->entidad_right, '', '', $rename_entidad);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $lj', $lj);
        }

        // Retornar la cláusula LEFT JOIN generada
        return $lj;
    }


    /**
     * TRASLADADO
     * Genera una cláusula SQL `IN` con la posibilidad de agregar una condición `AND` opcional.
     *
     * La función valida que los parámetros de entrada no estén vacíos. Si el valor de `$con_and` es `true`, agrega
     * la palabra clave `AND` al inicio de la cláusula. Luego genera una cláusula `IN` para el campo proporcionado
     * utilizando los valores en `$in_sql`.
     *
     * @param string $campo El nombre del campo que será utilizado en la cláusula `IN`.
     * @param bool $con_and Si es `true`, agrega la palabra clave `AND` al inicio de la cláusula.
     * @param string $in_sql Los valores que serán incluidos dentro de la cláusula `IN` (por ejemplo, una lista de valores separados por comas).
     *
     * @return string|array Devuelve una cadena con la cláusula `IN` generada. Si ocurre un error, devuelve un array con los detalles del fallo.
     */
    private function integra_key_in(string $campo, bool $con_and, string $in_sql)
    {
        $in_sql = trim($in_sql);
        if($in_sql === ''){
            return (new error())->error('Error $in_sql esta vacio',$in_sql);
        }
        $campo = trim($campo);
        if($campo === ''){
            return (new error())->error('Error $campo esta vacio',$campo);
        }
        $and = '';
        if($con_and){
            $and = 'AND';
        }
        $in_sql = "$and $campo IN ($in_sql)";

        return trim($in_sql);

    }

    /**
     * EM3
     * Genera un conjunto de cláusulas SQL `LEFT JOIN` dinámicamente basado en parámetros.
     *
     * Esta función recorre un objeto de parámetros que define las relaciones entre tablas, valida las propiedades necesarias,
     * y construye una lista de cláusulas `LEFT JOIN` concatenadas.
     *
     * @param stdClass $params Un objeto donde:
     *                         - Cada clave representa el nombre de la tabla izquierda.
     *                         - Cada valor es un objeto que incluye:
     *                           - `right`: El nombre de la tabla derecha en la relación.
     *                           - `ren_left`: Un alias opcional para la tabla izquierda.
     *
     * @return string|array Devuelve una cadena SQL con las cláusulas `LEFT JOIN` generadas. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar múltiples LEFT JOINs
     * $params = (object)[
     *     'sala' => (object)['right' => 'sala_usuario', 'ren_left' => 'sala_alias'],
     *     'plaza' => (object)['right' => 'sala', 'ren_left' => 'plaza_alias']
     * ];
     * $resultado = $this->joins($params);
     * echo $resultado;
     * // Resultado:
     * // LEFT JOIN sala AS sala_alias ON sala_alias.id = sala_usuario.sala_id
     * // LEFT JOIN plaza AS plaza_alias ON plaza_alias.id = sala.plaza_id
     *
     * // Ejemplo 2: Error por parámetro mal configurado
     * $params = (object)[
     *     'sala' => (object)['right' => 'sala_usuario'] // Falta 'ren_left'
     * ];
     * $resultado = $this->joins($params);
     * // Resultado:
     * // ['error' => 'Error $param->ren_left no existe', 'data' => {...}]
     * ```
     */
    final public function joins(stdClass $params)
    {
        // Inicializar la cadena de joins
        $joins = '';

        // Recorrer las relaciones definidas en los parámetros
        foreach ($params as $left => $param) {
            // Validar que el parámetro sea un objeto
            if (!is_object($param)) {
                return (new error())->error('Error $param no es un objeto', $param);
            }

            // Validar la existencia de la propiedad 'right'
            if (!isset($param->right)) {
                return (new error())->error('Error $param->right no existe', $param);
            }

            // Validar la existencia de la propiedad 'ren_left'
            if (!isset($param->ren_left)) {
                return (new error())->error('Error $param->ren_left no existe', $param);
            }

            // Validar que 'right' no esté vacío
            if (trim($param->right) === '') {
                return (new error())->error('Error $param->right esta vacia', $param);
            }

            // Generar la cláusula LEFT JOIN
            $lj = $this->left_join_base($left, $param->right, $param->ren_left);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $left join', $lj);
            }

            // Concatenar el LEFT JOIN a la cadena
            $joins .= $lj . ' ';
        }

        // Retornar las cláusulas LEFT JOIN generadas
        return trim($joins);
    }


    /**
     * EM3
     * Genera una cadena SQL con múltiples cláusulas LEFT JOIN.
     *
     * Esta función recibe un array de nombres de tablas o expresiones JOIN y los convierte en una
     * única cadena de texto con las cláusulas `LEFT JOIN` correctamente formateadas. Si un elemento
     * del array está vacío, devuelve un error.
     *
     * ---
     * **Parámetros:**
     *
     * @param array $joins Un array de nombres de tablas o expresiones JOIN a incluir en la consulta.
     *                     Cada elemento debe ser una cadena no vacía representando una tabla o un JOIN.
     *
     * ---
     * **Retorno:**
     *
     * @return string|array Devuelve una cadena con todas las cláusulas `LEFT JOIN` concatenadas y
     *                      correctamente formateadas. En caso de error, devuelve un array con detalles.
     *
     * ---
     * **Ejemplo de uso:**
     *
     * ```php
     * // Definir los JOINs a integrar
     * $joins = [
     *     'clientes AS c ON c.id = ventas.cliente_id',
     *     'productos AS p ON p.id = ventas.producto_id'
     * ];
     *
     * // Generar la cadena de JOINs
     * $resultado = $this->joins_replace($joins);
     *
     * // Imprimir el resultado
     * echo $resultado;
     * ```
     *
     * **Salida esperada:**
     * ```sql
     * LEFT JOIN clientes AS c ON c.id = ventas.cliente_id
     * LEFT JOIN productos AS p ON p.id = ventas.producto_id
     * ```
     *
     * ---
     * **Casos de uso y validaciones:**
     *
     * ```php
     * // Caso 1: JOINs válidos
     * $joins = ['empleados AS e ON e.id = departamentos.empleado_id'];
     * $resultado = $this->joins_replace($joins);
     * echo $resultado;
     * // Salida esperada:
     * // "LEFT JOIN empleados AS e ON e.id = departamentos.empleado_id"
     *
     * // Caso 2: Error por JOIN vacío
     * $joins = ['empleados AS e ON e.id = departamentos.empleado_id', ''];
     * $resultado = $this->joins_replace($joins);
     * print_r($resultado);
     * // Salida esperada:
     * // ['error' => 'Error $join esta vacio', 'data' => '']
     *
     * // Caso 3: Sin JOINs en la entrada
     * $joins = [];
     * $resultado = $this->joins_replace($joins);
     * print_r($resultado);
     * // Salida esperada:
     * // ""
     * ```
     *
     * ---
     * **Notas:**
     * - Si `$joins` está vacío, la función devuelve una cadena vacía (`""`).
     * - Cada elemento de `$joins` debe ser una cadena no vacía con la estructura adecuada.
     * - Si un JOIN está vacío, se genera un error con el mensaje `'Error $join esta vacio'`.
     * - Se usa `trim()` para eliminar espacios innecesarios antes de la validación y la concatenación.
     * - Los JOINs se concatenan con `LEFT JOIN` y se separan por un espacio para mantener la estructura SQL correcta.
     */

    private function joins_replace(array $joins)
    {
        $join_replace = '';
        foreach ($joins as $join){
            $join = trim($join);
            if($join === ''){
                return (new error())->error('Error $join esta vacio',$join);
            }
            $join_replace.="LEFT JOIN $join ";
        }
        return trim($join_replace);

    }

    /**
     * EM3
     * Genera una cadena SQL con múltiples cláusulas `LEFT JOIN` a partir de un array de configuraciones de unión.
     *
     * Esta función valida cada configuración de unión en el array proporcionado, construye las cláusulas `LEFT JOIN`
     * necesarias, y las combina en una única cadena SQL.
     *
     * @param array $joins Un array donde cada elemento es una configuración de unión. Cada configuración debe incluir:
     *                     - `entidad_left` (string): El nombre de la entidad base. No debe estar vacío.
     *                     - `entidad_right` (string): El nombre de la entidad que se unirá. No debe estar vacío.
     *                     Opcionalmente, puede incluir:
     *                     - `rename_entidad` (string): Un alias para la entidad base.
     *
     * @return string|array Devuelve una cadena SQL con las cláusulas `LEFT JOIN` generadas. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar múltiples LEFT JOIN válidos
     * $joins = [
     *     ['entidad_left' => 'empleados', 'entidad_right' => 'departamentos', 'rename_entidad' => 'emp'],
     *     ['entidad_left' => 'departamentos', 'entidad_right' => 'oficinas']
     * ];
     * $resultado = $this->joins_sql($joins);
     * echo $resultado;
     * // Resultado:
     * // LEFT JOIN empleados AS emp ON emp.departamento_id = departamentos.id
     * // LEFT JOIN departamentos ON departamentos.id = oficinas.departamento_id
     *
     * // Ejemplo 2: Error en validación de configuración
     * $joins = [
     *     ['entidad_left' => '', 'entidad_right' => 'departamentos']
     * ];
     * $resultado = $this->joins_sql($joins);
     * // Resultado:
     * // ['error' => 'Error validar $join', 'data' => [...]]
     * ```
     */
    private function joins_sql(array $joins)
    {
        // Inicializar la cadena SQL de uniones
        $joins_sql = '';

        // Recorrer cada configuración de unión en el array
        foreach ($joins as $join) {
            // Validar que las claves requeridas existan en la configuración
            $keys = ['entidad_left', 'entidad_right'];
            $valida = (new valida())->valida_keys($keys, $join);
            if (error::$en_error) {
                return (new error())->error('Error validar $join', $valida);
            }

            // Convertir la configuración a un objeto si es un array
            if (is_array($join)) {
                $join = (object)$join;
            }

            // Asignar la entidad base
            $join->entidad = $join->entidad_left;

            // Generar la cláusula LEFT JOIN
            $left_join = $this->integra_join($join);
            if (error::$en_error) {
                return (new error())->error('Error obtener $left_join', $left_join);
            }

            // Agregar la cláusula LEFT JOIN a la cadena
            $joins_sql .= " LEFT JOIN " . $left_join . " ";
        }

        // Retornar la cadena SQL con las uniones generadas
        return $joins_sql;
    }


    /**
     * EM3
     * Genera una cláusula SQL `LEFT JOIN` entre dos entidades.
     *
     * Esta función valida las entidades y claves de unión, genera los parámetros necesarios para la unión,
     * y construye la cláusula `LEFT JOIN` en formato SQL.
     *
     * @param string $entidad El nombre de la entidad (tabla) base que se utilizará en la unión. No debe estar vacío.
     * @param string $entidad_right El nombre de la entidad (tabla) que se unirá a la izquierda. No debe estar vacío.
     * @param string $key_left La clave de la entidad base que se utilizará en la condición de unión. Puede estar vacío.
     * @param string $key_right La clave de la entidad derecha que se utilizará en la condición de unión. Puede estar vacío.
     * @param string $rename_entidad Un alias opcional para la entidad base. Puede estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `LEFT JOIN` generada. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar LEFT JOIN válido con alias y claves personalizadas
     * $entidad = 'empleados';
     * $entidad_right = 'departamentos';
     * $key_left = 'departamento_id';
     * $key_right = 'id';
     * $rename_entidad = 'emp';
     * $resultado = $this->left_join($entidad, $entidad_right, $key_left, $key_right, $rename_entidad);
     * echo $resultado;
     * // Resultado:
     * // empleados AS emp ON emp.departamento_id = departamentos.id
     *
     * // Ejemplo 2: Error por entidad vacía
     * $entidad = '';
     * $entidad_right = 'departamentos';
     * $resultado = $this->left_join($entidad, $entidad_right);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     * ```
     */
    final public function left_join(
        string $entidad,
        string $entidad_right,
        string $key_left = '',
        string $key_right = '',
        string $rename_entidad = ''
    ) {
        // Validar que la entidad base no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Validar que la entidad derecha no esté vacía
        $entidad_right = trim($entidad_right);
        if ($entidad_right === '') {
            return (new error())->error('Error $entidad_right esta vacia', $entidad_right);
        }

        // Generar los parámetros base para la unión
        $params = $this->params_base_left($entidad, $entidad_right, $key_left, $key_right, $rename_entidad);
        if (error::$en_error) {
            return (new error())->error('Error al integrar $params', $params);
        }

        // Retornar la cláusula LEFT JOIN generada
        return "$params->table ON $params->on";
    }


    /**
     * EM3
     * Genera una cláusula SQL `LEFT JOIN` básica entre dos entidades, con soporte para alias en la tabla izquierda.
     *
     * Esta función valida los parámetros de entrada y construye dinámicamente una cláusula `LEFT JOIN`
     * que une dos entidades (tablas) en una consulta SQL, con la posibilidad de renombrar la entidad izquierda.
     *
     * @param string $entidad_left El nombre de la entidad (tabla) izquierda. No debe estar vacío.
     * @param string $entidad_right El nombre de la entidad (tabla) derecha. No debe estar vacío.
     * @param string $rename_entidad_left Un alias opcional para la entidad izquierda. Si está vacío, se utiliza el nombre original.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `LEFT JOIN` generada. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar un LEFT JOIN básico
     * $entidad_left = 'sala';
     * $entidad_right = 'sala_usuario';
     * $rename_entidad_left = 'sala_alias';
     * $resultado = $this->left_join_base($entidad_left, $entidad_right, $rename_entidad_left);
     * echo $resultado;
     * // Resultado:
     * // LEFT JOIN sala AS sala_alias ON sala_alias.id = sala_usuario.sala_id
     *
     * // Ejemplo 2: Error por entidad izquierda vacía
     * $entidad_left = '';
     * $entidad_right = 'sala_usuario';
     * $rename_entidad_left = 'sala_alias';
     * $resultado = $this->left_join_base($entidad_left, $entidad_right, $rename_entidad_left);
     * // Resultado:
     * // ['error' => 'Error $entidad_left esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Error por entidad derecha vacía
     * $entidad_left = 'sala';
     * $entidad_right = '';
     * $rename_entidad_left = 'sala_alias';
     * $resultado = $this->left_join_base($entidad_left, $entidad_right, $rename_entidad_left);
     * // Resultado:
     * // ['error' => 'Error $entidad_right esta vacia', 'data' => '']
     * ```
     */
    private function left_join_base(string $entidad_left, string $entidad_right, string $rename_entidad_left)
    {
        // Validar que la entidad izquierda no esté vacía
        $entidad_left = trim($entidad_left);
        if ($entidad_left === '') {
            return (new error())->error('Error $entidad_left esta vacia', $entidad_left);
        }

        // Validar que la entidad derecha no esté vacía
        $entidad_right = trim($entidad_right);
        if ($entidad_right === '') {
            return (new error())->error('Error $entidad_right esta vacia', $entidad_right);
        }

        // Validar y asignar alias a la entidad izquierda
        $rename_entidad_left = trim($rename_entidad_left);
        if ($rename_entidad_left === '') {
            $rename_entidad_left = $entidad_left;
        }

        // Obtener la representación SQL de la entidad izquierda con alias
        $ent = $this->entidad_base_as($entidad_left, $rename_entidad_left);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $ent', $ent);
        }

        // Generar la cláusula ON para el JOIN
        $on = $this->on_para_join($rename_entidad_left, $entidad_right, '', '');
        if (error::$en_error) {
            return (new error())->error('Error al obtener $on', $on);
        }

        // Construir y retornar la cláusula LEFT JOIN
        return "LEFT JOIN $ent ON $on";
    }


    /**
     * EM3
     * Genera una cláusula SQL `LEFT JOIN` inicial entre dos entidades (tablas).
     *
     * Esta función valida los nombres de las entidades, genera sus representaciones SQL con alias,
     * y crea una cláusula SQL `LEFT JOIN` con las condiciones de unión basadas en identificadores.
     *
     * @param string $entidad_left El nombre de la entidad (tabla) a la izquierda del `LEFT JOIN`. No debe estar vacío.
     * @param string $entidad_right El nombre de la entidad (tabla) a la derecha del `LEFT JOIN`. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `LEFT JOIN`. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula LEFT JOIN válida
     * $entidad_left = 'empleados';
     * $entidad_right = 'departamentos';
     * $resultado = $this->left_join_inicial($entidad_left, $entidad_right);
     * echo $resultado;
     * // Resultado:
     * // empleados AS empleados LEFT JOIN departamentos AS departamentos ON departamentos_id = empleados.id
     *
     * // Ejemplo 2: Entidad izquierda vacía (error)
     * $entidad_left = '';
     * $entidad_right = 'departamentos';
     * $resultado = $this->left_join_inicial($entidad_left, $entidad_right);
     * // Resultado:
     * // ['error' => 'Error $entidad_left esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Entidad derecha vacía (error)
     * $entidad_left = 'empleados';
     * $entidad_right = '';
     * $resultado = $this->left_join_inicial($entidad_left, $entidad_right);
     * // Resultado:
     * // ['error' => 'Error $entidad_right esta vacio', 'data' => '']
     * ```
     */
    final public function left_join_inicial(string $entidad_left, string $entidad_right)
    {
        // Validar que la entidad izquierda no esté vacía
        $entidad_left = trim($entidad_left);
        if ($entidad_left === '') {
            return (new error())->error('Error $entidad_left esta vacio', $entidad_left);
        }

        // Validar que la entidad derecha no esté vacía
        $entidad_right = trim($entidad_right);
        if ($entidad_right === '') {
            return (new error())->error('Error $entidad_right esta vacio', $entidad_right);
        }

        // Generar representación SQL con alias para la entidad izquierda
        $entidad_left_sql = $this->entidad_base_as($entidad_left, $entidad_left);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $entidad_left_sql', $entidad_left_sql);
        }

        // Generar representación SQL con alias para la entidad derecha
        $entidad_right_sql = $this->entidad_base_as($entidad_right, $entidad_right);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $entidad_right_sql', $entidad_right_sql);
        }

        // Generar condición de unión
        $on = $this->on_para_join($entidad_left, $entidad_right, $entidad_right . '_id', 'id');
        if (error::$en_error) {
            return (new error())->error('Error al obtener $on', $on);
        }

        // Retornar la cláusula LEFT JOIN
        return "$entidad_left_sql LEFT JOIN $entidad_right_sql ON $on";
    }


    /**
     * EM3
     * Genera un conjunto de LEFT JOINs SQL a partir de una lista de parámetros.
     *
     * Esta función recibe un array de parámetros, valida cada parámetro y genera una estructura de
     * LEFT JOIN SQL basada en la información proporcionada. Si un parámetro es un array, se convierte
     * en objeto antes de su procesamiento.
     *
     * ---
     * **Parámetros:**
     *
     * @param array $params Un array de parámetros que definen los LEFT JOINs a generar.
     *                      Cada elemento debe ser un array asociativo o un objeto con la siguiente estructura mínima:
     *                      - 'tabla' (string) → Nombre de la tabla a unir.
     *                      - 'alias' (string) → Alias opcional para la tabla.
     *                      - 'on' (string) → Condición ON de la unión.
     *
     * ---
     * **Retorno:**
     *
     * @return array Retorna un array de cadenas SQL con los LEFT JOINs generados.
     *               Si ocurre un error, devuelve un array con los detalles del error.
     *
     * ---
     * **Ejemplo de uso:**
     *
     * ```php
     * // Definir los parámetros para los LEFT JOINs
     * $params = [
     *     ['tabla' => 'clientes', 'alias' => 'c', 'on' => 'c.id = ventas.cliente_id'],
     *     ['tabla' => 'productos', 'alias' => 'p', 'on' => 'p.id = ventas.producto_id']
     * ];
     *
     * // Crear una instancia de la clase y generar los JOINs
     * $joins = $this->left_joins($params);
     *
     * // Imprimir los resultados
     * print_r($joins);
     * ```
     *
     * **Salida esperada:**
     * ```php
     * [
     *     "LEFT JOIN clientes AS c ON c.id = ventas.cliente_id",
     *     "LEFT JOIN productos AS p ON p.id = ventas.producto_id"
     * ]
     * ```
     *
     * ---
     * **Casos de uso y validaciones:**
     *
     * ```php
     * // Caso 1: Parámetro como array (convertido automáticamente a objeto)
     * $params = [
     *     ['tabla' => 'empleados', 'alias' => 'e', 'on' => 'e.id = departamentos.empleado_id']
     * ];
     * $joins = $this->left_joins($params);
     * // Salida esperada:
     * // ["LEFT JOIN empleados AS e ON e.id = departamentos.empleado_id"]
     *
     * // Caso 2: Error en parámetro no válido (ni array ni objeto)
     * $params = ['tabla' => 'empleados', 'alias' => 'e', 'on' => 'e.id = departamentos.empleado_id'];
     * $joins = $this->left_joins($params);
     * // Salida esperada:
     * // ['error' => 'Error $param debe ser un objeto', 'data' => 'empleados']
     *
     * // Caso 3: Validación fallida en el parámetro
     * $params = [
     *     (object)['tabla' => '', 'alias' => 'e', 'on' => 'e.id = departamentos.empleado_id']
     * ];
     * $joins = $this->left_joins($params);
     * // Salida esperada:
     * // ['error' => 'Error al validar $param', 'data' => [...]]
     * ```
     *
     * ---
     * **Notas:**
     * - Si `$param` es un array, se convierte en `stdClass` antes de la validación.
     * - Se usa `valida_param()` para asegurar que cada parámetro contiene la información necesaria.
     * - Si ocurre un error en cualquier paso, se devuelve un array con detalles del error.
     * - La función `integra_join()` se encarga de construir la estructura SQL del LEFT JOIN.
     */
    private function left_joins(array $params): array
    {
        $joins = array();
        foreach ($params as $param){
            if(is_array($param)){
                $param = (object)$param;
            }
            if(!is_object($param)){
                return (new error())->error('Error $param debe ser un objeto',$param);
            }
            $valida = $this->valida_param($param);
            if(error::$en_error){
                return (new error())->error('Error al validar $param',$valida);
            }
            $lj = $this->integra_join($param);
            if(error::$en_error){
                return (new error())->error('Error al integrar $lj',$lj);
            }
            $joins[] = $lj;
        }
        return $joins;

    }

    /**
     * TRASLADADO
     * Genera una cadena SQL de LEFT JOINs a partir de un array de uniones.
     *
     * Esta función toma un array de uniones (LEFT JOINs) y construye una cadena SQL con ellas. Valida que cada clave
     * y valor del array no estén vacíos antes de generar las uniones.
     *
     * @param array $lfs Array asociativo donde la clave es la tabla de la derecha (right) y el valor es la tabla de la izquierda (left) en la unión.
     *
     * @return array|string Retorna una cadena SQL con las uniones LEFT JOIN o un array de error si alguna validación falla.
     */

    final public function left_joins_sql(array $lfs)
    {
        $lfs_sql = '';
        foreach ($lfs as $right=>$left){
            $right = trim($right);
            if($right === ''){
                return (new error())->error('Error $right esta vacia', $right);
            }
            $left = trim($left);
            if($left === ''){
                return (new error())->error('Error $left esta vacia',$left);
            }
            $lf = (new sql())->left_join($right,$left);
            if(error::$en_error){
                return(new error())->error('Error al obtener $lf',$lf);
            }
            $lf = trim($lf);
            $lfs_sql.= " LEFT JOIN ".$lf;
            $lfs_sql = trim($lfs_sql);
        }
        return $lfs_sql;

    }

    /**
     * TRASLADADO
     * Genera una cadena de texto con las cláusulas "LEFT JOIN" basadas en un arreglo de entidades.
     *
     * Esta función toma un arreglo de nombres de entidades, valida que cada entidad no esté vacía,
     * y construye una cadena de texto con las cláusulas "LEFT JOIN" correspondientes para cada entidad.
     * Si alguna entidad está vacía, se devuelve un objeto de error.
     *
     * @param array $lfs Arreglo de nombres de entidades para las cláusulas "LEFT JOIN".
     *
     * @return string|array Retorna una cadena de texto con las cláusulas "LEFT JOIN" para las entidades,
     *                      o un objeto de error si alguna entidad está vacía.
     */
    final public function left_joins_txt(array $lfs)
    {
        $lf_joins = '';
        foreach ($lfs as $lf){
            $lf = trim($lf);
            if($lf === ''){
                return (new error())->error('Error $lf esta vacio',$lf);
            }
            $lf_joins.=" LEFT JOIN $lf ";
            $lf_joins = trim($lf_joins);
        }
        return $lf_joins;

    }

    /**
     * TRASLADADO
     * Realiza múltiples operaciones de "left join" en una serie de entidades y devuelve los resultados.
     *
     * Esta función itera sobre un arreglo de entidades, valida cada una para asegurarse de que
     * contiene las claves necesarias (`left` y `right`), y luego realiza una operación de "left join"
     * para cada par de entidades. Si alguna validación falla o ocurre un error durante el proceso,
     * se retorna un objeto de error.
     *
     * @param array $joins_base Arreglo de arreglos, donde cada uno contiene las claves `left` y `right`
     *                          para realizar la operación de "left join".
     *
     * @return array|error Retorna un arreglo asociativo con los resultados de las operaciones de "left join",
     *                     o un objeto de error si ocurre algún problema durante el proceso.
     */
    final public function rfs_lefts(array $joins_base): array
    {
        $lfs = array();
        foreach ($joins_base as $entidad_left){
            if(!is_array($entidad_left)){
                return (new error())->error('Error $entidad_left debe ser un array', $entidad_left);
            }
            $keys = array('left','right');
            $valida = (new valida())->valida_keys($keys, $entidad_left);
            if(error::$en_error){
                return (new error())->error('Error al validar entidad left', $valida);
            }
            $rs_lf = $this->rs_left($entidad_left);
            if(error::$en_error){
                return (new error())->error('Error al obtener $lf_contrato', $rs_lf);
            }
            $lfs[$entidad_left['left']] = $rs_lf;
        }
        return $lfs;

    }

    /**
     * EM3
     * Genera una cláusula SQL `LIMIT` para limitar el número de resultados de una consulta.
     *
     * Esta función valida que el límite sea mayor a 0 y construye dinámicamente la cláusula SQL `LIMIT`.
     *
     * @param int $limit El número máximo de resultados a devolver. Debe ser mayor a 0.
     *
     * @return string|array Devuelve una cláusula SQL `LIMIT` en el formato `LIMIT valor`.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar una cláusula LIMIT
     * $limit = 10;
     * $resultado = $this->limit_base($limit);
     * echo $resultado;
     * // Resultado:
     * // LIMIT 10
     *
     * // Ejemplo 2: Error por límite menor o igual a 0
     * $limit = 0;
     * $resultado = $this->limit_base($limit);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $limit debe ser mayor a 0', 'data' => 0]
     * ```
     */
    private function limit_base(int $limit)
    {
        // Validar que el límite sea mayor a 0
        if ($limit <= 0) {
            return (new error())->error('Error $limit debe ser mayor a 0', $limit);
        }

        // Generar y retornar la cláusula LIMIT
        return "LIMIT $limit";
    }



    /**
     * EM3
     * Genera una consulta SQL para contar el número de filas en una entidad donde un campo tiene un valor específico.
     *
     * Esta función valida los parámetros proporcionados (entidad, campo y valor), y construye una consulta SQL
     * que cuenta las filas (`COUNT(*)`) de la entidad (`$entidad`) donde el campo (`$campo`) coincide con el valor (`$value`).
     *
     * @param string $campo El nombre del campo que se utilizará en la condición `WHERE`. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla) en la que se realizará la consulta. No debe estar vacía.
     * @param string $value El valor que se buscará en el campo. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la consulta generada. En caso de error, devuelve un array
     *                      con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar consulta válida
     * $campo = 'status';
     * $entidad = 'empleados';
     * $value = 'activo';
     * $resultado = $this->n_rows($campo, $entidad, $value);
     * echo $resultado;
     * // Resultado:
     * // SELECT COUNT(*) AS n_rows FROM empleados WHERE status = 'activo'
     *
     * // Ejemplo 2: Campo vacío (error)
     * $campo = '';
     * $entidad = 'empleados';
     * $value = 'activo';
     * $resultado = $this->n_rows($campo, $entidad, $value);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacia', 'data' => '']
     *
     * // Ejemplo 3: Entidad vacía (error)
     * $campo = 'status';
     * $entidad = '';
     * $value = 'activo';
     * $resultado = $this->n_rows($campo, $entidad, $value);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     * ```
     */
    final public function n_rows(string $campo, string $entidad, string $value)
    {
        // Eliminar espacios en blanco alrededor de los parámetros
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        $campo = trim($campo);
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacia', $campo);
        }

        $value = trim($value);
        if ($value === '') {
            return (new error())->error('Error $value esta vacia', $value);
        }

        // Retorna la consulta SQL para contar filas
        return /** @lang MYSQL */ "SELECT COUNT(*) AS n_rows FROM $entidad WHERE $campo = '$value'";
    }


    /**
     * EM3
     * Obtiene el nombre de la tabla basado en un nombre original y uno renombrado.
     *
     * Esta función devuelve el nombre de la tabla, priorizando el nombre renombrado si está definido.
     * Si ambos parámetros están vacíos, genera un error.
     *
     * @param string $tabla_renombrada El nombre renombrado de la tabla. Puede estar vacío.
     * @param string $tabla_original El nombre original de la tabla. Puede estar vacío.
     *
     * @return string|array Devuelve el nombre de la tabla (renombrada u original). En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Nombre renombrado proporcionado
     * $tabla_renombrada = 'empleados_temp';
     * $tabla_original = 'empleados';
     * $resultado = $this->obten_nombre_tabla($tabla_renombrada, $tabla_original);
     * echo $resultado;
     * // Resultado:
     * // empleados_temp
     *
     * // Ejemplo 2: Nombre renombrado vacío, se usa el original
     * $tabla_renombrada = '';
     * $tabla_original = 'empleados';
     * $resultado = $this->obten_nombre_tabla($tabla_renombrada, $tabla_original);
     * echo $resultado;
     * // Resultado:
     * // empleados
     *
     * // Ejemplo 3: Ambos nombres vacíos
     * $tabla_renombrada = '';
     * $tabla_original = '';
     * $resultado = $this->obten_nombre_tabla($tabla_renombrada, $tabla_original);
     * // Resultado:
     * // ['error' => 'Error no pueden venir vacios todos los parametros', 'data' => '']
     * ```
     */
    final public function obten_nombre_tabla(string $tabla_renombrada, string $tabla_original)
    {
        // Validar que al menos uno de los parámetros no esté vacío
        if (trim($tabla_original) === '' && trim($tabla_renombrada) === '') {
            return (new error())->error('Error no pueden venir vacios todos los parametros', $tabla_renombrada);
        }

        // Priorizar el nombre renombrado si está definido
        if ($tabla_renombrada !== '') {
            $tabla_nombre = $tabla_renombrada;
        } else {
            $tabla_nombre = $tabla_original;
        }

        // Retornar el nombre de la tabla
        return $tabla_nombre;
    }


    /**
     * EM3
     * Genera una condición SQL para la cláusula `ON` en una unión (`JOIN`).
     *
     * Esta función construye una condición para una unión SQL utilizando los nombres de las entidades
     * (tablas) izquierda y derecha, así como las claves asociadas. Si las claves no se especifican,
     * se usan valores predeterminados (`id` para la izquierda y `{entidad_left}_id` para la derecha).
     *
     * @param string $entidad_left El nombre de la entidad (tabla) izquierda en la unión. No debe estar vacío.
     * @param string $entidad_right El nombre de la entidad (tabla) derecha en la unión. No debe estar vacío.
     * @param string $key_left (Opcional) La clave de la entidad izquierda. Por defecto es `id`.
     * @param string $key_right (Opcional) La clave de la entidad derecha. Por defecto es `{entidad_left}_id`.
     *
     * @return string|array Devuelve una cadena con la condición `ON` para la unión SQL.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar condición `ON` con claves específicas
     * $entidad_left = 'tabla1';
     * $entidad_right = 'tabla2';
     * $key_left = 'id';
     * $key_right = 'tabla1_id';
     * $resultado = $this->on_para_join($entidad_left, $entidad_right, $key_left, $key_right);
     * echo $resultado;
     * // Resultado:
     * // tabla1.id = tabla2.tabla1_id
     *
     * // Ejemplo 2: Usar valores predeterminados para las claves
     * $entidad_left = 'tabla1';
     * $entidad_right = 'tabla2';
     * $key_left = '';
     * $key_right = '';
     * $resultado = $this->on_para_join($entidad_left, $entidad_right, $key_left, $key_right);
     * echo $resultado;
     * // Resultado:
     * // tabla1.id = tabla2.tabla1_id
     *
     * // Ejemplo 3: Entidad izquierda vacía (error)
     * $entidad_left = '';
     * $entidad_right = 'tabla2';
     * $key_left = 'id';
     * $key_right = 'tabla1_id';
     * $resultado = $this->on_para_join($entidad_left, $entidad_right, $key_left, $key_right);
     * // Resultado:
     * // ['error' => 'Error $entidad_left esta vacia', 'data' => '']
     * ```
     */
    final public function on_para_join(string $entidad_left, string $entidad_right, string $key_left, string $key_right)
    {
        // Validar que la entidad izquierda no esté vacía
        $entidad_left = trim($entidad_left);
        if ($entidad_left === '') {
            return (new error())->error('Error $entidad_left esta vacia', $entidad_left);
        }

        // Validar que la entidad derecha no esté vacía
        $entidad_right = trim($entidad_right);
        if ($entidad_right === '') {
            return (new error())->error('Error $entidad_right esta vacia', $entidad_right);
        }

        // Asignar clave izquierda predeterminada si está vacía
        $key_left = trim($key_left);
        if ($key_left === '') {
            $key_left = 'id';
        }

        // Asignar clave derecha predeterminada si está vacía
        $key_right = trim($key_right);
        if ($key_right === '') {
            $key_right = $entidad_left . '_id';
        }

        // Construir y retornar la condición ON para la unión
        return "$entidad_left.$key_left = $entidad_right.$key_right";
    }


    /**
     * EM3
     * Genera una cláusula SQL `ORDER BY` para ordenar resultados de una consulta.
     *
     * Esta función valida que el campo no esté vacío y construye dinámicamente la cláusula SQL `ORDER BY` con el campo
     * y el tipo de orden especificados.
     *
     * @param string $campo El nombre del campo por el cual se ordenarán los resultados. No puede estar vacío.
     * @param string $type El tipo de orden (`ASC` para ascendente, `DESC` para descendente). No se valida su contenido.
     *
     * @return string|array Devuelve una cláusula SQL `ORDER BY` en el formato `ORDER BY campo type`.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar una cláusula ORDER BY ascendente
     * $campo = 'nombre';
     * $type = 'ASC';
     * $resultado = $this->order_by_base($campo, $type);
     * echo $resultado;
     * // Resultado:
     * // ORDER BY nombre ASC
     *
     * // Ejemplo 2: Generar una cláusula ORDER BY descendente
     * $campo = 'id';
     * $type = 'DESC';
     * $resultado = $this->order_by_base($campo, $type);
     * echo $resultado;
     * // Resultado:
     * // ORDER BY id DESC
     *
     * // Ejemplo 3: Error por campo vacío
     * $campo = '';
     * $type = 'ASC';
     * $resultado = $this->order_by_base($campo, $type);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     * ```
     */
    private function order_by_base(string $campo, string $type)
    {
        // Limpiar el nombre del campo
        $campo = trim($campo);

        // Validar que el campo no esté vacío
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        // Generar y retornar la cláusula ORDER BY
        return "ORDER BY $campo $type";
    }


    /**
     * EM3
     * Genera parámetros base para una unión SQL `LEFT JOIN` entre dos entidades.
     *
     * Esta función valida las entidades y claves de unión proporcionadas, y genera los parámetros necesarios
     * para construir una unión SQL `LEFT JOIN`, incluyendo el alias de la entidad y la condición de unión.
     *
     * @param string $entidad El nombre de la entidad (tabla) que se utilizará como base. No debe estar vacío.
     * @param string $entidad_right El nombre de la entidad (tabla) que se unirá a la izquierda. No debe estar vacío.
     * @param string $key_left La clave de la entidad izquierda que se utilizará en la condición de unión. No debe estar vacío.
     * @param string $key_right La clave de la entidad derecha que se utilizará en la condición de unión. No debe estar vacío.
     * @param string $rename Un alias opcional para la entidad izquierda. Puede estar vacío.
     *
     * @return stdClass|array Devuelve un objeto con los parámetros generados para la unión SQL. Incluye:
     *                        - `table`: La representación SQL de la tabla base con su alias.
     *                        - `on`: La condición de unión generada.
     *                        En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar parámetros válidos para una unión LEFT JOIN
     * $entidad = 'empleados';
     * $entidad_right = 'departamentos';
     * $key_left = 'departamento_id';
     * $key_right = 'id';
     * $rename = 'emp';
     * $resultado = $this->params_base_left($entidad, $entidad_right, $key_left, $key_right, $rename);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [table] => 'empleados AS emp',
     * //     [on] => 'emp.departamento_id = departamentos.id'
     * // )
     *
     * // Ejemplo 2: Error por entidad vacía
     * $entidad = '';
     * $resultado = $this->params_base_left($entidad, $entidad_right, $key_left, $key_right, $rename);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     * ```
     */
    private function params_base_left(
        string $entidad,
        string $entidad_right,
        string $key_left,
        string $key_right,
        string $rename
    ) {
        // Validar que la entidad base no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Validar que la entidad derecha no esté vacía
        $entidad_right = trim($entidad_right);
        if ($entidad_right === '') {
            return (new error())->error('Error $entidad_right esta vacia', $entidad_right);
        }

        // Generar representación SQL de la entidad base con alias
        $table = $this->entidad_base_as($entidad, $rename);
        if (error::$en_error) {
            return (new error())->error('Error al integrar entidad base', $table);
        }

        // Determinar el alias de la entidad izquierda
        $entidad_left = $entidad;
        $rename = trim($rename);
        if ($rename !== '') {
            $entidad_left = $rename;
        }

        // Generar la condición de unión
        $on = $this->on_para_join($entidad_left, $entidad_right, $key_left, $key_right);
        if (error::$en_error) {
            return (new error())->error('Error al integrar on', $on);
        }

        // Crear y retornar los parámetros de unión
        $params = new stdClass();
        $params->table = $table;
        $params->on = $on;
        return $params;
    }


    /**
     * EM3
     * Genera los parámetros necesarios para construir una lista SQL de campos, incluyendo el nombre del campo SQL
     * y una coma para separar múltiples campos.
     *
     * Esta función valida los valores de entrada, genera el nombre del campo SQL con la entidad asociada, y determina
     * si se necesita una coma para separar el campo en la lista SQL.
     *
     * @param string $campo El nombre del campo. No puede estar vacío.
     * @param string $campos_sql La lista actual de campos SQL. Puede estar vacía al inicio.
     * @param string $entidad El nombre de la entidad o tabla asociada al campo. No puede estar vacío.
     *
     * @return stdClass|array Devuelve un objeto `stdClass` con los parámetros generados:
     *                        - `campo_sql`: El nombre del campo SQL en el formato `entidad.campo`.
     *                        - `coma`: Una coma para separar el campo si la lista actual no está vacía.
     *                        En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar parámetros para un campo SQL
     * $campo = 'id';
     * $campos_sql = 'usuario.id AS usuario_id';
     * $entidad = 'usuario';
     * $resultado = $this->params_campo($campo, $campos_sql, $entidad);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object
     * // (
     * //     [campo_sql] => usuario.id AS usuario_id
     * //     [coma] => ,
     * // )
     *
     * // Ejemplo 2: Error por campo vacío
     * $campo = '';
     * $campos_sql = 'usuario.id AS usuario_id';
     * $entidad = 'usuario';
     * $resultado = $this->params_campo($campo, $campos_sql, $entidad);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     * ```
     */
    private function params_campo(string $campo, string $campos_sql, string $entidad)
    {
        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        // Validar que el campo no esté vacío
        $campo = trim($campo);
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        // Generar la coma si la lista de campos SQL no está vacía
        $coma = $this->coma($campos_sql);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $coma', $coma);
        }

        // Generar el nombre del campo SQL en el formato `entidad.campo`
        $campo_sql = $this->campo($entidad, $campo);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $campo_sql', $campo_sql);
        }

        // Crear el objeto con los parámetros generados
        $params = new stdClass();
        $params->campo_sql = $campo_sql;
        $params->coma = $coma;

        // Retornar los parámetros
        return $params;
    }


    /**
     * EM3
     * Genera parámetros para construir una declaración SQL de un campo con alias y su separador (coma).
     *
     * Esta función valida que el campo no esté vacío, genera su declaración SQL con alias utilizando
     * `campo_sql`, y determina si debe agregarse una coma como separador usando `coma`. Retorna un objeto
     * con los datos procesados del campo.
     *
     * @param string $campo El nombre del campo a procesar. No debe estar vacío.
     * @param string $campos_sql La cadena SQL actual acumulada, utilizada para determinar si se necesita una coma.
     *
     * @return stdClass|array Devuelve un objeto con las siguientes claves:
     *                        - `campo`: El nombre del campo original.
     *                        - `campo_sql`: La declaración SQL del campo con alias.
     *                        - `coma`: Una coma (`,`) si corresponde, o una cadena vacía si no.
     *                        En caso de error, devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar parámetros para un campo SQL
     * $campo = 'tabla.columna';
     * $campos_sql = 'tabla.id AS tabla_id';
     * $resultado = $this->param_campo_sql($campo, $campos_sql);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [campo] => tabla.columna
     * //     [campo_sql] => tabla.columna AS tabla_columna
     * //     [coma] => ,
     * // )
     *
     * // Ejemplo 2: Campo vacío (error)
     * $campo = '';
     * $campos_sql = 'tabla.id AS tabla_id';
     * $resultado = $this->param_campo_sql($campo, $campos_sql);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Campo con cadena SQL vacía
     * $campo = 'tabla.nombre';
     * $campos_sql = '';
     * $resultado = $this->param_campo_sql($campo, $campos_sql);
     * print_r($resultado);
     * // Resultado:
     * // stdClass Object (
     * //     [campo] => tabla.nombre
     * //     [campo_sql] => tabla.nombre AS tabla_nombre
     * //     [coma] =>
     * // )
     * ```
     */
    private function param_campo_sql(string $campo, string $campos_sql)
    {
        // Eliminar espacios alrededor del campo
        $campo = trim($campo);

        // Validar que el campo no esté vacío
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        // Generar la declaración SQL del campo con alias
        $campo_sql = $this->campo_sql($campo);
        if (error::$en_error) {
            return (new error())->error('Error al generar campo sql', $campo_sql);
        }

        // Determinar si debe agregarse una coma como separador
        $coma = $this->coma($campos_sql);
        if (error::$en_error) {
            return (new error())->error('Error obtener coma', $coma);
        }

        // Crear y retornar el objeto con los parámetros procesados
        $param = new stdClass();
        $param->campo = $campo;
        $param->campo_sql = $campo_sql;
        $param->coma = $coma;

        return $param;
    }


    /**
     * EM3
     * Genera una combinación de cláusulas SQL `ORDER BY` y `LIMIT` para estructurar una consulta.
     *
     * Esta función valida que los parámetros sean válidos, construye una cláusula `ORDER BY` basada en el campo y el tipo de orden,
     * y una cláusula `LIMIT` con el número máximo de resultados permitidos.
     *
     * @param string $campo El nombre del campo para la cláusula `ORDER BY`. No puede estar vacío.
     * @param int $limit El número máximo de resultados a devolver. Debe ser mayor a 0.
     * @param string $type El tipo de orden para `ORDER BY` (`ASC` para ascendente, `DESC` para descendente).
     *
     * @return string|array Devuelve una combinación de cláusulas SQL en el formato `ORDER BY campo type LIMIT valor`.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar una combinación de ORDER BY y LIMIT
     * $campo = 'id';
     * $limit = 10;
     * $type = 'ASC';
     * $resultado = $this->params_sql_base($campo, $limit, $type);
     * echo $resultado;
     * // Resultado:
     * // ORDER BY id ASC LIMIT 10
     *
     * // Ejemplo 2: Error por campo vacío
     * $campo = '';
     * $limit = 10;
     * $type = 'ASC';
     * $resultado = $this->params_sql_base($campo, $limit, $type);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por límite inválido
     * $campo = 'id';
     * $limit = 0;
     * $type = 'ASC';
     * $resultado = $this->params_sql_base($campo, $limit, $type);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $limit debe ser mayor a 0', 'data' => 0]
     * ```
     */
    private function params_sql_base(string $campo, int $limit, string $type)
    {
        // Limpiar el nombre del campo
        $campo = trim($campo);

        // Validar que el campo no esté vacío
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        // Validar que el límite sea mayor a 0
        if ($limit <= 0) {
            return (new error())->error('Error $limit debe ser mayor a 0', $limit);
        }

        // Generar la cláusula ORDER BY
        $order = $this->order_by_base($campo, $type);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $order', $order);
        }

        // Generar la cláusula LIMIT
        $limit = $this->limit_base($limit);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $limit', $limit);
        }

        // Retornar la combinación de cláusulas SQL
        return "$order $limit";
    }


    /**
     * TRASLADADO
     * Obtiene el alias de una entidad para una instrucción SQL `LEFT JOIN`.
     *
     * Esta función revisa si el objeto `$join` contiene la propiedad `rename_entidad_left`.
     * Si la propiedad no existe, se inicializa con una cadena vacía. El valor de la propiedad
     * es devuelto como resultado.
     *
     * @param stdClass $join Objeto que puede contener la propiedad `rename_entidad_left`.
     *                       Si no está definida, será inicializada con una cadena vacía.
     *
     * @return string Devuelve el valor de `rename_entidad_left` si está definido, de lo contrario devuelve una cadena vacía.
     *
     * @example Uso básico:
     * ```php
     * $join = new stdClass();
     * $resultado = $this->rename_entidad($join);
     * echo $resultado; // Salida: ''
     *
     * $join->rename_entidad_left = 'entidad_alias';
     * $resultado = $this->rename_entidad($join);
     * echo $resultado; // Salida: 'entidad_alias'
     * ```
     *
     * @example Uso en contexto SQL:
     * ```php
     * $join = new stdClass();
     * $join->rename_entidad_left = 'entidad_alias';
     *
     * $alias = $this->rename_entidad($join);
     * $sql = "SELECT * FROM tabla LEFT JOIN otra_tabla AS $alias ON tabla.id = otra_tabla.id";
     * echo $sql;
     * // Salida:
     * // SELECT * FROM tabla LEFT JOIN otra_tabla AS entidad_alias ON tabla.id = otra_tabla.id
     * ```
     */
    private function rename_entidad(stdClass $join): string
    {
        // Verifica si la propiedad rename_entidad_left existe en el objeto, si no, la inicializa
        if (!isset($join->rename_entidad_left)) {
            $join->rename_entidad_left = '';
        }

        // Devuelve el valor de rename_entidad_left
        return $join->rename_entidad_left;
    }


    /**
     * EM3
     * Obtiene el valor de `join` de un objeto de tipo `stdClass` si existe, o devuelve una cadena vacía.
     *
     * Esta función verifica si la propiedad `join` está definida en el objeto `$replaces`. Si existe,
     * retorna su valor; de lo contrario, devuelve una cadena vacía.
     *
     * @param stdClass $replaces Un objeto que puede contener la propiedad `join`.
     *
     * @return string Devuelve el valor de la propiedad `join` si está definida, o una cadena vacía si no.
     *
     * @example
     * ```php
     * // Ejemplo 1: Objeto con la propiedad `join`
     * $replaces = (object)[
     *     'join' => 'INNER JOIN tabla2 ON tabla1.id = tabla2.tabla1_id'
     * ];
     * $resultado = $this->replace_join_compare($replaces);
     * echo $resultado;
     * // Resultado:
     * // INNER JOIN tabla2 ON tabla1.id = tabla2.tabla1_id
     *
     * // Ejemplo 2: Objeto sin la propiedad `join`
     * $replaces = (object)[
     *     'where' => 'tabla1.status = "activo"'
     * ];
     * $resultado = $this->replace_join_compare($replaces);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     *
     * // Ejemplo 3: Objeto vacío
     * $replaces = new stdClass();
     * $resultado = $this->replace_join_compare($replaces);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     * ```
     */
    private function replace_join_compare(stdClass $replaces): string
    {
        // Inicializar la variable de retorno
        $replace_join_compare = '';

        // Verificar si la propiedad `join` está definida en el objeto
        if (isset($replaces->join)) {
            $replace_join_compare = $replaces->join;
        }

        // Retornar el valor de `join` o una cadena vacía
        return $replace_join_compare;
    }


    /**
     * EM3
     * Obtiene el valor de `join_replace` de un objeto de tipo `stdClass` si existe, o devuelve una cadena vacía.
     *
     * Esta función verifica si la propiedad `join_replace` está definida en el objeto `$replaces`. Si existe,
     * retorna su valor; de lo contrario, devuelve una cadena vacía.
     *
     * @param stdClass $replaces Un objeto que puede contener la propiedad `join_replace`.
     *
     * @return string Devuelve el valor de la propiedad `join_replace` si está definida, o una cadena vacía si no.
     *
     * @example
     * ```php
     * // Ejemplo 1: Objeto con la propiedad `join_replace`
     * $replaces = (object)[
     *     'join_replace' => 'LEFT JOIN tabla2 ON tabla1.id = tabla2.tabla1_id'
     * ];
     * $resultado = $this->replace_join_rs($replaces);
     * echo $resultado;
     * // Resultado:
     * // LEFT JOIN tabla2 ON tabla1.id = tabla2.tabla1_id
     *
     * // Ejemplo 2: Objeto sin la propiedad `join_replace`
     * $replaces = (object)[
     *     'where' => 'tabla1.status = "activo"'
     * ];
     * $resultado = $this->replace_join_rs($replaces);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     *
     * // Ejemplo 3: Objeto vacío
     * $replaces = new stdClass();
     * $resultado = $this->replace_join_rs($replaces);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     * ```
     */
    private function replace_join_rs(stdClass $replaces): string
    {
        // Inicializar la variable de retorno
        $replace_join_rs = '';

        // Verificar si la propiedad `join_replace` está definida en el objeto
        if (isset($replaces->join_replace)) {
            $replace_join_rs = $replaces->join_replace;
        }

        // Retornar el valor de `join_replace` o una cadena vacía
        return $replace_join_rs;
    }


    /**
     * TRASLADADO
     * Realiza una operación de "left join" en dos entidades basadas en claves especificadas.
     *
     * Esta función valida las claves `left` y `right` en el arreglo `entidad_left`,
     * luego inicializa y obtiene los valores de las claves `key_left` y `key_right`.
     * Si alguna validación falla o ocurre un error al obtener los valores, se retorna un objeto de error.
     * Finalmente, se realiza una operación de "left join" utilizando las claves y entidades proporcionadas.
     *
     * @param array $entidad_left Arreglo asociativo que contiene las entidades `left` y `right` junto con sus
     * respectivas claves.
     *
     * @return array|string Retorna el resultado de la operación de "left join", o un
     * objeto de error si ocurre algún problema durante el proceso.
     */
    private function rs_left(array $entidad_left)
    {
        $keys_compare[] = 'key_left';
        $keys_compare[] = 'key_right';

        $keys = array('left','right');
        $valida = (new valida())->valida_keys($keys, $entidad_left);
        if(error::$en_error){
            return (new error())->error('Error al validar entidad left', $valida);
        }
        $keys = $this->init_values_array($entidad_left,$keys_compare);
        if(error::$en_error){
            return (new error())->error('Error al obtener $keys', $keys);
        }

        $rs_lf = $this->left_join($entidad_left['left'],$entidad_left['right'],$keys->key_left,$keys->key_right);
        if(error::$en_error){
            return (new error())->error('Error al obtener $lf_contrato', $rs_lf);
        }

        return $rs_lf;

    }

    /**
     * EM3
     * Genera una consulta SQL `SELECT` básica con soporte para `JOIN`, `WHERE`, `ORDER BY` y `LIMIT`.
     *
     * Esta función permite construir dinámicamente una consulta SQL con validación de parámetros, generando sentencias
     * seguras y formateadas correctamente.
     *
     * @param string $campos Los campos a seleccionar. Por defecto, selecciona todos (`*`) si está vacío.
     * @param string $entidad_origen El nombre de la entidad o tabla principal de la consulta. No puede estar vacío.
     * @param string $key_id_entidad El campo clave para la condición `WHERE`. No puede estar vacío.
     * @param int $limit El límite de resultados a devolver. Debe ser mayor a 0.
     * @param stdClass $params_join Un objeto con las configuraciones de los `JOIN` necesarios en la consulta.
     * @param string $type_order El tipo de orden (`ASC` para ascendente, `DESC` para descendente) en la cláusula `ORDER BY`.
     * @param string $value El valor a buscar en la condición `WHERE`. Puede estar vacío dependiendo del operador.
     * @param string $type_operador El tipo de operador para la condición `WHERE` (`LIKE` o `IGUAL`). Por defecto, `LIKE`.
     *
     * @return string|array Devuelve una consulta SQL completa en el formato:
     *                      `SELECT campos FROM entidad JOINs WHERE condición ORDER BY campo LIMIT valor;`.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar una consulta SELECT básica
     * $campos = 'id, nombre, email';
     * $entidad_origen = 'usuario';
     * $key_id_entidad = 'nombre';
     * $limit = 10;
     * $params_join = new stdClass();
     * $type_order = 'ASC';
     * $value = 'Juan';
     * $type_operador = 'LIKE';
     * $resultado = $this->select_base($campos, $entidad_origen, $key_id_entidad, $limit, $params_join, $type_order, $value, $type_operador);
     * echo $resultado;
     * // Resultado:
     * // SELECT id, nombre, email FROM usuario AS usuario WHERE nombre LIKE '%Juan%' ORDER BY nombre ASC LIMIT 10;
     *
     * // Ejemplo 2: Error por entidad vacía
     * $campos = 'id, nombre';
     * $entidad_origen = '';
     * $resultado = $this->select_base($campos, $entidad_origen, $key_id_entidad, $limit, $params_join, $type_order, $value, $type_operador);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $entidad_origen esta vacia', 'data' => '']
     * ```
     */
    final public function select_base(
        string $campos,
        string $entidad_origen,
        string $key_id_entidad,
        int $limit,
        stdClass $params_join,
        string $type_order,
        string $value,
        string $type_operador = 'LIKE'
    ) {
        // Validar que la entidad principal no esté vacía
        $entidad_origen = trim($entidad_origen);
        if ($entidad_origen === '') {
            return (new error())->error('Error $entidad_origen esta vacia', $entidad_origen);
        }

        // Validar el operador
        $valida = $this->valida_type_operador($type_operador);
        if (error::$en_error) {
            return (new error())->error('Error al validar $type_operador', $valida);
        }
        $type_operador = strtoupper($type_operador);

        // Validar que el campo clave no esté vacío
        $key_id_entidad = trim($key_id_entidad);
        if ($key_id_entidad === '') {
            return (new error())->error('Error $key_id_entidad esta vacio', $key_id_entidad);
        }

        // Validar que el límite sea mayor a 0
        if ($limit <= 0) {
            return (new error())->error('Error $limit debe ser mayor a 0', $limit);
        }

        // Usar todos los campos si no se especifican
        $campos = trim($campos);
        if ($campos === '') {
            $campos = '*';
        }

        // Generar alias para la entidad principal
        $entidad = $this->entidad_base_as($entidad_origen, $entidad_origen);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $entidad', $entidad);
        }

        // Generar las cláusulas JOIN
        $joins = $this->joins($params_join);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $joins', $joins);
        }

        // Generar la cláusula WHERE
        $sentencia = $this->where_sentencia_base($key_id_entidad, $type_operador, $value);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $sentencia', $sentencia);
        }

        // Generar las cláusulas ORDER BY y LIMIT
        $params_sql = $this->params_sql_base($key_id_entidad, $limit, $type_order);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $params_sql', $params_sql);
        }

        // Retornar la consulta SQL completa
        return /** @lang MYSQL */ "SELECT $campos FROM $entidad $joins $sentencia $params_sql;";
    }


    /**
     * TRASLADADOS
     * Genera una consulta SQL básica basada en una entidad, un orden y parámetros específicos.
     *
     * Esta función toma la entidad base, el orden de la consulta y un objeto `stdClass` que contiene los
     * parámetros necesarios (`joins`, `campos` y `filtro`). Valida que la entidad base y el filtro no estén
     * vacíos, asigna valores predeterminados a los joins y campos si no están definidos, y construye una consulta
     * SQL básica. Si alguna validación falla, se devuelve un array con los detalles del error.
     *
     * @param string $entidad_base Nombre de la entidad principal para la consulta.
     * @param string $order Cláusula `ORDER BY` para ordenar los resultados.
     * @param stdClass $params Objeto que contiene los parámetros `joins`, `campos` y `filtro` para la consulta.
     *
     * @return string|array Devuelve la cadena SQL generada si el proceso es exitoso,
     *                      o un array de error si ocurre algún problema durante la validación o generación de la consulta.
     */
    final public function select_base_sql(string $entidad_base, string $order, stdClass $params)
    {
        $entidad_base = trim($entidad_base);
        if($entidad_base === ''){
            return (new error())->error('Error $entidad_base esta vacia', $entidad_base);
        }
        if(!isset($params->joins)){
            $params->joins = '';
        }
        if(!isset($params->campos)){
            $params->campos = '*';
        }
        if(!isset($params->filtro)) {
            return (new error())->error('Error  $params->filtro debe existir', $params);
        }
        $entidad_base_sql = "$entidad_base AS $entidad_base";
        $entidades_sql = "$entidad_base_sql $params->joins";
        return trim(/** @lang MYSQL */ "SELECT $params->campos FROM $entidades_sql WHERE $params->filtro $order");

    }

    /**
     * TRASLADADO
     * Genera una consulta SQL `SELECT` con un `LEFT JOIN` entre dos entidades y una cláusula `WHERE`.
     *
     * Esta función construye una instrucción SQL `SELECT` utilizando un `LEFT JOIN` entre
     * dos tablas o entidades especificadas. Permite personalizar los campos a seleccionar,
     * las entidades involucradas y los criterios del filtro `WHERE`.
     *
     * @param string $campos_sql Campos a seleccionar en la consulta SQL. Por defecto selecciona todos (`*`).
     * @param string $entidad_left Nombre de la tabla o entidad en el lado izquierdo del `LEFT JOIN`.
     *                             Este valor no puede estar vacío.
     * @param string $entidad_right Nombre de la tabla o entidad en el lado derecho del `LEFT JOIN`.
     *                              Este valor no puede estar vacío.
     * @param string $st_where Cláusula `WHERE` para filtrar los resultados de la consulta.
     *                         Este valor no puede estar vacío.
     *
     * @return string|array Devuelve la consulta SQL generada como una cadena. En caso de error, retorna un arreglo de error.
     *
     * @example Uso básico:
     * ```php
     * $campos_sql = 'a.id, b.nombre';
     * $entidad_left = 'tabla_a AS a';
     * $entidad_right = 'tabla_b AS b';
     * $st_where = 'a.id = b.id AND a.status = "activo"';
     *
     * $sql = $this->select_base_left($campos_sql, $entidad_left, $entidad_right, $st_where);
     * echo $sql;
     * // Salida:
     * // SELECT a.id, b.nombre FROM tabla_a AS a LEFT JOIN tabla_b AS b ON a.id = b.id WHERE a.id = b.id AND a.status = "activo";
     * ```
     *
     * @throws error Lanza un error si alguno de los parámetros requeridos está vacío.
     */
    final public function select_base_left(
        string $campos_sql, string $entidad_left, string $entidad_right, string $st_where)
    {
        // Valida y asigna campos seleccionados, si no se especifica, selecciona todos los campos.
        $campos_sql = trim($campos_sql);
        if ($campos_sql === '') {
            $campos_sql = '*';
        }

        // Valida que $entidad_left no esté vacío.
        $entidad_left = trim($entidad_left);
        if ($entidad_left === '') {
            return (new error())->error('Error $entidad_left esta vacio', $entidad_left);
        }

        // Valida que $entidad_right no esté vacío.
        $entidad_right = trim($entidad_right);
        if ($entidad_right === '') {
            return (new error())->error('Error $entidad_right esta vacio', $entidad_right);
        }

        // Valida que $st_where no esté vacío.
        $st_where = trim($st_where);
        if ($st_where === '') {
            return (new error())->error('Error $st_where esta vacio', $st_where);
        }

        // Genera la instrucción LEFT JOIN.
        $left = $this->left_join_inicial($entidad_left, $entidad_right);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $left', $left);
        }

        // Retorna la consulta SQL completa.
        return /** @lang MYSQL */ "SELECT $campos_sql FROM $left WHERE $st_where;";
    }



    /**
     * EM3
     * Genera una consulta SQL `SELECT` basada en un `id` específico.
     *
     * Este método construye una consulta `SELECT` utilizando una lista de columnas proporcionada
     * y filtrando los resultados con una condición `WHERE` basada en el `id` del registro.
     *
     * ### Validaciones
     * - Verifica que `$entidad` no esté vacío.
     * - Confirma que `$registro_id` sea mayor a 0.
     * - Genera la lista de columnas SQL llamando a `columnas_sql()`.
     * - Ajusta las condiciones adicionales (`$sql_where_extra`) si se proporcionan.
     *
     * ### Proceso
     * 1. Se valida que `$entidad` sea una cadena no vacía.
     * 2. Se valida que `$registro_id` sea un número mayor a 0.
     * 3. Se genera la lista de columnas utilizando `columnas_sql()`.
     * 4. Se construye la clave primaria con el formato `entidad.id`.
     * 5. Se ajustan las condiciones adicionales (`$sql_where_extra`).
     * 6. Se genera y retorna la consulta `SELECT`.
     *
     * @param stdClass $columnas Objeto que contiene la lista de columnas a incluir en la consulta SQL.
     *                           Cada propiedad de `$columnas` representa una columna con:
     *                           - `atributo` (string): Nombre del campo a incluir.
     *
     * @param string $entidad Nombre de la tabla o entidad a la que pertenecen los datos.
     *                        Debe ser una cadena no vacía.
     *                        Ejemplo de entrada:
     *                        ```php
     *                        'producto'
     *                        ```
     *
     * @param int $registro_id ID del registro a seleccionar.
     *                         Debe ser un número mayor a 0.
     *                         Ejemplo de entrada:
     *                         ```php
     *                         123
     *                         ```
     *
     * @param string $sql_where_extra (Opcional) Condiciones adicionales para la cláusula `WHERE`.
     *                                Se incluirán con `AND` si se proporciona.
     *                                Ejemplo de entrada:
     *                                ```php
     *                                "producto.estado = 'activo'"
     *                                ```
     *
     * @return string|array Devuelve una cadena con la consulta `SELECT` generada.
     *                En caso de error, devuelve un array con los detalles del fallo.
     *
     * @throws error Si `$entidad` está vacío, si `$registro_id` es menor a 0,
     *               o si ocurre un error al generar la lista de columnas.
     *
     * @example Entrada válida:
     * ```php
     * $columnas = (object)[
     *     (object)['atributo' => 'id'],
     *     (object)['atributo' => 'nombre'],
     *     (object)['atributo' => 'precio']
     * ];
     * $entidad = 'producto';
     * $registro_id = 123;
     * $sql_where_extra = "producto.estado = 'activo'";
     *
     * $resultado = $this->select_by_id($columnas, $entidad, $registro_id, $sql_where_extra);
     *
     * // Resultado esperado:
     * // "SELECT producto.id AS id, producto.nombre AS nombre, producto.precio AS precio
     * // FROM producto WHERE producto.id = 123 AND producto.estado = 'activo'"
     * ```
     *
     * @example Entrada sin `$sql_where_extra`:
     * ```php
     * $columnas = (object)[
     *     (object)['atributo' => 'id'],
     *     (object)['atributo' => 'nombre']
     * ];
     * $entidad = 'usuario';
     * $registro_id = 45;
     *
     * $resultado = $this->select_by_id($columnas, $entidad, $registro_id);
     *
     * // Resultado esperado:
     * // "SELECT usuario.id AS id, usuario.nombre AS nombre FROM usuario WHERE usuario.id = 45"
     * ```
     *
     * @example Error: `$entidad` está vacío
     * ```php
     * $columnas = (object)[
     *     (object)['atributo' => 'id']
     * ];
     * $entidad = '';
     * $registro_id = 123;
     *
     * $resultado = $this->select_by_id($columnas, $entidad, $registro_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $entidad esta vacia',
     * //     'data' => ''
     * // ]
     * ```
     *
     * @example Error: `$registro_id` menor o igual a 0
     * ```php
     * $columnas = (object)[
     *     (object)['atributo' => 'id']
     * ];
     * $entidad = 'producto';
     * $registro_id = 0;
     *
     * $resultado = $this->select_by_id($columnas, $entidad, $registro_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error $registro_id es menor a 0',
     * //     'data' => 0
     * // ]
     * ```
     *
     * @example Error: Fallo en `columnas_sql()`
     * ```php
     * $columnas = (object)[
     *     (object)['atributo' => '']
     * ];
     * $entidad = 'producto';
     * $registro_id = 123;
     *
     * $resultado = $this->select_by_id($columnas, $entidad, $registro_id);
     *
     * // Resultado esperado:
     * // [
     * //     'error' => 'Error al obtener $columnas_sql',
     * //     'data' => $columnas
     * // ]
     * ```
     */
    final public function select_by_id(
        stdClass $columnas,
        string $entidad,
        int $registro_id,
        string $sql_where_extra = ''
    ) {
        // Valida que $entidad no esté vacío
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Valida que $registro_id sea mayor a 0
        if ($registro_id <= 0) {
            return (new error())->error('Error $registro_id es menor a 0', $registro_id);
        }

        // Genera la lista de columnas SQL
        $columnas_sql = $this->columnas_sql($columnas, $entidad);
        if (error::$en_error) {
            return (new error())->error('Error al obtener $columnas_sql', $columnas_sql);
        }

        // Construye la clave primaria
        $key_id = $entidad . '.id';

        // Ajusta las condiciones adicionales
        $sql_where_extra = trim($sql_where_extra);
        if ($sql_where_extra !== '') {
            $sql_where_extra = " AND $sql_where_extra";
        }

        // Construye la consulta SQL
        $sql = /** @lang MYSQL */
            "SELECT $columnas_sql FROM $entidad WHERE $key_id = $registro_id $sql_where_extra";

        // Retorna la consulta SQL
        return trim($sql);
    }



    /**
     * EM3
     * Genera una consulta SQL `SELECT` con filtros, uniones (`LEFT JOIN`) y campos adicionales.
     *
     * Esta función valida los parámetros de entrada, construye dinámicamente una consulta SQL `SELECT`
     * que incluye filtros, uniones (`LEFT JOIN`) y campos extra, y retorna la consulta resultante.
     *
     * @param string $entidad El nombre de la entidad (tabla) base. No debe estar vacío.
     * @param string $filtro_sql La cláusula `WHERE` que define los filtros de la consulta. No debe estar vacía.
     * @param PDO $link Una conexión PDO válida para obtener los campos de la tabla si es necesario.
     * @param array $campos_extra Un array de campos adicionales que deben incluirse en la consulta.
     * @param array $joins Un array de configuraciones para generar las uniones SQL (`LEFT JOIN`).
     *
     * @return string|array Devuelve la consulta SQL generada como una cadena. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar consulta SELECT con uniones y campos extra
     * $entidad = 'empleados';
     * $filtro_sql = 'empleados.status = "activo"';
     * $campos_extra = ['empleados.nombre', 'departamentos.nombre AS departamento_nombre'];
     * $joins = [
     *     ['entidad_left' => 'empleados', 'entidad_right' => 'departamentos', 'rename_entidad' => 'emp']
     * ];
     * $resultado = $this->select_by_filter($entidad, $filtro_sql, $link, $campos_extra, $joins);
     * echo $resultado;
     * // Resultado:
     * // SELECT empleados.campos_sql empleados.nombre, departamentos.nombre AS departamento_nombre
     * // FROM empleados LEFT JOIN empleados AS emp ON emp.departamento_id = departamentos.id
     * // WHERE empleados.status = "activo"
     *
     * // Ejemplo 2: Error por entidad vacía
     * $entidad = '';
     * $resultado = $this->select_by_filter($entidad, $filtro_sql, $link, $campos_extra, $joins);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacia', 'data' => '']
     * ```
     */
    final public function select_by_filter(
        string $entidad,
        string $filtro_sql,
        PDO $link,
        array $campos_extra,
        array $joins
    ) {
        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        // Validar que el filtro SQL no esté vacío
        $filtro_sql = trim($filtro_sql);
        if ($filtro_sql === '') {
            return (new error())->error('Error $filtro_sql esta vacia', $filtro_sql);
        }

        // Generar la cadena de campos extra
        $campos_extra_sql = $this->campos_extra_sql($campos_extra);
        if (error::$en_error) {
            return (new error())->error('Error obtener $campos_extra_sql', $campos_extra_sql);
        }

        // Generar la cadena de uniones SQL
        $joins_sql = $this->joins_sql($joins);
        if (error::$en_error) {
            return (new error())->error('Error obtener $joins_sql', $joins_sql);
        }

        // Inicializar los campos por defecto
        $campos = new stdClass();
        $campos->campos_sql = '*';

        // Obtener los campos de la entidad si hay campos extra y uniones
        if (count($campos_extra) > 0 && count($joins) > 0) {
            $campos = (new consultas())->get_campos($entidad, $link);
            if (error::$en_error) {
                return (new error())->error('Error obtener $campos', $campos);
            }
        }

        // Construir la consulta SQL final
        $campos_query = "$campos->campos_sql $campos_extra_sql";
        return /** @lang MYSQL */ "SELECT $campos_query FROM $entidad $joins_sql WHERE $filtro_sql";
    }


    /**
     * EM3
     * Genera una sentencia SQL basada en un tipo de filtro y un array de condiciones.
     *
     * Esta función admite dos tipos de filtros: `NUMEROS` y `TEXTOS`. Dependiendo del tipo de filtro,
     * utiliza las funciones `genera_and` o `genera_and_textos` para construir una cláusula SQL.
     *
     * @param array $filtro Un array asociativo donde:
     *                      - Las claves son nombres de campos en formato `tabla.campo`.
     *                      - Los valores son los valores a comparar.
     * @param string $tipo_filtro El tipo de filtro que define el método a utilizar para generar la sentencia:
     *                            - `NUMEROS`: Genera sentencias utilizando `genera_and`.
     *                            - `TEXTOS`: Genera sentencias utilizando `genera_and_textos`.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula generada. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula para números
     * $filtro = [
     *     'empleados.id' => 1,
     *     'empleados.edad' => 30
     * ];
     * $tipo_filtro = 'NUMEROS';
     * $resultado = $this->sentencia($filtro, $tipo_filtro);
     * echo $resultado;
     * // Resultado:
     * // empleados.id = '1' AND empleados.edad = '30'
     *
     * // Ejemplo 2: Generar cláusula para textos
     * $filtro = [
     *     'empleados.nombre' => 'Juan',
     *     'empleados.apellido' => 'Pérez'
     * ];
     * $tipo_filtro = 'TEXTOS';
     * $resultado = $this->sentencia($filtro, $tipo_filtro);
     * echo $resultado;
     * // Resultado:
     * // empleados.nombre LIKE '%Juan%' AND empleados.apellido LIKE '%Pérez%'
     *
     * // Ejemplo 3: Tipo de filtro inválido (devuelve cadena vacía)
     * $filtro = [
     *     'empleados.nombre' => 'Juan'
     * ];
     * $tipo_filtro = 'INVALIDO';
     * $resultado = $this->sentencia($filtro, $tipo_filtro);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     * ```
     */
    final public function sentencia(array $filtro, string $tipo_filtro)
    {
        // Limpia y convierte el tipo de filtro a mayúsculas
        $tipo_filtro = trim($tipo_filtro);
        $tipo_filtro = strtoupper($tipo_filtro);

        $sentencia = '';

        // Genera la sentencia para filtros de números
        if ($tipo_filtro === 'NUMEROS') {
            $sentencia = $this->genera_and($filtro);
            if (error::$en_error) {
                return (new error())->error('Error al generar and', $sentencia);
            }
        }
        // Genera la sentencia para filtros de textos
        elseif ($tipo_filtro === 'TEXTOS') {
            $sentencia = $this->genera_and_textos($filtro);
            if (error::$en_error) {
                return (new error())->error('Error al generar and', $sentencia);
            }
        }

        // Retorna la sentencia generada o cadena vacía si el tipo de filtro es inválido
        return $sentencia;
    }




    /**
     * EM3
     * Genera una condición SQL de igualdad (`=`) entre un campo y un valor.
     *
     * Esta función valida los parámetros, escapa caracteres especiales en el campo y el valor,
     * y construye una condición SQL de igualdad. Si el valor es un campo (no un literal),
     * se omiten las comillas alrededor del valor.
     *
     * @param string $campo El nombre del campo que se usará en la condición. No debe estar vacío.
     * @param string $value El valor o campo con el que se comparará el campo. No debe estar vacío.
     * @param bool $value_es_campo (Opcional) Indica si `$value` es un campo en lugar de un literal.
     *                              Por defecto es `false` (se trata como un literal).
     *
     * @return string|array Devuelve la condición SQL generada. En caso de error, devuelve un array con
     *                      detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Comparar campo con un valor literal
     * $campo = 'status';
     * $value = 'activo';
     * $resultado = $this->sentencia_equals($campo, $value);
     * echo $resultado;
     * // Resultado:
     * // status = 'activo'
     *
     * // Ejemplo 2: Comparar campo con otro campo
     * $campo = 'tabla1.id';
     * $value = 'tabla2.id';
     * $resultado = $this->sentencia_equals($campo, $value, true);
     * echo $resultado;
     * // Resultado:
     * // tabla1.id = tabla2.id
     *
     * // Ejemplo 3: Campo vacío (error)
     * $campo = '';
     * $value = 'activo';
     * $resultado = $this->sentencia_equals($campo, $value);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     * ```
     */
    final public function sentencia_equals(string $campo, string $value, bool $value_es_campo = false)
    {
        // Validar que el campo no esté vacío
        $campo = trim($campo);
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        // Limpiar y escapar el campo y el valor
        $value = trim($value);
        $campo = addslashes($campo);
        $value = addslashes($value);

        // Si el valor no es un campo, envolver en comillas simples
        if (!$value_es_campo) {
            $value = "'$value'";
        }

        // Retornar la condición SQL
        return "$campo = $value";
    }


    /**
     * EM3
     * Genera una cláusula SQL `LIKE` con un valor específico para un campo.
     *
     * Esta función construye dinámicamente una cláusula SQL `LIKE`, asegurando que tanto el nombre del campo como el valor
     * estén limpios y correctamente escapados.
     *
     * @param string $campo El nombre del campo en la cláusula SQL. No puede estar vacío.
     * @param string $value El valor a buscar con `LIKE`. Puede estar vacío, pero será escapado.
     *
     * @return string|array Devuelve una cláusula SQL `LIKE` en el formato `campo LIKE '%valor%'`.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar una cláusula SQL LIKE
     * $campo = 'nombre';
     * $value = 'Juan';
     * $resultado = $this->sentencia_like($campo, $value);
     * echo $resultado;
     * // Resultado:
     * // nombre LIKE '%Juan%'
     *
     * // Ejemplo 2: Error por campo vacío
     * $campo = '';
     * $value = 'Juan';
     * $resultado = $this->sentencia_like($campo, $value);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     * ```
     */
    private function sentencia_like(string $campo, string $value)
    {
        // Limpiar el nombre del campo
        $campo = trim($campo);

        // Validar que el campo no esté vacío
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        // Escapar caracteres especiales en el campo
        $campo = addslashes($campo);

        // Limpiar y escapar el valor
        $value = trim($value);
        $value = addslashes($value);

        // Generar y retornar la cláusula SQL LIKE
        return "$campo LIKE '%$value%'";
    }


    /**
     * EM3
     * Genera una consulta SQL para listar las tablas de la base de datos actual.
     *
     * Esta función devuelve una consulta SQL simple para obtener todas las tablas
     * disponibles en la base de datos a la que está conectada la instancia de PDO.
     *
     * @return string Devuelve la consulta SQL `SHOW TABLES` como una cadena.
     *
     * @example
     * ```php
     * // Ejemplo 1: Obtener las tablas de la base de datos
     * $query = $this->show_tables();
     * echo $query;
     * // Resultado:
     * // SHOW TABLES
     *
     * // Ejemplo 2: Ejecutar la consulta para listar tablas
     * $resultado = $this->ejecuta_consulta2_obj($this->show_tables());
     * if (isset($resultado->error)) {
     *     echo "Error al obtener las tablas: " . $resultado->mensaje;
     * } else {
     *     foreach ($resultado->registros as $tabla) {
     *         echo "Tabla: " . reset($tabla);
     *     }
     * }
     * ```
     */
    final public function show_tables(): string
    {
        // Retornar la consulta SQL para listar las tablas
        return "SHOW TABLES";
    }


    /**
     * EM3
     * Sobreescribe dinámicamente una consulta SQL basada en un conjunto de configuraciones de reemplazo.
     *
     * Esta función toma una consulta SQL y un conjunto de reemplazos asociados a una transacción específica,
     * valida los datos, y aplica los reemplazos utilizando `consulta_sobreescrita`.
     *
     * @param string $consulta La consulta SQL original que se modificará.
     * @param array|object $replaces_consulta Un conjunto de configuraciones de reemplazo. Puede ser un array o un objeto.
     *                                        Debe contener un elemento correspondiente a `$transaccion`, que especifica
     *                                        los detalles del reemplazo.
     * @param string $transaccion La clave que identifica los reemplazos específicos dentro de `$replaces_consulta`.
     *
     * @return string|array Devuelve la consulta SQL modificada con los reemplazos realizados. En caso de error,
     *                      devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Reemplazos válidos en una consulta SQL
     * $consulta = "SELECT tabla1.nombre, tabla1.id FROM tabla1 INNER JOIN tabla2 ON tabla1.id = tabla2.tabla1_id";
     * $replaces_consulta = [
     *     'transaccion1' => [
     *         'join' => 'INNER JOIN tabla2 ON tabla1.id = tabla2.tabla1_id',
     *         'join_replace' => 'LEFT JOIN tabla2 ON tabla1.id = tabla2.tabla1_id',
     *         'columna' => 'tabla1.nombre',
     *         'columna_replace' => 'tabla2.nombre'
     *     ]
     * ];
     * $transaccion = 'transaccion1';
     * $resultado = $this->sobreescribe_sql($consulta, $replaces_consulta, $transaccion);
     * echo $resultado;
     * // Resultado:
     * // SELECT tabla2.nombre, tabla1.id FROM tabla1 LEFT JOIN tabla2 ON tabla1.id = tabla2.tabla1_id
     *
     * // Ejemplo 2: Transacción no encontrada (consulta original sin cambios)
     * $consulta = "SELECT tabla1.nombre FROM tabla1";
     * $replaces_consulta = [
     *     'transaccion2' => [
     *         'join' => 'INNER JOIN tabla2 ON tabla1.id = tabla2.tabla1_id',
     *         'join_replace' => 'LEFT JOIN tabla2 ON tabla1.id = tabla2.tabla1_id',
     *         'columna' => 'tabla1.nombre',
     *         'columna_replace' => 'tabla2.nombre'
     *     ]
     * ];
     * $transaccion = 'transaccion1'; // No coincide con ninguna clave en $replaces_consulta
     * $resultado = $this->sobreescribe_sql($consulta, $replaces_consulta, $transaccion);
     * echo $resultado;
     * // Resultado:
     * // SELECT tabla1.nombre FROM tabla1
     *
     * // Ejemplo 3: Reemplazos con formato incorrecto (error)
     * $consulta = "SELECT tabla1.nombre FROM tabla1";
     * $replaces_consulta = [
     *     'transaccion1' => 'esto_no_es_un_objeto'
     * ];
     * $transaccion = 'transaccion1';
     * $resultado = $this->sobreescribe_sql($consulta, $replaces_consulta, $transaccion);
     * // Resultado:
     * // ['error' => 'Error $replaces debe ser un objeto o un array', 'data' => [...]]
     * ```
     */
    final public function sobreescribe_sql(string $consulta, $replaces_consulta, string $transaccion)
    {
        // Convertir a array si el parámetro $replaces_consulta es un objeto
        if (is_object($replaces_consulta)) {
            $replaces_consulta = (array)$replaces_consulta;
        }

        // Verificar si existe un reemplazo para la transacción específica
        if (isset($replaces_consulta[$transaccion])) {
            $replaces = $replaces_consulta[$transaccion];

            // Convertir a objeto si es un array
            if (is_array($replaces)) {
                $replaces = (object)$replaces;
            }

            // Validar que $replaces sea un objeto
            if (!is_object($replaces)) {
                return (new error())->error('Error $replaces debe ser un objeto o un array', $replaces_consulta);
            }

            // Aplicar los reemplazos en la consulta
            $consulta = $this->consulta_sobreescrita($consulta, $replaces);
            if (error::$en_error) {
                return (new error())->error('Error al sobreescribir sql', $consulta);
            }
        }

        // Retornar la consulta modificada o la original si no se aplica reemplazo
        return $consulta;
    }


    /**
     * EM3
     * Genera una cláusula `WHERE` para una consulta SQL a partir de un conjunto de parámetros.
     *
     * Esta función toma un objeto de parámetros que describe los campos y valores para la condición `WHERE`,
     * valida los datos y construye dinámicamente la cláusula utilizando condiciones de igualdad (`=`) y operadores `AND`.
     *
     * @param stdClass $campos_where Un objeto que contiene los parámetros para construir la cláusula `WHERE`.
     *                               Cada propiedad debe ser un objeto con las siguientes claves:
     *                               - `campo` (string): El nombre del campo. No debe estar vacío.
     *                               - `value` (string): El valor o campo con el que se comparará.
     *                               - `value_es_campo` (bool, opcional): Indica si `value` es un campo en lugar de un literal.
     *                                 Por defecto es `false`.
     *
     * @return string|array Devuelve una cadena con la cláusula `WHERE` generada. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar una cláusula `WHERE` válida
     * $campos_where = (object)[
     *     (object)[
     *         'campo' => 'status',
     *         'value' => 'activo'
     *     ],
     *     (object)[
     *         'campo' => 'edad',
     *         'value' => '30',
     *         'value_es_campo' => false
     *     ]
     * ];
     * $resultado = $this->statement_where($campos_where);
     * echo $resultado;
     * // Resultado:
     * // status = 'activo' AND edad = '30'
     *
     * // Ejemplo 2: Falta la clave `campo` (error)
     * $campos_where = (object)[
     *     (object)[
     *         'value' => 'activo'
     *     ]
     * ];
     * $resultado = $this->statement_where($campos_where);
     * // Resultado:
     * // ['error' => 'Error $param->campo no existe', 'data' => [...]]
     *
     * // Ejemplo 3: Campo vacío (error)
     * $campos_where = (object)[
     *     (object)[
     *         'campo' => '',
     *         'value' => 'activo'
     *     ]
     * ];
     * $resultado = $this->statement_where($campos_where);
     * // Resultado:
     * // ['error' => 'Error $param->campo esta vacio', 'data' => [...]]
     * ```
     */
    final public function statement_where(stdClass $campos_where)
    {
        // Inicializar la cláusula WHERE
        $st_where = '';

        // Recorrer cada parámetro en $campos_where
        foreach ($campos_where as $param) {
            // Validar que el parámetro sea un objeto
            if (!is_object($param)) {
                return (new error())->error('Error $param debe ser un objeto', $param);
            }

            // Validar que el parámetro tenga las claves necesarias
            if (!isset($param->campo)) {
                return (new error())->error('Error $param->campo no existe', $param);
            }
            if (!isset($param->value)) {
                return (new error())->error('Error $param->value no existe', $param);
            }

            // Validar que el campo no esté vacío
            if (trim($param->campo) === '') {
                return (new error())->error('Error $param->campo esta vacio', $param);
            }

            // Determinar si se debe agregar `AND`
            $and = $this->and($st_where);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $and', $and);
            }

            // Validar y asignar valor predeterminado para `value_es_campo`
            if (!isset($param->value_es_campo)) {
                $param->value_es_campo = false;
            }

            // Generar la condición de igualdad
            $statement = $this->sentencia_equals($param->campo, $param->value, $param->value_es_campo);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $statement', $statement);
            }

            // Concatenar la condición al WHERE
            $st_where .= $and . $statement;
        }

        // Retornar la cláusula WHERE
        return $st_where;
    }


    /**
     * TRASLADADO
     * Genera una consulta SQL para sumar los valores de un campo específico basado en una entidad y un filtro SQL.
     *
     * @param string $campo El nombre del campo a sumar.
     * @param string $entidad El nombre de la tabla o entidad.
     * @param string $filtro_sql La condición de filtro SQL.
     * @return string|array La consulta SQL generada si los parámetros son válidos, de lo contrario un objeto de error.
     */
    final public function sum_by_filter(string $campo, string $entidad, string $filtro_sql)
    {
        $entidad = trim($entidad);
        if($entidad === ''){
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }

        $filtro_sql = trim($filtro_sql);
        if($filtro_sql === ''){
            return (new error())->error('Error $filtro_sql esta vacia', $filtro_sql);
        }

        $campo = trim($campo);
        if($campo === ''){
            return (new error())->error('Error $campo esta vacia', $campo);
        }

        return /** @lang MYSQL */ "SELECT SUM($campo) AS total FROM $entidad WHERE $filtro_sql";

    }

    /**
     * EM3
     * Valida los parámetros de entrada necesarios para renombrar un campo en una consulta SQL.
     *
     * Esta función verifica que los valores proporcionados para el alias, el campo y la entidad no estén vacíos.
     *
     * @param string $alias El alias que se utilizará para renombrar el campo. No debe estar vacío.
     * @param string $campo El nombre del campo que será renombrado. No debe estar vacío.
     * @param string $entidad El nombre de la entidad (tabla) que contiene el campo. No debe estar vacío.
     *
     * @return bool|array Devuelve `true` si todos los parámetros son válidos. En caso de error, devuelve un array
     *                    con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Validación exitosa
     * $alias = 'suma_monto';
     * $campo = 'monto';
     * $entidad = 'pagos';
     * $resultado = $this->valida_entrada_rename($alias, $campo, $entidad);
     * echo $resultado ? 'Validación exitosa' : 'Error';
     * // Resultado:
     * // Validación exitosa
     *
     * // Ejemplo 2: Error por alias vacío
     * $alias = '';
     * $campo = 'monto';
     * $entidad = 'pagos';
     * $resultado = $this->valida_entrada_rename($alias, $campo, $entidad);
     * // Resultado:
     * // ['error' => 'Error $alias esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Error por campo vacío
     * $alias = 'suma_monto';
     * $campo = '';
     * $entidad = 'pagos';
     * $resultado = $this->valida_entrada_rename($alias, $campo, $entidad);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     *
     * // Ejemplo 4: Error por entidad vacía
     * $alias = 'suma_monto';
     * $campo = 'monto';
     * $entidad = '';
     * $resultado = $this->valida_entrada_rename($alias, $campo, $entidad);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacio', 'data' => '']
     * ```
     */
    final public function valida_entrada_rename(string $alias, string $campo, string $entidad)
    {
        // Validar que el campo no esté vacío
        $campo = trim($campo);
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        // Validar que el alias no esté vacío
        $alias = trim($alias);
        if ($alias === '') {
            return (new error())->error('Error $alias esta vacio', $alias);
        }

        // Retornar true si todos los parámetros son válidos
        return true;
    }


    /**
     * EM3
     * Valida que un objeto de parámetros tenga las propiedades necesarias y que no estén vacías.
     *
     * Esta función verifica que un objeto (`stdClass`) contenga las propiedades `entidad` y `entidad_right`,
     * y que ambas propiedades no estén vacías tras eliminar espacios en blanco.
     *
     * @param stdClass $param El objeto de parámetros a validar.
     *
     * @return bool|array Devuelve `true` si el objeto tiene las propiedades requeridas y sus valores son válidos.
     *                    En caso de error, devuelve un array con los detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Objeto válido
     * $param = (object)[
     *     'entidad' => 'tabla_principal',
     *     'entidad_right' => 'tabla_relacionada'
     * ];
     * $resultado = $this->valida_param($param);
     * // Resultado: true
     *
     * // Ejemplo 2: Falta la propiedad `entidad`
     * $param = (object)[
     *     'entidad_right' => 'tabla_relacionada'
     * ];
     * $resultado = $this->valida_param($param);
     * // Resultado:
     * // ['error' => 'Error $param->entidad no existe', 'data' => [...]]
     *
     * // Ejemplo 3: Propiedad `entidad_right` vacía
     * $param = (object)[
     *     'entidad' => 'tabla_principal',
     *     'entidad_right' => ''
     * ];
     * $resultado = $this->valida_param($param);
     * // Resultado:
     * // ['error' => 'Error $param->entidad_right esta vacio', 'data' => [...]]
     * ```
     */
    private function valida_param(stdClass $param)
    {
        // Verificar que la propiedad `entidad` exista
        if (!isset($param->entidad)) {
            return (new error())->error('Error $param->entidad no existe', $param);
        }

        // Verificar que la propiedad `entidad_right` exista
        if (!isset($param->entidad_right)) {
            return (new error())->error('Error $param->entidad_right no existe', $param);
        }

        // Validar que `entidad` no esté vacía
        $entidad = trim($param->entidad);
        if ($entidad === '') {
            return (new error())->error('Error $param->entidad esta vacio', $param);
        }

        // Validar que `entidad_right` no esté vacía
        $entidad_right = trim($param->entidad_right);
        if ($entidad_right === '') {
            return (new error())->error('Error $param->entidad_right esta vacio', $param);
        }

        // Si todas las validaciones pasan, retornar true
        return true;
    }


    /**
     * EM3
     * Valida que el tipo de operador proporcionado sea válido y esté en la lista de operadores permitidos.
     *
     * Esta función asegura que `$type_operador` no esté vacío, lo convierte a mayúsculas y verifica si
     * está incluido en la lista de operadores permitidos (`$this->types_sentencias`).
     *
     * @param string $type_operador El tipo de operador a validar. No puede estar vacío.
     *
     * @return bool|array Devuelve `true` si el operador es válido. En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Validar un operador válido
     * $this->types_sentencias = ['SELECT', 'INSERT', 'UPDATE', 'DELETE'];
     * $type_operador = 'SELECT';
     * $resultado = $this->valida_type_operador($type_operador);
     * echo $resultado; // true
     *
     * // Ejemplo 2: Operador inválido
     * $type_operador = 'DROP';
     * $resultado = $this->valida_type_operador($type_operador);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $type_operador invalido', 'data' => ['SELECT', 'INSERT', 'UPDATE', 'DELETE']]
     *
     * // Ejemplo 3: Operador vacío
     * $type_operador = '';
     * $resultado = $this->valida_type_operador($type_operador);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error $type_operador esta vacio', 'data' => '']
     * ```
     */
    final public function valida_type_operador(string $type_operador)
    {
        // Limpiar el operador
        $type_operador = trim($type_operador);

        // Validar que el operador no esté vacío
        if ($type_operador === '') {
            return (new error())->error('Error $type_operador esta vacio', $type_operador);
        }

        // Convertir el operador a mayúsculas
        $type_operador = strtoupper($type_operador);

        // Verificar si el operador está en la lista de operadores permitidos
        if (!in_array($type_operador, $this->types_sentencias)) {
            return (new error())->error('Error $type_operador invalido', $this->types_sentencias);
        }

        // Retornar true si el operador es válido
        return true;
    }


    /**
     * EM3
     * Agrega un valor a una lista SQL delimitada por comas, manejando automáticamente la separación.
     *
     * Esta función permite construir dinámicamente una lista de valores SQL para utilizar en una cláusula `IN`.
     * Si ya hay valores en la lista, agrega una coma antes de añadir el nuevo valor.
     *
     * @param string $valor El valor a agregar. No puede estar vacío.
     * @param string $values_sql La lista actual de valores SQL. Puede estar vacía al inicio.
     *
     * @return string|array Devuelve la lista actualizada de valores SQL. En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Agregar valores a una lista SQL
     * $values_sql = '';
     * $values_sql = $this->values_in('123', $values_sql);
     * $values_sql = $this->values_in('456', $values_sql);
     * echo $values_sql;
     * // Resultado:
     * // '123', '456'
     *
     * // Ejemplo 2: Error por valor vacío
     * $values_sql = '';
     * $values_sql = $this->values_in('', $values_sql);
     * // Resultado:
     * // ['error' => 'Error $valor está vacío', 'data' => '']
     * ```
     */
    private function values_in(string $valor, string $values_sql)
    {
        // Limpia el valor
        $valor = trim($valor);

        // Validar que el valor no esté vacío
        if ($valor === '') {
            return (new error())->error('Error $valor está vacío', $valor);
        }

        // Generar la coma si la lista ya contiene valores
        $coma = $this->coma($values_sql);
        if (error::$en_error) {
            return (new error())->error('Error al generar $coma', $coma);
        }

        // Añadir el valor a la lista
        $values_sql .= "$coma '$valor'";

        // Retornar la lista actualizada
        return $values_sql;
    }



    /**
     * EM3
     * Genera una lista de valores SQL para una cláusula `IN`, formateando y validando cada valor.
     *
     * Esta función recorre un arreglo de valores, valida que no estén vacíos, y los agrega a una lista SQL delimitada por comas,
     * utilizando la función `values_in`.
     *
     * @param array $values Un arreglo de valores a incluir en la lista SQL. Ningún valor puede estar vacío.
     *
     * @return string|array Devuelve una lista de valores SQL en formato `'valor1', 'valor2', ...`.
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar una lista de valores SQL
     * $values = ['123', '456', '789'];
     * $resultado = $this->values_sql_in($values);
     * echo $resultado;
     * // Resultado:
     * // '123', '456', '789'
     *
     * // Ejemplo 2: Error por valor vacío en el arreglo
     * $values = ['123', '', '789'];
     * $resultado = $this->values_sql_in($values);
     * // Resultado:
     * // ['error' => 'Error $valor está vacío', 'data' => '']
     * ```
     */
    final public function values_sql_in(array $values)
    {
        // Inicializar la lista de valores SQL
        $values_sql = '';

        // Iterar sobre cada valor en el arreglo
        foreach ($values as $valor) {
            $valor = trim($valor);

            // Validar que el valor no esté vacío
            if ($valor === '') {
                return (new error())->error('Error $valor está vacío', $valor);
            }

            // Añadir el valor a la lista utilizando la función `values_in`
            $values_sql = $this->values_in($valor, $values_sql);
            if (error::$en_error) {
                return (new error())->error('Error al generar $values_sql', $values_sql);
            }
        }

        // Retornar la lista de valores SQL generada
        return $values_sql;
    }


    private function where_init(string $in_sql, array $row_data, string $sufijo)
    {
        $where = $this->where_empresa($in_sql,$row_data, $sufijo);
        if(error::$en_error){
            return (new error())->error('Error al obtener $where', $where);
        }

        $where = $this->where_plaza($row_data,$where);
        if(error::$en_error){
            return (new error())->error('Error al obtener $where', $where);
        }

        return $where;

    }

    final public function where_base_rpt(PDO $link, array $row_data, string $sufijo = '')
    {

        $in_sql = $this->genera_in_empresa($link);
        if(error::$en_error){
            return (new error())->error('Error al obtener $in_sql', $in_sql);
        }

        $where = $this->where_init($in_sql, $row_data, $sufijo);
        if(error::$en_error){
            return (new error())->error('Error al obtener $where', $where);
        }

        if($where !== ''){
            $where .= ' AND ';
        }

        return $where;

    }

    private function where_empresa(string $in_sql, array $row_data, string $sufijo)
    {
        $sufijo = trim($sufijo);

        $where = "empresa$sufijo.id IN ($in_sql)";
        if(isset($row_data['empresa_id']) && $row_data['empresa_id']!==''){
            $where = "empresa$sufijo.id = $row_data[empresa_id]";
        }

        return $where;

    }



    /**
     * TRASLADADO
     * Genera una condición WHERE SQL basada en el ID de una entidad.
     *
     * Esta función toma el nombre de una entidad y un valor de ID, valida que el nombre de la entidad no esté vacío
     * y que el ID sea mayor a 0. Si las validaciones pasan, genera una condición WHERE en formato SQL.
     *
     * @param string $entidad Nombre de la entidad que se usará en la condición SQL.
     * @param int $value Valor del ID que debe ser mayor a 0.
     *
     * @return string|array Retorna una cadena con la condición WHERE SQL o un array de error si alguna validación falla.
     */
    final public function where_id(string $entidad, int $value)
    {
        $entidad = trim($entidad);
        if($entidad === ''){
            return (new error())->error('Error $entidad esta vacia', $entidad);
        }
        if($value <= 0){
            return (new error())->error('Error $value debe ser mayor a 0', $value);
        }
        return "$entidad.id = $value";
    }

    private function where_plaza(array $row_data, string $where)
    {
        if(isset($row_data['plaza_id']) && $row_data['plaza_id']!==''){
            if($where !== ''){
                $where .= ' AND ';
            }
            $where .= "plaza.id = $row_data[plaza_id]";
        }

        return $where;

    }

    /**
     * EM3
     * Genera una cláusula SQL `WHERE` para filtrar registros no cancelados.
     *
     * Esta función toma el nombre de una entidad (tabla), lo valida y genera una cláusula SQL
     * que verifica si el campo `CANCELED` de la entidad tiene el valor `N` (no cancelado).
     *
     * @param string $entidad El nombre de la entidad (tabla) que se desea filtrar. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `WHERE`. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula `WHERE` válida
     * $entidad = 'facturas';
     * $resultado = $this->wh_canceled_n($entidad);
     * echo $resultado;
     * // Resultado:
     * // facturas.CANCELED = 'N'
     *
     * // Ejemplo 2: Entidad vacía (error)
     * $entidad = '';
     * $resultado = $this->wh_canceled_n($entidad);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacio', 'data' => '']
     * ```
     */
    final public function wh_canceled_n(string $entidad)
    {
        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        // Retornar la cláusula WHERE para registros no cancelados
        return "$entidad.CANCELED = 'N'";
    }


    /**
     * EM3
     * Genera una cláusula SQL `WHERE` para filtrar registros entre un rango de fechas.
     *
     * Esta función toma el nombre de un campo y dos campos de fechas (`inicio` y `fin`),
     * valida que no estén vacíos, y genera una cláusula SQL `BETWEEN` que verifica si el
     * valor del campo está entre las fechas especificadas.
     *
     * @param string $campo El nombre del campo que se evaluará en el rango de fechas. No debe estar vacío.
     * @param string $campo_fecha_fin El nombre del campo o valor que representa la fecha de fin del rango. No debe estar vacío.
     * @param string $campo_fecha_inicio El nombre del campo o valor que representa la fecha de inicio del rango. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `BETWEEN`. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula `BETWEEN` válida
     * $campo = 'empleados.fecha_ingreso';
     * $campo_fecha_inicio = "'2023-01-01'";
     * $campo_fecha_fin = "'2023-12-31'";
     * $resultado = $this->wh_entre_fechas($campo, $campo_fecha_fin, $campo_fecha_inicio);
     * echo $resultado;
     * // Resultado:
     * // empleados.fecha_ingreso BETWEEN '2023-01-01' AND '2023-12-31'
     *
     * // Ejemplo 2: Campo vacío (error)
     * $campo = '';
     * $campo_fecha_inicio = "'2023-01-01'";
     * $campo_fecha_fin = "'2023-12-31'";
     * $resultado = $this->wh_entre_fechas($campo, $campo_fecha_fin, $campo_fecha_inicio);
     * // Resultado:
     * // ['error' => 'Error $campo esta vacio', 'data' => '']
     *
     * // Ejemplo 3: Campo de fecha inicio vacío (error)
     * $campo = 'empleados.fecha_ingreso';
     * $campo_fecha_inicio = '';
     * $campo_fecha_fin = "'2023-12-31'";
     * $resultado = $this->wh_entre_fechas($campo, $campo_fecha_fin, $campo_fecha_inicio);
     * // Resultado:
     * // ['error' => 'Error $campo_fecha_inicio esta vacio', 'data' => '']
     * ```
     */
    final public function wh_entre_fechas(string $campo, string $campo_fecha_fin, string $campo_fecha_inicio)
    {
        // Validar que el campo no esté vacío
        $campo = trim($campo);
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        // Validar que el campo de fecha de inicio no esté vacío
        $campo_fecha_inicio = trim($campo_fecha_inicio);
        if ($campo_fecha_inicio === '') {
            return (new error())->error('Error $campo_fecha_inicio esta vacio', $campo_fecha_inicio);
        }

        // Validar que el campo de fecha de fin no esté vacío
        $campo_fecha_fin = trim($campo_fecha_fin);
        if ($campo_fecha_fin === '') {
            return (new error())->error('Error $campo_fecha_fin esta vacio', $campo_fecha_fin);
        }

        // Retornar la cláusula SQL BETWEEN
        return "$campo BETWEEN $campo_fecha_inicio AND $campo_fecha_fin";
    }


    /**
     * EM3
     * Genera una cláusula SQL `WHERE` para filtrar registros con estado activo.
     *
     * Esta función toma el nombre de una entidad (tabla), lo valida y genera una cláusula SQL
     * que verifica si el campo `status` de la entidad tiene el valor `activo`.
     *
     * @param string $entidad El nombre de la entidad (tabla) que se desea filtrar. No debe estar vacío.
     *
     * @return string|array Devuelve una cadena SQL con la cláusula `WHERE`. En caso de error, devuelve un array
     *                      con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula `WHERE` válida
     * $entidad = 'empleados';
     * $resultado = $this->wh_status_act($entidad);
     * echo $resultado;
     * // Resultado:
     * // empleados.status = 'activo'
     *
     * // Ejemplo 2: Entidad vacía (error)
     * $entidad = '';
     * $resultado = $this->wh_status_act($entidad);
     * // Resultado:
     * // ['error' => 'Error $entidad esta vacio', 'data' => '']
     * ```
     */
    final public function wh_status_act(string $entidad)
    {
        // Validar que la entidad no esté vacía
        $entidad = trim($entidad);
        if ($entidad === '') {
            return (new error())->error('Error $entidad esta vacio', $entidad);
        }

        // Retornar la cláusula WHERE para estado activo
        return "$entidad.status = 'activo'";
    }


    /**
     * EM3
     * Construye una cláusula SQL `WHERE` a partir de un array de condiciones.
     *
     * Esta función recorre un array de sentencias SQL y las combina en una sola cadena separada por `AND`.
     * Si el array está vacío, devuelve una cadena vacía.
     *
     * @param array $sentencias Un array de condiciones SQL. Cada elemento debe ser una cadena válida que represente una condición.
     *
     * @return string Devuelve una cadena SQL que combina todas las condiciones con `AND`. Si no hay condiciones, devuelve una cadena vacía.
     *
     * @example
     * ```php
     * // Ejemplo 1: Generar cláusula WHERE válida
     * $sentencias = [
     *     'contratos.status = "activo"',
     *     'contratos.CANCELED = "N"',
     *     'contratos.contrato_id = 123'
     * ];
     * $resultado = $this->where($sentencias);
     * echo $resultado;
     * // Resultado:
     * // contratos.status = "activo" AND contratos.CANCELED = "N" AND contratos.contrato_id = 123
     *
     * // Ejemplo 2: Array vacío
     * $sentencias = [];
     * $resultado = $this->where($sentencias);
     * echo $resultado;
     * // Resultado:
     * // (cadena vacía)
     * ```
     */
    final public function where(array $sentencias): string
    {
        // Inicializar la cadena SQL
        $sql = '';

        // Recorrer las sentencias y concatenarlas con AND
        foreach ($sentencias as $sentencia) {
            $and = '';
            if ($sql !== '') {
                $and = ' AND ';
            }
            $sql .= $and . $sentencia;
        }

        // Retornar la cláusula WHERE generada
        return $sql;
    }


    /**
     * EM3
     * Genera una cláusula SQL `WHERE` basada en el campo, el operador y el valor proporcionados.
     *
     * Esta función valida el tipo de operador (`LIKE` o `IGUAL`), limpia y escapa el campo y el valor,
     * y construye dinámicamente la cláusula SQL correspondiente.
     *
     * @param string $campo El nombre del campo para la cláusula `WHERE`. No puede estar vacío.
     * @param string $type_operador El tipo de operador a usar (`LIKE` o `IGUAL`). No puede estar vacío.
     * @param string $value El valor a comparar con el campo. Puede estar vacío, pero será escapado.
     *
     * @return string|array Devuelve una cláusula SQL `WHERE` en el formato:
     *                      - `WHERE campo LIKE '%valor%'`
     *                      - `WHERE campo = 'valor'`
     *                      En caso de error, devuelve un array con detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo: Generar cláusula WHERE con operador LIKE
     * $campo = 'nombre';
     * $type_operador = 'LIKE';
     * $value = 'Juan';
     * $resultado = $this->where_sentencia_base($campo, $type_operador, $value);
     * echo $resultado;
     * // Resultado:
     * // WHERE nombre LIKE '%Juan%'
     *
     * // Ejemplo 2: Generar cláusula WHERE con operador IGUAL
     * $campo = 'id';
     * $type_operador = 'IGUAL';
     * $value = '123';
     * $resultado = $this->where_sentencia_base($campo, $type_operador, $value);
     * echo $resultado;
     * // Resultado:
     * // WHERE id = '123'
     *
     * // Ejemplo 3: Error por operador no válido
     * $campo = 'id';
     * $type_operador = 'INVALIDO';
     * $value = '123';
     * $resultado = $this->where_sentencia_base($campo, $type_operador, $value);
     * print_r($resultado);
     * // Resultado:
     * // ['error' => 'Error al validar $type_operador', 'data' => [...]]
     * ```
     */
    private function where_sentencia_base(string $campo, string $type_operador, string $value)
    {
        // Validar que el operador no esté vacío
        $type_operador = trim($type_operador);
        if ($type_operador === '') {
            return (new error())->error('Error $type_operador esta vacio', $type_operador);
        }

        // Validar que el operador sea válido
        $valida = $this->valida_type_operador($type_operador);
        if (error::$en_error) {
            return (new error())->error('Error al validar $type_operador', $valida);
        }

        // Convertir el operador a mayúsculas para asegurar uniformidad
        $type_operador = strtoupper($type_operador);

        // Validar que el campo no esté vacío
        $campo = trim($campo);
        if ($campo === '') {
            return (new error())->error('Error $campo esta vacio', $campo);
        }

        // Inicializar la sentencia SQL
        $sentencia = '';

        // Generar la sentencia SQL según el operador
        if ($type_operador === 'LIKE') {
            $sentencia = $this->sentencia_like($campo, $value);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $sentencia', $sentencia);
            }
        }

        if ($type_operador === 'IGUAL') {
            $sentencia = $this->sentencia_equals($campo, $value);
            if (error::$en_error) {
                return (new error())->error('Error al obtener $sentencia', $sentencia);
            }
        }

        // Agregar la palabra clave WHERE si se generó una sentencia válida
        $where = '';
        if ($sentencia !== '') {
            $where = 'WHERE';
        }

        // Retornar la cláusula completa
        return trim("$where $sentencia");
    }





}