<?php
namespace desarrollo_em3\manejo_datos;

use desarrollo_em3\error\error;
use stdClass;

class webservice{

    /**
     * TRASLADADO
     * Inicializa una solicitud cURL configurada para enviar datos a un endpoint específico.
     *
     * Esta función crea una nueva instancia de cURL y la configura para enviar una solicitud a un endpoint
     * especificado. Permite configurar si la solicitud es de tipo POST y especificar los datos que se enviarán.
     * La función establece opciones básicas de cURL, como el seguimiento de redirecciones, el tiempo de espera
     * y la devolución de la respuesta como una cadena.
     *
     * @param string $endpoint La URL del endpoint al que se enviará la solicitud.
     * @param bool $es_post Indica si la solicitud debe ser de tipo POST. Si es `false`, se realizará una solicitud GET.
     * @param string $send_data Los datos que se enviarán en la solicitud. Se aplican solo si `$es_post` es `true`.
     *
     * @return resource Retorna un recurso cURL configurado y listo para ejecutar la solicitud.
     *
     * @access public
     * @final
     */
    final public function init(string $endpoint, bool $es_post, string $send_data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST,$es_post);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$send_data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        return $ch;

    }

}
