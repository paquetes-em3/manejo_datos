<?php
namespace desarrollo_em3\manejo_datos;


use desarrollo_em3\error\error;
use NumberFormatter;

class formato{

    final public function asigna_monto_moneda(string $key, $row)
    {
        $es_array = false;
        if(is_array($row)){
            $row = (object)$row;
            $es_array = true;
        }
        $monto = $this->moneda($row->$key);
        if(error::$en_error) {
           return  (new error())->error('Error al obtener asignar $monto', $monto);

        }
        $row->$key = $monto;
        if($es_array){
            $row = (array)$row;
        }
        return $row;

    }

    final public function asigna_montos_moneda(array $keys_moneda,  $row)
    {
        foreach ($keys_moneda as $key){
            $row = $this->asigna_monto_moneda($key,$row);
            if(error::$en_error) {
                return  (new error())->error('Error al asignar formato', $row);
            }
        }
        return $row;

    }

    private function download_csv(bool $download,string $name_out, string $url_file)
    {
        if($download) {
            ob_clean();
            header('Content-Type: application/csv');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=\"" . basename($name_out) . "\"");
            readfile($url_file);
            exit;
        }
        return file_get_contents($url_file);

    }


    private function genera_csv(array $headers, string $name_file, array $rows): array
    {

        $fp = fopen($name_file, 'w');
        if(count($headers) > 0) {
            fputcsv($fp, $headers);
        }
        foreach ($rows as $row) {
            fputcsv($fp, $row);
        }
        fclose($fp);

        return $rows;

    }
    final public function moneda($monto, string $currency = 'MXN', string $locale = 'es_MX')
    {
        $formatter = new NumberFormatter($locale, NumberFormatter::CURRENCY);
        return $formatter->formatCurrency($monto, $currency);

    }

    final public function out_json(bool $download, array $rows)
    {

        if($download) {
            ob_clean();
            header('Content-Type: application/json');
            echo json_encode($rows);
            exit;
        }
        return  json_encode($rows);
    }

    final public function out_csv(bool $download, array $rows, string $name_out, string $url_file)
    {
        $data = $this->genera_csv(array_keys($rows[0]),$name_out, $rows);
        if (error::$en_error) {
            return (new error())->error('Error al generar csv', $data);
        }
        $out = $this->download_csv($download,$name_out,$url_file);
        if (error::$en_error) {
            return (new error())->error('Error al dat salida de csv', $out);
        }
        return $out;

    }

}
