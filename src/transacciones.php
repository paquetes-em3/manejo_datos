<?php
namespace desarrollo_em3\manejo_datos;

use desarrollo_em3\error\error;
use PDO;
use Throwable;

class transacciones{
    private PDO $link;
    private string $tabla = '';
    private string $transaccion  = '';

    public function __construct(PDO $link){
        $this->link = $link;
    }

    /**
     * TRASLADADO
     * Ejecuta una consulta SQL y maneja los posibles errores.
     *
     * Esta función ejecuta una consulta SQL proporcionada, valida si la consulta no está vacía,
     * y maneja los posibles errores de ejecución. Devuelve un arreglo con el mensaje de éxito,
     * la consulta ejecutada, el resultado de la consulta y el ID del registro insertado si es
     * una transacción de tipo INSERT. Si se especifica, puede devolver los datos como un objeto.
     *
     * @param string $consulta La consulta SQL a ejecutar.
     * @param bool $return_obj (Opcional) Si es true, devuelve los datos como un objeto. Por defecto es false.
     *
     * @return array|object Devuelve un arreglo con el mensaje de éxito, la consulta ejecutada, el resultado de la consulta
     *                      y el ID del registro insertado si es una transacción de tipo INSERT.
     *                      Si ocurre un error, devuelve un array con el mensaje de error correspondiente.
     */
    final public function ejecuta_sql(string $consulta, bool $return_obj = false, string $tipo_transaccion = 'SELECT'){
        $consulta = trim($consulta);
        if($consulta === ''){
            return (new error())->error('La consulta no puede venir vacia',
                [$this->link->errorInfo(),$consulta]);
        }
        $result = $this->link->query($consulta);
        if($this->link->errorInfo()[1]){
            $mensaje_error = $this->link->errorInfo();
            $mensaje = '-';
            if(isset($mensaje_error[2])){
                $mensaje = $mensaje_error[2];
            }
            return (new error())->error('Error al ejecutar sql: '.$mensaje,
                [$this->link->errorInfo(),$consulta, $this->tabla]);
        }
        $registro_id = -1;
        if($tipo_transaccion ==='INSERT'){
            $registro_id = $this->link->lastInsertId();
        }
        $out = ['mensaje'=>'Exito','sql'=>$consulta,'result'=>$result, 'registro_id'=>$registro_id];
        if($return_obj){
            $out = (object)$out;
        }
        return $out;
    }

    /**
     * EM3
     * Ejecuta una consulta SQL de forma segura utilizando parámetros preparados.
     *
     * Esta función permite ejecutar una consulta SQL con protección contra inyecciones SQL
     * al utilizar parámetros preparados. Devuelve un array con los registros obtenidos,
     * el número de registros y la consulta SQL ejecutada, o un array de error en caso de falla.
     *
     * @param string $sql La consulta SQL a ejecutar. No debe estar vacía.
     * @param array $parametros Un array de parámetros para la consulta preparada. Por defecto, es un array vacío.
     *
     * @return array Devuelve un array con las siguientes claves si la consulta es exitosa:
     *               - `registros`: Un array asociativo con los resultados de la consulta.
     *               - `n_registros`: El número de registros devueltos.
     *               - `sql`: La consulta SQL ejecutada.
     *               En caso de error, devuelve un array con las claves:
     *               - `error`: Un indicador de error.
     *               - `mensaje`: Detalles del error.
     *
     * @example
     * ```php
     * // Ejemplo 1: Ejecutar una consulta válida con parámetros
     * $sql = "SELECT * FROM empleados WHERE departamento_id = :departamento_id";
     * $parametros = [':departamento_id' => 5];
     * $resultado = $this->ejecuta_consulta_segura($sql, $parametros);
     * if (isset($resultado['error'])) {
     *     echo "Error: " . $resultado['mensaje'];
     * } else {
     *     echo "Registros encontrados: " . $resultado['n_registros'];
     *     print_r($resultado['registros']);
     * }
     *
     * // Ejemplo 2: Consulta sin parámetros
     * $sql = "SELECT * FROM departamentos";
     * $resultado = $this->ejecuta_consulta_segura($sql);
     * if (isset($resultado['error'])) {
     *     echo "Error: " . $resultado['mensaje'];
     * } else {
     *     foreach ($resultado['registros'] as $registro) {
     *         echo "Departamento: " . $registro['nombre'];
     *     }
     * }
     *
     * // Ejemplo 3: Consulta vacía (error)
     * $sql = "";
     * $resultado = $this->ejecuta_consulta_segura($sql);
     * // Resultado:
     * // ['error' => 'Error sql esta vacio', 'data' => '']
     * ```
     */
    final public function ejecuta_consulta_segura(string $sql, array $parametros = array()): array
    {
        // Validar que la consulta SQL no esté vacía
        if ($sql === '') {
            return (new error())->error('Error sql esta vacio', $sql);
        }

        try {
            // Preparar y ejecutar la consulta
            $consulta_segura = $this->link->prepare($sql);
            $consulta_segura->execute($parametros);

            // Verificar errores en la ejecución
            if ($consulta_segura->errorInfo()[1]) {
                return (new error())->error('Error al ejecutar sql', [$sql, $consulta_segura->errorInfo()]);
            }

            // Obtener los resultados y el número de registros
            $registros = $consulta_segura->fetchAll(PDO::FETCH_ASSOC);
            $n_registros = $consulta_segura->rowCount();
            $consulta_segura->closeCursor();

            // Retornar los resultados
            return ['registros' => $registros, 'n_registros' => $n_registros, 'sql' => $sql];
        } catch (Throwable $e) {
            // Manejar excepciones durante la ejecución
            return (new error())->error('Error al ejecutar sql', $e->getMessage());
        }
    }


}
