<?php
namespace desarrollo_em3\manejo_datos;

use desarrollo_em3\error\error;
use desarrollo_em3\manejo_datos\sql\contrato_comision;
use desarrollo_em3\manejo_datos\sql\meta_asistente;
use desarrollo_em3\manejo_datos\sql\ohem_produccion_mensual;
use stdClass;

class _obten_meta_asistente
{
    /**
     * FIN
     * Genera consultas SQL relacionadas con la producción mensual, las metas y los contratos de un empleado.
     *
     * Esta función genera y valida consultas SQL para obtener información sobre la producción mensual,
     * las metas y los contratos de un empleado (identificado por $ohem_id) en un periodo específico.
     * Verifica que los parámetros de entrada sean válidos antes de generar las consultas.
     *
     * @param string $fecha_final La fecha final del periodo en formato 'YYYY-MM-DD'. No puede estar vacía.
     * @param string $fecha_inicial La fecha inicial del periodo en formato 'YYYY-MM-DD'. No puede estar vacía.
     * @param int $ohem_id El ID del empleado. Debe ser mayor a 0.
     * @param int $periodo_id El ID del periodo comercial. Debe ser mayor a 0.
     *
     * @return stdClass|array Un objeto que contiene las consultas SQL generadas.
     *                        Si algún parámetro no es válido, devuelve un array con el mensaje de error correspondiente.
     */
    final public function sqls(string $fecha_final, string $fecha_inicial, int $ohem_id, int $periodo_id)
    {
        if($ohem_id <= 0){
            return (new error())->error('Error ohem_id debe ser mayor a 0',$ohem_id);
        }
        if($periodo_id <= 0){
            return (new error())->error('Error $periodo_id debe ser mayor a 0',$periodo_id);
        }
        $fecha_inicial = trim($fecha_inicial);
        if($fecha_inicial === ''){
            return (new error())->error('Error fecha_inicial esta vacia',$fecha_inicial);
        }
        $fecha_final = trim($fecha_final);
        if($fecha_final === ''){
            return (new error())->error('Error $fecha_final esta vacia',$fecha_final);
        }

        $sql_meta_cantidad = (new meta_asistente())->cantidad_periodo($ohem_id,$periodo_id);
        if(error::$en_error){
            return (new error())->error('Error al generar  $sql_meta_cantidad', $sql_meta_cantidad);
        }

        $sql_plan_doble = (new contrato_comision())->contrato_doble($fecha_final,$fecha_inicial,$ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al generar  $sql_plan_doble', $sql_plan_doble);
        }
        $sql_n_ventas_personales = (new ohem_produccion_mensual())->n_ventas_personales($ohem_id);
        if(error::$en_error){
            return (new error())->error('Error al obtener $sql_n_ventas_personales',$sql_n_ventas_personales);
        }
        $sqls = new stdClass();
        $sqls->sql_meta_cantidad = $sql_meta_cantidad;
        $sqls->sql_plan_doble = $sql_plan_doble;
        $sqls->sql_n_ventas_personales = $sql_n_ventas_personales;

        return $sqls;

    }



}
